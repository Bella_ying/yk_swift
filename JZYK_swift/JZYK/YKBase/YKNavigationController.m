//
//  YKNavigationController.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKNavigationController.h"

@interface YKNavigationController ()

@end

@implementation YKNavigationController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupBar];
    }
    return self;
}

- (void)setupBar
{
    //导航栏背景色
    self.navigationBar.barTintColor = [UIColor whiteColor];        //背景颜色
    self.navigationBar.tintColor = Color.main;    //标题颜色
    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [QBColor yk_color_45_c2], NSFontAttributeName: [UIFont systemFontOfSize:18]};
    self.navigationBar.translucent = NO;
    [self setNeedsStatusBarAppearanceUpdate];
   
}

- (void)setIsHaveLineView:(BOOL)isHaveLineView
{
    _isHaveLineView = isHaveLineView;
    if (_isHaveLineView) {
        //有lineView
        UIView *lineView  = [UIView new];
        lineView.backgroundColor = [Color  backgroundColor];
        lineView.frame = CGRectMake(0, 43, WIDTH_OF_SCREEN, 1);
        [self.navigationBar addSubview:lineView];
    }
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (viewController != self.childViewControllers.firstObject && self.childViewControllers.count) {
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftBarButton];
        viewController.navigationItem.leftBarButtonItem.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
    
    //全局禁止滑动
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (UIButton *)leftBarButton
{
    UIButton *leftBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBarButton.frame = CGRectMake(0, 0, 55, 26);
    [leftBarButton addTarget:self action:@selector(popSelf) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_back"]];
    imageView.frame = CGRectMake(0, 5, 13, 21);
    [leftBarButton addSubview:imageView];
    
    return leftBarButton;
}

- (void)popSelf
{
    [self popViewControllerAnimated:YES];
}

@end
