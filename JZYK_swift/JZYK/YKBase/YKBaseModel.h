//
//  YKBaseModel.h
//  JZYK
//
//  Created by Jeremy on 2018/6/14.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKBaseModel : NSObject<NSCopying, NSCoding>

@end
