//
//  BaseControllerExtension.swift
//  JZYK
//
//  Created by Jeremy on 2018/6/19.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

extension YKBaseViewController {
    func changeControllerButton(_ button: Button, firstInput: InputView, secondInput: InputView) {
        var firstLengthLimit = 0;
        guard let firstType = firstInput.inputType else { return  }
        switch firstType {
        case .phoneNum:
            firstLengthLimit = 11;
        case .code:
            firstLengthLimit = 6;
        case .IDNum:
            firstLengthLimit = 18;
        case .loginPwd:
            firstLengthLimit = 6;
        case .name:
            firstLengthLimit = 4;
        }
        
        var secondLengthLimit = 0;
        guard let secondType = secondInput.inputType else { return  }
        switch secondType {
        case .phoneNum:
            secondLengthLimit = 11;
        case .code:
            secondLengthLimit = 6;
        case .IDNum:
            secondLengthLimit = 18;
        case .loginPwd:
            secondLengthLimit = 6;
        case .name:
            secondLengthLimit = 4;
        }
        
        firstInput.textFieldDidChangeCallBack = {(tf) in
            guard let firstCount = tf.text?.count else { return }
            if firstCount >= firstLengthLimit {
                if (secondInput.textField.text?.count)! >= secondLengthLimit {
                    button.switchButtonStatus(true)
                } else {
                    secondInput.textFieldDidChangeCallBack = { (tf) in
                        guard let count = tf.text?.count else { return }
                        if count >= secondLengthLimit && (firstInput.textField.text?.count)! >= firstLengthLimit {
                            button.switchButtonStatus(true)
                        } else {
                            button.switchButtonStatus(false)
                        }
                    }
                }
            } else {
                button.switchButtonStatus(false)
            }
            
        }
        
        secondInput.textFieldDidChangeCallBack = { (tf) in
            guard let firstCount = tf.text?.count else { return }
            if firstCount >= secondLengthLimit {
                if (firstInput.textField.text?.count)! >= firstLengthLimit {
                    button.switchButtonStatus(true)
                } else {
                    firstInput.textFieldDidChangeCallBack = { (tf) in
                        guard let count = tf.text?.count else { return }
                        if count >= firstLengthLimit && (secondInput.textField.text?.count)! >= secondLengthLimit {
                            button.switchButtonStatus(true)
                        } else {
                            button.switchButtonStatus(false)
                        }
                    }
                }
            } else {
                button.switchButtonStatus(false)
            }
        }
    }
    
    /// 找回交易密码跳转
    ///
    /// - Parameter comlete: 回调
    @objc public func findTransactionCode(toController comlete: @escaping (YKBaseViewController) -> Void )  {
        //kUserResetPayPassword
        guard let user = YKCacheManager.shared().yk_getCacheObject(forKey: kUserLoginCacheKey) as? YKUserModel else { return }
        guard let phone = user.username else { return }
        let findVc = TransactionCodeFindController()
        findVc.phoneText = phone;
        comlete(findVc)
//        CodeSecure.md5Encrypt(phoneNum: phone, encryptedCallBack: { (sign, random) in
//            let dict = ["phone": phone,
//                        "random": random,
//                        "sign": sign,
//                        "type" : "find_pay_pwd"];
//            HTTPManager.session.postRequest(forKey: kUserResetPwdCode, param: dict, succeed: { (json, code, unwrap, msg) in
//                if code == 0 {
////                    self.navigationController?.pushViewController(findVc, animated: true)
//                } else {
//                    iToast.makeText(msg).show();
//                }
//            }, failure: { (errMsg, isConnect) in
//                
//            })
//            
//        })
    }
    
}
