//
//  YKBaseViewController.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"
#import "YKLocationManager.h"

@interface YKBaseViewController()<UIGestureRecognizerDelegate>

@property (nonatomic, strong)UIButton *leftButton;

@end

@implementation YKBaseViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.showNavigationBar = YES;
        self.isStatusLight = NO;
        
        
    }
    return self;
}

- (BOOL)isStatusLight {
    if (!_isStatusLight) {
        return NO;
    }
    return _isStatusLight;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = self.isStatusLight ? UIStatusBarStyleLightContent : UIStatusBarStyleDefault;
    
    self.view.backgroundColor = [QBColor yk_backgroundColor];
    //设置是否隐藏NavigationBar
    [self.navigationController setNavigationBarHidden:!self.showNavigationBar animated:YES];
}

#pragma mark- 重写状态栏样式
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)hideKeyBoard
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}

- (void)dealloc
{
    if ([YKLocationManager location].amapActivate) {
        [[YKLocationManager location] yk_shutDownLocationService];
    }
}

- (void)hideNavigationBarLine
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)logIn
{
    if(![[YKUserManager sharedUser] yk_isLogin] ){
        if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[LoginViewController class]]) {
            LoginViewController *login = [LoginViewController login];
            if (login) {
                [self presentViewController:login animated:YES completion:nil];                
            }
        }
    } else {
#ifdef DEBUG
        [[iToast makeText:@"您已登录"] show];
#endif
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(void)creatBackButtonWithAction:(SEL)action {
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftButton];
    [self.leftButton addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UIButton *)leftButton
{
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftButton.frame = CGRectMake(0, 0, 55, 26);
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_back"]];
        imageView.frame = CGRectMake(0, 5, 13, 21);
        [_leftButton addSubview:imageView];
    }
    return _leftButton;
}


@end
