//
//  YKNavigationController.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKNavigationController : UINavigationController
@property (nonatomic,assign) BOOL isHaveLineView;//底部
@end
