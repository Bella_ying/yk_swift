//
//  YKBaseViewController.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKBaseViewController : UIViewController

@property (nonatomic, ) BOOL showNavigationBar;
@property (nonatomic, ) BOOL shouldNext;        //是否走认证向导
@property (nonatomic, ) BOOL isStatusLight;

- (void)hideNavigationBarLine;
- (void)logIn;

-(void)creatBackButtonWithAction:(SEL)action;
@end
