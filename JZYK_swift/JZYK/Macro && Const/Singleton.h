//
//  Singleton.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/4.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#ifndef Singleton_h
#define Singleton_h

// .h文件
#define SingletonH(name) + (instancetype)share##name;

// .m文件
#define SingletonM(name) \
static id _instance = nil; \
+ (id)allocWithZone:(struct _NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
} \
+ (instancetype)share##name \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[self alloc] init]; \
}); \
return _instance; \
} \
- (id)copyWithZone:(NSZone *)zone \
{ \
return _instance; \
}

#endif /* Singleton_h */
