//
//  YKGlobalConst.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/4.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

static CGFloat const kLineHeight = 0.5;
static NSString * const kSessionID = @"SESSIONID";
static NSString * const kUserAgentResult = @"uaResult";
static NSString * const kAppLocalVersion = @"app_local_version";
static NSString * const kFirstInApp = @"firstInApp";
static NSString * const kDebugDomainName = @".jisuqianbao.com";
static NSString * const kReleaseDomainName = @"jzyk.yuyaowangluo.com";
static NSString * const kReleaseURL = @"https://jzyk.yuyaowangluo.com/credit/web/credit-app/config";
static NSString * const kOfficialWebsite = @"http://www.yuyaowangluo.com/"; //官网链接
static NSString * const kPullRefreshUIDefaultTitle = @"信用让生活更美好";

/************************************  通知相关  ************************************/
static NSString * const kCustomVipAlertClosedNoti = @"kCustomVipAlertClosedNoti";
static NSString * const kUserLogOutNotiKey = @"loginOut";
static NSString * const kUserLoginSuccessNotiKey = @"loginSuccess";
