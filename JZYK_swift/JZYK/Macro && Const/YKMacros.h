//
//  DNMacros.h
//  dingniu
//
//  Created by Jeremy Wang on 05/02/2018.
//  Copyright © 2018 JSQB. All rights reserved.
//

#ifndef YKMacros_h
#define YKMacros_h

/************************************  UI适配相关的宏  ************************************/
#define HEIGHT_OF_SCREEN [[UIScreen mainScreen] bounds].size.height
#define WIDTH_OF_SCREEN [[UIScreen mainScreen] bounds].size.width

#define iPhoneX_HEIGHT_RATIO   (HEIGHT_OF_SCREEN == 812.0 ? 667.0/667.0 : HEIGHT_OF_SCREEN/667.0)
#define iPhoneX_ADD_NAV_HEIGHT (HEIGHT_OF_SCREEN == 812.0 ? 24 : 0)
#define NAV_HEIGHT             (HEIGHT_OF_SCREEN == 812.0 ? 88 : 64)
#define BOTTOM_HEIGHT          (HEIGHT_OF_SCREEN == 812.0 ? 34 : 0)
#define TABBAR_HEIGHT          (HEIGHT_OF_SCREEN == 812.0 ? 83 : 49)
#define STATUSBAR_HEIGHT       (HEIGHT_OF_SCREEN == 812.0  ? 44.f : 20.f)
#define AVAILABLE_IOS11        @available(iOS 11.0, *)

#define LINE_HEIGHT  0.5

/// 第一个参数是当下的控制器适配iOS11 一下的，第二个参数表示scrollview或子类
#define ADJUST_SCROLL_VIEW_INSET(controller,view) if(@available(iOS 11.0, *)) {view.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;} else if([controller isKindOfClass:[UIViewController class]]) {controller.automaticallyAdjustsScrollViewInsets = false;}

//使用像素值适配 即： 像素值 * 宏
#define WIDTH_RATIO (widthRatioOfCurrentAndIphone6() * 0.5)
#define HEIGHT_RATIO (heightRatioOfCurrentAndIphone6() * 0.5)

//使用pt值适配 即： pt值 * 宏
#define ASPECT_RATIO_HEIGHT (heightRatioOfCurrentAndIphone6())
#define ASPECT_RATIO_WIDTH (widthRatioOfCurrentAndIphone6())

#define IOS_SYSTEM_VERSION ([[[UIDevice currentDevice] systemVersion] doubleValue])

#define iOS8    @available(iOS 8.0, *)
#define iOS9    @available(iOS 9.0, *)
#define IOS9_2  @available(iOS 9.2, *)
#define iOS10   @available(iOS 10.0, *)
#define iOS11   @available(iOS 11.0, *)
#define iOS12   @available(iOS 12.0, *)

#define iOS_VERSION_7    (IOS_SYSTEM_VERSION >= 7.0)
#define iOS_VERSION_8    (IOS_SYSTEM_VERSION >= 8.0)
#define iOS_VERSION_9    (IOS_SYSTEM_VERSION >= 9.0)
#define iOS_VERSION_10   (IOS_SYSTEM_VERSION >= 10.0)
#define iOS_VERSION_11   (IOS_SYSTEM_VERSION >= 11.0)
#define iOS_VERSION_12   (IOS_SYSTEM_VERSION >= 12.0)

/************************************  项目全局相关的宏  ************************************/
#define KEY_WINDOW  ([UIApplication sharedApplication].keyWindow)

#define FILE_FULL_PATH(path) [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex: 0] stringByAppendingPathComponent: (path)]
#define FILE_FULL_PATH_IN_DOCUMENT(path) [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex: 0] stringByAppendingPathComponent: (path)]
#define FILE_CACHE_PATH(path) [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex: 0] stringByAppendingPathComponent: (path)]

#define IS_iPhone4 CGSizeEqualToSize(CGSizeMake(320, 480), [[UIScreen mainScreen] bounds].size)
#define IS_iPhone5 CGSizeEqualToSize(CGSizeMake(320, 568), [[UIScreen mainScreen] bounds].size)
#define IS_iPhone6 CGSizeEqualToSize(CGSizeMake(375, 667), [[UIScreen mainScreen] bounds].size)
#define IS_iPhone6p CGSizeEqualToSize(CGSizeMake(414, 736), [[UIScreen mainScreen] bounds].size)
#define IS_iPhoneX CGSizeEqualToSize(CGSizeMake(375, 812), [[UIScreen mainScreen] bounds].size)

#define is_User_logined ([YKUserManager sharedUser].yk_isLogin)

#define YK_NSUSER_DEFAULT ([NSUserDefaults standardUserDefaults])
#define YK_NOTIFICATION_CENTER [NSNotificationCenter defaultCenter]
#define YK_APP_VERSION    ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"])
#define WEAK_SELF     __weak typeof(self) weakSelf = self;
#define STRONG_SELF   __strong typeof(weakSelf) strongSelf = weakSelf;


#ifdef DEBUG
# define DLog(fmt, ...) NSLog((@"[函数名:%s]" "[行号:%d]" fmt),__FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
# define DLog(...);
#endif

//不被渲染图片
#define YK_ORIGINAL_IMAGE(name) [[UIImage imageNamed:name] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
//正常图片
#define YK_IMAGE(name) [UIImage imageNamed:name]

typedef void (^void_block_t)(void);

#define UA_FORMAT_STRING (@"%@ %@ %@  IOS %@/%@")    //UA设置，复制APP要更改rrkj字段

/*
 使用说明：
 NSString *str = NSStringFormat(@"%d",2);          str = 2;
 NSString *str1 = NSStringFormat(@"hello+%@",str); str1 = hello+2;
 */
#define NSStringFormat(format,...) [NSString stringWithFormat:format,##__VA_ARGS__]

#endif /* DNMacros_h */
