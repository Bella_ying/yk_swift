//
//  InlineFile.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/4.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//  全局使用的内联函数

static inline CGFloat widthRatioOfCurrentAndIphone6(){
    return [[UIScreen mainScreen] bounds].size.width/375.0;
}

static inline CGFloat heightRatioOfCurrentAndIphone6(){
    return [[UIScreen mainScreen] bounds].size.height/667.0;
}

//时间戳
static inline NSString * timestamp(){
    return [NSString stringWithFormat:@"%0.f", [[NSDate date] timeIntervalSince1970]];
}

//适配字体
static inline CGFloat adaptFontSize(CGFloat fontSize){
    return IS_iPhone6 ? fontSize : (IS_iPhone6 ? fontSize + floor(fontSize / 10) : fontSize - floor(fontSize / 10));
}

//首次登陆
static inline BOOL isFirstLaunch() {
    //此为找到plist文件中得版本号所对应的键 一般不知道这个健
    NSString *key = (NSString *)kCFBundleVersionKey;
    // 1.从plist中取出版本号
    NSString *version = [NSBundle mainBundle].infoDictionary[key];
    // 2.从沙盒中取出上次存储的版本号
    NSString * saveVersion = [YK_NSUSER_DEFAULT objectForKey:key];
    
    if(![version isEqualToString:saveVersion]  ) {
        //版本号不一样：第一次使用新版本
        //将新版本号写入沙盒
        [YK_NSUSER_DEFAULT setObject:version forKey:key];
        return YES;
    }
    if (![saveVersion isEqualToString: [YK_NSUSER_DEFAULT objectForKey:@"lastSaveVersion"]] && [YK_NSUSER_DEFAULT boolForKey:@"firstLogin"]) {
        [YK_NSUSER_DEFAULT setObject:saveVersion forKey:@"lastSaveVersion"];
        return YES;
    }
    return NO;
}

//获取APP名称
static inline NSString * appDisplayname() {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    
    return [infoDictionary objectForKey:@"CFBundleDisplayName"];
}


