//
//  HttpRequestKey.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

/*****************************App非模块功能*****************************/
#pragma mark App非模块功能

static NSString * const kAppEnvInfoKey = @"kAppEnvInfoKey";          // 环境key
static NSString * const kAppBundleVersion = @"kAppBundleVersion";    //缓存app版本的key，便于刚更新时区分当前版本与之前缓存的版本不一样
static NSString * const kConfigDataUrlKey = @"dataUrl";
//红点接口
static NSString * const kCreditHotDot = @"creditHotDot";

/*****************************账户相关*****************************/
#pragma mark 账户相关

//登录接口
static NSString * const kKDLoginKey = @"creditUserLogin";
//退出登录接口
static NSString * const kKDLogoutKey = @"creditUserLogout";
//快捷登录获取验证码
static NSString * const kUserQuickLoginCode = @"creditUserGetCodeLogin";
//快捷登录
static NSString * const kUserQuickLoginBySms = @"creditUserLoginBySms";
//设置手势密码拉取用户信息
static NSString * const kUserHandPasswordGetUserInfo = @"creditUserHandPasswordGetUserInfo";
//注册接口
static NSString * const kKDZhuceKey = @"creditUserRegister";
//注册验证码
static NSString * const KUserRegGetCode = @"creditUserRegGetCode";
//修改登录密码
static NSString * const kUserChangePwd = @"creditUserChangePwd";
//找回密码获取验证码
static NSString * const kUserResetPwdCode = @"creditUserResetPwdCode";
//获得未登录用户信息
static NSString * const kUserState = @"creditUserState";
//找回密码验证码
static NSString * const kUserVerifyResetPwdCode = @"creditUserResetPwdCode";
//找回密码验证个人信息
static NSString * const kUserVerifyResetPassword = @"creditUserVerifyResetPassword";
//找回登录密码设置新密码
static NSString * const kUserResetPassword = @"creditUserResetPassword";
//找回交易密码设置新密码
static NSString * const kUserResetPayPassword = @"creditUserResetPayPassword";
//初次设置交易密码
static NSString * const kUserSetPaypassword = @"creditUserSetPaypassword";
//修改交易密码
static NSString * const kUserChangePaypassword = @"creditUserChangePaypassword";
//我的借款
static NSString * const kUserLoanGetMyOrders = @"creditLoanGetMyOrders";
//订单冷静期间取消借款
static NSString * const kcreditLoanCancelColdTime = @"creditLoanCancelColdTime";
//账户余额
static NSString * const kcreditUserAccountBalance = @"creditUserAccountBalance";
//提现记录(活动提现)
static NSString * const kUserInviteRedPackPopMoneyList = @"userInviteRedPackPopMoneyList"; //红包提现记录
//提现记录(取现)
static NSString * const kCreditUserWithdrawLog = @"creditUserWithdrawLog";

/***************************我的优惠**********************/
//现金券-优惠券
static NSString * const kMyCoupon = @"creditMyCouponList";
//用户选择现金劵
static NSString * const KChoosecash = @"creditGetUserLoanList";
//确认现金劵抵扣
static NSString * const kConfirmCash = @"creditCouponDeductible";

//用户个人信息
static NSString * const kUserGetInfo = @"creditUserGetInfo";

//意见反馈
static NSString * const kCreditInfoFeedback = @"creditInfoFeedback";

/***************************首页**********************/
//桔子首页
static NSString * const kMallHomeIndex = @"mallHomeIndex";
//桔子消息中心
static NSString * const kCreditMsgCenter = @"creditMsgCenter";

static NSString * const kCreditGetMallList = @"creditGetMallList";
static NSString * const kAnouncementList = @"publicNoticeList"; //公告列表

static NSString * const kUserInviteRedPackCount = @"userInviteRedPackCount";//邀请红包汇总
static NSString * const kUserInviteRedPackList = @"userInviteRedPackList";   //邀请红包列表
static NSString *const kUserInviteRedPackPopMoney = @"userInviteRedPackPopMoney";        //红包提现申请

static NSString * const kCreditUserBaseProfile = @"CreditUserBaseProfile";//用户信息
//userInviteRedPackList
//userInviteRedPackPopMoneyList
//userInviteRedPackPopMoney

static NSString *const kCreditLoanAchieveLoan = @"creditLoanAchieveLoan";

//新版首页
static NSString * const kCreditAppIndexNew = @"creditAppMultiIndex";
//首页激活弹框点击事件
static NSString * const kCreditAppActiveShow = @"creditAppActiveShow";
//首页弹框
static NSString * const knoticePopBox  = @"noticePopBox";
//启动页广告
static NSString * const kNoticebounced = @"creditNoticeStartPop";
//首页滚动页
static NSString * const kNoticepopboxlist = @"creditNoticePopList";


//红包接口
static NSString * const kCreditIsExistCoupon = @"creditIsExistCoupon";
//拒就赔红包
static NSString * const kCreditUpdateRedPacket = @"creditUpdateRedPacket";
/**************************认证**********************/
//认证列表
static NSString * const kCreditCardGetVerificationInfo = @"creditCardGetVerificationInfo";
//获取个人信息
static NSString * const kCreditCardGetPersonInfo = @"creditCardGetPersonInfo";
//保存个人信息--未设置交易密码提交
static NSString * const kCreditCardSavePersonInfo = @"creditCardSavePersonInfo";
//保存个人信息--已设置交易密码提交
static NSString * const kCreditInfoSavePersonInfo = @"creditInfoSavePersonInfo";
//获取工作信息
static NSString * const kCreditCardGetWorkInfo = @"creditCardGetWorkInfo";
//保存工作信息
static NSString * const kCreditCardSaveWorkInfo = @"creditCardSaveWorkInfo";
//获取紧急联系人信息
static NSString * const kCreditCardGetContacts = @"creditCardGetContacts";
//保存紧急联系人信息
static NSString * const kCreditCardSaveContacts = @"creditCardSaveContacts";

//获取芝麻参数
static NSString * const kCreditZmMobileApi = @"creditZmMobileApi";
//上报芝麻返回信息
static NSString * const kZmMobileResultSave = @"creditZmMobileResultSave";
//上传通讯录总数目
static NSString * const kCreditInfoUploadContentsPrepared = @"creditInfoUploadContentsPrepared";
//上传通讯录
static NSString * const kInfoUpLoadContacts = @"creditInfoUploadContents";
//上报app信息
static NSString * const kReportKey = @"creditAppDeviceReport";
/**************************支付宝爬虫**********************/
//上报支付宝爬虫信息
static NSString * const kCrawlerKey = @"creditInfoCaptureUpload";

//获取支付宝加密信息
static NSString * const kCreditInfoCaptureInit = @"creditInfoCaptureInit";

//照片
//获取照片列表
static NSString * const kPictureGetPicList = @"creditPictureGetPicList";
//上传照片列表
static NSString * const kPictureUploadImage = @"creditPictureUploadImage";

static NSString * const kPictureUploadImg = @"creditPictureUploadImg";

//获取银行卡列表
static NSString * const kCreditCardBankList = @"creditCardBankList";
//获取绑卡验证码
static NSString * const kCreditCardGetCode = @"creditCardGetCode";
//获取用户绑卡
static NSString * const kCreditCardGetBankCard = @"creditCardGetBankCard";
//绑卡
static NSString * const kCreditCardBindCard = @"creditCardBindCard";
//获取用户是否开通存管
static NSString * const kCreditCardGetDepositOpenInfo = @"creditCardGetDepositOpenInfo";
//2.7.0 - 银行卡列表
static NSString * const kCreditCardCardList = @"creditCardCardList";
//2.7.0 - 设置主卡
static NSString * const kCreditCardSetMainCard = @"creditCardSetMainCard";
//2.7.0 - 解除绑定
static NSString * const kCreditCardUnbindCard = @"creditCardUnbindCard";
//3.0.0 - 获取个人信息
static NSString * const kCreditCardGetPersonAdditionInfo = @"creditCardGetPersonAdditionInfo";
//3.0.0 - 修改个人信息
static NSString * const kCreditCardSavePersonAdditionInfo = @"creditCardSavePersonAdditionInfo";

/***************************申请借钱*******************/
//借款首页
static NSString * const kCreditAppLoanIndex = @"creditAppLoanIndex";
//获取确认页面信息
static NSString * const kCreditLoanGetConfirmLoan = @"creditLoanGetConfirmLoan";
//获取确认页面信息(新)
static NSString * const kCreditLoanGetConfirmLoan_new = @"creditLoanGetConfirmLoanGai";
//申请借款
static NSString * const kCreditLoanApplyLoan = @"creditLoanApplyLoan";
//确认失败记录
static NSString * const kCreditConfirmFailedLoan = @"creditLoanConfirmFailedLoan";
//借款页面开通存管
static NSString * const kCreditUserDepositOpen = @"CreditUserDepositOpen";

/***************************提额**********************/
static NSString * const kUserCenterPromote = @"creditCardGetCardInfo";


/***************************还款**********************/
static NSString * const kUserLoan = @"creditLoanGetMyLoan";
//credit-loan/get-pay-order
static NSString * const kCreditLoanGetPayOrder = @"creditLoanGetPayOrder";
static NSString * const kCreditLoanApplyRepay = @"creditLoanApplyRepay"; //账单详情


static NSString *const kCreditCardChangeCardCheck = @"creditCardChangeCardCheck";
static NSString *const kCreditCardChangeCard = @"creditCardChangeCard";

/***************************上报用户位置信息**********************/
static NSString * const KUserLocation = @"creditInfoUploadLocation";
static NSString * const KUserAppInfo  = @"creditInfoUploadContents";

//认证中心获取额度
static NSString * const kCreditAppUserCreditTop = @"creditAppUserCreditTop";

//tabbar图片
static NSString * const kCreditTabBarList = @"creditTabBarList";
/***************************分享相关**********************/
//短信发送人数获取
static NSString * const kCreditMessageashare = @"creditCreditGetInviteLast";
//短信发送人数获取
static NSString * const kCreditMessageaUp = @"creditSendInviteSms";

//分享到社交网站计数统计
static NSString * const kSocialMediaSiteVisitStasticsURLKey = @"visit_stat_url";


//face++图片上报后台
static NSString * const kCreditFacePlusIdcard = @"FacePlusIdcard";

//百融SDK
static NSString * const kBrSdk = @"BrSdk";

/******************************公告******************************/
//['1'=>'借款首页','2'=>'借款详情页','3'=>'还款详情页','4'=>'我的页面','5'=>'提现页面'];
static NSString *const kLoanHomeAnouncementParam = @"1";
static NSString *const kLoanDetailAnouncementParam = @"2";
static NSString *const kPaymentDetailAnouncementParam = @"3";
static NSString *const kMineAnouncementParam = @"4";
static NSString *const kWithdrawAnouncementParam = @"5";

//['1'=>'借款首页','2'=>'借款详情页','3'=>'还款详情页','4'=>'我的页面','5'=>'提现页面']

static NSString *const kHomeAnouceStatuskey = @"homeAnouceStatus";
static NSString *const kLoanDetailAnouceStatuskey = @"loanDetailAnouceStatus";//kLoanDetailAnouncementParam
static NSString *const kPaymentDetailAnouceStatuskey = @"paymentDetailAnouceStatus";
static NSString *const kMineAnouceStatuskey = @"mineAnouceStatus";
static NSString *const kWithdrawAnouceStatuskey = @"withdrawAnouceStatus";

static NSString *const kCreditAppRandom = @"creditAppRandom";

static NSString *const kDiscoverGetInfo = @"discoverGetInfo";
static NSString *const kDiscoverQuotaLog = @"discoverQuotaLog"; //我的额度

static NSString *const kBqsSaveToken = @"BqsSaveToken";

static NSString *const kCreditCardGuide = @"creditCardGuide";

static NSString *const kCreditAppAppIndex = @"creditAppAppIndex";

static NSString *const kCreditAppAddAppJurisdiction = @"creditAppAddAppJurisdiction";

static NSString *const kCreditCardSaveZmxy = @"creditCardSaveZmxy";

static NSString *const kCreditLoanGetVipDialog = @"creditLoanGetVipDialog";

/******************************商城******************************/
static NSString *const kMallAppMallIndex = @"mallAppMallIndex";//app商城首页

static NSString *const kMallAppMallCzk = @"mallAppMallCzk";//app商城充值卡

static NSString *const kMallGetShopList = @"mallGetShopList";//获取标签商品列表

static NSString *const kMallGetShopListById = @"mallGetShopListById";//通过ID获取商品列表

static NSString *const kMallGetShopDetail = @"mallGetShopDetail";//获取商品详情

static NSString *const kAddressGetAll = @"addressGetAll";//获取所有地址信息

static NSString *const kMallShopInstallmentPost = @"mallShopInstallmentPost";//下单

static NSString *const kMallGetUserCard = @"mallGetUserCard";//获取银行卡信息，用于确认订单页，判断用户是否实名认证过


/******************************设置******************************/
static NSString *const kAddressUserAddressList = @"addressUserAddressList";//收货地址列表
static NSString *const kAddressUserAddressDefault = @"addressUserAddressDefault";//获取默认收货地址
static NSString *const kAddressGet = @"addressGet";//根据等级和父级ID获取地址列表
static NSString *const kAddressAddUserAddress = @"addressAddUserAddress";//用户收货地址添加
static NSString *const kAddressUpdateUserAddress = @"addressUpdateUserAddress";//用户收货地址修改
static NSString *const kAddressSetUserAddressStatus = @"addressSetUserAddressStatus";//用户收货地址状态改变 删除 或者 设置默认地址
static NSString *const kAbout_url = @"about_url";//关于我们




