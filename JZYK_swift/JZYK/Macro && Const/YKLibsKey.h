//
//  YKLibsKey.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//  第三方的库所使用的的key值

#import <Foundation/Foundation.h>

/* 使用高德地图API，请注册Key，注册地址：http://lbs.amap.com/console/key */
static NSString * const kAmapAPIKey = @"2a756d2259b3528bd1070556fac7da0d";

static NSString * const kAppMarket = @"AppStore";

static NSString * const kAppIDKey = @"1373573317";

static NSString * const kSchemeKey = @"schemejzyk";

static NSString * const kUmengAppKey = @"5ac9bc1da40fa369c7000109";

//极光推送相关
static NSString * const kJpushAppKey = @"0e11cf885441fb2a87e3d521";
static NSString * const kJpushMasterSecret = @"7220292d919b15f2e07db805";

//BugTags相关
static NSString *const kBugTagsAppKey = @"05a6481373d2af98e4eff53d6382fe1b";

//七鱼客服
static NSString *const kQiYuAppKey = @"a0589312596760e601e75f9b14643cab";

/**
 *  微信分享相关信息 wx0ff169d0b6be6e44 | c3c39453afdc0ed4227dacf6c6a0909f已更改
 */
static NSString * const kWxAppKey = @"wx0ff169d0b6be6e44";
static NSString * const kWxAppSecret = @"c3c39453afdc0ed4227dacf6c6a0909f";

/**
 *  QQ分享相关信息 1106994390 | dIIw0cgHnezskuMr已更改
 */
static NSString * const kQQ_URI = @"http://kj.hualiwangluo.com";
static NSString * const kQQAppID = @"1106910813";
static NSString * const kQQAppKey = @"yh3AUX3bIx6itI0F";
