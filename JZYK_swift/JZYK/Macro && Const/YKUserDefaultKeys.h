//
//  YKUserDefaultKeys.h
//  JZYK
//
//  Created by zhaoying on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#ifndef YKUserDefaultKeys_h
#define YKUserDefaultKeys_h

// 商城首页 installment_type 对应tab_type的key值 通知传值KEY
#define YK_MALL_INSTALLMENT_TYPE_KEY          @"ykInstallmentType"

// 商城首页 用户开启定位，高德返回位置信息成功时，发送通知，如果用户当前在商品详情页，更新用户的位置
#define YK_MALL_LOCATION_KEY          @"ykLocation"


#endif /*YKUserDefaultKeys_h */
