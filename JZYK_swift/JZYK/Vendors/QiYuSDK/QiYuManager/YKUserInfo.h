//
//  YKUserInfo.h
//  JZYK
//
//  Created by hongyu on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKUserInfo : NSObject

//真实姓名
@property (nonatomic, copy) NSString *realname;
//性别
@property (nonatomic, copy) NSString *sex;
//uid
@property (nonatomic, copy) NSString *uid;
//用户名
@property (nonatomic, copy) NSString *username;
//手机号码
@property (nonatomic, copy) NSString *phone;
//注册时间
@property (nonatomic, copy) NSString *regtime;

@end
