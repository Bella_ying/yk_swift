//
//  QiYuManager.m
//  JZYK
//
//  Created by hongyu on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "QiYuManager.h"
#import "YKUserInfo.h"
#import "YKTabBarController.h"

int64_t g_groupId;
int64_t g_staffId;
int64_t g_questionId;
BOOL    g_openRobotInShuntMode;

@interface QiYuManager ()<QYConversationManagerDelegate>

@end

@implementation QiYuManager

+ (instancetype)yk_sharedQiYuManager
{
    static dispatch_once_t onceToken;
    static QiYuManager *manager;
    dispatch_once(&onceToken, ^{
        manager = [QiYuManager new];
    });
    return manager;
}

- (void)yk_qiYuService
{
    [[HTTPManager session] postRequestForKey:@"CreditUserBaseProfile" showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        DLog(@"%@", json);
        YKUserInfo *ykUserInfo;
        if (code == 0 && success) {
            ykUserInfo = [YKUserInfo yy_modelWithDictionary:json];
        }
        QYUserInfo *userInfo = [[QYUserInfo alloc] init];
        userInfo.userId = ykUserInfo.uid;
        
        NSArray *array = @[@{@"key" : @"real_name", @"value" : ykUserInfo.realname},
                           @{@"key" : @"mobile_phone", @"value" : ykUserInfo.phone},
                           @{@"index" : @0, @"key" : @"sex", @"value" : ykUserInfo.sex, @"label" : @"性别"},
                           @{@"index" : @1, @"key" : @"reg_date", @"value" : ykUserInfo.regtime, @"label" : @"注册时间"}];
        
        NSData *userInfoData = [NSJSONSerialization dataWithJSONObject:array
                                                               options:0
                                                                 error:nil];
        if (json) {
            userInfo.data = [[NSString alloc] initWithData:userInfoData
                                                  encoding:NSUTF8StringEncoding];
        }
        DLog(@"-----%@------",userInfo.data);
        [[QYSDK sharedSDK] setUserInfo:userInfo];
        
        [[QYSDK sharedSDK] customUIConfig].sessionTipTextColor = GRAY_COLOR_45;
        [[QYSDK sharedSDK] customUIConfig].sessionTipTextFontSize = adaptFontSize(14);
        /**
         *  访客头像
         */
        [[QYSDK sharedSDK] customUIConfig].customerHeadImage = YK_IMAGE(@"user_header_icon");
//        [[QYSDK sharedSDK] customUIConfig].customerMessageBubbleNormalImage = YK_IMAGE(@"servise_chat_bg");
        // 消息间距
        [[QYSDK sharedSDK] customUIConfig].sessionMessageSpacing = 5.f;
        /**
         *  访客文本消息字体颜色
         */
        [[QYSDK sharedSDK] customUIConfig].customMessageTextColor = WHITE_COLOR;
        /**
         *  访客文本消息超链接字体颜色
         */
        [[QYSDK sharedSDK] customUIConfig].customMessageHyperLinkColor = [UIColor blueColor];
        /**
         *  访客文本消息字体大小
         */
        [[QYSDK sharedSDK] customUIConfig].customMessageTextFontSize = adaptFontSize(16);
        
        /**
         *  客服头像
         */
        [[QYSDK sharedSDK] customUIConfig].serviceHeadImage = [UIImage imageNamed:@"custom_service_icon"];
        /**
         *  客服文本消息字体颜色
         */
        [[QYSDK sharedSDK] customUIConfig].serviceMessageTextColor = GRAY_COLOR_45;
        /**
         *  客服文本消息超链接字体颜色
         */
        [[QYSDK sharedSDK] customUIConfig].serviceMessageHyperLinkColor = MAIN_THEME_COLOR;
        /**
         *  客服文本消息字体大小
         */
        [[QYSDK sharedSDK] customUIConfig].serviceMessageTextFontSize = adaptFontSize(14);
        /**
         *  键盘默认弹出
         */
        [[QYSDK sharedSDK] customUIConfig].autoShowKeyboard = NO;
        /**
         *  提示文本消息字体颜色；提示文本消息有很多种，比如“***为你服务”就是一种
         */
        [[QYSDK sharedSDK] customUIConfig].tipMessageTextColor = GRAY_COLOR_45;
        /**
         *  提示文本消息字体大小；提示文本消息有很多种，比如“***为你服务”就是一种
         */
        [[QYSDK sharedSDK] customUIConfig].tipMessageTextFontSize = adaptFontSize(14);
        [[QYSDK sharedSDK] customUIConfig].sessionMessageSpacing = 30 * WIDTH_RATIO;
        [QYCustomUIConfig sharedInstance].showEmoticonEntry = NO;
        QYSource *source = [[QYSource alloc] init];
        source.title =  @"桔子优卡";
        source.urlString = @"http://www.jisuqianbao.com/newh5/web/pc-site/index";
        QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
        sessionViewController.openRobotInShuntMode = YES;
        sessionViewController.sessionTitle = @"桔子优卡在线客服";
        sessionViewController.source = source;
        sessionViewController.groupId = self.groupId ? self.groupId : g_groupId;
        sessionViewController.staffId = self.staffId ? self.staffId : g_staffId;
        sessionViewController.commonQuestionTemplateId = self.commonQuestionTemplateId ? self.commonQuestionTemplateId : g_questionId;
        DLog(@"访客分组Id-%lld\n访客客服Id-%lld\n问题模板Id-%lld", sessionViewController.groupId, sessionViewController.staffId, sessionViewController.commonQuestionTemplateId);
        sessionViewController.openRobotInShuntMode = g_openRobotInShuntMode;
        sessionViewController.hidesBottomBarWhenPushed = YES;
        if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[QYSessionViewController class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[YKTabBarController yk_shareTabController] yk_pushToViewController:sessionViewController];
            });
        }
    } failure:^(NSString * _Nonnull error, BOOL isConnect) {
        
    }];
}

- (void)yk_qiYuDelegate
{
    [[[QYSDK sharedSDK] conversationManager] setDelegate:self];
}

#pragma mark- 未读消息数量
- (void)onUnreadCountChanged:(NSInteger)count
{
    if (self.allUnreadCount) {
        self.allUnreadCount(count);
    }
}

@end
