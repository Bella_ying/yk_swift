//
//  QiYuManager.h
//  JZYK
//
//  Created by hongyu on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QYSDK.h>

@interface QiYuManager : NSObject

@property (nonatomic, copy) void (^allUnreadCount)(NSInteger allUnreadCount);

// 访客分流 分组Id
@property (nonatomic, assign) int64_t groupId;
// 访客分流 客服Id
@property (nonatomic, assign) int64_t staffId;
// 常见问题 模版Id
@property (nonatomic, assign) int64_t commonQuestionTemplateId;

+ (instancetype)yk_sharedQiYuManager;
// 开启服务
- (void)yk_qiYuService;
// 签订代理
- (void)yk_qiYuDelegate;

@end
