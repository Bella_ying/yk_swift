//
//  YKGuidePageViewController.m
//  dingniu
//
//  Created by hongyu on 2018/3/7.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKGuidePageViewController.h"

#pragma mark- 加载滑动视图
@interface YKImageScrollView : UIScrollView

@end

@implementation YKImageScrollView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.bounces = NO;
    self.pagingEnabled = YES;
    self.showsVerticalScrollIndicator   = NO;
    self.showsHorizontalScrollIndicator = NO;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    __block CGRect frame = self.bounds;
    __block NSUInteger count = 0;
    
    [self.subviews enumerateObjectsUsingBlock:^(UIView *subView, NSUInteger idx, BOOL *stop) {
        if ([subView isKindOfClass:[UIImageView class]]) {
            CGFloat frameX = frame.size.width * idx;
            frame.origin.x = frameX;
            subView.frame  = frame;
            count++;
        }
    }];
    self.contentSize = CGSizeMake(frame.size.width * count, 0);
}

@end

#pragma mark- 视图加载

@interface YKGuidePageViewController ()

@property (nonatomic, strong)   YKImageScrollView *scrollView;
@property (nonatomic, strong) NSArray             *imageArray;

@property (nonatomic,copy) void (^enterBlock) (void);

@end

@implementation YKGuidePageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareUI];
}

+ (instancetype)startVcWithImageArray:(NSArray *)imageArray
                           enterBlock:(enterBlock)enter
{
    YKGuidePageViewController *startVc = [YKGuidePageViewController new];
    startVc.imageArray = imageArray;
    if (enter) {
        startVc.enterBlock = enter;
    }
    return startVc;
}

- (void)prepareUI
{
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.offset(0);
    }];
    
    WEAK_SELF
    [self.imageArray enumerateObjectsUsingBlock:^(NSString *imageName, NSUInteger idx, BOOL *stop) {
        
        UIImageView *imageV = [[UIImageView alloc] init];
        
        imageV.contentMode = UIViewContentModeScaleAspectFit;
        
        imageV.image = [UIImage imageNamed:imageName];
        
        imageV.tag = idx;
        
        [weakSelf.scrollView addSubview:imageV];
        
        if (idx == weakSelf.imageArray.count - 1) {
            
            [weakSelf setUpEnterButton:imageV];
        }
    }];
}

- (void)setUpEnterButton:(UIImageView *)imageView
{
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
    [imageView addGestureRecognizer:tap];
}

- (void)dismiss
{
    if (self.enterBlock) {
        self.enterBlock();
    }
}

- (void)gestureAction:(UITapGestureRecognizer *)tap
{
    [self dismiss];
}

- (YKImageScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [YKImageScrollView new];
    }
    return _scrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
