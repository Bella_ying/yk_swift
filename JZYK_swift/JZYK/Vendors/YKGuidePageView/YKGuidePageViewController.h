//
//  YKGuidePageViewController.H
//  dingniu
//
//  Created by hongyu on 2018/3/7.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^enterBlock) (void);

@interface YKGuidePageViewController : UIViewController

+ (instancetype)startVcWithImageArray:(NSArray *)imageArray
                           enterBlock:(enterBlock)enter;

@end
