//
//  BAAlertController.h
//  BAAlertController
//
//  Created by boai on 2017/6/5.
//  Copyright © 2017年 boai. All rights reserved.
//

#ifndef BAAlertController_h
#define BAAlertController_h

#import "UIAlertController+BAKit.h"
#import "NSObject+BARunTime.h"
#import "NSMutableAttributedString+BAKit.h"

#pragma mark - NotiCenter
#define BAKit_NotiCenter [NSNotificationCenter defaultCenter]

#pragma mark - 简单警告框
/*! view 用 BAKit_ShowAlertWithMsg */
#define BAKit_ShowAlertWithMsg(msg) [[[UIAlertView alloc] initWithTitle:@"温馨提示" message:(msg) delegate:nil cancelButtonTitle:@"确 定" otherButtonTitles:nil] show];
/*! VC 用 BAKit_ShowAlertWithMsg */
#define BAKit_ShowAlertWithMsg_ios8(msg) UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:msg preferredStyle:UIAlertControllerStyleAlert];\
UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确 定" style:UIAlertActionStyleDefault handler:nil];\
[alert addAction:sureAction];\
[self presentViewController:alert animated:YES completion:nil];

CG_INLINE UIColor *
BAKit_Color_RGBA(u_char r,u_char g, u_char b, u_char a) {
    return [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a];
}

#define BAKit_Color_Translucent    [UIColor colorWithRed:0.3f green:0.3f blue:0.3f alpha:0.5f]
#define BAKit_Color_White          [UIColor whiteColor]
#define BAKit_Color_Clear          [UIColor clearColor]
#define BAKit_Color_Black          [UIColor blackColor]
#define BAKit_Color_Red            [UIColor redColor]
#define BAKit_Color_Green          [UIColor greenColor]
#define BAKit_Color_Yellow         [UIColor yellowColor]
#define BAKit_Color_Orange         [UIColor orangeColor]

#define BAKit_Color_gray11         BAKit_Color_RGBA(248, 248, 248, 1.0)


#endif /* BAAlertController_h */
