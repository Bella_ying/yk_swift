//
//  BrAgentManager.m
//  KDFDApp
//
//  Created by yicheng on 2017/3/17.
//  Copyright © 2017年 cailiang. All rights reserved.
//

#import "BrAgentManager.h"
#import "BrAgent.h"

static BrAgentManager *_sharedManager;

@interface BrAgentManager ()

@end


@implementation BrAgentManager

+ (BrAgentManager *)yk_sharedManager
{
    @synchronized ([self class]) {
        if (!_sharedManager) {
            _sharedManager = [[[self class] alloc] init];
        }
        return _sharedManager;
    }
    return nil;
}

- (void)yk_initBrAgentWithType:(BRAGENTTYPE)type
{
    if (![[ConfigManager config] configForKey:@"br_apicode"] || [[[ConfigManager config] configForKey:@"br_apicode"] isEqualToString:@""]) {
        return;
    }
    
    [BrAgent setTimeoutInterval:3000]; //设置连接超时时间
    [BrAgent brInit:[[ConfigManager config] configForKey:@"br_apicode"]]; //获取后台下发的 百融key cid
    NSString *brType = @"";
    //区分提交到百融的事件
    switch (type) {
        case BRLOGIN:
        {
            brType = @"login";
        }
            break;
            
        case BRREGISTER:
        {
            brType = @"register";
        }
            break;
            
        case BRBORROW:
        {
            brType = @"lend";
        }
            
        default:
            break;
    }
    //组装调用百融SDK的参数
    NSMutableDictionary *loginInfo = [[NSMutableDictionary alloc] init];
    [loginInfo setObject:[NSString stringWithFormat:@"%li",(long)[YKUserManager sharedUser].uid] forKey:@"user_id"];
    [loginInfo setObject:[YKUserManager sharedUser].username forKey:@"user_name"];
    [loginInfo setObject:brType forKey:@"event"];
    
    //调用百融SDK
    [BrAgent onFraud:loginInfo completion:^(id feedback) {
        
        //将百融SDK返回参数提交到后台
        if ([feedback isKindOfClass:[NSDictionary class]] && [feedback[@"code"] integerValue] == 0) {
            if (feedback[@"response"] && [feedback[@"response"] isKindOfClass:[NSArray class]] && [feedback[@"response"] count] > 0) {
                
                DLog(@"%@",feedback);
                //取百融SDK返回的参数提交到后台
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                if (type == BRBORROW && self.order_id) [dic setValue:self.order_id forKey:@"order_id"];
                [dic setValuesForKeysWithDictionary:feedback[@"response"][0]];
                //上报后台
                [[HTTPManager session] postRequestForKey:kBrSdk showLoading:NO param:dic succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString * _Nonnull msg) {
                    if (code == 0 && success) {
                        DLog(@"百融SDK上报 = %@", msg);
                    }
                } failure:^(NSString * _Nonnull msg, BOOL isConnect) {
                    
                }];
            }
        }
    }];
}


@end
