//
//  BrAgentManager.h
//  KDFDApp
//
//  Created by yicheng on 2017/3/17.
//  Copyright © 2017年 cailiang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,BRAGENTTYPE) {
    BRLOGIN,        //登陆成功时调用
    BRREGISTER,     //注册成功时调用
    BRBORROW        //借款提交成功时调用
};


@interface BrAgentManager : NSObject


+ (BrAgentManager *)yk_sharedManager;

@property (nonatomic, copy) NSString *order_id;

/**
 传入事件类型，先和百融后台交互，成功后和后台交互

 param launchOptions 提交百融数据
 @param type 事件类型
 */
- (void)yk_initBrAgentWithType:(BRAGENTTYPE)type;


@end
