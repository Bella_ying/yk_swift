//
//  BqsDeviceFingerManager.h
//  KDFDApp
//
//  Created by wangjeremy on 24/07/2017.
//  Copyright © 2017 cailiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BqsDeviceFingerManager : NSObject

/**
 初始化白骑士设备指纹sdk并上传Tokenkey
 */
+ (instancetype)yk_sharedWhiteKnightManager;

-(void)yk_initBqsDFSDKAndSubmitTokenkeyToBackend;

@end
