//
//  BqsDeviceFingerManager.m
//  KDFDApp
//
//  Created by wangjeremy on 24/07/2017.
//  Copyright © 2017 cailiang. All rights reserved.
//

#import "BqsDeviceFingerManager.h"
#import "BqsDeviceFingerPrinting.h"

static NSString *const kPartnerID = @"shlr";

@interface BqsDeviceFingerManager ()<BqsDeviceFingerPrintingDelegate, BqsDeviceFingerPrintingContactsDelegate>

@end

@implementation BqsDeviceFingerManager

+ (instancetype)yk_sharedWhiteKnightManager
{
    static dispatch_once_t onceToken;
    static BqsDeviceFingerManager *manager;
    dispatch_once(&onceToken, ^{
        manager = [BqsDeviceFingerManager new];
    });
    return manager;
}

/*
 官方DEMO建议在viewWillAppear阶段初始化
*/

#pragma mark 初始化白骑士设备指纹sdk并上传Tokenkey
-(void)yk_initBqsDFSDKAndSubmitTokenkeyToBackend
{
    DLog(@"初始化白骑士SDK...");
    
    //1、添加设备信息采集回调（设置代理）
    [[BqsDeviceFingerPrinting sharedBqsDeviceFingerPrinting] setBqsDeviceFingerPrintingDelegate:self];
    [[BqsDeviceFingerPrinting sharedBqsDeviceFingerPrinting] setBqsDeviceFingerPrintingContactsDelegate:self];
    
    //2、配置初始化参数
    NSDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:kPartnerID forKey:@"partnerId"];//商户编号
    [params setValue:@"NO" forKey:@"isGatherContacts"];//是否采集通讯录，YES:采集 ,NO：不采集
#ifdef DEBUG
    [params setValue:@"NO" forKey:@"isTestingEnv"];//是否连接白骑士测试环境，NO：生产环境，YES：测试环境
#else
    [params setValue:@"NO" forKey:@"isTestingEnv"];//是否连接白骑士测试环境，NO：生产环境，YES：测试环境
#endif
    [params setValue:@"NO" forKey:@"isGatherGps"];
    [params setValue:@"NO" forKey:@"isGatherSensorinfo"];
    //3、执行初始化
    [[BqsDeviceFingerPrinting sharedBqsDeviceFingerPrinting] initBqsDFSdkWithParams:params];
    
    //4、提交tokenkey到公司服务器
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSString *tokenkey = [[BqsDeviceFingerPrinting sharedBqsDeviceFingerPrinting] getTokenKey];
        //如果上一次tokenkey与此次相同不提交
        if(![[YK_NSUSER_DEFAULT objectForKey:@"BQStoken"] isEqualToString:tokenkey]){
            [YK_NSUSER_DEFAULT setObject:tokenkey forKey:@"BQStoken"];
            [self submitTokenkey:tokenkey];
        }
    });
}


#pragma mark - BqsDeviceFingerPrintingDelegate
-(void)onBqsDFInitSuccess:(NSString *)tokenKey{
    DLog(@"初始化成功 tokenKey=%@", tokenKey);
}

-(void)onBqsDFInitFailure:(NSString *)resultCode withDesc:(NSString *)resultDesc{
    DLog(@"SDK初始化失败 resultCode=%@, resultDesc=%@", resultCode, resultDesc);
}

#pragma mark - BqsDeviceFingerPrintingContactsDelegate
-(void)onBqsDFContactsUploadSuccess:(NSString *)tokenKey{
    DLog(@"通讯录上传成功 tokenKey=%@", tokenKey);
}

-(void)onBqsDFContactsUploadFailure:(NSString *)resultCode withDesc:(NSString *)resultDesc{
    DLog(@"通讯录上传失败 resultCode=%@, resultDesc=%@", resultCode, resultDesc);
}

/**
 上传这个tokenkey到贵公司服务器，贵公司服务器将tokenkey传给白骑士服务器
 */
- (void)submitTokenkey:(NSString *)tokenkey
{
    DLog(@"提交tokenkey:%@", tokenkey);
    

    [[HTTPManager session] postRequestForKey:kBqsSaveToken showLoading:NO param:@{@"token_key":tokenkey,@"platform":@"ios"} succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString * _Nonnull msg) {
        if (code == 0 && success) {
            DLog(@"bqsRequest ==== %@", msg);
        }
    } failure:^(NSString * _Nonnull msg, BOOL isConnect) {
        DLog(@"bqsRequest  fail ==== %@",msg);
    }];
}

@end
