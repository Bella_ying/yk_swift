//
//  UITabBar+category.m
//  JZYK
//
//  Created by hongyu on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKTabBar+category.h"
#define TabbarItemNums 4.0

@implementation YKTabBar (category)

- (void)yk_showBadgeOnItemIndex:(NSInteger)index
{
    [self removeBadgeOnItemIndex:index];
    // 新建小红点
    UIView *redPointView = [[UIView alloc] init];
    redPointView.tag = 1000 + index;
    redPointView.layer.cornerRadius = 5;
    redPointView.clipsToBounds = YES;
    redPointView.backgroundColor = [UIColor yk_colorWithHexString:@"FF7575"];
    CGRect tabFram = self.frame;
    
    float percentX = (index + 0.6) / TabbarItemNums;
    CGFloat x = ceilf(percentX * tabFram.size.width);
    CGFloat y = ceilf(0.1 * tabFram.size.height);
    redPointView.frame = CGRectMake(x, y, 10, 10);
    [self addSubview:redPointView];
    [self bringSubviewToFront:redPointView];
}

-(void)yk_hideBadgeOnItemIndex:(NSInteger)index
{
    [self removeBadgeOnItemIndex:index];
}

- (void)removeBadgeOnItemIndex:(NSInteger)index
{
    for (UIView*subView in self.subviews) {
        if (subView.tag == 1000 + index) {
            [subView removeFromSuperview];
        }
    }
}

@end
