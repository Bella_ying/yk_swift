//
//  UITabBar+category.h
//  JZYK
//
//  Created by hongyu on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKTabBar (category)

- (void)yk_showBadgeOnItemIndex:(NSInteger)index;

- (void)yk_hideBadgeOnItemIndex:(NSInteger)index;

@end
