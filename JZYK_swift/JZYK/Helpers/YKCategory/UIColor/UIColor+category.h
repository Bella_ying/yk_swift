//
//  UIColor+category.h
//  JZYK
//
//  Created by hongyu on 2018/6/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (category)

+ (UIColor *)yk_colorWithHexString:(NSString *)hexString;
+ (UIColor *)yk_colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha;
+ (UIColor *(^)(NSString * hexColor))yk_hex;

@end
