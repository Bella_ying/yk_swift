//
//  UIImageView+category.h
//  JZYK
//
//  Created by hongyu on 2018/6/20.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ActionBlock)(UIButton *button);
@interface UIButton (category)

- (void)yk_showRedPointOnButtonWithCount:(NSInteger)count;

- (void)yk_hideRedPointOnButton;

- (void)yk_handleControlEvent:(UIControlEvents)controlEvent withBlock:(ActionBlock)block ;

@end
