//
//  UIImageView+category.m
//  JZYK
//
//  Created by hongyu on 2018/6/20.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "UIButton+category.h"
static char overviewKey;
@implementation UIButton (category)

- (void)yk_showRedPointOnButtonWithCount:(NSInteger)count
{
    [self removeBadgeOnButton];
    
    UIView *noReadView = [[UIView alloc] init];
    noReadView.tag = 1000;
    noReadView.layer.cornerRadius  = 7;
    noReadView.layer.masksToBounds = YES;
    noReadView.layer.borderWidth   = 0.5;
    noReadView.layer.borderColor   = [UIColor whiteColor].CGColor;
    
    noReadView.backgroundColor = [UIColor whiteColor];
    CGFloat x = 17 * ASPECT_RATIO_WIDTH;
    CGFloat y = -3.5;
    // count > 99 展示99+ 增加宽度
    noReadView.frame = CGRectMake(x, y, count > 99 ? 20 : 14, 14);
    [self addSubview:noReadView];
    [self bringSubviewToFront:noReadView];
    
    UILabel *numLabel = [UILabel new];
    // count > 99 展示99+ < 99 展示count
    if (count <= 99) {
        numLabel.text = [NSString stringWithFormat:@"%zd", count];
    } else {
        numLabel.text = @"99+";
    }
    numLabel.textColor = MAIN_THEME_COLOR;
    numLabel.textAlignment = NSTextAlignmentCenter;
    // count 0-10 10-99 99+ 展示不同字体大小
    NSInteger font;
    if (count < 10) {
        font = 12;
    } else if (count >= 10 && count <= 99) {
        font = 10;
    } else  {
        font = 9;
    }
    numLabel.font = [UIFont boldSystemFontOfSize:font];
    [noReadView addSubview:numLabel];
    [numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.offset(0);
    }];
}

-(void)yk_hideRedPointOnButton
{
    [self removeBadgeOnButton];
}

- (void)removeBadgeOnButton
{
    for (UIView*subView in self.subviews) {
        if (subView.tag == 1000) {
            [subView removeFromSuperview];
        }
    }
}

- (void)yk_handleControlEvent:(UIControlEvents)controlEvent withBlock:(ActionBlock)block {
    objc_setAssociatedObject(self, &overviewKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addTarget:self action:@selector(callActionBlock:) forControlEvents:controlEvent];
}

- (void)callActionBlock:(id)sender {
    ActionBlock block = (ActionBlock)objc_getAssociatedObject(self, &overviewKey);
    if (block) {
        block(self);
    }
}

@end
