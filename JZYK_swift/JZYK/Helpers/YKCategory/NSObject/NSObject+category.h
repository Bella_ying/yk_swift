//
//  NSObject+category.h
//  KDZX
//
//  Created by zhaoying on 2017/9/7.
//  Copyright © 2017年 11. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (category)

/**
 Method Swizzling来把系统的方法交换为我们自己的方法，从而给系统方法添加一些我们想要的功能。

 @param orginalSelector 系统方法
 @param swizzledSelector 替换方法
 */
+ (void)yk_methodWithOriginalSelector:(SEL)orginalSelector
                   bySwizzledSelector:(SEL)swizzledSelector;

@end
