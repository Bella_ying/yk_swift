//
//  NSObject+category.m
//  KDZX
//
//  Created by zhaoying on 2017/9/7.
//  Copyright © 2017年 11. All rights reserved.
//

#import "NSObject+category.h"

@implementation NSObject (category)

+ (void)yk_methodWithOriginalSelector:(SEL)orginalSelector bySwizzledSelector:(SEL)swizzledSelector
{
    Class class = [self class];
    Method originalMethod = class_getInstanceMethod(class, orginalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    BOOL didAddMethod     = class_addMethod(class, orginalSelector,
                                            method_getImplementation(swizzledMethod),
                                            method_getTypeEncoding(swizzledMethod));
    if (didAddMethod) {
        class_replaceMethod(class,swizzledSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}

@end
