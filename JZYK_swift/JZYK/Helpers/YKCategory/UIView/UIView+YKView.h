//
//  UIView+YKView.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (YKView)

@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign, readonly) CGFloat maxX;
@property (nonatomic, assign, readonly) CGFloat maxY;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize  size;
@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat bottom;


- (void)yk_removeAllSubViews;
- (BOOL)yk_findAndResignFirstResponder;
- (UIView *)yk_findFirstResponder;
- (UIViewController *)yk_viewController;

@end
