//
//  NSTimer+YKTimer.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (YKTimer)

+ (NSTimer *)yk_schedualTimerWithTimeInterval:(NSTimeInterval)interval
                                      repeats:(BOOL)repeats
                                        block:(void(^)(void))block;

@end
