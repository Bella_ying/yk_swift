//
//  NSTimer+YKTimer.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "NSTimer+YKTimer.h"

@implementation NSTimer (YKTimer)
/*
 为什么NStimer会出现retain cycle?
 因为在viewController中调用timer的时候，其target是self实例，所以timer就会引用self，而如果计时器是用实例变量保存的时候，这时，self引用了timer。
 */
+ (NSTimer *)yk_schedualTimerWithTimeInterval:(NSTimeInterval)interval
                                      repeats:(BOOL)repeats
                                        block:(void(^)(void))block
{
    return [self scheduledTimerWithTimeInterval:interval
                                         target:self
                                       selector:@selector(yk_blockInvove:)
                                       userInfo:[block copy]
                                        repeats:repeats];
    //Block要拷贝到堆上，否则等到稍后要执行的时候，该block可能无效。
}

+ (void)yk_blockInvove:(NSTimer *)timer
{
    void (^block)(void) = timer.userInfo;
    if (block) {
        block();
    }
}

@end
