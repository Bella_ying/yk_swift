//
//  UIFont+YKFont.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/7.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (YKFont)

+ (UIFont *(^)(CGFloat fontSize))yk_size;

@end
