//
//  UIFont+YKFont.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/7.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "UIFont+YKFont.h"

@implementation UIFont (YKFont)

+ (UIFont *(^)(CGFloat fontSize))yk_size
{
    return ^(CGFloat fontSize){
        return [UIFont systemFontOfSize:adaptFontSize(fontSize)];
    };
}

@end
