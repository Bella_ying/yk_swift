
//
//  NSDictionary+category.m
//  KDZX
//
//  Created by zhaoying on 2017/9/7.
//  Copyright © 2017年 11. All rights reserved.
//

#ifndef YK_TARGET_NEED_UNICODE_CONVERSION
#ifdef DEBUG
#define YK_TARGET_NEED_UNICODE_CONVERSION 1
#else
#define YK_TARGET_NEED_UNICODE_CONVERSION 0
#endif
#endif

#if YK_TARGET_NEED_UNICODE_CONVERSION

#import <objc/runtime.h>
#import "NSObject+category.h"

#endif

#import "NSDictionary+category.h"

@implementation NSDictionary (category)

#if YK_TARGET_NEED_UNICODE_CONVERSION

+ (void)load
{
    // 更换系统方法
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self yk_methodWithOriginalSelector:@selector(descriptionWithLocale:indent:)
                         bySwizzledSelector:@selector(YKDescriptionWithLocale:indent:)];
    });
}

#pragma mark - debug环境下unicode转换为中文
- (NSString *)YKDescriptionWithLocale:(id)locale indent:(NSUInteger)level
{
    return [self stringByReplaceUnicode:[self YKDescriptionWithLocale:locale indent:level]];
}

#endif

- (NSString *)stringByReplaceUnicode:(NSString *)unicodeString
{
    NSMutableString *convertedString = [unicodeString mutableCopy];
    [convertedString replaceOccurrencesOfString:@"\\U" withString:@"\\u" options:0 range:NSMakeRange(0, convertedString.length)];
    CFStringRef transform = CFSTR("Any-Hex/Java");
    CFStringTransform((__bridge CFMutableStringRef)convertedString, NULL, transform, YES);
    
    return convertedString;
}

@end
