//
//  MBProgressHUD+CustomToast.m
//  dingniu
//
//  Created by Jeremy Wang on 27/02/2018.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "MBProgressHUD+CustomToast.h"
#import "MBProgressHUD.h"

@implementation MBProgressHUD (CustomToast)

+ (void)showText:(NSString *)text iconName:(NSString *)iconName
{
    [MBProgressHUD showText:text iconName:iconName view:KEY_WINDOW];
}

+ (void)showText:(NSString *)text iconName:(NSString *)iconName afterHideBlock:(void(^)(void))block
{
    [MBProgressHUD showText:text iconName:iconName view:KEY_WINDOW afterHideBlock:block];
}

+ (void)showText:(NSString *)text iconName:(NSString *)iconName view:(UIView *)view
{
    [MBProgressHUD showText:text iconName:iconName view:view afterHideBlock:nil];
}

+ (void)showText:(NSString *)text iconName:(NSString *)iconName view:(UIView *)view  afterHideBlock:(void(^)(void))block
{
    if (view == nil) {
        view = [[UIApplication sharedApplication].windows lastObject];
    }
    view.userInteractionEnabled = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        // 快速显示一个提示信息
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.label.text = text;
        hud.label.numberOfLines= 0;
        hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
        hud.backgroundView.color = [UIColor colorWithRed:51/255 green:51/255 blue:51/255 alpha:0.7];
        hud.label.textColor = [UIColor yk_colorWithHexString:@"#333333"];
        hud.bezelView.style =  MBProgressHUDBackgroundStyleBlur;
        hud.label.font = [UIFont systemFontOfSize:15.0];
        hud.userInteractionEnabled= NO;
        hud.bezelView.backgroundColor =[UIColor whiteColor];    //背景颜色
        // 设置图片
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
        // 再设置模式
        hud.mode = MBProgressHUDModeCustomView;
        
        // 隐藏时候从父控件中移除
        hud.removeFromSuperViewOnHide = YES;
        
        // 1秒之后再消失
        [hud hideAnimated:YES afterDelay:2];
        
        [hud setCompletionBlock:^{
            view.userInteractionEnabled = YES;
            if (block) {
                block();
            }
        }];
    });
}

+ (void)showHudWithMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].windows lastObject] animated:YES];
        hud.label.text     = message;
        hud.label.numberOfLines = 0;
        hud.label.font     = [UIFont systemFontOfSize:adaptFontSize(15)];
        hud.bezelView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
        [hud setOffset:CGPointMake(0, 0)];
        hud.mode            = MBProgressHUDModeText;
        hud.margin          = 7.f;
        hud.label.textColor = WHITE_COLOR;
        hud.layer.cornerRadius = 8.f;
        hud.removeFromSuperViewOnHide = YES;
        hud.animationType  = MBProgressHUDAnimationZoomOut; // 消失动画
        [hud hideAnimated:YES afterDelay:1.f];
    });
}

@end
