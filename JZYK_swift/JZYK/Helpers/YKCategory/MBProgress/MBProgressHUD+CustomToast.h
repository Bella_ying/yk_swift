//
//  MBProgressHUD+CustomToast.h
//  dingniu
//
//  Created by Jeremy Wang on 27/02/2018.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (CustomToast)

+ (void)showText:(NSString *)text iconName:(NSString *)iconName;

+ (void)showText:(NSString *)text iconName:(NSString *)iconName view:(UIView *)view;

+ (void)showText:(NSString *)text iconName:(NSString *)iconName afterHideBlock:(void(^)(void))block;

/** 只展示文字 */
+ (void)showHudWithMessage:(NSString *)message;

@end
