//
//  NSDate+category.h
//  KDFDApp
//
//  Created by hongyu on 2017/8/31.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (category)

+ (NSCalendar *)currentCalendar;
+ (NSDate *)yk_convertDateToLocalTime:(NSDate *)forDate;

#pragma mark - 相对日期
+ (NSDate *)dateNow;
+ (NSDate *)dateTomorrow;
+ (NSDate *)dateYesterday;
+ (NSDate *)dateWithDaysFromNow:(NSInteger)days;
+ (NSDate *)dateWithDaysBeforeNow:(NSInteger)days;
+ (NSDate *)dateWithHoursFromNow:(NSInteger)dHours;
+ (NSDate *)dateWithHoursBeforeNow:(NSInteger)dHours;
+ (NSDate *)dateWithMinutesFromNow:(NSInteger)dMinutes;
+ (NSDate *)dateWithMinutesBeforeNow:(NSInteger)dMinutes;

#pragma mark - 日期转字符串
- (NSString *)stringWithDateStyle:(NSDateFormatterStyle)dateStyle timeStyle:(NSDateFormatterStyle)timeStyle;
- (NSString *)stringWithFormat:(NSString *)format;
@property (nonatomic, readonly) NSString *shortString;
@property (nonatomic, readonly) NSString *shortDateString;
@property (nonatomic, readonly) NSString *shortTimeString;
@property (nonatomic, readonly) NSString *mediumString;
@property (nonatomic, readonly) NSString *mediumDateString;
@property (nonatomic, readonly) NSString *mediumTimeString;
@property (nonatomic, readonly) NSString *longString;
@property (nonatomic, readonly) NSString *longDateString;
@property (nonatomic, readonly) NSString *longTimeString;

#pragma mark - 日期比较
- (BOOL)yk_isEqualToDateIgnoringTime:(NSDate *)aDate;

- (BOOL)yk_isToday;
- (BOOL)yk_isTomorrow;
- (BOOL)yk_isYesterday;

- (BOOL)yk_isSameWeekAsDate:(NSDate *)aDate;
- (BOOL)yk_isThisWeek;
- (BOOL)yk_isNextWeek;
- (BOOL)yk_isLastWeek;

- (BOOL)yk_isSameMonthAsDate:(NSDate *)aDate;
- (BOOL)yk_isThisMonth;
- (BOOL)yk_isNextMonth;
- (BOOL)yk_isLastMonth;

- (BOOL)yk_isSameYearAsDate:(NSDate *)aDate;
- (BOOL)yk_isThisYear;
- (BOOL)yk_isNextYear;
- (BOOL)yk_isLastYear;

- (BOOL)yk_isEarlierThanDate:(NSDate *)aDate;
- (BOOL)yk_isLaterThanDate:(NSDate *)aDate;

- (BOOL)yk_isInFuture;
- (BOOL)yk_isInPast;

#pragma mark - 日期规则
- (BOOL)yk_isTypicallyWorkday;
- (BOOL)yk_isTypicallyWeekend;

#pragma mark - 调整日期
- (NSDate *)yk_dateByAddingYears:(NSInteger)dYears;
- (NSDate *)yk_dateBySubtractingYears:(NSInteger)dYears;
- (NSDate *)yk_dateByAddingMonths:(NSInteger)dMonths;
- (NSDate *)yk_dateBySubtractingMonths:(NSInteger)dMonths;
- (NSDate *)yk_dateByAddingDays:(NSInteger)dDays;
- (NSDate *)yk_dateBySubtractingDays:(NSInteger)dDays;
- (NSDate *)yk_dateByAddingHours:(NSInteger)dHours;
- (NSDate *)yk_dateBySubtractingHours:(NSInteger)dHours;
- (NSDate *)yk_dateByAddingMinutes:(NSInteger)dMinutes;
- (NSDate *)yk_dateBySubtractingMinutes:(NSInteger)dMinutes;

#pragma mark - 极端日期
- (NSDate *)yk_dateAtStartOfDay;
- (NSDate *)yk_dateAtEndOfDay;

#pragma mark - 日期间隔
- (NSInteger)yk_minutesAfterDate:(NSDate *)aDate;
- (NSInteger)yk_minutesBeforeDate:(NSDate *)aDate;
- (NSInteger)yk_hoursAfterDate:(NSDate *)aDate;
- (NSInteger)yk_hoursBeforeDate:(NSDate *)aDate;
- (NSInteger)yk_daysAfterDate:(NSDate *)aDate;
- (NSInteger)yk_daysBeforeDate:(NSDate *)aDate;
- (NSInteger)yk_distanceInDaysToDate:(NSDate *)anotherDate;

#pragma mark - 分解日期
@property (readonly) NSInteger nearestHour;
@property (readonly) NSInteger hour;
@property (readonly) NSInteger minute;
@property (readonly) NSInteger seconds;
@property (readonly) NSInteger day;
@property (readonly) NSInteger month;
@property (readonly) NSInteger weekOfMonth;
@property (readonly) NSInteger weekOfYear;
@property (readonly) NSInteger weekday;
@property (readonly) NSInteger nthWeekday;
@property (readonly) NSInteger year;

@end
