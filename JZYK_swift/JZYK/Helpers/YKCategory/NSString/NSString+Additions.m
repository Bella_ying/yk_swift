//
//  NSString+Additions.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/4.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "NSString+Additions.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Additions)

+ (id)yk_deserializeMessageJSON:(NSString *)messageJSON
{
    return [NSJSONSerialization JSONObjectWithData:[messageJSON dataUsingEncoding:NSUTF8StringEncoding]
                                           options:NSJSONReadingAllowFragments error:nil];
}

- (BOOL)yk_isEmpty
{
    if (!self || [self isEqualToString:@""]) {
        return YES;
    }
    return NO;
}

- (BOOL)yk_isValidString
{
    if (self && ![self isKindOfClass:[NSNull class]] && [self isKindOfClass:[NSString class]] && self.length && ![self isEqualToString:@""] && ![self isEqualToString:@"<null>"] && ![self isEqualToString:@"null"]) {
        return YES;
    }
    return NO;
}

// 返回md5加密后的字符串
- (NSString *)yk_stringEncryptedWithMD5
{
    if(self == nil || [self length] == 0)
        return nil;
    
    const char *value = [self UTF8String];
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (CC_LONG)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    return outputString;
}

// 把传入的参数按照get的方式打包到url后面。
+ (NSString *)yk_addQueryStringToUrl:(NSString *)url
                           params:(NSDictionary *)params
{
    if (nil == url) {
        return @"";
    }
    NSMutableString *urlWithQuerystring = [[NSMutableString alloc] initWithString:url];
    // Convert the params into a query string
    if (params) {
        for(id key in params) {
            NSString *sKey = [key description];
            NSString *sVal = [[params objectForKey:key] description];
            //是否有？，必须处理这个
            if ([urlWithQuerystring rangeOfString:@"?"].location==NSNotFound) {
                [urlWithQuerystring appendFormat:@"?%@=%@", [NSString yk_urlEscape:sKey], [NSString yk_urlEscape:sVal]];
            } else {
                [urlWithQuerystring appendFormat:@"&%@=%@", [NSString yk_urlEscape:sKey], [NSString yk_urlEscape:sVal]];
            }
        }
    }
    
    return urlWithQuerystring;
}

//-----------------------
+ (NSString *)yk_addQueryStringWithparams:(NSDictionary *)params
{
    NSMutableString *urlWithQuerystring = [[NSMutableString alloc] initWithString:@""];
    if (params) {
        for(id key in params) {
            NSString *sKey = [key description];
            NSString *sVal = [[params objectForKey:key] description];
            if ([urlWithQuerystring rangeOfString:@"&"].location==NSNotFound) {
                if (params.count == 1) {
                    [urlWithQuerystring appendFormat:@"%@=%@", [NSString yk_urlEscape:sKey], [NSString yk_urlEscape:sVal]];
                } else {
                    [urlWithQuerystring appendFormat:@"%@=%@&", [NSString yk_urlEscape:sKey], [NSString yk_urlEscape:sVal]];
                }
            } else {
                [urlWithQuerystring appendFormat:@"%@=%@", [NSString yk_urlEscape:sKey], [NSString yk_urlEscape:sVal]];
            }
        }
    }
    
    return urlWithQuerystring;
}

- (NSAttributedString *)yk_stringWithParagraphlineSpeace:(CGFloat)lineSpacing
                                           wordSpace :(CGFloat)wordSpace
                                            textColor:(UIColor *)textcolor
                                             textFont:(UIFont *)font
{
    // 设置段落
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = lineSpacing;
    // NSKernAttributeName字体间距
    NSDictionary *attributes = @{ NSParagraphStyleAttributeName:paragraphStyle,NSKernAttributeName:@(wordSpace)};
    NSMutableAttributedString * attriStr = [[NSMutableAttributedString alloc] initWithString:self attributes:attributes];
    // 创建文字属性
    NSDictionary * attriBute = @{NSForegroundColorAttributeName:textcolor,NSFontAttributeName:font};
    [attriStr addAttributes:attriBute range:NSMakeRange(0, self.length)];
    return attriStr;
}


/**
 *  计算富文本字体size
 *
 *  @param lineSpeace 行高
 *  @param font       字体
 *  @param width      字体所占宽度
 *
 *  @return 富文本高度
 */
- (CGSize)yk_getSpaceLabelHeightwithSpeace:(CGFloat)lineSpeace
                              wordSpace:(CGFloat)wordSpace
                               withFont:(UIFont *)font
                              withWidth:(CGFloat)width
{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    //    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    /** 行高 */
    paraStyle.lineSpacing = lineSpeace;
    // NSKernAttributeName字体间距
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@(wordSpace)
                          };
    CGSize size = [self boundingRectWithSize:CGSizeMake(width,MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size;
}


- (CGFloat)yk_getHtmlStringHeightWithMaxWid:(CGFloat)wid
{
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[self dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    CGRect rect = [attrStr boundingRectWithSize:CGSizeMake(wid, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    return  CGRectGetHeight(rect);
}

+ (NSString *(^)(NSInteger integer))yk_integerToString
{
    return ^(NSInteger integer){
        return [NSString stringWithFormat:@"%ld",(long)integer];
    };
}

/*
 URL 字符串 里面可能包含某些字符，比如‘$‘ ‘&’ ‘？’...等，这些字符在 URL 语法中是具有特殊语法含义的，
 比如 URL ：http://www.baidu.com/s?wd=%BD%AA%C3%C8%D1%BF&rsv_bp=0&rsv_spt=3&inputT=3512
 中 的 & 起到分割作用 等等，如果 你提供的URL 本身就含有 这些字符，就需要把这些字符 转化为 “%+ASCII” 形式，以免造成冲突
 */
+ (NSString *)yk_urlEscape:(NSString *)unencodedString
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                 (CFStringRef)unencodedString,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                 kCFStringEncodingUTF8));
}

/*
 作用同上一个函数相反
 */
+ (NSString *)yk_urlUnescape:(NSString *)input
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                                 (CFStringRef)input,
                                                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                                 kCFStringEncodingUTF8));
}

//可能返回的数据中带转义字符 需要替换掉
- (NSString *)yk_clearString:(NSString *)jsonString
{
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\v" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\f" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\b" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\a" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\e" withString:@""];
    return jsonString;
}

- (NSString *)yk_urlClear
{
    NSString *tmpStr = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    tmpStr = [tmpStr stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    tmpStr = [tmpStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return tmpStr;
}

/*
 html文本
 */
+ (NSAttributedString *)yk_addAttributeWithHtml5String:(NSString *)str
{
    if (str&&[str isKindOfClass:[NSString class]]) {
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        return attrStr;
    }
    return nil;
}

+ (NSString *)clearPhonePrefixNumber:(NSString *)phone
{
    phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"+86" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    return phone;
}

+ (NSString *)yk_sessionHostForURL:(NSString *)urlStr
{
    if ([urlStr containsString:kDebugDomainName]) {
        return kReleaseDomainName;
    }
    if ([NSURL URLWithString:urlStr].host) {
        return [NSURL URLWithString:urlStr].host;
    }
    return @"";
}

- (NSDictionary *)urlParam
{
    //去除空格
    NSMutableString *mStr = [self mutableCopy];
    CFStringTrimWhitespace((CFMutableStringRef)mStr);
    
    NSArray *tempArray = [self componentsSeparatedByString:@"?"];
    if (tempArray.count ==2) {
        NSArray *tempArray2 = [tempArray[1] componentsSeparatedByString:@"&"];
        NSMutableDictionary *mutableDic = [@{} mutableCopy];
        [tempArray2 enumerateObjectsUsingBlock:^(NSString *str, NSUInteger idx, BOOL *stop) {
            NSArray *dicArray = [str componentsSeparatedByString:@"="];
            if (dicArray.count == 2) {
                mutableDic[dicArray[0]] = dicArray[1];
                DLog(@"%@",mutableDic);
            }
        }];
        return mutableDic;
    }
    return nil;
}

+ (NSString *)yk_numberToCommaSymbolformat:(NSString *)str
{
    NSString *intStr;
    NSString *floStr;
    if ([str containsString:@"."]) {
        NSRange range = [str rangeOfString:@"."];
        floStr = [str substringFromIndex:range.location];
        intStr = [str substringToIndex:range.location];
    }else{
        floStr = @"";
        intStr = str;
    }
    
    if (intStr.length <=3) {
        return [intStr stringByAppendingString:floStr];
    }else{
        NSInteger length = intStr.length;
        NSInteger count = length/3;
        NSInteger y = length%3;
        NSString *tit = [intStr substringToIndex:y] ;
        NSMutableString *det = [[intStr substringFromIndex:y] mutableCopy];
        
        for (int i =0; i < count; i ++) {
            NSInteger index = i + i *3;
            [det insertString:@","atIndex:index];
        }
        
        if (y ==0) {
            det = [[det substringFromIndex:1]mutableCopy];
        }
        
        intStr = [tit stringByAppendingString:det];
        return [intStr stringByAppendingString:floStr];
    }
}

+ (NSString *)yk_phoneNumberHiddenMiddle:(NSString*)number{
    
    NSString *numberString = [number stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    return numberString;
}

+ (BOOL)validateIdentityCard:(NSString *)identityCard {
    
    NSString *pattern = @"(^[0-9]{15}$)|([0-9]{17}([0-9]|X|x)$)";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:identityCard];
    return isMatch;
}

//进行urlEncode编码
- (NSString *)yk_URLEncodedString
{
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                                    (CFStringRef)self,
                                                                                                    (CFStringRef)@"!$&'()*+,-./:;=?@_~%#[]",
                                                                                                    NULL,
                                                                                                    kCFStringEncodingUTF8));
    return encodedString;
}

@end
