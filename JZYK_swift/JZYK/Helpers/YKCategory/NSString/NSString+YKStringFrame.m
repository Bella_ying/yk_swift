//
//  NSString+YKStringFrame.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "NSString+YKStringFrame.h"

@implementation NSString (YKStringFrame)

//size
- (CGSize)yk_sizeWithFontSize:(CGFloat)fontSize
{
    return [self sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
}

- (CGSize)yk_sizeWithSystemFont:(UIFont *)font
{
    return [self sizeWithAttributes:@{NSFontAttributeName:font}];
}

- (CGSize)yk_sizewithFontSize:(CGFloat)fontSize
                  maxWidth:(CGFloat)width
                 maxHeight:(CGFloat)height
{
    return [self yk_sizewithSystemFont:[UIFont systemFontOfSize:fontSize] maxWidth:width maxHeight:height];
}

- (CGSize)yk_sizewithSystemFont:(UIFont *)font
                    maxWidth:(CGFloat)width
                   maxHeight:(CGFloat)height
{
    CGSize size = CGSizeMake(width, height);
    if (height == 0) {
        size = CGSizeMake(size.width, MAXFLOAT);
    }
    if (width == 0) {
        size = CGSizeMake(MAXFLOAT, size.height);
    }
    
    return [self boundingRectWithSize:size
                              options:NSStringDrawingUsesLineFragmentOrigin
                           attributes:@{NSFontAttributeName:font}
                              context:nil].size;
}

//height
- (CGFloat)yk_heightWIthFontSize:(CGFloat)fontSize
{
    return [self yk_sizeWithFontSize:fontSize].height;
}

- (CGFloat)yk_heightWithSystemFont:(UIFont *)font
{
    return [self yk_sizeWithSystemFont:font].height;
}

- (CGFloat)yk_heightWIthFontSize:(CGFloat)fontSize maxWidth:(CGFloat)width
{
    return [self yk_sizewithFontSize:fontSize maxWidth:width maxHeight:0].height;
}

- (CGFloat)yk_heightWithSystemFont:(UIFont *)font maxWidth:(CGFloat)width
{
    return [self yk_sizewithSystemFont:font maxWidth:width maxHeight:0].height;
}

//width
- (CGFloat)yk_widthWithFontSize:(CGFloat)fontSize
{
    return [self yk_sizeWithFontSize:fontSize].width;
}

- (CGFloat)yk_widthWithSystemFont:(UIFont *)font
{
    return [self yk_sizeWithSystemFont:font].width;
}

@end
