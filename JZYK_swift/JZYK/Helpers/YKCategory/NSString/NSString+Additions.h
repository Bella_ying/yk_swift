//
//  NSString+Additions.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/4.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (BOOL)yk_isEmpty;

- (BOOL)yk_isValidString;

- (NSString *)yk_stringEncryptedWithMD5;

- (NSAttributedString *)yk_stringWithParagraphlineSpeace:(CGFloat)lineSpacing
                                           wordSpace :(CGFloat)wordSpace
                                            textColor:(UIColor *)textcolor
                                             textFont:(UIFont *)font;

- (CGSize)yk_getSpaceLabelHeightwithSpeace:(CGFloat)lineSpeace
                              wordSpace:(CGFloat)wordSpace
                               withFont:(UIFont *)font
                              withWidth:(CGFloat)width;

- (CGFloat)yk_getHtmlStringHeightWithMaxWid:(CGFloat)wid;

+ (id)yk_deserializeMessageJSON:(NSString *)messageJSON;

// 把传入的参数按照get的方式打包到url后面。
+ (NSString *)yk_addQueryStringToUrl:(NSString *)url
                           params:(NSDictionary *)params;

+ (NSString *(^)(NSInteger integer))yk_integerToString;

+ (NSString *)yk_urlEscape:(NSString *)unencodedString;

+ (NSString *)yk_urlUnescape:(NSString *)input;

+ (NSString *)yk_addQueryStringWithparams:(NSDictionary *)params;///--------------

/**
 把url后拼接的参数提取成NSDictionary
  @return 参数的NSDictionary
 */
- (NSDictionary *)urlParam;

/**
 可能返回的数据中带转义字符 需要替换掉

 @param jsonString 原
 @return 替换掉后的
 */
- (NSString *)yk_clearString:(NSString *)jsonString;

/**
 去除URL中的一些转义字符

 @return 转义后的字符
 */
- (NSString *)yk_urlClear;

// html文本
+ (NSAttributedString *)yk_addAttributeWithHtml5String:(NSString *)str;

/**
 去除电话号码前缀数字

 @param phone 号码
 @return 去除后的电话号
 */
+ (NSString *)clearPhonePrefixNumber:(NSString *)phone;

+ (NSString *)yk_sessionHostForURL:(NSString *)urlStr;

/**
 * 数字字符串加上逗号（,）
 */
+ (NSString *)yk_numberToCommaSymbolformat:(NSString *)num;

/**
 * 手机号码隐藏中间四位
 */
+ (NSString *)yk_phoneNumberHiddenMiddle:(NSString*)number;

/**
 *  身份证号验证(只验证数字+位数 和 最后的X)
 */
+ (BOOL)validateIdentityCard:(NSString *)identityCard;

- (NSString *)yk_URLEncodedString;

@end
