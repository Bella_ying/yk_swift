//
//  NSString+YKStringFrame.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (YKStringFrame)

/**
 *  获取字符串占得size
 *
 *  @param fontSize 字体大小（使用默认字体）
 *
 *  @return size
 */
- (CGSize)yk_sizeWithFontSize:(CGFloat)fontSize;

/**
 *  获取字符串占得size
 *
 *  @param font 字体
 *
 *  @return size
 */
- (CGSize)yk_sizeWithSystemFont:(UIFont *)font;

/**
 *  获取在固定宽度/高度的组件中占得size
 *
 *  @param fontSize 字体大小
 *  @param width    宽度
 *  @param height   高度
 *
 *  @return size
 */
- (CGSize)yk_sizewithFontSize:(CGFloat)fontSize
                  maxWidth:(CGFloat)width
                 maxHeight:(CGFloat)height;

/**
 *  获取在固定宽度/高度的组件中占得size
 *
 *  @param font     字体
 *  @param width    宽度
 *  @param height   高度
 *
 *  @return size
 */
- (CGSize)yk_sizewithSystemFont:(UIFont *)font
                    maxWidth:(CGFloat)width
                   maxHeight:(CGFloat)height;

/**
 *  获取字符串在宽度为整个屏幕宽度情况下所占高度
 *
 *  @param fontSize 字体大小
 *
 *  @return 高度
 */
- (CGFloat)yk_heightWIthFontSize:(CGFloat)fontSize;

/**
 *  获取字符串在宽度为整个屏幕宽度情况下所占高度
 *
 *  @param font     字体
 *
 *  @return 高度
 */
- (CGFloat)yk_heightWithSystemFont:(UIFont *)font;

/**
 *  获取在值定宽度情况下的高度
 *
 *  @param fontSize 字体大小
 *  @param width    制定宽度
 *
 *  @return 高度
 */
- (CGFloat)yk_heightWIthFontSize:(CGFloat)fontSize maxWidth:(CGFloat)width;

/**
 *  获取在值定宽度情况下的高度
 *
 *  @param font  字体
 *  @param width 指定宽度
 *
 *  @return 高度
 */
- (CGFloat)yk_heightWithSystemFont:(UIFont *)font maxWidth:(CGFloat)width;

/**
 *  获取字符串在不考虑换行情况下所占的最大宽度
 *
 *  @param fontSize 字体大小
 *
 *  @return 宽度
 */
- (CGFloat)yk_widthWithFontSize:(CGFloat)fontSize;

/**
 *  获取字符串在不考虑换行情况下所占的最大宽度
 *
 *  @param font 字体
 *
 *  @return 宽度
 */
- (CGFloat)yk_widthWithSystemFont:(UIFont *)font;

@end
