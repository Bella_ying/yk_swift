//
//  YKCrashHandler.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKCrashHandler.h"
#include <libkern/OSAtomic.h>
#include <execinfo.h>

NSString * const YKCrashHandlerSignalExceptionName = @"YKCrashHandlerSignalExceptionName";
NSString * const YKCrashHandlerSignalKey = @"YKCrashHandlerSignalKey";
NSString * const YKCrashHandlerAddressesKey = @"YKCrashHandlerAddressesKey";

//就像大家更熟悉的const一样，volatile是一个类型修饰符（type specifier）。它是被设计用来修饰被不同线程访问和修改的变量。
volatile int32_t UncaughtExceptionCount = 0;
const int32_t UncaughtExceptionMaximum = 10;

const NSInteger UncaughtExceptionHandlerSkipAddressCount = 4;
const NSInteger UncaughtExceptionHandlerReportAddressCount = 5;

@implementation YKCrashHandler

+ (NSArray *)backtrace
{
    void* callstack[128];
    int frames = backtrace(callstack, 128);
    char **strs = backtrace_symbols(callstack, frames);
    
    int i;
    NSMutableArray *backtrace = [NSMutableArray arrayWithCapacity:frames];
    for (
         i = UncaughtExceptionHandlerSkipAddressCount;
         i < UncaughtExceptionHandlerSkipAddressCount +
         UncaughtExceptionHandlerReportAddressCount;
         i++){
        [backtrace addObject:[NSString stringWithUTF8String:strs[i]]];
    }
    free(strs);
    
    return backtrace;
}

- (void)alertView:(UIAlertView *)anAlertView clickedButtonAtIndex:(NSInteger)anIndex
{
    dismissed = YES;
}

//处理
- (void)handleException:(NSException *)exception
{
    NSString *message =@"因为未知的事件发生，应用即将关闭";
    //    @"An unexpected event happened causing the application to shutdown.";
    NSString *title = @"抱歉";
    NSString *buttonTittle = @"好的";
    UIAlertView *thisAlert = [[UIAlertView alloc] initWithTitle:title  message:message delegate:nil cancelButtonTitle:buttonTittle otherButtonTitles:nil, nil];
    [thisAlert show];
    
    CFRunLoopRef runLoop = CFRunLoopGetCurrent();
    CFArrayRef allModes = CFRunLoopCopyAllModes(runLoop);
    
    while (!dismissed){
        for (NSString *mode in (NSArray *)CFBridgingRelease(allModes)){
            CFRunLoopRunInMode((CFStringRef)CFBridgingRetain(mode), 0.001, false);
        }
    }
    
    CFRelease(allModes);
    
    NSSetUncaughtExceptionHandler(NULL);
    signal(SIGABRT, SIG_DFL);
    signal(SIGILL, SIG_DFL);
    signal(SIGSEGV, SIG_DFL);
    signal(SIGFPE, SIG_DFL);
    signal(SIGBUS, SIG_DFL);
    signal(SIGPIPE, SIG_DFL);
    
    if ([[exception name] isEqualToString:YKCrashHandlerSignalExceptionName]){
        kill(getpid(), [[[exception userInfo] objectForKey:YKCrashHandlerSignalKey] intValue]);
    }else{
        [exception raise];
    }
}

@end

void HandleException(NSException *exception)
{
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    if (exceptionCount > UncaughtExceptionMaximum){
        return;
    }
    
    NSArray *callStack = [YKCrashHandler backtrace];
    NSMutableDictionary *userInfo = [[exception userInfo] mutableCopy];
    [userInfo setObject:callStack forKey:YKCrashHandlerAddressesKey];
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        NSException *excep = [NSException exceptionWithName:[exception name] reason:[exception reason] userInfo:userInfo];
        [[[YKCrashHandler alloc] init] handleException:excep];
    });
}

void SignalHandler(int signal)
{
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    
    if (exceptionCount > UncaughtExceptionMaximum){
        return;
    }
    
    NSMutableDictionary *userInfo = [@{YKCrashHandlerSignalKey :@(signal)} mutableCopy];
    
    NSArray *callStack = [YKCrashHandler backtrace];
    [userInfo setObject:callStack forKey:YKCrashHandlerAddressesKey];
    dispatch_sync(dispatch_get_main_queue(), ^{
        NSException *excep = [NSException exceptionWithName:YKCrashHandlerSignalExceptionName reason:[NSString stringWithFormat:@"Signal %d was raised.", signal] userInfo:@{YKCrashHandlerSignalKey:@(signal)}];
        [[[YKCrashHandler alloc] init] handleException:excep];
    });
}

/*
 这个类为每一个可用的信号都安装了NSSetUncaughtExceptionHandler处理程序。如果出现异常，会调用这个处理程序并访问当前RunLoop下的全部模式。进程会被杀掉，并且backtrace(堆栈的回溯信息)被读入到一个NSDictionary类的对象中，并且传递给handleException方法，这个方法呈现了一个AlertView控件。
 */
void InstallCrashExceptionHandler()
{
    NSSetUncaughtExceptionHandler(&HandleException);
    signal(SIGABRT, SignalHandler);
    signal(SIGILL, SignalHandler);
    signal(SIGSEGV, SignalHandler);
    signal(SIGFPE, SignalHandler);
    signal(SIGBUS, SignalHandler);
    signal(SIGPIPE, SignalHandler);
}
