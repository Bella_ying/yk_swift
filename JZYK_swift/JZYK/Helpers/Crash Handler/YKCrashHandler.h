//
//  YKCrashHandler.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKCrashHandler : NSObject
{
    BOOL dismissed;
}

void InstallCrashExceptionHandler(void);

@end
