//
//  YKTools.h
//  JZYK
//
//  Created by hongyu on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, YKPermissionStatus) {
    YKPermissionStatusAuthorized    = 0, // 授权
    YKPermissionStatusNotDetermined = 1, // 未曾访问过
    YKPermissionStatusNotDenied     = 2  // 拒绝
};

@interface YKTools : NSObject

/**
 字符串大小比较
 
 @param firstString  第一个字符串
 @param sencondSting 第二个字符串
 @return 比较结果
 */
+ (NSString *)yk_compareTheSizeWithFirstStr:(NSString *)firstString
                               secondStr:(NSString *)sencondSting;

+ (CGSize)yk_sizeWithLabel:(UILabel *)label;

/**
 推送权限
 @return YKPermissionStatus 权限状态
 */
+ (YKPermissionStatus)yk_permissionsWithNotifications;

/**
 定位权限
 @return YKPermissionStatus 权限状态
 */
+ (YKPermissionStatus)yk_permissionsWithLocation;

/**
 通讯录权限
 @return YKPermissionStatus 权限状态
 */
+ (YKPermissionStatus)yk_permissionsWithAddressBook;

/**
 日历权限
 @return YKPermissionStatus 权限状态
 */
+ (YKPermissionStatus)yk_permissionsWithCalendar;

/**
 相机权限
 @return YKPermissionStatus 权限状态
 */
+ (YKPermissionStatus)yk_permissionsWithCamera;

/**
 相册权限
 @return YKPermissionStatus 权限状态
 */
+ (YKPermissionStatus)yk_permissionsWithPhoto;


// 配置阴影
+ (void)yk_prepareShadowWithView:(UIView *)view
                     shadowColor:(UIColor *)color
                    shadowOffset:(CGSize)offset
                   shadowOpacity:(CGFloat)opacity
                    shadowRadius:(CGFloat)radius;

// 配置圆角
+ (void)yk_prepareRadiusWithView:(UIView *)view
                   masksToBounds:(BOOL)bounds
                    cornerRadius:(CGFloat)radius
                     borderWidth:(CGFloat)width
                     borderColor:(UIColor *)color;

// 储存图片
+ (void)yk_saveImageToLocalWithUrl:(NSString *)url
                               key:(NSString *)key;

// 本地是否有图片
+ (BOOL)yk_isLocalHaveImageKey:(NSString *)key;

// 获取图片
+ (UIImage *)yk_getImageFromLocalWithKey:(NSString *)key;

// 本地数据写入
+ (void)yk_writeToDocumentPathWithData:(id)data fileName:(NSString *)name;

// 本地数据读取
+ (id)yk_getDataToDocumentPathWithFileName:(NSString *)name;

/**
 跳转开启权限
 */
+ (void)yk_jumpPermissions;

/**
 获取全局UA

 @return 全局UA字符串
 */
+ (NSString *)yk_getGlobalUserAgent;

+ (void)yk_deleteTheWKWebviewCache:(BOOL)allDelete;

/**
 加密的手机号码

 @param str 加密前的手机字符串
 @return 加密后的字符串
 */
+ (NSString *)yk_phoneSecurityString:(NSString *)str;


/**
 返回size

 @param size 原来的size
 @param labelText 内容
 @param font 文字
 @return 返回之后的size
 */
+(CGSize)yk_labelRectWithSize:(CGSize)size
                 LabelText:(NSString *)labelText
                      font:(UIFont *)font;
@end
