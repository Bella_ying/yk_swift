//
//  YKTools.m
//  JZYK
//
//  Created by hongyu on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKTools.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <Contacts/CNContact.h>
#import <Contacts/CNContactStore.h>
#import <EventKit/EventKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import "YKAppStarter.h"

static const CGFloat space = 5;

@implementation YKTools

+ (NSString *)yk_getGlobalUserAgent
{
    NSString *userAgent = @"";
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserAgentResult]) {
        userAgent = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAgentResult];
    }else{
        userAgent = [YKAppStarter yk_generateUserAgent];
    }
    return userAgent;
}

+ (NSString *)yk_compareTheSizeWithFirstStr:(NSString *)firstString
                               secondStr:(NSString *)sencondString
{
    if ([firstString compare:sencondString] == NSOrderedAscending) {
        return @"big";
    } else if ([firstString compare:sencondString] == NSOrderedDescending) {
        return @"small";
    } else if ([firstString compare:sencondString] == NSOrderedSame) {
        return @"same";
    }
    return nil;
}

+ (CGSize)yk_sizeWithLabel:(UILabel *)label
{
    CGSize size = [label.text sizeWithAttributes:@{NSFontAttributeName : label.font}];
    size.width += space;
    return size;
}

#pragma mark- 推送权限  authorized 授权 denied 拒绝 notDetermined 未曾访问过 restricted 未被授权
+ (YKPermissionStatus)yk_permissionsWithNotifications
{
    return [[UIApplication sharedApplication] currentUserNotificationSettings].types == UIUserNotificationTypeNone ? YKPermissionStatusNotDenied : YKPermissionStatusAuthorized;
}

#pragma mark- 定位权限
+ (YKPermissionStatus)yk_permissionsWithLocation
{
    CLAuthorizationStatus CLstatus = [CLLocationManager authorizationStatus];
    if (CLstatus == kCLAuthorizationStatusAuthorizedAlways || CLstatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
        return YKPermissionStatusAuthorized;
    } else if (CLstatus == kCLAuthorizationStatusNotDetermined) {
        CLLocationManager *manager = [CLLocationManager new];
        [manager requestWhenInUseAuthorization];
        return YKPermissionStatusNotDetermined;
    } else {
        return YKPermissionStatusNotDenied;
    }
}

#pragma mark- 通讯录权限
+ (YKPermissionStatus)yk_permissionsWithAddressBook
{
    if (iOS9) {
        CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
        if (status == CNAuthorizationStatusAuthorized) {
            return YKPermissionStatusAuthorized;
        } else if (status == CNAuthorizationStatusNotDetermined) {
            CNContactStore *contactStore = [CNContactStore new];
            [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
            return YKPermissionStatusNotDetermined;
        } else {
            return YKPermissionStatusNotDenied;
        }
    } else {
        ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
        if (status == kABAuthorizationStatusAuthorized) {
            return YKPermissionStatusAuthorized;
        } else if (status == kABAuthorizationStatusNotDetermined) {
            ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            });
            return YKPermissionStatusNotDetermined;
        } else {
            return YKPermissionStatusNotDenied;
        }
    }
}

#pragma mark- 日历权限
+ (YKPermissionStatus)yk_permissionsWithCalendar
{
    EKAuthorizationStatus EKstatus = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    if (EKstatus == EKAuthorizationStatusAuthorized) {
        return YKPermissionStatusAuthorized;
    } else if (EKstatus == EKAuthorizationStatusNotDetermined) {
        EKEventStore *store = [EKEventStore new];
        [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError * _Nullable error) {
        }];
        return YKPermissionStatusNotDetermined;
    } else {
        return YKPermissionStatusNotDenied;
    }
}

#pragma mark- 相机权限
+ (YKPermissionStatus)yk_permissionsWithCamera
{
    AVAuthorizationStatus AVstatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (AVstatus == AVAuthorizationStatusAuthorized) {
        return YKPermissionStatusAuthorized;
    } else if (AVstatus == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        }];
        return YKPermissionStatusNotDetermined;
    } else {
        return YKPermissionStatusNotDenied;
    }
}

#pragma mark- 相册权限
+ (YKPermissionStatus)yk_permissionsWithPhoto
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized) {
        return YKPermissionStatusAuthorized;
    } else if (status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        }];
        return YKPermissionStatusNotDetermined;
    } else {
        return YKPermissionStatusNotDenied;
    }
}


#pragma mark- 配置阴影
+ (void)yk_prepareShadowWithView:(UIView *)view
                     shadowColor:(UIColor *)color
                    shadowOffset:(CGSize)offset
                   shadowOpacity:(CGFloat)opacity
                    shadowRadius:(CGFloat)radius
{
    view.layer.shadowColor   = [color CGColor];
    view.layer.shadowOpacity = opacity;
    view.layer.shadowRadius  = radius;
    view.layer.shadowOffset  = offset;
}

#pragma mark- 配置圆角
+ (void)yk_prepareRadiusWithView:(UIView *)view
                   masksToBounds:(BOOL)bounds
                    cornerRadius:(CGFloat)radius
                     borderWidth:(CGFloat)width
                     borderColor:(UIColor *)color
{
    view.layer.masksToBounds = bounds;
    view.layer.cornerRadius  = radius;
    view.layer.borderWidth   = width;
    view.layer.borderColor   = [color CGColor];
}

#pragma mark- 储存图片
+ (void)yk_saveImageToLocalWithUrl:(NSString *)url
                               key:(NSString *)key
{
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    UIImage *image    =  [UIImage imageWithData:imageData];
    [YK_NSUSER_DEFAULT setObject:UIImagePNGRepresentation(image) forKey:key];
}

#pragma mark- 本地是否有图片
+ (BOOL)yk_isLocalHaveImageKey:(NSString *)key
{
    NSData *imageData = [YK_NSUSER_DEFAULT objectForKey:key];
    return imageData ? YES : NO;
}

#pragma mark- 获取图片
+ (UIImage *)yk_getImageFromLocalWithKey:(NSString *)key
{
    NSData *imageData = [YK_NSUSER_DEFAULT objectForKey:key];
    UIImage *image;
    if (imageData) {
        image = [UIImage imageWithData:imageData];
    }
    return image;
}

#pragma mark- 本地数据存储
+ (void)yk_writeToDocumentPathWithData:(id)data fileName:(NSString *)name
{
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [pathArray lastObject];
    DLog(@"%@", documentPath);
    NSString *arrPath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", name]];
    BOOL result = [data writeToFile:arrPath atomically:YES];
    if (result == YES) {
        NSLog(@"=======数据写入成功");
    }
    else {
        NSLog(@"=======数据写入失败");
    }
}

+ (id)yk_getDataToDocumentPathWithFileName:(NSString *)name
{
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [pathArray lastObject];
    NSString *arrPath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", name]];
    NSArray *array = [NSArray arrayWithContentsOfFile:arrPath];
    return array;
}

+ (void)yk_jumpPermissions
{
    NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    if([[UIApplication sharedApplication] openURL:url]) {
        NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}

+ (void)yk_deleteTheWKWebviewCache:(BOOL)allDelete
{
    //因为在分期商城刷新时，不需要清除cookies和session；但是在退出登录时需要全部清除
    if(@available(iOS 9.0, *)) {
        NSArray * types;
        if (allDelete) {
            types = @[WKWebsiteDataTypeCookies,
                      WKWebsiteDataTypeSessionStorage,
                      WKWebsiteDataTypeDiskCache,
                      WKWebsiteDataTypeMemoryCache];
        }else{
            types = @[WKWebsiteDataTypeDiskCache,
                      WKWebsiteDataTypeMemoryCache];
        }
        
        NSSet * websiteDataTypes = [NSSet setWithArray:types];
        [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:[NSDate dateWithTimeIntervalSince1970:0] completionHandler:^{
        }];
    }else{
        NSString * libPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask, YES) firstObject];
        libPath = [libPath stringByAppendingString:@"/Cookies"];
        NSError * errors;
        [[NSFileManager defaultManager] removeItemAtPath:libPath error:&errors];
    }
}

+ (NSString *)yk_phoneSecurityString:(NSString *)str
{
    if (str.length != 11) {
        return str;
    }
    return [NSString stringWithFormat:@"%@****%@",[str substringToIndex:3],[str substringFromIndex:7]];
}

+(CGSize)yk_labelRectWithSize:(CGSize)size
                    LabelText:(NSString *)labelText
                         font:(UIFont *)font
{

    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    CGSize actualsize = [labelText boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    actualsize.width  += 20;
//    actualsize.height += 10;
    return actualsize;
}
@end
