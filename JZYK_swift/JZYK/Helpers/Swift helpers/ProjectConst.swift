//
//  ProjectConst.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

/************************************  UI相关  ************************************/
//颜色相关
let BUTTONGRAY = "#d9d9d9";
let YGBGREY = "#eff3f5";
let YGBPickerGREY = "#f4f4f6";
let projectShortName = "jzyk"

/************************************  项目全局相关  ************************************/
let kAppLocalVersion = "app_local_version";
let kFirstInApp = "firstInApp";
let kDebugDomainName = ".jisuqianbao.com";
let kReleaseDomainName = "jzyk.yuyaowangluo.com";
let kReleaseURL = "https://jzyk.yuyaowangluo.com/credit/web/credit-app/config";
let kOfficialWebsite = "http://www.yuyaowangluo.com/"; //官网链接
let kSessionID = "SESSIONID";
let kUserAgentResult = "uaResult";
let kPullRefreshUIDefaultTitle = "信用让生活更美好";


