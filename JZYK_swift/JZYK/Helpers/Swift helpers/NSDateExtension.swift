//
//  NSDateExtension.swift
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/15.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import Foundation

extension Date {
    public static func currentTimeConvertToString() -> String? {
        let currentTime = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yy-MM-dd HH:mm:ss"
        return formatter.string(from: currentTime)
    }
}
