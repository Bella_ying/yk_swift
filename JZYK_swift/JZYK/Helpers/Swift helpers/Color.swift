//
//  Color.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Hue

class Color: UIColor {
    //计算属性不在内存中存储值，减少内存开销，同时也方便控制取值权限（存储属性也可以控制权限）
   @objc public static var main: UIColor {
        get {
            return UIColor(hex: QBProject.yk_current().mainThemeColor)
        }
    }

    @objc public static var mainButtonDisabledColor: UIColor {
        get {
            return main.withAlphaComponent(0.5)
        }
    }
    /*
     c7-用于页面背景底色
     */
    @objc public static var backgroundColor: UIColor {
        get {
            return UIColor(hex: "#F5F5F7")
        }
    }
    /*
     用于重要级文字信息、内页标题等主要文字;
     如导航名称、大板块标题、输入后文字
     */
    @objc public static var color_45_C2: UIColor {
        get {
            return UIColor(hex: "#454545")
        }
    }
    /*
     用于重要级文字信息、内页标题信息;
     如借款详情已完成的状态、列表正文色等
     */
    @objc public static var color_66_C3: UIColor {
        get {
            return UIColor(hex: "#666666")
        }
    }
    /*
     辅助色、用于部分文字强调等;
     如button下面提示文字、借款记录时间等
     */
    @objc public static var color_8D_C4: UIColor {
        get {
            return UIColor(hex: "#8D8D8D")
        }
    }
    /*
     提示性、待输入状态、时间等文字;
     如借款详情页还款状态、注册登录填写资料未输入信息等
     */
    @objc public static var color_AD_C5: UIColor {
        get {
            return UIColor(hex: "#ADADAD")
        }
    }
    
    /// 用于分割线、标签描边
    @objc public static var color_E6_C6: UIColor {
        get {
            return UIColor(hex: "#E6E6E6")
        }
    }
    /*
     用于分割线、线框颜色
     */
    @objc public static var color_CC_C8: UIColor {
        get {
            return UIColor(hex: "#E6E6E6")
        }
    }
    /*
     用于关闭按钮和箭头颜色
     */
    @objc public static var color_DC_C9: UIColor {
        get {
            return UIColor(hex: "#DCDCDC")
        }
    }
    
    /*
     用于文本颜色
     */
    @objc public static var color_33_C8: UIColor {
        get {
            return UIColor(hex: "#333333")
        }
    }
    
    /*
     用于公告的背景色 -- 浅橙
     */
    @objc public static var anoucementBgColor: UIColor {
        get {
            return UIColor(hex: "#FF9552")
        }
    }
    
    /*
     用于滑竿miniTrak -- 浅橙
     */
    @objc public static var color_slideMiniTrack: UIColor {
        get {
            return UIColor(hex: "#FF9F44")
        }
    }
    
    /*
     用于边框颜色
     */
    @objc public static var color_E8_C6: UIColor {
        get {
            return UIColor(hex: "#E8E8E8")
        }
    }
    
    /*
     用于分期背景色
     */
    @objc public static var color__C6: UIColor {
        get {
            return UIColor(hex: "#E8E8E8")
        }
    }
    
    /*
     用于贷款用途选择项颜色 -- 类型主色的橙色
     */
    @objc public static var color_loanPorpose: UIColor {
        get {
            return UIColor(hex: "#FF8132")
        }
    }
    
    /*
     用于错误提示代码-红色
     */
    @objc public static var color_error: UIColor {
        get {
            return UIColor(hex: "#E96763")
        }
    }
    /*
     用于忘记密码 - 黄色
     */
    @objc public static var color_yellow: UIColor {
        get {
            return UIColor(hex: "#FFAC4C")
        }
    }
    
    @objc public static func colorHex(_ hex: String) -> UIColor {
        return UIColor(hex: hex)
    }
}
