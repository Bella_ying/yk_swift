//
//  Debugger.swift
//  JZYK
//
//  Created by Jeremy on 2018/7/23.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class YKDebugger: NSObject {
    
   /// 是否debug模式，默认是true
    /*
     e.g.
     if YKDebugger.isDebug {
        print("is Debug");
     } else {
        print("is release");
     }
     */
    static public var isDebug: Bool {
        #if DEBUG // 判断是否在测试环境下
        return true;
        #else
        return false;
        #endif
    }
    
    /// 根据条件输出log
    ///
    /// - Parameters:
    ///   - debuglog: DEBUG环境
    ///   - releaseLog: RELEASE环境
    public static func debugPrint(_ debuglog:@escaping()->Void, _ releaseLog:@escaping()->Void) {
        #if DEBUG // 判断是否在测试环境下
        debuglog()
        #else
        releaseLog()
        #endif
    }
    
    
    /// debuglog
    ///
    /// - Parameter debuglog: DEBUG环境
    public static func debugLog(_ debuglog:@escaping()->Void) {
        #if DEBUG // 判断是否在测试环境下
        debuglog()
        #else
        #endif
    }
    
    
    /// releaseLog
    ///
    /// - Parameter releaseLog: RELEASE环境
    public static func releaseLog( _ releaseLog:@escaping()->Void) {
        #if DEBUG // 判断是否在测试环境下
        #else
        releaseLog()
        #endif
    }
}
