//
//  GlobalConst.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Device

//MARK: 屏幕设置系列
let WIDTH_OF_SCREEN  = UIScreen.main.bounds.size.width;
let HEIGHT_OF_SCREEN = UIScreen.main.bounds.size.height;
let BOUNDS_OF_SCREEN = UIScreen.main.bounds;

let iPhoneX_HEIGHT_RATIO = (HEIGHT_OF_SCREEN == 812.0 ? 667.0/667.0 : HEIGHT_OF_SCREEN/667.0);
let NAV_HEIGHT           = (HEIGHT_OF_SCREEN == 812.0 ? 88 : 64);
let BOTTOM_HEIGHT        = (HEIGHT_OF_SCREEN == 812.0 ? 34 : 0);
let TABBAR_HEIGHT        = (HEIGHT_OF_SCREEN == 812.0 ? 83 : 49);
let STATUSBAR_HEIGHT     = (HEIGHT_OF_SCREEN == 812.0  ? 44.0 : 20.0);


/** 高度667(6s)为基准比例！！！做到不同屏幕适配高度*/
let ASPECT_RATIO_HEIGHT =  HEIGHT_OF_SCREEN/667.0;
/** 宽度375.0f(6s)为基准比例！！！做到不同屏幕适配宽度  */
let ASPECT_RATIO_WIDTH =  WIDTH_OF_SCREEN/375.0;


let left_scale = x_scale(x:12);

let USER_DEFAULT = UserDefaults.standard

/** 适配屏幕宽度比例*f  */
public func x_scale(x:CGFloat) -> CGFloat {
    return ASPECT_RATIO_WIDTH * x;
}

/** 适配屏幕高度比例*f  */
public func y_scale(y:CGFloat) -> CGFloat {
    return ASPECT_RATIO_HEIGHT * y;
}

public func font_scale(font:CGFloat) -> UIFont {
    return UIFont.systemFont(ofSize: x_scale(x: font));
}

public func isIphoneX() -> Bool {
    if Device.size() == .screen5_8Inch {
        return true
    }
    return false
}

public func isIphone8() -> Bool {
    if Device.size() == .screen4_7Inch {
        return true
    }
    return false
}

public func isIphone8P() -> Bool {
    if Device.size() == .screen5_5Inch {
        return true
    }
    return false
}

public func isIphoneSE() -> Bool {
    if Device.size() == .screen4Inch {
        return true
    }
    return false
}

