//
//  YKGzipUtility.h
//  KDFDApp
//
//  Created by Jeremy Wang on 28/02/2018.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKGzipUtility : NSObject

/**
 gzip数据压缩

 @param pUncompressedData 准备压缩的数据
 @return gzip压缩后的数据
 */
+ (NSData *)gzipData:(NSData*)pUncompressedData;

/**
 gzip解压缩

 @param compressedData gzip压缩后的数据
 @return 解压后的数据
 */
+ (NSData *)ungzipData:(NSData *)compressedData;  //解压缩

@end
