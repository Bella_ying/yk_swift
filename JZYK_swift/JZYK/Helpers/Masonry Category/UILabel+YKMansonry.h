//
//  UILabel+YKMansonry.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (YKMansonry)

+ (UILabel *)yk_labelWithFontSize:(NSInteger)size
                     textColor:(UIColor *)color
                     superView:(UIView *)superView
                    masonrySet:(void (^)(UILabel *label, MASConstraintMaker *make))block;

+ (UILabel *)yk_labelWithFontSize:(NSInteger)size
                  textColorHex:(NSString *)colorHex
                     superView:(UIView *)superView
                    masonrySet:(void (^)(UILabel *label, MASConstraintMaker *make))block;

@end
