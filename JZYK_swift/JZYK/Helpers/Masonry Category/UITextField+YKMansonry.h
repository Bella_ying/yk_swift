//
//  UITextField+YKMansonry.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (YKMansonry)

+ (UITextField *)yk_textFieldWithFontSize:(NSInteger)size
                          textColorHex:(NSString *)colorHex
                           placeHolder:(NSString *)placeHolder
                             superView:(UIView *)superView
                            masonrySet:(void (^)(UITextField *textfield, MASConstraintMaker *make))block;

+ (UITextField *)yk_textFieldWithFontSize:(NSInteger)size
                             textColor:(UIColor *)color
                           placeHolder:(NSString *)placeHolder
                             superView:(UIView *)superView
                            masonrySet:(void (^)(UITextField *textfield, MASConstraintMaker *make))block;

@end
