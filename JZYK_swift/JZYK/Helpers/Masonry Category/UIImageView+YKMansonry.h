//
//  UIImageView+YKMansonry.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//



@interface UIImageView (YKMansonry)

+ (UIImageView *)yk_imageViewWithImageName:(NSString *)imageName
                              superView:(UIView *)superView
                             masonrySet:(void (^)(UIImageView *imageView, MASConstraintMaker *make))block;

+ (UIImageView *)yk_imageViewWithImageName:(NSString *)imageName
                                 superView:(UIView *)superView
                              cornerRadius:(CGFloat)radius
                                masonrySet:(void (^)(UIImageView *imageView, MASConstraintMaker *make))block;

@end
