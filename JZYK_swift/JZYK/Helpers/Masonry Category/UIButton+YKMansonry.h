//
//  UIButton+YKMansonry.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/4.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (YKMansonry)

+ (UIButton *)yk_buttonFontSize:(NSInteger)size
                colorHexText:(NSString *)colorHexText
         backGroundColorText:(NSString *)bgColorHexText
                   superView:(UIView *)superView
                  masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block;

+ (UIButton *)yk_buttonWithFontSize:(NSInteger)size
                    colorHexText:(NSString *)colorHex
                 backGroundColor:(UIColor *)color
                       superView:(UIView *)superView
                      masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block;

+ (UIButton *)yk_buttonWithFontSize:(NSInteger)size
                    colorHexText:(NSString *)colorHex
                 backGroundColor:(UIColor *)color
                    cornerRadius:(CGFloat)Radius
                       superView:(UIView *)superView
                      masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block;

+ (UIButton *)yk_buttonFontSize:(NSInteger)size
                   textColor:(UIColor *)textcolor
             backGroundColor:(UIColor *)bgColor
                   imageName:(NSString *)imageName
                   superView:(UIView *)superView
                  masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block;

+ (UIButton *)yk_buttonWithImageName:(NSString *)imageName
                        superView:(UIView *)superView
                       masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block;

///较全的属性:包含backGroundImage
+ (UIButton *)yk_buttonFontSize:(NSInteger)size
                      textColor:(UIColor *)textcolor
                backGroundImage:(NSString *)bgColor
                      imageName:(NSString *)imageName
                   cornerRadius:(CGFloat)Radius
                      superView:(UIView *)superView
                     masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block;

@end
