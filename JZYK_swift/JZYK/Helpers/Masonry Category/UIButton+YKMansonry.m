//
//  UIButton+YKMansonry.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/4.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//


#import "UIButton+YKMansonry.h"

@implementation UIButton (YKMansonry)

+ (UIButton *)yk_buttonFontSize:(NSInteger)size
                colorHexText:(NSString *)colorHexText
         backGroundColorText:(NSString *)bgColorHexText
                   superView:(UIView *)superView
                  masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block
{
    return [UIButton yk_buttonWithFontSize:size
                           colorHexText:colorHexText
                        backGroundColor:[QBColor yk_colorWithHex:bgColorHexText]
                              superView:superView
                             masonrySet:block];
}

+ (UIButton *)yk_buttonWithFontSize:(NSInteger)size
                   colorHexText:(NSString *)colorHex
                backGroundColor:(UIColor *)color
                      superView:(UIView *)superView
                     masonrySet:(void (^)(UIButton *button,MASConstraintMaker *make))block
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:size];
    [btn setTitleColor:[QBColor yk_colorWithHex:colorHex] forState:UIControlStateNormal];
    [btn setBackgroundColor:color];
    
    [superView addSubview:btn];
    btn.translatesAutoresizingMaskIntoConstraints = NO;
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(btn,make);
        }
    }];
    
    return btn;
}

+ (UIButton *)yk_buttonWithFontSize:(NSInteger)size
                   colorHexText:(NSString *)colorHex
                backGroundColor:(UIColor *)color
                   cornerRadius:(CGFloat)Radius
                      superView:(UIView *)superView
                     masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block;
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:size];
    [btn setTitleColor:[QBColor yk_colorWithHex:colorHex] forState:UIControlStateNormal];
    [btn setBackgroundColor:color];
    btn.layer.cornerRadius = Radius;
    [superView addSubview:btn];
    btn.translatesAutoresizingMaskIntoConstraints = NO;
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(btn,make);
        }
    }];
    
    return btn;
}

+ (UIButton *)yk_buttonFontSize:(NSInteger)size
                   textColor:(UIColor *)textcolor
             backGroundColor:(UIColor *)bgColor
                   imageName:(NSString *)imageName
                   superView:(UIView *)superView
                  masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:size];
    [btn setTitleColor:textcolor forState:UIControlStateNormal];
    [btn setBackgroundColor:bgColor];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [superView addSubview:btn];
    btn.translatesAutoresizingMaskIntoConstraints = NO;
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(btn,make);
        }
    }];
    
    return btn;
}

+ (UIButton *)yk_buttonWithImageName:(NSString *)imageName
                       superView:(UIView *)superView
                      masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [superView addSubview:btn];
    btn.translatesAutoresizingMaskIntoConstraints = NO;
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(btn,make);
        }
    }];
    
    return btn;
}

+ (UIButton *)yk_buttonFontSize:(NSInteger)size
                      textColor:(UIColor *)textcolor
                backGroundImage:(NSString *)bgImageName
                      imageName:(NSString *)imageName
                   cornerRadius:(CGFloat)Radius
                      superView:(UIView *)superView
                     masonrySet:(void (^)(UIButton *button, MASConstraintMaker *make))block
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    if (bgImageName.length > 0) {
        [btn setBackgroundImage:[UIImage imageNamed:bgImageName] forState:UIControlStateNormal];
    }
    if (imageName.length > 0) {
        [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    btn.titleLabel.font = [UIFont systemFontOfSize:size];
    [btn setTitleColor:textcolor forState:UIControlStateNormal];
    btn.layer.cornerRadius = Radius;
    btn.layer.masksToBounds = YES;
    [superView addSubview:btn];
    btn.translatesAutoresizingMaskIntoConstraints = NO;
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(btn,make);
        }
    }];
    
    return btn;
}

@end
