//
//  UITextField+YKMansonry.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "UITextField+YKMansonry.h"

@implementation UITextField (YKMansonry)

+ (UITextField *)yk_textFieldWithFontSize:(NSInteger)size
                             textColorHex:(NSString *)colorHex
                              placeHolder:(NSString *)placeHolder
                                superView:(UIView *)superView
                               masonrySet:(void (^)(UITextField *textfield, MASConstraintMaker *make))block
{
    UITextField *textfield = [UITextField new];
    textfield.font = [UIFont systemFontOfSize:size];
    textfield.textColor = [Color colorHex:colorHex];
    textfield.placeholder = placeHolder;
    textfield.translatesAutoresizingMaskIntoConstraints = NO;
    [superView addSubview:textfield];
    
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(textfield,make);
        }
    }];
    
    return textfield;
}

+ (UITextField *)yk_textFieldWithFontSize:(NSInteger)size
                             textColor:(UIColor *)color
                           placeHolder:(NSString *)placeHolder
                             superView:(UIView *)superView
                            masonrySet:(void (^)(UITextField *textfield, MASConstraintMaker *make))block
{
    UITextField *textfield = [UITextField new];
    textfield.font = [UIFont systemFontOfSize:size];
    textfield.textColor = color;
    textfield.placeholder = placeHolder;
    textfield.translatesAutoresizingMaskIntoConstraints = NO;
    [superView addSubview:textfield];
    
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(textfield,make);
        }
    }];
    
    return textfield;
}

@end
