//
//  UIView+YKMansonry.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "UIView+YKMansonry.h"

@implementation UIView (YKMansonry)

+ (UIView *)yk_viewWithColor:(UIColor *)color
                superView:(UIView *)superView
               masonrySet:(void (^)(UIView *view, MASConstraintMaker *make))block
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = color;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [superView addSubview:view];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(view,make);
        }
    }];
    return view;
}

+ (UIView *)yk_viewWithColorHexText:(NSString *)colorHex
                       superView:(UIView *)superView
                      masonrySet:(void (^)(UIView *view,MASConstraintMaker *make))block
{
    return [UIView yk_viewWithColor:[Color colorHex:colorHex]
                       superView:superView
                      masonrySet:block];
}

+ (UIView *)yk_viewWithColorHexText:(NSString *)colorHexText
                       superView:(UIView *)superView
                    cornerRadius:(CGFloat)radius
                      masonrySet:(void (^)(UIView *view,MASConstraintMaker *make))block
{
    UIView *view = [UIView new];
    view.backgroundColor = [Color colorHex:colorHexText];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view.layer.cornerRadius = radius;
    [superView addSubview:view];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(view,make);
        }
    }];
    return view;
}

@end
