//
//  UIView+YKMansonry.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (YKMansonry)

+ (UIView *)yk_viewWithColor:(UIColor *)color
                superView:(UIView *)superView
               masonrySet:(void (^)(UIView *view, MASConstraintMaker *make))block;

+ (UIView *)yk_viewWithColorHexText:(NSString *)colorHex
                       superView:(UIView *)superView
                      masonrySet:(void (^)(UIView *view,MASConstraintMaker *make))block;

+ (UIView *)yk_viewWithColorHexText:(NSString *)colorHexText
                       superView:(UIView *)superView
                    cornerRadius:(CGFloat)radius
                      masonrySet:(void (^)(UIView *view,MASConstraintMaker *make))block;

@end
