//
//  UILabel+YKMansonry.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "UILabel+YKMansonry.h"

@implementation UILabel (YKMansonry)

+ (UILabel *)yk_labelWithFontSize:(NSInteger)size
                     textColor:(UIColor *)color
                     superView:(UIView *)superView
                    masonrySet:(void (^)(UILabel *label, MASConstraintMaker *make))block
{
    UILabel *label = [UILabel new];
    label.font = [UIFont systemFontOfSize:size];
    label.textColor = color;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    [superView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(label,make);
        }
    }];
    return label;
}

+ (UILabel *)yk_labelWithFontSize:(NSInteger)size
                  textColorHex:(NSString *)colorHex
                     superView:(UIView *)superView
                    masonrySet:(void (^)(UILabel *label, MASConstraintMaker *make))block
{
    return [UILabel yk_labelWithFontSize:size
                            textColor:[Color colorHex:colorHex]
                            superView:superView
                           masonrySet:block];
}

@end
