//
//  UIImageView+YKMansonry.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/5.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "UIImageView+YKMansonry.h"

@implementation UIImageView (YKMansonry)

+ (UIImageView *)yk_imageViewWithImageName:(NSString *)imageName
                              superView:(UIView *)superView
                             masonrySet:(void (^)(UIImageView *imageView, MASConstraintMaker *make))block
{
    UIImageView *imageView = nil;
    if (!imageName || [imageName isEqualToString:@""]) {
        imageView = [UIImageView new];
    } else {
        imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    }
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [superView addSubview:imageView];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(imageView,make);
        }
    }];
    
    return imageView;
}

+ (UIImageView *)yk_imageViewWithImageName:(NSString *)imageName
                                 superView:(UIView *)superView
                              cornerRadius:(CGFloat)radius
                                masonrySet:(void (^)(UIImageView *imageView, MASConstraintMaker *make))block
{
    UIImageView *imageView = nil;
    if (!imageName || [imageName isEqualToString:@""]) {
        imageView = [UIImageView new];
    } else {
        imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    }
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    imageView.layer.cornerRadius = radius;
    [superView addSubview:imageView];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (block) {
            block(imageView,make);
        }
    }];
    
    return imageView;
}

@end
