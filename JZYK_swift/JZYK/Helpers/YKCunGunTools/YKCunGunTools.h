//
//  YKCunGunTools.h
//  JZYK
//
//  Created by 吴春艳 on 2018/7/4.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YKCunGunEntity.h"
#import "YKVerifyBindingCardModel.h"

@interface YKCunGunTools : NSObject

/**
 存管弹框判断
 @param cunGunEntity 存管Entity
 @param isCommitOrder 切记：如果不用，传YES；(本来提交订单时是有区别的，现在没区别了，所以都传yes)
 */
+ (void)yk_cunguanAlertWithCunGunEntity:(YKCunGunEntity *)cunGunEntity isCommitOrder:(BOOL)isCommitOrder bindBankComplete:(void (^)(void))bindBankComplete;

/**
 返回钱包版本的存管弹框判断
 @param cunGunEntity 存管Entity
 @param bindBankComplete 绑卡完成回调
 */
+ (void)yk_cunguanAlertWithCunGunEntity:(YKVerifyBindingCardModel *)cunGunEntity bindBankComplete:(void (^)(NSString * card_desc, NSString *card_id, NSString *bank_name))bindBankComplete;

+ (void)yk_featchCunGuanDataWithCunguanFinishBlock:(void (^)(void))cunguanFinishBlock bindBankComplete:(void (^)(BOOL bankPageBlock,NSString * card_desc, NSString *card_id, NSString *bank_name))bindBankComplete;

@end
