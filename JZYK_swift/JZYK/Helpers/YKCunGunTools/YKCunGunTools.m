//
//  YKCunGunTools.m
//  JZYK
//
//  Created by 吴春艳 on 2018/7/4.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKCunGunTools.h"
#import "YKAddBankCardViewController.h"
#import "YKBorrowDepositAlertView.h"

@implementation YKCunGunTools

+ (void)yk_cunguanAlertWithCunGunEntity:(YKCunGunEntity *)cunGunEntity isCommitOrder:(BOOL)isCommitOrder bindBankComplete:(void (^)(void))bindBankComplete {

    if (!cunGunEntity && !cunGunEntity.alert) {
        return;
    }
    if (isCommitOrder && [cunGunEntity.type intValue] == 1) { //开户
        QLAlert * alert = [QLAlert new];
        [alert showWithTitle:nil message:cunGunEntity.alert.alert_desc contentTextAlignement:NSTextAlignmentLeft btnTitleArray:@[cunGunEntity.alert.alert_confirm_yes,cunGunEntity.alert.alert_confirm_no] btnClicked:^(NSInteger index) {
            if (index == 0) {
                YKAddBankCardViewController * vc = [YKAddBankCardViewController new];
                vc.bankBindComplete = ^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
                    if (bindBankComplete) {
                        bindBankComplete();
                    }
                };
                [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
            }
        }];
    }
    if ([cunGunEntity.type intValue] == 2) {//理财户
        QLAlert * alert = [QLAlert new];
        alert.contentTextAlignement = NSTextAlignmentLeft;
        NSArray * arr = @[];
        if ([cunGunEntity.alert.alert_confirm yk_isValidString]) {
            arr = @[cunGunEntity.alert.alert_confirm];
        }
        [alert prepareUIWithTitle:cunGunEntity.alert.alert_title contentText:cunGunEntity.alert.alert_desc topRightCloseBtn:YES aboutLineBankBtnTitle:@"" buttonWithBgImage:NO buttonTitleArray:arr buttonClicked:^(NSInteger index) {
        } aboutLineBankBtnClicked:^{
        } closeButtonClicked:^{
        }];
        [alert show];
    } else if ([cunGunEntity.type intValue] == 3) { //设置存管交易密码
        QLAlert * alert = [QLAlert new];
        alert.contentTextAlignement = NSTextAlignmentLeft;
        [alert prepareUIWithTitle:cunGunEntity.alert.alert_title contentText:cunGunEntity.alert.alert_desc topRightCloseBtn:YES aboutLineBankBtnTitle:@"" buttonWithBgImage:YES buttonTitleArray:@[cunGunEntity.alert.alert_confirm] buttonClicked:^(NSInteger index) {
            YKBrowseWebController * vc = [YKBrowseWebController new];
            vc.url = cunGunEntity.alert.alert_confirm_target;
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } aboutLineBankBtnClicked:^{
        } closeButtonClicked:^{
        }];
        [alert show];
    } else if ([cunGunEntity.type intValue] == 4) { //认证过期:已解绑请重绑
        QLAlert * alert = [QLAlert new];
        alert.contentTextAlignement = NSTextAlignmentLeft;
        [alert prepareUIWithTitle:cunGunEntity.alert.alert_title contentText:cunGunEntity.alert.alert_desc topRightCloseBtn:NO aboutLineBankBtnTitle:cunGunEntity.alert.about_title buttonWithBgImage:NO buttonTitleArray:@[cunGunEntity.alert.alert_confirm] buttonClicked:^(NSInteger index) {
            YKAddBankCardViewController * vc = [YKAddBankCardViewController new];
            vc.bankBindComplete = ^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
                if (bindBankComplete) {
                    bindBankComplete();
                }
            };
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } aboutLineBankBtnClicked:^{
            YKBrowseWebController * vc = [YKBrowseWebController new];
            vc.url = cunGunEntity.alert.about_title_url;
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } closeButtonClicked:^{
        }];
        [alert show];
    } else if ([cunGunEntity.type intValue] == 5) { //其他平台开户 or 银行卡变更
        QLAlert * alert = [QLAlert new];
        alert.contentTextAlignement = NSTextAlignmentLeft;
        [alert prepareUIWithTitle:cunGunEntity.alert.alert_title contentText:cunGunEntity.alert.alert_desc topRightCloseBtn:NO aboutLineBankBtnTitle:cunGunEntity.alert.about_title buttonWithBgImage:NO buttonTitleArray:@[cunGunEntity.alert.alert_confirm] buttonClicked:^(NSInteger index) {
        } aboutLineBankBtnClicked:^{
            YKBrowseWebController * vc = [YKBrowseWebController new];
            vc.url = cunGunEntity.alert.about_title_url;
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } closeButtonClicked:^{
        }];
        [alert show];
    } else if ([cunGunEntity.type intValue] == 6) { //目前卡不支持存管,请更换卡
        QLAlert * alert = [QLAlert new];
        alert.contentTextAlignement = NSTextAlignmentLeft;
        [alert prepareUIWithTitle:cunGunEntity.alert.alert_title contentText:cunGunEntity.alert.alert_desc topRightCloseBtn:YES aboutLineBankBtnTitle:cunGunEntity.alert.about_title buttonWithBgImage:YES buttonTitleArray:@[cunGunEntity.alert.alert_confirm] buttonClicked:^(NSInteger index) {
            YKAddBankCardViewController * vc = [YKAddBankCardViewController new];
            vc.bankBindComplete = ^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
                if (bindBankComplete) {
                    bindBankComplete();
                }
            };
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } aboutLineBankBtnClicked:^{
            YKBrowseWebController * vc = [YKBrowseWebController new];
            vc.url = cunGunEntity.alert.about_title_url;
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } closeButtonClicked:^{
        }];
        [alert show];
    }
}

+ (void)yk_cunguanAlertWithCunGunEntity:(YKVerifyBindingCardModel *)cunGunEntity bindBankComplete:(void (^)(NSString * card_desc, NSString *card_id, NSString *bank_name))bindBankComplete
{
    if([cunGunEntity.is_deposit_open_account intValue] == 1) { //开户
        YKBorrowDepositAlertView *alertView = [YKBorrowDepositAlertView new];
        [alertView updateUI:cunGunEntity];
        alertView.openSuccess = ^{
        };
        [alertView setGoAboutDepositBlk:^{
        }];
        [alertView show];
    }else if ([cunGunEntity.is_deposit_open_account intValue] == 2) { //银行卡信息变更 or 其他平台开户
        QLAlert * alert = [QLAlert new];
        alert.contentTextAlignement = NSTextAlignmentLeft;
        [alert prepareUIWithTitle:cunGunEntity.alert.alert_title contentText:cunGunEntity.alert.alert_desc topRightCloseBtn:NO aboutLineBankBtnTitle:cunGunEntity.alert.about_title buttonWithBgImage:NO buttonTitleArray:@[cunGunEntity.alert.alert_confirm] buttonClicked:^(NSInteger index) {
        } aboutLineBankBtnClicked:^{
            YKBrowseWebController * vc = [YKBrowseWebController new];
            vc.url = cunGunEntity.about_deposit;
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } closeButtonClicked:^{
        }];
        [alert show];
    }else if ([cunGunEntity.is_deposit_open_account intValue] == 3) { //银行卡不支持存管，更换银行卡
        QLAlert * alert = [QLAlert new];
        alert.contentTextAlignement = NSTextAlignmentLeft;
        [alert prepareUIWithTitle:cunGunEntity.alert.alert_title contentText:cunGunEntity.alert.alert_desc topRightCloseBtn:YES aboutLineBankBtnTitle:cunGunEntity.alert.about_title buttonWithBgImage:YES buttonTitleArray:@[cunGunEntity.alert.alert_confirm] buttonClicked:^(NSInteger index) {
            YKAddBankCardViewController * vc = [YKAddBankCardViewController new];
            vc.bankBindComplete = ^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
                if (bindBankComplete) {
                    bindBankComplete(card_desc,card_id,bank_name);
                }
            };
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } aboutLineBankBtnClicked:^{
            YKBrowseWebController * vc = [YKBrowseWebController new];
            vc.url = cunGunEntity.about_deposit;
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } closeButtonClicked:^{
        }];
        [alert show];
    }else if ([cunGunEntity.is_deposit_open_account intValue] == 4) { //口袋理财的投资用户
        QLAlert * alert = [QLAlert new];
        alert.contentTextAlignement = NSTextAlignmentLeft;
        NSArray * arr = @[];
        if ([cunGunEntity.alert.alert_confirm yk_isValidString]) {
            arr = @[cunGunEntity.alert.alert_confirm];
        }
        [alert prepareUIWithTitle:cunGunEntity.alert.alert_title contentText:cunGunEntity.alert.alert_desc topRightCloseBtn:YES aboutLineBankBtnTitle:@"" buttonWithBgImage:NO buttonTitleArray:arr buttonClicked:^(NSInteger index) {
            UIViewController * cunrrentVC = [YKTabBarController yk_shareTabController].yk_getCurrentViewController;
            [cunrrentVC.navigationController popViewControllerAnimated:YES];
        } aboutLineBankBtnClicked:^{
        } closeButtonClicked:^{
            UIViewController * cunrrentVC = [YKTabBarController yk_shareTabController].yk_getCurrentViewController;
            [cunrrentVC.navigationController popViewControllerAnimated:YES];
        }];
        [alert show];
    } else if ([cunGunEntity.is_deposit_open_account intValue] == 5) { //--认证过期：其他平台解绑
        QLAlert * alert = [QLAlert new];
        alert.contentTextAlignement = NSTextAlignmentLeft;
        [alert prepareUIWithTitle:cunGunEntity.alert.alert_title contentText:cunGunEntity.alert.alert_desc topRightCloseBtn:NO aboutLineBankBtnTitle:cunGunEntity.alert.about_title buttonWithBgImage:NO buttonTitleArray:@[cunGunEntity.alert.alert_confirm] buttonClicked:^(NSInteger index) {
            YKAddBankCardViewController * vc = [YKAddBankCardViewController new];
            vc.bankBindComplete = ^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
                if (bindBankComplete) {
                    bindBankComplete(card_desc,card_id,bank_name);
                }
            };
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } aboutLineBankBtnClicked:^{
            YKBrowseWebController * vc = [YKBrowseWebController new];
            vc.url = cunGunEntity.about_deposit;
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
        } closeButtonClicked:^{
        }];
        [alert show];
    }
}


+ (void)yk_featchCunGuanDataWithCunguanFinishBlock:(void (^)(void))cunguanFinishBlock bindBankComplete:(void (^)(BOOL bankPageBlock,NSString * card_desc, NSString *card_id, NSString *bank_name))bindBankComplete
{
    [HTTPManager.session getRequestForKey:kCreditCardGetDepositOpenInfo showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        if (code == 0 && success) {
            YKVerifyBindingCardModel * cunguanModel = [YKVerifyBindingCardModel yy_modelWithJSON:json];
            if (cunguanModel.have_card != 1) {  //没卡
                YKAddBankCardViewController * vc = [YKAddBankCardViewController new];
                vc.bankBindComplete = ^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
                    if (bindBankComplete) {
                        bindBankComplete(YES,card_desc,card_id,bank_name);
                    }
                };
                [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
            } else {
                if ([cunguanModel.is_deposit_open_account intValue] == 0) {
                    if (cunguanFinishBlock) {
                        cunguanFinishBlock();
                    }
                } else {
                    if ([cunguanModel.card_no yk_isValidString] && cunguanModel.card_no.length >= 4 && [cunguanModel.bank_name yk_isValidString]) {
                        NSString * sixCardId = [cunguanModel.card_no substringFromIndex:cunguanModel.card_no.length - 4];
                        NSString * bankDec = [NSString stringWithFormat:@"%@ (%@)",cunguanModel.bank_name,sixCardId];
                        if (bindBankComplete) {
                            bindBankComplete(NO,bankDec,sixCardId,cunguanModel.bank_name);
                        }
                    }
                    [YKCunGunTools yk_cunguanAlertWithCunGunEntity:cunguanModel bindBankComplete:^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
                        if (bindBankComplete) {
                            bindBankComplete(YES,card_desc,card_id,bank_name);
                        }
                    }];
                }
            }
        } else if(code != 0){
            [[iToast makeText:msg] show];
        }
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

@end




