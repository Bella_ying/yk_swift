//
//  YKBorrowDepositAlertView.h
//  KDFDApp
//
//  Created by Kiran on 2018/7/11.
//  Copyright © 2018年 Kiran. All rights reserved.
// 开通存管弹框

#import "YKBorrowDepositAlertView.h"
#import "YKVerifyBindingCardModel.h"
#import "YKProtocolView.h"

@interface YKBorrowDepositAlertView ()<UITextFieldDelegate>

//银行卡
@property (nonatomic, strong) UILabel *bankLabel;
//手机号
@property (nonatomic, strong) UITextField *phoneTf;
//底部线
@property (nonatomic, strong) UIView *line;
//手机号底部线
@property (nonatomic, strong) UIView *line1;
//手机验证码
@property (nonatomic, strong) UITextField *codeTf;
@property (nonatomic, strong) UIButton *getCodeBtn;
//验证码底部线
@property (nonatomic, strong) UIView *line2;
//关于存管
@property (nonatomic, strong) UIButton *aboutDepositBtn;
//取消按钮
@property (nonatomic, strong) UIButton *cancelBtn;
//确定按钮
@property (nonatomic, strong) UIButton *confirmBtn;

//协议
@property (nonatomic, strong) YKProtocolView *protocolView;

@property (nonatomic, strong) YKVerifyBindingCardModel *cardEntity;

@end

@implementation YKBorrowDepositAlertView

- (void)configUI
{
    WEAK_SELF
    [self.alertBackGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(37.5 * ASPECT_RATIO_WIDTH);
        make.right.offset(-37.5 * ASPECT_RATIO_WIDTH);
        make.centerY.offset(-30);
    }];
    
    //关闭按钮
    UIButton *closeBtn = [UIButton new];
    [closeBtn setImage:[UIImage imageNamed:@"webview_close"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.alertBackGroundView addSubview:closeBtn];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.offset(40);
        make.right.offset(-10 *ASPECT_RATIO_WIDTH);
        make.top.offset(10 *ASPECT_RATIO_WIDTH);
        
    }];
    [closeBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    self.cancelBtn = closeBtn;
    
    //title
    UILabel *titleLabel = [UILabel new];
    titleLabel.text = @"开通存管";
    titleLabel.textColor = Color.color_66_C3;
    titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(18)];
    [self.alertBackGroundView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.offset(30 *ASPECT_RATIO_WIDTH);
    }];
    
    //银行卡
    UILabel *bankDescLabel = [UILabel new];
    bankDescLabel.text = @"收款银行卡";
    bankDescLabel.textColor = Color.color_66_C3;
    bankDescLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)] ;
    [self.alertBackGroundView addSubview:bankDescLabel];
    [bankDescLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15 *ASPECT_RATIO_WIDTH);
        make.top.equalTo(titleLabel.mas_bottom).offset(25 *ASPECT_RATIO_WIDTH);
    }];
    
    self.bankLabel = [UILabel new];
    self.bankLabel.textColor = Color.color_66_C3;
    self.bankLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
    [self.alertBackGroundView addSubview:self.bankLabel];
    [self.bankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bankDescLabel.mas_centerY);
        make.right.offset(-15 *ASPECT_RATIO_WIDTH);
    }];
    //分割线
    self.line = [UIView new];
    self.line.backgroundColor = LINE_COLOR;
    [self.alertBackGroundView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15 *ASPECT_RATIO_WIDTH);
        make.right.offset(-15 *ASPECT_RATIO_WIDTH);
        make.top.equalTo(bankDescLabel.mas_bottom).offset(15 *ASPECT_RATIO_WIDTH);
        make.height.offset(LINE_HEIGHT);
    }];
    
    //预留手机号
    UILabel *phoneDescLabel = [UILabel new];
    phoneDescLabel.text = @"预留手机号";
    phoneDescLabel.textColor = Color.color_66_C3;
    phoneDescLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
    [self.alertBackGroundView addSubview:phoneDescLabel];
    [phoneDescLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15 *ASPECT_RATIO_WIDTH);
        make.top.equalTo(bankDescLabel.mas_bottom).offset(30 *ASPECT_RATIO_WIDTH);
    }];
    
    self.phoneTf = [UITextField new];
    self.phoneTf.placeholder = @"请输入手机号";
    self.phoneTf.textColor = Color.color_66_C3;
    self.phoneTf.font = [UIFont systemFontOfSize:adaptFontSize(14)] ;
    self.phoneTf.textAlignment = NSTextAlignmentRight;
    self.phoneTf.tintColor = Color.main;
    self.phoneTf.delegate = self;
    self.phoneTf.keyboardType = UIKeyboardTypeNumberPad;
    [self.alertBackGroundView addSubview:self.phoneTf];
    [self.phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-15 *ASPECT_RATIO_WIDTH);
        make.centerY.equalTo(phoneDescLabel.mas_centerY);
        make.left.equalTo(phoneDescLabel.mas_right).offset(10*ASPECT_RATIO_WIDTH);
        make.height.offset(40*ASPECT_RATIO_WIDTH);
    }];
    
    //分割线
    self.line1 = [UIView new];
    self.line1.backgroundColor = LINE_COLOR;
    [self.alertBackGroundView addSubview:self.line1];
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15 *ASPECT_RATIO_WIDTH);
        make.right.offset(-15 *ASPECT_RATIO_WIDTH);
        make.top.equalTo(phoneDescLabel.mas_bottom).offset(15 *ASPECT_RATIO_WIDTH);
        make.height.offset(LINE_HEIGHT);
    }];
    
    
    //验证码
    UILabel *codeLabel = [UILabel new];
    codeLabel.text = @"验证码";
    codeLabel.textColor = Color.color_66_C3;
    codeLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
    [self.alertBackGroundView addSubview:codeLabel];
    [codeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15 *ASPECT_RATIO_WIDTH);
        make.top.equalTo(phoneDescLabel.mas_bottom).offset(30 *ASPECT_RATIO_WIDTH);
    }];
    
    //验证码
    UIButton * codeBtn = [UIButton yk_buttonFontSize:14 textColor:Color.main backGroundImage:@"" imageName:@"" cornerRadius:3 superView:self.alertBackGroundView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        STRONG_SELF
        make.right.offset(-15 *ASPECT_RATIO_WIDTH);
        make.centerY.equalTo(codeLabel.mas_centerY);
        make.height.offset(30);
        make.width.offset(75);
        
        button.layer.borderColor = Color.main.CGColor;
        button.layer.borderWidth = 1.0;
        [button setTitle:@"获取" forState:UIControlStateNormal];
        [button addTarget:strongSelf action:@selector(sendMessage:) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    self.codeTf = [UITextField new];
    self.codeTf.placeholder = @"请输入6位验证码";
    self.codeTf.textAlignment = NSTextAlignmentRight;
    self.codeTf.textColor = Color.color_66_C3;
    self.codeTf.font = [UIFont systemFontOfSize:adaptFontSize(14)];
    self.codeTf.adjustsFontSizeToFitWidth = YES;
    self.codeTf.delegate = self;
    self.codeTf.keyboardType = UIKeyboardTypeNumberPad;
    self.codeTf.tintColor = Color.main;
    [self.alertBackGroundView addSubview:self.codeTf];
    [self.codeTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(codeLabel.mas_right).offset(5 *ASPECT_RATIO_WIDTH);
        make.centerY.equalTo(codeBtn.mas_centerY);
        make.right.equalTo(codeBtn.mas_left).offset(-10 *ASPECT_RATIO_WIDTH);
        make.height.offset(40 *ASPECT_RATIO_WIDTH);
    }];
    
    //分割线
    UIView *lineView = [UIView new];
    lineView.backgroundColor = LINE_COLOR;
    [self.alertBackGroundView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        STRONG_SELF
        make.left.offset(15 *ASPECT_RATIO_WIDTH);
        make.right.offset(-15 *ASPECT_RATIO_WIDTH);
        make.top.equalTo(strongSelf.codeTf.mas_bottom);
        make.height.offset(LINE_HEIGHT);
    }];
    self.line2 = lineView;
    
    //保存按钮
    self.confirmBtn = [UIButton yk_buttonFontSize:17 textColor:Color.whiteColor backGroundImage:@"btn_bg_image" imageName:@"" cornerRadius:29.0f superView:self.alertBackGroundView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        STRONG_SELF
        make.right.offset(-15 *ASPECT_RATIO_WIDTH);
        make.left.offset(15 *ASPECT_RATIO_WIDTH);
        make.top.equalTo(lineView.mas_bottom).offset(15*ASPECT_RATIO_WIDTH);
        make.height.offset(58);

        [button setTitle:@"开通" forState:UIControlStateNormal];
        [button addTarget:strongSelf action:@selector(confirmBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    /*协议视图*/
    self.protocolView = [YKProtocolView new];
    self.protocolView.preferredMaxLayoutWidth = WIDTH_OF_SCREEN - 220 * WIDTH_RATIO;
    [self.alertBackGroundView addSubview:self.protocolView];
    [self.protocolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.confirmBtn.mas_bottom).offset(10);
        make.centerX.equalTo(self.alertBackGroundView);
        make.width.mas_equalTo(WIDTH_OF_SCREEN - 220 * WIDTH_RATIO);
    }];
    
    //关于存管
    UIButton *aboutBtn = [UIButton new];
    aboutBtn.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
    [aboutBtn setTitleColor:Color.main forState:UIControlStateNormal];
    [aboutBtn setTitle:@"关于存管" forState:UIControlStateNormal];
    [aboutBtn addTarget:self action:@selector(goAboutDeposit) forControlEvents:UIControlEventTouchUpInside];
    [self.alertBackGroundView addSubview:aboutBtn];
    [aboutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        STRONG_SELF
        make.centerX.offset(0);
        make.top.equalTo(strongSelf.protocolView.mas_bottom).offset(10*ASPECT_RATIO_WIDTH);
        make.bottom.offset(-15 *ASPECT_RATIO_HEIGHT);
    }];
    self.aboutDepositBtn = aboutBtn;
}

//更新UI
- (void)updateUI:(id)model
{
    self.cardEntity = model;
    //刷新协议
    [self.protocolView yk_refreshProtocolTextEntity:self.cardEntity.protocols protocolString:@"protocol_name" url:@"protocol_url" tapAction:^(NSAttributedString *text, NSDictionary *obj, NSUInteger idx) {
        [self dismiss];
        YKBrowseWebController *webView = [[YKBrowseWebController alloc] init];
        NSString *url = [obj[@"protocol_url"] yk_URLEncodedString];
        if (idx==1) {
            //URL需要Encoding,因为在JSON解析后已经被decoding过，因为URL还要重新发送给webView去loadRequest,不能包含中文字符
            url = [url yk_URLEncodedString];
        }
        webView.url = url;
        [[YKTabBarController yk_shareTabController] yk_pushToViewController:webView];
    }];

    if (self.cardEntity.card_no.length > 4) {
        self.bankLabel.text = NSStringFormat(@"%@(尾号%@)",self.cardEntity.bank_name,[self.cardEntity.card_no substringFromIndex:self.cardEntity.card_no.length - 4]) ;
    }else{
        self.bankLabel.text = NSStringFormat(@"%@(%@)",self.cardEntity.bank_name,self.cardEntity.card_no) ;
    }
    self.phoneTf.text = self.cardEntity.phone_num;
}

- (void)dismissSelf
{
    if (self.openSuccess) {
        self.openSuccess();
    }
    [self dismiss];
}

//关于存管
- (void)goAboutDeposit
{
    if (self.goAboutDepositBlk) {
        self.goAboutDepositBlk();
    }
    [self dismiss];
    YKBrowseWebController *web = [YKBrowseWebController new];
    web.url = self.cardEntity.about_deposit;
    [[YKTabBarController yk_shareTabController] yk_pushToViewController:web];
}

#pragma mark -- 发送验证码
- (void)sendMessage:(UIButton *)sender
{
    [CodeSecure md5EncryptWithPhoneNum:[NSString stringWithFormat:@"%@",self.phoneTf.text] encryptedCallBack:^(NSString * _Nonnull sign, NSString * _Nonnull random) {
        NSDictionary * parm = @{@"type": @"open-account",
                                @"bank_id": [NSString stringWithFormat:@"%@",self.cardEntity.bank_id],
                                @"card_no": [NSString stringWithFormat:@"%@",self.cardEntity.card_no],
                                @"phone": [NSString stringWithFormat:@"%@",self.phoneTf.text],
                                @"sign": sign,
                                @"random": random};
        [CodeSecure bindCardCodeSendWithParam:parm sendComplete:^(BOOL isSuccess) {
            if (isSuccess) {
                [CodeButton timerCountDownForButton:sender elapsedTime:90 title:@"s" finish:^{
                }];
            }
        }];
    }];
}

#pragma mark -- 开通存管按钮点击
- (void)confirmBtnClick
{
    WEAK_SELF
    if (self.phoneTf.text.length != 11) {
        [[iToast makeText:@"手机号码输入有误"] show];
        return;
    }
    
    if (self.codeTf.text.length == 0) {
         [[iToast makeText:@"请输入验证码"] show];
        return;
    }
    
    if (!self.protocolView.isSelected) {
        [[iToast makeText:@"请先阅读并授权协议"] show];
        return;
    }
    [[HTTPManager session] postRequestForKey:kCreditUserDepositOpen showLoading:YES param:@{@"code":self.codeTf.text} succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        STRONG_SELF
        if (code == 0) {
            [strongSelf dismissSelf];
            [[iToast makeText:@"存管开通成功"] show];
        } else{
            [[iToast makeText:msg] show];
        }
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

- (void)show{
    [super show];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneTf) {
        if (range.location > 10) {
            return false;
        }
    }
    if (textField == self.codeTf) {
        if (range.location > 5) {
            return false;
        }
    }
    return true;
}

#pragma mark-- 点击空白隐藏键盘
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    id view = [super hitTest:point withEvent:event];
    if ([view isKindOfClass:[UIView class]]) {
        [KEY_WINDOW endEditing:YES];
        return view;
    } else {
        return view;
    }
}

@end
