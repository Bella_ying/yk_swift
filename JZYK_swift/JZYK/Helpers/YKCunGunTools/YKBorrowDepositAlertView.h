//
//  YKBorrowDepositAlertView.h
//  KDFDApp
//
//  Created by Kiran on 2018/7/11.
//  Copyright © 2018年 Kiran. All rights reserved.
// 开通存管弹框

#import "QLBaseAlertView.h"

@interface YKBorrowDepositAlertView : QLBaseAlertView

@property (nonatomic, copy) void_block_t openSuccess;
@property (nonatomic, copy) void(^goAboutDepositBlk)(void);

@end
