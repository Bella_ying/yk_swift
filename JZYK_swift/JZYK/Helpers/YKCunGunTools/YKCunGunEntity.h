//
//  YKCunGunEntity.h
//  JZYK
//
//  Created by 吴春艳 on 2018/7/4.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKCunGunContentEntity : NSObject
@property (nonatomic, copy) NSString *alert_title;
@property (nonatomic, copy) NSString *alert_desc;
@property (nonatomic, copy) NSString *alert_confirm;
@property (nonatomic, copy) NSString *alert_confirm_target;
@property (nonatomic, copy) NSString *about_title;
@property (nonatomic, copy) NSString *about_title_url;
@property (nonatomic, copy) NSString *alert_confirm_no;
@property (nonatomic, copy) NSString *alert_confirm_yes;

@end


@interface YKCunGunEntity : NSObject
//type-2:口袋理财户；6:目前卡不支持存管，更卡； 5:其他平台开户 or 银行卡变更,确认； 1:去开通存管 4：已解绑请重绑 3：设置存管交易密码
@property (nonatomic, copy) NSString *type;
@property (nonatomic, strong) YKCunGunContentEntity *alert;

@end
