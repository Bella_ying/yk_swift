//
//  TextField.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/21.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import SnapKit

enum InputType {
    case phoneNum   //数字
    case code       //数字
    case loginPwd   //字母与数字与符号
    case name       //字母
    case IDNum      //字母与数字
}

class InputView: UIView {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var line: UIView!
    private var text: String = ""
    public var checkButtonStatus: ((Bool)->Void)?
    public var textFieldDidChangeCallBack: ((UITextField)->Void)?
    
    public var textFieldClearCallBack: ((UITextField)->Void)?        //清空输入框
    
    private var isButtonsRemoved: Bool? {
        didSet {
            if let removed = isButtonsRemoved {
                self.textField.clearButtonMode = removed ? .whileEditing : .never;
            }
        }
    }
    
   public var inputType: InputType? {
        didSet {
            
            guard let type = inputType else {
                return
            }
            var placeText: String = "";
            switch type {
            case .phoneNum:
                placeText = "请输入手机号码"
                self.textField.keyboardType = .phonePad
            case .code:
                placeText = "请输入6位验证码"
                self.textField.keyboardType = .phonePad
            case .IDNum:
                placeText = "请输入身份证号"
                self.textField.keyboardType = .default
            case .loginPwd:
                placeText = "请输入密码"
                self.textField.keyboardType = .default
                self.textField.isSecureTextEntry = true //密码默认安全模式
            case .name:
                placeText = "请输入真实姓名"
                self.textField.keyboardType = .default
            }
            self.textField.textColor =  Color.color_45_C2;
            self.textField.attributedPlaceholder = NSAttributedString(string: placeText, attributes: [NSAttributedStringKey.foregroundColor:UIColor(hex: "#CCCCCC")]);
        }
    }
    
    @IBOutlet weak var codeButton: CodeButton!
    
    @IBOutlet weak var secureButton: UIButton!
    
    @IBAction func switchSecureMode(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        textField.isSecureTextEntry = !textField.isSecureTextEntry
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.tintColor = Color.main;
        self.textField.delegate = self;
        self.textField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    static var input: InputView {
        get {
            return Bundle.main.loadNibNamed("InputView", owner: self, options: nil)?.last as! InputView;
        }
    }
    
    var placeHolder: String? {
        get {
            return self.textField.placeholder
        }
        set {
            self.textField.placeholder = newValue
        }
    }
    
    //监听键盘的输入变化
    @objc func textFieldDidChange(textField: UITextField) {
        self.textFieldDidChangeCallBack?(textField)
        guard let length = calculateLengthForInput() else { return }
        InputView.limitText(length: length, forTextField: textField);
        if let isEnabled = buttonCanEnable(textField: textField), isEnabled == true {
            self.checkButtonStatus?(isEnabled)
            
            if inputType == .loginPwd {
                iToast.makeText("密码长度不能超过16位").show()
            }
        }
    }
    
    private func buttonCanEnable(textField: UITextField) ->Bool? {
        guard let length = calculateLengthForInput() else { return nil }
        guard let inputText = textField.text else {
            return nil;
        }
        if length == inputText.count {
            return true
        }
        return false;
    }
    
    private func calculateLengthForInput() -> Int? {
        var length = 1000;
        guard let type = inputType else {
            return nil;
        }
        switch type {
        case .phoneNum:
            length = 11;
        case .code:
            length = 6;
        case .IDNum:
            length = 18;
        case .loginPwd:
            length = 16;
        case .name:
            length = 15;
        }
        return length;
    }
    
    @objc static func limitText(length: Int, forTextField textField: UITextField) {
//    private func limitText(length: Int, forTextField textField: UITextField) {
        let kMaxLength: Int = length
        guard let toBeString = textField.text else { return }
        let lang = UIApplication.shared.textInputMode?.primaryLanguage
        if (lang == "zh-Hans") { //中文输入
            let selectedRange: UITextRange? = textField.markedTextRange   //获取高亮部分
            var position: UITextPosition? = nil
            if let aStart = selectedRange?.start {
                position = textField.position(from: aStart, offset: 0)
            }
            if position == nil {
                // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
                if toBeString.count > kMaxLength {
                    textField.text = (toBeString as NSString?)?.substring(to: kMaxLength)
                }
            } else {
                //有高亮选择的字符串，则暂不对文字进行统计和限制
            }
        } else {
            //中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
            if toBeString.count > kMaxLength {
                textField.text = (toBeString as NSString?)?.substring(to: kMaxLength)
            }
        }
    }
}

extension InputView {
    func removeAllButtons() {
        self.codeButton.removeFromSuperview()
        self.secureButton.removeFromSuperview()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.subviews.forEach { [weak self] in
            self?.isButtonsRemoved = ($0 != self?.codeButton && $0 != self?.secureButton)
        }
    }
}

extension InputView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.line.backgroundColor = Color.main
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.line.backgroundColor = Color.color_DC_C9
        YKDebugger.debugLog {
            print(textField.text!)            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.textFieldClearCallBack?(textField);
        return true
    }
}


