//
//  TextField.swift
//  KeyBorad
//
//  Created by Jeremy on 2018/6/12.
//  Copyright © 2018 Jeremy. All rights reserved.
//

import UIKit
enum TextFieldKeyBoardType {
    case system
    case custom
}


class TextField: UITextField {
    var textFieldReturnAction: ((TextField)->Void)?
    var customType: CustomKeyboardType? {
        didSet {
            keyBoard.customkeyBoardType = { [weak self] (keyBoard) in
                return (self?.customType)!
            }
            self.inputView = keyBoard;
            keyBoard.setupKeyBoard()
        }
    }
    
    lazy var keyBoard: KeyBoardView = {
        let keyBoard = KeyBoardView.defaultKeyBoard;
        
        keyBoard.inputEndAction = { [weak self] (text) in
            self?.text = text
            self?.textFieldReturnAction?(self!)
            self?.resignFirstResponder()
        }
        keyBoard.inputTextClearAction = {[weak self] (keyBoard) in
            self?.text = ""
        }
        keyBoard.keyClickAction = {[weak self] (text) in
            self?.text = text
        }
        keyBoard.textDeleteAction = {[weak self] (text) in
            self?.deleteBackward()
            self?.text = text;
        }
        return keyBoard
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.inputView = keyBoard;
        keyBoard.setupKeyBoard()
    }
    
    func setKeyBoardType(keyboard: TextFieldKeyBoardType) {
        switch keyboard {
        case .system:
            self.inputView = nil;
        case .custom:
            self.inputView = self.keyBoard
        }
    }
}


