//
//  KeyBoardView.swift
//  KeyBorad
//
//  Created by Jeremy on 2018/6/12.
//  Copyright © 2018 Jeremy. All rights reserved.
//

import UIKit

enum CustomKeyboardType {
    case pureNumber
    case numberWithDot
    case IDType
    case safeMode
}

class KeyBoardView: UIView {
    var string: String = ""
    var inputEndAction: ((String?)->Void)?                         //输入完成
    var customkeyBoardType: ((KeyBoardView) -> CustomKeyboardType)? //定制键盘类型
    var inputTextClearAction: ((KeyBoardView)->Void)?               //清空输入
    var keyClickAction: ((String?)->Void)?
    var textDeleteAction: ((String?)->Void)?                                //点击删除
    
    static var defaultKeyBoard: KeyBoardView {
        get {
            let keyboard = Bundle.main.loadNibNamed("KeyBoardView", owner: self, options: nil)?.last as! KeyBoardView
            //226 键盘默认高度
            keyboard.backgroundColor = UIColor.red;
            return keyboard;
        }
    }
    
    var IDkeys: Array<String> {
        get {
            return ["1", "2", "3", "4", "5", "6", "7", "8", "9", "X", "0", ""];
        }
    }
    
    var numberWithDotKeys: Array<String> {
        get {
            return ["1", "2", "3", "4", "5", "6", "7", "8", "9", ".", "0", ""];
        }
    }
    
    var pureNumKeys: Array<String> {
        get {
            return ["1", "2", "3", "4", "5", "6", "7", "8", "9", "", "0", ""];
        }
    }
    
    func safeModekeys() ->Array<String> {
        var container: Array<String> = [];
        let keysMaxNum: uint = 10
        for _ in 0..<keysMaxNum {
            while (true) {
                let titleNum = arc4random() % keysMaxNum
                let titleString = "\(titleNum)";
                if !container.contains(titleString) {
                    container.append(titleString)
                    break
                }
                if container.count == 10 {
                    break;
                }
            }
        }
        container.insert("完成", at: 9);
        container.append("");
        return container;
    }
    
    private var keyBoardKeys: Array<String>?
    
    func setupKeyBoard() {
        guard let type = self.customkeyBoardType?(self) else { return }
        switch type {
        case .pureNumber:
            self.keyBoardKeys = self.pureNumKeys
        case .numberWithDot:
            self.keyBoardKeys = self.numberWithDotKeys
        case .IDType:
            self.keyBoardKeys = self.IDkeys
        case .safeMode:
            self.keyBoardKeys = self.safeModekeys()
        }
        
        for (idx, value) in self.keyBoardKeys!.enumerated() {
            let button = viewWithTag(500+idx) as! UIButton;
            
            button.addTarget(self, action: #selector(keysOnClick(button:)), for: .touchUpInside);
            button.setTitle(value, for: .normal);
            button.setTitleColor(UIColor.white, for: .normal);
//            button.titleLabel?.font = UIFont.systemFont(ofSize: 30, weight: .bold);
            if idx == 11 {
                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(clearInputField(gesture:)))
                button.addGestureRecognizer(longPress);
            }
        }
    }
    
    @objc func keysOnClick(button: UIButton) {
        if button.tag - 500 == 9 {   //. X 完成
            guard let type = self.customkeyBoardType?(self) else { return }
            if type == .safeMode {
                self.inputEndAction?(self.string) //点击完成
            }
            if type == .numberWithDot || type == .IDType {
                self.string.append(button.currentTitle!)
            }
        }
        else if button.tag-500 == 11 {
            if self.string.count > 0 {
                self.string = String(self.string.dropLast())
                self.textDeleteAction?(self.string)
            }
        }
        else {
            self.string.append(button.currentTitle!)
        }
//        print("\(button.tag-500+1) click");
        self.keyClickAction?(self.string)
    }
    
    @objc func clearInputField(gesture: UILongPressGestureRecognizer) {
        self.inputTextClearAction?(self)//长按删除
        self.string = "";
        self.keyClickAction?(self.string)
    }
}







