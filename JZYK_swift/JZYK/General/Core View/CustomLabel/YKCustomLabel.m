//
//  YKCustomLabel.m
//  JZYK
//
//  Created by zhaoying on 2018/7/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKCustomLabel.h"

@implementation YKCustomLabel

- (instancetype)init {
    if (self = [super init]) {
        _textInsets = UIEdgeInsetsZero;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _textInsets = UIEdgeInsetsZero;
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, _textInsets)];
}

@end
