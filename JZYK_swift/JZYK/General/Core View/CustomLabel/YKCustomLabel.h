//
//  YKCustomLabel.h
//  JZYK
//
//  Created by zhaoying on 2018/7/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKCustomLabel : UILabel
@property (nonatomic, assign) UIEdgeInsets textInsets; // 控制字体与控件边界的间隙
@end
