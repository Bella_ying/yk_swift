//
//  HDTableViewMaker.h
//
//  Created by 洪东 on 7/15/16.
//  Copyright © 2016 Cocbin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HDTableData.h"
#pragma mark -- Class HDTableViewMaker
@class HDTableSectionMaker;
@interface HDTableViewMaker : NSObject

@property (nonatomic, strong) HDTableData *tableData;

- (instancetype)initWithTableView:(UITableView *)tableView;

- (instancetype)initWithTableData:(HDTableData *)tableData;

- (HDTableViewMaker * (^)(CGFloat))hd_height;

- (HDTableViewMaker * (^)(UIView * (^)(void)))hd_tableViewHeaderView;

- (HDTableViewMaker * (^)(UIView * (^)(void)))hd_tableViewFooterView;

- (HDTableViewMaker * (^)(NSInteger))hd_sectionCount;

- (HDTableViewMaker * (^)(SectionCountBlock))hd_sectionCountBk;

- (HDTableViewMaker * (^)(SectionMakeBlock))hd_sectionMaker;

- (HDTableViewMaker *) hd_sectionMaker:(SectionMakeBlock)sectionMakeBlock;

- (HDTableViewMaker * (^)(SectionMakeBlock))hd_addSectionMaker;

- (HDTableViewMaker *) hd_addSectionMaker:(SectionMakeBlock)sectionMakeBlock;

- (HDTableViewMaker * (^)(CellWillDisplayBlock))hd_cellWillDisplay;

- (HDTableViewMaker * (^)(CellEndDisplayingBlock))hd_cellEndDisplaying;

- (HDTableViewMaker * (^)(CommitEditingBlock))hd_commitEditing;
// 开始滑动
- (HDTableViewMaker * (^)(ScrollViewDidScrollBlock))hd_scrollViewDidScroll;
// 开始拖动
- (HDTableViewMaker * (^)(ScrollViewWillBeginDraggingBlock))hd_scrollViewWillBeginDraggingScroll;
// 结束拖动
- (HDTableViewMaker * (^)(ScrollViewDidEndDeceleratingBlock))hd_scrollViewDidEndDeceleratingScroll;

@end
