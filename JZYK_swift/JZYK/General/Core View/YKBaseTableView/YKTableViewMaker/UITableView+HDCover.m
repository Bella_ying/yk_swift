//
//  UITableView+HDCover.m
//  HDTableViewMakerDemo
//
//  Created by 洪东 on 8/26/16.
//  Copyright © 2016 Abner. All rights reserved.
//

#import "UITableView+HDCover.h"
#import "NSObject+HDAssociated.h"
#import "NSBundle+HDTableViewMaker.h"
#import "Masonry.h"

#pragma mark - HDCoverSetView

@interface HDCoverSetEmptyView : UIView
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, copy) HDTMVoidBlock reloadClickBlock;
@end

@implementation HDCoverSetEmptyView

- (instancetype) init{
    self = [super init];
    if (self) {
        [self addSubview:self.imageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.button];
        [self setupConstraints];
    }
    return self;
}

- (void)didMoveToSuperview
{
    self.frame = self.superview.bounds;
    
    void(^fadeInBlock)(void) = ^{self.alpha = 1.0;};
    
    [UIView animateWithDuration:0.25
                     animations:fadeInBlock
                     completion:NULL];
    
}

- (void)setupConstraints
{
    
   [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.centerX.equalTo(self);
       make.top.equalTo(self.mas_top).offset(200 * ASPECT_RATIO_WIDTH);
       make.size.mas_equalTo(CGSizeMake(100 * ASPECT_RATIO_WIDTH, 100 * ASPECT_RATIO_WIDTH));
   }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.imageView);
        make.top.mas_equalTo(self.imageView.mas_bottom).offset(4 * ASPECT_RATIO_WIDTH);
    }];
    
    [_button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.titleLabel);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(96 * ASPECT_RATIO_WIDTH);
        make.size.mas_equalTo(CGSizeMake(306 * ASPECT_RATIO_WIDTH, 54 * ASPECT_RATIO_WIDTH));
    }];
    
}

- (UIImageView *)imageView
{
    if (!_imageView)
    {
        _imageView = [UIImageView new];
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.accessibilityIdentifier = @"empty set background image";
    }
    return _imageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel)
    {
        _titleLabel = [UILabel new];
        _titleLabel.backgroundColor = [UIColor clearColor];
        
        _titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        _titleLabel.textColor = GRAY_COLOR_AD;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.numberOfLines = 0;
        _titleLabel.accessibilityIdentifier = @"empty set title";
    }
    return _titleLabel;
}

- (UIButton *)button
{
    if (!_button)
    {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.backgroundColor = [UIColor clearColor];
        [_button setBackgroundImage:[UIImage imageNamed:@"btn_bg_image"] forState:UIControlStateNormal];
        _button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        _button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _button.accessibilityIdentifier = @"empty set button";
        [_button setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        _button.titleLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(17)];
        [_button addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}

- (void)didTapButton:(UIButton *)sender{
    if (self.reloadClickBlock) {
        self.reloadClickBlock();
    }
}

@end

@interface HDCoverSetErrorView : UIView
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, copy) HDTMVoidBlock reloadClickBlock;
@end

@implementation HDCoverSetErrorView
- (instancetype) init{
    self = [super init];
    if (self) {
        [self addSubview:self.imageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.button];
        [self setupConstraints];
    }
    return self;
}

- (void)didMoveToSuperview
{
    self.frame = self.superview.bounds;
    
    void(^fadeInBlock)(void) = ^{self.alpha = 1.0;};
    
    [UIView animateWithDuration:0.25
                     animations:fadeInBlock
                     completion:NULL];
    
}

- (void)setupConstraints
{
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.mas_top).offset(200 * ASPECT_RATIO_WIDTH);
        make.size.mas_equalTo(CGSizeMake(100 * ASPECT_RATIO_WIDTH, 100 * ASPECT_RATIO_WIDTH));
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.imageView);
        make.top.mas_equalTo(self.imageView.mas_bottom).offset(5 * ASPECT_RATIO_WIDTH);
    }];
    
    [_button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.titleLabel);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(96 * ASPECT_RATIO_WIDTH);
        make.size.mas_equalTo(CGSizeMake(306 * ASPECT_RATIO_WIDTH, 54 * ASPECT_RATIO_WIDTH));
    }];
    
}

- (UIImageView *)imageView
{
    if (!_imageView)
    {
        _imageView = [UIImageView new];
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.accessibilityIdentifier = @"empty set background image";
    }
    return _imageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel)
    {
        _titleLabel = [UILabel new];
        _titleLabel.backgroundColor = [UIColor clearColor];
        
        _titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        _titleLabel.textColor = GRAY_COLOR_AD;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.numberOfLines = 0;
        _titleLabel.accessibilityIdentifier = @"empty set title";
    }
    return _titleLabel;
}

- (UIButton *)button
{
    if (!_button)
    {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.backgroundColor = [UIColor clearColor];
        [_button setBackgroundImage:[UIImage imageNamed:@"btn_bg_image"] forState:UIControlStateNormal];
        _button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        _button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _button.accessibilityIdentifier = @"empty set button";
        [_button setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        _button.titleLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(17)];
        [_button addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}

- (void)didTapButton:(UIButton *)sender{
    if (self.reloadClickBlock) {
        self.reloadClickBlock();
    }
}
@end

@interface HDCoverSetLodingView : UIView
@property (nonatomic, strong) UIActivityIndicatorView *activityView;
@end

@implementation HDCoverSetLodingView

- (instancetype)init
{
    self =  [super init];
    if (self) {
        [self addSubview:self.activityView];
        [self setupConstraints];
    }
    return self;
}

- (void)setupConstraints
{

    [self.activityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    }];

}

- (UIActivityIndicatorView *)activityView
{
    if (!_activityView) {
        _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [_activityView startAnimating];
    }
    return _activityView;
}

- (void)didMoveToSuperview
{
    self.frame = self.superview.bounds;
    
    void(^fadeInBlock)(void) = ^{self.alpha = 1.0;};
    
    [UIView animateWithDuration:0.25
                     animations:fadeInBlock
                     completion:NULL];
    
}

@end


@interface HDCoverSetView : UIView

@property (nonatomic, copy) HDTMVoidBlock hd_emptyBtnClickBlock;

@property (nonatomic, copy) HDTMVoidBlock hd_errorBtnClickBlock;

@property (nonatomic, assign) HDTableViewCoverType hd_tableViewCoverType;

@property (nonatomic, strong) HDCoverSetEmptyView *coverEmptyView;

@property (nonatomic, strong) HDCoverSetErrorView *coverErrorView;

@property (nonatomic, strong) HDCoverSetLodingView *coverLoadingView;

@property (nonatomic, weak) __kindof UIView *curCoverView;

@property (nonatomic, assign) CGFloat verticalOffset;
@property (nonatomic, assign) CGFloat verticalSpace;

- (void)configCoverType:(HDTableViewCoverType)coverType;

@end

@implementation HDCoverSetView

#pragma mark - Initialization Methods

- (instancetype)init
{
    self =  [super init];
    if (self) {

    }
    return self;
}

- (void)configCoverType:(HDTableViewCoverType)coverType{
    if (_hd_tableViewCoverType!=coverType) {
        [self.curCoverView removeFromSuperview];
        _hd_tableViewCoverType = coverType;
        [self addSubview:self.curCoverView];
        [self.curCoverView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
}


#pragma mark - Getters

- (UIView *)curCoverView
{
    switch (self.hd_tableViewCoverType) {
        case HDTableViewCoverTypeNull:
        {
            
        }
            break;
        case HDTableViewCoverTypeEmpty:
        {
            _curCoverView = self.coverEmptyView;
        }
            break;
        case HDTableViewCoverTypeError:
        {
            _curCoverView = self.coverErrorView;
        }
            break;
        case HDTableViewCoverTypeLoading:
        {
            _curCoverView = self.coverLoadingView;
        }
            break;
        default:
            break;
    }
//    NSAssert(_curCoverView, @"========curCoverView是空的========");
    return _curCoverView;;
}

- (HDCoverSetLodingView *)coverLoadingView
{
    if (!_coverLoadingView) {
        _coverLoadingView = [HDCoverSetLodingView new];
        _coverLoadingView.backgroundColor = [UIColor clearColor];
        _coverLoadingView.userInteractionEnabled = YES;
        _coverLoadingView.alpha = 0;
    }
    return _coverLoadingView;
}

- (HDCoverSetEmptyView *)coverEmptyView
{
    if (!_coverEmptyView) {
        _coverEmptyView = [HDCoverSetEmptyView new];
        _coverEmptyView.backgroundColor = [UIColor whiteColor];
        _coverEmptyView.userInteractionEnabled = YES;
        _coverEmptyView.alpha = 0;
        HDWeakSelf;
        [_coverEmptyView setReloadClickBlock:^(){
            if (__weakSelf.hd_emptyBtnClickBlock) {
                __weakSelf.hd_emptyBtnClickBlock();
            }
        }];
    }
    return _coverEmptyView;
}

- (HDCoverSetErrorView *)coverErrorView
{
    if (!_coverErrorView) {
        _coverErrorView = [HDCoverSetErrorView new];
        _coverErrorView.backgroundColor = [UIColor clearColor];
        _coverErrorView.userInteractionEnabled = YES;
        _coverErrorView.alpha = 0;
        HDWeakSelf;
        [_coverErrorView setReloadClickBlock:^(){
            if (__weakSelf.hd_errorBtnClickBlock) {
                __weakSelf.hd_errorBtnClickBlock();
            }
        }];
    }
    return _coverErrorView;
}


- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *hitView = [super hitTest:point withEvent:event];
    
    // Return any UIControl instance such as buttons, segmented controls, switches, etc.
    if ([hitView isKindOfClass:[UIControl class]]) {
        return hitView;
    }
    
    // Return either the contentView or customView
    if ([hitView isEqual:_curCoverView]) {
        return hitView;
    }
    
    return nil;
}

@end

#pragma mark - UITableView + HDCover

@interface UITableView ()
@property (nonatomic, strong) HDCoverSetView *coverSetView;
@end


@implementation UITableView (HDCover)
/**
 *  GET&SET
 */
- (HDCoverSetView *)coverSetView
{
    HDCoverSetView *view = [self hd_getAssociatedObjectWithKey:_cmd];
    if (!view)
    {
        view = [HDCoverSetView new];
        view.backgroundColor = self.backgroundColor;
        view.hidden = YES;
        [self setCoverSetView:view];
    }
    return view;
}

-(void)setCoverSetView:(HDCoverSetView *)coverSetView{
    [self hd_setAssociatedRetainObject:coverSetView key:@selector(coverSetView)];
}

- (void)hd_coverType:(HDTableViewCoverType)coverType{
    if (coverType==HDTableViewCoverTypeNull) {
        [self.coverSetView removeFromSuperview];
        [self setCoverSetView:nil];
        self.scrollEnabled = YES;
    }else{
        [self.coverSetView configCoverType:coverType];
        if (!self.coverSetView.superview) {
//            [self insertSubview:self.coverSetView atIndex:0];
            [self.superview addSubview:self.coverSetView];
            [self.coverSetView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.equalTo(self);
                make.center.equalTo(self);
            }];
        }

        self.coverSetView.hidden = NO;
        self.coverSetView.clipsToBounds = YES;

        HDWeakSelf;
        [UIView performWithoutAnimation:^{
            HDStrongSelf;
            [self.coverSetView setNeedsLayout];
            [self.coverSetView layoutIfNeeded];
        }];
        self.scrollEnabled = NO;
    }
}

- (void) hd_coverLoading{
    self.hd_tableViewCoverType = HDTableViewCoverTypeLoading;
}

- (void) hd_coverEmpty{
    self.hd_tableViewCoverType = HDTableViewCoverTypeEmpty;
}

- (void) hd_coverError{
    self.hd_tableViewCoverType = HDTableViewCoverTypeError;
}

- (void) hd_coverDismiss{
    self.hd_tableViewCoverType = HDTableViewCoverTypeNull;
}
- (void)hd_coverEmpty:(HDTMVoidBlock)emptyBtnClickBlock{
    self.coverSetView.hd_emptyBtnClickBlock = emptyBtnClickBlock;
    [self hd_coverEmpty];
}

- (void)hd_coverError:(HDTMVoidBlock)errorBtnClickBlock{
    self.coverSetView.hd_errorBtnClickBlock = errorBtnClickBlock;
    [self hd_coverError];
}

- (void)yk_coverEmptyImage:(UIImage *)image content:(NSString *)content buttonTitle:(NSString *)title isShowButton:(BOOL)show
{
    if (image) {
        self.coverSetView.coverEmptyView.imageView.image = image;
    }
    if (content.length) {
        self.coverSetView.coverEmptyView.titleLabel.text = content;
    }
    if (title.length) {
        [self.coverSetView.coverEmptyView.button setTitle:title forState:UIControlStateNormal];
    }
    self.coverSetView.coverEmptyView.button.hidden = !show;
}

- (void)yk_coverErrorImage:(UIImage *)image content:(NSString *)content buttonTitle:(NSString *)title isShowButton:(BOOL)show
{
    if (image) {
        self.coverSetView.coverErrorView.imageView.image = image;
    }
    if (content.length) {
        self.coverSetView.coverErrorView.titleLabel.text = content;
    }
    if (title.length) {
        [self.coverSetView.coverErrorView.button setTitle:title forState:UIControlStateNormal];
    }
    self.coverSetView.coverErrorView.button.hidden = !show;
}

/**
 *  GET
 */

- (void)setHd_tableViewCoverType:(HDTableViewCoverType)hd_tableViewCoverType{
    if (self.hd_tableViewCoverType == hd_tableViewCoverType) {
        return;
    }
    [self hd_setAssociatedAssignObject:@(hd_tableViewCoverType) key:@selector(hd_tableViewCoverType)];
    [self hd_coverType:hd_tableViewCoverType];
}

- (HDTableViewCoverType)hd_tableViewCoverType
{
    // 注意，取出的是一个对象，不能直接返回
    id tmp = [self hd_getAssociatedObjectWithKey:_cmd];
    NSNumber *number = tmp;
    return number.unsignedIntegerValue;
}

- (HDTMVoidBlock)hd_errorBtnClickBlock{
    return self.coverSetView.hd_errorBtnClickBlock;
}

- (HDTMVoidBlock)hd_emptyBtnClickBlock{
    return self.coverSetView.hd_emptyBtnClickBlock;
}

- (void)setHd_errorBtnClickBlock:(HDTMVoidBlock)hd_errorBtnClickBlock{
   self.coverSetView.hd_errorBtnClickBlock = hd_errorBtnClickBlock;
}

- (void)setHd_emptyBtnClickBlock:(HDTMVoidBlock)hd_emptyBtnClickBlock{
    self.coverSetView.hd_emptyBtnClickBlock = hd_emptyBtnClickBlock;
}

@end
