//
//  YKRefreshFooter
//  dingniu
//
//  Created by hongyu on 2018/2/28.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKRefreshFooter.h"

@implementation YKRefreshFooter

- (void)prepare
{
    [super prepare];
    self.stateLabel.font             = [UIFont systemFontOfSize:adaptFontSize(13)];
    self.stateLabel.textColor        = [UIColor yk_colorWithHexString:@"#ADADAD"];
}

@end
