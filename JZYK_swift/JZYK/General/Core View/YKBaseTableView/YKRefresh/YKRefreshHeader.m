//
//  YKRefreshHeader
//  dingniu
//
//  Created by hongyu on 2018/2/28.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKRefreshHeader.h"

@implementation YKRefreshHeader

- (void)prepare
{
    [super prepare];
    self.lastUpdatedTimeLabel.hidden = YES;
    self.stateLabel.hidden           = YES;
    self.automaticallyChangeAlpha    = YES;
    [self setImages:[self prepareRefreshStatusWithStartPage:1  endPage:21  imageName:@"loading"] duration:1.5f forState:MJRefreshStateIdle];
    [self setImages:[self prepareRefreshStatusWithStartPage:22 endPage:32  imageName:@"loading"] duration:1.5f forState:MJRefreshStateWillRefresh];
    [self setImages:[self prepareRefreshStatusWithStartPage:33 endPage:63  imageName:@"loading"] duration:1.5f forState:MJRefreshStateRefreshing];
    self.height += IS_iPhoneX ? 20 : 0;
}

- (NSMutableArray *)prepareRefreshStatusWithStartPage:(NSInteger)startPage endPage:(NSInteger)endPage imageName:(NSString *)name
{
    NSMutableArray *imageArray = [NSMutableArray array];
    for (NSInteger i = startPage; i <= endPage; i++) {
        NSString *imgName = [NSString stringWithFormat:@"%@_%ld",name, i];
        UIImage *image = [UIImage imageNamed:imgName];
        if (image) {
            [imageArray addObject:image];
        }
    }
    return imageArray;
}



@end
