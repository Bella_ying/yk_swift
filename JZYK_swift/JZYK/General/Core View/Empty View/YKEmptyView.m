//
//  DNEmptyView.m
//  dingniu
//
//  Created by Jeremy Wang on 05/03/2018.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "YKEmptyView.h"

@interface YKEmptyView()

@end

@implementation YKEmptyView

+ (YKEmptyView *(^)(UIView *superView, YKEmptyViewCoverType type))dn_emptyViewReloadClick:(void_block_t)reloadAction
{
    return ^(UIView *superView, YKEmptyViewCoverType type){
        if (!superView || ![superView isKindOfClass:[UIView class]]) {
            NSAssert(NO, @"superview must set");
        }
        YKEmptyView *emptyView = [YKEmptyView new];
        emptyView.backgroundColor = [UIColor yk_colorWithHexString:@"#F5F5F7"];
        emptyView.frame = superView.bounds;
        emptyView.emptyViewCoverType = type;
        [superView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[YKEmptyView class]]) {
                [obj removeFromSuperview];
            }
        }];
        [superView addSubview:emptyView];
        [emptyView setReloadClickBlock:^{
            if (reloadAction) {
                reloadAction();
            }
        }];
        return emptyView;
    };
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUpEmptyView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.emptyImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        if (self.isRefresh) {
            make.centerY.offset(80);
            make.centerX.offset(0);
        }
    }];
}

- (void)setUpEmptyView
{

    UIImageView *imageView = [UIImageView new];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100 * ASPECT_RATIO_WIDTH, 100 * ASPECT_RATIO_WIDTH));
        make.centerX.offset(0);
        make.centerY.offset(-80);
    }];
    self.emptyImageView = imageView;
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = [UIFont systemFontOfSize:13.0];
    titleLabel.textColor = [UIColor yk_colorWithHexString:@"#ADADAD"];
    titleLabel.text = @"没有数据！您可以尝试重新获取";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.equalTo(imageView.mas_bottom).offset(3);
       
    }];
    self.titleLabel = titleLabel;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.titleLabel.font = [UIFont systemFontOfSize:18];
    [button setTitleColor:[UIColor yk_colorWithHexString:@"#E22D2D" ] forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    button.layer.cornerRadius = 25;
    button.layer.masksToBounds = YES;
    button.layer.borderColor = MAIN_THEME_COLOR.CGColor;
    button.layer.borderWidth = 1;
    [button setTitle:@"重新加载" forState:UIControlStateNormal];
    [button setTitleColor:MAIN_THEME_COLOR forState:UIControlStateNormal];
    [button addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.equalTo(titleLabel.mas_bottom).offset(33);
        make.size.mas_equalTo(CGSizeMake(183, 45));
    }];
    
    self.emptyButton = button;
    
}

- (void)didMoveToSuperview
{
    self.frame = self.superview.bounds;
    
    void(^fadeInBlock)(void) = ^{self.alpha = 1.0;};
    
    [UIView animateWithDuration:0.25
                     animations:fadeInBlock
                     completion:NULL];
    
}

- (void)didTapButton:(UIButton *)sender{
    [self removeFromSuperview];
    if (self.reloadClickBlock) {
        self.reloadClickBlock();
    }
}

- (void)setEmptyViewCoverType:(YKEmptyViewCoverType)emptyViewCoverType
{
    [self.emptyButton setTitle:@"点击刷新" forState:UIControlStateNormal];
     self.emptyButton.hidden = YES;
    switch (emptyViewCoverType) {
        case YKEmptyViewCoverTypeNoNetwork:
        {
            self.titleLabel.text = @"检查网络连接~";
            self.emptyImageView.image = [UIImage imageNamed:@"img_wuwifi"];;
            self.emptyButton.hidden = NO;
        }
            break;
        case YKEmptyViewCoverTypeNoShop:
        {
            self.emptyImageView.image = [UIImage imageNamed:@"img_wushop"];
            self.titleLabel.text = @"暂无商品哦～";
            self.emptyButton.hidden = YES;
        }
            break;
        case YKEmptyViewCoverTypeNoCoupon:
        {
            self.emptyImageView.image = [UIImage imageNamed:@"ticket_no"];
            self.titleLabel.text = @"暂无优惠券～";
            self.emptyButton.hidden = YES;
        }
            break;
       
        default:
            break;
    }
}


@end
