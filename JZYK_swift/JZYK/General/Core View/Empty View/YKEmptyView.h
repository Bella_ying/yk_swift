//
//  DNEmptyView.h
//  dingniu
//
//  Created by Jeremy Wang on 05/03/2018.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger,YKEmptyViewCoverType) {
    YKEmptyViewCoverTypeNoNetwork,        //无网络
    YKEmptyViewCoverTypeNoShop,        //无商品
    YKEmptyViewCoverTypeNoCoupon,     //无优惠券
    
};

@interface YKEmptyView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *emptyImageView;
@property (nonatomic, strong) UIButton *emptyButton;
@property (nonatomic, copy) void_block_t reloadClickBlock;
@property (nonatomic, ) YKEmptyViewCoverType emptyViewCoverType;
@property (nonatomic, ) BOOL isRefresh;

+ (YKEmptyView *(^)(UIView *superView, YKEmptyViewCoverType type))dn_emptyViewReloadClick:(void_block_t)reloadAction;
@end

