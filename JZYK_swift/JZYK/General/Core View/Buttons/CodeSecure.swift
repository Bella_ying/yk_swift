//
//  CodeSecure.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/24.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import Foundation
import CryptoSwift

private let publicKey = "jsqb#666*5"

class CodeSecure: NSObject {
    @objc static func md5Encrypt(phoneNum num: String?,
                           encryptedCallBack: @escaping(String, String)->Void) {
        guard let num = num else { return }
        HTTPManager.session.getRequest(forKey: kCreditAppRandom, succeed: { (json, code, unwrapNormal, msg) in
            if code == 0 && unwrapNormal{
                if let random = json["random"] as? String {
                    let sign = "\(num)\(random)\(publicKey)"
                    let md5Sigh = sign.md5()
                    encryptedCallBack(md5Sigh, random)
                }
            }
        }) { (errMsg, isConnect) in
            
        }
    }
    
    @objc static func codeSend(param: Dictionary<String, Any>?, _ button: Button? = nil) {
        HTTPManager.session.postRequest(forKey: kUserQuickLoginCode, param: param, succeed: { (json, code, unwrapNormal, msg) in
            if code == 0 {
            }
            iToast.makeText(msg).show()
        }) { (errMsg, isConnect) in
            
        }
    }
}

extension CodeSecure {
    //绑卡验证码
    @objc static func bindCardCodeSend(param: Dictionary<String, Any>? ,sendComplete:((Bool) -> (Void))?) {
        HTTPManager.session.postRequest(forKey: kCreditCardGetCode, param: param, succeed: { (json, code, unwrapNormal, msg) in
            if code == 0{
                sendComplete?(true)
            }
            iToast.makeText(msg).show()
        }) { (errMsg, isConnect) in
        }
    }
    
    //快借登录界面
    @objc static func quickLoginCodeSend(param: Dictionary<String, Any>?, _ button: Button? = nil) {
        HTTPManager.session.postRequest(forKey: kUserQuickLoginCode, param: param, succeed: { (json, code, unwrapNormal, msg) in
            if code == 0 {
                
            }
            iToast.makeText(msg).show()
        }) { (errMsg, isConnect) in
            
        }
    }
    
    //找回登录密码界面，点击获取按钮调用获得验证码
    @objc static func resetPwdCodeSend(param: Dictionary<String, Any>?, _ button: Button? = nil) {
        HTTPManager.session.postRequest(forKey: kUserResetPwdCode, param: param, succeed: { (json, code, unwrapNormal, msg) in
            if code == 0 {
                
            }
            iToast.makeText(msg).show()
        }) { (errMsg, isConnect) in
            
        }
    }
    
    @objc static func registCodeSend(param: Dictionary<String, Any>?, _ button: Button? = nil) {
        HTTPManager.session.postRequest(forKey: KUserRegGetCode, param: param, succeed: { (json, code, unwrapNormal, msg) in
            if code == 0 {
                
            }
            iToast.makeText(msg).show()
        }) { (errMsg, isConnect) in
            
        }
    }
    
}



