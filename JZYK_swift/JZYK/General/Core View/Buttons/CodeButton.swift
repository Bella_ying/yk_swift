//
//  CodeButton.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/21.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Dispatch

enum CodeType {
    case quckLogin  //快捷登录
    case resetPwd   //重置登录密码
    case resetTransaction //重置交易密码
    case registCode //注册
}

class CodeButton: UIButton {

    static var code: CodeButton {
        get {
            return Bundle.main.loadNibNamed("CodeButton", owner: self, options: nil)?.last as! CodeButton
        }
    }
    
    var codeAction: ((UIButton)->String?)?
    public var codeType: CodeType?
    
    override func draw(_ rect: CGRect) {
        self.addTarget(self, action:#selector(click(item:)), for:.touchDown)
    }
    
    @objc private func click(item: UIButton){
        item.isSelected = !item.isSelected
        let num = self.codeAction?(item)
        guard let phone = num, phone.count == 11 else {
            iToast.makeText("请输入11位的手机号码").show();
            return
        }
        CodeButton.timerCountDown(forButton: item, title: "秒") { }
        CodeSecure.md5Encrypt(phoneNum: phone, encryptedCallBack: { (sign, random) in
            var dict: Dictionary<String, Any> = ["phone": phone,
                                                 "sign": sign,
                                                 "random": random]
            guard let type = self.codeType else { return }
            switch type {
            case .quckLogin:
                CodeSecure.quickLoginCodeSend(param: dict);
            case .resetPwd:
                dict = ["phone": phone,
                        "sign": sign,
                        "random": random,
                        "type":"find_pwd"]
                CodeSecure.resetPwdCodeSend(param: dict);
                
            case .resetTransaction:
                dict = ["phone": phone,
                        "sign": sign,
                        "random": random,
                        "type":"find_pay_pwd"]
                CodeSecure.resetPwdCodeSend(param: dict);
            case .registCode:
                CodeSecure.registCodeSend(param: dict);
            }
        })
        
        
    }
    
   public func switchButtonStatus(_ status: Bool) {
        if status {
            self.layer.borderColor = Color.main.cgColor
            self.setTitleColor(Color.main, for: .normal)
            self.isEnabled = true;
        } else {
            self.setTitleColor(Color.color_DC_C9, for: .normal)
            self.layer.borderColor = Color.color_CC_C8.cgColor
            self.isEnabled = false;
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupButton()
    }
    
    private func setupButton() {
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = true;
        self.layer.borderColor = Color.main.cgColor
        self.layer.borderWidth = 1;
        self.isEnabled = true;
        self.setTitle("获取", for: .normal)
        self.setTitleColor(Color.main, for: .normal)
    }
    
    @objc static func timerCountDown(forButton btn: UIButton,
                             elapsedTime: Int = 60,
                               title: String = "",
                              finish: @escaping()->Void) {
        if !btn.isEnabled { return }
        btn.setTitleColor(Color.color_DC_C9, for: .normal)
        btn.layer.borderColor = Color.color_CC_C8.cgColor
        
        var timeout: Int = elapsedTime
        let timer = DispatchSource.makeTimerSource() as! DispatchSource
        //倒计时时间
        timer.schedule(wallDeadline: .now(), repeating: 1.0)
        timer.setEventHandler {
            //每秒执行
            if timeout <= 0 {
                //倒计时结束，关闭
                timer.cancel()
                DispatchQueue.main.async(execute: {() -> Void in
                    btn.isEnabled = true
                    btn.layer.borderColor = Color.main.cgColor
                    btn.setTitleColor(Color.main, for: .normal)
                    btn.setTitle("重新发送", for: .normal)
                    //设置界面的按钮显示 根据自己需求设置
                    finish()
                })
            } else {
                let seconds: Int = timeout % (elapsedTime + 1)
                let secondString = String(format: "%.2d", seconds)
                DispatchQueue.main.async(execute: {() -> Void in
                    btn.isEnabled = false
                    //设置动画
                    UIView.beginAnimations(nil, context: nil)
                    UIView.setAnimationDuration(1)
                    let btnTitle = title.isEmpty ? "\(secondString)" : "\(secondString)\(title)"
                    btn.setTitle(btnTitle, for: .normal)
                    UIView.commitAnimations()
                })
                timeout -= 1
            }
        }
        timer.resume()
    }
}



