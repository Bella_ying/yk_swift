//
//  Button.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/21.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

@objcMembers class Button: UIButton {
    static var main: Button {
        get {
            return Bundle.main.loadNibNamed("Button", owner: self, options: nil)?.last as! Button
        }
    }
    @objc public var onClick: ((UIButton)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupButton()
    }
    
    override func draw(_ rect: CGRect) {
        self.addTarget(self, action:#selector(click(item:)), for:.touchDown)
    }
    
    @objc private func click(item: UIButton){
        self.superview?.endEditing(true)
        self.onClick?(item)
    }
    
    private func setupButton() {
        self.backgroundColor = UIColor.clear;
        self.setBackgroundImage(#imageLiteral(resourceName: "btn_bg_image"), for: .normal);
        self.setTitleColor(UIColor.white, for: .normal);
    }
    
    public func switchButtonStatus(_ status: Bool) {
        if status {
            self.setBackgroundImage(#imageLiteral(resourceName: "btn_bg_image"), for: .normal);
             self.isEnabled = true;
        } else {
            self.setBackgroundImage(#imageLiteral(resourceName: "btn_disabled_bg_image"), for: .normal)
            self.isEnabled = false;
        }
    }
}
