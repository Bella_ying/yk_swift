//
//  JWPickerView.h
//  PickerView
//
//  Created by Jeremy Wang on 29/09/2017.
//  Copyright © 2017 Jeremy.L.Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, JWPickerViewType) {
    JWPickerViewTypeSingleComponent,
    JWPickerViewTypeMultipleComponent,
    JWPickerViewTypeRelevantComponent
};

@interface JWToolBar : UIView

@end

@interface JWSingleComponentPicker : UIView

@end


@interface JWMultipleComponentPicker : UIView

@end


@interface JWRelevantPicker : UIView

@end


@interface JWDatePickerPattern : NSObject

@property (nonatomic, strong) NSDate * _Nonnull date;
@property (nonatomic, strong) NSDate * _Nonnull minDate;
@property (nonatomic, strong) NSDate * _Nonnull maxDate;
@property (nonatomic, strong) NSLocale * _Nonnull locale;
@property (nonatomic, ) UIDatePickerMode pickerMode;

@end


@interface JWDatePicker : UIView

@end


@interface JWPickerView : UIView

+ (JWPickerView *_Nonnull)pickerWithToolBarTitle:(NSString *_Nullable)tittle
                                      pickerType:(JWPickerViewType)pickerType
                                            data:(NSArray * _Nonnull)data
                                  cancleComplete:(void(^_Nullable)(void))cancleComplete
                                 confirmComplete:(void(^_Nullable)(id _Nonnull selectedIndex, id _Nonnull selectedData))confirmComplete;

+ (JWPickerView *_Nonnull)datePickerWithToolBarTittle:(NSString *_Nullable)tittle
                                              pattern:(JWDatePickerPattern *_Nullable)pattern
                                       cancleComplete:(void(^_Nullable)(void))cancleComplete
                                      confirmComplete:(void(^_Nullable)(NSDate *_Nonnull))confirmComplete;

+ (JWPickerView *_Nonnull)cityPickerCancleComplete:(void(^_Nullable)(void))cancleComplete
                           confirmComplete:(void(^_Nullable)(id _Nonnull selectedIndex, id _Nonnull selectedData))confirmComplete;

@end
