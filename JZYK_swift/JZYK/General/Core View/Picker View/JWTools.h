//
//  JWTools.h
//  PickerView
//
//  Created by wangjeremy on 28/09/2017.
//  Copyright © 2017 Jeremy.L.Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, JWDateMode) {
    JWDateModeDate, //年月日
    JWDateModeTime, //时分秒
    JWDateModeCountDownTimer,
    JWDateModeDateAndTime
};

#define WEAK_SELF     __weak typeof(self) weakSelf = self;
#define STRONG_SELF __strong typeof(weakSelf) strongSelf = weakSelf;

#define HEIGHT_OF_SCREEN    [[UIScreen mainScreen] bounds].size.height
#define WIDTH_OF_SCREEN     [[UIScreen mainScreen] bounds].size.width

static NSString *const kChinaMainland = @"zh_CN";
static CGFloat const kToolBarHeight = 44.0f;
static const CGFloat kPickerViewHeight = 260.0f;

@interface UIView (JWLayout)

- (NSLayoutConstraint *(^)(id))jw_left;
- (NSLayoutConstraint *(^)(id))jw_right;
- (NSLayoutConstraint *(^)(id))jw_bottom;
- (NSLayoutConstraint *(^)(id))jw_top;
- (NSLayoutConstraint *(^)(id,CGFloat))jw_height;

- (CGRect (^)(CGFloat, CGFloat, CGFloat))jw_frame;

- (UILabel *(^)(NSString *))jw_label;
- (UIButton *(^)(SEL, NSString *, UIColor *))button;


@end


@interface JWTools : NSObject

void ifDebug(void(^void_block)());

+ (NSString *(^)(NSDate *current, JWDateMode dateMode))dateDescription;

+ (NSString *)unicodeToChinese:(NSString *)unicodeStr;

@end
