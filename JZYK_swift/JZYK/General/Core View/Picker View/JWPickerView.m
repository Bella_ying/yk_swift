//
//  JWPickerView.m
//  PickerView
//
//  Created by Jeremy Wang on 29/09/2017.
//  Copyright © 2017 Jeremy.L.Wang. All rights reserved.
//

#import "JWPickerView.h"
#import "JWTools.h"

typedef void(^ValueDidChanged_t)(id);
typedef void(^DateValueChanged_t)(NSDate *);

static CGFloat const kToolBarOffset = 10.0f;
static CGFloat const kToolBarButtonWidth = 80.0f;
static CGFloat const kTooBarLineHeight = 1.0f;

#define ToolBar_Subview_Height (self.bounds.size.height - kTooBarLineHeight)
#define ToolBar_Width self.bounds.size.width


@interface JWToolBar ()

@property (nonatomic, strong) UIColor *textColor; //默认是黑色
@property (nonatomic, copy) void(^confirmComplete)(void);
@property (nonatomic, copy) void(^cancleComplete)(void);

@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) UIButton *cancleButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation JWToolBar

- (instancetype)initWithToolBarTitle:(NSString *)toolBarTittle
                           textColor:(UIColor *)textColor
                     backGroundColor:(UIColor *)bgColor
                      cancleComplete:(void(^)(void))cancleComplete
                     confirmComplete:(void(^)(void))confirmComplete {
    if (self = [super init]) {
        self.confirmComplete = confirmComplete;
        self.cancleComplete = cancleComplete;
        self.titleLabel.text = toolBarTittle;
        self.textColor = textColor? : [UIColor blackColor];
        self.backgroundColor = bgColor ? : [UIColor whiteColor];
        [self addSubview:self.doneButton];
        [self addSubview:self.cancleButton];
        [self addSubview:self.titleLabel];
        [self addSubview:self.lineView];
    }
    return self;
}

#pragma mark Lazy methods
- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = self.button(@selector(confirmButtonClick),@"确定",self.textColor);
    }
    return _doneButton;
}

- (UIButton *)cancleButton {
    if (!_cancleButton) {
        _cancleButton = self.button(@selector(cancleButtonClick),@"取消",self.textColor);
    }
    return _cancleButton;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = self.textColor;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    }
    return _titleLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = self.textColor;
    }
    return _lineView;
}

- (void)confirmButtonClick {
    if (self.confirmComplete) {
        self.confirmComplete();
    }
}

- (void)cancleButtonClick {
    if (self.cancleComplete) {
        self.cancleComplete();
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.cancleButton.frame = self.jw_frame(kToolBarOffset,kToolBarButtonWidth,ToolBar_Subview_Height);
    
    self.doneButton.frame = self.jw_frame(ToolBar_Width - kToolBarOffset - kToolBarButtonWidth,kToolBarButtonWidth,ToolBar_Subview_Height);
    
    self.titleLabel.frame = self.jw_frame(CGRectGetMaxX(self.cancleButton.frame) + kToolBarOffset,ToolBar_Width - (CGRectGetMaxX(self.cancleButton.frame) + kToolBarOffset) - 2*kToolBarOffset - kToolBarButtonWidth,ToolBar_Subview_Height);
    
    self.lineView.frame = CGRectMake(0, ToolBar_Subview_Height, ToolBar_Width, kTooBarLineHeight);
}

@end

/*********************************************************************/

@interface JWSingleComponentPicker ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, copy) NSArray<NSString *> *data;
@property (nonatomic, copy) ValueDidChanged_t valueDidChangeHandler;
@property (nonatomic, strong) JWToolBar *toolBar;
@property (nonatomic, strong) UIPickerView *pickerView;

@property (nonatomic, strong) NSNumber *selectedIndex;
@property (nonatomic, copy) NSString *selectedData;

@end

@implementation JWSingleComponentPicker

- (instancetype)initWithToolBarTitle:(NSString *)tittle
                        defaultIndex:(NSInteger)defaultIndex
                                data:(NSArray<NSString *> * )data
               valueDidChangeHandler:(ValueDidChanged_t ) valueDidChangeHandler
                      cancleComplete:(void(^)(void))cancleComplete
                     confirmComplete:(void(^)(NSNumber *, NSString *))confirmComplete
{
    if (self = [super init]) {
        if (!data || ![data isKindOfClass:[NSArray class]]) {
            ifDebug(^{
                NSAssert(NO, @"数据格式不符，初始化未完成");
            });
            return nil;
        }
        
        self.data = data;
        self.valueDidChangeHandler = valueDidChangeHandler;
        
        WEAK_SELF
        self.toolBar = [[JWToolBar alloc] initWithToolBarTitle:tittle textColor:MAIN_THEME_COLOR backGroundColor:nil cancleComplete:cancleComplete confirmComplete:^{
            if (confirmComplete) {
                confirmComplete(weakSelf.selectedIndex, weakSelf.selectedData);
            }
        }];
        
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.pickerView];
        [self addSubview:self.toolBar];
        self.toolBar.textColor = MAIN_THEME_COLOR;
        
        if (defaultIndex < 0 || defaultIndex >= data.count) {
            ifDebug(^{
                DLog(@"默认数值不合法，可能造成数组越界，重置为0");
            });
            defaultIndex = 0;
        }
        self.selectedIndex = @(defaultIndex);
        
        [self valueDidChange];
        //起始位置
        [self.pickerView selectRow:[self.selectedIndex integerValue] inComponent:0 animated:NO];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:@[self.jw_left(self.toolBar), self.jw_right(self.toolBar), self.jw_height(self.toolBar,kToolBarHeight), self.jw_top(self.toolBar)]];
    
    self.pickerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:@[self.jw_left(self.pickerView), self.jw_right(self.pickerView), self.jw_height(self.pickerView,self.bounds.size.height - kToolBarHeight), self.jw_bottom(self.pickerView)]];
}

- (void)valueDidChange {
    if (self.valueDidChangeHandler) {
        self.valueDidChangeHandler(self.selectedData);
    }
}

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [UIPickerView new];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
    }
    return _pickerView;
}

#pragma mark lazy method
- (NSString *)selectedData {
    if ([_selectedIndex integerValue] >= 0 || [_selectedIndex integerValue] < self.data.count) {
        _selectedData = self.data[[self.selectedIndex integerValue]];
    }
    return _selectedData;
}


#pragma mark UIPickerViewDelegate UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.data.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedIndex = @(row);
    [self valueDidChange];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    return self.jw_label(self.data[row]);
}
@end

/*********************************************************************/

@interface JWMultipleComponentPicker ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, copy)NSArray<NSArray<NSString *> *> * _Nonnull data;
@property (nonatomic, copy) ValueDidChanged_t valueDidChangeHandler;
@property (nonatomic, strong) JWToolBar *toolBar;
@property (nonatomic, strong) UIPickerView *pickerView;

@property (nonatomic, strong) NSMutableArray *selectedIndexes;
@property (nonatomic, copy) NSArray *selectedValues;

@end

@implementation JWMultipleComponentPicker

- (instancetype _Nullable )initWithToolBarTitle:(NSString *)tittle
                                 defaultIndexes:(NSArray *)defaultIndexes
                                           data:(NSArray<NSArray<NSString *> *> * )data
                          valueDidChangeHandler:(ValueDidChanged_t  ) valueDidChangeHandler
                                 cancleComplete:(void(^)(void))cancleComplete
                                confirmComplete:(void(^)(NSArray *, NSArray<NSString *> *))confirmComplete
{
    self = [super init];
    if (self) {
        
        if (![self isDataInTheCorrectFormat:data]) {
            ifDebug(^{
                NSAssert(NO, @"数据格式不符，初始化未完成");
            });
            return nil;
        }
        
        self.data = data;
        self.valueDidChangeHandler = valueDidChangeHandler;
        
        WEAK_SELF
        self.toolBar = [[JWToolBar alloc] initWithToolBarTitle:tittle textColor:MAIN_THEME_COLOR backGroundColor:nil cancleComplete:cancleComplete confirmComplete:^{
            if (confirmComplete) {
                confirmComplete(weakSelf.selectedIndexes,weakSelf.selectedValues);
            }
        }];
        
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.pickerView];
        [self addSubview:self.toolBar];
        
        self.selectedIndexes = [defaultIndexes mutableCopy];
        
        [self valueDidChange];
        
        [self setPickerViewPosition];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:@[self.jw_left(self.toolBar), self.jw_right(self.toolBar), self.jw_height(self.toolBar,kToolBarHeight), self.jw_top(self.toolBar)]];
    
    self.pickerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:@[self.jw_left(self.pickerView), self.jw_right(self.pickerView), self.jw_height(self.pickerView,self.bounds.size.height - kToolBarHeight), self.jw_bottom(self.pickerView)]];
}

- (void)valueDidChange
{
    if (self.valueDidChangeHandler) {
        self.valueDidChangeHandler(self.selectedValues);
    }
}

#pragma mark 数据格式是否正确
- (BOOL)isDataInTheCorrectFormat:(id)data
{
    if(data && [data isKindOfClass:[NSArray class]]){
        for (id object in data) {
            if ([object isKindOfClass:[NSArray class]]) {
                return YES;
            }
        }
    }
    return NO;
}

- (UIPickerView *)pickerView
{
    if (!_pickerView) {
        _pickerView = [UIPickerView new];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
    }
    return _pickerView;
}

- (void)setSelectedIndexes:(NSMutableArray *)selectedIndexes {
    NSMutableArray *tempData = [NSMutableArray array];
    if (!selectedIndexes || !selectedIndexes.count) {
        for (id object in self.data) {
            [tempData addObject:@0];//用0补足
        }
    }else if (selectedIndexes.count < self.data.count){//默认值的个数<列的个数时
        tempData = selectedIndexes;
        NSInteger count = self.data.count-selectedIndexes.count;
        for (NSInteger idx = 0; idx < count-1; idx++) {
            [tempData addObject:@0];////用0补足
        }
    }else if (selectedIndexes.count > self.data.count){//默认值的个数>列的个数时
        tempData = selectedIndexes;
        NSInteger count = selectedIndexes.count - self.data.count;
        for (NSInteger idx = 0; idx < count-1; idx++) {
            [tempData removeLastObject];
        }
    }else {//相等，判断下标是否一致
        tempData = selectedIndexes;
        [selectedIndexes enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger defaultIndex = [obj integerValue];
            NSArray *values = self.data[idx];
            if (defaultIndex < 0 || defaultIndex >= values.count) {
                [tempData replaceObjectAtIndex:idx withObject:@0];
            }
        }];
    }
    _selectedIndexes = tempData;
}

- (NSArray *)selectedValues {
    NSMutableArray *tempArray = [@[] mutableCopy];
    [self.selectedIndexes enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSArray *values = [self.data objectAtIndex:idx];
        NSUInteger defaultIndex = [obj integerValue];
        [tempArray addObject:[values objectAtIndex:defaultIndex]];
    }];
    return [tempArray copy];
}


-(void)setPickerViewPosition {
    [self.selectedIndexes enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.pickerView selectRow:[obj integerValue] inComponent:idx animated:YES];
    }];
}

#pragma mark UIPickerViewDelegate UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return self.data.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.data[component].count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self.selectedIndexes replaceObjectAtIndex:component withObject:@(row)];
    [self valueDidChange];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    return self.jw_label(self.data[component][row]);
}

@end

/*********************************************************************/

@interface JWRelevantPicker ()<UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, copy) NSArray *data;
@property (nonatomic, copy) ValueDidChanged_t valueDidChangeHandler;
@property (nonatomic, strong) JWToolBar *toolBar;
@property (nonatomic, strong) UIPickerView *pickerView;

@property (nonatomic, strong) NSMutableArray<NSString *> *selectedValues;

@end

@implementation JWRelevantPicker

- (instancetype _Nullable )initWithToolBarTitle:(NSString *)tittle
                                  defaultValues:(NSArray *)defaultValues
                                           data:(NSArray<NSArray<NSString *> *> * )data
                          valueDidChangeHandler:(ValueDidChanged_t ) valueDidChangeHandler
                                 cancleComplete:(void(^)(void))cancleComplete
                                confirmComplete:(void(^)(NSArray *))confirmComplete {
    self = [super init];
    if (self) {
        if(![self isRelevantDataInTheCorrectFormat:data]){
            ifDebug(^{
                NSAssert(NO, @"数据格式不符，初始化未完成");
            });
        }
        
        _data = data;
        self.valueDidChangeHandler = valueDidChangeHandler;
        
        WEAK_SELF
        self.toolBar = [[JWToolBar alloc] initWithToolBarTitle:tittle textColor:MAIN_THEME_COLOR backGroundColor:nil cancleComplete:cancleComplete confirmComplete:^{
            if (confirmComplete) {
                confirmComplete(weakSelf.selectedValues);
            }
        }];
        
        [self addSubview:self.toolBar];
        [self addSubview:self.pickerView];
        self.backgroundColor = [UIColor whiteColor];
        
        self.selectedValues = [defaultValues mutableCopy];
        [self valueDidChange];
        [self setPickerViewPosition];
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:@[self.jw_left(self.toolBar), self.jw_right(self.toolBar), self.jw_height(self.toolBar,kToolBarHeight), self.jw_top(self.toolBar)]];
    
    self.pickerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:@[self.jw_left(self.pickerView), self.jw_right(self.pickerView), self.jw_height(self.pickerView,self.bounds.size.height - kToolBarHeight), self.jw_bottom(self.pickerView)]];
}

- (void)valueDidChange {
    if (self.valueDidChangeHandler) {
        self.valueDidChangeHandler(self.selectedValues);
    }
}

#pragma mark 数据格式是否正确
- (BOOL)isRelevantDataInTheCorrectFormat:(NSArray *)data {
    if(data && data.count){
        __block BOOL correct = NO;
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            correct = !idx ? [obj isKindOfClass:[NSArray class]] : [obj isKindOfClass:[NSDictionary class]];
            if (!correct) *stop = YES;
        }];
        return correct;
    }
    return NO;
}

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [UIPickerView new];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
    }
    return _pickerView;
}

- (void)setSelectedValues:(NSMutableArray<NSString *> *)selectedValues {
    NSMutableArray *tempData = [NSMutableArray array];
    if (selectedValues.count < self.data.count){//默认值的个数<列的个数时
        tempData = selectedValues;
        NSInteger count = self.data.count-selectedValues.count;
        for (NSInteger idx = 0; idx < count-1; idx++) {
            [tempData addObject:@""];//用空字符补足
        }
    }else if (selectedValues.count > self.data.count){//默认值的个数>列的个数时
        tempData = selectedValues;
        NSInteger count = selectedValues.count - self.data.count;
        for (NSInteger idx = 0; idx < count-1; idx++) {
            [tempData removeLastObject];//移除多余
        }
    }else {//相等，判断下标是否一致
        tempData = selectedValues;
        _selectedValues = tempData;
        [selectedValues enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            [self relevantDataHandler:^(NSArray *tempArray) {
                if (tempArray && tempArray.count) {
                    [tempData replaceObjectAtIndex:idx withObject:tempArray.firstObject];
                }
            }](idx);
            
        }];
    }
    _selectedValues = tempData;
}

-(void)setPickerViewPosition {
    [self.selectedValues enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        __block NSInteger row = 0;
        
        [self relevantDataHandler:^(NSArray *tempArray) {
            if (tempArray && tempArray.count) {
                row = [tempArray indexOfObject:obj];
            }
        }](idx);
        
        [self.pickerView selectRow:row inComponent:idx animated:YES];
    }];
}

#pragma mark UIPickerViewDelegate, UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return self.data.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    __block NSInteger count = 0;
    
    [self relevantDataHandler:^(NSArray *tempArray) {
        if (tempArray && tempArray.count) {
            count = tempArray.count;
        }
    }](component);
    
    return count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    [self relevantDataHandler:^(NSArray *tempArray) {
        if (!tempArray || !tempArray.count) {
            for (NSUInteger idx = component; idx < self.selectedValues.count; idx++) {
                [self.selectedValues replaceObjectAtIndex:component withObject:@""];
            }
        }else{
            [self.selectedValues replaceObjectAtIndex:component withObject:[tempArray objectAtIndex:row]];
        }
    }](component);
    
    if (component < self.data.count-1) {
        [pickerView reloadComponent:component+1];
        [self pickerView:pickerView didSelectRow:0 inComponent:component+1];
        [pickerView selectRow:0 inComponent:component+1 animated:YES];
    }else{
        [self valueDidChange];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    __block NSString *tittle = @"";
    
    [self relevantDataHandler:^(NSArray *tempArray) {
        if (tempArray && tempArray.count) {
            tittle = [tempArray objectAtIndex:row];
        }
    }](component);
    
    return self.jw_label(tittle);
}

#pragma mark 通过给定的component的值，返回所需要的每个component所对应的数组并进行处理
- (void(^)(NSUInteger idx))relevantDataHandler:(void(^)(NSArray *tempArray))handler{
    return ^(NSUInteger idx){
        NSArray *tempArray = nil;
        if (!idx) {
            tempArray = (NSArray *)self.data.firstObject;
        }else{
            tempArray = [self rowDataAtIndex:idx];
        }
        if (handler) {
            handler(tempArray);
        }
    };
}

#pragma mark 根据索引寻找相对应的没组的数据
- (NSArray *)rowDataAtIndex:(NSUInteger)index {
    NSDictionary *dict = (NSDictionary *)[self.data objectAtIndex:index];
    NSString *key = self.selectedValues[index-1];    //上一列选中值
    NSArray *nextRowsData = [dict objectForKey:key]; //上一列选中值对应的这一列的数据
    return nextRowsData;
}

@end

/*********************************************************************/

@implementation JWDatePickerPattern

- (instancetype)init {
    self = [super init];
    if (self) {
        self.date = [NSDate date];
        self.pickerMode = UIDatePickerModeDate;
        self.locale = [NSLocale localeWithLocaleIdentifier:kChinaMainland];
    }
    return self;
}

@end

@interface JWDatePicker ()

@property (nonatomic, strong) JWToolBar *toolBar;
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) JWDatePickerPattern *pattern;
@property (nonatomic, copy) DateValueChanged_t valueDidChangeHandler;
@property (nonatomic, strong) UIDatePicker *datePicker;
@end

@implementation JWDatePicker

- (instancetype)initWithToolBarTittle:(NSString *)tittle
                              pattern:(JWDatePickerPattern *)pattern
                valueDidChangeHandler:(DateValueChanged_t )valueDidChangeHandler
                       cancleComplete:(void(^)(void))cancleComplete
                      confirmComplete:(void(^)(NSDate *))confirmComplete
{
    self = [super init];
    if (self) {
        self.selectedDate = [NSDate date];
        self.pattern = pattern;
        self.valueDidChangeHandler = valueDidChangeHandler;
        
        WEAK_SELF
        self.toolBar = [[JWToolBar alloc] initWithToolBarTitle:tittle textColor:MAIN_THEME_COLOR backGroundColor:nil cancleComplete:cancleComplete confirmComplete:^{
            if (confirmComplete) {
                confirmComplete(weakSelf.selectedDate);
            }
        }];
        
        if (self.valueDidChangeHandler) {
            self.valueDidChangeHandler(self.selectedDate);
        }
        
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.toolBar];
        [self addSubview:self.datePicker];
    }
    return self;
}

#pragma mark lazy method
- (UIDatePicker *)datePicker {
    if (!_datePicker) {
        _datePicker = [UIDatePicker new];
        [_datePicker addTarget:self action:@selector(datePicked:) forControlEvents:UIControlEventValueChanged];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        _datePicker.locale = [NSLocale localeWithLocaleIdentifier:kChinaMainland];
        if (self.pattern) {
            _datePicker.date = self.pattern.date;
            _datePicker.minimumDate = self.pattern.minDate;
            _datePicker.maximumDate = self.pattern.maxDate;
            _datePicker.datePickerMode = self.pattern.pickerMode;
            _datePicker.locale = self.pattern.locale;
            self.selectedDate = self.pattern.date;
        }
    }
    return _datePicker;
}

- (void)datePicked:(UIDatePicker *)datePicker {
    self.selectedDate = datePicker.date;
    if (self.valueDidChangeHandler) {
        self.valueDidChangeHandler(self.selectedDate);
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:@[self.jw_left(self.toolBar), self.jw_right(self.toolBar), self.jw_height(self.toolBar,kToolBarHeight), self.jw_top(self.toolBar)]];
    
    self.datePicker.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:@[self.jw_left(self.datePicker), self.jw_right(self.datePicker), self.jw_height(self.datePicker,self.bounds.size.height - kToolBarHeight), self.jw_bottom(self.datePicker)]];
}

@end

/*********************************************************************/



@interface JWPickerView ()

@property (nonatomic ,strong) UIView *pickerView;

@end

@implementation JWPickerView

+ (JWPickerView *)pickerWithToolBarTitle:(NSString *)tittle
                              pickerType:(JWPickerViewType)pickerType
                                    data:(NSArray * )data
                          cancleComplete:(void(^)(void))cancleComplete
                         confirmComplete:(void(^)(id selectedIndex, id selectedData))confirmComplete
{
    JWPickerView *picker = [JWPickerView new];
    
    UIView *componentPicker = nil;
    switch (pickerType) {
        case JWPickerViewTypeSingleComponent:
        {
            componentPicker = [[JWSingleComponentPicker alloc] initWithToolBarTitle:tittle defaultIndex:0 data:data valueDidChangeHandler:nil cancleComplete:^{
                if (cancleComplete) {
                    cancleComplete();
                    [picker dismissPicker];
                }
            } confirmComplete:^(NSNumber *selectedIndex, NSString * _Nullable selectedData) {
                if (confirmComplete) {
                    confirmComplete(selectedIndex,selectedData);
                    [picker dismissPicker];
                }
            }];
        }
            break;
            
        case JWPickerViewTypeMultipleComponent:
        {
            componentPicker = [[JWMultipleComponentPicker alloc] initWithToolBarTitle:tittle defaultIndexes:@[@0,@0,@0] data:data valueDidChangeHandler:nil cancleComplete:^{
                if (cancleComplete) {
                    cancleComplete();
                }
                [picker dismissPicker];
            } confirmComplete:^(NSArray *selectedIndexes, NSArray<NSString *> * _Nullable selectedValues) {
                if (confirmComplete) {
                    confirmComplete(selectedIndexes,selectedValues);
                }
                [picker dismissPicker];
            }];
        }
            break;
            
        case JWPickerViewTypeRelevantComponent:
        {
            componentPicker = [[JWRelevantPicker alloc] initWithToolBarTitle:tittle defaultValues:@[@0,@0,@0] data:data valueDidChangeHandler:nil cancleComplete:^{
                if (cancleComplete) {
                    cancleComplete();
                }
                [picker dismissPicker];
            } confirmComplete:^(NSArray *selectedValues) {
                if (confirmComplete) {
                    confirmComplete(nil,selectedValues);
                }
                [picker dismissPicker];
            }];
        }
            break;
    }

    picker.pickerView = componentPicker;
    [picker showPicker];
    return picker;
}

+ (JWPickerView *)datePickerWithToolBarTittle:(NSString *)tittle
                                      pattern:(JWDatePickerPattern *)pattern
                               cancleComplete:(void(^)(void))cancleComplete
                              confirmComplete:(void(^)(NSDate *))confirmComplete
{
    JWPickerView *picker = [JWPickerView new];
    JWDatePicker *datePicker = [[JWDatePicker alloc] initWithToolBarTittle:tittle pattern:pattern valueDidChangeHandler:nil cancleComplete:^{
        if (cancleComplete) {
            cancleComplete();
        }
        [picker dismissPicker];
    } confirmComplete:^(NSDate * _Nonnull selectedDate) {
        if (confirmComplete) {
            confirmComplete(selectedDate);
        }
        [picker dismissPicker];
    }];
    picker.pickerView = datePicker;
    [picker showPicker];
    return picker;
}


#pragma mark - life cycle
- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOtherView:)];
        [self addGestureRecognizer:tapGes];
    }
    return self;
}

- (void)tapOtherView:(UITapGestureRecognizer *)tap {
    CGPoint location = [tap locationInView:self];
    DLog(@"%lf,%lf",location.x,location.y);
    // 点击除了picker以外的部分
    if (location.y < HEIGHT_OF_SCREEN - kPickerViewHeight) {
        [self dismissPicker];
    }
}

- (void)showPicker
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (!window) return;
    // 移除所有的活动，例如键盘等
    [window endEditing:YES];
    /// 添加view
    [window addSubview:self];
    /// 设置pickerView的frame为屏幕底部外面 --- 动画初始位置
    self.pickerView.frame = CGRectMake(0, HEIGHT_OF_SCREEN, WIDTH_OF_SCREEN, kPickerViewHeight);
    
    [UIView animateWithDuration:0.25f animations:^{
        // 设置pickerView的动画结束位置, 显示在屏幕底部
        self.pickerView.frame = CGRectMake(0, HEIGHT_OF_SCREEN - kPickerViewHeight, WIDTH_OF_SCREEN, kPickerViewHeight);;
    } completion:nil];
}

- (void)dismissPicker
{
    [UIView animateWithDuration:0.25f animations:^{
        self.pickerView.frame = CGRectMake(0, HEIGHT_OF_SCREEN, WIDTH_OF_SCREEN, kPickerViewHeight);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (self.superview) {
        self.frame = self.superview.bounds;
        self.pickerView.frame = CGRectMake(0, HEIGHT_OF_SCREEN - kPickerViewHeight, WIDTH_OF_SCREEN, kPickerViewHeight);
    }
}

- (void)setPickerView:(UIView *)pickerView
{
    _pickerView = pickerView;
    [self addSubview:_pickerView];
}

+ (JWPickerView *)cityPickerCancleComplete:(void(^)(void))cancleComplete
                           confirmComplete:(void(^)(id selectedIndex, id selectedData))confirmComplete
{
   return [JWPickerView pickerWithToolBarTitle:@""
                                    pickerType:JWPickerViewTypeRelevantComponent
                                          data:[self citiesData]
                                cancleComplete:cancleComplete
                               confirmComplete:confirmComplete];
}

+ (NSArray *)citiesData
{
//    NSString *str = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"city" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
//    NSArray *dataArray = [NSString yk_deserializeMessageJSON:str];
//    NSMutableArray *provinces = [@[] mutableCopy];
//    NSMutableArray *cities = [@[] mutableCopy];
//    NSMutableArray *areas = [@[] mutableCopy];
//    
//    for (NSDictionary *tempProvince in dataArray) {
//        [provinces addObject:tempProvince[@"name"]];
//        for (NSDictionary *cityName in tempProvince[@"city"]) {
//            [cities addObject:cityName[@"name"]];
//            for (NSString *area in cityName[@"area"]) {
//                
//            }
//        }
//    }
//    return [@[provinces,cities,areas] copy];
    return nil;
}

@end
