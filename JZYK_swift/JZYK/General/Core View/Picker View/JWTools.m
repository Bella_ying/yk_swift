//
//  JWTools.m
//  PickerView
//
//  Created by wangjeremy on 28/09/2017.
//  Copyright © 2017 Jeremy.L.Wang. All rights reserved.
//



#import "JWTools.h"


@implementation UIView (JWLayout)

- (NSLayoutConstraint *(^)(id, NSLayoutAttribute, id, NSLayoutAttribute, CGFloat))jw_layout{
    return ^(id view1, NSLayoutAttribute attr1, id view2, NSLayoutAttribute attr2, CGFloat constant){
        return [NSLayoutConstraint constraintWithItem:view1 attribute:attr1 relatedBy:NSLayoutRelationEqual toItem:view2 attribute:attr2 multiplier:1.0f constant:constant];
    };
}

- (NSLayoutConstraint *(^)(id, NSLayoutAttribute))jw_extend {
    return ^(id view1, NSLayoutAttribute attr){
        return self.jw_layout(view1,attr,self,attr,0);
    };
}

- (NSLayoutConstraint *(^)(id))jw_left {
    return ^(id view){
        return self.jw_extend(view,NSLayoutAttributeLeading);
    };
}

- (NSLayoutConstraint *(^)(id))jw_right {
    return ^(id view){
        return self.jw_extend(view,NSLayoutAttributeTrailing);
    };
}

- (NSLayoutConstraint *(^)(id))jw_bottom {
    return ^(id view){
        return self.jw_extend(view,NSLayoutAttributeBottom);
    };
}

- (NSLayoutConstraint *(^)(id))jw_top {
    return ^(id view){
        return self.jw_extend(view,NSLayoutAttributeTop);
    };
}

- (NSLayoutConstraint *(^)(id,CGFloat))jw_height {
    return ^(id view, CGFloat height){
        return self.jw_layout(view,NSLayoutAttributeHeight,nil,NSLayoutAttributeNotAnAttribute,height);;
    };
}

- (CGRect (^)(CGFloat, CGFloat, CGFloat))jw_frame {
    return ^(CGFloat X, CGFloat Width, CGFloat Height){
        CGRect cancelBtnRect = CGRectZero;
        cancelBtnRect.origin.x = X;
        cancelBtnRect.size.width = Width;
        cancelBtnRect.size.height = Height;
        return cancelBtnRect;
    };
}

- (UILabel *(^)(NSString *))jw_label {
    return ^(NSString *text){
        UILabel *label = [UILabel new];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont systemFontOfSize:18.0f];
        label.backgroundColor = [UIColor clearColor];
        label.adjustsFontSizeToFitWidth = YES;
        label.text = text;
        return label;
    };
}

- (UIButton *(^)(SEL, NSString *, UIColor *))button {
    return ^(SEL action, NSString *buttontitle, UIColor *color){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:buttontitle forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        [button setTitleColor:color forState:UIControlStateNormal];
        return button;
    };
}

@end

@implementation JWTools

void ifDebug(void(^void_block)(void)) {
#if DEBUG
    if (void_block) {
        void_block();
    }
#endif
}

+ (NSString *(^)(NSDate *current, JWDateMode dateMode))dateDescription {
    return ^(NSDate *current, JWDateMode dateMode){
        // 将当前时间以字符串形式输出
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //yyyy-MM-dd #EEEE HH:mm:ss  EEEE为星期几，EEE为周几
        //@"yyyy年MM月dd日
        NSString *dateFormatString = nil;
        
        switch (dateMode) {
            case JWDateModeDate:
                dateFormatString = @"yyyy年MM月dd日";
                break;
            case JWDateModeDateAndTime:
                dateFormatString = @"yyyy年MM月dd日HH时mm分ss秒";
                break;
            case JWDateModeTime:
                dateFormatString = @"HH:mm:ss";
                break;
            case JWDateModeCountDownTimer:
                dateFormatString = @"HH小时mm分";
                break;
        }
        [dateFormatter setDateFormat:dateFormatString];
        NSString *strDate = [dateFormatter stringFromDate:current];
        return strDate;
    };
}

+ (NSString *)unicodeToChinese:(NSString *)unicodeStr {
    if ([unicodeStr rangeOfString:@"\\u"].location == NSNotFound) {
        return unicodeStr;
    }
    NSString *tempStr1 = [unicodeStr stringByReplacingOccurrencesOfString:@"\\u"withString:@"\\U"];
    NSString *tempStr2 = [tempStr1 stringByReplacingOccurrencesOfString:@"\""withString:@"\\\""];
    NSString *tempStr3 = [[@"\""stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString * returnStr = [NSPropertyListSerialization propertyListWithData:tempData options:NSPropertyListImmutable format:NULL error:NULL];
    
    return [returnStr stringByReplacingOccurrencesOfString:@"\\r\\n"withString:@"\n"];
}

@end
