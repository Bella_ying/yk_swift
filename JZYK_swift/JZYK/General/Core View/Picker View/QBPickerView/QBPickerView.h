//
//  QBPickerView.h
//  h2omeX
//
//  Created by hongyu on 2017/11/8.
//  Copyright © 2017年 Shanghai Ku Ming Information Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^QBPickerSureBlock)(NSString * _Nonnull selectStr);

@interface QBPickerView : UIView

+ (instancetype _Nullable )sharedPickerManager;
/**
 单排pickeriew
 （无论第一次，还是多少次选择后都会从第一个开始选择）

 @param data 数据源(必须是字符串数组)
 @param complete 确认后的操作
 */
- (void)showSinglePickerByDefaultChoiseWithData:(NSArray<NSString *> * _Nonnull)data
                                confirmComplete:(QBPickerSureBlock _Nonnull)complete;

/**
 选择完picker后，再次选择还会记得自己的位置
 
 @param data 数据源(必须是字符串数组)
 @param complete 确认后的操作
 */
- (void)showSinglePickerRemainLastChoiseWithData:(NSArray<NSString *> * _Nonnull)data
                                 confirmComplete:(QBPickerSureBlock _Nonnull )complete;

/**
 单排pickeriew

 @param data 数据源(必须是字符串数组)
 @param selectRow 进入picker后选择哪个item
 @param complete 确认后的操作
 */
- (void)showSinglePickerWithData:(NSArray<NSString *> * _Nonnull)data
                        selectRow:(NSInteger)selectRow
                         confirmComplete:(QBPickerSureBlock _Nonnull)complete;

@end
