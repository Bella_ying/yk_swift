//
//  QBPickerView.m
//  h2omeX
//
//  Created by hongyu on 2017/11/8.
//  Copyright © 2017年 Shanghai Ku Ming Information Technology Co., Ltd. All rights reserved.
//

#define QBRGBA(R, G, B, A) \
[UIColor colorWithRed:R/255.f green:G/255.f blue:B/255.f alpha:A]

#import "QBPickerView.h"

static const CGFloat subViewH = 300;

@interface QBPickerView ()<UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSInteger _selectedIndex;
}

@property (nonatomic, strong) UIView         *lucencyView;
@property (nonatomic, strong) UIPickerView   *pickerView;
@property (nonatomic, strong) UIView         *subView;
@property (nonatomic, strong) UIView         *subTopView;
@property (nonatomic, strong) UIButton       *sureButton;
@property (nonatomic, strong) UIButton       *cancelButton;
@property (nonatomic, strong) NSMutableArray<NSString *> *datasArray;
@property (nonatomic, copy)   NSString       *selectedStr;


@property (nonatomic, copy) QBPickerSureBlock _Nonnull sureBlock;

@end

@implementation QBPickerView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _selectedIndex = 0;
        [self prepareUI];
        [self preparePickerView];
    }
    return self;
}

+ (instancetype)sharedPickerManager
{
    static dispatch_once_t onceToken;
    static QBPickerView *pickerView;
    dispatch_once(&onceToken, ^{
        pickerView = [QBPickerView new];
    });
    return pickerView;
}

- (void)prepareUI
{
    self.hidden = YES;
    self.backgroundColor = QBRGBA(0, 0, 0, 0);
    [self addSubview:self.lucencyView];
    [self.lucencyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 0, subViewH * ASPECT_RATIO_WIDTH, 0));
    }];
    self.frame  = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self addSubview:self.subView];
    [_subView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsMake([UIScreen mainScreen].bounds.size.height - subViewH * ASPECT_RATIO_WIDTH, 0, 0, 0));
    }];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.lucencyView addGestureRecognizer:tap];
}

- (void)preparePickerView
{
    [self.subView addSubview:self.subTopView];
    [self.subTopView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.subView).with.insets(UIEdgeInsetsMake(0, 0, subViewH * ASPECT_RATIO_WIDTH - 50, 0));
    }];
    [self.subTopView addSubview:self.sureButton];
    [self.sureButton addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.subTopView.mas_top).offset(0);
        make.bottom.equalTo(self.subTopView.mas_bottom).offset(0);
        make.right.equalTo(self.subTopView.mas_right).offset(0);
        make.width.mas_offset(50);
    }];
    [self.subTopView addSubview:self.cancelButton];
    [self.cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.subTopView.mas_top).offset(0);
        make.bottom.equalTo(self.subTopView.mas_bottom).offset(0);
        make.left.equalTo(self.subTopView.mas_left).offset(0);
        make.width.mas_offset(50);
    }];
    
    self.pickerView = [UIPickerView new];
    _pickerView.delegate   = self;
    _pickerView.dataSource = self;
    [self.subView addSubview:self.pickerView];
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.subTopView.mas_bottom).offset(0);
        make.bottom.equalTo(self.subView.mas_bottom).offset(0);
        make.left.equalTo(self.subView.mas_left).offset(0);
        make.right.equalTo(self.subView.mas_right).offset(0);
    }];
}

- (void)sureAction:(UIButton *)button
{
    if (self.sureBlock) {
        self.sureBlock(self.selectedStr);
    }
    [self dismissPickerView];
}

- (void)cancelAction:(UIButton *)button
{
    [self dismissPickerView];
}

- (void)showPickerView
{
    self.hidden = NO;
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.subView.layer.transform = CATransform3DMakeTranslation(0, subViewH * ASPECT_RATIO_WIDTH, 0);
    [UIView animateWithDuration:0.3 animations:^{
        self.subView.layer.transform = CATransform3DMakeTranslation(0, 0, 0);
        self.backgroundColor = QBRGBA(0, 0, 0, 0.5);
    } completion:^(BOOL finished) {

    }];
}

- (void)dismissPickerView
{
    [UIView animateWithDuration:0.3 animations:^{
        self.subView.layer.transform = CATransform3DMakeTranslation(0, subViewH * ASPECT_RATIO_WIDTH, 0);
        self.backgroundColor = QBRGBA(0, 0, 0, 0);
    } completion:^(BOOL finished) {
        self.hidden = YES;
        [self removeFromSuperview];
    }];
}

- (void)tapAction
{
    [self dismissPickerView];
}

- (void)showSinglePickerByDefaultChoiseWithData:(NSArray<NSString *> * _Nonnull)data
                                confirmComplete:(QBPickerSureBlock _Nonnull)complete
{
    [self showSinglePickerWithData:data selectRow:0 confirmComplete:complete];
}

- (void)showSinglePickerRemainLastChoiseWithData:(NSArray<NSString *> * _Nonnull)data
                                 confirmComplete:(QBPickerSureBlock _Nonnull)complete
{
    [self showSinglePickerWithData:data selectRow:_selectedIndex confirmComplete:complete];
}

- (void)showSinglePickerWithData:(NSArray<NSString *> * _Nonnull)data
                  selectRow:(NSInteger)selectRow
                   confirmComplete:(QBPickerSureBlock _Nonnull)complete
{
    if (!data || !data.count) {
        return;
    }
    
    [data enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSAssert([obj isKindOfClass:[NSString class]], @"datas must contains NSString type object, check your data");
    }];
    if (complete) {
        self.sureBlock = complete;
    }
    self.datasArray = [NSMutableArray arrayWithArray:data];
    [self.pickerView reloadAllComponents];
    if (self.datasArray.count && selectRow < self.datasArray.count) {
        [self.pickerView selectRow:selectRow inComponent:0 animated:NO];
        self.selectedStr = self.datasArray[selectRow];
    }
    [self showPickerView];
}

#pragma mark- UIPickerViewDelegate & UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.datasArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    DLog(@"%zd", row);
    return self.datasArray[row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedStr = self.datasArray[row];
    _selectedIndex = row;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = 20.0f;
    
    UIView * textView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    textView.backgroundColor = [UIColor clearColor];
    
    UILabel * completeLabel     = [UILabel new];
    completeLabel.center        = textView.center;
    completeLabel.bounds        = CGRectMake(0, 0, width, height);
    completeLabel.textColor     = [UIColor yk_colorWithHexString:@"#666666"];
    completeLabel.textAlignment = NSTextAlignmentCenter;
    completeLabel.font = [UIFont systemFontOfSize:17];
    if (component == 0) {
        completeLabel.text = self.datasArray[row];
    }
    [textView addSubview:completeLabel];
    
    ((UIView *)[self.pickerView.subviews objectAtIndex:1]).backgroundColor = [UIColor yk_colorWithHexString:@"#EEEEEE"];
    ((UIView *)[self.pickerView.subviews objectAtIndex:2]).backgroundColor = [UIColor yk_colorWithHexString:@"#EEEEEE"];
    return textView;
}

- (UIView *)lucencyView
{
    if (!_lucencyView) {
        _lucencyView = [UIView new];
        _lucencyView.backgroundColor = [UIColor clearColor];
    }
    return _lucencyView;
}

- (UIView *)subView
{
    if (!_subView) {
        _subView = [UIView new];
        _subView.backgroundColor = [UIColor whiteColor];
    }
    return _subView;
}

- (UIView *)subTopView
{
    if (!_subTopView) {
        _subTopView = [UIView new];
        _subTopView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _subTopView;
}

- (UIButton *)sureButton
{
    if (!_sureButton) {
        _sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sureButton setTitle:@"确定" forState:UIControlStateNormal];
        _sureButton.titleLabel.font = [UIFont systemFontOfSize:17];
        _sureButton.titleLabel.textAlignment = NSTextAlignmentRight;
        [_sureButton setTitleColor:MAIN_THEME_COLOR forState:UIControlStateNormal];
    }
    return _sureButton;
}

- (UIButton *)cancelButton
{
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:17];
        _cancelButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        [_cancelButton setTitleColor:MAIN_THEME_COLOR forState:UIControlStateNormal];
    }
    return _cancelButton;
}

@end
