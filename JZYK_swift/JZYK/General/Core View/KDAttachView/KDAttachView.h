//
//  KDAttachView.h
//  KDLC
//
//  Created by 曹晓丽 on 16/8/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AttachViewDelegate <NSObject>

//点击事件必须通过此代理来实现
- (void)buttonTouchAction;

@end

typedef NS_ENUM(NSUInteger, AttachDirection) {
  AttachNone,       //无，可停留在任意位置，放下后不向任意方向靠拢
  AttachLeft,       //往左靠拢
  AttachRight,      //往右靠拢
  AttachTop,        //往上靠拢
  AttachBottom,     //往下靠拢
  AttachNatural     //自然，离哪个方向近就往哪靠
};

@interface KDAttachView : UIButton

@property(nonatomic,assign)id<AttachViewDelegate> attachViewDelegate;   //点击事件的代理
@property(nonatomic, copy) void (^clickSelfAticon)(void);
@property (nonatomic, assign) BOOL isCanDrag; //是否可以拖动，默认不可拖动
@property (nonatomic, assign) BOOL isAllShow;
/**
 *  创建有粘性view
 *
 *  @param direction    朝哪个方向靠拢
 *  @param cut          是否砍掉tabbar的高度
 */
- (instancetype)initWithDirection:(AttachDirection)direction andCutTabBar:(BOOL)cut;

- (void)yk_hiddenView;
- (void)yk_showView;

@end
