//
//  KDAttachView.m
//  KDLC
//
//  Created by 曹晓丽 on 16/8/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "KDAttachView.h"

@interface KDAttachView()

@property (nonatomic, assign) AttachDirection direction;
@property (nonatomic, assign) BOOL isCut;
@property (nonatomic, retain) UIPanGestureRecognizer *pan;

@end

@implementation KDAttachView

- (instancetype)initWithDirection:(AttachDirection)direction andCutTabBar:(BOOL)cut{
  if (self = [super init]) {
    self.direction = direction;
    self.isCut = cut;
    self.isCanDrag = NO;
    [self addTarget:self action:@selector(didTouched:) forControlEvents:UIControlEventTouchUpInside];

  }
  return self;
}

- (void)setIsCanDrag:(BOOL)isCanDrag
{
    _isCanDrag = isCanDrag;
    if (_isCanDrag) {
        [self addGestureRecognizer:self.pan];
    }else{
        [self removeGestureRecognizer:self.pan];
    }
    
}
- (void)didTouched:(KDAttachView *)button{

    if (self.isAllShow) {
//        if (self.attachViewDelegate) {
//          [self.attachViewDelegate buttonTouchAction];
//        }
        if (self.clickSelfAticon) {
            self.clickSelfAticon();
        }
    }else{
        [UIView animateWithDuration:0.5 animations:^{
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.superview.mas_right).offset(0);
            }];
        } completion:^(BOOL finished) {
            self.isAllShow = YES;
        }];
    }

}

- (void)yk_hiddenView{
    if (self.isAllShow) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.superview.mas_right).offset(self.frame.size.width * 0.5);
        }];
        self.isAllShow = NO;
    }
}
- (void)yk_showView{
    if (!self.isAllShow) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.superview.mas_right).offset(0);
        }];
        self.isAllShow = YES;
    }
}


//手势事件 －－ 改变位置

-(void)changePostion:(UIPanGestureRecognizer *)pan
{
    CGPoint point = [pan translationInView:self];
//    CGFloat width = [UIScreen mainScreen].bounds.size.width;
//    CGFloat height = [UIScreen mainScreen].bounds.size.height - (_isCut ? 49 : 0);
    CGRect originalFrame = self.frame;
    

        
        originalFrame.origin.x += point.x;
        
        originalFrame.origin.y += point.y;
        

    self.frame = originalFrame;
    [pan setTranslation:CGPointZero inView:self];
    
    if (pan.state == UIGestureRecognizerStateBegan) {
        self.enabled = NO;
    }
    else if (pan.state == UIGestureRecognizerStateChanged)
    {
    }
    else {
        self.enabled = YES;
    }
    if (pan.state == UIGestureRecognizerStateEnded || pan.state == UIGestureRecognizerStateCancelled) {
        [self endTouchMethod];
    }
    
}

//结束手势后靠边的方法

- (void)endTouchMethod{
    if (_direction == AttachNone) {
        
    }else if (_direction == AttachLeft){
        [UIView animateWithDuration:0.25 animations:^(void){
            self.center = CGPointMake(self.frame.size.width / 2, self.center.y);
        }];
    }else if (_direction == AttachRight){
        [UIView animateWithDuration:0.25 animations:^(void){
            self.center = CGPointMake(self.superview.frame.size.width - self.frame.size.width / 2, self.center.y);
        }];
    }else if (_direction == AttachTop){
        [UIView animateWithDuration:0.25 animations:^(void){
            self.center = CGPointMake(self.center.x, self.frame.size.height / 2);
        }];
    }else if (_direction == AttachBottom){
        [UIView animateWithDuration:0.25 animations:^(void){
            self.center = CGPointMake(self.center.x, self.superview.frame.size.height - self.frame.size.height / 2-(self.isCut ? 49 : 0));
        }];
    }else if (_direction == AttachNatural){
        [UIView animateWithDuration:0.25 animations:^(void){
            CGRect containerBounds = CGRectMake(0, 0, self.superview.bounds.size.width, self.superview.bounds.size.height - (self.isCut ? 49 : 0)) ;
            
            UIEdgeInsets edgeInsets = UIEdgeInsetsMake(self.frame.origin.y, self.frame.origin.x, containerBounds.size.height - (self.frame.origin.y + self.frame.size.height), containerBounds.size.width - (self.frame.origin.x + self.frame.size.width));
            CGFloat edgeDistance = edgeInsets.top;
            CGPoint destPosition = CGPointMake(self.frame.origin.x, 0);
            
            if (edgeInsets.bottom < edgeDistance)
            {
                edgeDistance = edgeInsets.bottom;
                destPosition = CGPointMake(self.frame.origin.x, containerBounds.size.height - self.frame.size.height);
            }
            if (edgeInsets.left < edgeDistance)
            {
                edgeDistance = edgeInsets.left;
                destPosition = CGPointMake(0, self.frame.origin.y);
            }
            if (edgeInsets.right < edgeDistance)
            {
                destPosition = CGPointMake(containerBounds.size.width - self.frame.size.width, self.frame.origin.y);
            }
            
            if (destPosition.x < 0)
            {
                destPosition.x = 0;
            }
            else if (destPosition.x > containerBounds.size.width - self.frame.size.width)
            {
                destPosition.x = containerBounds.size.width - self.frame.size.width;
            }
            if (destPosition.y < 0)
            {
                destPosition.y = 0;
            }
            else if (destPosition.y > containerBounds.size.height - self.frame.size.height )
            {
                destPosition.y = containerBounds.size.height - self.frame.size.height;
            }
             self.frame = CGRectMake(destPosition.x, destPosition.y, self.frame.size.width, self.frame.size.height);
        }];
    }

}

#pragma mark getter

- (UIPanGestureRecognizer *)pan{
    if (!_pan) {
        _pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(changePostion:)];
        
    }
    return _pan;
}

@end
