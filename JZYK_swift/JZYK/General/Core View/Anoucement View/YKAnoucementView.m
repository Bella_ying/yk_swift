//
//  YKAnoucementView.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAnoucementView.h"
#import <SDCycleScrollView/SDCycleScrollView.h>

@interface YKAnoucementView()

@property (nonatomic, strong) SDCycleScrollView *textScrollView;

@end

@implementation YKAnoucementView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = Color.anoucementBgColor;
        [self configUI];
    }
    return self;
}

- (void)configUI {
    //公告的小喇叭
    UIImageView * loudspeakerImg = [UIImageView yk_imageViewWithImageName:@"notice" superView:self masonrySet:^(UIImageView *imageView, MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.left.offset(15);
    }];
    
    UIButton *closeButton = [UIButton yk_buttonWithImageName:@"webview_close" superView:self masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        make.top.bottom.offset(0);
        make.right.offset(-5);
        make.width.offset(33);
        
        [button addTarget:self action:@selector(closeAnoucementAction:) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    //文字滚动
    self.textScrollView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN - 70, 30)];
    self.textScrollView.onlyDisplayText = YES;
    self.textScrollView.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.textScrollView.userInteractionEnabled = NO;
    self.textScrollView.autoScrollTimeInterval = 5.0f;
    self.textScrollView.titleLabelTextFont = [UIFont systemFontOfSize:adaptFontSize(13)];
    self.textScrollView.titleLabelTextColor = Color.color_66_C3;
    self.textScrollView.titleLabelBackgroundColor = self.backgroundColor;
    self.textScrollView.clickItemOperationBlock = ^(NSInteger currentIndex){
    };
    [self addSubview:self.textScrollView];
    [self.textScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(loudspeakerImg.mas_right).offset(8);
        make.right.equalTo(closeButton.mas_left).offset(8);
        make.top.bottom.offset(0);
    }];
}

- (void)updateAnoucementViewWithTitleArr:(NSArray *)titleArr {
    self.textScrollView.titlesGroup = titleArr;
}

- (void)closeAnoucementAction:(UIButton *)sender {
    self.hidden = YES;
}

@end
