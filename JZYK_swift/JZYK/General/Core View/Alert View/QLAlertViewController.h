//
//  QLAlertViewController.h
//  QuickLoan
//
//  Created by Jeremy on 2018/4/10.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, QLAlertControllerType) {
    QLAlertControllerTypeDefault = 0,                // 默认，非定制
    QLAlertControllerTypeCustomized = 1              // 外部定制
};

typedef NS_ENUM(NSUInteger, QLAlertControllerButtonLayoutType) {
    QLAlertControllerButtonLayoutTypeHorizontal = 0,               // 默认，按钮横向排列
    QLAlertControllerButtonLayoutTypeVertical = 1                  // 按钮纵向排列
};

@interface QLAlertViewController : UIViewController

// 容器
@property (nonatomic, strong) UIView *containerView;
//按钮数组
@property (nonatomic, strong) NSArray *buttonTitles;

//弹框点击事件的block
@property (nonatomic, copy) void (^onButtonTouchUpInside)(NSInteger buttonIndex);
//弹框类型
@property (nonatomic, assign) QLAlertControllerType alertControllerType;
//弹框底部按钮的排列类型
@property (nonatomic, assign) QLAlertControllerButtonLayoutType alertControllerButtonLayoutType;

//标题颜色
@property (nonatomic, strong) UIColor *titleTextColor;
//标题字号
@property (nonatomic, strong) UIFont *titleTextFont;
//内容颜色
@property (nonatomic, strong) UIColor *contentTextColor;
//内容字号
@property (nonatomic, strong) UIFont *contentTextFont;
//title富文本修饰的内容
@property(nonatomic, strong) NSAttributedString *titleAttributedText;
//content富文本修饰的内容
@property(nonatomic, strong) NSAttributedString *contentAttributedText;
@property (nonatomic, assign) NSTextAlignment contentTextAlignement;

//按钮字体颜色
@property (nonatomic, strong) UIColor *btnTitleColor;
//按钮字体字号
@property (nonatomic, strong) UIFont *btnTitleFont;
//按钮背景颜色
@property (nonatomic, strong) UIColor *btnBgColor;

@property (nonatomic, strong) UIView *alertView;

//定制alert弹框
- (void)prepareExternalCustomUI;

/**
 @param content 弹框内容
 */
- (void)prepareUIWithContentText:(NSString *)content;

/**
 @param title 弹框标题
 @param content 弹框内容
 */
- (void)prepareUIWithTitle:(NSString *)title contentText:(NSString *)content;

/**
 @param imageName 图片名字
 @param title 标题
 @param content 内容
 */
- (void)prepareUIImageName:(NSString *)imageName
                   title  :(NSString *)title
                   content:(NSString *)content;

- (void)setOnButtonTouchUpInside:(void (^)( NSInteger buttonIndex))onButtonTouchUpInside;

@end
