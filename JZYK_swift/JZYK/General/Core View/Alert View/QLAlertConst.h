//
//  QLAlertConst.h
//  QuickLoan
//
//  Created by Jeremy on 2018/4/10.
//  Copyright © 2018 JSQB. All rights reserved.
//

extern NSString * const kAleartClose;

extern const CGFloat kAlertDefaultButtonHeight;         //button高度
extern const CGFloat kAlertDefaultButtonSpacerHeight;   //按钮之间的间距
extern const CGFloat kAlertDefaultButtonBottomPadding;//按钮距父视图下间距
extern const CGFloat kAlertDefaultContentLeftRightPadding;
extern const CGFloat kAlertViewCornerRadius;            //弹框圆角宽度
extern const CGFloat kAlertDefaultPadding;              //内容边距
extern const CGFloat kAlertDefaultTotalWidth;           //弹框宽度
extern const CGFloat kAlertBankButtonHeight; //可能不用
extern const CGFloat kAlertDefaultTitleTop;            //标题的上间距
extern const CGFloat kAlertBankButtonTopPadding;
extern const CGFloat kAlertBankButtonBottomPadding;
extern const CGFloat kAlertDefaultTitleHeight;         //标题的高度
extern const CGFloat kAlertDefaultContainerWidth;       //内容宽度
