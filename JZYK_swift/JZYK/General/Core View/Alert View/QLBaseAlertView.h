//
//  QLBaseAlertView.h
//  QuickLoan
//
//  Created by Jeremy on 2018/4/11.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QLBaseAlertView : UIView

@property (nonatomic, strong) UIView *alertBackGroundView;

- (void)configUI;

- (void)updateUI:(id)model;

- (void)show;

- (void)dismiss;

- (instancetype(^)(id model))updateAlertView;

- (void(^)(void))showAlert;


@end
