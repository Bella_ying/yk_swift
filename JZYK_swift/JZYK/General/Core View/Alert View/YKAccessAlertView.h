//
//  YKAccessAlertView.h
//  KDFDApp
//
//  Created by hongyu on 2017/8/28.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^YKUpDateCloseBlock)(void);
typedef void(^YKSelectContantsPhoneBlock)(NSString *phone);
typedef void(^YKSelectedCloseGestureViewBlock) (void);
typedef void(^YKSelectedPushCloseBlock) (void);
typedef void(^YKStudentBottomBlock) (void);

typedef NS_ENUM(NSUInteger, YKShowAlertType) {
    YKShowAlertTypeUpdate = 0,            // 更新提示
    YKShowAlertTypeFace = 1,              // 人脸识别提示
    YKShowAlertTypeStudent = 2,           // 学生提示
    YKShowAlertTypeContactsPhone = 3 ,    // 选择联系人
    YKShowAlertTypeGestureForgetPsw = 4,  // 忘记密码
    YKShowAlertTypeLocation = 5,                   // 定位权限
    YKShowAlertTypeContactBook = 6,                // 通讯录权限
    YKShowAlertTypeCalendar= 7,                    // 日历权限
    YKShowAlertTypeHandGesture = 9,                // 手势密码权限
    YKShowAlertTypePush = 10,                      // 消息推送权限
};

@interface YKAccessAlertView : UIView

@property (nonatomic, copy) YKUpDateCloseBlock upDateCloseBlock;
@property (nonatomic, copy) YKStudentBottomBlock   studentBottomBlock;
@property (nonatomic, copy) YKSelectContantsPhoneBlock selectContantsPhoneBlock;
@property (nonatomic, copy) YKSelectedCloseGestureViewBlock selectedCloseGestureBlock;
@property (nonatomic, copy) YKSelectedPushCloseBlock selectedPushCloseBlock;
//人脸识别
@property (nonatomic,copy) void (^identificationAction) (void);
//手势密码，忘记密码
@property (nonatomic,copy) void (^forgetGestureBlock) (void);

//手势密码，在首页开启手势密码权限
@property (nonatomic,copy) void (^openGestureBlock) (void);

+ (instancetype)sharedAlertManager;

/**
 提示组件
  @param type 可选类型
 @param text 显示文本:更新弹框 和 选择联系人弹框 需要；其他不传；
 */
- (instancetype)showAlertType:(YKShowAlertType)type contentText:(NSString *)text;

@end
