//
//  QLAlertViewController.m
//  QuickLoan
//
//  Created by Jeremy on 2018/4/10.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QLAlertViewController.h"
#import <Masonry/Masonry.h>
#import "QLAlertMacros.h"
#import "QLAlertAnimation.h"
#import "QLAlertConst.h"

@interface QLAlertViewController ()<UIViewControllerTransitioningDelegate>

@property (nonatomic, strong) UILabel *titleLabel;        //标题
@property (nonatomic, strong) UITextView *contentTextView;//内容说明
@property (nonatomic, assign) CGFloat containerViewHeight;
@property (nonatomic, strong) NSMutableArray *btnArray;
@property (nonatomic, strong) UIImageView *imgView;       //有图片+内容
@property (nonatomic, strong) UIButton *closeBtn;

@end

@implementation QLAlertViewController

CGFloat btnHeight = 0;
CGFloat btnSpacerHeight = 0;

- (instancetype)init {
    if (self = [super init]) {
        self.transitioningDelegate = self;                          // 设置自己为转场代理
        self.modalPresentationStyle = UIModalPresentationCustom;    // 自定义转场模式
    }
    return self;
}

#pragma mark - UIViewControllerTransitioningDelegate
/** 返回Present动画 */
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    QLAlertAnimation *animation = [QLAlertAnimation new];
    animation.animationType = QLAlertAnimationTypePresent;
    return animation;
}

/** 返回Dismiss动画 */
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    QLAlertAnimation *animation = [QLAlertAnimation new];
    animation.animationType = QLAlertAnimationTypeDismiss;
    return animation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:self.alertView];
    
    switch (self.alertControllerType) {
        case QLAlertControllerTypeDefault:
        {
            switch (self.alertControllerButtonLayoutType) {
                case QLAlertControllerButtonLayoutTypeHorizontal:
                {
                    [self addHorizontalButtonsToView:self.alertView];
                    [self.alertView mas_updateConstraints:^(MASConstraintMaker *make) {
                        [make center];
                        make.width.offset(kAlertDefaultTotalWidth);
                        make.height.offset(self.containerViewHeight+ kAlertDefaultPadding + btnHeight + kAlertDefaultPadding );
                    }];
                }
                    break;
                case QLAlertControllerButtonLayoutTypeVertical:
                {
                    [self addVerticalButtonsToView:self.alertView];
                    WEAK_SELF
                    [self.alertView mas_updateConstraints:^(MASConstraintMaker *make) {
                        [make center];
                        make.width.offset(kAlertDefaultTotalWidth);
                        make.height.offset(weakSelf.containerViewHeight + kAlertDefaultPadding + btnHeight * weakSelf.buttonTitles.count + kAlertDefaultPadding);
                    }];
                }
                    break;
            }
        }
            break;
        case QLAlertControllerTypeCustomized:
        {
            [self.alertView addSubview:self.containerView];
            if (_alertControllerButtonLayoutType == QLAlertControllerButtonLayoutTypeHorizontal) {
                [self addHorizontalButtonsToView:self.alertView];
            }else{
                [self addVerticalButtonsToView:self.alertView];
            }
        }
            break;
    }
}

- (void)prepareExternalCustomUI
{
    _buttonTitles = @[kAleartClose];
    _alertControllerType = QLAlertControllerTypeCustomized;
}

- (void)prepareUIWithContentText:(NSString *)content
{
    return [self prepareUIWithTitle:nil contentText:content];
}

- (void)prepareUIWithTitle:(NSString *)title contentText:(NSString *)content
{
    _buttonTitles = @[kAleartClose];
    _alertControllerType = QLAlertControllerTypeDefault;
    
    self.titleLabel.text = title;
    self.contentTextView.text = content;
    
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.contentTextView];
    
    // 添加约束 titleLabel
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(kAlertDefaultContainerWidth);
        make.centerX.equalTo(self.containerView);
//        make.top.offset(kAlertDefaultPadding);
//        make.bottom.equalTo(self.contentTextView.mas_top).offset(-kAlertDefaultPadding);
        make.top.offset(kAlertDefaultTitleTop);
    }];
    
    // 添加约束 contentTextView
    CGFloat textViewHeight = [self.contentTextView sizeThatFits:CGSizeMake(kAlertDefaultContainerWidth, 0)].height;
    CGFloat titleHeight = [self.titleLabel sizeThatFits:CGSizeMake(kAlertDefaultContainerWidth, 0)].height;
    if (textViewHeight > ALERT_SCREEN_WIDTH/2) {
        textViewHeight = ALERT_SCREEN_HEIGHT/2;
    }
    [self.contentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(kAlertDefaultContainerWidth);
        make.centerX.equalTo(self.containerView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(kAlertDefaultPadding);
        make.bottom.offset(0);
        make.height.offset(textViewHeight);
    }];
    
    // 添加约束
//    self.containerViewHeight = textViewHeight + titleHeight + kAlertDefaultPadding*2;
    self.containerViewHeight = textViewHeight + titleHeight + kAlertDefaultPadding + kAlertDefaultTitleTop;
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.centerX.equalTo(self.alertView);
        make.width.offset(kAlertDefaultContainerWidth);
        make.height.offset(self.containerViewHeight);
    }];
    
}

#pragma mark 一个图片+内容的弹框
- (void)prepareUIImageName:(NSString *)imageName
                   title  :(NSString *)title
                   content:(NSString *)content
{
    _buttonTitles = @[kAleartClose];
    _alertControllerType = QLAlertControllerTypeDefault;
    _alertControllerButtonLayoutType = QLAlertControllerButtonLayoutTypeVertical;
    
    self.contentTextView.text = content;
    self.titleLabel.text   = title;
    
    [self.containerView addSubview:self.imgView];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.contentTextView];
    [self.containerView addSubview:self.closeBtn];
    
    self.imgView.image = [UIImage imageNamed:imageName];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.offset(0);
        make.height.offset(140);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(kAlertDefaultContainerWidth);
        make.centerX.equalTo(self.containerView);
        make.top.equalTo(self.imgView.mas_bottom).offset(kAlertDefaultPadding);
        make.bottom.equalTo(self.contentTextView.mas_top).offset(-kAlertDefaultPadding);
    }];
    
    CGFloat textViewHeight = [self.contentTextView sizeThatFits:CGSizeMake(kAlertDefaultContainerWidth, 0)].height;
    [self.contentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.containerView);
        make.width.offset(kAlertDefaultContainerWidth);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(kAlertDefaultPadding);
        make.height.offset(textViewHeight);
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-kAlertDefaultPadding);
        make.top.offset(kAlertDefaultPadding);
        make.width.height.offset(30);
    }];
    
    self.containerViewHeight = 140 + textViewHeight +[self.titleLabel sizeThatFits:CGSizeMake(kAlertDefaultContainerWidth, 0)].height +kAlertDefaultPadding*2;
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.centerX.equalTo(self.alertView);
        make.width.offset(kAlertDefaultTotalWidth);
        make.height.offset(self.containerViewHeight);
    }];
}
- (void)setTitleTextColor:(UIColor *)titleTextColor
{
    if (titleTextColor)self.titleLabel.textColor = titleTextColor;
}
- (void)setContentTextColor:(UIColor *)contentTextColor
{
    if (contentTextColor)self.contentTextView.textColor = contentTextColor;
}
- (void)setTitleTextFont:(UIFont *)titleTextFont
{
    if (titleTextFont)self.titleLabel.font = titleTextFont;
}
- (void)setContentTextFont:(UIFont *)contentTextFont
{
    if (contentTextFont)self.contentTextView.font = contentTextFont;
}
- (void)setTitleAttributedText:(NSAttributedString *)titleAttributedText
{
    if (titleAttributedText) self.titleLabel.attributedText = titleAttributedText;
}
- (void)setContentAttributedText:(NSAttributedString *)contentAttributedText
{
    if (contentAttributedText)self.contentTextView.attributedText = contentAttributedText;
}
- (void)setBtnTitleColor:(UIColor *)btnTitleColor
{
    if (btnTitleColor){
        for (UIButton * closeBtn in self.btnArray) {
            [closeBtn setTitleColor:btnTitleColor forState:UIControlStateNormal];
        }
    }
}
- (void)setBtnTitleFont:(UIFont *)btnTitleFont
{
    if (btnTitleFont) {
        for (UIButton * closeBtn in self.btnArray) {
            [closeBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
        }
    }
}
- (void)setBtnBgColor:(UIColor *)btnBgColor
{
    if (btnBgColor) {
        for (UIButton * closeBtn in self.btnArray) {
            closeBtn.backgroundColor = btnBgColor;
        }
    }
}

#pragma mark --setter
- (void)setContentTextAlignement:(NSTextAlignment)contentTextAlignement{
    _contentTextAlignement = contentTextAlignement;
    self.contentTextView.textAlignment = contentTextAlignement;
}

- (void)addHorizontalButtonsToView: (UIView *)container
{
    if (_buttonTitles == nil) return;
    
    [self addAssertions];
    
    if (_buttonTitles && [_buttonTitles count] == 2) {
        btnHeight       = kAlertDefaultButtonHeight;
        btnSpacerHeight = kAlertDefaultButtonSpacerHeight;
    }else if (_buttonTitles && [_buttonTitles count] == 1){
        btnHeight       = kAlertDefaultButtonHeight;
        btnSpacerHeight = 0;
    }else {
        btnHeight = 0;
        btnSpacerHeight = 0;
    }
    
    CGFloat buttonWidth = (kAlertDefaultContainerWidth - btnSpacerHeight) / [_buttonTitles count];
    
    for (int i = 0; i<[_buttonTitles count]; i++) {
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeButton addTarget:self action:@selector(buttonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [closeButton setTag:i];
        [closeButton setTitle:[_buttonTitles objectAtIndex:i] forState:UIControlStateNormal];
        [closeButton setTitleColor:i == 1 ? ALERT_CANCEL_COLOR : ALERT_CONFIRM_COLOR  forState:UIControlStateNormal];
        [closeButton.titleLabel setFont:ALERT_BUTTON_TEXT_FONT];
        closeButton.titleLabel.numberOfLines = 0;
        closeButton.layer.cornerRadius = kAlertViewCornerRadius;
        closeButton.clipsToBounds = YES;
        [self.alertView addSubview:closeButton];
        [closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.offset(buttonWidth);
            make.right.offset( - i *( buttonWidth + btnSpacerHeight ) - kAlertDefaultPadding );
            make.bottom.offset(0);
            make.height.offset(btnHeight);
        }];
        [self.btnArray addObject:closeButton];
        
        if (i == 0) {
            // 水平分割线
            UIView *hLineView = [[UIView alloc] init];
            hLineView.backgroundColor = ALERT_LINE_COLOR;
            [self.alertView addSubview:hLineView];
            [hLineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(-btnHeight);
                make.left.offset(13);
                make.right.offset(-13);
                make.height.offset(1);
            }];
        }else{
            //两个按钮时，中间的竖直分割线
            UIView *vLineView = [[UIView alloc] init];
            vLineView.backgroundColor = ALERT_LINE_COLOR;
            [self.alertView addSubview:vLineView];
            [vLineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(-6);
                make.height.offset(btnHeight-12);
                make.centerX.equalTo(self.alertView);
                make.width.offset(1);
            }];
        }
    }
}

//底部按钮纵向排列
- (void)addVerticalButtonsToView: (UIView *)container
{
    if (_buttonTitles == nil) return;
    [self addAssertions];
    
    if (_buttonTitles && [_buttonTitles count] == 2) {
        btnHeight       = kAlertDefaultButtonHeight;
        btnSpacerHeight = kAlertDefaultButtonSpacerHeight;
    }else if (_buttonTitles && [_buttonTitles count] == 1){
        btnHeight       = kAlertDefaultButtonHeight;
        btnSpacerHeight = 0;
    }else {
        btnHeight = 0;
        btnSpacerHeight = 0;
    }
    
    CGFloat buttonWidth = kAlertDefaultContainerWidth;
    
    for (int i = 0; i<[_buttonTitles count]; i++) {
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeButton addTarget:self action:@selector(buttonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [closeButton setTag:i];
        [closeButton setTitle:[_buttonTitles objectAtIndex:i] forState:UIControlStateNormal];
        [closeButton.titleLabel setFont:ALERT_BUTTON_TEXT_FONT];
        closeButton.layer.cornerRadius = kAlertViewCornerRadius;
        closeButton.clipsToBounds = YES;
        [closeButton setTitleColor: WHITE_COLOR  forState:UIControlStateNormal];
        closeButton.backgroundColor = MAIN_THEME_COLOR;
        [container addSubview:closeButton];
        [closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.offset(buttonWidth);
            make.centerX.equalTo(container);
            make.bottom.offset(- i *btnHeight  - kAlertDefaultPadding);
            make.height.offset(btnHeight);
        }];
        [self.btnArray addObject:closeButton];
        
    }
}
- (void)buttonTouchUpInside:(id)sender
{
    UIButton *btn = sender;
    if (self.onButtonTouchUpInside) {
        self.onButtonTouchUpInside(btn.tag);
    }
    [self close];
}
#pragma mark - 添加断言（如传入的标题不是NSString）
- (void)addAssertions
{
    for  (id btnTitle in _buttonTitles) {
        if (![btnTitle isKindOfClass:[NSString class]]) {
            NSAssert(NO, @"按钮的标题必须是NSString类型");
        }
    }
}
#pragma mark 懒加载

- (UIView *)alertView
{
    if (!_alertView) {
        CGSize dialogSize = [self countDialogSize];
        _alertView = [[UIView alloc] initWithFrame:CGRectMake((ALERT_SCREEN_WIDTH - dialogSize.width) / 2, (ALERT_SCREEN_HEIGHT - dialogSize.height) / 2, dialogSize.width, dialogSize.height)];
        _alertView.backgroundColor = [UIColor whiteColor];
        _alertView.layer.cornerRadius = kAlertViewCornerRadius;
        _alertView.clipsToBounds = YES;
        _alertView.layer.opacity = 0.5f;
        _alertView.layer.transform = CATransform3DMakeScale(1.3f, 1.3f, 1.0);
    }
    [_alertView addSubview:self.containerView];
    return _alertView;
}
- (CGSize)countDialogSize
{
    if (_buttonTitles && [_buttonTitles count] > 0) {
        btnHeight       = kAlertDefaultButtonHeight;
        btnSpacerHeight = kAlertDefaultButtonSpacerHeight;
        
    } else {
        btnHeight = 0;
        btnSpacerHeight = 0;
    }
    CGFloat dialogWidth = kAlertDefaultTotalWidth;
    CGFloat dialogHeight  = self.containerView.frame.size.height + btnHeight + btnSpacerHeight ;
    
    return CGSizeMake(dialogWidth, dialogHeight);
}

- (UITextView *)contentTextView
{
    if (!_contentTextView) {
        _contentTextView = [[UITextView alloc]init];
        _contentTextView.showsVerticalScrollIndicator = NO;
        _contentTextView.textAlignment = NSTextAlignmentCenter;
        _contentTextView.textColor = ALERT_CONTENT_TEXT_COLOR;
        _contentTextView.font = ALERT_CONTENT_TEXT_FONT;
        _contentTextView.editable = NO;
        _contentTextView.selectable = NO;
    }
    return _contentTextView;
}
- (NSMutableArray *)btnArray
{
    if (!_btnArray) {
        _btnArray = [@[]mutableCopy];
    }
    return _btnArray;
}
- (UIView *)containerView
{
    if (!_containerView) {
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kAlertDefaultTotalWidth, 150)];
        _containerView.layer.cornerRadius = kAlertViewCornerRadius;
        _containerView.clipsToBounds = YES;
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}
- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = ALERT_TITLE_TEXT_COLOR;
        _titleLabel.font = ALERT_TITLE_TEXT_FONT;
    }
    return _titleLabel;
}

-(UIImageView *)imgView
{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] init];
    }
    return _imgView;
}

- (UIButton*)closeBtn
{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn setImage:[UIImage imageNamed:@"webview_close"] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(closeAlert) forControlEvents:UIControlEventTouchUpInside];
        _closeBtn.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _closeBtn;
}

- (void)closeAlert {
    [self close];
//    [[NSNotificationCenter defaultCenter] postNotificationName:kCustomVipAlertClosedNoti object:nil userInfo:@{@"isClose":@1}];
}

- (void)close
{
    // 点击button后自动dismiss
    if (self) {
        [self dismissViewControllerAnimated:YES  completion:nil];
    }
}

@end
