//
//  YKAlertView.m
//  KDFDApp
//
//  Created by hongyu on 2017/8/28.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import "YKAccessAlertView.h"
#import "YKContantsListCell.h"
#import "UIView+YKView.h"

static const CGFloat bgViewAlpla   = 0.6f;
static const NSInteger num_default = -1;
@interface YKAccessAlertView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIView      *bgView;
// 版本更新UI
@property (nonatomic, strong) UIView      *subView;
@property (nonatomic, strong) UIButton    *upDateButton;
@property (nonatomic, strong) UIImageView *rocketImageView;
@property (nonatomic, strong) UILabel     *titleLabel;
@property (nonatomic, strong) UITextView  *upDateTitleTextView;
@property (nonatomic, strong) UIButton    *closeButton;
// 通知推送UI
@property (nonatomic, strong) UIView      *notificationView;
@property (nonatomic, strong) UILabel     *notificationTitleLabel;
@property (nonatomic, strong) UILabel     *detailTextLabel;
@property (nonatomic, strong) UIImageView *notificationImageView;
@property (nonatomic, strong) UIButton    *openButton;
@property (nonatomic, strong) UIButton    *notificationCloseButton;
@property (nonatomic, copy)   NSString    *alertContentText;
@property (nonatomic, assign) YKShowAlertType alertType;

// 人脸识别UI

// 学生提示UI
@property (nonatomic, strong) UIView      *studentSubView;
@property (nonatomic, strong) UIImageView *studentImageView;
@property (nonatomic, strong) UILabel     *studentLabel;
@property (nonatomic, strong) UIButton    *studentBottomButton;
@property (nonatomic, strong) UIButton    *studentCloseButton;
// 多联系人UI
@property (nonatomic, strong) UIView      *contantsSubView;
@property (nonatomic, strong) UILabel     *contantsTitleLabel;
@property (nonatomic, strong) UILabel     *contantsContentLabel;
@property (nonatomic, strong) UITableView *contantsListView;
@property (nonatomic, strong) UIButton    *contantsSelectButton;
@property (nonatomic, strong) NSArray     *contantsListArray;
@property (nonatomic, assign) NSInteger   selectedIndex;
@property (nonatomic, copy) NSString      *selectContantsPhone;
@end

@implementation YKAccessAlertView

+ (instancetype)sharedAlertManager
{
    static dispatch_once_t onceToken;
    static YKAccessAlertView *alertView;
    dispatch_once(&onceToken, ^{
        alertView = [YKAccessAlertView new];
    });
    return alertView;
}

- (instancetype)initWithAlertType:(YKShowAlertType)type text:(NSString *)text
{
    if (self = [super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.alpha = 0;
        [self addSubview:self.bgView];
        self.alertType = type;
        switch (type) {
            case YKShowAlertTypeUpdate:
                [self prepareUpdateUIWithText:text];
                break;
            case YKShowAlertTypeFace:
                [self prepareFaceUI];
                break;
            case YKShowAlertTypeStudent:
                [self prepareStudentMessageUI];
                break;
            case YKShowAlertTypeContactsPhone:
                [self prepareContactsPhoneUIWithText:text];
                break;
            case YKShowAlertTypeGestureForgetPsw:
                [self prepareGestureForgetPswUI];
                break;
            default:
                [self preparePermissionsUI]; //各种权限类的弹框
            break;
        }
    }
    return self;
}

- (instancetype)showAlertType:(YKShowAlertType)type contentText:(NSString *)text {
    YKAccessAlertView *alertView = [[YKAccessAlertView sharedAlertManager] initWithAlertType:type text:text];
    [alertView showAlertView];
    return alertView;
}

- (void)showAlertView
{
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgView];
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismissAlertView
{
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.bgView yk_removeAllSubViews];
        [self yk_removeAllSubViews];
    }];
}

- (void)prepareUpdateUIWithText:(NSString *)text
{
    [self.bgView addSubview:self.subView];
    [self.subView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.bgView).with.insets(UIEdgeInsetsMake(190 * ASPECT_RATIO_WIDTH, 38 * ASPECT_RATIO_WIDTH, (IS_iPhoneX ? 200 : 126) * ASPECT_RATIO_WIDTH, 38 * ASPECT_RATIO_WIDTH));
    }];
    [self.subView addSubview:self.upDateButton];
    [self.upDateButton addTarget:self
                          action:@selector(upDateButtonAction:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self.upDateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.subView.mas_left).offset(16 * ASPECT_RATIO_WIDTH);
        make.right.equalTo(self.subView.mas_right).offset(-16 * ASPECT_RATIO_WIDTH);
        make.bottom.equalTo(self.subView.mas_bottom).offset(-26 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(50 * ASPECT_RATIO_WIDTH);
    }];
    [self.subView addSubview:self.rocketImageView];
    [self.rocketImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.subView.mas_top).offset(-55);
        make.left.equalTo(self.subView.mas_left).offset(0);
        make.right.equalTo(self.subView.mas_right).offset(0);
        make.height.equalTo(self.subView.mas_height).multipliedBy(IS_iPhoneX ? 0.45 : 0.53);
    }];
    [self.subView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.rocketImageView.mas_bottom).offset(9 * ASPECT_RATIO_WIDTH);
        make.left.right.offset(0);
        make.height.mas_offset(27 * ASPECT_RATIO_WIDTH);
    }];
    self.upDateTitleTextView.text = text;
    [self.subView addSubview:self.upDateTitleTextView];
    [self.upDateTitleTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10 * ASPECT_RATIO_WIDTH);
        make.left.equalTo(self.subView.mas_left).offset(20 * ASPECT_RATIO_WIDTH);
        make.right.equalTo(self.subView.mas_right).offset(-20 * ASPECT_RATIO_WIDTH);
        make.bottom.equalTo(self.upDateButton.mas_top).offset(0);
    }];
    [self.bgView addSubview:self.closeButton];
    [self.closeButton addTarget:self
                         action:@selector(closeAction:)
               forControlEvents:UIControlEventTouchUpInside];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        if (IS_iPhone5) {
            make.bottom.equalTo(self.subView.mas_top).offset(-15 * ASPECT_RATIO_WIDTH);
        } else if (IS_iPhoneX) {
            make.bottom.equalTo(self.subView.mas_top).offset(-5 * ASPECT_RATIO_WIDTH);
        } else {
            make.bottom.equalTo(self.subView.mas_top).offset(0);
        }
        make.right.equalTo(self.bgView.mas_right).offset(-37 * ASPECT_RATIO_WIDTH);
        make.width.mas_offset(28 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(41 * ASPECT_RATIO_WIDTH);
    }];
    [self addRoundedCorners:UIRectCornerBottomRight | UIRectCornerBottomLeft view:self.upDateButton radii:CGSizeMake(5, 5) viewRect:CGRectMake(0, 0, WIDTH_OF_SCREEN - 100 * ASPECT_RATIO_WIDTH, 50 * ASPECT_RATIO_WIDTH)];
}

- (void)addRoundedCorners:(UIRectCorner)corners
                     view:(UIView *)view
                    radii:(CGSize)radii
                 viewRect:(CGRect)rect
{
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:rect
                                                  byRoundingCorners:corners
                                                        cornerRadii:radii];
    CAShapeLayer *shape = [CAShapeLayer new];
    [shape setPath:rounded.CGPath];
    view.layer.mask = shape;
}

- (void)upDateButtonAction:(UIButton *)button
{
    [self dismissAlertView];
    NSString *str = @"http://itunes.apple.com/app/id1412595764?mt=8";
    if ([[UIDevice currentDevice].systemVersion doubleValue] < 7.0f) {
        str = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1412595764";
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (void)closeAction:(UIButton *)button
{
    [self dismissAlertView];
    if ([button isEqual:self.closeButton]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.alertType == YKShowAlertTypeUpdate) {
                if (self.upDateCloseBlock) {
                    self.upDateCloseBlock();
                }
            }
        });
    }
    if ([button isEqual:self.notificationCloseButton]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.alertType == YKShowAlertTypeHandGesture) {
                if (self.selectedCloseGestureBlock) {
                    self.selectedCloseGestureBlock();
                }
            }
            if (self.alertType == YKShowAlertTypePush) {
                if (self.selectedPushCloseBlock) {
                    self.selectedPushCloseBlock();
                }
            }
        });
    }
}

#pragma mark- 各种权限的弹框UI
- (void)preparePermissionsUI
{
    [self.bgView addSubview:self.notificationView];
    [self.notificationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView.mas_centerY).offset(0);
        make.left.offset(37 *ASPECT_RATIO_WIDTH);
        make.right.offset(-37 *ASPECT_RATIO_WIDTH);
        make.height.offset(168 *ASPECT_RATIO_WIDTH);
    }];

    [self.bgView addSubview:self.notificationImageView];
    [self.notificationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.notificationView.mas_top).offset(5);
        make.left.offset(37 *ASPECT_RATIO_WIDTH);
        make.right.offset(-37 *ASPECT_RATIO_WIDTH);
        make.height.mas_equalTo(192 *ASPECT_RATIO_WIDTH);
    }];
    
    [self.notificationView addSubview:self.notificationTitleLabel];
    [self.notificationTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.notificationImageView.mas_bottom).offset(22 * ASPECT_RATIO_WIDTH);
        make.left.equalTo(self.notificationView.mas_left).offset(0);
        make.right.equalTo(self.notificationView.mas_right).offset(0);
    }];
    [self.notificationView addSubview:self.detailTextLabel];
    [self.detailTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.notificationTitleLabel.mas_bottom).offset(4 * ASPECT_RATIO_WIDTH);
        make.left.equalTo(self.notificationView.mas_left).offset(0);
        make.right.equalTo(self.notificationView.mas_right).offset(0);
    }];
    
    [self.notificationView addSubview:self.openButton];
    [self.openButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.notificationView.mas_bottom).offset(-26 * ASPECT_RATIO_WIDTH);
        make.left.equalTo(self.notificationView.mas_left).offset(15 * ASPECT_RATIO_WIDTH);
        make.right.equalTo(self.notificationView.mas_right).offset(-15 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(50 * ASPECT_RATIO_WIDTH);
    }];
    [self.openButton addTarget:self action:@selector(openButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bgView addSubview:self.notificationCloseButton];
    [self.notificationCloseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.notificationImageView.mas_top).offset(10 * ASPECT_RATIO_WIDTH);
        make.right.equalTo(self.notificationImageView.mas_right);
        make.width.mas_offset(28 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(41 * ASPECT_RATIO_WIDTH);
    }];
    [self.notificationCloseButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.alertType == YKShowAlertTypeLocation) {
        self.notificationImageView.image = [UIImage imageNamed:@"locationAccess"];
        self.notificationTitleLabel.text = @"开启位置信息";
        self.detailTextLabel.text        = @"打开位置信息，便于享受该地区的优惠活动！";
    } else if (self.alertType == YKShowAlertTypeContactBook) {
        self.notificationImageView.image = [UIImage imageNamed:@"phoneBookAccess"];
        self.notificationTitleLabel.text = @"开启通讯录权限";
        self.detailTextLabel.text        = @"开启通讯录权限 以便更好的计算您的额度";
    } else if (self.alertType == YKShowAlertTypeCalendar) {
        self.notificationImageView.image = [UIImage imageNamed:@"calenderAccess"];
        self.notificationTitleLabel.text = @"开启日历提醒";
        self.detailTextLabel.text        = @"打开日历权限，不错过还款提醒";
    } else if (self.alertType == YKShowAlertTypeHandGesture) {
        self.notificationImageView.image = [UIImage imageNamed:@"gesturePswAccess"];
        self.notificationTitleLabel.text = @"开启手势密码";
        self.detailTextLabel.text        = @"为了您的账户安全，建议开启手势密码";
    } else if (self.alertType == YKShowAlertTypePush) {
        self.notificationImageView.image = [UIImage imageNamed:@"home_push_icon"];
        self.notificationTitleLabel.text = @"开启消息推送";
        self.detailTextLabel.text        = @"独家活动 优惠信息实时掌控 ";
    }
}

- (void)openButtonAction:(UIButton *)button
{
    if (self.alertType == YKShowAlertTypePush) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }else if (self.alertType == YKShowAlertTypeHandGesture) {
        if (self.openGestureBlock) {
            self.openGestureBlock();
        }
    }else {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
            if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 10.0) {
                if (@available(iOS 10.0, *)) {
                    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
     [self dismissAlertView];
}

- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _bgView.backgroundColor = [GRAY_COLOR_45 colorWithAlphaComponent:bgViewAlpla];
        _bgView.alpha = 0;
    }
    return _bgView;
}

- (UIView *)subView
{
    if (!_subView) {
        _subView = [UIView new];
        _subView.backgroundColor = [UIColor whiteColor];
        _subView.layer.cornerRadius = 3.f;
    }
    return _subView;
}

- (UIButton *)upDateButton
{
    if (!_upDateButton) {
        _upDateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_upDateButton setBackgroundImage:YK_IMAGE(@"btn_bg_image") forState:UIControlStateNormal];
        _upDateButton.clipsToBounds = YES;
        [_upDateButton setTitle:@"立即体验" forState:UIControlStateNormal];
        _upDateButton.titleLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(17)];
    }
    return _upDateButton;
}

- (UIImageView *)rocketImageView
{
    if (!_rocketImageView) {
        _rocketImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_update_icon"]];
        _rocketImageView.userInteractionEnabled = YES;
    }
    return _rocketImageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.text = @"更新内容";
        _titleLabel.textColor = GRAY_COLOR_45;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(17)];
    }
    return _titleLabel;
}

- (UITextView *)upDateTitleTextView
{
    if (!_upDateTitleTextView) {
        _upDateTitleTextView = [UITextView new];
        _upDateTitleTextView.editable = NO;
        _upDateTitleTextView.scrollEnabled = YES;
        _upDateTitleTextView.textAlignment = NSTextAlignmentCenter;
        _upDateTitleTextView.textColor = LABEL_TEXT_COLOR;
        _upDateTitleTextView.font = [UIFont systemFontOfSize:adaptFontSize(14)];
    }
    return _upDateTitleTextView;
}

- (UIButton *)closeButton
{
    if (!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setImage:[UIImage imageNamed:@"access_pop_close"] forState:UIControlStateNormal];
    }
    return _closeButton;
}

- (UIView *)notificationView
{
    if (!_notificationView) {
        _notificationView = [UIView new];
        _notificationView.layer.masksToBounds = YES;
        _notificationView.backgroundColor = [UIColor whiteColor];
        _notificationView.layer.cornerRadius  = 3.f;
        _notificationView.userInteractionEnabled = YES;
    }
    return _notificationView;
}

- (UILabel *)notificationTitleLabel
{
    if (!_notificationTitleLabel) {
        _notificationTitleLabel = [UILabel new];
        _notificationTitleLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(17)];
        _notificationTitleLabel.text = @"开启通讯录权限";
        _notificationTitleLabel.textColor     = GRAY_COLOR_45;
        _notificationTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _notificationTitleLabel;
}

- (UILabel *)detailTextLabel
{
    if (!_detailTextLabel) {
        _detailTextLabel = [UILabel new];
        _detailTextLabel.font = [UIFont systemFontOfSize:adaptFontSize(IS_iPhone5 ? 13 : 14)];
        _detailTextLabel.text = @"开启通讯录权限";
        _detailTextLabel.textColor     = Color.color_AD_C5;
        _detailTextLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _detailTextLabel;
}

- (UIImageView *)notificationImageView
{
    if (!_notificationImageView) {
        _notificationImageView = [UIImageView new];
        _notificationImageView.image = [UIImage imageNamed:@"icon_permission_notification"];
        _notificationImageView.contentMode = UIViewContentModeScaleToFill;
    }
    return _notificationImageView;
}

- (UIButton *)openButton
{
    if (!_openButton) {
        _openButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_openButton setTitle:@"立即开启" forState:UIControlStateNormal];
        [_openButton setBackgroundImage:[UIImage imageNamed:@"btn_bg_image"] forState:UIControlStateNormal];
        _openButton.titleLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(17)];
        _openButton.layer.cornerRadius = 5.f;
        _openButton.clipsToBounds = YES;
    }
    return _openButton;
}

- (UIButton *)notificationCloseButton
{
    if (!_notificationCloseButton) {
        _notificationCloseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_notificationCloseButton setImage:[UIImage imageNamed:@"access_pop_close"] forState:UIControlStateNormal];
        [_notificationCloseButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    }
    return _notificationCloseButton;
}

#pragma mark 人脸识别提示
- (void)prepareFaceUI
{
    WEAK_SELF
    UIImageView *faceImgView = [UIImageView yk_imageViewWithImageName:@"faceTip" superView:self.bgView masonrySet:^(UIImageView *imageView, MASConstraintMaker *make) {
        STRONG_SELF
        make.centerY.offset(-30);
        make.left.equalTo(strongSelf.bgView).offset(37*ASPECT_RATIO_WIDTH);
        make.right.equalTo(strongSelf.bgView).offset(-37*ASPECT_RATIO_WIDTH);
        make.height.offset(400 * ASPECT_RATIO_WIDTH);
    }];
    
    //确定btn
    [UIButton yk_buttonFontSize:19 textColor:Color.whiteColor backGroundImage:@"btn_bg_image" imageName:@"" cornerRadius:25*ASPECT_RATIO_WIDTH superView:self.bgView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        STRONG_SELF
        make.top.equalTo(faceImgView.mas_bottom).offset(26*ASPECT_RATIO_WIDTH);
        make.left.offset(50*ASPECT_RATIO_WIDTH);
        make.right.offset(-50*ASPECT_RATIO_WIDTH);
        make.height.mas_equalTo(50*ASPECT_RATIO_WIDTH);
        [button setTitle:@"我知道了" forState:UIControlStateNormal];
        [button addTarget:strongSelf action:@selector(identificationBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    //关闭btn
    [UIButton yk_buttonWithImageName:@"access_pop_close" superView:self.bgView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        STRONG_SELF
        make.bottom.equalTo(faceImgView.mas_top);
        make.right.equalTo(faceImgView.mas_right);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(42*ASPECT_RATIO_WIDTH);
        button.contentMode = UIViewContentModeRight;
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [button addTarget:strongSelf action:@selector(dismissAlertView) forControlEvents:UIControlEventTouchUpInside];
    }];
   
}
- (void)identificationBtnAction
{
    if (self.identificationAction) {
        self.identificationAction();
    }
    [self dismissAlertView];
}

#pragma mark- 学生提示
- (void)prepareStudentMessageUI
{
    [self.bgView addSubview:self.studentSubView];
    [self.studentSubView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView.mas_top).offset(103 * ASPECT_RATIO_HEIGHT);
        make.left.equalTo(self.bgView.mas_left).offset(36.5 * ASPECT_RATIO_WIDTH);
        make.right.equalTo(self.bgView.mas_right).offset(-36.5 * ASPECT_RATIO_WIDTH);
//        make.bottom.equalTo(self.bgView.mas_bottom).offset(-137 * ASPECT_RATIO_HEIGHT);
    }];
    [self.studentSubView addSubview:self.studentImageView];
    [self.studentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.studentSubView.mas_top).offset(45 * ASPECT_RATIO_HEIGHT);
        make.left.equalTo(self.studentSubView.mas_left).offset(0 * ASPECT_RATIO_WIDTH); //55
        make.right.equalTo(self.studentSubView.mas_right).offset(0 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(190 * ASPECT_RATIO_WIDTH);
    }];
    UIView *bottomBackView = [[UIView alloc] init];
    bottomBackView.backgroundColor = WHITE_COLOR;
    [self.studentSubView addSubview:bottomBackView];
    [bottomBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.studentImageView.mas_bottom);
        make.left.right.equalTo(self.studentSubView);
        make.bottom.equalTo(self.studentSubView);
    }];
    
    [bottomBackView addSubview:self.studentLabel];
    [self.studentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomBackView).offset(25 * ASPECT_RATIO_HEIGHT);
        make.left.equalTo(bottomBackView).offset(15 * ASPECT_RATIO_WIDTH);
        make.right.equalTo(bottomBackView).offset(-15 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(64 * ASPECT_RATIO_WIDTH);
    }];
    [bottomBackView addSubview:self.studentBottomButton];
    [self.studentBottomButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.studentLabel.mas_bottom).offset(15);
        make.left.equalTo(bottomBackView).offset(15);
        make.right.equalTo(bottomBackView).offset(-15);
        make.bottom.equalTo(bottomBackView.mas_bottom).offset(-25);
//        make.height.mas_offset(55 * ASPECT_RATIO_HEIGHT);
    }];
    [self.studentBottomButton addTarget:self action:@selector(studentBottomAction:) forControlEvents:UIControlEventTouchUpInside];
//    [self.bgView addSubview:self.studentCloseButton];
//    [self.studentCloseButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.studentSubView.mas_bottom).offset(15 * ASPECT_RATIO_WIDTH);
//        make.left.equalTo(self.bgView.mas_left).offset(WIDTH_OF_SCREEN / 2 - 15 * ASPECT_RATIO_WIDTH);
//        make.width.mas_offset(30 * ASPECT_RATIO_WIDTH);
//        make.height.mas_offset(30 * ASPECT_RATIO_WIDTH);
//    }];
//    [self.studentCloseButton addTarget:self action:@selector(studentCloseAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)studentBottomAction:(UIButton *)button
{
    if (self.studentBottomBlock) self.studentBottomBlock();
    [self dismissAlertView];
}

- (void)studentCloseAction:(UIButton *)button
{
    [self dismissAlertView];
}

- (UIView *)studentSubView
{
    if (!_studentSubView) {
        _studentSubView = [UIView new];
//        _studentSubView.layer.cornerRadius  = 16.f;
//        _studentSubView.layer.masksToBounds = YES;
        _studentSubView.backgroundColor = [UIColor clearColor];
    }
    return _studentSubView;
}

- (UIImageView *)studentImageView
{
    if (!_studentImageView) {
        _studentImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_student_message"]];
    }
    return _studentImageView;
}

- (UILabel *)studentLabel
{
    if (!_studentLabel) {
        _studentLabel = [UILabel new];
        _studentLabel.text = @"桔子优卡严格遵守相关法律法规，坚决不向学生提供贷款服务，请确认您已不是在校学生。";
        _studentLabel.textColor = Color.color_66_C3;
        _studentLabel.font = [UIFont systemFontOfSize:adaptFontSize(16)];
        _studentLabel.numberOfLines = 0;
    }
    return _studentLabel;
}

- (UIButton *)studentBottomButton
{
    if (!_studentBottomButton) {
        _studentBottomButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _studentBottomButton.backgroundColor = Color.color_8D_C4;//61cae4
        UIImage *image = [UIImage imageNamed:@"system_alertCommit_btn"];
        [_studentBottomButton setBackgroundImage:image forState:(UIControlStateNormal)];
        [_studentBottomButton setTitle:@"我已知悉" forState:(UIControlStateNormal)];
        _studentBottomButton.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(17)];
        [_studentBottomButton setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
        _studentBottomButton.layer.cornerRadius = image.size.height * 0.5;
        _studentBottomButton.layer.masksToBounds = YES;
    }
    return _studentBottomButton;
}

- (UIButton *)studentCloseButton
{
    if (!_studentCloseButton) {
        _studentCloseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_studentCloseButton setImage:[UIImage imageNamed:@"icon_student_close"] forState:UIControlStateNormal];
    }
    return _studentCloseButton;
}

#pragma mark- 多联系人选择视图
- (void)prepareContactsPhoneUIWithText:(NSString *)text
{
    self.selectedIndex = num_default;
    self.selectContantsPhone = @"";
    [self.bgView addSubview:self.contantsSubView];
    [self.contantsSubView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView.mas_top).offset(122 * ASPECT_RATIO_HEIGHT);
        make.left.equalTo(self.bgView.mas_left).offset(37 * ASPECT_RATIO_WIDTH);
        make.right.equalTo(self.bgView.mas_right).offset(-37 * ASPECT_RATIO_WIDTH);
        make.bottom.equalTo(self.bgView.mas_bottom).offset(-224 * ASPECT_RATIO_HEIGHT);
    }];
    [self.contantsSubView addSubview:self.contantsTitleLabel];
    [self.contantsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contantsSubView.mas_top).offset(20 * ASPECT_RATIO_HEIGHT);
        make.left.equalTo(self.contantsSubView.mas_left).offset(0);
        make.right.equalTo(self.contantsSubView.mas_right).offset(0);
        make.height.mas_offset(16 * ASPECT_RATIO_HEIGHT);
    }];
    [self.contantsSubView addSubview:self.contantsContentLabel];
    [self.contantsContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contantsTitleLabel.mas_bottom).offset(25 * ASPECT_RATIO_HEIGHT);
        make.left.equalTo(self.contantsSubView.mas_left).offset(15 * ASPECT_RATIO_WIDTH);
        make.right.equalTo(self.contantsSubView.mas_right).offset(-15 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(38 * ASPECT_RATIO_HEIGHT);
    }];
    [self.contantsSubView addSubview:self.contantsListView];
    [self.contantsListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contantsContentLabel.mas_bottom).offset(0);
        make.left.equalTo(self.contantsSubView.mas_left).offset(38 * ASPECT_RATIO_WIDTH);
        make.right.equalTo(self.contantsSubView.mas_right).offset(-38 * ASPECT_RATIO_WIDTH);
        make.bottom.equalTo(self.contantsSubView.mas_bottom).offset(-55 * ASPECT_RATIO_HEIGHT);
    }];
    [self.contantsSubView addSubview:self.contantsSelectButton];
    [self.contantsSelectButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contantsSubView.mas_left).offset(0);
        make.right.equalTo(self.contantsSubView.mas_right).offset(0);
        make.bottom.equalTo(self.contantsSubView.mas_bottom).offset(0);
        make.height.mas_offset(55 * ASPECT_RATIO_HEIGHT);
    }];
    [self.contantsSelectButton addTarget:self action:@selector(selectContantsPhoneAction:) forControlEvents:UIControlEventTouchUpInside];
    self.contantsSelectButton.backgroundColor = Color.color_CC_C8;
    self.contantsListArray = [text componentsSeparatedByString:@":"];
    [self.contantsListView reloadData];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.contantsListView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60 * ASPECT_RATIO_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.contantsListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YKContantsListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YKContantsListCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.telephoneLabel.text = self.contantsListArray[indexPath.row];
    cell.telephoneLabel.layer.masksToBounds = YES;
    cell.telephoneLabel.layer.cornerRadius  = 3.5f;
    cell.telephoneLabel.layer.borderWidth   = 1;
    cell.telephoneLabel.layer.borderColor   = Color.main.CGColor;
    if (self.selectedIndex == indexPath.row) {
        [self prepareSelectCell:cell];
    } else {
        [self prepareDeselectCell:cell];
    }
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"新Cell将要被选中");
    return indexPath;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"即将取消上一个选中	");
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"选中新cell");
    self.selectedIndex = indexPath.row;
    YKContantsListCell *cell = (YKContantsListCell *)[tableView cellForRowAtIndexPath:indexPath];
    self.selectContantsPhone = cell.telephoneLabel.text;
    self.contantsSelectButton.backgroundColor = Color.main;
    [self prepareSelectCell:cell];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"取消上一个选中	");
    YKContantsListCell *cell = (YKContantsListCell *)[tableView cellForRowAtIndexPath:indexPath];
    [self prepareDeselectCell:cell];
}

- (void)prepareSelectCell:(YKContantsListCell *)cell
{
    cell.telephoneLabel.backgroundColor = Color.main;
    cell.telephoneLabel.textColor = [UIColor whiteColor];
    cell.telephoneLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(14)];
    cell.telephoneLabel.layer.borderWidth = 0;
}

- (void)prepareDeselectCell:(YKContantsListCell *)cell
{
    cell.telephoneLabel.backgroundColor = [UIColor whiteColor];
    cell.telephoneLabel.textColor = Color.color_66_C3;
    cell.telephoneLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
    cell.telephoneLabel.layer.borderWidth = 1;
}

- (void)selectContantsPhoneAction:(UIButton *)button
{
    if ([self.selectContantsPhone isEqualToString:@""]) {
        button.backgroundColor = Color.color_CC_C8;
        return;
    } else {
        button.backgroundColor = Color.main;
    }
    if (self.selectContantsPhoneBlock) self.selectContantsPhoneBlock(self.selectContantsPhone);
    [self dismissAlertView];
}

- (UIView *)contantsSubView
{
    if (!_contantsSubView) {
        _contantsSubView = [UIView new];
        _contantsSubView.layer.cornerRadius  = 5.f;
        _contantsSubView.layer.masksToBounds = YES;
        _contantsSubView.backgroundColor = [UIColor whiteColor];
    }
    return _contantsSubView;
}

- (UILabel *)contantsTitleLabel
{
    if (!_contantsTitleLabel) {
        _contantsTitleLabel = [UILabel new];
        _contantsTitleLabel.text = @"选择联系人号码";
        _contantsTitleLabel.textColor = Color.color_66_C3;
        _contantsTitleLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(16)];
        _contantsTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _contantsTitleLabel;
}

- (UILabel *)contantsContentLabel
{
    if (!_contantsContentLabel) {
        _contantsContentLabel = [UILabel new];
        _contantsContentLabel.text = @"该联系人有多个手机号，请选择一个常用手机号：";
        _contantsContentLabel.textColor = Color.color_66_C3;
        _contantsContentLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
        _contantsContentLabel.numberOfLines = 0;
    }
    return _contantsContentLabel;
}

- (UITableView *)contantsListView
{
    if (!_contantsListView) {
        _contantsListView = [[UITableView alloc] init];
        _contantsListView.separatorStyle  = UITableViewCellSeparatorStyleNone;
        _contantsListView.tableFooterView = [UIView new];
        _contantsListView.delegate = self;
        _contantsListView.dataSource = self;
        _contantsListView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_contantsListView registerNib:[UINib nibWithNibName:@"YKContantsListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"YKContantsListCell"];
    }
    return _contantsListView;
}

- (UIButton *)contantsSelectButton
{
    if (!_contantsSelectButton) {
        _contantsSelectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _contantsSelectButton.backgroundColor = Color.main;
        [_contantsSelectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_contantsSelectButton setTitle:@"确认选择" forState:UIControlStateNormal];
        _contantsSelectButton.titleLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(16)];
    }
    return _contantsSelectButton;
}

#pragma mark - 手势忘记密码
- (void)prepareGestureForgetPswUI
{
    UIView *gestureView = [UIView yk_viewWithColor:[UIColor whiteColor] superView:self.bgView masonrySet:^(UIView *view, MASConstraintMaker *make) {
        make.top.equalTo(self.bgView.mas_top).offset(172 * ASPECT_RATIO_WIDTH);
        make.width.offset(300* ASPECT_RATIO_WIDTH);
        make.height.offset(170 * ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(self.bgView);
        view.layer.cornerRadius = 5;
        view.clipsToBounds = YES;
    }];
    UILabel *titleLabel = [UILabel yk_labelWithFontSize:adaptFontSize(18) textColor:Color.color_33_C8 superView:gestureView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.offset(50 *ASPECT_RATIO_WIDTH);
       label.textAlignment = NSTextAlignmentCenter;
       label.text = @"手势密码已关闭,请重新登录";
    }];
    
    UILabel *contentLabel = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_AD_C5 superView:gestureView masonrySet:^(UILabel *label, MASConstraintMaker *make){
        make.top.equalTo(titleLabel.mas_bottom);
        make.left.right.offset(0);
        make.height.offset(70 *ASPECT_RATIO_WIDTH);
        label.text = @"在个人中心-设置中,可开启手势密码";
        label.textAlignment = NSTextAlignmentCenter;
    }];
    
    UIButton *sureButton = [UIButton yk_buttonFontSize:adaptFontSize(17) textColor:[UIColor whiteColor] backGroundImage:@"" imageName:@"" cornerRadius:0 superView:gestureView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        make.top.equalTo(contentLabel.mas_bottom);
        make.left.right.offset(0);
        make.height.offset(50 *ASPECT_RATIO_WIDTH);
    }];
    [sureButton setTitle:@"确定" forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(gestureSureBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
}
- (void)gestureSureBtnClick
{
     [self dismissAlertView];
    if (self.forgetGestureBlock) {
        self.forgetGestureBlock();
    }
}
@end
