//
//  QLSingleButtonAlertView.m
//  QuickLoan
//
//  Created by Jeremy on 2018/4/12.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QLSingleButtonAlertView.h"
#define ALERT_BUTTON_TEXT_FONT     [UIFont systemFontOfSize:adaptFontSize(16)]

@interface QLSingleButtonAlertView()

@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UIButton *confirmButton;
@property (nonatomic, copy) void(^action_blk)(void);

@end

@implementation QLSingleButtonAlertView

+ (void(^)(NSString *message))showAlertAction:(void(^)(void))action
{
    return ^(NSString *message){
        if (![message yk_isValidString]) {
            [[iToast makeText:@"抱歉，暂时没有数据展示"] show];
            return;
        }
        QLSingleButtonAlertView *singleAlert = [QLSingleButtonAlertView new];
        singleAlert.updateAlertView(message);
        singleAlert.showAlert();
        [singleAlert setAction_blk:^{
            if (action) {
                action();
            }
        }];
    };
}


-(void)configUI
{
    [super configUI];
    UIButton *closeBtn = [UIButton yk_buttonWithImageName:@"webview_close"
                                                superView:self.alertBackGroundView
                                               masonrySet:^(UIButton *button, MASConstraintMaker *make) {
                                                   make.width.height.offset(30);
                                                   make.top.offset(12);
                                                   make.right.offset(-15);
                                                   button.contentMode = UIViewContentModeScaleAspectFit;
                                                   [button addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
                                               }];
    
    [closeBtn.titleLabel setFont:ALERT_BUTTON_TEXT_FONT];
    
    UITextView *textView = [UITextView new];
    textView.editable = NO;
    textView.selectable = NO;
    textView.font = [UIFont systemFontOfSize:adaptFontSize(14)];
    textView.textColor = [QBColor yk_color_66_c3];
    textView.textAlignment = NSTextAlignmentCenter;
    [self.alertBackGroundView addSubview:textView];
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.equalTo(closeBtn.mas_bottom).offset(10);
        make.height.offset(30);
        make.left.offset(23*ASPECT_RATIO_WIDTH);
        make.right.offset(-23*ASPECT_RATIO_WIDTH);
    }];
    self.textView = textView;
    
    self.confirmButton = [UIButton yk_buttonFontSize:adaptFontSize(18)  textColor:WHITE_COLOR backGroundColor:MAIN_THEME_COLOR imageName:@"" superView:self.alertBackGroundView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        button.layer.cornerRadius = 5;
        button.layer.masksToBounds = YES;
        [button setTitle:@"去赚现金" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonTouchAction) forControlEvents:UIControlEventTouchUpInside];
        make.top.equalTo(self.textView.mas_bottom).offset(15*ASPECT_RATIO_WIDTH);
        make.left.offset(15*ASPECT_RATIO_WIDTH);
        make.right.offset(-15*ASPECT_RATIO_WIDTH);
        make.height.offset(45*ASPECT_RATIO_WIDTH);
        make.bottom.offset(-25*ASPECT_RATIO_WIDTH);
    }];
    [self.confirmButton.titleLabel setFont:ALERT_BUTTON_TEXT_FONT];
}

- (void)buttonTouchAction
{
    if (self.action_blk) {
        self.action_blk();
        [self dismiss];
        self.action_blk = nil;  //避免引用循环
    }
}

- (void)updateUI:(id)model
{
    NSString *tmpStr = (NSString *)model;
    self.textView.text = tmpStr;
}


@end
