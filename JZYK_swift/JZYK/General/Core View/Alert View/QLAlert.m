//
//  QLAlert.m
//  QuickLoan
//
//  Created by Jeremy Wang on 2018/4/10.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QLAlert.h"
#import "QLAlertConst.h"
#import "QLAlertMacros.h"
#import "QLAlertViewController.h"

@interface QLAlert()

@property (nonatomic, strong) UIView *alertView;
@property (nonatomic, strong) UILabel *titleLabel;        //标题
@property (nonatomic, strong) UITextView *contentTextView;//内容说明
@property (nonatomic, assign) CGFloat containerViewHeight;
@property (nonatomic, strong) NSMutableArray *btnArray;
@property (nonatomic, strong) UIImageView *imgView;       //有图片+内容
@property (nonatomic, strong) UIButton *closeBtn;
@property (nonatomic, strong) UIButton *aboutLineBankBtn;//关于存管的btn

@end

@implementation QLAlert

CGFloat buttonHeight = 0;
CGFloat buttonSpacerHeight = 0;
CGFloat buttonBottomPadding = 0;

+ (instancetype)alert
{
    static dispatch_once_t predicate;
    static QLAlert *manager = nil;
    dispatch_once(&predicate, ^{
        manager = [QLAlert new];
    });
    return manager;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.frame = [UIScreen mainScreen].bounds;
    }
    return self;
}

- (void)externalCustomUIWithButtonTitleArray : (NSArray <NSString *> *)buttonTitleArray
                                buttonClicked:(void (^) (NSInteger index))buttonClicked
{
    _closeOnTouchUpOutside = NO;
    _buttonTitles = buttonTitleArray;
    _onButtonTouchUpInside = buttonClicked;
    _alertViewType = QLAlertViewTypeCustomized;
}

- (void)prepareUIWithContentText:(NSString *)content
               buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
                   buttonClicked:(void (^) (NSInteger index))buttonClicked
{
    
    [self prepareUIWithTitle:nil contentText:content buttonTitleArray:buttonTitleArray buttonClicked:buttonClicked];
}

- (void)prepareUIWithTitle:(NSString *)title
               contentText:(NSString *)content
         buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
             buttonClicked:(void (^) (NSInteger index))buttonClicked
{
    [self prepareUIWithTitle:title titleIsHtml:NO contentText:content contentIsHtml:NO buttonTitleArray:buttonTitleArray buttonClicked:buttonClicked];

}
- (void)prepareUIWithTitle:(NSObject *)title titleIsHtml:(BOOL)titleIsHtml
               contentText:(NSObject *)content contentIsHtml:(BOOL)contentIsHtml
         buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
             buttonClicked:(void (^) (NSInteger index))buttonClicked{
    _closeOnTouchUpOutside = NO;
    _buttonTitles = buttonTitleArray;
    _onButtonTouchUpInside = buttonClicked;
    _alertViewType = QLAlertViewTypeDefault;
    
    if (titleIsHtml) {
        self.titleLabel.attributedText = (NSAttributedString *)title;
    }else{
        self.titleLabel.text = (NSString *)title;
    }
    if (contentIsHtml) {
        self.contentTextView.attributedText = (NSAttributedString *)content;
    }else{
        self.contentTextView.text = (NSString *)content;
    }
    
    [self.alertView addSubview:self.containerView];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.contentTextView];
    
    // 添加约束 titleLabel
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(kAlertDefaultContainerWidth);
        make.centerX.equalTo(self.containerView);
        
        
        make.top.offset(kAlertDefaultTitleTop);
    }];
    
    // 添加约束 contentTextView
    CGFloat textViewHeight = [self.contentTextView sizeThatFits:CGSizeMake(kAlertDefaultContainerWidth, 0)].height;
    CGFloat titleHeight = [self.titleLabel sizeThatFits:CGSizeMake(kAlertDefaultContainerWidth, 0)].height;
    if (textViewHeight < 50) {
        textViewHeight = 50;
    }
    [self.contentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(kAlertDefaultContainerWidth);
        make.centerX.equalTo(self.containerView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(kAlertDefaultPadding);
        make.bottom.offset(0);
        make.height.offset(textViewHeight);
    }];
    
    // 添加约束
    self.containerViewHeight = textViewHeight + titleHeight + kAlertDefaultPadding + kAlertDefaultTitleTop;
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.centerX.equalTo(self.alertView);
        make.width.offset(kAlertDefaultContainerWidth);
        make.height.offset(self.containerViewHeight).priorityHigh();
    }];
}
//MARK:一个图片+内容的弹框
- (void)prepareUIImageName:(NSString *)imageName
                   title  :(NSString *)title
                   content:(NSString *)content
         buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
             buttonClicked:(void (^) (NSInteger index))buttonClicked
{
    
    _closeOnTouchUpOutside = NO;
    _buttonTitles = buttonTitleArray;
    _onButtonTouchUpInside = buttonClicked;
    _alertViewType = QLAlertViewTypeDefault;
    
    self.titleLabel.text = title;
    self.contentTextView.text = content;
    
    [self.alertView addSubview:self.containerView];
    [self.containerView addSubview:self.imgView];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.contentTextView];
    [self.containerView addSubview:self.closeBtn];
    
    self.imgView.image = [UIImage imageNamed:imageName];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.offset(0);
        make.height.offset(140);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(kAlertDefaultContainerWidth);
        make.centerX.equalTo(self.containerView);
        make.top.equalTo(self.imgView.mas_bottom).offset(kAlertDefaultPadding);
        make.bottom.equalTo(self.contentTextView.mas_top).offset(-kAlertDefaultPadding);
    }];
    
    CGFloat textViewHeight = [self.contentTextView sizeThatFits:CGSizeMake(kAlertDefaultContainerWidth, 0)].height;
    [self.contentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.containerView);
        make.width.offset(kAlertDefaultContainerWidth);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(kAlertDefaultPadding);
        make.height.offset(textViewHeight);
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-kAlertDefaultPadding);
        make.top.offset(kAlertDefaultPadding);
        make.width.height.offset(30);
    }];
    
    self.containerViewHeight = 140 + textViewHeight +[self.titleLabel sizeThatFits:CGSizeMake(kAlertDefaultContainerWidth, 0)].height +kAlertDefaultPadding*2;
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.centerX.equalTo(self.alertView);
        make.width.offset(kAlertDefaultContainerWidth);
        make.height.offset(self.containerViewHeight).priorityHigh();
    }];
}

- (void)prepareUIWithTitle:(NSString *)title
               contentText:(NSString *)content
          topRightCloseBtn:(BOOL)hasCloseBtn
     aboutLineBankBtnTitle:(NSString *)lineBankBtnTitle
         buttonWithBgImage:(BOOL)hasBgImage
         buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
             buttonClicked:(void (^) (NSInteger index))buttonClicked
   aboutLineBankBtnClicked:(void (^) (void))aboutLineBankBtnClicked
        closeButtonClicked:(void (^) (void))closeBtnClicked
{
    [self prepareUIWithTitle:title contentText:content buttonTitleArray:buttonTitleArray buttonClicked:buttonClicked];
    
    self.btnHasBgImage = hasBgImage;
    if (hasCloseBtn) {
        _closeButtonTouchUpInside = closeBtnClicked;
        [self.alertView addSubview:self.closeBtn];
        [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5);
            make.top.offset(5);
            make.width.height.offset(32);
        }];
    }
    if (lineBankBtnTitle.length > 0) {
        _aboutLineBankBtnTouchUpInside = aboutLineBankBtnClicked;
        [self.containerView addSubview:self.aboutLineBankBtn];
        [self.aboutLineBankBtn setTitle:lineBankBtnTitle forState:UIControlStateNormal];
        [self.aboutLineBankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentTextView.mas_bottom).offset(kAlertBankButtonTopPadding);
            make.right.equalTo(self.contentTextView.mas_right);
            make.height.mas_equalTo(kAlertBankButtonHeight);
            make.bottom.offset(-kAlertBankButtonBottomPadding);
        }];
        
        CGFloat addByHeight = kAlertBankButtonTopPadding + kAlertBankButtonHeight + kAlertBankButtonBottomPadding;
        //更新内容的下边距约束
        [self.contentTextView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-addByHeight);
        }];
        
        //更新containerView的高度
        self.containerViewHeight = self.containerViewHeight + addByHeight;
        [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.offset(self.containerViewHeight).priorityHigh();
        }];
    }
}

#pragma mark 在外部设置显示在keyWindow上alertView的属性
- (void)setTitleTextColor:(UIColor *)titleTextColor
{
    if (titleTextColor)self.titleLabel.textColor = titleTextColor;
}
- (void)setContentTextColor:(UIColor *)contentTextColor
{
    if (contentTextColor)self.contentTextView.textColor = contentTextColor;
}
- (void)setTitleTextFont:(UIFont *)titleTextFont
{
    if (titleTextFont)self.titleLabel.font = titleTextFont;
}
- (void)setContentTextFont:(UIFont *)contentTextFont
{
    if (contentTextFont)self.contentTextView.font = contentTextFont;
}
- (void)setTitleAttributedText:(NSAttributedString *)titleAttributedText
{
    if (titleAttributedText) self.titleLabel.attributedText = titleAttributedText;
}
- (void)setContentAttributedText:(NSAttributedString *)contentAttributedText
{
    if (contentAttributedText)self.contentTextView.attributedText = contentAttributedText;
}
- (void)setBtnTitleColor:(UIColor *)btnTitleColor
{
    if (btnTitleColor){
        for (UIButton * closeBtn in self.btnArray) {
            [closeBtn setTitleColor:btnTitleColor forState:UIControlStateNormal];
        }
    }
}
- (void)setBtnTitleFont:(UIFont *)btnTitleFont
{
    if (btnTitleFont) {
        for (UIButton * closeBtn in self.btnArray) {
            [closeBtn.titleLabel setFont:btnTitleFont];
        }
    }
}
- (void)setBtnBgColor:(UIColor *)btnBgColor
{
    if (btnBgColor) {
        for (UIButton * closeBtn in self.btnArray) {
            closeBtn.backgroundColor = btnBgColor;
        }
    }
}

#pragma mark 展示在keyWindow上的alertView的显示
- (void)show
{
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [[UIApplication sharedApplication].keyWindow addSubview:self.alertView];
    if (_alertViewType == QLAlertViewTypeCustomized) {
        [self.alertView addSubview:self.containerView];
        
    }else{
        [self.alertView mas_updateConstraints:^(MASConstraintMaker *make) {
            [make center];
            make.width.offset(kAlertDefaultTotalWidth);
//            make.height.offset(self.containerViewHeight + buttonHeight).priorityHigh();
        }];
    }
    [self addButtonsToView:self.alertView];
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
                         self.alertView.layer.opacity = 1.0f;
                         self.alertView.layer.transform = CATransform3DMakeScale(1, 1, 1);
                     }
                     completion:NULL
     ];
    
}

- (void)dismissAlert {
    [self close];
}


#pragma mark 展示在keyWindow上的alertView的关闭
- (void)close
{
    if (self.closeButtonTouchUpInside) {
        self.closeButtonTouchUpInside();
    }
    CATransform3D currentTransform = self.alertView.layer.transform;
    self.alertView.layer.opacity = 1.0f;
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
                         self.alertView.layer.transform = CATransform3DConcat(currentTransform, CATransform3DMakeScale(0.6f, 0.6f, 1.0));
                         self.alertView.layer.opacity = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         for (UIView *v in [self.alertView subviews]) {
                             [v removeFromSuperview];
                         }
                         for (UIView *v in [self subviews]) {
                             [v removeFromSuperview];
                         }
                         for (UIView *v in [self.containerView subviews]) {
                             [v removeFromSuperview];
                         }
                         [self.alertView removeFromSuperview];
                         [self removeFromSuperview];
                         [self.containerView removeFromSuperview];
                         self.containerViewHeight = 0;
                         self.buttonTitles = nil;
                     }
     ];
}
#pragma mark 展示在keyWindow上的alertView上的底部按钮
- (void)addButtonsToView:(UIView *)container
{
    if (_buttonTitles==NULL) return;
    [self addAssertions];
    
    if (_buttonTitles && [_buttonTitles count] == 2) {
        buttonHeight       = kAlertDefaultButtonHeight;
        buttonSpacerHeight = kAlertDefaultButtonSpacerHeight;
    }else if (_buttonTitles && [_buttonTitles count] == 1){
        buttonHeight       = kAlertDefaultButtonHeight;
        buttonSpacerHeight = 0;
    }else {  //_buttonTitles=@[],快捷的改一下只有标题、内容没有按钮的情况
        buttonHeight = 0;
        buttonSpacerHeight = 0;
        UIButton *noButton = [UIButton new];
        [self.alertView addSubview:noButton];
        [noButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.offset(0);
            make.right.offset(0) ;
            make.bottom.offset(-35);
            make.height.offset(0);
            make.top.equalTo(self.containerView.mas_bottom);
        }];
    }
    
    if (self.btnHasBgImage) {
        buttonBottomPadding = -kAlertDefaultButtonBottomPadding;
    } else{
        buttonBottomPadding = 0;
    }
    
    CGFloat buttonWidth = (kAlertDefaultContainerWidth - buttonSpacerHeight) / [_buttonTitles count];
    
    for (int i=0; i<[_buttonTitles count]; i++) {
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeButton addTarget:self action:@selector(buttonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [closeButton setTag:i];
        [closeButton setTitle:[_buttonTitles objectAtIndex:i] forState:UIControlStateNormal];
        [closeButton setTitleColor:i == 1 ? ALERT_CANCEL_COLOR: ALERT_CONFIRM_COLOR  forState:UIControlStateNormal];
        [closeButton.titleLabel setFont:ALERT_BUTTON_TEXT_FONT];
        closeButton.titleLabel.numberOfLines = 0;
        closeButton.layer.cornerRadius = kAlertViewCornerRadius;
        closeButton.clipsToBounds = YES;
        if (self.btnHasBgImage) { //按钮有背景图片
            [closeButton setBackgroundImage:[UIImage imageNamed:@"btn_bg_image"] forState:UIControlStateNormal];
            [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        [self.alertView addSubview:closeButton];
        [closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.offset(buttonWidth);
            make.right.offset( - i *( buttonWidth + buttonSpacerHeight ) - kAlertDefaultContentLeftRightPadding);
            make.bottom.offset(buttonBottomPadding);
            make.height.offset(buttonHeight);
            make.top.equalTo(self.containerView.mas_bottom);
        }];
        [self.btnArray addObject:closeButton];
        
         //分割线:按钮有背景图片时本身很清晰，不需要与上面有分割线了；
        if (self.btnHasBgImage == NO) {
            if (i == 0) {
                // 水平分割线
                UIView *hLineView = [[UIView alloc] init];
                hLineView.backgroundColor = ALERT_LINE_COLOR;
                [self.alertView addSubview:hLineView];
                [hLineView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.offset(-buttonHeight);
                    make.left.offset(13);
                    make.right.offset(-13);
                    make.height.offset(0.5);
                }];
                
            }else{
                //两个按钮时，中间的竖直分割线
                UIView *vLineView = [[UIView alloc] init];
                vLineView.backgroundColor = ALERT_LINE_COLOR;
                [self.alertView addSubview:vLineView];
                [vLineView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.offset(-6);
                    make.height.offset(buttonHeight-12);
                    make.centerX.equalTo(self.alertView);
                    make.width.offset(0.5);
                }];
            }
        }
    }
}
#pragma mark - 添加断言（如传入的标题不是NSString）
- (void)addAssertions
{
    for  (id btnTitle in _buttonTitles) {
        if (![btnTitle isKindOfClass:[NSString class]]) {
            NSAssert(NO, @"按钮的标题必须是NSString类型");
        }
    }
}
#pragma mark 展示在keyWindow上的alertView上的底部按钮点击事件
- (void)buttonTouchUpInside:(id)sender
{
    UIButton *btn = sender;
    if (self.onButtonTouchUpInside) {
        self.onButtonTouchUpInside(btn.tag);
    }
    [self close];
}

#pragma mark 点击阴影，关闭
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!_closeOnTouchUpOutside) return;
    UITouch *touch = [touches anyObject];
    if ([touch.view isKindOfClass:[QLAlert class]]) {
        [self close];
    }
}

#pragma mark 关于存管的按钮点击
- (void)aboutLineBankBtnClick:(UIButton *)sender {
    
    [self close];
    if (self.aboutLineBankBtnTouchUpInside) {
        self.aboutLineBankBtnTouchUpInside();
    }
}

#pragma mark 懒加载
- (UIView *)alertView
{
    if (!_alertView) {
        CGSize dialogSize = [self countDialogSize];
        _alertView = [[UIView alloc] initWithFrame:CGRectMake((ALERT_SCREEN_WIDTH - dialogSize.width) / 2, (ALERT_SCREEN_HEIGHT - dialogSize.height) / 2, dialogSize.width, dialogSize.height)];
        _alertView.backgroundColor = [UIColor whiteColor];
        _alertView.layer.cornerRadius = kAlertViewCornerRadius;
        _alertView.clipsToBounds = YES;
        _alertView.layer.shouldRasterize = YES;
        _alertView.layer.rasterizationScale = [[UIScreen mainScreen] scale];
        _alertView.layer.opacity = 0.5f;
        _alertView.layer.transform = CATransform3DMakeScale(1.3f, 1.3f, 1.0);
    }
    return _alertView;
}

- (CGSize)countDialogSize
{
    if (_buttonTitles && [_buttonTitles count] == 2) {
        buttonHeight       = kAlertDefaultButtonHeight;
        buttonSpacerHeight = buttonBottomPadding;
    }else if (_buttonTitles && [_buttonTitles count] == 1){
        buttonHeight       = kAlertDefaultButtonHeight;
        buttonSpacerHeight = 0;
    }else {
        buttonHeight = 0;
        buttonSpacerHeight = 0;
    }
    CGFloat dialogWidth = kAlertDefaultTotalWidth;
    CGFloat dialogHeight = self.containerView.frame.size.height + buttonHeight + buttonSpacerHeight;
    return CGSizeMake(dialogWidth, dialogHeight);
}
- (NSMutableArray *)btnArray
{
    if (!_btnArray) {
        _btnArray = [@[]mutableCopy];
    }
    return _btnArray;
}
- (UIView *)containerView
{
    if (!_containerView) {
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kAlertDefaultTotalWidth, 150)];
        _containerView.layer.cornerRadius = kAlertViewCornerRadius;
        _containerView.clipsToBounds = YES;
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}

- (UITextView *)contentTextView
{
    if (!_contentTextView) {
        _contentTextView = [[UITextView alloc]init];
        _contentTextView.showsVerticalScrollIndicator = NO;
        _contentTextView.textAlignment = NSTextAlignmentCenter;
        _contentTextView.textColor = ALERT_CONTENT_TEXT_COLOR;
        _contentTextView.font = ALERT_CONTENT_TEXT_FONT;
        _contentTextView.editable = NO;
        _contentTextView.selectable = NO;
    }
    return _contentTextView;
}
- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = ALERT_TITLE_TEXT_COLOR;
        _titleLabel.font = ALERT_TITLE_TEXT_FONT;
    }
    return _titleLabel;
}

-(UIImageView *)imgView
{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] init];
    }
    return _imgView;
}

- (UIButton*)closeBtn
{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn setImage:[UIImage imageNamed:@"webview_close"] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}

- (UIButton*)aboutLineBankBtn
{
    if (!_aboutLineBankBtn) {
        _aboutLineBankBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_aboutLineBankBtn setTitleColor:ALERT_AboutLineBank_COLOR forState:UIControlStateNormal];
        _aboutLineBankBtn.titleLabel.font = ALERT_AboutLineBank_TEXT_FONT;
        [_aboutLineBankBtn addTarget:self action:@selector(aboutLineBankBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _aboutLineBankBtn;
}

#pragma mark --setter
- (void)setContentTextAlignement:(NSTextAlignment)contentTextAlignement{
    _contentTextAlignement = contentTextAlignement;
    self.contentTextView.textAlignment = contentTextAlignement;
}

#pragma mark 显示在ViewController上的alertViewController
- (void)showWithMessage:(NSString * )message
        singleBtnTitle : (NSString *)btnTitle
{
    [self showWithMessage:message singleBtnTitle:btnTitle btnClicked:^(NSInteger index) {
        
    }];
}

- (void)showWithMessage:(NSString * )message
        singleBtnTitle : (NSString *)btnTitle
             btnClicked:(void (^) (NSInteger index))btnClicked
{
    [self showWithTitle:nil message:message btnTitleArray:@[btnTitle] btnClicked:btnClicked];
}
- (void)showWithMessage:(NSString * )message
         btnTitleArray : (NSArray <NSString *> *)btnTitleArray
             btnClicked:(void (^) (NSInteger index))btnClicked
{
    [self showWithTitle:nil message:message btnTitleArray:btnTitleArray btnClicked:btnClicked];
}
- (void)showWithTitle:(NSString*)title
              message:(NSString * )message
       btnTitleArray : (NSArray <NSString *> *)btnTitleArray
           btnClicked:(void (^) (NSInteger index))btnClicked

{
    QLAlertViewController *alert = [QLAlertViewController new];
    [alert prepareUIWithTitle:title contentText:message];
    if (self.alertViewButtonLayoutType == QLAlertViewLayoutTypeVertical) {
        alert.alertControllerButtonLayoutType = QLAlertControllerButtonLayoutTypeVertical ;
    }
    [alert setButtonTitles:btnTitleArray];
    alert.onButtonTouchUpInside = btnClicked;
    [[self appRootViewController] presentViewController:alert animated:YES completion:nil];
    
}
- (void)showWithTitle:(NSString*)title
              message:(NSString * )message
contentTextAlignement:(NSTextAlignment)contentTextAlignement
       btnTitleArray : (NSArray <NSString *> *)btnTitleArray
           btnClicked:(void (^) (NSInteger index))btnClicked

{
    QLAlertViewController *alert = [QLAlertViewController new];
    [alert prepareUIWithTitle:title contentText:message];
    alert.contentTextAlignement = NSTextAlignmentLeft;
    if (self.alertViewButtonLayoutType == QLAlertViewLayoutTypeVertical) {
        alert.alertControllerButtonLayoutType = QLAlertControllerButtonLayoutTypeVertical ;
    }
    [alert setButtonTitles:btnTitleArray];
    alert.onButtonTouchUpInside = btnClicked;
    [[self appRootViewController] presentViewController:alert animated:YES completion:nil];
}

- (void)showExternalCustomUIWithBtnTitleArray : (NSArray <NSString *> *)btnTitleArray
                                    btnClicked:(void (^) (NSInteger index))btnClicked
{
    QLAlertViewController *alert = [QLAlertViewController new];
    alert.containerView = self.containerView;
    
    [alert prepareExternalCustomUI];
    if (self.alertViewButtonLayoutType == QLAlertViewLayoutTypeVertical ) {
        alert.alertControllerButtonLayoutType = QLAlertControllerButtonLayoutTypeVertical;
    }
    [alert setButtonTitles:btnTitleArray];
    alert.onButtonTouchUpInside = btnClicked;
    [[self appRootViewController] presentViewController:alert animated:YES completion:nil];
    
}

- (void)showWithUIImageName:(NSString *)imageName
                    title  :(NSString *)title
                    content:(NSString *)content
             btnTitleArray : (NSArray <NSString *> *)btnTitleArray
                 btnClicked:(void (^) (NSInteger index))btnClicked
{
    QLAlertViewController *alert = [QLAlertViewController new];
    [alert prepareUIImageName:imageName title:title content:content];
    [alert setButtonTitles:btnTitleArray];
    alert.onButtonTouchUpInside = btnClicked;
    [[self appRootViewController] presentViewController:alert animated:YES completion:nil];
}
- (UIViewController *)appRootViewController
{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

- (void)showBottomBtnVerticalAlignmentWithMessage:(NSString * )message
                                   btnTitleArray : (NSArray <NSString *> *)btnTitleArray
                                       btnClicked:(void (^) (NSInteger index))btnClicked
{
    [self showBottomBtnVerticalAlignmentWithTitle:@"" message:message btnTitleArray:btnTitleArray btnClicked:btnClicked];
}
- (void)showBottomBtnVerticalAlignmentWithTitle:(NSString*)title
                                        message:(NSString * )message
                                 btnTitleArray : (NSArray <NSString *> *)btnTitleArray
                                     btnClicked:(void (^) (NSInteger index))btnClicked
{
    QLAlertViewController *alert = [QLAlertViewController new];
    [alert prepareUIWithTitle:title contentText:message];
    alert.alertControllerButtonLayoutType = QLAlertControllerButtonLayoutTypeVertical;
    [alert setButtonTitles:btnTitleArray];
    alert.onButtonTouchUpInside = btnClicked;
    [[self appRootViewController] presentViewController:alert animated:YES completion:nil];
}
- (void)showBottomBtnVerticalAlignmentWithMessage:(NSString * )message
                                  singleBtnTitle : (NSString *)btnTitle
                                       btnClicked:(void (^) (NSInteger index))btnClicked
{
    [self showBottomBtnVerticalAlignmentWithTitle:@"" message:message btnTitleArray:@[btnTitle] btnClicked:btnClicked];
}

- (void)showBottomBtnVerticalAlignmentWithMessage:(NSString * )message
                                  singleBtnTitle : (NSString *)btnTitle
{
    [self showBottomBtnVerticalAlignmentWithMessage:message btnTitleArray:@[btnTitle] btnClicked:^(NSInteger index) {
        
    }];
}



@end
