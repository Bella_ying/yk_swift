//
//  QLSingleButtonAlertView.h
//  QuickLoan
//
//  Created by Jeremy on 2018/4/12.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QLBaseAlertView.h"

@interface QLSingleButtonAlertView : QLBaseAlertView

+ (void(^)(NSString *message))showAlertAction:(void(^)(void))action;

@end
