//
//  QLBaseAlertView.m
//  QuickLoan
//
//  Created by Jeremy on 2018/4/11.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QLBaseAlertView.h"

@implementation QLBaseAlertView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configUI];
    }
    return self;
}

- (void)configUI
{
    [self.alertBackGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(75 * WIDTH_RATIO);
        make.right.offset(-75 * WIDTH_RATIO);
        make.centerY.offset(0);
    }];
}

- (instancetype(^)(id model))updateAlertView {
    return ^(id model){
        [self updateUI:model];
        return self;
    };
}

- (void)updateUI:(id)model
{
    
}

- (void(^)())showAlert {
    return ^{
        [self show];
    };
}

- (void)show
{
    if (!self.superview) {
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
        [[UIApplication sharedApplication].keyWindow layoutSubviews];
        [self layoutSubviews];
    }
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    self.alertBackGroundView.alpha = 0.0f;
    self.hidden = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
        self.alertBackGroundView.alpha = 1.0f;
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
        self.alertBackGroundView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

//懒加载
- (UIView *)alertBackGroundView
{
    if (!_alertBackGroundView) {
        _alertBackGroundView = [UIView new];
        _alertBackGroundView.backgroundColor = [UIColor whiteColor];
        _alertBackGroundView.layer.masksToBounds = YES;
        _alertBackGroundView.layer.cornerRadius = 5.0f;
        [self addSubview:_alertBackGroundView];
    }
    return _alertBackGroundView;
}

@end
