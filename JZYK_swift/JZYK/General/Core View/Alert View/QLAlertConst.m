//
//  QLAlertConst.m
//  QuickLoan
//
//  Created by Jeremy on 2018/4/10.
//  Copyright © 2018 JSQB. All rights reserved.
//

NSString * const kAleartClose = @"关闭";

const CGFloat kAlertDefaultButtonHeight           = 54.f;
const CGFloat kAlertDefaultButtonSpacerHeight     = 15.f;   //底部功能按钮中间的间隔
const CGFloat kAlertDefaultContentLeftRightPadding  = 20.f;   //内容左右间距,底部功能按钮左右间距
const CGFloat kAlertDefaultButtonBottomPadding     = 24.f;   //底部功能按钮有背景图时的下间距
const CGFloat kAlertViewCornerRadius              = 5.f;
const CGFloat kAlertDefaultPadding                = 18.0;      //内容yu标题间的边距
const CGFloat kAlertDefaultTitleHeight         = 50; //标题的高度
const CGFloat kAlertDefaultTitleTop         = 25; //标题的上间距
const CGFloat kAlertBankButtonHeight    = 32.f;   //关于存管按钮的高度
const CGFloat kAlertBankButtonTopPadding     = 5.f;   //关于存管按钮上间距
const CGFloat kAlertBankButtonBottomPadding     = 10.f;   //关于存管按钮下间距
const CGFloat kAlertDefaultTotalWidth             = 300.0f;   //弹框宽度
const CGFloat kAlertDefaultContainerWidth         = kAlertDefaultTotalWidth - 2*kAlertDefaultContentLeftRightPadding;
