//
//  QLDescriptionAlert.h
//  QuickLoan
//
//  Created by Jeremy on 2018/4/11.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QLBaseAlertView.h"

@interface QLDescriptionAlert : QLBaseAlertView

+ (void)showMessage:(NSString *)message;

+ (void)showLargeMessage:(NSString *)message;

@end
