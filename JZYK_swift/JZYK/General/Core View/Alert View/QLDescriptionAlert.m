//
//  QLDescriptionAlert.m
//  QuickLoan
//
//  Created by Jeremy on 2018/4/11.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QLDescriptionAlert.h"
#define ALERT_BUTTON_TEXT_FONT     [UIFont systemFontOfSize:adaptFontSize(16)]

@interface QLDescriptionAlert()
@property (nonatomic ,strong) UILabel *titleLabel;
@property (nonatomic ,strong) UITextView *textView;

@end

@implementation QLDescriptionAlert

+ (void)showLargeMessage:(NSString *)message
{
    if (![message yk_isValidString]) {
        [[iToast makeText:@"抱歉，暂时没有数据展示"] show];
        return;
    }
    QLDescriptionAlert *descAlert = [QLDescriptionAlert new];
    [descAlert.textView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.offset(160);
    }];
    [descAlert refreshHtmlText:message];
    descAlert.showAlert();
}

+ (void)showMessage:(NSString *)message
{
    if (![message yk_isValidString]) {
        [[iToast makeText:@"抱歉，暂时没有数据展示"] show];
        return;
    }
    QLDescriptionAlert *descAlert = [QLDescriptionAlert new];
    descAlert.updateAlertView(message);
    descAlert.showAlert();
}

- (void)configUI
{
    [self.alertBackGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(75 * WIDTH_RATIO);
        make.right.offset(-75 * WIDTH_RATIO);
        make.centerY.offset(0);
    }];
    
   UIButton *closeBtn = [UIButton yk_buttonWithImageName:@"webview_close"
                                               superView:self.alertBackGroundView
                                              masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        make.width.height.offset(30);
        make.top.offset(12);
        make.right.offset(-15);
        button.contentMode = UIViewContentModeScaleAspectFit;
        [button addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }];
    [closeBtn.titleLabel setFont:ALERT_BUTTON_TEXT_FONT];
    
    UILabel *titleLabel = [UILabel yk_labelWithFontSize:adaptFontSize(18)
                                              textColor:[QBColor yk_color_45_c2]
                                              superView:self.alertBackGroundView
                                             masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        label.font = [UIFont boldSystemFontOfSize:17];
        make.centerX.offset(0);
        make.top.equalTo(closeBtn.mas_bottom);
    }];
    self.titleLabel = titleLabel;
    
    UITextView *textView = [UITextView new];
    textView.editable = NO;
    textView.selectable = NO;
    textView.font = [UIFont systemFontOfSize:adaptFontSize(14)];
    textView.textColor = [QBColor yk_colorWithHex:@"#666666"];
    [self.alertBackGroundView addSubview:textView];
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.equalTo(closeBtn.mas_bottom).offset(-7);
        make.height.offset(60);
        make.left.offset(29*ASPECT_RATIO_WIDTH);
        make.right.offset(-29*ASPECT_RATIO_WIDTH);
        make.bottom.offset(-30);
    }];
    self.textView = textView;
}

- (void)updateUI:(id)model
{
    NSString *tmpStr = (NSString *)model;
    self.textView.text = tmpStr;
}

- (void)refreshHtmlText:(NSString *)text
{
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.textView.attributedText = attributedString;
}


@end
