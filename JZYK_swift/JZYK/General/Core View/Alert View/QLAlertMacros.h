//
//  QLAlertMacros.h
//  QuickLoan
//
//  Created by Jeremy on 2018/4/10.
//  Copyright © 2018 JSQB. All rights reserved.
//

#ifndef QLAlertMacros_h
#define QLAlertMacros_h

#ifdef __OBJC__
// 弱引用
#define ALERT_WEAK_SELF __weak typeof(self) weakSelf = self;
// 强引用
#define ALERT_STRONG_SELF __strong typeof(weakSelf) strongSelf = weakSelf;

// 日志输出
#ifdef DEBUG
#define AlertLog(...) NSLog(__VA_ARGS__)
#else
#define AlertLog(...)
#endif

// 屏幕尺寸
#define ALERT_SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define ALERT_SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

// 颜色
#define ALERT_CANCEL_COLOR         [QBColor yk_color_8D_c4] 
#define ALERT_CONFIRM_COLOR        [QBColor yk_color_66_c3]
#define ALERT_TITLE_TEXT_COLOR     [QBColor yk_color_45_c2]
#define ALERT_CONTENT_TEXT_COLOR   [QBColor yk_color_66_c3]
#define ALERT_LINE_COLOR           [QBColor yk_color_DC_C9]
#define ALERT_AboutLineBank_COLOR  [QBColor yk_mainThemecolor_c1]


// 字体大小
#define ALERT_CONTENT_TEXT_FONT    [UIFont systemFontOfSize:adaptFontSize(14)]
#define ALERT_TITLE_TEXT_FONT    [UIFont systemFontOfSize:adaptFontSize(17)]
#define ALERT_BUTTON_TEXT_FONT     [UIFont systemFontOfSize:adaptFontSize(16)]
#define ALERT_AboutLineBank_TEXT_FONT     [UIFont systemFontOfSize:adaptFontSize(13)]

#endif

#endif /* QLAlertMacros_h */
