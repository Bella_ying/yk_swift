//
//  QLAlertAnimation.m
//  QuickLoan
//
//  Created by Jeremy on 2018/4/10.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QLAlertAnimation.h"
#import "QLAlertViewController.h"

@implementation QLAlertAnimation

#pragma mark UIViewControllerAnimatedTransitioning delegate method
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    if (self.animationType == QLAlertAnimationTypePresent) {
        return 0.4;
    }
    return 0.15;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    [self animateDo](self.animationType,transitionContext);
}

- (void(^)(QLAlertAnimationType animationType, id<UIViewControllerContextTransitioning> transitionContext))animateDo
{
    return ^(QLAlertAnimationType animationType, id<UIViewControllerContextTransitioning> transitionContext){
        switch (animationType) {
            case QLAlertAnimationTypePresent:
            {
                [self bounceAnimationWithContext:transitionContext];
            }
                break;
            case QLAlertAnimationTypeDismiss:
            {
                [self fadeOutAnimationWithContext:transitionContext];
            }
                break;
            default:
                break;
        }
    };
}

#pragma mark private method
- (void)bounceAnimationWithContext:(id<UIViewControllerContextTransitioning>)transitionContext
{
    QLAlertViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    toVC.alertView.alpha = 0;
    toVC.alertView.transform = CGAffineTransformMakeScale(0, 0);
    
    UIView *containerView = [transitionContext containerView];
    [containerView addSubview:toVC.view];
    
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    [UIView animateWithDuration:duration
                          delay:0
         usingSpringWithDamping:0.7
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         toVC.alertView.alpha = 1;
                         toVC.alertView.transform = CGAffineTransformIdentity;
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:YES];
                     }];
}

- (void)fadeOutAnimationWithContext:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    [UIView animateWithDuration:duration
                     animations:^{
                         fromVC.view.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:YES];
                     }];
}


@end
