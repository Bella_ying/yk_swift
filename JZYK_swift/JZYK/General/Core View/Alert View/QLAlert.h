//
//  QLAlert.h
//  QuickLoan
//
//  Created by Jeremy Wang on 2018/4/10.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, QLAlertViewType) {
    QLAlertViewTypeDefault = 0,                // 默认，非定制
    QLAlertViewTypeCustomized = 1              // 外部定制
};

typedef NS_ENUM(NSUInteger, QLAlertViewLayoutType) {
    QLAlertViewLayoutTypeHorizontal = 0,               // 默认，按钮横向排列
    QLAlertViewLayoutTypeVertical = 1                  // 按钮纵向排列
};

@interface QLAlert : UIView

// 容器
@property (nonatomic, strong) UIView *containerView;
//按钮数组
@property (nonatomic, strong) NSArray *buttonTitles;
// 点击阴影，是否关闭
@property (nonatomic, assign) BOOL closeOnTouchUpOutside;
//弹框类型
@property (nonatomic, assign) QLAlertViewType alertViewType;
//弹框点击事件的block
@property (nonatomic, copy) void (^onButtonTouchUpInside)(NSInteger buttonIndex);
@property (nonatomic, copy) void (^closeButtonTouchUpInside)(void);//右上角关闭按钮
@property (nonatomic, copy) void (^aboutLineBankBtnTouchUpInside)(void);//关于存管的按钮
//标题颜色
@property (nonatomic, strong) UIColor *titleTextColor;
//标题字号
@property (nonatomic, strong) UIFont *titleTextFont;
//内容颜色
@property (nonatomic, strong) UIColor *contentTextColor;
//内容字号
@property (nonatomic, strong) UIFont *contentTextFont;
//title富文本修饰的内容
@property(nonatomic, strong)   NSAttributedString *titleAttributedText;
//content富文本修饰的内容
@property(nonatomic, strong)   NSAttributedString *contentAttributedText;
//按钮字体颜色
@property (nonatomic, strong) UIColor *btnTitleColor;
//按钮字体字号
@property (nonatomic, strong) UIFont *btnTitleFont;
//按钮背景颜色
@property (nonatomic, strong) UIColor *btnBgColor;
//按钮有圆角渐变的背景图片
@property (nonatomic, assign) BOOL btnHasBgImage;
//内容位置-默认居中
@property (nonatomic, assign) NSTextAlignment contentTextAlignement;

+ (instancetype)alert;

//定制alert弹框
- (void)externalCustomUIWithButtonTitleArray : (NSArray <NSString *> *)buttonTitleArray
                                buttonClicked:(void (^) (NSInteger index))buttonClicked;
/**
 @param content 弹框内容
 @param buttonTitleArray 按钮标题数组
 @param buttonClicked 按钮点击事件
 
 */

- (void)prepareUIWithContentText:(NSString *)content
               buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
                   buttonClicked:(void (^) (NSInteger index))buttonClicked;

/**
 @param title 弹框标题
 @param content 弹框内容
 */
- (void)prepareUIWithTitle:(NSString *)title
               contentText:(NSString *)content
         buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
             buttonClicked:(void (^) (NSInteger index))buttonClicked;
/**
 @param title 弹框标题
 @param content 弹框内容
 @param titleIsHtml 是否是html文本
 @param contentIsHtml 是否是html文本
 */
- (void)prepareUIWithTitle:(NSObject *)title titleIsHtml:(BOOL)titleIsHtml
               contentText:(NSObject *)content contentIsHtml:(BOOL)contentIsHtml
         buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
             buttonClicked:(void (^) (NSInteger index))buttonClicked;
/**
 @param imageName 图片名字
 @param title 标题
 @param content 内容
 */
- (void)prepareUIImageName:(NSString *)imageName
                   title  :(NSString *)title
                   content:(NSString *)content
         buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
             buttonClicked:(void (^) (NSInteger index))buttonClicked;

///针对借款详情的弹框；content下后有“关于存管”的按钮；右上角有关闭X号按钮；
/**
 @param title 弹框标题
 @param content 弹框内容
 @param hasCloseBtn 右上角是否有x号关闭按钮
 @param lineBankBtnTitle content下是否有“关于存管”按钮，按钮的标题；’标题‘.length > 0为有标题；
 @param hasBgImage 地下btn的样式，是否带主题按钮背景图；
 */
- (void)prepareUIWithTitle:(NSString *)title
               contentText:(NSString *)content
          topRightCloseBtn:(BOOL)hasCloseBtn
     aboutLineBankBtnTitle:(NSString *)lineBankBtnTitle
         buttonWithBgImage:(BOOL)hasBgImage
         buttonTitleArray : (NSArray <NSString *> *)buttonTitleArray
             buttonClicked:(void (^) (NSInteger index))buttonClicked
   aboutLineBankBtnClicked:(void (^) (void))aboutLineBankBtnClicked
        closeButtonClicked:(void (^) (void))closeBtnClicked;

- (void)show;
- (void)close;
- (void)dismissAlert;
- (void)setOnButtonTouchUpInside:(void (^)( NSInteger buttonIndex))onButtonTouchUpInside;


/***************************** controller *****************************/

#pragma mark 显示在ViewController上的
//弹框底部按钮的排列类型
@property (nonatomic, assign) QLAlertViewLayoutType alertViewButtonLayoutType;

/**
 展示没有标题，只有内容的一个按钮的弹框
 
 @param message 内容
 @param btnTitle 按钮标题
 */
- (void)showWithMessage:(NSString * )message
        singleBtnTitle : (NSString *)btnTitle;

/**
 在controller上展示没有标题，只有内容的一个按钮的弹框
 
 @param message 内容
 @param btnTitle 按钮标题
 @param btnClicked 按钮的点击事件
 */
- (void)showWithMessage:(NSString * )message
        singleBtnTitle : (NSString *)btnTitle
             btnClicked:(void (^) (NSInteger index))btnClicked;

/**
 在controller上展示没有标题，只有内容的弹框
 
 @param message 内容
 @param btnTitleArray 按钮标题数组  如@[@"确定",@"取消"]，则"确定"按钮在右侧，"取消"按钮在左侧
 @param btnClicked 按钮点击事件
 */
- (void)showWithMessage:(NSString * )message
         btnTitleArray : (NSArray <NSString *> *)btnTitleArray
             btnClicked:(void (^) (NSInteger index))btnClicked;

/**
 @param title 标题 ，如未为空，则不显示
 @param message 内容
 @param btnTitleArray 按钮标题数组  如@[@"确定",@"取消"]，则"确定"按钮在右侧，"取消"按钮在左侧
 @param btnClicked 按钮点击事件
 */

- (void)showWithTitle:(NSString*)title
              message:(NSString * )message
       btnTitleArray : (NSArray <NSString *> *)btnTitleArray
           btnClicked:(void (^) (NSInteger index))btnClicked;

//同上，但加一个内容对齐方式
- (void)showWithTitle:(NSString*)title
              message:(NSString * )message
contentTextAlignement:(NSTextAlignment)contentTextAlignement
       btnTitleArray : (NSArray <NSString *> *)btnTitleArray
           btnClicked:(void (^) (NSInteger index))btnClicked;

/**
 在controller上显示定制的UI
 
 @param btnTitleArray 按钮标题数组
 @param btnClicked 按钮点击事件
 */
- (void)showExternalCustomUIWithBtnTitleArray : (NSArray <NSString *> *)btnTitleArray
                                    btnClicked:(void (^) (NSInteger index))btnClicked;


/**
 @param imageName 图片名字
 @param title 标题
 @param content 内容
 @param btnTitleArray 按钮标题数组
 @param btnClicked 按钮点击事件
 */
- (void)showWithUIImageName:(NSString *)imageName
                    title  :(NSString *)title
                    content:(NSString *)content
             btnTitleArray : (NSArray <NSString *> *)btnTitleArray
                 btnClicked:(void (^) (NSInteger index))btnClicked;


/**
 底部按钮纵向排列
 @param title 标题
 @param message 内容
 @param btnTitleArray 按钮标题数组
 @param btnClicked 按钮点击事件
 */
- (void)showBottomBtnVerticalAlignmentWithTitle:(NSString*)title
                                        message:(NSString * )message
                                 btnTitleArray : (NSArray <NSString *> *)btnTitleArray
                                     btnClicked:(void (^) (NSInteger index))btnClicked;

/**
 底部按钮纵向排列
 
 @param message 内容
 @param btnTitleArray 按钮标题数组
 @param btnClicked 按钮点击事件
 */
- (void)showBottomBtnVerticalAlignmentWithMessage:(NSString * )message
                                   btnTitleArray : (NSArray <NSString *> *)btnTitleArray
                                       btnClicked:(void (^) (NSInteger index))btnClicked;


/**
 在controller上展示没有标题，只有内容的弹框
 底部按钮纵向排列，按钮有背景色
 
 @param message 内容
 @param btnTitle 按钮标题
 @param btnClicked 按钮点击事件
 */
- (void)showBottomBtnVerticalAlignmentWithMessage:(NSString * )message
                                  singleBtnTitle : (NSString *)btnTitle
                                       btnClicked:(void (^) (NSInteger index))btnClicked;

/**
 只有一个按钮，按钮有背景色
 
 @param message 内容
 @param btnTitle 标题
 */
- (void)showBottomBtnVerticalAlignmentWithMessage:(NSString * )message
                                  singleBtnTitle : (NSString *)btnTitle;

@end
