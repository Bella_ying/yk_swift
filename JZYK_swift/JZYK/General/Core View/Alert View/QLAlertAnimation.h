//
//  QLAlertAnimation.h
//  QuickLoan
//
//  Created by Jeremy on 2018/4/10.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, QLAlertAnimationType) {
    QLAlertAnimationTypePresent,
    QLAlertAnimationTypeDismiss,
};

@interface QLAlertAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) QLAlertAnimationType animationType;

@end
