//
//  HYProgressHUD.H
//  JZYK
//
//  Created by hongyu on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HYProgressHUD : NSObject

+ (instancetype _Nullable )yk_shareProgressHUD;

- (void)yk_showImageHudWithMessage:(NSString * _Nullable)message;

- (void)yk_showSpecificationsHudWithMessage:(NSString * _Nullable)message;

- (void)yk_hideAnimated;

- (void)yk_hideAnimatedWithAfterTime:(NSInteger)time;

@end
