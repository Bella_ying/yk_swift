//
//  HYProgressHUD.m
//  JZYK
//
//  Created by hongyu on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "HYProgressHUD.h"
#import "MBProgressHUD.h"

@interface HYProgressHUD ()

@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation HYProgressHUD

+ (instancetype)yk_shareProgressHUD
{
    static dispatch_once_t onceToken;
    static HYProgressHUD *hud;
    dispatch_once(&onceToken, ^{
        hud = [HYProgressHUD new];
    });
    return hud;
}

- (void)yk_showImageHudWithMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].windows lastObject] animated:YES];
        self.hud.mode = MBProgressHUDModeCustomView;
        self.hud.label.text     = message == nil ? @"" : message;
        self.hud.label.textColor = LABEL_TEXT_COLOR;
        self.hud.label.numberOfLines = 0;
        self.hud.label.font     = [UIFont systemFontOfSize:adaptFontSize(13)];
        self.hud.bezelView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
        self.hud.backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
        
        UIImage *image = [[UIImage imageNamed:@"refresh_21"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        UIImageView* mainImageView = [[UIImageView alloc] initWithImage:image];
        mainImageView.animationImages = [self addImageToArrayWithStartPage:21 endPage:43];
        [mainImageView setAnimationDuration:0.5f];
        [mainImageView setAnimationRepeatCount:0];
        [mainImageView startAnimating];
        self.hud.customView = mainImageView;
        self.hud.animationType = MBProgressHUDAnimationFade;
    });
}

- (void)yk_hideAnimated
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.hud hideAnimated:YES];
    });
}

- (NSMutableArray <UIImage *> *)addImageToArrayWithStartPage:(NSInteger)startPage endPage:(NSInteger)endPage
{
    NSMutableArray *imageArray = [NSMutableArray array];
    for (NSInteger page = startPage; page <= endPage; page++) {
        NSString *imgName = [NSString stringWithFormat:@"refresh_%zd", page];
        UIImage *image = [UIImage imageNamed:imgName];
        [imageArray addObject:image];
    }
    return imageArray;
}

@end
