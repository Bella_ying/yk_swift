//
//  JSQBAnouncementView.h
//  KDFDApp
//
//  Created by wangjeremy on 04/05/2017.
//  Copyright © 2017 cailiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKLoanEntity.h"

typedef NS_ENUM(NSInteger, PageType){
    PageTypeLoanCash  = 0 ,   //取现页
    PageTypeMine      = 1 ,   //我的页面
    PageTypeRepayBill = 2 ,   //待还账单
};

typedef void (^ClickCloseBlock)(UIView *view);

@interface YKPaoMaNoticeView : UIView

@property (nonatomic,copy) ClickCloseBlock clickCloseBLk;  //点击关闭时的事件
@property(nonatomic,copy) void(^anouncementInfoBlk)(NSString * pageInfoURL);//没有实现。。

- (instancetype)initWithFrame:(CGRect)frame pageType:(PageType)pageType;

- (void)refreshNoticeWithData:(YKNoticeModel *)data complete:(void (^)(BOOL isHidden))complete;
/**
 *独立于ID判断的另一种方式，直接赋值（使用于有的接口不用ID判断，例如添加银行卡页面）
 *不利用ID进行判断显示或隐藏，直接用参数
 */
- (void)refreshNoticeWithContent:(NSString *)content isShow:(BOOL)isShow complete:(void (^)(BOOL isHidden))complete;

@end
