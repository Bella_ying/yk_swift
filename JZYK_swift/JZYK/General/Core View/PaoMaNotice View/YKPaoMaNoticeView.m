//
//  JSQBAnouncementView.m
//  KDFDApp
//
//  Created by wangjeremy on 04/05/2017.
//  Copyright © 2017 cailiang. All rights reserved.
//

#import "YKPaoMaNoticeView.h"
#import "PaoMaView.h"
#import "MarqueeLabel.h"
@interface YKPaoMaNoticeView()

//@property (nonatomic, strong) PaoMaView *scrollDirectionView;
@property (nonatomic, assign) PageType pageType;
@property (nonatomic, copy) NSString *pageUrl;//跳转的URL
@property (nonatomic, strong) YKNoticeModel *noticeModel;
@property (nonatomic, strong) MarqueeLabel *textLabel;

@end

@implementation YKPaoMaNoticeView

- (instancetype)initWithFrame:(CGRect)frame pageType:(PageType)pageType {
    if (self = [super initWithFrame:frame]) {
        
        self.pageType = pageType;
        [self setupViewWithHeight:frame.size.height];
    }
    return self;
}

- (void)setupViewWithHeight:(NSInteger)height {
    
    //公告的小喇叭
    UIImageView * loudspeakerImg = [UIImageView yk_imageViewWithImageName:@"orange" superView:self masonrySet:^(UIImageView *imageView, MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.left.offset(15);
    }];
    
    UIButton *closeButton = [UIButton yk_buttonWithImageName:@"webview_close" superView:self masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        make.top.bottom.offset(0);
        make.right.offset(-5);
        make.width.offset(33);
        
        [button addTarget:self action:@selector(closeAnoucementAction:) forControlEvents:UIControlEventTouchUpInside];
    }];
    
//    self.scrollDirectionView = [[PaoMaView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN - 60, height) text:@"" font:[UIFont systemFontOfSize:adaptFontSize(13)] textColor:Color.color_8D_C4];
//    __weak typeof(self) weakSelf = self;
//    [self.scrollDirectionView setClickItemActionBlock:^{
//        [weakSelf anouncementInfoPage];
//    }];
//    self.backgroundColor = self.scrollDirectionView.backgroundColor = Color.whiteColor;
//    [self addSubview:self.scrollDirectionView];
//    [self.scrollDirectionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(loudspeakerImg.mas_right).offset(8);
//        make.right.equalTo(closeButton.mas_left).offset(-2);
//        make.centerY.offset(0);
//        make.height.offset(height);
//    }];
    
    [self addSubview:self.textLabel];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(loudspeakerImg.mas_right).offset(8);
//        make.right.equalTo(closeButton.mas_left).offset(-2);
        make.right.mas_lessThanOrEqualTo(closeButton.mas_left).offset(-5);
        make.centerY.offset(0);
        make.height.offset(height);
    }];
}

- (void)closeAnoucementAction:(UIButton *)button{
    DLog(@"点击公告消失");
    if (self.pageType == PageTypeLoanCash) {
        [YK_NSUSER_DEFAULT setInteger:[self.noticeModel.noticeID integerValue] forKey:@"PageTypeLoanCash"];
    } else if (self.pageType == PageTypeMine) {
        [YK_NSUSER_DEFAULT setInteger:[self.noticeModel.noticeID integerValue] forKey:@"PageTypeMine"];
    } else if (self.pageType == PageTypeRepayBill){
        [YK_NSUSER_DEFAULT setInteger:[self.noticeModel.noticeID integerValue] forKey:@"PageTypeRepayBill"];
    }
    
    self.hidden = YES;
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.offset(0);
    }];
    if (self.clickCloseBLk) {
        self.clickCloseBLk(self);
    }
}

- (void)anouncementInfoPage{
    if (self.anouncementInfoBlk) {
        self.anouncementInfoBlk(self.pageUrl);
    }
}

- (void)refreshNoticeWithData:(YKNoticeModel *)data complete:(void (^)(BOOL isHidden))complete {
    
    self.noticeModel = data;
//    [self.scrollDirectionView yk_reloadPaoMaViewWithText:data.content];
    self.textLabel.text = data.content;
    
    NSInteger savedNoticeID = 0;
    if (self.pageType == PageTypeLoanCash) {
        savedNoticeID = [YK_NSUSER_DEFAULT integerForKey:@"PageTypeLoanCash"];
       
    } else if (self.pageType == PageTypeMine) {
        savedNoticeID = [YK_NSUSER_DEFAULT integerForKey:@"PageTypeMine"];
    } else if (self.pageType == PageTypeRepayBill){
        savedNoticeID = [YK_NSUSER_DEFAULT integerForKey:@"PageTypeRepayBill"];
    }
    if (savedNoticeID == [data.noticeID integerValue]) {
        self.hidden = YES;
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.offset(0);
        }];
       
    } else {
        self.hidden = NO;
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.offset(30);
        }];
    }
    if (complete) {
        complete(self.hidden);
    }
    
}
//不利用ID进行判断显示或隐藏，直接用参数
- (void)refreshNoticeWithContent:(NSString *)content isShow:(BOOL)isShow complete:(void (^)(BOOL isHidden))complete{
//    [self.scrollDirectionView yk_reloadPaoMaViewWithText:content];
    self.textLabel.text = content;

    if (!isShow) {
        self.hidden = YES;
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.offset(0);
        }];
        
    } else {
        self.hidden = NO;
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.offset(30);
        }];
    }
    if (complete) {
        complete(self.hidden);
    }
}

- (MarqueeLabel *)textLabel
{
    if (!_textLabel) {
        _textLabel = [[MarqueeLabel alloc] init];
        _textLabel.font = [UIFont systemFontOfSize:13];
        _textLabel.trailingBuffer = 30;
        _textLabel.textColor = Color.color_8D_C4;
        _textLabel.rate = 50;
    }
    
    return _textLabel;
}

@end
