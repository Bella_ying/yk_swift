//
//  PaoMaView.m
//  KDLC
//
//  Created by apple on 15-2-2.
//  Copyright (c) 2015年 llyt. All rights reserved.
//

#import "PaoMaView.h"

@interface PaoMaView ()

@property (nonatomic, retain) UILabel *firstLabel;
@property (nonatomic, retain) UILabel *secLabel;


@property (nonatomic, retain) UIFont *textFont;
@property (nonatomic, retain) UIColor *textColor;

@property (nonatomic, assign) CGFloat pageWidth;
@property (nonatomic, retain) NSString *labelText;

@end

@implementation PaoMaView

- (instancetype)initWithFrame:(CGRect)frame text:(NSString *)text font:(UIFont *)font textColor:(UIColor *)color
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        
        _labelText = text;
        _textColor = color;
        _textFont = font;
        
        [self relayoutUI];

    }
    return self;
}

//重新更新布局
- (void)relayoutUI
{
    BOOL isAnimated = _pageWidth > self.frame.size.width;
    _pageWidth = [_labelText yk_widthWithFontSize:_textFont.pointSize];
    if (_pageWidth > self.frame.size.width) {
        
        _pageWidth += self.frame.size.width / 2;
        self.firstLabel.frame = CGRectMake(0, 0, _pageWidth, self.frame.size.height);
        self.secLabel.frame = CGRectMake(_pageWidth - 1, 0, _pageWidth + 1, self.frame.size.height);
        self.firstLabel.text = _secLabel.text = _labelText;
    } else {
        _pageWidth = self.frame.size.width;
        self.firstLabel.frame = self.bounds;
        self.firstLabel.text = _labelText;
        self.secLabel.text = @"";
    }
    if ((isAnimated && _pageWidth <= self.frame.size.width) || (!isAnimated && _pageWidth > self.frame.size.width && self.window) ) {
        [self scrollViewMove];
    }
}

- (void)didMoveToWindow
{
    [super didMoveToWindow];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self scrollViewMove];
    });
}

- (void)scrollViewMove
{
    if (_pageWidth <= self.frame.size.width) {
        return;
    }
    
    [self setContentOffset:CGPointZero animated:NO];
    CGFloat animationTime =  _pageWidth / self.frame.size.width * 7;
    __weak typeof(self) weakSelf = self;
    //UIView动画会在递归调用的时候存在循环引用问题。
    /*
     若是使用self，PaoMaView这个View一直存在于内存中没有被释放，因为UIView Animation对其的强引用使其一直存在于内存中，直至Block里面的代码执行完毕。但是这段代码里可以看到，completion中又继续调用了[self scrollViewMove];因此，系统会继续保留对PaoMaView的引用，从而即使PaoMaView的SuperView被销毁了，PaoMaView还是没有被销毁，无限地循环调用着动画，占据内存的同时也耗费着CPU。
     */
    [UIView animateWithDuration:animationTime delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.contentOffset = CGPointMake(self->_pageWidth, 0);
    } completion:^(BOOL finished) {
        if (self.window) {
            [weakSelf scrollViewMove];
        }
    }];
}

- (void)clickAction{
    if (self.clickItemActionBlock) {
        self.clickItemActionBlock();
    }
}

- (void)yk_reloadPaoMaViewWithText:(NSString *)text {
    _labelText = text;
    [self relayoutUI];
}

- (UILabel *)firstLabel{
    if (!_firstLabel) {
        _firstLabel = [[UILabel alloc] init];
        _firstLabel.textColor = _textColor;
        _firstLabel.font = _textFont;
        [self addSubview:_firstLabel];
    }
    return _firstLabel;
}

- (UILabel *)secLabel{
    if (!_secLabel) {
        _secLabel = [[UILabel alloc] init];
        _secLabel.textColor = _textColor;
        _secLabel.font = _textFont;
        [self addSubview:_secLabel];
    }
    return _secLabel;
}

@end
