//
//  PaoMaView.h
//  KDLC
//
//  Created by apple on 15-2-2.
//  Copyright (c) 2015年 llyt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaoMaView : UIScrollView

@property(nonatomic,copy) void(^clickItemActionBlock)(void);

- (instancetype)initWithFrame:(CGRect)frame text:(NSString *)text font:(UIFont *)font textColor:(UIColor *)color;

- (void)yk_reloadPaoMaViewWithText:(NSString *)text;


@end
