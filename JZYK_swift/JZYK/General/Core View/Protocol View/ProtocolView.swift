//
//  ProtocolView.swift
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/17.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import SnapKit

typealias CheckBoxActionCallBack = ((Bool)->Void)
typealias ProtocolTouchAction = ((Int)->Void)

let linkValue = "link"

@objcMembers class ProtocolView: UIView {
    private lazy var protocolTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.clear
        textView.isEditable = false;
        textView.isScrollEnabled = false;
        textView.delegate = self;
        textView.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue : Color.main]
        return textView;
    }()
    lazy var textAttachment: NSTextAttachment = {
        let ta = NSTextAttachment()
        ta.bounds = CGRect(x: 0, y: -3, width: 15, height: 15);
        return ta;
    }()
    
    var checkBoxImage: UIImage?
    var attributedString: NSMutableAttributedString?
    var selectedIconString: String?
    var unSelectIconString: String?
    var isSelect: Bool?
    var isCheckBoxSelect: Bool?
    var checkBoxActionCallBack: CheckBoxActionCallBack?
    var protocolTouchAction: ProtocolTouchAction?
    
    private func setupUI() {
        self.addSubview(protocolTextView)
        protocolTextView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview();
        }
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc public convenience init(frame: CGRect,
                                  protocolStartSting startStr: String,
                                  protocolStrings strs: [String],
                                  selectedIconName selName: String,
                                  unselectedIconName unselName: String,
                                  isCheckBoxSelect: Bool,
                                  checkBoxSelection: @escaping(CheckBoxActionCallBack),
                                  protocolTouchAction: @escaping(ProtocolTouchAction)) {
        self.init(frame: frame)
        self.checkBoxActionCallBack = checkBoxSelection;
        self.protocolTouchAction = protocolTouchAction;
        self.isSelect = true;
        self.isCheckBoxSelect = isCheckBoxSelect;
        self.selectedIconString = selName;
        self.unSelectIconString = unselName;
        let protocolText = strs.joined(separator: "")
        self.attributedString = NSMutableAttributedString.init(string: " \(startStr)\(protocolText)")
        self.attributedString?.addAttribute(NSAttributedStringKey.foregroundColor, value: Color.color_8D_C4, range: NSMakeRange(0, startStr.count+1))

        for (idx, value) in strs.enumerated() {
            
            self.attributedString?.addAttribute(NSAttributedStringKey.link,
                                                value: "\(idx)",
                range: (self.attributedString?.string.range(of: value)?.nsRange)!)
        }
        if !selName.isEmpty {
            self.textAttachment.image = UIImage(named: selName);
            let attachString = NSAttributedString(attachment: self.textAttachment);
            self.attributedString?.insert(attachString, at: 0);
            self.attributedString?.addAttribute(NSAttributedStringKey.link, value: linkValue, range: (self.attributedString?.string.range(of: attachString.string)?.nsRange)!)
        }
        guard let content = self.attributedString else {
            self.isHidden = true;
            return
        }
        //通过富文本来设置行间距
        let paraph = NSMutableParagraphStyle()
        paraph.lineSpacing = 10
        self.attributedString?.addAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.paragraphStyle:paraph], range: NSMakeRange(0, content.length))
        self.protocolTextView.attributedText = content;
    }
    
}

extension ProtocolView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return interactUrl(URL);
    }
    
    private func interactUrl(_ url: URL) -> Bool {
        if url == URL(string: "0") {
            self.protocolTouchAction?(0)
            return false
        } else if (url == URL(string: "1")) {
            self.protocolTouchAction?(1)
            return false
        }
        
        guard let _ = self.isCheckBoxSelect else { return true }
        if url == URL(string: linkValue) {
            self.isSelect = !self.isSelect!;
            updateUIAfterCheckBoxSelect();
            self.checkBoxActionCallBack?(self.isSelect!)
            return false
        }
        return true;
    }
    //更新点击之后的复选框
    private func updateUIAfterCheckBoxSelect() {
        self.textAttachment.image = UIImage(named: self.isSelect! ? self.selectedIconString! : self.unSelectIconString!)
        let attachString = NSAttributedString(attachment: self.textAttachment);
        self.attributedString?.replaceCharacters(in: NSMakeRange(0, 1), with: attachString);
        self.protocolTextView.attributedText = self.attributedString;
        self.attributedString?.addAttribute(NSAttributedStringKey.link, value: linkValue, range: (self.attributedString?.string.range(of: attachString.string)?.nsRange)!)
        let paraph = NSMutableParagraphStyle()
        paraph.lineSpacing = 10
        self.attributedString?.addAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.paragraphStyle:paraph], range: NSMakeRange(0, self.attributedString!.length))
        self.protocolTextView.attributedText = self.attributedString;
    }
}

extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}
