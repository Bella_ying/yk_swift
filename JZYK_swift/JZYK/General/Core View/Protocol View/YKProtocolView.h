//
//  YKProtocolView.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/7.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKProtocolView : UIView

@property (nonatomic, readonly, getter=isSelected) BOOL selected;
@property (nonatomic ,copy) void(^ _Nullable SelectStatusDidChangeBlock)(BOOL isSelected);
@property (nonatomic, assign) CGFloat preferredMaxLayoutWidth;

//是否默认选中,不可取消选中
@property (nonatomic, assign) BOOL btnSelected;

/**
 Convenient method
 */
+ (YKProtocolView *_Nonnull(^_Nonnull)(UIView * _Nonnull superView, UIView * _Nonnull topView))makeProtocol;

/**
 布局协议视图
 
 @param topView topview
 */
- (void)yk_layoutWithTopview:(UIView * _Nonnull)topView;


/**
 刷新协议视图
 
 @param entity 数据
 @param protocolString 协议名字符串
 @param urlString 协议跳转字符串
 @param tapAction 点击响应
 */
- (void)yk_refreshProtocolTextEntity:(id _Nonnull )entity
                      protocolString:(NSString *_Nonnull)protocolString
                                 url:(NSString *_Nonnull)urlString
                           tapAction:(void(^_Nullable)(NSAttributedString * _Nonnull text, NSDictionary * _Nonnull obj, NSUInteger idx))tapAction;

/**
 刷新协议视图
 
 @param entity 数据
 @param protocolString 协议名字符串
 @param urlString 协议跳转字符串
 */
- (void)yk_refreshProtocolTextEntity:(id _Nonnull )entity
                      protocolString:(NSString *_Nonnull)protocolString
                                 url:(NSString *_Nonnull)urlString;

@end
