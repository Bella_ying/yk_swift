//
//  YKProtocolView.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/7.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKProtocolView.h"
#import "YYText.h"
#import "UIButton+YKMansonry.h"

@interface YKProtocolView()

@property (nonatomic, readwrite, strong) YYLabel *protocolLabel;
@property (nonatomic, readwrite, getter=isSelected) BOOL selected;
@property (nonatomic,strong) UIButton *protocolButton;
@end

@implementation YKProtocolView

//MARK: Convenient method
+ (YKProtocolView *(^)(UIView *superView, UIView *topView))makeProtocol
{
    return ^(UIView *superView, UIView *topView){
        YKProtocolView *protocolView = [YKProtocolView new];
        [superView addSubview:protocolView];
        [protocolView yk_layoutWithTopview:topView];
        return protocolView;
    };
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.hidden = YES;
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews
{
    UIButton *protocolButton = [UIButton yk_buttonWithImageName:@"choice_normal" superView:self masonrySet:^(UIButton *view, MASConstraintMaker *make) {
        make.left.offset(0);
        make.top.offset(5);
        make.width.height.offset(15);
        view.contentMode = UIViewContentModeScaleAspectFill;
    }];
    self.selected = protocolButton.selected = YES;
    [protocolButton setImage:[UIImage imageNamed:@"choice_pressed"] forState:UIControlStateSelected];
    [protocolButton setImage:[UIImage imageNamed:@"choice_pressed"] forState:UIControlStateHighlighted];
    [protocolButton addTarget:self action:@selector(protocolButtonSelect:) forControlEvents:UIControlEventTouchUpInside];
    self.protocolButton = protocolButton;
    
    YYLabel *protocolLabel = [YYLabel new];
    [self addSubview:protocolLabel];
    [protocolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(protocolButton.mas_right).offset(5);
        make.top.equalTo(protocolButton.mas_top).offset(2);
        make.right.offset(0);
        make.bottom.offset(0);
    }];
    protocolLabel.numberOfLines = 0;
    protocolLabel.userInteractionEnabled = YES;
    self.protocolLabel = protocolLabel;
    protocolLabel.preferredMaxLayoutWidth = WIDTH_OF_SCREEN - 50; //设置最大的宽度后多行约束才正确
}

- (void)setPreferredMaxLayoutWidth:(CGFloat)preferredMaxLayoutWidth {
    _preferredMaxLayoutWidth = preferredMaxLayoutWidth;
    self.protocolLabel.preferredMaxLayoutWidth = preferredMaxLayoutWidth;
}

- (void)yk_layoutWithTopview:(UIView *)topView
{
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(10);
        make.centerX.equalTo(self.superview);
        make.width.mas_equalTo(WIDTH_OF_SCREEN - 30);
    }];
}

- (void)protocolButtonSelect:(UIButton *)button
{
    if (self.btnSelected) {
        self.selected = button.selected = YES;
    }else{
        self.selected = button.selected = !button.selected;
    }
    
    if (self.SelectStatusDidChangeBlock) self.SelectStatusDidChangeBlock(self.selected);
}

- (void)yk_refreshProtocolTextEntity:(id)entity
                      protocolString:(NSString *)protocolString
                                 url:(NSString *)urlString
                           tapAction:(void(^)(NSAttributedString * _Nonnull text, NSDictionary *obj, NSUInteger idx))tapAction
{
    if ([entity isKindOfClass:[NSArray class]] && [(NSArray *)entity count]) {
        NSArray *data = (NSArray *)entity;
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSDictionary class]] && idx == data.count-1 && [[obj allKeys] count] == 2) {
                self.hidden = NO;
            }
        }];
    }else{
        self.hidden = YES;
        return;
    }
    NSArray *data = (NSArray *)entity;
    NSString *startStr = @"我已阅读并同意下列协议";
    NSMutableString *muString = [NSMutableString stringWithString:startStr];
    
    [data enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [muString appendString:obj[protocolString]];
    }];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:muString];
    text.yy_font = UIFont.yk_size(13);
    text.yy_color = [QBColor yk_color_AD_c5];
    text.yy_alignment = NSTextAlignmentLeft;
    text.yy_lineSpacing = 5;
    __block NSInteger location = startStr.length;
    [data enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [text yy_setTextHighlightRange:NSMakeRange(location, [obj[protocolString] length])
                                 color:MAIN_THEME_COLOR
                       backgroundColor:[UIColor clearColor]
                             tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                                 if (tapAction) {
                                     tapAction(text,obj,idx);
                                 }
                             }];
        location = location + [obj[protocolString] length];
    }];
    self.protocolLabel.attributedText = text;
}

- (void)yk_refreshProtocolTextEntity:(id)entity
                      protocolString:(NSString *)protocolString
                                 url:(NSString *)urlString
{
    [self yk_refreshProtocolTextEntity:entity protocolString:protocolString url:urlString
                             tapAction:^(NSAttributedString * _Nonnull text, NSDictionary *obj, NSUInteger idx) {
                                 YKBrowseWebController *simpleWeb = [[YKBrowseWebController alloc] init];
                                 simpleWeb.url = obj[urlString];
                                 [self.yk_viewController.navigationController pushViewController:simpleWeb animated:YES];
                                 DLog("click to jump");
                             }];
}

@end
