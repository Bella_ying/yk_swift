//
//  ChooseLocationView.h
//  ChooseLocation
//
//  Created by Sekorm on 16/8/22.
//  Copyright © 2016年 HY. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZMBAddressSelectionView;

@protocol ZMBAddressSelectionViewDelegate <NSObject>

- (void)addressSelectionView:(ZMBAddressSelectionView *)selectionView willSelectedProvince:(NSInteger)provinceId;
- (void)addressSelectionView:(ZMBAddressSelectionView *)selectionView willSelectedCity:(NSInteger)cityId;
- (void)addressSelectionView:(ZMBAddressSelectionView *)selectionView willSelectedCounty:(NSInteger)countyId;

@end

@interface ZMBAddressSelectionView : UIView

@property (nonatomic,weak) id<ZMBAddressSelectionViewDelegate> delegate;

@property (nonatomic, copy) void(^addressSelectionFinished)(NSInteger provinceId, NSInteger cityId, NSInteger districtId, NSInteger townId, NSString *fullName);

@property (nonatomic,assign) BOOL hiddenNavView;//是否不用导航

- (void)showInView:(UIView *)view;

- (void)reloadProvinceTable:(NSArray *)provinces;
- (void)reloadCityTable:(NSArray *)cities;
- (void)reloadDistrictTable:(NSArray *)districts;
- (void)reloadTownsTable:(NSArray *)towns;



@end
