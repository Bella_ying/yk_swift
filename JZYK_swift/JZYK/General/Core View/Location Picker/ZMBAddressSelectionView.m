//
//  ChooseLocationView.m
//  ChooseLocation
//
//  Created by Sekorm on 16/8/22.
//  Copyright © 2016年 HY. All rights reserved.
//

#import "ZMBAddressSelectionView.h"
#import "ZMBAddressView.h"
#import "ZMBAddressCell.h"
#import "YKMallAddress.h"


#define HYScreenW [UIScreen mainScreen].bounds.size.width

static  CGFloat  const  kHYTopViewHeight = 50; //顶部视图的高度
static  CGFloat  const  kHYTopTabbarHeight = 35; //地址标签栏的高度
#define kTotalHeight (400 * ASPECT_RATIO_WIDTH) //白色块总高度
@interface ZMBAddressSelectionView ()
<UITableViewDataSource,
UITableViewDelegate,
UIScrollViewDelegate,
UIGestureRecognizerDelegate>


@property (nonatomic, strong) UIView *coverView;
@property (nonatomic, strong) UIView *presentView;

@property (nonatomic, weak) ZMBAddressView * topTabbar;
@property (nonatomic, weak) UIScrollView * contentView;
@property (nonatomic, weak) UIView * underLine;

@property (nonatomic, copy) NSArray * provinces;
@property (nonatomic, copy) NSArray * cities;
@property (nonatomic, copy) NSArray * districts;
@property (nonatomic, copy) NSArray *towns;

@property (nonatomic, strong) NSMutableArray * tableViews;
@property (nonatomic, strong) NSMutableArray * topTabbarItems;

@property (nonatomic, strong) YKMallTown *selectedTownItem;

@property (nonatomic, strong) NSMutableArray<NSString *> *addresses;
@property (nonatomic, copy) NSString *fullAddress;

@property (nonatomic, assign) NSInteger provinceID;
@property (nonatomic, assign) NSInteger cityID;
@property (nonatomic, assign) NSInteger districtID;
@property (nonatomic, assign) NSInteger townID;

@end

@implementation ZMBAddressSelectionView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}


- (void)showInView:(UIView *)view
{
   
    if (view) {
        [view addSubview:self];
    }
    else {
        [[UIApplication sharedApplication].delegate.window addSubview:self];
    }
    
    CGRect cframe = self.coverView.frame;
    
    cframe.size.height = self.frame.size.height;
    self.coverView.frame = cframe;
    
    [UIView animateKeyframesWithDuration:0.3 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 animations:^{
            
            CGRect frame = self.presentView.frame;
            CGRect cframe = self.coverView.frame;
            CGFloat y = self.frame.size.height - self.presentView.frame.size.height;
            frame.origin.y = y;
            cframe.size.height = self.frame.size.height - frame.size.height;
            
            self.coverView.frame = cframe;
            self.presentView.frame = frame;
            self.coverView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        }];
        
    } completion:^(BOOL finished) {
    }];
}
- (void)setHiddenNavView:(BOOL)hiddenNavView
{
     CGSize size = [UIScreen mainScreen].bounds.size;
    if (hiddenNavView) {
        self.frame = CGRectMake(0, 0, size.width, size.height +NAV_HEIGHT);
    }else{
        self.frame = CGRectMake(0, 0, size.width, size.height);
    }
    [self layoutIfNeeded];
    
}
- (void)hidden
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = self.presentView.frame;
        CGRect cframe = self.coverView.frame;
        frame.origin.y = self.frame.size.height;
        cframe.size.height = self.frame.size.height;
        self.presentView.frame = frame;
        self.coverView.frame = cframe;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)reloadProvinceTable:(NSArray *)provinces
{
    _provinces = provinces;
    UITableView *tableView = self.tableViews.firstObject;
    [tableView reloadData];
}

- (void)reloadCityTable:(NSArray *)cities
{
    _cities = cities;
    UITableView *tableView = self.tableViews.count > 1 ? self.tableViews[1] : nil;
    [tableView reloadData];
}

- (void)reloadDistrictTable:(NSArray *)districts
{
    _districts = districts;
    UITableView *tableView = self.tableViews.count > 2 ? self.tableViews[2] : nil;
    [tableView reloadData];
}

- (void)reloadTownsTable:(NSArray *)towns
{
//    if (towns.count == 0 || !towns) {
//        [self setUpAddress:self.selectedItem];
//        return;
//    }
    _towns = towns;
    UITableView *tableView = self.tableViews.count > 3 ? self.tableViews[3] : nil;
    [tableView reloadData];
}


#pragma mark - setUp UI

- (void)setUp
{
    CGSize size = [UIScreen mainScreen].bounds.size;
    
    self.frame = CGRectMake(0, 0, size.width, size.height);
    
    _coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0)];
    [self addSubview:_coverView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(hidden)];
    [self.coverView addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    
    _presentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, kTotalHeight)];
    [self addSubview:_presentView];
    _presentView.backgroundColor = WHITE_COLOR;
    
    UIView * topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, kHYTopViewHeight)];
    topView.backgroundColor = [UIColor whiteColor];
    [_presentView addSubview:topView];
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"选择所在省市";
    titleLabel.textColor = Color.color_45_C2;
    titleLabel.font = [UIFont systemFontOfSize:18];
    [titleLabel sizeToFit];
    [topView addSubview:titleLabel];
    
    titleLabel.center = CGPointMake(topView.frame.size.width * 0.5, topView.frame.size.height * 0.5);
    
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [topView addSubview:closeButton];
    closeButton.frame = CGRectMake(topView.frame.size.width - 60 , 0, 45, kHYTopViewHeight);
    [closeButton setImage:[UIImage imageNamed:@"webview_close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(hidden) forControlEvents: UIControlEventTouchUpInside];
    
    
    UIView * separateLine = [self separateLine];
//    [topView addSubview: separateLine];
    
    CGRect separateLineframe = separateLine.frame;
    separateLineframe.origin.y = topView.frame.size.height - separateLine.frame.size.height;
    separateLine.frame = separateLineframe;
    
    
    ZMBAddressView * topTabbar = [[ZMBAddressView alloc]initWithFrame:CGRectMake(0, topView.frame.size.height, _presentView.frame.size.width, kHYTopTabbarHeight)];
    [_presentView addSubview:topTabbar];
    _topTabbar = topTabbar;
    [self addTopBarItem];
    UIView * separateLine1 = [self separateLine];
    [topTabbar addSubview: separateLine1];
    CGRect separateLine1Frame = separateLine1.frame;
    separateLine1Frame.origin.y = topTabbar.frame.size.height - separateLine.frame.size.height;
    separateLine1.frame = separateLine1Frame;
    
    [_topTabbar layoutIfNeeded];
    topTabbar.backgroundColor = [UIColor whiteColor];
    
    UIView * underLine = [[UIView alloc] initWithFrame:CGRectZero];
    [topTabbar addSubview:underLine];
    _underLine = underLine;
    CGRect underLineFrame = underLine.frame;
    underLineFrame.size.height = 2.0f;
    underLine.frame = underLineFrame;
    
    UIButton * btn = self.topTabbarItems.lastObject;
    [self changeUnderLineFrame:btn];
    
    underLineFrame.origin.y = separateLine1.frame.origin.y - underLine.frame.size.height;
    underLine.frame = underLineFrame;
    
    
    _underLine.backgroundColor = [UIColor orangeColor];
    UIScrollView * contentView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(topTabbar.frame), _presentView.frame.size.width, kTotalHeight - kHYTopViewHeight - kHYTopTabbarHeight)];
    contentView.contentSize = CGSizeMake(HYScreenW, 0);
    [_presentView addSubview:contentView];
    _contentView = contentView;
    _contentView.pagingEnabled = YES;
    [self addNewTableView];
    _contentView.delegate = self;
    
}


- (void)addNewTableView
{
    CGFloat heightG = 60 * ASPECT_RATIO_WIDTH;
    if (IS_iPhone5) {
        heightG = 60;
    }
    if (IS_iPhoneX) {
        heightG += BOTTOM_HEIGHT;
        heightG += 20;
    }
    
    
    UITableView * tableView = [[UITableView alloc]initWithFrame:CGRectMake(self.tableViews.count * HYScreenW, 0, HYScreenW, kTotalHeight - kHYTopViewHeight - kHYTopTabbarHeight - heightG)];
    [_contentView addSubview:tableView];
    [self.tableViews addObject:tableView];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = 40;
    [tableView registerClass:[ZMBAddressCell class] forCellReuseIdentifier:@"ZMBAddressCell"];
}

- (void)addTopBarItem
{
    
    UIButton * topBarItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [topBarItem setTitle:@"请选择" forState:UIControlStateNormal];
    [topBarItem setTitleColor:Color.color_66_C3 forState:UIControlStateNormal];
    topBarItem.titleLabel.font = [UIFont systemFontOfSize:14];
    [topBarItem sizeToFit];
    topBarItem.center = CGPointMake(_topTabbar.center.x, _topTabbar.frame.size.height * 0.5);
    [self.topTabbarItems addObject:topBarItem];
    [_topTabbar addSubview:topBarItem];
    [topBarItem addTarget:self action:@selector(topBarItemClick:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - TableViewDatasouce

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if([self.tableViews indexOfObject:tableView] == 0){
        return self.provinces.count;
    }else if ([self.tableViews indexOfObject:tableView] == 1){
        return self.cities.count;
    }else if ([self.tableViews indexOfObject:tableView] == 2){
        return self.districts.count;
    }else if ([self.tableViews indexOfObject:tableView] == 3){
        return self.towns.count;
    }
    return self.provinces.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZMBAddressCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ZMBAddressCell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //省级别
    if([self.tableViews indexOfObject:tableView] == 0){
        
        YKMallProvince * item = self.provinces[indexPath.row];
        cell.addressLabel.text = item.name;
        cell.isSelected = item.isSelected;
        
        //市级别
    }else if ([self.tableViews indexOfObject:tableView] == 1){
        YKMallCity * item = self.cities[indexPath.row];
        cell.addressLabel.text = item.name;
        cell.isSelected = item.isSelected;
        
        //区级别
    }else if ([self.tableViews indexOfObject:tableView] == 2){
        YKMallCounty * item = self.districts[indexPath.row];
        cell.addressLabel.text = item.name;
        cell.isSelected = item.isSelected;
        //县级别
    } else if ([self.tableViews indexOfObject:tableView] == 3) {
        if (indexPath.row < self.towns.count) {
            YKMallTown * item = self.towns[indexPath.row];
            cell.addressLabel.text = item.name;
            cell.isSelected = item.isSelected;
        }
    }

    return cell;
}

#pragma mark - TableViewDelegate
- (void)chooseDetailLocation:(NSString *)name {
    [self addTopBarItem];
    [self addNewTableView];
    [self scrollToNextItem:name];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.tableViews indexOfObject:tableView] == 0){
        //之前有选中省，重新选择省,切换省.
        [self removeLastItemBeforeIndex:0];
        YKMallProvince *item = self.provinces[indexPath.row];
        item.cityIdx = indexPath.row;
        [self chooseDetailLocation:item.name];
        self.provinceID = item.provinceId;
        [self.delegate addressSelectionView:self willSelectedProvince:item.cityIdx];
    }
    else if ([self.tableViews indexOfObject:tableView] == 1){
        //重新选择市,切换市
        [self removeLastItemBeforeIndex:1];
        YKMallCity * item = self.cities[indexPath.row];
        item.countyIdx = indexPath.row;
        [self chooseDetailLocation:item.name];
        self.cityID = item.cityId;
        [self.delegate addressSelectionView:self willSelectedCity:item.countyIdx];
    }
    else if ([self.tableViews indexOfObject:tableView] == 2 && self.districts && self.districts.count > 0){
        //重新选择区,切换区
        [self removeLastItemBeforeIndex:2];
        
        YKMallCounty * item = self.districts[indexPath.row];
        item.townIdx = indexPath.row;
        self.districtID = item.countyId;
        if (item.town) {
            [self chooseDetailLocation:item.name];
            [self.delegate addressSelectionView:self willSelectedCounty:item.townIdx];
            
        } else {
            NSInteger index = self.contentView.contentOffset.x / HYScreenW;
            UIButton * btn = self.topTabbarItems[index];
            [btn setTitle:item.name forState:UIControlStateNormal];
            [btn sizeToFit];
            [_topTabbar layoutIfNeeded];
            self.fullAddress = nil;
            self.fullAddress = [NSString stringWithFormat:@"%@ %@",[self.addresses componentsJoinedByString:@" "], item.name];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self hidden];
                if (self.addressSelectionFinished) {
                     self.addressSelectionFinished(self.provinceID,self.cityID,self.districtID,0, self.fullAddress);
                }
            });
        }
    } else if ([self.tableViews indexOfObject:tableView] == 3 && self.towns && self.towns > 0 ) {
        YKMallTown * item = self.towns[indexPath.row];
        self.selectedTownItem = item;
        self.townID = item.townId;
        NSInteger index = self.contentView.contentOffset.x / HYScreenW;
        UIButton * btn = self.topTabbarItems[index];
        [btn setTitle:item.name forState:UIControlStateNormal];
        [btn sizeToFit];
        [_topTabbar layoutIfNeeded];
        self.fullAddress = nil;
        self.fullAddress = [NSString stringWithFormat:@"%@ %@",[self.addresses componentsJoinedByString:@" "], item.name];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self hidden];
            if (self.addressSelectionFinished) {
                self.addressSelectionFinished(self.provinceID,self.cityID,self.districtID,self.townID, self.fullAddress);
            }
        });
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZMBAddressCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.isSelected = YES;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZMBAddressCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.isSelected = NO;
}

#pragma mark - scrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _contentView) {
        NSInteger index = _contentView.contentOffset.x / HYScreenW;
        [UIView animateWithDuration:0.25 animations:^{
            [self changeUnderLineFrame:self.topTabbarItems[index]];
        }];
    }
}

#pragma mark - private

//点击按钮,滚动到对应位置
- (void)topBarItemClick:(UIButton *)btn
{
    NSInteger index = [self.topTabbarItems indexOfObject:btn];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.contentView.contentOffset = CGPointMake(index * HYScreenW, 0);
        [self changeUnderLineFrame:btn];
    }];
}

//调整指示条位置
- (void)changeUnderLineFrame:(UIButton  *)btn
{
    NSUInteger selectIdx = [self.topTabbarItems indexOfObject:btn];
    [self.topTabbarItems enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL * _Nonnull stop) {
        [button setTitleColor:Color.color_66_C3 forState:UIControlStateNormal];
        if (selectIdx == idx) {
            [btn setTitleColor:Color.main forState:UIControlStateNormal];
        }
    }];
//    CGRect underLineFrame = _underLine.frame;
//    underLineFrame.origin.x = btn.frame.origin.x;
//    underLineFrame.size.width = btn.frame.size.width;
//    _underLine.frame = underLineFrame;
}

//完成地址选择,执行chooseFinish代码块
- (void)setUpAddress:(YKMallTown *)addressItem
{
    NSInteger index = self.contentView.contentOffset.x / HYScreenW;
    UIButton * btn = self.topTabbarItems[index];
    [btn setTitle:addressItem.name forState:UIControlStateNormal];
    
    [btn sizeToFit];
    [_topTabbar layoutIfNeeded];
    [self changeUnderLineFrame:btn];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self hidden];
        
        if (self.addressSelectionFinished) {
            self.addressSelectionFinished(self.provinceID,self.cityID,self.districtID,self.townID, self.fullAddress);
        }
    });
}

//当重新选择省或者市的时候，需要将下级视图移除。
- (void)removeLastItemBeforeIndex:(NSUInteger)idx
{
    while (self.tableViews.count > idx + 1) {
        [self.tableViews.lastObject performSelector:@selector(removeFromSuperview) withObject:nil withObject:nil];
        [self.tableViews removeLastObject];
        
        [self.topTabbarItems.lastObject performSelector:@selector(removeFromSuperview) withObject:nil withObject:nil];
        [self.topTabbarItems removeLastObject];
        [self.addresses removeObjectAtIndex:idx];
    }
}

//滚动到下级界面,并重新设置顶部按钮条上对应按钮的title
- (void)scrollToNextItem:(NSString *)preTitle
{
    NSInteger index = self.contentView.contentOffset.x / HYScreenW;
    UIButton * btn = self.topTabbarItems[index];
    [btn setTitle:preTitle forState:UIControlStateNormal];
    [self.addresses addObject:preTitle];
    [btn sizeToFit];
    [_topTabbar layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        CGSize  size = self.contentView.contentSize;
        self.contentView.contentSize = CGSizeMake(size.width + HYScreenW, 0);
        CGPoint offset = self.contentView.contentOffset;
        self.contentView.contentOffset = CGPointMake(offset.x + HYScreenW, offset.y);
        [self changeUnderLineFrame: [self.topTabbar.subviews lastObject]];
    }];
}


#pragma mark - getter 方法

//分割线
- (UIView *)separateLine
{
    UIView * separateLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0.5)];
    separateLine.backgroundColor = UIColor.yk_hex(@"#E6E6E6");
    return separateLine;
}

- (NSMutableArray *)tableViews
{
    if (_tableViews == nil) {
        _tableViews = [NSMutableArray array];
    }
    return _tableViews;
}

- (NSMutableArray *)topTabbarItems
{
    if (_topTabbarItems == nil) {
        _topTabbarItems = [NSMutableArray array];
    }
    return _topTabbarItems;
}

//省级别数据源
- (NSArray *)provinces
{
    if (_provinces == nil) {
        _provinces = [NSArray array];
        
    }
    return _provinces;
}

//市级别数据源
- (NSArray *)cities
{
    if (_cities == nil) {
        
        _cities = [NSArray array];
    }
    return _cities;
}

//区级别数据源
- (NSArray *)districts
{
    if (_districts == nil) {
        
        _districts = [NSArray array];
    }
    return _districts;
}
//镇级别数据源
- (NSArray *)towns
{
    if (_towns == nil) {
        
        _towns = [NSArray array];
    }
    return _towns;
}
- (NSMutableArray<NSString *> *)addresses
{
    if (!_addresses) {
        _addresses = [NSMutableArray array];
    }
    return _addresses;
}

@end
