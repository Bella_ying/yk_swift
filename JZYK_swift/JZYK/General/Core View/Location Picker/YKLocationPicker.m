//
//  YKLocationPicker.m
//  JZYK
//
//  Created by Jeremy on 2018/6/25.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKLocationPicker.h"
#import "ZMBAddressSelectionView.h"
#import "YKMallAddress.h"

@interface YKLocationPicker()<ZMBAddressSelectionViewDelegate>

@property (nonatomic, strong) ZMBAddressSelectionView *selectView;
//省
@property (nonatomic, strong) NSMutableArray *province;
//市
@property (nonatomic, strong) NSMutableArray *city;
//区
@property (nonatomic, strong) NSMutableArray *county;
//镇
@property (nonatomic, strong) NSMutableArray *town;

@property (nonatomic,strong) YKMallAddress *address;

@end

@implementation YKLocationPicker

- (NSMutableArray *)province
{
    if (!_province) {
        _province = [NSMutableArray array];
    }
    return _province;
}

- (NSMutableArray *)city
{
    if (!_city) {
        _city = [NSMutableArray array];
    }
    return _city;
}

- (NSMutableArray *)county
{
    if (!_county) {
        _county = [NSMutableArray array];
    }
    return _county;
}

- (NSMutableArray *)town
{
    if (!_town) {
        _town = [NSMutableArray array];
    }
    return _town;
}

+ (instancetype)shared
{
    static dispatch_once_t onceToken;
    static YKLocationPicker *picker;
    dispatch_once(&onceToken, ^{
        picker = [YKLocationPicker new];
        [picker fetchAddressData];
    });
    return picker;
}

- (ZMBAddressSelectionView *)selectView
{
    if (!_selectView) {
        _selectView = [ZMBAddressSelectionView new];
        _selectView.delegate = self;
        __weak typeof(self) weakSelf = self;
        [_selectView setAddressSelectionFinished:^(NSInteger provinceId, NSInteger cityId, NSInteger districtId, NSInteger townId, NSString *fullName) {
            NSLog(@"provinceId ------->%ld cityId ------->%ld districtId ------->%ld townId ------->%ld    -------fullname   %@",provinceId,cityId,districtId,townId,fullName);
            if (weakSelf.pickerDidSelect_blk_t) {
                weakSelf.pickerDidSelect_blk_t(provinceId, cityId, districtId, townId, fullName);
            }
        }];
    }
    return _selectView;
}

- (void)yk_showInView:(UIView *)view
{
    
    [self.selectView showInView:view];
}
- (void)setHiddenNavView:(BOOL)hiddenNavView
{
     self.selectView.hiddenNavView = hiddenNavView;
}
//MARK:请求网络数据
- (void)fetchAddressData
{
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kAddressGetAll showLoading:YES param:@{} succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDic, NSInteger code , BOOL success, NSString *msg) {
        if (code == 0 && success) {
//            self.address = [YKMallAddress yy_modelWithJSON:jsonDic];
//            DLog(@"====%@", weakSelf.address);
//            [self.province addObjectsFromArray:weakSelf.address.province];
            NSArray *provinces = [NSArray yy_modelArrayWithClass:[YKMallProvince class] json:jsonDic[@"province"]];
            [self.province addObjectsFromArray:provinces];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.selectView reloadProvinceTable:provinces];
            });
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        
         [[iToast makeText:errMsg] show];
    }];
}

- (void)addressSelectionView:(ZMBAddressSelectionView *)selectionView willSelectedProvince:(NSInteger)provinceId
{
    NSInteger provinceIdx = provinceId;
    [self.city removeAllObjects];
    [self.city addObjectsFromArray:[self.province[provinceIdx] city]];
    [self.selectView reloadCityTable:self.city];
}


- (void)addressSelectionView:(ZMBAddressSelectionView *)selectionView willSelectedCity:(NSInteger)cityId
{
    NSInteger cityIdx = cityId;
    [self.county removeAllObjects];
    [self.county addObjectsFromArray:[self.city[cityIdx] county]];
    [self.selectView reloadDistrictTable:self.county];
}

- (void)addressSelectionView:(ZMBAddressSelectionView *)selectionView willSelectedCounty:(NSInteger)countyId
{
    [self.town removeAllObjects];
    [self.town addObjectsFromArray:[self.county[countyId] town]];
    [self.selectView reloadTownsTable:self.town];
}


@end
