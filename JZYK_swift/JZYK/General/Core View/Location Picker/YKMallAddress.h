//
//  YKLocationPickerEntity.h
//  JZYK
//
//  Created by Jeremy on 2018/6/25.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface YKMallTown: NSObject
@property (nonatomic,assign)NSInteger townId ;//镇id
@property (nonatomic,copy) NSString *name;//镇的名字
@property (nonatomic, assign) BOOL  isSelected;
@property (nonatomic, assign) NSInteger Id;
@end

@interface YKMallCounty: NSObject
@property (nonatomic,assign)NSInteger countyId ;//区id
@property (nonatomic,copy) NSString *name;//区名字
@property (nonatomic, strong) NSArray <YKMallTown *>*town;//镇的数组
@property (nonatomic, assign) BOOL  isSelected;
@property (nonatomic, assign) NSInteger townIdx;
@end

@interface YKMallCity: NSObject
@property (nonatomic,assign)NSInteger cityId ;//市id
@property (nonatomic,copy) NSString *name;//市名字
@property (nonatomic, strong) NSArray <YKMallCounty *>*county;//区的数组
@property (nonatomic, assign) BOOL  isSelected;
@property (nonatomic, assign) NSInteger countyIdx;
@end

@interface YKMallProvince: NSObject

@property (nonatomic,assign)NSInteger provinceId ;//省id
@property (nonatomic,copy) NSString *name;//省名字
@property (nonatomic, strong) NSArray <YKMallCity *>*city;//市的数组
@property (nonatomic, assign) BOOL  isSelected;
@property (nonatomic, assign) NSInteger cityIdx;

@end

@interface YKMallAddress: NSObject
@property (nonatomic, strong) NSArray <YKMallProvince *>*province;//省的数组
@end

@interface YKLocationItem: NSObject

@property (nonatomic, assign) BOOL  isSelected;
@property (nonatomic, assign) NSInteger Id;

@end
