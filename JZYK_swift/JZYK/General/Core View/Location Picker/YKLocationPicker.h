//
//  YKLocationPicker.h
//  JZYK
//
//  Created by Jeremy on 2018/6/25.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKLocationPicker : NSObject

@property (nonatomic, copy) void(^pickerDidSelect_blk_t)(NSInteger provinceId, NSInteger cityId, NSInteger districtId, NSInteger townId, NSString *fullAddress);
@property (nonatomic,assign) BOOL hiddenNavView;//是否不用导航

+ (instancetype)shared;

- (void)yk_showInView:(UIView *)view;

@end
