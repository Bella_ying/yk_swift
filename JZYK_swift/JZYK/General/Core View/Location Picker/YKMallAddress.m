//
//  YKLocationPickerEntity.m
//  JZYK
//
//  Created by Jeremy on 2018/6/25.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKMallAddress.h"
@implementation YKMallTown
+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{@"townId":@"id"};
}
@end

@implementation YKMallCounty
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"town":[YKMallTown class]};
}
+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{@"countyId":@"id"};
}
@end

@implementation YKMallCity
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"county":[YKMallCounty class]};
}
+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{@"cityId":@"id"};
}
@end
@implementation YKMallProvince
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"city":[YKMallCity class]};
}
+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{@"provinceId":@"id"};
}
@end

@implementation YKMallAddress
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"province":[YKMallProvince class]};
}
@end
