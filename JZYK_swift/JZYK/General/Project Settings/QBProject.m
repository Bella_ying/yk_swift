//
//  QBProject.m
//  QuickLoan
//
//  Created by Jeremy Wang on 2018/5/3.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QBProject.h"

@implementation QBProjectElement

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"amount_font":@"fonts.amount_font",
             @"large_title_font":@"fonts.large_title_font",
             @"title_font":@"fonts.title_font",
             @"alert_title_font":@"fonts.alert_title_font",
             @"most_text_font":@"fonts.most_text_font",
             @"input_text_font":@"fonts.input_text_font",
             @"list_text_font":@"fonts.list_text_font",
             @"auxiliary_hint_font":@"fonts.auxiliary_hint_font",
             @"auxiliary_font":@"fonts.auxiliary_font"
             };
}

@end

@interface QBProject()

@property (nonatomic, copy, readwrite) NSArray<QBProjectElement *> *jsonData;
@property (nonatomic, copy, readwrite) NSString *companyName;
@property (nonatomic, copy, readwrite) NSString *shortName;
@property (nonatomic, copy, readwrite) NSString *mainThemeColor;
@property (nonatomic, copy, readwrite) NSString *appName;
@property (nonatomic, copy, readwrite) NSString *appMarket;
@property (nonatomic, copy, readwrite) NSString *scheme_key;

@end

@implementation QBProject

+ (QBProject *)yk_currentProject
{
    static dispatch_once_t  predicate ;
    static QBProject *project = nil;
    dispatch_once(&predicate, ^{
        project = [[QBProject alloc] init];
    });
    return project;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self handleJson];
    }
    return self;
}

+ (NSArray *)getLocalProjectJson
{
    NSString *str = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"project" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
    NSArray *data = [NSString yk_deserializeMessageJSON:str];
    return data;
}

#pragma mark - JSON serialization
- (void)handleJson
{
    self.jsonData = [NSArray yy_modelArrayWithClass:[QBProjectElement class] json:[QBProject getLocalProjectJson]];
    for (QBProjectElement *element in self.jsonData) {
        if ([[[DeviceInfo bundleID] lowercaseString] containsString:element.short_name] || [[DeviceInfo bundleID] containsString:@"ent"]) {
            self.companyName = element.company_name;
            self.shortName =  element.short_name;
            self.mainThemeColor = element.main_theme_color;
            self.appName = element.appName;
            self.appMarket = element.app_market;
            self.scheme_key = element.scheme_key;
            break;
        }
    }
}

@end
