//
//  QBColor.m
//  QuickLoan
//
//  Created by Jeremy on 2018/5/4.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "QBColor.h"

@implementation QBColor

+ (UIColor *)yk_mainThemecolor_c1
{
    return [Color main];
}

+ (UIColor *)yk_mainButtonDisabledColor
{  
    return [Color mainButtonDisabledColor];
}
+ (UIColor *)yk_backgroundColor
{
    return [Color backgroundColor];
}

+ (UIColor *)yk_color_45_c2
{
    return [Color color_45_C2];
}

+ (UIColor *)yk_color_66_c3
{
    return [Color color_66_C3];
}

+ (UIColor *)yk_color_8D_c4
{
    return [Color color_8D_C4];
}

+ (UIColor *)yk_color_AD_c5
{
    return [Color color_AD_C5];
}

+ (UIColor *)yk_color_E6_c6
{
    return [Color color_E6_C6];
}

+ (UIColor *)yk_color_CC_C8
{
    return [Color color_CC_C8];
}

+ (UIColor *)yk_color_DC_C9
{
    return [Color color_DC_C9];
}

+ (UIColor *)color_33_C8
{
    return [Color color_33_C8];
}

+ (UIColor *)anoucementBgColor
{
    return [Color anoucementBgColor];
}

+ (UIColor *)color_slideMiniTrack
{
    return [Color color_slideMiniTrack];
}

+ (UIColor *)yk_colorWithHex:(NSString *)hex
{
    return [Color colorHex:hex];
}

@end
