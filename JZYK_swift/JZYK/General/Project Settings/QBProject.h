//
//  QBProject.h
//  QuickLoan
//
//  Created by Jeremy Wang on 2018/5/3.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import <Foundation/Foundation.h>
@class QBProjectElement;

@interface QBProjectElement : NSObject

@property (nonatomic, copy) NSString *appName;
@property (nonatomic, copy) NSString *short_name;
@property (nonatomic, copy) NSString *scheme_key;
@property (nonatomic, copy) NSString *company_name;
@property (nonatomic, copy) NSString *main_theme_color;
@property (nonatomic, copy) NSString *app_market;
@property (nonatomic, assign) NSInteger amount_font;
@property (nonatomic, assign) NSInteger large_title_font;
@property (nonatomic, assign) NSInteger title_font;
@property (nonatomic, assign) NSInteger alert_title_font;
@property (nonatomic, assign) NSInteger most_text_font;
@property (nonatomic, assign) NSInteger input_text_font;
@property (nonatomic, assign) NSInteger list_text_font;
@property (nonatomic, assign) NSInteger auxiliary_hint_font;
@property (nonatomic, assign) NSInteger auxiliary_font;

@end


@interface QBProject : NSObject

@property (nonatomic, copy, readonly) NSArray<QBProjectElement *> *jsonData;
@property (nonatomic, copy, readonly) NSString *companyName;
@property (nonatomic, copy, readonly) NSString *shortName;
@property (nonatomic, copy, readonly) NSString *mainThemeColor;
@property (nonatomic, copy, readonly) NSString *appName;
@property (nonatomic, copy, readonly) NSString *appMarket;
@property (nonatomic, copy, readonly) NSString *scheme_key;

+ (QBProject *)yk_currentProject;

@end
