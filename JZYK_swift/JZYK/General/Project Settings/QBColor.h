//
//  QBColor.h
//  QuickLoan
//
//  Created by Jeremy on 2018/5/4.
//  Copyright © 2018 JSQB. All rights reserved.
//

/************************************  颜色相关的宏  ************************************/
#define MAIN_THEME_COLOR      [QBColor yk_mainThemecolor_c1]       //主题色
#define LABEL_TEXT_COLOR            [QBColor yk_color_66_c3]             //黑色6
#define GRAY_COLOR_8D         [QBColor yk_color_8D_c4]             //灰色
#define GRAY_COLOR_AD         [QBColor yk_color_AD_c5]             //灰色
#define BORDER_DISABLED_COLOR [QBColor yk_color_E6_c6]             //置灰，不可用
#define LINE_COLOR            [QBColor yk_color_CC_C8]             //分割线
#define GRAY_COLOR_45               [QBColor yk_color_45_c2]             //454545
#define GRAY_BACKGROUND_COLOR [QBColor yk_backgroundColor]         //灰色背景色(主色调)
#define WHITE_COLOR           [QBColor yk_colorWithHex:@"#ffffff"] //白色
#define BLACK_COLOR           [QBColor yk_colorWithHex:@"#000000"] //黑色
#define BLACK_COLOR_33        [QBColor yk_colorWithHex:@"#333333"] //黑色
#define BLUE_COLOR_75         [QBColor yk_colorWithHex:@"#75BBFE"] //蓝色
#define GRAY_COLOR_CC         [QBColor yk_colorWithHex:@"#CCCCCC"] //灰色cc


#import <UIKit/UIKit.h>

@interface QBColor : UIColor

/**
 主色调，用于导航、重要文字、按钮和icon
 
 如首页tab切换icon、文字选中状态、button颜色
 @return 主色调
 */
+ (UIColor *)yk_mainThemecolor_c1;

/**
 按钮被禁用的时候按钮背景颜色
 
 @return 按钮背景颜色
 */
+ (UIColor *)yk_mainButtonDisabledColor;

/**
 用于重要级文字信息、内页标题等主要文字
 
 如导航名称、大板块标题、输入后文字
 @return c2
 */
+ (UIColor *)yk_color_45_c2;

/**
 用于重要级文字信息、内页标题信息
 
 如借款详情已完成的状态、列表正文色等
 @return c3
 */
+ (UIColor *)yk_color_66_c3;

/**
 辅助色、用于部分文字强调等
 
 如button下面提示文字、借款记录时间等
 @return c4
 */
+ (UIColor *)yk_color_8D_c4;

/**
 提示性、待输入状态、时间等文字
 
 如借款详情页还款状态、注册登录填写资料未输入信息等
 @return c5
 */
+ (UIColor *)yk_color_AD_c5;

/**
 用于按钮的边框颜色

 @return c6
 */
+ (UIColor *)yk_color_E6_c6;
/**
 用于分割线、标签描边
 
 @return c8
 */
+ (UIColor *)yk_color_CC_C8;

/**
 用于分割线模块的底色
 
 @return c9
 */
+ (UIColor *)yk_color_DC_C9;

/**
 用于文本颜色
 
 @return c9
 */
+ (UIColor *)color_33_C8;

/**
 用于公告背景色
 
 @return 公告背景色-浅橙
 */
+ (UIColor *)anoucementBgColor;

/**
 用于滑竿
 
 @return 滑竿色-浅橙
 */
+ (UIColor *)color_slideMiniTrack;

+ (UIColor *)yk_colorWithHex:(NSString *)hex;

+ (UIColor *)yk_backgroundColor;

@end
