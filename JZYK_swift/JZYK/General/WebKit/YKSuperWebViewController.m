//
//  YKSuperWebViewController.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKSuperWebViewController.h"
#import "YKJSHandler.h"
#import "YKWebKitSupport.h"

static NSString *const kProgressKeyPathKey = @"estimatedProgress";
static NSString *const kNavTitleKeyPathKey = @"title";

@interface YKSuperWebViewController ()<WKNavigationDelegate,WKUIDelegate, NetworkChangedDelegate>

@property (nonatomic, strong) YKJSHandler *jsHandler;
@property (nonatomic, assign) CGFloat lastProgress;//上次进度条位置
@property (nonatomic, strong) YKEmptyView *emptyView;
@property (nonatomic, copy) NSString *userAgent;

@end

@implementation YKSuperWebViewController

- (void)setUrl:(NSString *)url
{
    _url = [url yk_isValidString] ? url : kOfficialWebsite;
    
    NSDictionary *parameters = [_url containsString:@"clientType=h5"] ? @{@"fromapp": @1} : @{@"fromapp":@1, @"clientType":@"iOS"};
    _url = [NSString yk_addQueryStringToUrl:_url params:parameters];
    _url = [NSString stringWithFormat:@"%@&%@",_url,[DeviceInfo appVersion]];
    _url = [_url yk_urlClear];
    [self configUserAgent];
    [self webReload];
}

/**
 网页加载
 */
- (void)webReload
{
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:_url]];
    
    if (is_User_logined) {
        NSString *cookieStr = [YKWebKitSupport yk_fetchLoginUserSession];
        DLog(@"=====%@",cookieStr);
        
        //请求头传入cookie
        [urlRequest setHTTPShouldHandleCookies:YES];
        //设置请求头部信息
        [urlRequest setValue:cookieStr forHTTPHeaderField:@"Cookie"];
    }
    [[ReachabilityManager defaultReachability] ennaleWatch];
    [ReachabilityManager defaultReachability].delegate = self;
    [self.webView loadRequest:urlRequest];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initWKWebView];
    //适配iOS11
    if (@available(iOS 11.0, *)){
        self.webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)configUserAgent
{
    NSString *newUserAgent = [YKTools yk_getGlobalUserAgent];
    if (@available(iOS 9.0, *)) {
        if (![self.webView.customUserAgent containsString:@"jzyk"]) {
            [self.webView setCustomUserAgent:newUserAgent];
        }
    }else { //小于9.0
        if (![[self.webView valueForKey:@"applicationNameForUserAgent"] containsString:@"jzyk"]) {
            [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"userAgent":newUserAgent}];
            [self.webView setValue:newUserAgent forKey:@"applicationNameForUserAgent"];
        }
    }
}

/**
 对WkWebView及其所属UI控件进行初始化
 */
-(void)initWKWebView
{
   self.jsHandler = [[YKJSHandler alloc] initWebViewController:self configuration:self.webConfiguration];
    
    [self configUserAgent];
    
    
    WEAK_SELF
    [self.webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        DLog(@"result ===> %@",result);
        
    }];
    [self.view addSubview:self.webView];
    [self.webView addSubview:self.progressView];
    //监控进度
    [self.webView addObserver:self forKeyPath:kProgressKeyPathKey options:NSKeyValueObservingOptionNew context:nil];
}

- (WKWebViewConfiguration *)webConfiguration
{
    if (!_webConfiguration) {
        _webConfiguration = [WKWebViewConfiguration new];
        _webConfiguration.preferences.javaScriptEnabled = YES;//打开js交互
        // 以下两个属性是允许H5视屏自动播放,并且全屏,可忽略
        _webConfiguration.allowsInlineMediaPlayback = YES;
        _webConfiguration.mediaPlaybackRequiresUserAction = NO;
    }
    return _webConfiguration;
}

#pragma mark lazy load
- (WKWebView *)webView
{
    if (!_webView) {
        _webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN,  HEIGHT_OF_SCREEN - NAV_HEIGHT) configuration:self.webConfiguration];
        _webView.navigationDelegate = self;
        _webView.backgroundColor = [UIColor clearColor];
        _webView.scrollView.bounces = NO;
        _webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
        _webView.UIDelegate = self;
    }
    return _webView;
}

- (UIProgressView *)progressView
{
    //进度条
    if (!_progressView) {
        _progressView = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progressView.tintColor = MAIN_THEME_COLOR;
        _progressView.trackTintColor = [UIColor clearColor];
        _progressView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 3.0);
    }
    return _progressView;
}

#pragma mark --进度条
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:kProgressKeyPathKey]) {
        [self updateProgress:self.webView.estimatedProgress];
    }
}

#pragma mark -  更新进度条
-(void)updateProgress:(CGFloat)progress
{
    self.progressView.alpha = 1;
    if(progress > _lastProgress){
        [self.progressView setProgress:self.webView.estimatedProgress animated:YES];
    }else{
        [self.progressView setProgress:self.webView.estimatedProgress];
    }
    _lastProgress = progress;
    
    if (progress >= 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.progressView.alpha = 0;
            [self.progressView setProgress:0];
            self.lastProgress = 0;
            self.navigationItem.title = self.webView.title;  //为应对VUE的异步编程问题，在页面加载完后赋值title
        });
    }
}

#pragma mark --navigation delegate
-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    DLog(@"absoluteString ===> %@",webView.URL.absoluteString);
    [self showSearchRightItem:[webView.URL.absoluteString containsString:@"isShowSearch"]];
    //更新返回按钮
    [self updateNavigationItems];
    if (self.startLoadWeb_blk) {
        self.startLoadWeb_blk(webView, navigationAction);
    }
    WKNavigationActionPolicy policy = WKNavigationActionPolicyAllow;
    NSURL * url = navigationAction.request.URL;
    
    [self openTheDisabledUrl:url policy:&policy];
    
    decisionHandler(policy);
}

//MARK:打开禁用的一些URL链接
- (void)openTheDisabledUrl:(NSURL *)url policy:(WKNavigationActionPolicy *)policy
{
    //打开wkwebview禁用了电话和跳转appstore 通过这个方法打开
    UIApplication *app = [UIApplication sharedApplication];
    
    if ([url.scheme isEqualToString:@"tel"] ||
        [url.host containsString:@"itunes.apple.com"] ||
        [url.absoluteString containsString:@"alipay://alipayclient"] ||
        [url.absoluteString containsString:@"taobao://"] ||
        [url.absoluteString containsString:@"lipayqr://"] ||
        [url.absoluteString containsString:@"alipays://"] ||
        [url.absoluteString containsString:@"wechat://"] ||
        [url.absoluteString containsString:@"weixin://"]) {
        if ([app canOpenURL:url]) {
            [app openURL:url];
        }
        *policy = WKNavigationActionPolicyCancel;
    }
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    
    NSHTTPURLResponse *response = (NSHTTPURLResponse *)navigationResponse.response;
    // 获取cookie,并设置到本地
    NSArray *cookies =[NSHTTPCookie cookiesWithResponseHeaderFields:[response allHeaderFields] forURL:response.URL];
    for (NSHTTPCookie *cookie in cookies) {
        if ([cookie.name isEqualToString:kSessionID]) {
            if (is_User_logined) {
                if (![cookie.value isEqualToString:[YKUserManager sharedUser].sessionId]) {
                    NSMutableDictionary *cookieDict = [NSMutableDictionary dictionary];
                    //将cookie的属性字典拼接到可变字典里，方便修改
                    [cookieDict addEntriesFromDictionary:cookie.properties];
                    [cookieDict setObject:[YKUserManager sharedUser].sessionId forKey:cookie.name];
                    NSHTTPCookie *cookieTemp = [NSHTTPCookie cookieWithProperties:cookieDict];
                    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookieTemp];
                }else{
                    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
                }
                DLog(@"^^^^^^^^^^^^^^^^^");
            }
        }
    }
    decisionHandler(WKNavigationResponsePolicyAllow);
}

//准备加载页面
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    if (self.didStartLoadWeb_blk) {
        self.didStartLoadWeb_blk(webView);
    }
    
}

//加载完毕
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
//    self.navigationItem.title = webView.title;//应对前端框架改变造成的标题后置刷新问题；
    [self updateNavigationItems];
    [self updateProgress:webView.estimatedProgress];
    if (self.afterLoadFinish_blk) {
        self.afterLoadFinish_blk(webView);
    }
    //解决ajax发起请求无效的问题
    [YKWebKitSupport yk_injectJSForWebview:webView];
    if ([webView.URL.absoluteString containsString:@"choose-repay-type"]) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [webView reload];
        });
    }
}

//加载失败
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    if (self.afterLoadFaild_blk) {
        self.afterLoadFaild_blk(webView);
    }
}

//解决白屏问题
- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView
{
    [webView reload];
}

#pragma mark - WKUIDelegate
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提醒" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)dealloc
{
    DLog(@"%s",__FUNCTION__);
    [_jsHandler cancelHandler];
    self.webView.navigationDelegate = nil;
    [self.webView removeObserver:self forKeyPath:kProgressKeyPathKey];
}

- (void)isConnect:(BOOL)connect
{
    if (self.emptyView) {
        [self.emptyView removeFromSuperview];
    }
    if (!connect) {
        self.emptyView = [YKEmptyView dn_emptyViewReloadClick:^{
            [self webReload];
        }](self.view,YKEmptyViewCoverTypeNoNetwork);
    }
}
  
/**
 更新导航按钮，子类需要重载
 */
- (void)updateNavigationItems {}

- (void)showSearchRightItem:(BOOL)show {}

@end
