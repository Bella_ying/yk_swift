//
//  YKBaseWebViewController.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKSuperWebViewController.h"

/*
 * 返回到指定的页面
 */
typedef NS_ENUM(NSUInteger, PopToDesignatedPage) {
    PopToDesignatedPageNone = 0,   //默认，从哪来到哪去
    PopToDesignatedPageMine,       //返回到 我的
    PopToDesignatedPageMall,       //返回到 商城
    PopToDesignatedPageMallComfirmOrder, //返回到 商城确认订单页
    PopToDesignatedPageVerifyCenter, //返回到 认证中心
};

@interface YKBaseWebViewController : YKSuperWebViewController

//在多级跳转后，是否在返回按钮右侧展示关闭按钮
@property (nonatomic, ) BOOL isShowCloseBtn;
@property (nonatomic, ) BOOL isShowBackBtn;                                        //是否展示返回按钮
@property (nonatomic, ) BOOL isReload;                                             //刷新网页
@property (nonatomic, ) NSInteger uid;                                             //是否登录过
@property (nonatomic, strong) UIBarButtonItem *closeItem;
@property (nonatomic, strong) UIBarButtonItem *backItem;
@property (nonatomic, strong) UIBarButtonItem *rightBarButtonItem;           //右上角长留按钮
@property (nonatomic, strong) UIBarButtonItem *searchRightItem;              //右边搜索按钮
@property (nonatomic, strong) NSArray<UIBarButtonItem *> *rightBarButtonItems;
@property (nonatomic, copy) void (^itemBlock)(YKBaseWebViewController *activity);   //在导航栏右边自定义不同的点击事件
@property (nonatomic, copy) NSString *navTitle ;
@property (nonatomic, assign) PopToDesignatedPage popToDesignatedPage;             //返回到指定的页面

//关闭按钮点击事件，同上
- (void)closeAction:(id)sender;

@end
