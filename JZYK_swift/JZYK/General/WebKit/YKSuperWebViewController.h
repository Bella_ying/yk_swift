//
//  YKSuperWebViewController.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"
#import <WebKit/WebKit.h>



@interface YKSuperWebViewController : YKBaseViewController

//webview delegate block
@property (nonatomic, copy) void (^startLoadWeb_blk)(WKWebView *webView, WKNavigationAction *navigationAction);
@property (nonatomic, copy) void (^didStartLoadWeb_blk)(WKWebView *webView);
@property (nonatomic, copy) void (^afterLoadFinish_blk)(WKWebView *webView);
@property (nonatomic, copy) void (^afterLoadFaild_blk)(WKWebView *webView);

//跳转的时候要用到
@property (nonatomic, strong) UIViewController *viewController;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) WKWebViewConfiguration *webConfiguration;
@property (nonatomic, copy)   NSString *url;

//更新进度条
-(void)updateProgress:(CGFloat)progress;

//更新导航栏按钮，子类去实现
-(void)updateNavigationItems;

//刷新web
- (void)webReload;

//是否展示search
- (void)showSearchRightItem:(BOOL)show;

@end
