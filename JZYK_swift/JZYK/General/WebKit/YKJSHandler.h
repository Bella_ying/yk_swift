//
//  YKJSHandler.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
@class YKSuperWebViewController;

@interface YKPasteEntity : NSObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *tip;

@end

@interface YKJSHandler : NSObject

@property (nonatomic, weak, readonly) YKSuperWebViewController * webVC;
@property (nonatomic, strong, readonly) WKWebViewConfiguration * configuration;
//@property (nonatomic, strong) KDAttachView *redAttach;

- (instancetype)initWebViewController:(YKBaseViewController *)webVC
                        configuration:(WKWebViewConfiguration *)configuration;

- (void)cancelHandler;

@end
