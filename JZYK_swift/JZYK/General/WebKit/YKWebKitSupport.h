//
//  YKWebKitSupport.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@interface YKWebKitSupport : NSObject

@property (nonatomic, strong, readonly) WKProcessPool *processPool;

+ (instancetype)sharedSupport;

/**
 js注入

 @param webview webview
 */
+ (void)yk_injectJSForWebview:(WKWebView *)webview;

/**
 获取登录用户的session

 @return sessionID
 */
+ (NSString *)yk_fetchLoginUserSession;


@end
