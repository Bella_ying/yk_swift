//
//  YKBrowseWebControllerViewController.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKBrowseWebController.h"

@interface YKBrowseWebController ()

@end

@implementation YKBrowseWebController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //导航栏显示，返回，关闭按钮
    self.isShowBackBtn = YES;
    self.isShowCloseBtn = YES;
    
}


@end
