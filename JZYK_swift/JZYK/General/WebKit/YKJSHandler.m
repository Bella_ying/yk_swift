//
//  YKJSHandler.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKJSHandler.h"
#import "YKShareEntity.h"
#import "YKProgressHud.h"
#import "YKSuperWebViewController.h"
#import "YKClickManager.h"
#import "QiYuManager.h"
#import "YKAlbumManager.h"

@implementation YKPasteEntity
@end

@interface YKJSHandler()<WKScriptMessageHandler, UIActionSheetDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) YKShareEntity *entity;
@property (nonatomic, strong) UIButton *btnRight;
@property (nonatomic, strong) NSString *orderId;
/**
 支付宝数据抓取进度
 */
@property (nonatomic, copy)  NSString  * progress;
@property (nonatomic, strong) YKProgressHud *hud;
@property (nonatomic, strong) UIButton * stopBtn;
@property (nonatomic, strong) UIView * topView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIActionSheet *uploadSheet;
/**分享成功回调*/
@property(nonatomic, copy) NSString * platformStr;
//分享成功后，分享的平台；QQ、微信
@property(nonatomic, copy)  NSString *platformTypeStr;

@end

@implementation YKJSHandler

- (instancetype)initWebViewController:(YKBaseViewController *)webVC
                        configuration:(WKWebViewConfiguration *)configuration;
{
    self = [super init];
    if (self) {
        _webVC = (YKSuperWebViewController*)webVC;
        _configuration = configuration;
        //注册JS 事件
        [configuration.userContentController addScriptMessageHandler:self name:@"backPage"];
        [configuration.userContentController addScriptMessageHandler:self name:@"Share"];
        [configuration.userContentController addScriptMessageHandler:self name:@"returnNativeMethod"];
        [configuration.userContentController addScriptMessageHandler:self name:@"copyTextMethod"];
        [configuration.userContentController addScriptMessageHandler:self name:@"makePhoneCall"];
        [configuration.userContentController addScriptMessageHandler:self name:@"checkAppIsInstalled"];
        [configuration.userContentController addScriptMessageHandler:self name:@"hidenTabBar"];
        //支付宝爬虫
        [configuration.userContentController addScriptMessageHandler:self name:@"fetchingAlipay"];
        [configuration.userContentController addScriptMessageHandler:self name:@"uploadPage"];
        [configuration.userContentController addScriptMessageHandler:self name:@"toNativeWebview"];
        [configuration.userContentController addScriptMessageHandler:self name:@"openBrowser"];
        [configuration.userContentController addScriptMessageHandler:self name:@"hideTitle"];
    }
    return self;
}

#pragma mark -  JS 调用 Native  代理
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    //    message.body  --  Allowed types are NSNumber, NSString, NSDate, NSArray,NSDictionary, and NSNull.
    DLog(@"body:%@",message.body);
    if ([message.name isEqualToString:@"backPage"]) {
        //返回
        if (self.webVC.presentingViewController) {
            [self.webVC dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self.webVC.navigationController popViewControllerAnimated:YES];
        }
    }else if ([message.name isEqualToString:@"Share"]){
        //分享
        [self shareMethod:message.body];
    }else if ([message.name isEqualToString:@"returnNativeMethod"]){
        //调用原生的方法
        [self returnNativeMethod:message.body];
    }else if ([message.name isEqualToString:@"copyTextMethod"]){
        //复制
        [self copyTextMethod:message.body];
    }else if ([message.name isEqualToString:@"makePhoneCall"]){
        //打电话
        [self makePhoneCall:message.body];
    }else if ([message.name isEqualToString:@"checkAppIsInstalled"]){
        //检查是否只能装了某个app
        [self checkAppIsInstalled:message.body];
    } else if ([message.name isEqualToString:@"fetchingAlipay"]){
        [self goneLayout:message.body];
    } else if ([message.name isEqualToString:@"uploadPage"]){
        [self uploadPage];
    } else if ([message.name isEqualToString:@"hidenTabBar"]) {
        [self hidenTabBar];
    }else if ([message.name isEqualToString:@"toNativeWebview"]){
        [self toNativeWebview:message.body];
    }else if ([message.name isEqualToString:@"openBrowser"]){
        [self openBrowser:message.body];
    }
    else if ([message.name isEqualToString:@"hideTitle"]) {
        [self hideTitle:[message.body integerValue]];
    }
}

#pragma mark JS调用原生分享方法
- (void)shareMethod:(NSString *)paramStr
{
    if (![paramStr isKindOfClass:[NSString class]]) return;
    
    NSString *jsonString = paramStr;
    jsonString = [jsonString yk_clearString:jsonString];
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *param = nil;
    NSError *error = nil;
    if (jsonData) {
        param = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    }
    
    if (error) {
        jsonString = [paramStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        jsonString = [jsonString yk_clearString:jsonString];
        jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error1 = nil;
        param = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error1];
        if (error1) {
            DLog(@"%@",error1.userInfo);
            return;
        }
    }
    
    WEAK_SELF
    dispatch_async(dispatch_get_main_queue(), ^{
        STRONG_SELF
        strongSelf.entity = [YKShareEntity yy_modelWithDictionary:param];
        switch ([strongSelf.entity.type integerValue]) {
            case 1:
                strongSelf.webVC.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"分享" style:UIBarButtonItemStylePlain target:self action:@selector(shareBtnClick)];
                break;
            case 2:
                [[YKShareManager shareManager] yk_singleShare:self.entity];
                break;
            default:
                [strongSelf shareBtnClick];
                break;
        }
    });
}

- (NSDictionary <NSString *,NSString *>*)platformMap{
    return @{
             @"WEIXIN":@"微信",
             @"WEIXIN_CIRCLE":@"朋友圈",
             @"SINA":@"新浪微博",
             @"QQ":@"QQ",
             @"QZONE":@"QQ空间"
             };
}

- (void)shareBtnClick
{
    YKShareManager *share = [YKShareManager shareManager];
    [share yk_showWithShareEntity: self.entity];
    [share setShareSuccess_blk:^(NSString *platform, NSString *platformTypeStr) {
        self.platformStr = platform;
        self.platformTypeStr = platformTypeStr;
        __block NSString *mapString = nil;
        [self.platformMap enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
            if ([platform isEqualToString:key]) {
                mapString = obj;
                *stop = YES;
            }
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[iToast makeText:@"分享成功"] show];
            [self shareSuccessReport];
        });
    }];
}

- (void)sharesuccess
{
    NSString * tempstr = [NSString stringWithFormat:@"shareResult('%@','%@','%@',iOS)",self.entity.callback,self.platformStr,self.entity.params];
    [self.webVC.webView evaluateJavaScript:tempstr completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        
    }];
}

-(void)shareSuccessReport
{
    NSString *sourceUrl = self.webVC.url ? self.webVC.url : @"" ;
    self.platformTypeStr = self.platformTypeStr ? self.platformTypeStr : @"";
    NSString *currentUrl = self.entity.share_url ? self.entity.share_url :@"";
    [[InterfaceReport defaultReport] reportInfoAfterShareWithCurrentUrl:currentUrl sourceUrl:sourceUrl platformString:self.platformTypeStr];
}


/**
 隐藏web头部文字

 @param hide 1隐藏， 0 不隐藏
 */
- (void)hideTitle:(NSInteger)hide
{
    if (hide == 1) {
        self.webVC.navigationItem.title = @"";
    }
}

#pragma mark JS调用原生的方法
- (void)returnNativeMethod:(NSString*)paramStr
{
    if (![paramStr isKindOfClass:[NSString class]]) return;
    
    NSString *jsonString = [paramStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *param = nil;
    if (jsonData) {
        NSError *serializationError = nil;
        param = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&serializationError];
        if (serializationError) {
            DLog(@"%@",serializationError.userInfo);
            return;
        }
    }
    
    //通过网页跳转原生页面
    if (param[@"skip_code"]) {
        [[YKJumpManager shareYKJumpManager] yk_jumpWithParamDic:param fromH5:YES];
        return;
    }
    self.orderId = param[@"order_id"];
    NSInteger type = [param[@"type"] integerValue];
    
    switch (type) {
        case 0:
        {
            //加一下延迟 防止返回到上一个页面显示不正常
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self.webVC.navigationController popViewControllerAnimated:NO];
            });
        }
            break;
        case 1:
        {
            
            
        }
            break;
        case 2:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
//                XYKLoginRegisterVC *gesters = [[XYKLoginRegisterVC alloc] initWithType:FINDTRADINGPWDCHECK];
//                gesters.phone = [UserManager sharedUser].username;
//                [self.webVC.navigationController pushViewController:gesters animated:YES];
            });
            
        }
            break;
        case 3:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
//                [[KDTabBarController shareTabController] ql_setSelectedIndex:0 viewController:[KDVerifyListVC new]];
            });
            
        }
            break;
        case 4:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
//                [[KDTabBarController shareTabController]ql_setSelectedIndex:0 viewController:nil];
            });
            
        }
            break;
        case 5:
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (param[@"is_help"] && [[NSString stringWithFormat:@"%@",param[@"is_help"]] integerValue] == 1) {
//                    [self contactQQ:[NSString stringWithFormat:@"%@",param[@"qq"]]];
                    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"帮助" style:(UIBarButtonItemStylePlain) target:self action:@selector(rightHelpItemAction)];
                    rightItem.tintColor = LABEL_TEXT_COLOR;
                    [rightItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateNormal];
                    self.webVC.navigationItem.rightBarButtonItem = rightItem;
                }else
                {
//                    self.webVC.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"咨询客服" style:UIBarButtonItemStylePlain target:self action:@selector(contactQQ:)];
                }
            });
            
        }
            break;
        case 7:{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                UserModel *userModel = [UserModel kd_modelWithJSON:param[@"user"]];
//                [[UserManager sharedUser] loginSuccessToUpdateUserInfo:userModel];
//
//                if ([NSUSER_DEFAULT boolForKey:@"loginFromUserTab"]) {
//                    [[KDTabBarController shareTabController] ql_setSelectedIndex:(([KDTabBarController shareTabController].viewControllers.count) - 1) viewController:nil];
//                    [NSUSER_DEFAULT removeObjectForKey:@"loginFromUserTab"];
//                }
//                [[KDCacheManager sharedCacheManager]cacheObject:@"no" withKey:QCAlertShow];
//                [self.webVC.navigationController dismissViewControllerAnimated:YES completion:nil];
//                [[NSNotificationCenter defaultCenter] postNotificationName:kUserLoginSuccessNotiKey object:nil];
//            });
        }
            break;
        case 8:
        {
            //借款列表
            dispatch_async(dispatch_get_main_queue(), ^{
//                KDTradeRecordVC *vcTradeRecord = [[KDTradeRecordVC alloc]init];
//                [self.webVC.navigationController pushViewController:vcTradeRecord animated:YES];
            });
        }
            break;
        case 9:{
            dispatch_async(dispatch_get_main_queue(), ^{
//                [KDAlbumManager shareAlbumInstance].objectId = param[@"object_id"];
//                _uploadSheet = [[UIActionSheet alloc] initWithTitle:kRepaymentAleartTip delegate:self cancelButtonTitle:kAleartCancel destructiveButtonTitle:nil otherButtonTitles:kRepaymentCamera,kRepaymentPhotoAlbum, nil];
//                _uploadSheet.delegate = self;
//                _uploadSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
//                [_uploadSheet showInView:self.webVC.view];
            });
        }
            break;
        case 10:{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:param[@"url"]]];
            });
        }
            break;
        case 11:{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.webVC.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"催收投诉" style:UIBarButtonItemStylePlain target:self action:@selector(navRightBtnClick)];
            });
        }
            break;
        default:
        {
        }
            break;
    }
    //0:返回原生 1.忘记密码 2.忘记交易密码 3.认证页面 4.借款首页 5.小钱包增加帮助按钮 6.拒就赔红包 7.前程数据授权回调 8.借款纪录9.上传还款凭证
    DLog(@">>>>>>>%@",param);
}

#pragma mark - 催收投诉跳转
- (void)navRightBtnClick
{
    DLog(@"催收投诉跳转");
}
#pragma mark - 到七鱼
- (void)rightHelpItemAction{
    //到7鱼七鱼
    [QiYuManager yk_sharedQiYuManager].commonQuestionTemplateId = 0;
    [[QiYuManager yk_sharedQiYuManager] yk_qiYuService];
    [[QiYuManager yk_sharedQiYuManager] yk_qiYuDelegate];
}
#pragma mark - 去qq
- (void)contactQQ:(NSString *)qq
{
    //埋点（帮助中心）
    [YKClickManager yk_reportWithKey:@"help_center" array:@[@"帮助中心"]];
    BOOL success = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mqqwpa://im/chat?chat_type=crm&uin=%@&version=1&src_type=web&web_src=file:://",qq]]];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self alertShow:success ? @"1" : @"0"];
    });
}

- (void)alertShow:(NSString *)success
{
    if ([success isEqualToString:@"0"]) {
        
        [[QLAlert alert] showWithTitle:@"" message:@"跳转异常，请确认是否安装过qq" btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {

        }];
    }
}

#pragma mark - 复制粘贴版
- (void)copyTextMethod:(NSString *)paramStr
{
    if (![paramStr isKindOfClass:[NSString class]]) return;
    NSString *jsonString = [paramStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *param = nil;
    if (jsonData) {
        NSError *serializationError = nil;
        param = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&serializationError];
        if (serializationError) {
            DLog(@"%@",serializationError.userInfo);
            return;
        }
    }
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    YKPasteEntity *entity = [YKPasteEntity yy_modelWithJSON:param];
    if (![entity.text isEqualToString:@""]&&![entity.tip isEqualToString:@""]) {
        pasteboard.string = entity.text;
        [[iToast makeText:entity.tip]show];
    }
    DLog(@"%@",param);
}

#pragma mark- 打电话
- (void)makePhoneCall:(NSString *)phoneNum
{
    if (phoneNum) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", phoneNum]];
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark- 判断客户端是否安装了某个APP
- (BOOL)checkAppIsInstalled:(NSString *)url
{
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]];
}

#pragma mark - 上传凭证
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [YKAlbumManager shareAlbumInstance].responseSuccessBlock = ^(){
        [self.webVC.webView reload];
    };
    if (buttonIndex == 0) {
        [[YKAlbumManager shareAlbumInstance] showCamera];
    }
    else if (buttonIndex == 1){
        [[YKAlbumManager shareAlbumInstance] showPhotoAlbum];
    }
}

#pragma mark - 支付宝爬虫相关
//==========支付宝爬虫相关========
//遮罩 liang882288  13949102946
- (void)goneLayout:(NSString *)param
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.hud) {
            [self certTopview];
//            self.hud = [YKProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
            self.hud.frame = CGRectMake(0, NAV_HEIGHT, WIDTH_OF_SCREEN, HEIGHT_OF_SCREEN - NAV_HEIGHT );
            self.hud.backgroundColor=[UIColor grayColor];
        }else{
            if (param.intValue == 0) {
                self.hud.hidden = NO;
            }else{
                self.hud.hidden = YES;
            }
        }
    });
}

#pragma mark - 取缓存
- (NSString *)getText:(NSString *)param
{
    YKCacheManager *cacheM = [YKCacheManager sharedCacheManager];
    return [cacheM yk_getCacheObjectforKey:param atDirectoryPath:ZFBPath] ? [cacheM yk_getCacheObjectforKey:param atDirectoryPath:ZFBPath] : @"";
}

#pragma mark - 缓存
- (void)saveText:(NSString *)param  :(NSString *)text
{
    DLog(@"%@ %@",param,text);
    YKCacheManager *cacheM = [YKCacheManager sharedCacheManager];
    if ([cacheM yk_getCacheObjectforKey:param atDirectoryPath:ZFBPath]) {
        [cacheM yk_removeCacheObjectForKey:param atDirectoryPath:ZFBPath];
    }
    [cacheM yk_cacheObject:text forKey:param atDirectoryPath:ZFBPath];
}

//进度条
-(void)setProgress:(NSString *)pararm
{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        self. hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
//        self. hud.label.text = NSLocalizedString(@"验证中,请耐心等待...", @"HUD loading title");
//        [MBProgressHUD HUDForView:[UIApplication sharedApplication].keyWindow ].progress = pararm.floatValue/100.f;
//        if (pararm.floatValue==100.f) {
//            [self.hud hideAnimated:YES];
//        }
//    });
}

//上传抓取到网页
-(NSString*)uploadPage
{
    //获取当前加载网页的源码
    NSString *jsToGetHTMLSource = @"document.getElementsByTagName('html')[0].innerHTML";
    __block  NSString *HTMLSource = nil;
    [self.webVC.webView evaluateJavaScript:jsToGetHTMLSource completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        HTMLSource = result;
    }];
    return HTMLSource;
}

- (void)hidenTabBar
{
    if ([NSStringFromClass([self.webVC class]) isEqualToString:@"YKMallWebViewController"]) {
        
        self.webVC.navigationController.tabBarController.tabBar.hidden = YES;
    }
}

#pragma mark - 去原生的webView页
- (void)toNativeWebview:(NSString*)url
{
    YKBrowseWebController *webVC = [[YKBrowseWebController alloc] init];
    webVC.url = url;
    [self.webVC.navigationController pushViewController:webVC animated:YES];
}

#pragma mark - 借款协议中点击"下载"按钮打开自带的浏览器，然后下载
- (void)openBrowser:(NSString*)url
{
    if (![url isKindOfClass:[NSString class]]) return;
    [[UIApplication sharedApplication ] openURL:[NSURL URLWithString:url]];
}

#pragma mark - 创建假导航栏
-(void)certTopview
{
    self.topView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,WIDTH_OF_SCREEN , NAV_HEIGHT)];
    self.topView.backgroundColor=MAIN_THEME_COLOR;
    [[UIApplication sharedApplication].keyWindow addSubview:self.topView];
    
    self.stopBtn=[UIButton buttonWithType:UIButtonTypeSystem];
    self.stopBtn.frame=CGRectMake(0, 22, 36, 36);
    [self.stopBtn setImage:[UIImage imageNamed:@"webview_back"] forState:UIControlStateNormal];
    [self.topView addSubview:self.stopBtn];
    [self.stopBtn addTarget:self action:@selector(stopBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.topView.centerX-40.f, 24.f, 160.f, 30.f)];
    self.titleLabel.text = @"数据验证中";
    self.titleLabel.textColor=[UIColor whiteColor];
    [self.topView addSubview:self.titleLabel];
}

-(void)stopBtnClick
{
    WEAK_SELF
    [[QLAlert alert] showWithTitle:@"提示" message:@"返回操作将中断支付宝认证,确认要退出吗？" btnTitleArray:@[@"取消认证",@"继续认证"] btnClicked:^(NSInteger index) {
        STRONG_SELF
        if (index == 0) {
            [strongSelf pushlastPage];
        }
    }];
}

-(void)pushlastPage
{
    [self.topView removeFromSuperview];
    self.topView = nil;
    [self.hud removeFromSuperview];
    self.hud = nil;
    [self.webVC.navigationController popViewControllerAnimated:YES];
}

#pragma mark -  记得要移除
-(void)cancelHandler
{
    [_configuration.userContentController removeScriptMessageHandlerForName:@"backPage"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"Share"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"returnNativeMethod"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"copyTextMethod"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"makePhoneCall"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"checkAppIsInstalled"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"fetchingAlipay"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"uploadPage"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"hidenTabBar"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"toNativeWebview"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"openBrowser"];
    [_configuration.userContentController removeScriptMessageHandlerForName:@"hideTitle"];
}
@end
