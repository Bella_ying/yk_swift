//
//  YKBaseWebViewController.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKBaseWebViewController.h"
#import "YKNavigationController.h"

@interface YKBaseWebViewController ()

@property (nonatomic, ) BOOL showSearch;

@end

@implementation YKBaseWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_itemBlock) {
        _itemBlock(self);
    }
    if (_navTitle) {
        self.navigationItem.title = _navTitle;
    }
}
-(void)updateNavigationItems
{
    if (self.isShowBackBtn) {
        if (self.isShowCloseBtn && self.webView.canGoBack) {
            self.navigationItem.leftBarButtonItems = @[self.backItem,self.closeItem];
        }else{
            self.navigationItem.leftBarButtonItems = nil;
            self.navigationItem.leftBarButtonItem = self.backItem;
        }
    }
    
    if (self.rightBarButtonItem) {
        self.navigationItem.rightBarButtonItem = self.rightBarButtonItem;
    } else if (self.rightBarButtonItems) {
        self.navigationItem.rightBarButtonItems = self.rightBarButtonItems;
    }
    self.navigationItem.rightBarButtonItem = self.showSearch ? self.searchRightItem : nil;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //umeng路径访问统计
    [YKClickManager yk_reportPageBeginWithName:self.url];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //umeng路径访问统计
    [YKClickManager yk_reportPageEndWithName:self.url];
}
#pragma mark - 返回关闭按钮初始化（懒加载）
- (UIBarButtonItem *)closeItem
{
    if (!_closeItem) {
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        btnClose.backgroundColor = [UIColor clearColor];
        [btnClose setImage:[UIImage imageNamed:@"webview_close"] forState:UIControlStateNormal];
        btnClose.imageEdgeInsets =UIEdgeInsetsMake(5, 10, 5, 10);
        btnClose.frame = CGRectMake(0, 0, 41, 31);
        [btnClose addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
        _closeItem = [[UIBarButtonItem alloc] initWithCustomView:btnClose];
    }
    return _closeItem;
}

- (void)closeAction:(id)sender
{
    if (self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (UIBarButtonItem *)backItem
{
    if (!_backItem) {
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        btnBack.backgroundColor = [UIColor clearColor];
        [btnBack setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, 25, 44);
        [btnBack addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        _backItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    }
    return _backItem;
}

- (UIBarButtonItem *)searchRightItem
{
    if (!_rightBarButtonItem) {
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.backgroundColor = [UIColor clearColor];
        [rightButton setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
        rightButton.frame = CGRectMake(0, 0, 25, 44);
        [rightButton addTarget:self action:@selector(search) forControlEvents:UIControlEventTouchUpInside];
        _rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    }
    return _rightBarButtonItem;
}

- (void)search
{
    self.url = [[ConfigManager config] getSearchUrl];
}

- (void)backAction:(UIButton *)button
{
    if (![self isKindOfClass:[YKBaseWebViewController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
   else{
        YKBaseWebViewController *webVC = (YKBaseWebViewController *)self;
        [webVC.webView stopLoading];
        if ([webVC.webView canGoBack]) {
            DLog(@"%@",webVC.webView.URL.absoluteString)
            if ([webVC.webView.URL.absoluteString containsString:@"pop_back"]) { //如果当前h5页面包含pop_back字段，则点击返回键时直接回到原生页面
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
            [webVC.webView goBack];
            //返回上一页面前判断是否应该显示
            BOOL showRight = [webVC.webView.backForwardList.backItem.URL.absoluteString containsString:@"isShowSearch"];
            self.navigationItem.rightBarButtonItem = showRight ? self.searchRightItem : nil;
        } else {
            if (self.popToDesignatedPage == PopToDesignatedPageMine) {
                [[YKTabBarController yk_shareTabController] yk_setSelectedIndex:YKTabSelectedIndexMine viewController:nil];
            } else if (self.popToDesignatedPage == PopToDesignatedPageMall) {
                [[YKTabBarController yk_shareTabController] yk_setSelectedIndex:YKTabSelectedIndexMall viewController:nil];
            } else if (self.popToDesignatedPage == PopToDesignatedPageMallComfirmOrder) {
                [[YKTabBarController yk_shareTabController] yk_popToViewController:@"YKConfirmOrderViewController"];
            } else if (self.popToDesignatedPage == PopToDesignatedPageVerifyCenter) {
                [[YKTabBarController yk_shareTabController] yk_popToViewController:@"YKVerifyCenterListVC"];
            } else {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

- (void)showSearchRightItem:(BOOL)show
{
    self.showSearch = show;
}

@end
