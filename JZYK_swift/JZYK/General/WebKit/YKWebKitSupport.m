//
//  YKWebKitSupport.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKWebKitSupport.h"

@interface YKWebKitSupport()
@property (nonatomic, strong, readwrite) WKProcessPool *processPool;
@end

@implementation YKWebKitSupport

+ (instancetype)sharedSupport
{
    static YKWebKitSupport *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [YKWebKitSupport new];
    });
    return  _instance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.processPool = [WKProcessPool new];
    }
    return self;
}

+ (NSString *)yk_fetchLoginUserSession
{
    NSString *cookieStr = @"";
    if ([YKUserManager sharedUser].yk_isLogin) {
        NSString *sessionID = [YKUserManager sharedUser].sessionId;
        cookieStr = [NSString stringWithFormat:@"%@=%@;",kSessionID,sessionID];
    }
    return cookieStr;
}

#pragma mark js注入
+ (void)yk_injectJSForWebview:(WKWebView *)webview
{
    [[YKUserManager sharedUser] yk_handleCookieForURLString:webview.URL.absoluteString];
    NSString *JSFuncString =
    @"function setCookie(cname,cvalue,exdays)\
    {\
    var d = new Date();\
    d.setTime(d.getTime()+(exdays*24*60*60*1000));\
    var expires = 'expires='+d.toGMTString();\
    document.cookie = cname + '=' + cvalue + '; ' + expires + ';path=/';\
    }";
    
    //拼接js字符串
    NSMutableString *JSCookieString = JSFuncString.mutableCopy;
    NSString *userSessionID = [[YKUserManager sharedUser] sessionId];
    
    if ([userSessionID yk_isValidString]) {
        NSString *excuteJSString = [NSString stringWithFormat:@"setCookie('SESSIONID', '%@', 12);", userSessionID];
        [JSCookieString appendString:excuteJSString];
    }
    if (JSFuncString.length && JSCookieString.length >= JSFuncString.length) {
        //执行js
        [webview evaluateJavaScript:JSCookieString completionHandler:nil];
    }
}

@end
