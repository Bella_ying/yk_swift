//
//  YKAlbumManager.m
//  JZYK
//
//  Created by Jeremy on 2018/7/23.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKAlbumManager.h"
#import "ELCImagePickerController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface YKAlbumManager()<ELCImagePickerControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic ,strong)NSData *imageData;
@property (nonatomic ,strong)NSData *caremaData;

@end

@implementation YKAlbumManager

+(instancetype)shareAlbumInstance
{
    static YKAlbumManager *manager = nil;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        manager = [[YKAlbumManager alloc]init];
    });
    return manager;
}

- (void)showPhotoAlbum
{
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] init];
    elcPicker.maximumImagesCount = 1 ;
    elcPicker.imagePickerDelegate = self;
    [[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] presentViewController:elcPicker animated:YES completion:nil];
}

- (void)showCamera
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        //        [self performSelector:@selector(showAlert:) withObject:@"相机打开失败" afterDelay:0.5f];
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf showAlert:@"相机打开失败"];
        });
        return;
    }
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == ALAuthorizationStatusRestricted || authStatus == ALAuthorizationStatusDenied) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showAlert:@"请在设置-->隐私-->相机-->人人快借中打开相机使用权限"];
        });
        return;
    }
    UIImagePickerController *imageVC = [[UIImagePickerController alloc] init];
    imageVC.allowsEditing = NO;
    imageVC.delegate = self;
    imageVC.sourceType = UIImagePickerControllerSourceTypeCamera;
    [[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] presentViewController:imageVC animated:YES completion:nil];
}

- (void)showAlert:(NSString *)alertText
{
    
    [[QLAlert alert] showWithTitle:nil message:alertText btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {
        
    }];
}

//相册代理
#pragma mark ELCImagePickerControllerDelegate
- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    
    [info enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* uploadImage=[dict objectForKey:UIImagePickerControllerOriginalImage];
                self.imageData = UIImageJPEGRepresentation(uploadImage, 0.5);
                [[HTTPManager session] uploadImageWithData:self.imageData
                                              serverUrlKey:kPictureUploadImg
                                                       key:@"attach"
                                                  fileName:@"imageFile.jpg"
                                                parameters:@{@"type":@13,@"object_id":self.objectId}
                                             uploadSuccess:^(NSDictionary<NSString *,id> * _Nonnull json) {
                                                 [[QLAlert alert] showWithTitle:nil message:json[@"message"] btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {
                                                     if (self.responseSuccessBlock) {
                                                         self.responseSuccessBlock ();
                                                     }
                                                     [[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] dismissViewControllerAnimated:YES completion:^{}];
                                                 }];
                                                 
                } uploadProgress:^(NSProgress * _Nonnull progress) {
                    
                } uploadFailure:^(NSString * _Nonnull errmsg) {
                    [[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] dismissViewControllerAnimated:YES completion:^{}];
                }];
            }
        }
    }];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        self.caremaData = UIImageJPEGRepresentation(image, 0.5);
        [[HTTPManager session] uploadImageWithData:self.caremaData
                                      serverUrlKey:kPictureUploadImg
                                               key:@"attach"
                                          fileName:@"imageFile.jpg"
                                        parameters:@{@"type":@13,@"object_id":self.objectId}
                                     uploadSuccess:^(NSDictionary<NSString *,id> * _Nonnull json) {
                                         [[QLAlert alert] showWithTitle:nil message:json[@"message"] btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {
                                             if (self.responseSuccessBlock) {
                                                 self.responseSuccessBlock ();
                                             }
                                             [[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] dismissViewControllerAnimated:YES completion:^{}];
                                         }];
                                         
                                     } uploadProgress:^(NSProgress * _Nonnull progress) {
                                         
                                     } uploadFailure:^(NSString * _Nonnull errmsg) {
                                         [[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] dismissViewControllerAnimated:YES completion:^{}];
                                     }];
    }
}

@end
