//
//  YKPushManager.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKPushManager.h"
#import "YKUserManager.h"
#import "YKJumpManager.h"
#import <JPush/JPUSHService.h>


// iOS10注册APNs所需头件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

static NSString * const kJpushChannel = @"App Store";

@interface YKPushManager() <JPUSHRegisterDelegate, UNUserNotificationCenterDelegate>
@property (nonatomic, getter=isrunning) BOOL running;
@end

@implementation YKPushManager

SingletonM(YKPushManager)

#pragma mark 初始化APNs 初始化Jpush
- (void)yk_initJpushServiceWithOptions:(NSDictionary *)launchOptions{
    //Required
    if (@available(iOS 10.0, *)) {
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    }
    //可以添加自定义categories
    [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                      UIUserNotificationTypeSound |
                                                      UIUserNotificationTypeAlert)
                                          categories:nil];
    
    //Required
    // 如需继续使用pushConfig.plist文件声明appKey等配置内容，请依旧使用[JPUSHService setupWithOption:launchOptions]方式初始化。
    
    //    * @param channel 发布渠道. 可选.
    //    * @param isProduction 是否生产环境. 如果为开发状态,设置为 NO; 如果为生产状态,应改为 YES.
    BOOL isProduction = YES;
#ifdef DEBUG
    isProduction = NO;
#else
    //    [JPUSHService setLogOFF];
#endif
    [JPUSHService setupWithOption:launchOptions
                           appKey:kJpushAppKey
                          channel:kJpushChannel
                 apsForProduction:isProduction];
}

//注册极光
- (void)yk_registerDeviceToken:(NSData *)token
{
    [JPUSHService registerDeviceToken:token];  //注册deviceToken
    [self yk_setPushAlias];
}

- (void)yk_setPushAlias
{
    if ([YKUserManager sharedUser].yk_isLogin) {
        [JPUSHService setAlias:NSString.yk_integerToString([YKUserManager sharedUser].uid)  completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
            if (iResCode == 0) {
                DLog(@"别名设置成功");
            }
        } seq:1];
    }
}

//收到推送消息
- (void)yk_handleRemoteNotification:(NSDictionary *)userInfo
{
    DLog(@"aps_alert+++++++++++%@",userInfo[@"aps"][@"alert"]);
    [JPUSHService handleRemoteNotification:userInfo];
    UIApplicationState appState = [[UIApplication sharedApplication] applicationState];
    if (appState == UIApplicationStateActive) {
        QLAlert *alert = [QLAlert alert];
        [alert prepareUIWithTitle:@"" contentText:userInfo[@"aps"][@"alert"]? :@"您有一条推送通知！" buttonTitleArray:@[@"查看",@"忽略"] buttonClicked:^(NSInteger index) {
            if (index == 0) {
                //点击跳转
                [[YKJumpManager shareYKJumpManager] yk_jumpWithParamDic:userInfo];
            }
        }];
        [alert show];
    }else{
        //跳转
        [[YKJumpManager shareYKJumpManager] yk_jumpWithParamDic:userInfo];
    }
}

//本地推送
- (void)yk_showLocalNotification:(UILocalNotification *)notification
{
    [JPUSHService showLocalNotificationAtFront:notification identifierKey:nil];
}


#pragma mark JPUSHRegisterDelegate
// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center
        willPresentNotification:(UNNotification *)notification
          withCompletionHandler:(void (^)(NSInteger))completionHandler API_AVAILABLE(ios(10.0)){
    UNNotificationRequest *request = notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]
        ]) {
        [JPUSHService handleRemoteNotification:userInfo];
        DLog(@"iOS10 前台收到远程通知:%@", [self logDic:userInfo]);
    }else{
        // 判断为本地通知
        DLog(@"iOS10 前台收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
        // 更新显示的徽章个数
    }
    NSInteger badgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber;
    badgeNumber--;
    badgeNumber = badgeNumber >= 0 ? badgeNumber : 0;
    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeNumber;
    completionHandler(UNNotificationPresentationOptionAlert |
                      UNNotificationPresentationOptionSound |
                      UNNotificationPresentationOptionBadge);
    // 需要执 这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center
 didReceiveNotificationResponse:(UNNotificationResponse *)response
          withCompletionHandler:(void(^)())completionHandler API_AVAILABLE(ios(10.0))
{
    // Required
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler(); // 系统要求执行这个方法
}

// log NSSet with UTF8
// if not ,log will be \Uxxx
- (NSString *)logDic:(NSDictionary *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str = [NSPropertyListSerialization propertyListWithData:tempData options:NSPropertyListImmutable format:NULL error:nil];
    return str;
}

- (void)yk_deletePushAlias
{
    [JPUSHService deleteAlias:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        if (iResCode == 0) {
            DLog(@"别名删除成功");
        }
    } seq:1];
}

@end
