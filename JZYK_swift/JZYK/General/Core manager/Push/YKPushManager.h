//
//  YKPushManager.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKPushManager : NSObject

@property (nonatomic, copy) NSString *messageID;

SingletonH(YKPushManager)

//注册极光SDK
- (void)yk_initJpushServiceWithOptions:(NSDictionary *)launchOptions;

//在极光注册tocken
- (void)yk_registerDeviceToken:(NSData *)tocken;

//收到推送消息
- (void)yk_handleRemoteNotification:(NSDictionary *)userInfo;

//本地推送
- (void)yk_showLocalNotification:(UILocalNotification *)notification;

//设置别名  uid
- (void)yk_setPushAlias;

//登出删除别名
- (void)yk_deletePushAlias;
@end
