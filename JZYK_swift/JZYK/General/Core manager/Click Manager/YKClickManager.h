//
//  YKClickManager.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//  埋点统计

#import <Foundation/Foundation.h>

@interface YKClickManager : NSObject

//通过默认参数的标识value传单个参数
+ (void)yk_reportWithKey:(NSString *)key
                   array:(NSArray *)valueArray;

//通过自定义参数的标识传多个参数
+ (void)yk_reportWithKey:(NSString *)key
           dictionary:(NSDictionary *)valueDic;

+ (void)yk_reportPageBeginWithName:(NSString *)name;

+ (void)yk_reportPageEndWithName:(NSString *)name;

@end
