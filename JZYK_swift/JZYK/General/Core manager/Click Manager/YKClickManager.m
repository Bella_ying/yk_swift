//
//  YKClickManager.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKClickManager.h"
#import <UMMobClick/MobClick.h>

@implementation YKClickManager

//通过默认参数的标识value传单个参数
+ (void)yk_reportWithKey:(NSString *)key
                   array:(NSArray *)valueArray
{
    if (valueArray.count > 0) {
        for (id obj in valueArray) {
            [MobClick event:key label:[NSString stringWithFormat:@"%@",obj]];
        }
    }
}

//通过自定义参数的标识传多个参数
//通过自定义参数的标识传多个参数
+ (void)yk_reportWithKey:(NSString *)key
              dictionary:(NSDictionary *)valueDic
{
    [MobClick event:key attributes:valueDic];
}

+ (void)yk_reportPageBeginWithName:(NSString *)name
{
    [MobClick beginLogPageView:name];//("PageOne"为页面名称，可自定义)
}

+ (void)yk_reportPageEndWithName:(NSString *)name
{
    [MobClick endLogPageView:name];
}

@end
