//
//  InterfaceReport.swift
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/13.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//  接口上报

import Foundation

@objcMembers class InterfaceReport: NSObject {
    @objc public static let defaultReport = InterfaceReport();
    
    /// 上报信息
    ///
    /// - Parameter report: 是否上报时间
    @objc public func reportInfo(timeStringReport report: Bool) {
        var uid = "";
        var userName = "";
        var deviceID = "";
        var netType = "";
        var installTime: String = ""
        if let dateSting = Date.currentTimeConvertToString() {
            installTime = report ? dateSting : ""
        }
        let appMarket = QBProject.yk_current().appMarket!;
        let adID = DeviceInfo.deviceID()
        
        if YKUserManager.sharedUser().uid != 0 {
            uid = "\(YKUserManager.sharedUser().uid)"
        }
        if !YKUserManager.sharedUser().username.isEmpty {
            userName = YKUserManager.sharedUser().username
        }
        deviceID = DeviceInfo.deviceID()
        
        if ReachabilityManager.defaultReachability.reachability?.connection != .none {
            netType = (ReachabilityManager.defaultReachability.reachability?.connection.description)!
            let params: Dictionary<String, Any> = [
                "IdentifierId"  :adID,
                "appMarket"     :appMarket,
                "device_id"     :deviceID,
                "installed_time":installTime,
                "uid"           :uid,
                "username"      :userName,
                "net_type"      :netType
            ];
            
            HTTPManager.session.postRequest(forKey: kReportKey, param: params, succeed: { (json, code, unwrapStatus, msg) in
                if code == 0 {
                    YKDebugger.debugLog {
                        print("上报信息成功")
                    }
                } else {
                    YKDebugger.debugLog {
                        print("上报信息失败")
                    }
                }
            }) { (errMsg, isConnect) in
                print(errMsg)
            };
        }
    }
    
    /// 分享成功之后上报信息
    ///
    /// - Parameters:
    ///   - current:  currentUrl
    ///   - source: sourceUrl
    ///   - platform: platformString
    @objc public func reportInfoAfterShare(currentUrl current: String,
                                           sourceUrl source: String,
                                           platformString platform: String) {
        guard let sourceTag = getSourceTag(fromShareUrl: current) else { return }
        let params: Dictionary<String, String> = [
            "source_url":source,
            "current_url":current,
            "source_tag":sourceTag,
            "action":platform,
            "user_agent": YKTools.yk_getGlobalUserAgent()
        ];
        shareSuccessRequest(info: params)
    }
    
    /// 获得sourceTag
    ///
    /// - Parameter url: share_url
    /// - Returns: sourceTag
    private func getSourceTag(fromShareUrl share_url: String?) ->String? {
        var sharePlatArray = [String]()
        var sourceTag = ""
        guard let shareUrl = share_url else {
            return nil;
        }
        
        if shareUrl.count > 0 {
            sharePlatArray = shareUrl.components(separatedBy: "?")
        }
        if sharePlatArray.count > 0 {
            let str = sharePlatArray[1]
            if (str.components(separatedBy: "&").count) > 0 {
                for sourceStr: String? in str.components(separatedBy: "&") {
                    guard let source = sourceStr else { return nil}
                    if source.contains("source_tag") {
                        if source.components(separatedBy: "=").count  > 0 {
                            sourceTag = source.components(separatedBy: "=")[1]
                        }
                    }
                }
            }
        }
        return sourceTag
    }
    
    /// 分享成功发起的请求
    ///
    /// - Parameter params: params
    private func shareSuccessRequest(info params: Dictionary<String, String>) {
        HTTPManager.session.getRequest(forKey: kSocialMediaSiteVisitStasticsURLKey, succeed: { (json, code, unwrapSuccess, msg) in
            if code == 0 {
                
            }else if code == -2 {  //登录态失效
                iToast.makeText(msg).show()
            }
        }) { (errMsg, isConnect) in
            
        }
    }
}
