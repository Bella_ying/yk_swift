//
//  ReachabilityManager.swift
//  ReachabilityDemo
//
//  Created by Jeremy Wang on 2018/6/13.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Reachability

@objc protocol NetworkChangedDelegate {
    func isConnect(_ connect: Bool);
}



@objcMembers class ReachabilityManager: NSObject {
    @objc public static let defaultReachability = ReachabilityManager()
    public var reachability: Reachability?
    let hostNames = ["jzyk-app-v1.dev.cs.jisuqianbao.com","jzyk.yuyaowangluo.com",kDebugDomainName]
    
    var isWatchOn = false
    
    public var updateInfoWhenReachable: ((Reachability)->Void)?
    public var updateInfoWhenNotReachable: ((Reachability, String)->Void)?
    public var unableToStart: ((String)->Void)?
    
    weak var delegate: NetworkChangedDelegate?
    
    @objc public func ennaleWatch() {
        YKDebugger.debugPrint({
            if self.hostNames.count > 0 {
                self.startHost(at: 0);
            }
        }) {
            if self.hostNames.count >= 1 {
                self.startHost(at: 1);
            }
        }
    }
    
    @objc public func disableWatch() {
        self.stopNotifier()
        
    }
    
    private func startHost(at index: Int) {
        stopNotifier()
        setupReachability(hostNames[index])
        startNotifier()
    }
    
    private func setupReachability(_ hostName: String?) {
        let reachability: Reachability?
        if let hostName = hostName {
            reachability = Reachability(hostname: hostName)
        } else {
            reachability = Reachability()
        }
        self.reachability = reachability
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reachabilityChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
    }
    
    private func startNotifier() {
        if !self.isWatchOn {

            YKDebugger.debugLog {
                print("--- start notifier")                
            };

            do {
                try reachability?.startNotifier()
                self.isWatchOn = true;
            } catch {
                self.unableToStart?("Unable to start\nnotifier")
                return
            }
        }
    }
    
    private func stopNotifier() {
        if self.isWatchOn {
            YKDebugger.debugLog {
                print("--- stop notifier")
            }
            reachability?.stopNotifier()
            NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
            reachability = nil
            self.isWatchOn = false;
        }
    }
    
    @objc private func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        if reachability.connection != .none {
            self.updateInfoWhenReachable?(reachability)
            self.delegate?.isConnect(true)
        } else {
            self.updateInfoWhenNotReachable?(reachability, "\(reachability.connection)")
            self.delegate?.isConnect(false)
        }
    }
    
    deinit {
        stopNotifier()
    }
}
