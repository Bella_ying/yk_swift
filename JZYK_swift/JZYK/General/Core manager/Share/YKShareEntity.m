//
//  YKShareEntity.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKShareEntity.h"

@implementation YKShareEntity

- (void)setShare_logo:(NSString *)share_logo
{
    if (![share_logo isEqualToString:@""]) {
        _share_logo = share_logo;
    } else {
        _share_logo = @"share";
    }
}

- (void)setShareBtnTitle:(NSString *)shareBtnTitle
{
    if ([shareBtnTitle isEqualToString:@""]||!shareBtnTitle) {
        _shareBtnTitle = @"分享";
    }
}

@end
