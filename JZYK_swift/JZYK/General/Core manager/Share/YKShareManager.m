//
//  YKShareManager.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKShareManager.h"
#import "YKShareEntity.h"
#import <UShareUI/UShareUI.h>


@interface YKShareManager()
//分享的数据
@property (nonatomic, strong) YKShareEntity *entity;
@end

@implementation YKShareManager

+ (instancetype)shareManager
{
    static dispatch_once_t once;
    static YKShareManager *shareManager = nil;
    dispatch_once(&once, ^{
        shareManager = [[YKShareManager alloc]init];
    });
    return shareManager;
}



- (void)yk_showWithShareEntity:(YKShareEntity *)entity
{
    self.entity = entity;
    //分享渠道处理
    NSArray *sharePlatArray;
    NSMutableArray *sharePlatFormArray = [@[] mutableCopy];
    if (entity.platform.length >0) {
        sharePlatArray = [entity.platform componentsSeparatedByString:@","];
    }
    
    if (sharePlatArray.count > 0) {
        
        [sharePlatArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            //添加微信渠道
            if ([obj isEqualToString:@"WEIXIN"]) {
                [sharePlatFormArray addObject:@(UMSocialPlatformType_WechatSession)];
            }
            //添加朋友圈渠道
            if ([obj isEqualToString:@"WEIXIN_CIRCLE"]) {
                [sharePlatFormArray addObject:@(UMSocialPlatformType_WechatTimeLine)];
            }
            //添加qq渠道
            if ([obj isEqualToString:@"QQ"]) {
                [sharePlatFormArray addObject:@(UMSocialPlatformType_QQ)];
            }
            //添加qq空间渠道
            if ([obj isEqualToString:@"QZONE"]) {
                [sharePlatFormArray addObject:@(UMSocialPlatformType_Qzone)];
            }
            //添加短信渠道
            if ([obj isEqualToString:@"SMS"]) {
                [sharePlatFormArray addObject:@(UMSocialPlatformType_Sms)];
            }
            //添加邀请渠道
            if ([obj isEqualToString:@"SMS_INVITE"]) {
                
            }
        }];
    }
    if (sharePlatFormArray.count == 0) {
        [sharePlatFormArray addObjectsFromArray:@[@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine),@(UMSocialPlatformType_QQ),@(UMSocialPlatformType_Qzone)]];
    }
    
    //设置用户自定义的平台
    [UMSocialUIManager removeAllCustomPlatformWithoutFilted];
    [UMSocialUIManager setPreDefinePlatforms:sharePlatFormArray];
    
    [UMSocialShareUIConfig shareInstance].sharePageGroupViewConfig.sharePageGroupViewPostionType =  UMSocialSharePageGroupViewPositionType_Bottom;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageItemStyleType = UMSocialPlatformItemViewBackgroudType_None;
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        //根据分享数据类型分享数据
        if ([entity.share_data_type integerValue] == 1) {
            [self shareImageToPlatformType:platformType entity:entity];
        } else {
            [self shareWebPageToPlatformType:platformType entity:entity];
        }
    }];
}
#pragma mark - 单个条用分享
-(void)yk_singleShare:(YKShareEntity *)entity
{
    self.entity = entity;
    UMSocialPlatformType type = UMSocialPlatformType_WechatSession;
    if ([entity.platform isEqualToString:@"WEIXIN"]) {
        type = UMSocialPlatformType_WechatSession;
    }else if ([entity.platform isEqualToString:@"WEIXIN_CIRCLE"])
    {
        type = UMSocialPlatformType_WechatTimeLine;
    }else if ([entity.platform isEqualToString:@"QQ"])
    {
        type = UMSocialPlatformType_QQ;
    }else if ([entity.platform isEqualToString:@"QZONE"])
    {
        type = UMSocialPlatformType_Qzone;
    }else if ([entity.platform isEqualToString:@"SMS"])
    {
        type = UMSocialPlatformType_Sms;
    }else if ([entity.platform isEqualToString:@"SMS_INVITE"])
    {
        [self smsInvite];
        return;
    }
    //根据分享数据类型分享数据
    if ([entity.share_data_type integerValue] == 1) {
        [self shareImageToPlatformType:type entity:entity];
    } else {
        [self shareWebPageToPlatformType:type entity:entity];
    }
}

- (NSString*)SharingPlatform:(UMSocialPlatformType)type
{
    NSString * typeStr ;
    
    switch (type) {
        case UMSocialPlatformType_WechatSession:
        {
            typeStr = @"WEIXIN" ;
        }
            break;
        case UMSocialPlatformType_WechatTimeLine:
        {
            typeStr = @"WEIXIN_CIRCLE" ;
        }
            break;
        case UMSocialPlatformType_QQ:
        {
            typeStr = @"QQ" ;
        }
            break;
        case UMSocialPlatformType_Qzone:
        {
            typeStr = @"QZONE" ;
        }
            break;
        case UMSocialPlatformType_Sms:
        {
            typeStr = @"SMS" ;
        }
            break;
        case UMSocialPlatformType_Sina:
        {
            typeStr = @"SINA" ;
        }
            break;
        default:
            break;
    }
    
    return typeStr;
}

//网页分享
- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
                            entity:(YKShareEntity *)entity
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:entity.share_title descr:entity.share_body thumImage:[entity.share_logo yk_isValidString] ? entity.share_logo : [UIImage imageNamed:@"share"]];
    //设置网页地址
    shareObject.webpageUrl = entity.share_url;
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    WEAK_SELF
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] completion:^(id data, NSError *error) {
        STRONG_SELF
        if (error) {
            DLog(@"************Share fail with error %@*********",error.userInfo[@"message"]);
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
                if (strongSelf.shareSuccess_blk) {
                    DLog(@"====%ld",platformType);
                    NSString *platformTypeStr = [strongSelf SharingPlatform:platformType];
                    strongSelf.shareSuccess_blk(entity.platform,platformTypeStr);
                }
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}


//分享图片
- (void)shareImageToPlatformType:(UMSocialPlatformType)platformType
                          entity:(YKShareEntity *)entity
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建图片内容对象
    UMShareImageObject *shareObject = [[UMShareImageObject alloc] init];
    //如果有缩略图，则设置缩略图本地
    shareObject.thumbImage = entity.share_image;
    
    [shareObject setShareImage:[UIImage imageNamed:@"share"]];
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    WEAK_SELF
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:[[YKTabBarController yk_shareTabController] yk_getCurrentViewController]  completion:^(id data, NSError *error) {
        STRONG_SELF
        
        if (error) {
            DLog(@"************Share fail with error %@*********",error.userInfo[@"message"]);
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
                if (strongSelf.shareSuccess_blk) {
                    DLog(@"====%ld",platformType);
                    NSString *platformTypeStr = [strongSelf SharingPlatform:platformType];
                    strongSelf.shareSuccess_blk(entity.platform,platformTypeStr);
                }
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

#pragma mark - 我的邀请
- (void)smsInvite
{
//    if ([[KDGetAddressBook shareControl]getPersonInfo]&&[[KDGetAddressBook shareControl]sortMethod]) {
//        [[KDTabBarController shareTabController]ql_pushToViewController:[KDSMSViewController new]];
//    } else {
//        //请检查是否在设置－隐私－通讯录授权给我们的应用或您的通讯录没有联系人!
//        //确定
//        [[QLAlert alert] showWithTitle:@"" message:kAddressAleartTip btnTitleArray:@[kAleartConfirm] btnClicked:^(NSInteger index) {
//
//        }];
//    }
}

@end
