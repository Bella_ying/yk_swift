//
//  YKShareManager.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YKShareEntity;

@interface YKShareManager : NSObject

@property (nonatomic ,copy) void(^shareSuccess_blk)(NSString *platform, NSString *platformTypeStr);

/**
 分享单例

 @return 返回实例
 */
+ (instancetype)shareManager;

/**
 *  分享API
 *
 *  @param entity 分享参数
 */
- (void)yk_showWithShareEntity:(YKShareEntity *)entity;

/**
 单独调起一个分享平台
 @param entity 分享参数
 */
- (void)yk_singleShare:(YKShareEntity *)entity;

@end
