//
//  YKShareEntity.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKShareEntity : NSObject
//0直接弹分享选择框 1右上角出来分享按钮 3只分享图片
@property (nonatomic, copy) NSString *type;

//0：默认分享方式  1：分享大图
@property (nonatomic, copy) NSString *share_data_type;

//分享title
@property (nonatomic, copy) NSString *share_title;
//分享描述
@property (nonatomic, copy) NSString *share_body;
//分享链接
@property (nonatomic, copy) NSString *share_url;
//分享图片
@property (nonatomic, copy) NSString *share_logo;
//单纯的只分享图片
@property (nonatomic, copy) NSData *share_image;

//按钮文案
@property (nonatomic, copy) NSString *shareBtnTitle;
//是否分享
@property (nonatomic, copy) NSString *isShare;
//分享有奖描述
@property (nonatomic, copy) NSString *sharePageTitle;

//分享渠道['wx','wechatf','qq','qqzone','sina','sms'];
@property (nonatomic, copy) NSString *platform;
//分享调起方式
@property (nonatomic ,copy) NSString *params;
//是否上报
@property (nonatomic, copy) NSString *shareIsUp;
//上报id
@property (nonatomic, copy) NSString *shareUpId;
//上报类型
@property (nonatomic, copy) NSString *shareUpType;
//上报url
@property (nonatomic, copy) NSString *shareUpUrl;
//js上报方法
@property (nonatomic, copy) NSString *callback;

//分享成功后事件
@property (nonatomic, copy) void_block_t block;

@end
