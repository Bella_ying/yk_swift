//
//  YKLocationManager.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKLocationManager.h"

/*
 定位节点
 启动
 借款
 个人信息
 */


static NSInteger const kDefaultLocationTimeout = 10;
static NSInteger const kDefaultReGeocodeTimeout = 5;

@interface YKLocationManager()<AMapLocationManagerDelegate>

@property (nonatomic, strong) AMapLocationManager *locationManager;
@property (nonatomic, ) BOOL amapActivate;

@end

@implementation YKLocationManager

static YKLocationManager *manager = nil;

+ (instancetype)location
{
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        manager = [YKLocationManager new];
        [manager initLocationServiceConfiguration];
    });
    return manager;
}

+ (void)yk_registAmapServices
{
    [AMapServices sharedServices].apiKey = (NSString *)kAmapAPIKey;
}

- (void)initLocationServiceConfiguration
{
    self.amapActivate = YES;
    self.locationManager = [[AMapLocationManager alloc] init];
    
    [self.locationManager setDelegate:self];
    
    //设置期望定位精度
    /*
     高德提供了 kCLLocationAccuracyBest 参数，设置该参数可以获取到精度在10m左右的定位结果，
     但是相应的需要付出比较长的时间（10s左右），越高的精度需要持续定位时间越长。
     推荐：kCLLocationAccuracyHundredMeters，一次还不错的定位，偏差在百米左右，
     超时时间设置在2s-3s左右即可。
     */
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    
    //设置不允许系统暂停定位
    [self.locationManager setPausesLocationUpdatesAutomatically:NO];
    
    //设置允许在后台定位
    [self.locationManager setAllowsBackgroundLocationUpdates:YES];
    
    //设置定位超时时间
    [self.locationManager setLocationTimeout:kDefaultLocationTimeout];
    
    //设置逆地理超时时间
    [self.locationManager setReGeocodeTimeout:kDefaultReGeocodeTimeout];
}

- (void)yk_shutDownLocationService
{
    self.amapActivate = NO;
    //停止定位
    [self.locationManager stopUpdatingLocation];
    
    [self.locationManager setDelegate:nil];
    DLog(@"%s",__func__);
}

- (void)yk_enableLocationServiceWithCompletion:(void (^ __nullable)(CLLocation *location, AMapLocationReGeocode *regeoCode))completion
{
    WEAK_SELF
    //进行单次带逆地理定位请求
    [self.locationManager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        if (error != nil && error.code == AMapLocationErrorLocateFailed)
        {
            //定位错误：此时location和regeocode没有返回值，不进行annotation的添加
            DLog(@"定位错误:{%ld - %@};", (long)error.code, error.localizedDescription);
            return;
        }
        else if (error != nil
                 && (error.code == AMapLocationErrorReGeocodeFailed
                     || error.code == AMapLocationErrorTimeOut
                     || error.code == AMapLocationErrorCannotFindHost
                     || error.code == AMapLocationErrorBadURL
                     || error.code == AMapLocationErrorNotConnectedToInternet
                     || error.code == AMapLocationErrorCannotConnectToHost))
        {
            //逆地理错误：在带逆地理的单次定位中，逆地理过程可能发生错误，此时location有返回值，regeocode无返回值，进行annotation的添加
            DLog(@"逆地理错误:{%ld - %@};", (long)error.code, error.localizedDescription);
        }
        else if (error != nil && error.code == AMapLocationErrorRiskOfFakeLocation)
        {
            //存在虚拟定位的风险：此时location和regeocode没有返回值，不进行annotation的添加
            DLog(@"存在虚拟定位的风险:{%ld - %@};", (long)error.code, error.localizedDescription);
            return;
        }
        else
        {
            //没有错误：location有返回值，regeocode是否有返回值取决于是否进行逆地理操作，进行annotation的添加
        }
        if (regeocode)
        {
            DLog(@"location ===> lat:%f;lon:%f \n accuracy:%.2fm", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
            DLog(@"regeocode ===> %@ \n %@-%@-%.2fm", regeocode.formattedAddress,regeocode.citycode, regeocode.adcode, location.horizontalAccuracy);
            
            //上报位置信息
            [weakSelf upLoadLocationWith:regeocode location:location];
            
            if (completion) {
                completion(location, regeocode);
            }
        }
    }];
}

#pragma mark- 上报位置信息
- (void)upLoadLocationWith:(AMapLocationReGeocode *)code location:(CLLocation *)location
{
    NSString *locationStr = [NSString stringWithFormat:@"%@/%@/%@",code.province,code.city,code.district];
    [[HTTPManager session] postRequestForKey:KUserLocation
                                 showLoading:NO param:@{
                                                        @"longitude":[NSNumber numberWithDouble:location.coordinate.longitude],
                                                        @"latitude":[NSNumber numberWithDouble:location.coordinate.latitude],
                                                        @"address": code.formattedAddress ? code.formattedAddress : @"",
                                                        @"mallAddress": locationStr ? locationStr :@"",
                                                        @"time":[NSNumber numberWithInteger:[[NSDate date] timeIntervalSince1970]]
                                                        }
                                     succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString * _Nonnull msg) {
                                         if (code == 0) {
                                             DLog(@"%@", msg);
                                         }
                                         else if (code == -2) {
                                             [self yk_shutDownLocationService];
                                             return;
                                         }
                                     } failure:^(NSString * _Nonnull msg, BOOL isConnection) {
                                         
                                     }];
}

- (void)dealloc
{
    [self yk_shutDownLocationService];
}

@end
