//
//  YKLocationManager.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>

@interface YKLocationManager : NSObject

@property (nonatomic, readonly) BOOL amapActivate;
/**
 YKLocationManager 的伪单例，生成实例需要用次方法

 @return YKLocationManager的实例
 */
+ (instancetype)location;

/**
 关闭定位服务
 */
- (void)yk_shutDownLocationService;

/**
 开启定位服务
 */
- (void)yk_enableLocationServiceWithCompletion:(void (^ __nullable)(CLLocation *location, AMapLocationReGeocode *regeoCode))completion;

/**
 注册高德地图服务
 */
+ (void)yk_registAmapServices;

@end
