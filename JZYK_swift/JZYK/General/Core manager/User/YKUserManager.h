//
//  YKUserManager.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YKUserModel;

@interface YKUserManager : NSObject
//sessionID
@property (nonatomic, copy) NSString  *sessionId;
//用户ID
@property (nonatomic, assign) NSInteger uid;
//用户名
@property (nonatomic, copy) NSString *username;
//实名
@property (nonatomic, copy) NSString *realname;
//交易密码状态
@property (nonatomic, assign) BOOL real_pay_pwd_status;
//性别
@property (nonatomic, copy) NSString *sex;
//存cookie的host
@property (nonatomic, strong) NSMutableArray *cookieHostArray;

//用户的定位信息
@property (nonatomic, copy) NSString *locationStr;


//登录成功个回调
@property (nonatomic, copy) void(^userLoginSuccess_blk)(void);


+ (YKUserManager *)sharedUser;

/**
 登录成功更新用户信息
 @param userModel 数据
 */
- (void)yk_loginSuccessToUpdateUserInfo:(YKUserModel *)userModel;

/**
 清除cookie的影响信息
 */
- (void)yk_clearCookieInfluenceInfo;

// 判断是否登录
- (BOOL)yk_isLogin;

//处理cookie
- (void)yk_handleCookieForURLString:(NSString *)url;

//退出登录
- (void)yk_logoutAndDeleteUserInfo;

//更新实名认证信息
- (void)yk_updateRealName:(NSString *)name;

//更新用户名
- (void)yk_updateUserName:(NSString *)name;
//更新性别信息
- (void)yk_updateSex:(NSString *)sex;

//更新交易密码信息
- (void)yk_updatePayPassWordStatus:(BOOL)status;

//清空登录信息(在code为-2的时候，清空信息)
- (void)yk_clearLoginStatusInfo;

//通过拉取一个接口来获取登录态是否已经失效
- (void)yk_checkLoginStatus:(void(^)(BOOL sucOrFail))status;

- (void)yk_loginOut;

@end
