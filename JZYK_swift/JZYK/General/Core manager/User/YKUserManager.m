//
//  YKUserManager.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKUserManager.h"
#import "YKUserModel.h"
#import "YKHomeViewController.h"

@interface YKUserManager()

@property (nonatomic, strong) YKUserModel *userModel;

@end

static YKUserManager *_sharedUser = nil;
@implementation YKUserManager

+ (YKUserManager *)sharedUser
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedUser = [[[self class] alloc] init];
        [_sharedUser initUser];
    });
    return _sharedUser;
}

- (void)initUser
{
    if ([[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:kUserLoginCacheKey]) {
        self.userModel = [[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:kUserLoginCacheKey];
        [self initUserManager:self.userModel];
    }else{
        self.uid = 0;
        self.sessionId = @"";
        self.username = @"";
        self.realname = @"";
        self.real_pay_pwd_status = 0;
        self.sex = @"";
    }
}

- (void)initUserManager:(YKUserModel *)userModel
{
    self.uid = [userModel.uid integerValue];
    self.sessionId = userModel.sessionid;
    self.username = userModel.username;
    self.realname = userModel.realname;
    self.real_pay_pwd_status = userModel.real_pay_pwd_status;
    self.sex = userModel.sex;
}

- (void)yk_loginSuccessToUpdateUserInfo:(YKUserModel *)userModel
{
    [[YKCacheManager sharedCacheManager] yk_cacheObject:userModel forKey:kUserLoginCacheKey];
    [self initUser];
    [[YKPushManager shareYKPushManager] yk_setPushAlias];
    //上报设备信息
    [[InterfaceReport defaultReport] reportInfoWithTimeStringReport:NO];
    if (self.userLoginSuccess_blk) {
        self.userLoginSuccess_blk();
    }
    //登录成功上报百融
    [[BrAgentManager yk_sharedManager] yk_initBrAgentWithType:BRLOGIN];
}

- (void)yk_clearCookieInfluenceInfo
{
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    for (NSHTTPCookie *cookie in cookies) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
}

- (BOOL)yk_isLogin
{
    YKUserModel *user = [[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:kUserLoginCacheKey];
    return user.uid != 0 && ![user.sessionid isEqualToString:@""] && user.sessionid;;
}

//处理cookie
- (void)yk_handleCookieForURLString:(NSString *)url
{
    //失去登录态则删除cookie
    if (!self.yk_isLogin) {
        //        [self clearCookieInfluenceInfo];
        return;
    }
    //有登陆则写入Cookie
    NSString *sessionHost = [NSString yk_sessionHostForURL:url];
    if ([sessionHost yk_isValidString]) {
        [self setDomainForHost:sessionHost];
    }
    [_cookieHostArray enumerateObjectsUsingBlock:^(NSString *host, NSUInteger idx, BOOL * _Nonnull stop) {
        NSHTTPCookie *sessionCookie = [NSHTTPCookie cookieWithProperties:@{NSHTTPCookieValue : self.sessionId, NSHTTPCookieName : kSessionID, NSHTTPCookiePath : @"/", NSHTTPCookieDomain : host}];
        DLog(@"usermanager======%@",sessionCookie);
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:sessionCookie];
    }];
    DLog(@"====%@",_cookieHostArray);
}

- (void)setDomainForHost:(NSString *)host
{
    __block BOOL have = NO;
    [_cookieHostArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isEqualToString:host]) {
            have = YES;
            *stop = YES;
        }
    }];
    if (!have) {
        if (!_cookieHostArray) {
            _cookieHostArray = [@[kDebugDomainName] mutableCopy];
        }
        [_cookieHostArray addObject:host];
    }
    //默认
    NSArray *array = [[ConfigManager config] configForKey:@"shareCookieDomain"];
    if (array && [array isKindOfClass:[NSArray class]]) {
        [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (![self.cookieHostArray containsObject:obj]) {
                [self.cookieHostArray addObject:obj];
            }
        }];
    }
}

//退出登录
- (void)yk_logoutAndDeleteUserInfo
{
    [_cookieHostArray removeAllObjects];
    [_cookieHostArray addObject:kDebugDomainName];
    
    if ([[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:kUserLoginCacheKey]) {
        YKUserModel *userModel = [[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:kUserLoginCacheKey];
        userModel.sessionid = @"";
        userModel.uid = @"0";
        userModel.realname = @"";
        //        model.username = self.username;
        userModel.real_pay_pwd_status =  0;
        userModel.sex = @"";
        [[YKCacheManager sharedCacheManager] yk_cacheObject:userModel forKey:kUserLoginCacheKey];
        [self initUser];
    }
    
    //删除WKWebview缓存
    [YKTools yk_deleteTheWKWebviewCache:YES];
    //退出登录时，推送去除别名
    [[YKPushManager shareYKPushManager] yk_deletePushAlias];
    //再次清扫cookie
    [self yk_clearCookieInfluenceInfo];
}

//更新实名认证信息
- (void)yk_updateRealName:(NSString *)name
{
    self.realname = name;
    self.userModel.realname = name;
    [[YKCacheManager sharedCacheManager] yk_cacheObject:self.userModel forKey:kUserLoginCacheKey];
}

//更新用户名
- (void)yk_updateUserName:(NSString *)name
{
    self.username = name;
    self.userModel.username = name;
    [[YKCacheManager sharedCacheManager] yk_cacheObject:self.userModel forKey:kUserLoginCacheKey];
}

//更新性别信息
- (void)yk_updateSex:(NSString *)sex
{
    self.sex = sex;
    self.userModel.sex = sex;
    [[YKCacheManager sharedCacheManager] yk_cacheObject:self.userModel forKey:kUserLoginCacheKey];
}

//更新交易密码信息
- (void)yk_updatePayPassWordStatus:(BOOL)status
{
    self.real_pay_pwd_status = status;
    self.userModel.real_pay_pwd_status = status;
    [[YKCacheManager sharedCacheManager] yk_cacheObject:self.userModel forKey:kUserLoginCacheKey];
}

//清空登录信息(在code为-2的时候，清空信息)
- (void)yk_clearLoginStatusInfo
{

    [self yk_logoutAndDeleteUserInfo];
}

//通过拉取一个接口来获取登录态是否已经失效
- (void)yk_checkLoginStatus:(void(^)(BOOL sucOrFail))status
{
    //后台已解除单一登录态，现在采取此逻辑判断登录态问题
    BOOL isLogined = self.yk_isLogin ;
    if (status) {
        status(isLogined);
    }
    if (!isLogined) {
        [self yk_clearLoginStatusInfo];
        //确定
        //您的登录态已失效，请重新登录
        [[QLAlert alert] showWithTitle:nil message:@"登录态失效" btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {
            [YKPageManager yk_presentLoginPageAndReturn];
        }];
    }
}



- (void)yk_loginOut
{
    [YK_NSUSER_DEFAULT removeObjectForKey:@"SHOWVERIFYSTUDENT"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserLogOutNotiKey object:nil];
    //    [[KDRedPointManager shareRedPointInstance] removeCacheInRedPoint];
    [self yk_logoutAndDeleteUserInfo];
    [YKPageManager yk_logoutToHome];
}

@end
