//
//  YKUserModel.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YKBaseModel.h"

@interface YKUserModel : YKBaseModel

@property (nonatomic, copy) NSString *sessionid;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *username;    //用户名（手机号）
@property (nonatomic, copy) NSString *realname;    //目前都是空
@property (nonatomic, copy) NSString *sex;         //性别
@property (nonatomic, copy) NSString *regtime;     //注册时间
@property (nonatomic, copy) NSString *special;     //默认为0  没有进行联系人认证并且申请了订单 则为1
@property (nonatomic, ) BOOL real_pay_pwd_status;  //是否设置交易密码
@property (nonatomic, ) BOOL real_verify_status;   // 0 未身份认证，1身份认证
@property (nonatomic, ) BOOL real_contact_status;  //是否进行了联系人认证, 0:没有 1：有

@property (nonatomic, copy) NSString *locationStr;    //用户 定位信息

@end


