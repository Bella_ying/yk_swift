//
//  YKUserModel.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKUserModel.h"

@implementation YKUserModel
//item
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"real_contact_status":@"item.real_contact_status",
             @"real_verify_status":@"item.real_verify_status",
             @"real_pay_pwd_status":@"item.real_pay_pwd_status",
             @"realname":@"item.realname",
             @"sessionid": @"item.sessionid",
             @"special": @"item.special",
             @"uid":@"item.uid",
             @"username":@"item.username"
             };
}

@end
