//
//  YKPageManager.h
//  JZYK
//
//  Created by Jeremy on 2018/6/14.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//  页面跳转的集中管理

#import <Foundation/Foundation.h>

@interface YKPageManager : NSObject

+ (void)yk_presentLoginPageAndReturn;

+ (void)yk_logoutToHome;

@end
