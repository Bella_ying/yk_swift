//
//  YKPageManager.m
//  JZYK
//
//  Created by Jeremy on 2018/6/14.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKPageManager.h"

@implementation YKPageManager

+ (void)yk_presentLoginPageAndReturn
{
    YKTabBarController *tabVC = [YKTabBarController yk_shareTabController];
    YKBaseViewController *controller = (YKBaseViewController *)[tabVC yk_getCurrentViewController];
    [controller.navigationController popToRootViewControllerAnimated:NO];
    [tabVC yk_jumpTabBarIndex:YKTabSelectedIndexHome];
    [controller logIn];
    return;
}

+ (void)yk_logoutToHome
{
    YKTabBarController *tabVC = [YKTabBarController yk_shareTabController];
    YKBaseViewController *controller = (YKBaseViewController *)[tabVC yk_getCurrentViewController];
    [controller.navigationController popToRootViewControllerAnimated:NO];
    [tabVC yk_jumpTabBarIndex:YKTabSelectedIndexHome];
}

@end
