//
//  ConfigManager.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Dispatch
import Kingfisher

enum ConfigInfoType: Int {
    case production = 3   //正式环境
    case dev = 4          //测试环境
}

@objcMembers class ConfigManager: NSObject {
    #if DEBUG
      var configInfoType: ConfigInfoType = .dev
    #else
      var configInfoType: ConfigInfoType = .production
    #endif
    
    var getConfigSuccess: (()->Void)?
    private var configDic: Dictionary<String, Any>?
    private var urlDic: Dictionary<String, Any>?
    
    @objc public static let config = ConfigManager()

    @objc public func getQiyuTemplateID(forKey name: String) -> Int {
        guard let configDic = self.configDic else { return 0 }
        guard let dic = configDic["qiyu_template"] as? Dictionary<String, Any> else { return 0 }
        var ID = 0;
        dic.forEach { (key, value) in
            if name == key {
                if let temp = value as? Int {
                    ID = temp;
                }
            }
        }
        
        return ID;
    }
    
    @objc public var registProtocolText: String {
        guard let configDic = self.configDic else { return "" }
        guard let registProtocolText = configDic["register_protocol_url"] as? String else { return "" }
        return registProtocolText
    }
    
    @objc public var privateProtocolText: String {
        guard let configDic = self.configDic else { return "" }
        guard let privateProtocolText = configDic["private_url"] as? String else { return "" }
        return privateProtocolText
    }
    
    private func fullFillPath(path: String) ->String {
        let docPath = (NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as NSString) as NSString
        let filePath = docPath.appendingPathComponent(path)
        return filePath
    }
    
    //通过URL获取配置
    @objc public func getConfigWithURL(_ url: String?) {
        /**
         下发配置文件每次只会拉取一次
         在拉下发配置文件的时候先用本地的配置文件初始化一下本地的变量
         */
        let fileName = "config_0\(configInfoType.rawValue).plist"
        let fileManager = FileManager.default
        
        ////有从网络上拉取的下发配置文件
        if fileManager.fileExists(atPath: fullFillPath(path: fileName)) {
            let dict =  NSDictionary(contentsOfFile: fullFillPath(path: fileName))
            if let dict = dict { //文件正常
                self.configDic = dict["item"] as? Dictionary<String, Any>
                self.urlDic = self.configDic?["dataUrl"] as? Dictionary<String, Any>
            } else { //文件异常（比如上次写失败了）取本地文件
                try? fileManager.removeItem(atPath: fullFillPath(path: fileName))
                let dic = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "config", ofType: "plist")!)
                if let dic = dic {
                    self.configDic = dic["item"] as? Dictionary<String, Any>
                    self.urlDic = self.configDic?["dataUrl"] as? Dictionary<String, Any>
                }
            }
        } else {//没有从网络上拉取缓存到本地的下发配置文件，则用本地默认的配置文件
            let localDict = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "config", ofType: "plist")!)
            if let localDict = localDict {
                self.configDic = localDict["item"] as? Dictionary<String, Any>
                self.urlDic = self.configDic?["dataUrl"] as? Dictionary<String, Any>
            }
        }
        if let url = url {
            fetchConfigData(forURL: url, saveAs: fileName)
        }
    }
    
    @objc public func getURLWithKey(_ key: String) ->String? {
        return urlDic![key] as? String
    }
    
    @objc public func configForKey(_ key: String) -> Any {
        return configDic![key] ?? ""
    }
    
    @objc public func getRandomRefreshDesc() -> String {
        var defaultString = "信用让生活更美好"
        if let configDic = configDic {
            guard let refreshText = configDic["refresh_text"] else { return defaultString }
            if refreshText is Array<String> {
                let texts = refreshText as! Array<String>
                if texts.count > 0 {
                    defaultString = texts[Int(arc4random()) % texts.count]
                }
            }
        }
        return defaultString
    }
    
    @objc public func getSearchUrl() -> String {
        guard let configDic = self.configDic else { return "" }
        guard let searchUrl = configDic["h5_help_search"] as? String else { return "" }
        return searchUrl;
    }
}

//MARK: private methods
private extension ConfigManager {
    func fetchConfigData(forURL url: String, saveAs fileName: String) {
        HTTPManager.session.getRequest(forUrl: url, param: ["configVersion":DeviceInfo.appVersion()], succeed: { (json, code, unwrapNormal, msg) in
            if code == 0 {
                let item = json["item"] as? Dictionary<String, Any>
                self.configDic = item
                guard let itemDict = item else { return }
                let urlDic = itemDict["dataUrl"] as? Dictionary<String, Any>
                self.urlDic = urlDic
                // 写缓存文件
                let data =  json as NSDictionary
                let result = data.write(toFile: self.fullFillPath(path: fileName), atomically: true)
                if result {
                    YKDebugger.debugLog {
                        print("file write successful")                        
                    }
                }
                DispatchQueue.main.async {
                    self.getConfigSuccess?()
                }
                let imgPreLoad = self.configDic?["imgPreload"]//可能没有意义，因为imgPreload可能为空
                if let imgPreload = imgPreLoad, imgPreload is Array<String> {
                    let images = imgPreLoad as! Array<String>
                    if images.isEmpty { return }
                    images.forEach({ (url) in
                        // 预下载图片
                        let resource = ImageResource(downloadURL: URL(string: url)!, cacheKey: "text")
                        let retriveImageTask = KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil, completionHandler: {
                            (image, error, cacheType, imageURL) in
                            if error == nil {
                                YKDebugger.debugLog {
                                    print("获取图片成功")
                                }
                            } else {
                                YKDebugger.debugLog {
                                    print("获取图片失败")
                                }
                            }
                        })
                        retriveImageTask.cancel()
                    })
                }
            } else if code == -2 {
                DispatchQueue.main.async {
                    self.getConfigSuccess?()
                }
            }
        }) { (errMsg, isConnect) in
            
        }
    }
}
