//
//  QBMXSdk.h
//  KDFDApp
//
//  Created by hongyu on 2017/9/26.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKMXSdk : NSObject

+ (instancetype)shareMxSdk;

/**
 魔蝎SDK认证方法
 @param vc 跳转控制器
 @param taskType 认证分类
 * email:邮箱 bank:网银 insuarnce:车险保单 fund:公积金
 * alipay:支付宝 jingdong:京东 taobao:淘宝 carrier:运营商
 * qq:腾讯QQ maimai:脉脉 linkedin:领英 chsi:学信网
 * zhengxin:征信报告 tax:个人所得税
 */
- (void)mxSdkFunctionWithFromVc:(UIViewController *)vc taskType:(NSString *)taskType;

@end
