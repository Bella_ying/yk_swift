//
//  QBMXSdk.m
//  KDFDApp
//
//  Created by hongyu on 2017/9/26.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import "YKMXSdk.h"
#import "MoxieSDK.h"
#import "MoxieStatusView.h"

#ifdef DEBUG

//钱包的测试账号
//#define MXTheApiKey @"9ddc84c98d214db0a4dd0dcaa39a80bf" // 测试
//#define MXTheToken  @"fbd230085e344439903d49d096421efa"

//桔子优卡测试魔蝎
#define MXTheApiKey @"251482b6aa81401ea9dd4a1102febaf9"
#define MXTheToken  @"966940ddde3f49c8b4b139e57e264736"

#else
//大白回收的线上
//#define MXTheApiKey @"7cb7a444f251442eb45e667e88a93559" // 线上 //db1e9088b8ee427abb7d5a33001dfc87
//#define MXTheToken  @"569260c5c0724ed69b62e99227e2c804" //1a3a0e3d1dad4a44957484e1ea3f3d89

//桔子优卡正式魔蝎--
#define MXTheApiKey @"c36f6c8f985d4ab4b05ecfd88b601e43"
#define MXTheToken  @"5a5f06185f304cadadd88cf8d327035b"

#endif

static const CGFloat delayTime = 2.0f;

@interface YKMXSdk ()
<
MoxieSDKDelegate,
MoxieSDKDataSource
>

@property (nonatomic, strong) UIViewController *jumpVc;

@end

@implementation YKMXSdk

+ (instancetype)shareMxSdk
{
    static dispatch_once_t onceToken;
    static YKMXSdk *mxSdk;
    dispatch_once(&onceToken, ^{
        mxSdk = [YKMXSdk new];
    });
    return mxSdk;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self prepareSDK];
    }
    return self;
}

- (void)prepareSDK
{
    // 必须配置的基本参数
    [MoxieSDK shared].delegate = self;
    [MoxieSDK shared].apiKey   = MXTheApiKey;
    // 自定义moxieStatusView
    [MoxieSDK shared].dataSource  = self;
    [MoxieSDK shared].quitDisable = YES;
    [MoxieSDK shared].quitOnFail  = YES;
}

#pragma mark- moxieSDK dataSource
- (UIView *)statusViewForMoxieSDK
{
    MoxieStatusView *statusView = [[MoxieStatusView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, HEIGHT_OF_SCREEN) themeColor:[UIColor redColor]];
    [MoxieSDK shared].progressDelegate = statusView;
    return statusView;
}

#pragma mark- moxieSDK deletage
- (void)receiveMoxieSDKResult:(NSDictionary *)resultDictionary
{
    //任务结果code，详细参考文档
    int code = [resultDictionary[@"code"] intValue];
    //是否登录成功
    BOOL loginDone = [resultDictionary[@"loginDone"] boolValue];
    //任务类型
    NSString *taskType = resultDictionary[@"taskType"];
    //任务Id
    NSString *taskId  = resultDictionary[@"taskId"];
    //描述
    NSString *message = resultDictionary[@"message"];
    //当前账号
    NSString *account = resultDictionary[@"account"];
    //用户在该业务平台上的userId，例如支付宝上的userId（目前支付宝，淘宝，京东支持）
    NSString *businessUserId = resultDictionary[@"businessUserId"] ? resultDictionary[@"businessUserId"] : @"";
    DLog(@"get import result---code:%d,taskType:%@,taskId:%@,message:%@,account:%@,loginDone:%d，businessUserId:%@",code,taskType,taskId,message,account,loginDone,businessUserId);
    //【登录中】假如code是2且loginDone为false，表示正在登录中
    if(code == 2 && loginDone == false) {
        DLog(@"任务正在登录中，SDK退出后不会再回调任务状态，任务最终状态会从服务端回调，建议轮询APP服务端接口查询任务/业务最新状态");
    }
    //【采集中】假如code是2且loginDone为true，已经登录成功，正在采集中
    else if(code == 2 && loginDone == true) {
        DLog(@"任务已经登录成功，正在采集中，SDK退出后不会再回调任务状态，任务最终状态会从服务端回调，建议轮询APP服务端接口查询任务/业务最新状态");
    }
    //【采集成功】假如code是1则采集成功（不代表回调成功）
    else if(code == 1) {
        DLog(@"任务采集成功，任务最终状态会从服务端回调，建议轮询APP服务端接口查询任务/业务最新状态");
        [self goBackWithDelayTime:delayTime];
    }
    //【未登录】假如code是-1则用户未登录
    else if(code == -1) {
        DLog(@"用户未登录");
        [self goBackWithDelayTime:0];
    }
    //【任务失败】该任务按失败处理，可能的code为0，-2，-3，-4
    else{
        DLog(@"任务失败");
    }
}

- (void)goBackWithDelayTime:(CGFloat)time
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[YKTabBarController yk_shareTabController] yk_popToViewController:NSStringFromClass([self.jumpVc class])];
    });
}

- (void)mxSdkFunctionWithFromVc:(UIViewController *)vc taskType:(NSString *)taskType
{
    // 配置跳转vc
    self.jumpVc = vc;
    [MoxieSDK shared].fromController = vc;
    [MoxieSDK shared].taskType = taskType;
    [MoxieSDK shared].userId   = [NSString stringWithFormat:@"%zd", [YKUserManager sharedUser].uid];
    [[MoxieSDK shared] startFunction];
    DLog(@"上报用户id %zd", [YKUserManager sharedUser].uid);
}

@end
