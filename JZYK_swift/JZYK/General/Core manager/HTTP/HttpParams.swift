//
//  HTTPParams.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/11.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

struct HttpParams {
    static var appMarket: String {
        get {
            return "AppStore"
        }
    }
    static var appVersion: String {
        get {
            return DeviceInfo.appVersion()
        }
    }
    static var clientType: String {
        get {
            return UIDevice.current.systemName
        }
    }
    static var deviceId: String {
        get {
            return DeviceInfo.deviceID()
        }
    }
    static var deviceName: String {
        get {
            return DeviceInfo.deviceName()
        }
    }
    static var osVersion: String {
        get {
            return UIDevice.current.systemVersion
        }
    }
}
