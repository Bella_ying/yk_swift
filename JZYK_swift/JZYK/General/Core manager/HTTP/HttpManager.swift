//
//  HTTPManager.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/14.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Alamofire

enum HTTPVerbType {
    case Get
    case Post
}

typealias Succeed_callback = ((Dictionary<String, Any>, _ code: Int, _ successful: Bool, _ msg: String)->Void)
typealias Failure_callback = ((_ errMsg: String, _ isConnect: Bool)->Void)

@objcMembers class HTTPManager: NSObject {
    //单例创建
    @objc public static let session: HTTPManager = {
        let session = HTTPManager()
        return session
    }()
    
    lazy var reachabilityManager: ReachabilityManager = {
        let reachabilityManager = ReachabilityManager.defaultReachability;
        reachabilityManager.delegate = self;
        return reachabilityManager;
    }()
    
    var errMsg: String = ""
    
    var failure: Failure_callback?
    lazy var hud = HYProgressHUD.yk_share();
    
    private var appededParam: Dictionary<String, String> {
        get {
            return ["appMarket":HttpParams.appMarket,
                    "appVersion":HttpParams.appVersion,
                    "clientType":HttpParams.clientType,
                    "deviceId":HttpParams.deviceId,
                    "deviceName":HttpParams.deviceName,
                    "osVersion":HttpParams.osVersion];
        }
    }
    
    @objc public func getRequest(forKey key: String,
                                 showLoading: Bool = false,
                                 param: Dictionary<String, String>? = nil,
                                 succeed: @escaping(Succeed_callback),
                                 failure: @escaping(Failure_callback)) {
        if let url = ConfigManager.config.getURLWithKey(key) {
            self.getRequest(forUrl: url,
                            showLoading: showLoading,
                            param: param,
                            succeed: succeed,
                            failure: failure)
        }
    }
    
    @objc public func getRequest(forUrl url: String,
                                 showLoading: Bool = false,
                                 param: Dictionary<String, String>? = nil,
                                 succeed: @escaping(Succeed_callback),
                                 failure: @escaping(Failure_callback)) {
        var finalUrl = url
        if let param = param {
            if !param.isEmpty && param.count > 0 {
                param.forEach { (key, value) in
                    if key.count != 0 {
                        if finalUrl.contains("?") {
                            finalUrl.append("&\(key)=\(value)")
                        }else{
                            finalUrl.append("?\(key)=\(value)")
                        }
                    }
                }
            }
        }
        self.request(type: .Get,
                     url: finalUrl,
                     showLoading: showLoading,
                     succeed: succeed,
                     failure: failure)
    }
    
    @objc public func postRequest(forKey key: String,
                                  showLoading: Bool = false,
                                  param: Dictionary<String, Any>?,
                                  succeed: @escaping(Succeed_callback),
                                  failure: @escaping(Failure_callback)) {
        if let url = ConfigManager.config.getURLWithKey(key) {
            self.postRequest(forUrl: url,
                             showLoading: showLoading,
                             param: param,
                             succeed: succeed,
                             failure: failure)
        }
    }
    
    @objc public func postRequest(forUrl url: String,
                                  showLoading: Bool = false,
                                  param: Dictionary<String, Any>?,
                                  succeed: @escaping(Succeed_callback),
                                  failure: @escaping(Failure_callback)) {
        self.request(type: .Post,
                     url: url,
                     showLoading: showLoading,
                     parames: param,
                     succeed: succeed,
                     failure: failure)
    }
    
    
    func request(type: HTTPVerbType,
                 url: String,
                 showLoading: Bool = false,
                 parames: [String:Any]? = nil,
                 succeed: @escaping(Succeed_callback),
                 failure: @escaping(Failure_callback)) {
        var method: HTTPMethod
        switch type {
        case .Get:
            method = .get
        case .Post:
            method = .post
        }
        let sessionURL = String.addQueryStringTo(URL:url, param: appededParam)!
        YKDebugger.debugLog {
            print("sessionURL ====> \(sessionURL)")
        }
        YKUserManager.sharedUser().yk_handleCookie(forURLString: sessionURL)
        self.reachabilityManager.ennaleWatch() //开启网络状态监听
        
        self.failure = failure;
        //网络状态联接不正常提示
        if self.reachabilityManager.reachability?.connection != .none {//网络联接正常开启
            if showLoading {
                self.hud?.yk_showImageHud(withMessage: nil);
            }
            YKTools.yk_getGlobalUserAgent()
            
            let headers: HTTPHeaders = [
                "User-Agent": YKTools.yk_getGlobalUserAgent(),
                "Accept": "application/json"
            ];
            
            Alamofire.request(sessionURL, method:method, parameters:parames, headers: headers)
                .responseJSON { (retultObject) in
                
                switch retultObject.result {
                //成功
                case .success:
                    if let value = retultObject.result.value {
                        let dict = value as! Dictionary<String, Any>
                        // 成功闭包
                        let code = String(describing: dict["code"]!)
                        guard let codeNum = Int(code) else { return }
                        //增加判断，防止万一数据结构有变的崩溃
                        if showLoading {
                            self.hud?.yk_hideAnimated()
                        }
                        YKDebugger.debugLog {
                            print("params ==> \(String(describing: parames))")
                        }
                        if codeNum == -2 && !url.contains("upload-location"){
                            YKUserManager.sharedUser().yk_logoutAndDeleteUserInfo()
                            guard let controller = YKTabBarController.yk_shareTab().yk_getCurrentViewController() as? YKBaseViewController else { return }
                            controller.logIn();
                        }
                        //data解包成功
                        if let json = dict["data"] as? Dictionary<String, Any> {
                            
                            if let msg = dict["message"] as? String {
                                succeed(json, codeNum, true, msg)
                            } else {
                                succeed(json, codeNum, true, "数据结构有变，message解包失败")
                            }
                            YKDebugger.debugLog {
                                print("successful unwrap ==>\(json)") //如果数据过多，打印时间较长，这里去掉打印
                            }
                        } else { //data解包失败
                            if let msg = dict["message"] as? String {
                                succeed(dict, codeNum, false, msg)
                            } else {
                                succeed(dict, codeNum, false, "数据结构有变，data,message解包失败")
                            }
                            YKDebugger.debugLog {
                                print("result ==> \(dict)")
                            }
                        }
                        self.reachabilityManager.disableWatch() //关闭网络状态监听
                    }
                //失败
                case .failure:
                    if showLoading {
                        self.hud?.yk_hideAnimated()
                    }
                }
            }
        }
    }
    
    @objc public func getBodyRequest(forKey key: String,
                                     showLoading: Bool = false,
                                     param: Dictionary<String, Any>? = nil,
                                     succeed: @escaping(Succeed_callback),
                                     failure: @escaping(Failure_callback)) {
        if let url = ConfigManager.config.getURLWithKey(key) {
            let sessionURL = String.addQueryStringTo(URL:url, param: appededParam)!
            let headers: HTTPHeaders = [
                "Accept": "application/json"
            ]
            Alamofire.request(sessionURL,
                              method: .get,
                              parameters: param,
                              encoding: JSONEncoding.default,
                              headers: headers)
                .responseJSON(queue: DispatchQueue.main) { (json) in
                    YKDebugger.debugLog {
                        print(json);
                    }
            }
        }
    }
    
    /// 上传图片
    ///
    /// - Parameters:
    ///   - data: 图片数据
    ///   - urlKey: 上传的服务器URLKey值
    ///   - key: key
    ///   - fileName: fileName
    ///   - parameters: 参数
    ///   - uploadSuccess: 上传成功处理
    ///   - uploadProgress: 上传进度
    ///   - uploadFailure: 上传失败处理
    @objc public func uploadImage(data: Data,
                                  serverUrlKey urlKey: String,
                                  key: String,
                                  fileName: String,
                                  parameters: Dictionary<String, AnyObject>,
                                  uploadSuccess: @escaping ([String: Any])->Void,
                                  uploadProgress: @escaping (Progress) -> Void,
                                  uploadFailure: @escaping (String)->Void ) {
        guard let url = ConfigManager.config.getURLWithKey(urlKey) else {
            return
        }
        let headers: HTTPHeaders = [
            "User-Agent": YKTools.yk_getGlobalUserAgent(),
        ];
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data, withName: key, fileName: fileName, mimeType: "image/jpeg")
            for (key, val) in parameters {
                multipartFormData.append(val.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, headers: headers) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let jsonResponse = response.result.value as? [String: Any] {
                        YKDebugger.debugLog {
                            print(jsonResponse)
                        }
                        uploadSuccess(jsonResponse)
                    }
                }
                upload.uploadProgress(closure: { (progress) in
                    uploadProgress(progress)
                })
            case .failure(let encodingError):
                YKDebugger.debugLog {
                    print(encodingError)
                }
            }
        }
    }
    
    
    deinit {
        self.reachabilityManager.disableWatch()
    }
    
    
    /// 上传data类型数据  -- 上传通讯录有用到
    ///
    /// - Parameters:
    ///   - key: 上传的服务器URLKey值
    ///   - parameters: 参数
    ///   - uploadSuccess: 上传成功处理
    ///   - uploadFailure: 上传失败处理
    @objc public func uploadGzipp(forKey key: String,
                                  showLoading: Bool = false,
                                  param: Dictionary<String, Any>? = nil,
                                  succeed: @escaping(Succeed_callback),
                                  failure: @escaping(Failure_callback)) {
        if let url = ConfigManager.config.getURLWithKey(key) {
            let sessionURL = String.addQueryStringTo(URL:url, param: appededParam)!
            let url = URL(string: sessionURL)!
            let paramStr = NSString.yk_addQueryStringWithparams(param) //把字典转化为查询字符串格式
            let data = YKGzipUtility.gzipData(paramStr?.data(using: .utf8))
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "POST"
            urlRequest.httpBody = data
            urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue("gzip", forHTTPHeaderField: "Content-Encoding")
            
            self.reachabilityManager.ennaleWatch() //开启网络状态监听
            if self.reachabilityManager.reachability?.connection != .none {//网络联接正常开启
                if showLoading {
                    self.hud?.yk_showImageHud(withMessage: nil);
                }
                Alamofire.request(urlRequest).responseJSON(queue: DispatchQueue.main) { (json) in
                    switch json.result {
                    //成功
                    case .success:
                        if let value = json.result.value {
                            let dict = value as! Dictionary<String, Any>
                            // 成功闭包
                            let code = String(describing: dict["code"]!)
                            guard let codeNum = Int(code) else { return }
                            //增加判断，防止万一数据结构有变的崩溃
                            if showLoading {
                                self.hud?.yk_hideAnimated()
                            }
                            YKDebugger.debugLog {
                                print("params ==> \(String(describing: param))")
                            }
                            if codeNum == -2 && !sessionURL.contains("upload-location"){
                                YKUserManager.sharedUser().yk_logoutAndDeleteUserInfo()
                                guard let controller = YKTabBarController.yk_shareTab().yk_getCurrentViewController() as? YKBaseViewController else { return }
                                controller.logIn();
                            }
                            if let json = dict["data"] as? Dictionary<String, Any> {
                                
                                if let msg = dict["message"] as? String {
                                    succeed(json, codeNum, true, msg)
                                } else {
                                    succeed(json, codeNum, true, "数据结构有变，message解包失败")
                                }
                            } else {
                                if let msg = dict["message"] as? String {
                                    succeed(dict, codeNum, false, msg)
                                } else {
                                    succeed(dict, codeNum, false, "数据结构有变，data,message解包失败")
                                }
                                YKDebugger.debugLog {
                                    print("result ==> \(dict)")
                                }
                            }
                            self.reachabilityManager.disableWatch() //关闭网络状态监听
                        }
                    //失败
                    case .failure:
                        if showLoading {
                            self.hud?.yk_hideAnimated()
                        }
                    }
                }
            }
        }
    }
}

extension HTTPManager: NetworkChangedDelegate {
    func isConnect(_ connect: Bool) {
        if !connect {
            self.failure!("当前暂无网络连接，请检查网络设置是否正常", false)
        }else {
            self.failure!("", true)
        }
        self.reachabilityManager.disableWatch() //关闭网络状态监听
    }
}

















