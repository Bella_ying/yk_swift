//
//  YKAlbumManager.h
//  JZYK
//
//  Created by Jeremy on 2018/7/23.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^ResponseSuccessBlock)(void);

@interface YKAlbumManager : NSObject

@property (nonatomic, copy)NSString *objectId;

@property (nonatomic, copy) ResponseSuccessBlock responseSuccessBlock;


+ (instancetype)shareAlbumInstance;


- (void)showPhotoAlbum;

- (void)showCamera;

@end
