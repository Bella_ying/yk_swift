//
//  YKContactManager.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKContactFetcher : NSObject
//保存排序好的数组index
@property (nonatomic, strong) NSMutableArray *dataArray;
//数组里面保存每个获取Vcard（名片）
@property (nonatomic, strong) NSMutableArray *dataArrayDic;
//通讯录总数量
@property (nonatomic, assign) NSInteger addressAmount;
/**
 单例

 @return 单例
 */
+ (YKContactFetcher *)shareFetcher;

/**
 获取通讯录

 @return 获取通讯录
 */
- (NSDictionary *)fetchContactData;
/**
 排序后的通讯录数据

 @return 排序后的通讯录数据
 */
- (NSArray *)sortedContactData;

@end
