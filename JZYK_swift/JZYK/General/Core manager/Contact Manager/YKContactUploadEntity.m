//
//  YKContactEntity.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKContactUploadEntity.h"

@implementation YKContactUploadEntity

// 直接添加以下代码即可自动完成
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self yy_modelEncodeWithCoder:aCoder];
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    return [self yy_modelInitWithCoder:aDecoder];
}
- (id)copyWithZone:(NSZone *)zone {
    return [self yy_modelCopy];
}
- (NSUInteger)hash {
    return [self yy_modelHash];
}
@end
