//
//  YKContactEntity.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//  用于记录不同的用户上传通讯录的历史

#import <Foundation/Foundation.h>

@interface YKContactUploadEntity : NSObject<NSCoding,NSCopying>

@property (nonatomic, copy) NSString *uid;            //uid
@property (nonatomic, copy) NSString *addressAmount;  //上次上传的总数
@property (nonatomic, copy) NSArray *uploadedAddress; //上传过的通讯录(用于对比用户有没有新的未上传的通讯录)

@end
