//
//  YKContactManager.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKContactFetcher.h"
#import <MessageUI/MessageUI.h>
#import <AddressBookUI/AddressBookUI.h>

static YKContactFetcher *fetcher = nil;

@interface YKContactFetcher()<ABPeoplePickerNavigationControllerDelegate>

@end

@implementation YKContactFetcher

+ (YKContactFetcher *)shareFetcher
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        fetcher = [YKContactFetcher new];
    });
    return fetcher;
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)dataArrayDic
{
    if (!_dataArrayDic) {
        _dataArrayDic = [NSMutableArray array];
    }
    return _dataArrayDic;
}

- (NSDictionary *)fetchContactData
{
    [self.dataArray removeAllObjects];
    [self.dataArrayDic removeAllObjects];
    
    //typedef CFTypeRef ABAddressBookRef;
    //typedef const void * CFTypeRef;
    //指向常量的指针
    ABAddressBookRef addressBook = nil;
    /*
     /如果系统版本大于等于6.0，使用对应的api获取通讯录，
     注意，必须先请求用户的同意，如果未获得同意或者用户未操作，此通讯录的内容为空
     */
    if (@available(iOS 6.0, *)) {
        /*
         //为了保证用户同意后在进行操作，此时使用多线程的信号量机制，
         创建信号量，信号量的资源数0表示没有资源，调用dispatch_semaphore_wait会立即等待。
         */
        addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        //等待同意后向下执行
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ////发出访问通讯录的请求
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error){
            /*用户同意，才会执行此block里面的方法;
             此方法发送一个信号，增加一个资源数
             */
            dispatch_semaphore_signal(sema);});
        /*
         如果之前的block没有执行，则sema的资源数为零，程序将被阻塞
         当用户选择同意，block的方法被执行， sema资源数为1；
         */
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    } else {
        //6.0之前的系统，不需要获得同意
         addressBook = ABAddressBookCreate();
    }
    //通讯录信息已获得，开始取出
    CFArrayRef results = ABAddressBookCopyArrayOfAllPeople(addressBook);
    //联系人条目数
    NSInteger peopleCount = 0;
    if (results) {
        peopleCount = CFArrayGetCount(results);
    }
    for (NSInteger idx = 0; idx < peopleCount; idx++) {
        NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];
        ABRecordRef record = CFArrayGetValueAtIndex(results, idx);
        //取得完整名字（与上面firstName、lastName无关）
        CFStringRef  fullName = ABRecordCopyCompositeName(record);
        [tempDic setObject:(__bridge NSString*)fullName ? (__bridge NSString *)fullName : @"" forKey:@"name"];
        NSString *phoneStr = @"";
        // 读取电话,不只一个
        ABMultiValueRef phones = ABRecordCopyValue(record, kABPersonPhoneProperty);
        
        NSInteger phoneCount = ABMultiValueGetCount(phones);
        //大于5个号码 判断为垃圾号码 可能是杀毒软件添加的 我们去掉这些号码
        if (phoneCount >5) {
            continue;
        }
        for (NSInteger num = 0; num < phoneCount; num++) {
            // label
            CFStringRef lable = ABMultiValueCopyLabelAtIndex(phones, num);
            // phone number
            CFStringRef number = ABMultiValueCopyValueAtIndex(phones, num);
            // localize label
            CFStringRef local = ABAddressBookCopyLocalizedLabel(lable);
            //此处可使用一个字典来存储通讯录信息
            if (num > 0) {
                phoneStr = [phoneStr stringByAppendingString:@":"];
            }
            phoneStr = [phoneStr stringByAppendingString:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, num)];
            phoneStr = [NSString clearPhonePrefixNumber:phoneStr];
            if (local) {
                CFRelease(local);
            }
            if (lable) {
                CFRelease(lable);
            }
            if (number) {
                CFRelease(number);
            }
        }
        [tempDic setObject:phoneStr  forKey:@"mobile"];
        [self.dataArrayDic addObject:tempDic];
        if (fullName) {
            CFRelease(fullName);
        }
        if (phones) {
            CFRelease(phones);
        }
        record = NULL;
    }
    if (results) {
        CFRelease(results);
    }
    results = nil;
    
    if (addressBook) {
        CFRelease(addressBook);
    }
    addressBook = NULL;
    
    //排序
    //建立一个字典，字典保存key是A-Z  值是数组
    NSMutableDictionary *index = [NSMutableDictionary dictionary];
    for (NSDictionary*dic in self.dataArrayDic) {
        NSString *str = [dic objectForKey:@"name"];
        NSString *phone = [dic objectForKey:@"mobile"];
        //获得中文拼音首字母，如果是英文或数字则#
        if (!str || [str isEqualToString:@""]) {
            continue;
        }
        if (!phone) {
            continue;
        }
        NSString *firstLetter = nil;
        
        if ([str canBeConvertedToEncoding:NSASCIIStringEncoding]) {
            firstLetter = [[NSString stringWithFormat:@"%c",[str characterAtIndex:0]] uppercaseString];
        }else {
            if ([self convertPinyinUseChineseCharacter:str] && ![[self convertPinyinUseChineseCharacter:str] isEqualToString:@""]) {
                firstLetter = [[NSString stringWithFormat:@"%c",[[self convertPinyinUseChineseCharacter:str] characterAtIndex:0]]uppercaseString];
            } else {
                firstLetter = @"#";
            }
        }
        //如果首字母是数字或者特殊符号
        if (firstLetter.length > 0) {
            if (!isalpha([firstLetter characterAtIndex:0])) {
                firstLetter = @"#";
            }
        }
        if ([[index allKeys] containsObject:firstLetter]) {
            //判断index字典中，是否有这个key如果有，取出值进行追加操作
            [[index objectForKey:firstLetter] addObject:dic];
        } else {
            NSMutableArray *tempArray = [NSMutableArray array];
            [tempArray addObject:dic];
            [index setObject:tempArray forKey:firstLetter];
        }
    }
    
    [self.dataArray addObjectsFromArray:[index allKeys] ? [index allKeys] : @[]];
    
    return [index copy];
}

- (NSArray *)sortedContactData
{
    return  [self.dataArray sortedArrayUsingFunction:cmp context:NULL];
}

- (NSString *)convertPinyinUseChineseCharacter:(NSString *)str
{
    NSMutableString *mutableString = [NSMutableString stringWithString:str];
    CFStringTransform((CFMutableStringRef)mutableString, NULL, kCFStringTransformToLatin, false);
    mutableString = (NSMutableString *)[mutableString stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale currentLocale]];
    return mutableString;
}

// 全部转换为小写
- (NSString *)upperStr:(NSString*)str
{
    return [str lowercaseStringWithLocale:[NSLocale currentLocale]];
}

//构建数组排序方法SEL
NSInteger cmp(NSString *lhs, NSString *rhs, void * p)
{
    if([lhs compare:rhs] == 1){
        return NSOrderedDescending;
    }else
        return  NSOrderedAscending;
}
@end
