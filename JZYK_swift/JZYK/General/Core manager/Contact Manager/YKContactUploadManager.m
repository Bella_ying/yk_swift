//
//  YKContactUploadManager.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKContactUploadManager.h"
#import "YKContactFetcher.h"
#import "YKAccessAlertView.h"
#import "YKContactUploadEntity.h"
#import "YKGzipUtility.h"

@interface YKContactUploadManager()

@property (nonatomic, assign) NSInteger items; //每次上传数据的数量

@end

@implementation YKContactUploadManager

+(instancetype)shareContact
{
    static dispatch_once_t predicate;
    static YKContactUploadManager *manager = nil;
    dispatch_once(&predicate, ^{
        manager = [YKContactUploadManager new];
        manager.items = 500;
    });
    return manager;
}

- (void)yk_uploadContactItem
{
    NSArray *package = [self getUploadAddress];
    //没有更多通讯录时不再上传
    if (!package || [package count] == 0) {
        return;
    }
    YKContactUploadEntity *entity = [[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:[NSString stringWithFormat:@"%@%ld",addressUserUpLoaded,[YKUserManager sharedUser].uid]];
    NSString *count = entity.addressAmount;
    count = count ? count : @"0";
    NSDictionary * param = @{@"contacts_count":count};
    [[HTTPManager session] getRequestForKey:kCreditInfoUploadContentsPrepared showLoading:NO param:param succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString * _Nonnull msg) {
        if (code == 0) {
            
            if (![NSJSONSerialization isValidJSONObject:package]) {
                return ;
            }
            NSError *error;
            NSData *data = [NSJSONSerialization dataWithJSONObject:package options:0 error:&error];
            if (error) {
                return;
            }
            NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSDictionary * param1 = @{@"data":jsonStr,@"type":@"3"};
            //👇上传通讯录失败，需要更改
            [[HTTPManager session] uploadGzippForKey:kInfoUpLoadContacts showLoading:NO param:param1 succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString * _Nonnull msg) {
                
            } failure:^(NSString * _Nonnull msg, BOOL isLink) {
                
            }];
            
        }
    } failure:^(NSString * _Nonnull msg, BOOL isLink) {
        
    }];
}

- (NSArray *)getUploadAddress
{
    //未登录状态不上传
    if (![YKUserManager sharedUser].yk_isLogin) {
        return @[];
    }
    
    //获取当前通讯录
    NSDictionary *dataDic = [[YKContactFetcher shareFetcher] fetchContactData];
    NSArray *titleArr = [[YKContactFetcher shareFetcher] sortedContactData];
    if (!dataDic || !titleArr) {
        return @[];
    }
    NSMutableArray *tempArray = [@[] mutableCopy];
    [titleArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (dataDic[obj] && [dataDic[obj] isKindOfClass:[NSArray class]]) {
            [tempArray addObjectsFromArray:dataDic[obj]];
        }
    }];
    
    //获取所有通讯录，用dictionary特性去重
    NSMutableDictionary *addressDic = [@{} mutableCopy];
    [tempArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSDictionary class]] && obj[@"name"] && obj[@"mobile"] && ![obj[@"mobile"] isEqualToString:@""]) {
            [addressDic setObject:obj[@"name"] forKey:[self clearPhone:obj[@"mobile"]]];
        }
    }];
    
    //对比已经上传过得通讯录，筛选出没有上传过的
    YKContactUploadEntity *entity = [[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:[NSString stringWithFormat:@"%@%ld",addressUserUpLoaded,[YKUserManager sharedUser].uid]];
    if (!entity) {
        entity = [YKContactUploadEntity yy_modelWithJSON:@{}];
        entity.addressAmount = [NSString stringWithFormat:@"%ld",addressDic.allKeys.count];
        entity.uploadedAddress = @[];
        [[YKCacheManager sharedCacheManager] yk_cacheObject:entity forKey:[NSString stringWithFormat:@"%@%ld",addressUserUpLoaded,[YKUserManager sharedUser].uid]];
    }
    
    //去掉上传过的
    [entity.uploadedAddress enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (addressDic[obj[@"mobile"]]) {
            [addressDic removeObjectForKey:obj[@"mobile"]];
        }
    }];
    
    //保证每次上传的条数不超过上限
    while (addressDic.count > self.items) {
        [addressDic removeObjectForKey:addressDic.allKeys[0]];
    }
    
    //组装返回数据
    NSMutableArray *array = [@[] mutableCopy];
    [addressDic.allKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [array addObject:@{@"name":addressDic[obj],@"mobile":obj,@"user_id":@([YKUserManager sharedUser].uid)}];
    }];
    
    return array;
}

- (NSString *)clearPhone:(NSString *)phone
{
    phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"+86" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    return phone;
}

- (BOOL)yk_checkContactPermissionGrantStatusWithFailToTip:(BOOL)isTip
{
    if ([[YKContactFetcher shareFetcher] fetchContactData] && [[YKContactFetcher shareFetcher] sortedContactData].count > 0) {
        return YES;
    } else {
        //是否提醒
        if (isTip) {
            //提示打开
            [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeContactBook contentText:@""];
        }
        return NO;
    }
}



@end
