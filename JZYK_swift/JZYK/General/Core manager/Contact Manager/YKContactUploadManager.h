//
//  YKContactUploadManager.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  思路
 *  每次上传的时候看一下本地有没有对应当前用户上传
 *  每次上传的时候对比当前用户通讯录
 */
@interface YKContactUploadManager : NSObject

+ (instancetype)shareContact;

/**
 上传通讯录条目
 */
- (void)yk_uploadContactItem;

/**
 检查通讯录权限是否开启并在位开启时提示新用户开启

 @param isTip 是否提示新用户开启
 @return 是否开启权限
 */
- (BOOL)yk_checkContactPermissionGrantStatusWithFailToTip:(BOOL)isTip;

@end
