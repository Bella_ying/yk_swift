//
//  YKCacheManager.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKCacheManager.h"

@interface YKCacheManager()
@property (nonatomic, retain) NSCache *memoryCache;     //内存缓存，kill掉程序或超出设置的最大缓存则清除
@end

@implementation YKCacheManager

+ (instancetype)sharedCacheManager
{
    
    static YKCacheManager *_sharedCache = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedCache = [[YKCacheManager alloc] init];
    });
    
    return _sharedCache;
}

- (NSCache *)memoryCache
{
    if (!_memoryCache) {
        _memoryCache = [[NSCache alloc] init];
        _memoryCache.countLimit = 20;       //设置内存最大缓存20条，超出则自动清除
    }
    return _memoryCache;
}

- (NSString *)getPath
{
    NSString *path = [NSHomeDirectory() stringByAppendingString:@"/Documents/userJZYK"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return path;
}
//新增
- (NSString *)getPathWithDirectory:(NSString *)directory
{
    NSString *tempPath = [@"/Documents/" stringByAppendingString:[NSString stringWithFormat:@"%@",directory]];
    NSString *path = [NSHomeDirectory() stringByAppendingString:tempPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return path;
}

- (NSString *)getPathWithKey:(NSString *)key
{
    return [NSString stringWithFormat:@"%@/%@.jzyk",[self getPath],key];
}
//新增
- (NSString *)getPathWithKey:(NSString *)key andDicretory:(NSString *)directory{
    return [NSString stringWithFormat:@"%@/%@.jzyk",[self getPathWithDirectory:directory],key];
}

- (BOOL)yk_cacheObject:(id<NSCoding>)object forKey:(NSString *)key{
    if (!key || [key isEqualToString:@""]) {
        DLog(@"缓存key不能为空！！");
        return NO;
    }
    if (object) {
        NSMutableData *data = [NSMutableData data];
        NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
        [archiver encodeObject:object forKey:[NSString stringWithFormat:@"%@shyy",key]];
        [archiver finishEncoding];
        return  [data writeToFile:[self getPathWithKey:key] atomically:YES];
    }
    return NO;
}
////新增根据目录来缓存
- (void)yk_cacheObject:(id<NSCoding>)object forKey:(NSString *)key atDirectoryPath:(NSString *)path
{
    if (!key || [key isEqualToString:@""]) {
        DLog(@"缓存key不能为空！！");
        return;
    }
    if (object) {
        NSMutableData *data = [NSMutableData data];
        NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
        [archiver encodeObject:object forKey:[NSString stringWithFormat:@"%@shyy",key]];
        [archiver finishEncoding];
        [data writeToFile:[self getPathWithKey:key andDicretory:path] atomically:YES];
    }
    
}

- (id)yk_getCacheObjectForKey:(NSString *)key
{
    if (!key || [key isEqualToString:@""]) {
        DLog(@"缓存key不能为空！！");
        return nil;
    }
    NSMutableData *data = [NSMutableData dataWithContentsOfFile:[self getPathWithKey:key]];
    if (data && ![data isKindOfClass:[NSNull class]]) {
        NSKeyedUnarchiver *unArchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        return [unArchiver decodeObjectForKey:[NSString stringWithFormat:@"%@shyy",key]];
    }
    return nil;
}
//新增根据目录来取
- (id)yk_getCacheObjectforKey:(NSString *)key atDirectoryPath:(NSString *)path
{
    if (!key || [key isEqualToString:@""]) {
        DLog(@"缓存key不能为空！！");
        return nil;
    }
    NSMutableData *data = [NSMutableData dataWithContentsOfFile:[self getPathWithKey:key andDicretory:path]];
    if (data && ![data isKindOfClass:[NSNull class]]) {
        NSKeyedUnarchiver *unArchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        return [unArchiver decodeObjectForKey:[NSString stringWithFormat:@"%@shyy",key]];
    }
    return nil;
}

- (void)yk_removeCacheWithKey:(NSString *)key
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:[self getPathWithKey:key]]) {
        [fileManager removeItemAtPath:[self getPathWithKey:key] error:&error];
        if (error) {
            DLog(@"remove cache error = %@",error);
        }
    }else{
        DLog(@"无对应文件！");
    }
}

- (void)yk_removeCacheObjectForKey:(NSString *)key atDirectoryPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:[self getPathWithKey:key andDicretory:path]]) {
        [fileManager removeItemAtPath:[self getPathWithKey:key andDicretory:path]error:&error];
        if (error) {
            DLog(@"remove cache error = %@",error);
        }
    }else{
        DLog(@"无对应文件！");
    }
}


//根据目录移除某个目录下所有文件
- (void)yk_removeaAllCacheAtDirectoryPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    [fileManager removeItemAtPath:[self getPathWithDirectory:path] error:&error];
    if (error) {
        DLog(@"remove cache error = %@",error);
    }
}

- (void)yk_removeAllCache
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    [fileManager removeItemAtPath:[self getPath] error:&error];
    if (error) {
        DLog(@"remove cache error = %@",error);
    }
}

- (void)yk_memoryCacheObject:(id)object forKey:(NSString *)key
{
    DLog(@"设置---->%@,%@",object,key);
    [self.memoryCache setObject:object forKey:key];
}

- (id)yk_getMemoryCacheObjectForKey:(NSString *)key
{
    DLog(@"缓存中---->%@,%@",[self.memoryCache objectForKey:key],key);
    return [self.memoryCache objectForKey:key];
}

#pragma mark - NSCacheDelegate
- (void)cache:(NSCache *)cache willEvictObject:(id)obj
{
    DLog(@"清除了---->%@",obj);
}
@end
