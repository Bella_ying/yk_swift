//
//  CacheKey.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

//用户信息
static NSString *const ZFBPath = @"KDZFB";
//用户信息userLoginInfo
static NSString *const kUserLoginCacheKey = @"LoginUser";


/***************首页相关***************/

static NSString *const kPublicNotice = @"publicNotice";
static NSString *const kHomeAnoucePublicNoticekey = @"kHomeAnoucePublicNoticekey";
static NSString *const kLoanDetailAnoucePublicNoticekey = @"kLoanDetailAnoucePublicNoticekey";
static NSString *const kPaymentDetailAnoucePublicNoticekey = @"kPaymentDetailAnoucePublicNoticekey";
static NSString *const kMineAnoucePublicNoticekey = @"kMineAnoucePublicNoticekey";
static NSString *const kWithdrawAnoucePublicNoticekey = @"kWithdrawAnoucePublicNoticekey";

//首页未登录数据缓存
static NSString *const unLoginhomeModel = @"unLoginhomeModel";
//首页登录数据缓存
static NSString *const loginHomeModel = @"loginHomeModel";
//个人中心
static NSString *const UserCenter = @"UserCenter";
//首页弹框
static NSString *const bounArray = @"dataArray";

//通讯录
static NSString *const AddressArray = @"AddressArray";
static NSString *const NewAddressArray = @"NewAddressArray";
static NSString *const CopyNewAddressArray = @"CopyNewAddressArray";
//通讯录对应用户上传历史
static NSString *const addressUserUpLoaded = @"addressUserUpLoaded";

//红点
static NSString *const loanRedPoint = @"loanRedPoint";
static NSString *const messageRedPoint = @"messageRedPoint";
static NSString *const couponRedPoint = @"couponRedPoint";
static NSString *const redPackRedPoint = @"redPackRedPoint";
static NSString *const verificationRedPoint = @"verificationRedPoint";

//判断红点缓存
static NSString *const loanPoint = @"loanPoint";
static NSString *const messagePoint = @"messagePoint";
static NSString *const couponPoint = @"couponPoint";
static NSString *const redPackPoint = @"redPackPoint";
static NSString *const verificationPoint = @"verificationPoint";

//首次显示前程数据提示框
static NSString *const QCAlertShow = @"QCAlertShow";

//tabbar缓存
static NSString *const tabbarRedPoint = @"tabbarRedPoint";
static NSString *const kTabbarPointCacheKey = @"tabbarPoint";
static NSString *const kMineRedPointCacheKey = @"mineRedPoint";
static NSString *const kMinePointCacheKey = @"minePoint";
//支付宝加密数据
static NSString *const zfbCapture = @"zfbCapture";

//判断用户是否弹出前程提示框
static NSString *const QCAlert = @"QCAlert";

//tabBar的model数组
static NSString *const kTabBarEntityArrayCacheKey = @"tabBarEntityArray";

static NSString *const tabBarImageUrlArray = @"tabBarImageUrlArray";

static NSString *const tabBarImagesIsCacheAll = @"tabBarImagesIsCacheAll";

//首页通讯录是否全部上传成功
static NSString *const contactIsUploadAll = @"contactIsUploadAll";
