//
//  YKCacheManager.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/8.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CacheKey.h"

@interface YKCacheManager : NSObject

/**
 *  返回共享的唯一实例，不需要设属性
 *
 */
+ (instancetype)sharedCacheManager;

/**
 *  缓存数据
 *
 *  @param object 需要缓存的对象
 *  @param key    唯一标识，也作为文件名
 */
- (BOOL)yk_cacheObject:(id<NSCoding>)object
                forKey:(NSString *)key;


/**
 *  缓存数据
 *
 *  @param object 需要缓存的对象
 *  @param key    唯一标识，也作为文件名
 *  @param path  可以根据路径来存储
 */
- (void)yk_cacheObject:(id<NSCoding>)object
                forKey:(NSString *)key
       atDirectoryPath:(NSString *)path;

/**
 *  取出缓存
 *
 *  @return 返回缓存对象
 */
- (id)yk_getCacheObjectForKey:(NSString *)key;


/**
 *  新增根据目录来取缓存
 *
 *  @return 返回缓存对象
 */

- (id)yk_getCacheObjectforKey:(NSString *)key
              atDirectoryPath:(NSString *)path;

/**
 *  根据对应的key删除磁盘缓存
 *
 *  @param key  唯一标识，也作为文件名
 */

- (void)yk_removeCacheWithKey:(NSString *)key;

/**
 *  根据对应的key删除磁盘缓存
 *
 *  @param key  唯一标识，也作为文件名
 *  @param path 自定义下document的路径
 */
- (void)yk_removeCacheObjectForKey:(NSString *)key
                   atDirectoryPath:(NSString *)path;
/**
 *  删除所有的磁盘缓存
 *
 */
- (void)yk_removeAllCache;

/**
 *  移除某个目录下所有文件
 *
 *  @param path  目录
 */
- (void)yk_removeaAllCacheAtDirectoryPath:(NSString *)path;

/**
 *  缓存数据
 *
 *  @param object 需要缓存的对象
 *  @param key    唯一标识，也作为文件名
 */
- (void)yk_memoryCacheObject:(id)object
                      forKey:(NSString *)key;

/**
 *  取出缓存
 *
 *  @return 返回缓存对象
 */
- (id)yk_getMemoryCacheObjectForKey:(NSString *)key;

@end
