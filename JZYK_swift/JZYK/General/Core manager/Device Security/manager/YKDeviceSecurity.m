//
//  YKDeviceSecurity.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/20.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//  手机设备的安全管理类

#import "YKDeviceSecurity.h"
#import "YKTouchIDRecognitionManager.h"
#import "YKTouchIDTool.h"
#import "QBGestureTool.h"
#import "QBGestureViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>

@implementation YKDeviceSecurity

+ (DeviceSecurityType)isDeviceSupportType
{
    if (NSFoundationVersionNumber < NSFoundationVersionNumber_iOS_8_0) {
        return DeviceSecurityTypeNone;
    }
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    BOOL can = [context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error];
    if (@available(iOS 11.0, *)) {
        switch (context.biometryType) {
            case LABiometryTypeTouchID:
                return DeviceSecurityTypeTouchID;
                break;
            case LABiometryTypeFaceID:
                return DeviceSecurityTypeFaceID;
                break;
                
            case LABiometryNone:
                return DeviceSecurityTypeNone;
                break;
        }
    }else {
        return can ? DeviceSecurityTypeTouchID : DeviceSecurityTypeNone;
    }
    
}
#pragma mark 验证指纹
+ (void)verifyTouchID:(void(^)(void))dismiss
{
    //验证是否是登录状态，并开启了指纹解锁
    if ([YKTouchIDTool yk_isTouchIDEnableByUser]) {
        //验证指纹
        [[YKTouchIDRecognitionManager shareYKTouchIDRecognitionManager] yk_loadAuthentication];
        [[YKTouchIDRecognitionManager shareYKTouchIDRecognitionManager] setTouchIDBlock:^(NSDictionary *callBackDic) {
            dispatch_async(dispatch_get_main_queue(), ^{
                DLog(@"%@",callBackDic);
                if ([callBackDic[@"code"] integerValue] == 5 ) {
                    //指纹验证成功
                    if (dismiss) {
                        dismiss();
                    }
                }else{
//                    [[QLAlert alert] showWithMessage:callBackDic[@"message"] singleBtnTitle:@"确定"];
                }
            });
        }];
    }
}
#pragma mark 验证手势
+ (void)verifyGesturePasswordShowInViewController:(UIViewController *)viewController
{
    //验证是否是登录状态，并开启了手势密码
    if ([QBGestureTool isGestureEnable] && [YKUserManager sharedUser].yk_isLogin) {
        QBGestureViewController *gestureVC = [[QBGestureViewController alloc]init];
        gestureVC.rootViewController = viewController;
        [gestureVC showInViewController:viewController type:QBGestureTypeVerifying];
    }
}
+ (void)presentLoginViewController:(UIViewController*)viewController
{
    if ([YKTouchIDTool yk_isHiddenLoginCancelBtnByUser]){
        //如果登录页面的返回按钮hidden
        dispatch_async(dispatch_get_main_queue(), ^{
            [YKTouchIDTool yk_saveHiddenLoginCancelBtnByUser:YES];
            if(![[YKUserManager sharedUser] yk_isLogin] ){
                if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[LoginViewController class]]) {
                    LoginViewController *login = [LoginViewController login];
                    [viewController presentViewController:login animated:YES completion:nil];
                }
            }
        });
    }
        
}

@end
