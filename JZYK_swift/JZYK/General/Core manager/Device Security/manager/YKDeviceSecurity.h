//
//  YKDeviceSecurity.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/20.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DeviceSecurityType) {
    DeviceSecurityTypeTouchID,
    DeviceSecurityTypeFaceID,
    DeviceSecurityTypeNone,
};

@interface YKDeviceSecurity : NSObject

+ (DeviceSecurityType)isDeviceSupportType;

+ (void)verifyTouchID:(void(^)(void))dismiss;

+ (void)verifyGesturePasswordShowInViewController:(UIViewController*)viewController;

+ (void)presentLoginViewController:(UIViewController*)viewController;
@end
