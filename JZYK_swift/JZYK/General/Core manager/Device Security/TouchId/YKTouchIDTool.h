//
//  DNTouchIDTool.h
//  dingniu
//
//  Created by zhaoying on 2018/4/11.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKTouchIDTool : NSObject
// 用户是否开启指纹解锁
+ (void)yk_saveTouchIDByUser:(BOOL)isEnable;

+ (BOOL)yk_isTouchIDEnableByUser;

+ (void)yk_saveHiddenLoginCancelBtnByUser:(BOOL)isEnable;

+ (BOOL)yk_isHiddenLoginCancelBtnByUser;

@end
