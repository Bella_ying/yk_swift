//
//  DNTouchIdViewController.m
//  dingniu
//
//  Created by zhaoying on 2018/4/10.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKTouchIdViewController.h"
#import "YKTouchIDRecognitionManager.h"
#import "YKTouchIDTool.h"
@interface YKTouchIdViewController ()
@property (weak, nonatomic) IBOutlet UIButton *unlockTheFingerprintBtn;
- (IBAction)unlockTheFingerprintBtnClick:(id)sender;
- (IBAction)otherLoginType:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (nonatomic,strong) UIViewController *keyVC;
@end

@implementation YKTouchIdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.unlockTheFingerprintBtn.layer.cornerRadius = 45/2;
    self.unlockTheFingerprintBtn.clipsToBounds = YES;
    
    if (IS_iPhoneX) {
        self.navigationItem.title = @"面容id";
        [self.unlockTheFingerprintBtn setTitle:@"点击进行面容id解锁" forState:UIControlStateNormal];
        self.imgView.image = [UIImage imageNamed:@"set_faceid"];
    }else{
        self.navigationItem.title = @"TOUCH ID";
        [self.unlockTheFingerprintBtn setTitle:@"点击进行指纹解锁" forState:UIControlStateNormal];
        self.imgView.image = [UIImage imageNamed:@"set_touchid"];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)unlockTheFingerprintBtnClick:(id)sender
{
    [self unlockTheFingerprint];
    
}

- (IBAction)otherLoginType:(id)sender
{
    [[YKUserManager sharedUser] yk_loginOut];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismiss];
        [YKTouchIDTool yk_saveHiddenLoginCancelBtnByUser:YES];
        if(![[YKUserManager sharedUser] yk_isLogin] ){
            if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[LoginViewController class]]) {
                LoginViewController *login = [LoginViewController login];
                [self.keyVC presentViewController:login animated:YES completion:nil];
            }
        } else {
#ifdef DEBUG
            [[iToast makeText:@"您已登录"] show];
#endif
        }
    });
}
//MARK:开启指纹解锁
- (void)unlockTheFingerprint
{
    WEAK_SELF
    //验证指纹
    [[YKTouchIDRecognitionManager shareYKTouchIDRecognitionManager] yk_loadAuthentication];
    [[YKTouchIDRecognitionManager shareYKTouchIDRecognitionManager] setTouchIDBlock:^(NSDictionary *callBackDic) {
        dispatch_async(dispatch_get_main_queue(), ^{
            DLog(@"%@",callBackDic);
            if ([callBackDic[@"code"] integerValue] == 5 ) {
                //指纹验证成功
                [weakSelf dismiss];
            }else{
                [[QLAlert alert] showWithMessage:callBackDic[@"message"] singleBtnTitle:@"确定"];
            }
        });
    }];
}

- (void)yk_showInViewController:(UIViewController *)vc
{
    self.keyVC = vc;
    [vc presentViewController:self animated:YES completion:nil];
}
- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
