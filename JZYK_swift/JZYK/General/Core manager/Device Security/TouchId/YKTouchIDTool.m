//
//  DNTouchIDTool.m
//  dingniu
//
//  Created by zhaoying on 2018/4/11.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKTouchIDTool.h"
#import "QBGestureTool.h"
// 保存用户是否开启手势解锁的状态
static NSString * const kYKTouchIDEnableByUserKey = @"YKTouchIDEnableByUserKey";
static NSString * const kYKHiddenLoginCancelBtnByUserKey = @"YKHiddenLoginCancelBtnByUserKey";

@implementation YKTouchIDTool
// 用户是否开启指纹解锁
+ (void)yk_saveTouchIDByUser:(BOOL)isEnable
{
    
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gestureEnableByUserKey = [NSString stringWithFormat:@"%@%@",usrName,kYKTouchIDEnableByUserKey];
    [df setBool:isEnable forKey:gestureEnableByUserKey];
    [df synchronize];
}
+ (BOOL)yk_isTouchIDEnableByUser
{
    
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gestureEnableByUserKey = [NSString stringWithFormat:@"%@%@",usrName,kYKTouchIDEnableByUserKey];
    BOOL enable = [[df objectForKey:gestureEnableByUserKey] boolValue];
    return enable;
}

+ (void)yk_saveHiddenLoginCancelBtnByUser:(BOOL)isEnable
{
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gestureEnableByUserKey = [NSString stringWithFormat:@"%@%@",usrName,kYKHiddenLoginCancelBtnByUserKey];
    [df setBool:isEnable forKey:gestureEnableByUserKey];
    [df synchronize];
}
+ (BOOL)yk_isHiddenLoginCancelBtnByUser
{
    /*
     登录页面的返回按钮是否hidden，
     1、首先如果用户设置了手势密码，手势密码解锁错误的话，是要hidden
     2、如果用户设置了指纹解锁，指纹解锁失败的话，还是要hidden
     */
    BOOL isBackBtnHidden = NO;
    BOOL isGestureEnable = [QBGestureTool isGestureEnable];
    if ([YKTouchIDTool yk_isTouchIDEnableByUser]) {
        //如果开启了指纹解锁
        BOOL isTouchIDEnable = [self isTouchIDEnable];
        isBackBtnHidden = isGestureEnable || isTouchIDEnable;
    }else{
        isBackBtnHidden = isGestureEnable;
    }
    return isBackBtnHidden;
}
+ (BOOL)isTouchIDEnable
{
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gestureEnableByUserKey = [NSString stringWithFormat:@"%@%@",usrName,kYKHiddenLoginCancelBtnByUserKey] ;
    BOOL enable = [[df objectForKey:gestureEnableByUserKey] boolValue];
    return enable;
}
@end
