//
//  DNTouchIDRecognitionManager.m
//  dingniu
//
//  Created by zhaoying on 2018/4/11.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKTouchIDRecognitionManager.h"
#import<LocalAuthentication/LocalAuthentication.h>
/************************************ 指纹和面容识别相关 ************************************/
static NSString * const KTouchIdFingerprintIdentification = @"支持指纹识别";
static NSString * const KTouchIdFacialRecognition = @"支持面容识别";
static NSString * const KTouchIdFingerprintLocalizedReasonString = @"请按住Home键完成验证";
static NSString * const KTouchIdFacialLocalizedReasonString = @"请识别脸部完成验证";
static NSString * const KTouchIdFingerprintIdentificationIsNotSupported = @"您的设备不支持指纹识别";
static NSString * const KTouchIdFacialRecognitionFaileIsNotSupported = @"您的设备不支持面容识别";
static NSString * const KTouchIdFingerprintIdentificationSuccess = @"指纹识别成功";
static NSString * const KTouchIdFacialRecognitionSuccess = @"面容识别成功";
static NSString * const KTouchIdNOFingerprintIdentification = @"设备没有注册过指纹";
static NSString * const KTouchIdNOFacialRecognition = @"设备没有注册过面容";
static NSString * const KTouchIdFingerprintIdentificationFaile = @"指纹识别失败";
static NSString * const KTouchIdFacialRecognitionFaile = @"面容识别失败";
static NSString * const KTouchIdAuthorizationFailure = @"授权失败";
static NSString * const KTouchIdUserCancellationValidation = @"用户取消验证";
static NSString * const KTouchIdUserSelectsPassword = @"用户选择输入密码";
static NSString * const KTouchIdUserCancelAuthorization = @"取消授权";
static NSString * const KTouchIdLock = @"Touch ID被锁，需要您输入密码解锁";
static NSString * const KFaceIdLock = @"Face ID被锁，需要您输入密码解锁";
static NSString * const KTouchIdHang = @"用户不能控制情况下APP被挂起";

@implementation YKTouchIDRecognitionManager
SingletonM(YKTouchIDRecognitionManager)
/**
 监测指纹识别
 */
- (void)yk_checkTouchIDPrint
{
    LAContext *myContext = [[LAContext alloc] init];
    // 这个属性是设置指纹输入失败之后的弹出框的选项
    NSError *authError = nil;
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
    {
    
        NSDictionary *dic = @{
                              @"code" : @(TouchIDState_SUCCESSSUPPORT),
                              @"message" : IS_iPhoneX ? KTouchIdFacialRecognition :KTouchIdFingerprintIdentification
                              };
        [self runTouchIDStateBlockWith:dic];
        
    }else
    {
        NSDictionary * dic = [self CheckTouchIDStateBlockWith:authError message:@"" success:NO];
        [self runTouchIDStateBlockWith:dic];
    }
}
/**
 * 指纹登录验证
 */
- (void)yk_loadAuthentication
{
    LAContext *myContext = [[LAContext alloc] init];
    // 这个属性是设置指纹输入失败之后的弹出框的选项
//    myContext.localizedFallbackTitle = @"输入密码";
    NSError *authError = nil;
    NSString *myLocalizedReasonString = KTouchIdFingerprintLocalizedReasonString;
    if (IS_iPhoneX) {
        myLocalizedReasonString = KTouchIdFacialLocalizedReasonString;
    }
    // MARK: 判断设备是否支持指纹识别
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
    {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:myLocalizedReasonString reply:^(BOOL success, NSError * _Nullable error) {
            
            NSDictionary *dic = [self CheckTouchIDStateBlockWith:error message:@"" success:success];
            [self runTouchIDStateBlockWith:dic];
        }];
    }
    else
    {
        DLog(@"设备不支持指纹");
        NSDictionary *dic = @{
                              @"code" : @(TouchIDState_DEVICENOTSUPPORTED),
                              @"message" : IS_iPhoneX ? KTouchIdFacialRecognitionFaileIsNotSupported :KTouchIdFingerprintIdentificationIsNotSupported
                              };
        [self runTouchIDStateBlockWith:dic];
    }
}

- (NSDictionary *)CheckTouchIDStateBlockWith:(NSError *)error message:(NSString *)message success:(BOOL)success
{
    TouchIDRecognitionState touchIdState = TouchIDState_DEVICENOTSUPPORTED;
    if(success)
    {
        touchIdState = TouchIDState_FINGETSUCCESS;
        message = IS_iPhoneX ? KTouchIdFacialRecognitionSuccess : KTouchIdFingerprintIdentificationSuccess;
    }
    else
    {
        // 错误码
        switch (error.code)
        {
            case LAErrorAuthenticationFailed: // Authentication was not successful, because user failed to provide valid credentials
            {
                DLog(@"授权失败"); // -1 连续三次指纹识别错误
                touchIdState = TouchIDState_ERROR;
                message = KTouchIdAuthorizationFailure;
            }
                break;
            case LAErrorUserCancel: // Authentication was canceled by user (e.g. tapped Cancel button)
            {
                DLog(@"用户取消验证Touch ID"); // -2 在TouchID对话框中点击了取消按钮
                touchIdState = TouchIDState_ERROR;
                message = KTouchIdUserCancellationValidation;
            }
                break;
            case LAErrorUserFallback: // Authentication was canceled, because the user tapped the fallback button (Enter Password)
            {
                DLog(@"用户选择输入密码，切换主线程处理"); // -3 在TouchID对话框中点击了输入密码按钮
                touchIdState = TouchIDState_ChoosePassWord;
                message = KTouchIdUserSelectsPassword;
            }
                break;
            case LAErrorSystemCancel: // Authentication was canceled by system (e.g. another application went to foreground)
            {
                DLog(@"取消授权，如其他应用切入，用户自主"); // -4 TouchID对话框被系统取消，例如按下Home或者电源键
                touchIdState = TouchIDState_ERROR;
                message = KTouchIdUserCancelAuthorization;
            }
                break;
            case LAErrorPasscodeNotSet: // Authentication could not start, because passcode is not set on the device.
                
            {
                DLog(@"设备系统未设置密码"); // -5
                touchIdState = TouchIDState_DEVICENOTSAFEGUARD;
                message = @"设备未处于安全保护中";
            }
                break;
            case LAErrorTouchIDNotAvailable: // Authentication could not start, because Touch ID is not available on the device
            {
                DLog(@"设备未设置Touch ID"); // -6
                touchIdState = TouchIDState_DEVICENOTREGUIDFIN;
                message = IS_iPhoneX ? KTouchIdNOFacialRecognition : KTouchIdNOFingerprintIdentification;
            }
                break;
            case LAErrorTouchIDNotEnrolled: // Authentication could not start, because Touch ID has no enrolled fingers
            {
                DLog(@"用户未录入指纹"); // -7
                touchIdState = TouchIDState_DEVICENOTREGUIDFIN;
                message = IS_iPhoneX ? KTouchIdNOFacialRecognition : KTouchIdNOFingerprintIdentification;
            }
                break;
               
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_9_0
            case LAErrorTouchIDLockout: //Authentication was not successful, because there were too many failed Touch ID attempts and Touch ID is now locked. Passcode is required to unlock Touch ID, e.g. evaluating LAPolicyDeviceOwnerAuthenticationWithBiometrics will ask for passcode as a prerequisite 用户连续多次进行Touch ID验证失败，Touch ID被锁，需要用户输入密码解锁，先Touch ID验证密码
            {
                DLog(@"Touch ID被锁，需要用户输入密码解锁"); // -8 连续五次指纹识别错误，TouchID功能被锁定，下一次需要输入系统密码
                touchIdState = TouchIDState_ERROR;
                message = IS_iPhoneX ? KFaceIdLock : KTouchIdLock;
            }
                break;
            case LAErrorAppCancel: // Authentication was canceled by application (e.g. invalidate was called while authentication was in progress) 如突然来了电话，电话应用进入前台，APP被挂起啦");
            {
                DLog(@"用户不能控制情况下APP被挂起"); // -9
                touchIdState = TouchIDState_ERROR;
                message = KTouchIdHang;
            }
                break;
            case LAErrorInvalidContext: // LAContext passed to this call has been previously invalidated.
            {
                DLog(@"LAContext传递给这个调用之前已经失效"); // -10
                touchIdState = TouchIDState_ERROR;
                message = IS_iPhoneX ? KTouchIdFacialRecognitionFaile : KTouchIdFingerprintIdentificationFaile;
            }
                break;
#else
#endif
            default:
            {
                touchIdState = TouchIDState_ERROR;
                message = IS_iPhoneX ? KTouchIdFacialRecognitionFaile : KTouchIdFingerprintIdentificationFaile;
                break;
            }
        }
    }
    NSDictionary *dic = @{
                          @"code" : @(touchIdState),
                          @"message" : message,
                          };
    return dic;
}

/**
 执行回调
 */
- (void)runTouchIDStateBlockWith:(NSDictionary *)dic
{
    
    if (self.touchIDBlock) {
        self.touchIDBlock(dic);
    }
}

@end
