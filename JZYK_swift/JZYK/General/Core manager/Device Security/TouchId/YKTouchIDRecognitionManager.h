//
//  DNTouchIDRecognitionManager.h
//  dingniu
//
//  Created by zhaoying on 2018/4/11.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TouchIDRecognitionState) {
    
    TouchIDState_DEVICENOTSUPPORTED = 1, //设备不支持
    TouchIDState_DEVICENOTSAFEGUARD = 2, //设备未处于安全保护中
    TouchIDState_DEVICENOTREGUIDFIN = 3,//设备没有注册过指纹
    TouchIDState_SUCCESSSUPPORT = 4,//支持指纹识别
    TouchIDState_FINGETSUCCESS = 5,//指纹识别成功
    TouchIDState_ERROR = 6,//指纹识别失败
    TouchIDState_ChoosePassWord = 7,//选择输入密码
};
typedef void(^TouchIDRecognitionManagerBlock)(NSDictionary *callBackDic);
@interface YKTouchIDRecognitionManager : NSObject
SingletonH(YKTouchIDRecognitionManager)

/**
 回调 包含状态以及提示信息
 */
@property (nonatomic, copy) TouchIDRecognitionManagerBlock touchIDBlock;

/**
 * 指纹登录验证
 */
- (void)yk_loadAuthentication;

/**
 监测指纹识别
 */
- (void)yk_checkTouchIDPrint;

@end
