//
//  QBGestureScreen.h
//  1022
//
//  Created by ZhaoYing on 2017/11/8.
//  Copyright © 2017年 com.ulife. All rights reserved.
//

#import <Foundation/Foundation.h>
@class QBGestureScreen;
@protocol QBGestureScreenDelegate <NSObject>

- (void)screen:(QBGestureScreen *)screen didSetup:(NSString *)psw;
@end
@interface QBGestureScreen : NSObject

+ (instancetype)shared;
- (void)show;
- (void)dismiss;
@end



