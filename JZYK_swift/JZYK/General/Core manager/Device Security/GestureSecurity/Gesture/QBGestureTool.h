//
//  QBGestureTool.h
//  1022
//
//  Created by ZhaoYing on 2017/11/8.
//  Copyright © 2017年 com.ulife. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QBGestureTool : NSObject
// 用户是否开启手势解锁
+ (void)saveGestureEnableByUser:(BOOL)isEnable;
+ (BOOL)isGestureEnableByUser;

// 保存 读取用户设置的密码
+ (void)saveGesturePsw:(NSString *)psw;
+ (NSString *)getGesturePsw;
+ (void)deleteGesturePsw;
+ (BOOL)isGesturePswEqualToSaved:(NSString *)psw;

+ (BOOL)isGestureEnable;
+ (BOOL)isGesturePswSavedByUser;

+ (void)saveGestureResetEnableByTouchID:(BOOL)enable;
+ (BOOL)isGestureResetEnableByTouchID;


+ (void)saveGestureErrorTimes:(NSInteger)errorTimes;
+ (NSInteger)getGestureErrorTimes;
+ (void)deleteGestureErrorTimes;

@end
