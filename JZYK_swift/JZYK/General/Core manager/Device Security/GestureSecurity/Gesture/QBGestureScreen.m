//
//  QBGestureScreen.m
//  1022
//
//  Created by ZhaoYing on 2017/11/8.
//  Copyright © 2017年 com.ulife. All rights reserved.
//

#import "QBGestureScreen.h"
#import "QBGestureViewController.h"
@interface QBGestureScreen ()<QBGestureViewDelegate>
{
    QBGestureViewController *_gestureVC;
}
@property (nonatomic, strong) UIWindow *window;
@end

@implementation QBGestureScreen

+ (instancetype)shared {
    
    static dispatch_once_t onceToken;
    static QBGestureScreen *share = nil;
    dispatch_once(&onceToken, ^{
        
        share = [[self alloc]init];
    });
    
    return share;
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        _window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        _window.windowLevel = UIWindowLevelStatusBar;
        
        UIViewController *vc = [[UIViewController alloc]init];
        _window.rootViewController = vc;
    }
    
    return self;
}

- (void)show {
    
    self.window.hidden = NO;
    [self.window makeKeyWindow];
    self.window.windowLevel = UIWindowLevelStatusBar;
    
    
    QBGestureViewController *viV = [[QBGestureViewController alloc]init];
    viV.delegate = self;
    [viV showInViewController:self.window.rootViewController type:QBGestureTypeScreen];
    _gestureVC = viV;
}

- (void)dismiss {
    
    [self.window.rootViewController dismissViewControllerAnimated:YES completion:^{
        
        [self.window resignKeyWindow];
        self.window.windowLevel = UIWindowLevelNormal;
        self.window.hidden = YES;
    }];
}

- (void)dealloc {
    if (self.window) {
        self.window = nil;
    }
}

- (void)gestureViewVerifiedSuccess:(QBGestureViewController *)vc {
    
    [self performSelector:@selector(hide) withObject:nil afterDelay:0.6];
    
}

- (void)hide {
    [self.window resignKeyWindow];
    self.window.windowLevel = UIWindowLevelNormal;
    self.window.hidden = YES;
}


@end
