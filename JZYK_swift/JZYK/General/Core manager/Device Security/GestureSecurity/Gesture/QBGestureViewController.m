//
//  QBGestureViewController.m
//  1022
//
//  Created by ZhaoYing on 2017/11/8.
//  Copyright © 2017年 com.ulife. All rights reserved.
//
#define QBSCREEN_HEIGHT [[UIScreen mainScreen]bounds].size.height
#define QBSCREEN_WIDTH [[UIScreen mainScreen]bounds].size.width
#define QBNaigationBarHeight 64

#import "QBGestureViewController.h"
#import "QBCircleView.h"
#import "QBWarnLabel.h"
#import "QBCircleViewConst.h"
#import "QBGestureTool.h"
#import "QBGestureEntity.h"
#import "QBCircleBtn.h"
#import "YKDeviceSecurity.h"

static NSString *firstGesturePsw;

@interface QBGestureViewController ()<CircleViewDelegate>
{
    BOOL _isHiddenNavigationBarWhenDisappear;//记录当页面消失时是否需要隐藏系统导航
    BOOL _isHasTabBarController;//是否含有tabbar
    BOOL _isHasNavitationController;//是否含有导航
    
    // 用于区分是设置密码,还是更新密码
    BOOL isShowDefaultImage;
}
@property (nonatomic, strong) QBCircleView *lockView;
@property (nonatomic, strong) QBWarnLabel *msgLabel;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIImageView *showImgView;
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIButton *forgetButton;
@property (nonatomic, strong) UIButton *otherButton;
//@property (nonatomic,strong)  UILabel *timeLabel;
//@property (nonatomic,strong)  UILabel *infoLabel;
@property (nonatomic,assign) NSInteger lastErrorTimes;
@property (nonatomic,strong) QBGestureEntity *gestureEntity;

@end

@interface QBGestureViewController ()

@end

@implementation QBGestureViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_isHasNavitationController == YES) {
        if (self.navigationController.navigationBarHidden == YES) {
            _isHiddenNavigationBarWhenDisappear = NO;
        } else {
            self.navigationController.navigationBarHidden = YES;
            _isHiddenNavigationBarWhenDisappear = YES;
        }
    }
    // 进来先清空存的第一个密码
    [QBCircleViewConst saveGesture:nil Key:gestureOneSaveKey];
//    [self loadHandPasswordData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(verifyGesturePassword)name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)verifyGesturePassword
{
//    [self loadHandPasswordData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (_isHiddenNavigationBarWhenDisappear == YES) {
        self.navigationController.navigationBarHidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WHITE_COLOR;
    [self buildUI];
//    WEAK_SELF
//    [YKDeviceSecurity verifyTouchID:^{
//        [weakSelf dismiss];
//    }];
}
//- (void)loadHandPasswordData
// {
//     [[HTTPManager session] postRequestForKey:kUserHandPasswordGetUserInfo showLoading:NO param:@{} succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL unwrap, NSString * _Nonnull msg) {
//         if (code == 0) {
//             [self updataGestureUI:json];
//         } else {
//             [[QLAlert alert] showWithTitle:@"" message:msg btnTitleArray:@[@"确认"] btnClicked:^(NSInteger index) {}];
//         }
//     } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
//
//     }];
// }
//- (void)updataGestureUI:(NSDictionary*)data
//{
//    if (![data objectForKey:@"data"] || ![[data objectForKey:@"data"] isKindOfClass:[NSDictionary class]]) {
//        return;
//    }
//    self.gestureEntity = [QBGestureEntity yy_modelWithDictionary:[data objectForKey:@"data"]];
//
//    NSString *timeStr = [NSString stringWithFormat:@"%@ %@.",self.gestureEntity.day,self.gestureEntity.month] ;
//    [self showTimeLabel:timeStr];
//
//    self.infoLabel.text = self.gestureEntity.msg;
//     if (self.gestureEntity.is_birthday == 1){
//        self.infoLabel.textColor = [UIColor yk_colorWithHexString:@"#ff3d55"];
//    }
//    self.lockView.isBirthday = self.gestureEntity.is_birthday;
//
//    if (self.type == QBGestureTypeVerifying) {
//        [self.showImgView sd_setImageWithURL:[NSURL URLWithString:self.gestureEntity.avatar] placeholderImage:[UIImage imageNamed:@"gesture_header_icon"]];
//    }
//
//}
//- (void)showTimeLabel:(NSString*)timeStr
//{
//    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:timeStr];
//    [AttributedStr addAttribute:NSFontAttributeName
//                          value:[UIFont systemFontOfSize:adaptFontSize(30)]
//                          range:NSMakeRange(0, 2)];
//    if (self.gestureEntity.month.length == 2) {
//        [AttributedStr addAttribute:NSFontAttributeName
//                              value:[UIFont systemFontOfSize:adaptFontSize(16)]
//                              range:NSMakeRange(timeStr.length - 3, 3)];
//    }else if (self.gestureEntity.month.length == 3){
//        [AttributedStr addAttribute:NSFontAttributeName
//                              value:[UIFont systemFontOfSize:adaptFontSize(16)]
//                              range:NSMakeRange(timeStr.length - 4, 4)];
//    }
//    self.timeLabel.attributedText = AttributedStr;
//}
- (void)buildUI
{
    WEAK_SELF
//    [self.view addSubview:self.timeLabel];
//    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.offset(60 *ASPECT_RATIO_WIDTH);
//        make.left.offset(15 *ASPECT_RATIO_WIDTH);
//        make.width.mas_greaterThanOrEqualTo(100 *ASPECT_RATIO_WIDTH);
//        make.height.offset(25 *ASPECT_RATIO_WIDTH);
//    }];
//    [self getTheTimeOFcurrentSystem];
//
//    self.infoLabel.text = @"信用是您最好的财富";
//    [self.view addSubview:self.infoLabel];
//    [self.infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        STRONG_SELF
//        make.top.equalTo(strongSelf.timeLabel.mas_bottom).offset(10 *ASPECT_RATIO_WIDTH);
//        make.left.offset(15 *ASPECT_RATIO_WIDTH);
//        make.right.offset(-15 *ASPECT_RATIO_WIDTH);
//        make.height.mas_lessThanOrEqualTo(40 *ASPECT_RATIO_WIDTH);
//    }];
    
    [self.view addSubview:self.showImgView];
    [self.showImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        STRONG_SELF
        make.centerX.equalTo(strongSelf.view);
        make.top.offset(IS_iPhoneX? 112 *ASPECT_RATIO_WIDTH:88 *ASPECT_RATIO_WIDTH);
        make.width.height.offset(60 *ASPECT_RATIO_WIDTH);
    }];
    [self.view addSubview:self.msgLabel];
    [self.msgLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        STRONG_SELF
        make.top.equalTo(strongSelf.showImgView.mas_bottom).offset(12 *ASPECT_RATIO_WIDTH);
        make.left.offset(15 *ASPECT_RATIO_WIDTH);
        make.right.offset(-15 *ASPECT_RATIO_WIDTH);
        make.height.offset(16);
    }];
    
    [self.view addSubview:self.lockView];
    

    if (self.type == QBGestureTypeSetting) {
        [self addCancelBtn];
    } else {
        [self addForgetPswBtnAndOtherLoginModes];
    }
}
////获取当前系统的时间,并转化成“10 十一月.”
//-(void)getTheTimeOFcurrentSystem
//{
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_Hans"];
//    formatter.locale = locale;
//    [formatter setDateFormat:@"MM-dd"];
//    NSString *dateString       = [formatter stringFromDate: [NSDate date]];
//    NSArray * array = [dateString componentsSeparatedByString:@"-"];
//    NSString *month = [array firstObject];
//    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
//    numberFormatter.numberStyle = kCFNumberFormatterRoundHalfDown;
//    month = [NSString stringWithFormat:@"%@月",[numberFormatter stringFromNumber:[NSNumber numberWithInteger:[month integerValue]]]];
//    NSString *day = [array lastObject];
//    NSString *timeStr = [NSString stringWithFormat:@"%@ %@.",day,month];
//    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:timeStr];
//    [AttributedStr addAttribute:NSFontAttributeName
//                          value:[UIFont systemFontOfSize:adaptFontSize(30)]
//                          range:NSMakeRange(0, 2)];
//    if (month.length == 2) {
//        [AttributedStr addAttribute:NSFontAttributeName
//                              value:[UIFont systemFontOfSize:adaptFontSize(16)]
//                              range:NSMakeRange(timeStr.length - 3, 3)];
//    }else if (month.length == 3){
//        [AttributedStr addAttribute:NSFontAttributeName
//                              value:[UIFont systemFontOfSize:adaptFontSize(16)]
//                              range:NSMakeRange(timeStr.length - 4, 4)];
//    }
//    self.timeLabel.attributedText = AttributedStr;
//}

- (void)addCancelBtn
{
    WEAK_SELF
    [self.view addSubview:self.cancelButton];
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        STRONG_SELF
        make.bottom.equalTo(strongSelf.view).offset(IS_iPhoneX ? -72 *ASPECT_RATIO_WIDTH : -38 *ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(strongSelf.view);
        make.width.offset(40 *ASPECT_RATIO_WIDTH);
        make.height.offset(30 *ASPECT_RATIO_WIDTH);
    }];
}

- (void)addForgetPswBtnAndOtherLoginModes
{
    WEAK_SELF
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor yk_colorWithHexString:@"#cccccc"];
    [self.view addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        STRONG_SELF
        make.width.offset(1 *ASPECT_RATIO_WIDTH);
        make.height.offset(15 *ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(strongSelf.view);
        make.bottom.offset(-45 *ASPECT_RATIO_WIDTH);
    }];
    
    [self.view addSubview:self.forgetButton];
    [self.forgetButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lineView.mas_left).offset(-15 *ASPECT_RATIO_WIDTH);
        make.width.offset(150 *ASPECT_RATIO_WIDTH);
        make.height.offset(30 *ASPECT_RATIO_WIDTH);
        make.bottom.offset(-40 *ASPECT_RATIO_WIDTH);
    }];
    [self.forgetButton setTitleEdgeInsets:UIEdgeInsetsMake(0,0,0,-15*ASPECT_RATIO_WIDTH)];
    
    [self.view addSubview:self.otherButton];
    [self.otherButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lineView.mas_right).offset(15 *ASPECT_RATIO_WIDTH);
        make.width.offset(150 *ASPECT_RATIO_WIDTH);
        make.height.offset(30 *ASPECT_RATIO_WIDTH);
        make.bottom.offset(-40 *ASPECT_RATIO_WIDTH);
    }];
    
}
- (void)cancelButtonClock:(UIButton *)button {
    [self back];
    if (self.delegate && [self.delegate respondsToSelector:@selector(gestureViewCanceled:)]) {
        [self.delegate gestureViewCanceled:self];
    }
}

- (void)dismiss {
    
    [self back];
}
- (void)back {
    
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)delayBack {
    
    dispatch_time_t dalayTime = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC*0.4);
    dispatch_queue_t queueDelay = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_after(dalayTime, queueDelay, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self back];
        });
    });
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelButton setTitleColor:[Color color_66_C3] forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(cancelButtonClock:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (QBCircleView *)lockView {
    if (_lockView == nil) {
        _lockView = [[QBCircleView alloc]init];
        _lockView.delegate = self;
    }
    return _lockView;
}

- (QBWarnLabel *)msgLabel {
    if (_msgLabel == nil) {
        _msgLabel = [[QBWarnLabel alloc]init];
        [self.view addSubview:_msgLabel];
    }
    return _msgLabel;
}

- (UIImageView *)showImgView {
    if (!_showImgView) {
        _showImgView = [[UIImageView alloc]init];
        _showImgView.image = [UIImage imageNamed:@"mine_gesture"];
    }
    return _showImgView;
}

- (UIButton *)forgetButton {
    if (!_forgetButton) {
        _forgetButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _forgetButton.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        [_forgetButton setTitle:@"忘记手势密码?" forState:UIControlStateNormal];
        [_forgetButton setTitleColor:[Color color_66_C3] forState:UIControlStateNormal];
        [_forgetButton addTarget:self action:@selector(forgetButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forgetButton;
}

//- (UILabel *)timeLabel
//{
//    if (!_timeLabel) {
//        _timeLabel = [[UILabel alloc]init];
//        if (@available(iOS 8.2, *)) {
//            _timeLabel.font = [UIFont systemFontOfSize:adaptFontSize(30) weight:UIFontWeightMedium];
//        } else {
//            _timeLabel.font = [UIFont systemFontOfSize:adaptFontSize(30)];
//        }
//        _timeLabel.textColor = LABEL_TEXT_COLOR;
//    }
//    return _timeLabel;
//}
//- (UILabel *)infoLabel
//{
//    if (!_infoLabel) {
//        _infoLabel = [[UILabel alloc] init];
//        _infoLabel.textColor = GRAY_COLOR_AD;
//        _infoLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
//        _infoLabel.numberOfLines = 0;
//    }
//    return _infoLabel;
//}

- (void)forgetButtonClick
{
    QLAlert *alert = [QLAlert alert];
    [alert prepareUIWithTitle:@"手势密码已关闭,请重新登录" contentText:@"在个人中心-设置，可重新开启手势密码" buttonTitleArray:@[@"确定"] buttonClicked:^(NSInteger index) {
        if (index == 0) {
            [self dismissViewControllerAnimated:NO completion:nil];
            [QBGestureTool deleteGesturePsw];
            [QBGestureTool saveGestureEnableByUser:NO];
            [self otherButtonClick];
        }
    }];
    [alert show];
}

- (void)loginOut
{
    [[YKUserManager sharedUser] yk_loginOut];
}

- (UIButton *)otherButton
{
    if (!_otherButton) {
        _otherButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _otherButton.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        [_otherButton setTitle:@"使用其他账号登录" forState:UIControlStateNormal];
        [_otherButton setTitleColor:[Color color_66_C3] forState:UIControlStateNormal];
        [_otherButton addTarget:self action:@selector(otherButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _otherButton;
}

- (void)otherButtonClick
{
    [self loginOut];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismiss];
        if(![[YKUserManager sharedUser] yk_isLogin] ){
            if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[LoginViewController class]]) {
                LoginViewController *login = [LoginViewController login];
                [self.rootViewController presentViewController:login animated:YES completion:nil];
            }
        }
    });
}

#pragma mark 自己的方法
- (void)showInViewController:(UIViewController *)vc type:(QBGestureType)type
{
    self.type = type;
    [vc presentViewController:self animated:YES completion:nil];
    switch (type) {
        case QBGestureTypeSetting:
            [self gestureSetting];
            break;
        case QBGestureTypeVerifying:
            [self gestureVerity];
            break;
        case QBGestureTypeUpdate:
            [self gertureUpdate];
            break;
        case QBGestureTypeScreen:
            [self gestureScreen];
            break;
            
        default:
            break;
    }
    
}
- (void)gestureSetting
{
    self.titleLab.text = @"设置手势密码";
    [self.lockView setType:CircleViewTypeSetting];
    [self.msgLabel showNormalMsg:gestureTextBeforeSet]; //绘制解锁图案
}

- (void)gestureVerity
{
    self.titleLab.text = @"验证手势密码";
    [self.lockView setType:(CircleViewTypeLogin)];
    self.msgLabel.textColor = GRAY_COLOR_45;
    self.msgLabel.text = [NSString stringWithFormat:@"您好！%@",[YKTools yk_phoneSecurityString:[[YKUserManager sharedUser] username]]];
    self.showImgView.image =  [UIImage imageNamed:@"mineIcon"];
}

- (void)gertureUpdate
{
    self.titleLab.text = @"修改手势密码";
    [self.lockView setType:CircleViewTypeVerify];
    [self.msgLabel showNormalMsg:gestureTextOldGesture];
}

- (void)gestureScreen
{
    [self.lockView setType:CircleViewTypeLogin];
    [self.msgLabel showNormalMsg:gestureTextBeforeSet];
//    self.showImgView.image = [UIImage imageNamed:@"gesture_header_icon"];
}

#pragma mark - 设置手势密码代理方法
/**
 *  连线个数少于4个时，通知代理
 *
 *  @param view    circleView
 *  @param type    type
 *  @param gesture 手势结果
 */
- (void)circleView:(QBCircleView *)view type:(CircleViewType)type connectCirclesLessThanNeedWithGesture:(NSString *)gesture {
    
    NSString *gestureOne = [QBCircleViewConst getGestureWithKey:gestureOneSaveKey];
    
    // 看是否存在第一个密码
    if (gestureOne.length > 0) {
        [self.msgLabel showWarnMsgAndShake:gestureTextDrawAgainError];
        
    } else {
        
        [self.msgLabel showWarnMsgAndShake:gestureTextConnectLess];
    }
}

/**
 *  连线个数多于或等于4个，获取到第一个手势密码时通知代理
 *
 *  @param view    circleView
 *  @param type    type
 *  @param gesture 第一个次保存的密码
 */
- (void)circleView:(QBCircleView *)view type:(CircleViewType)type didCompleteSetFirstGesture:(NSString *)gesture {
    
    [self.msgLabel showWarnMsg:gestureTextDrawAgain];
    self.msgLabel.textColor  = [Color color_45_C2];
}

/**
 *  获取到第二个手势密码时通知代理
 *
 *  @param view    circleView
 *  @param type    type
 *  @param gesture 第二次手势密码
 *  @param equal   第二次和第一次获得的手势密码匹配结果
 */
- (void)circleView:(QBCircleView *)view type:(CircleViewType)type didCompleteSetSecondGesture:(NSString *)gesture result:(BOOL)equal {
    
    if (equal) {
        
        [self.msgLabel showSuccessMsg];
        [QBCircleViewConst saveGesture:gesture Key:gestureFinalSaveKey];
        [self delayBack];
        
        if (self.type == QBGestureTypeUpdate) {
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(gestureView:didUpdate:)]) {
                
                [self.delegate gestureView:self didUpdate:gesture];
            }
        } else {
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(gestureView:didSetted:)]) {
                
                [self.delegate gestureView:self didSetted:gesture];
            }
        }
        
    } else {
        
        [self.msgLabel showWarnMsgAndShake:gestureTextDrawAgainError];
        
    }
}

#pragma mark - 登录手势密码代理方法
/**
 *  登陆或者验证手势密码输入完成时的代理方法
 *
 *  @param view    circleView
 *  @param type    type
 *  @param gesture 登陆时的手势密码
 */
- (void)circleView:(QBCircleView *)view type:(CircleViewType)type didCompleteLoginGesture:(NSString *)gesture result:(BOOL)equal {
    
    
    // 此时的type有两种情况 Login or verify
    if (type == CircleViewTypeLogin) {
        if (equal) {
            [self back];
            if (self.delegate && [self.delegate respondsToSelector:@selector(gestureViewVerifiedSuccess:)]) {
                
                [self.delegate gestureViewVerifiedSuccess:self];
            }
        } else {
            self.lastErrorTimes ++;
            if (self.lastErrorTimes >= 5) {
                [self otherButtonClick];
                return;
            }
            NSString * errorStr = [NSString stringWithFormat:@"手势密码错误，还可以输入%ld次",(5 - self.lastErrorTimes)];
            [self.msgLabel showWarnMsgAndShake:errorStr];
        }
    } else if (type == CircleViewTypeVerify) {
        
        if (equal) {
            
            [self.lockView setType:CircleViewTypeSetting];
            [self.msgLabel showNormalMsg:gestureTextBeforeSet];
//            self.showImgView.image = [UIImage imageWithContentsOfFile:gestureDefaultImagePath];
            isShowDefaultImage = YES;
        } else {
            
            [self.msgLabel showWarnMsgAndShake:gestureTextGestureVerifyError];
        }
    }
}

- (void)cicleView:(QBCircleView *)view type:(CircleViewType)type didEndGesture:(UIImage *)image {
    
    if (type == CircleViewTypeSetting) {
        
        if (self.type == QBGestureTypeScreen) {
            
            return;
        }
        
        if (isShowDefaultImage) {
            isShowDefaultImage = NO;
//            self.showImgView.image = [UIImage imageWithContentsOfFile:gestureDefaultImagePath];
        } else {
            
//            self.showImgView.image = image;
        }
        
    }
}

@end
