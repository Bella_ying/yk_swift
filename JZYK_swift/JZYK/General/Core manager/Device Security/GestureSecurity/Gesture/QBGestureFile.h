//
//  QBGestureFile.h
//  1022
//
//  Created by ZhaoYing on 2017/11/8.
//  Copyright © 2017年 com.ulife. All rights reserved.
//

#ifndef QBGestureFile_h
#define QBGestureFile_h
// 保存用户是否开启手势解锁的状态
#define kQBGestureEnableByUserKey  @"QBGestureEnableByUserKey"

// 保存 用户设置的手势密码
#define  kQBGesturePsw @"QBGesturePswIsSectted"

#endif /* QBGestureFile_h */
