//
//  QBGestureViewController.h
//  1022
//
//  Created by ZhaoYing on 2017/11/8.
//  Copyright © 2017年 com.ulife. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QBGestureViewController;
@protocol QBGestureViewDelegate <NSObject>

@optional
- (void)gestureView:(QBGestureViewController *)vc didSetted:(NSString *)psw ;
- (void)gestureView:(QBGestureViewController *)vc didUpdate:(NSString *)psw ;
- (void)gestureViewVerifiedSuccess:(QBGestureViewController *)vc ;
- (void)gestureViewCanceled:(QBGestureViewController *)vc ;

@end
typedef NS_ENUM(NSInteger, QBGestureType) {
    
    QBGestureTypeSetting = 0,
    QBGestureTypeVerifying,
    QBGestureTypeUpdate,
    QBGestureTypeScreen,
};
@interface QBGestureViewController : UIViewController
@property (nonatomic, assign) id <QBGestureViewDelegate> delegate;
@property (nonatomic, assign) QBGestureType type;
@property (nonatomic,strong) UIViewController *rootViewController;

- (void)showInViewController:(UIViewController *)vc type:(QBGestureType)type ;

- (void)dismiss;
@end
