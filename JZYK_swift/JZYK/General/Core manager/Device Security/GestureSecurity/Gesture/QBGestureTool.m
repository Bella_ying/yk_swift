//
//  QBGestureTool.m
//  1022
//
//  Created by ZhaoYing on 2017/11/8.
//  Copyright © 2017年 com.ulife. All rights reserved.
//

#import "QBGestureTool.h"
#import "QBGestureFile.h"
@implementation QBGestureTool
// 用户是否开启手势解锁
+ (void)saveGestureEnableByUser:(BOOL)isEnable {
    
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gestureEnableByUserKey = NSStringFormat(@"%@%@",usrName,kQBGestureEnableByUserKey);
    [df setBool:isEnable forKey:gestureEnableByUserKey];
}

+ (BOOL)isGestureEnableByUser
{
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gestureEnableByUserKey = NSStringFormat(@"%@%@",usrName,kQBGestureEnableByUserKey);
    BOOL enable = [[df objectForKey:gestureEnableByUserKey] boolValue];

    return enable;
}

// 保存 读取用户设置的密码
+ (void)saveGesturePsw:(NSString *)psw
{
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gesturePsw = NSStringFormat(@"%@%@",usrName,kQBGesturePsw);
    [df setObject:psw forKey:gesturePsw];
}

+ (NSString *)getGesturePsw
{
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gesturePsw = NSStringFormat(@"%@%@",usrName,kQBGesturePsw);
    NSString *psw = [df objectForKey:gesturePsw];
    return psw;
}

+ (BOOL)isGesturePswSavedByUser
{
    NSString *psw = [self getGesturePsw];

    return (psw && psw.length > 0);
}

+ (void)deleteGesturePsw
{
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gesturePsw = NSStringFormat(@"%@%@",usrName,kQBGesturePsw);
    [df setObject:@"" forKey:gesturePsw];
}

+ (BOOL)isGesturePswEqualToSaved:(NSString *)psw
{
    if (!psw || psw.length <= 0) {
        return NO;
    }

    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *gesturePsw = NSStringFormat(@"%@%@",usrName,kQBGesturePsw);
    NSString *str = [df objectForKey:gesturePsw];

    if (!str || str.length <= 0) {
        return NO;
    }

    if ([psw isEqualToString:str]) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL)isGestureEnable
{
    BOOL isEnableByUser = [self isGestureEnableByUser];
    NSString *psw = [self getGesturePsw];
    BOOL enable = isEnableByUser && psw && psw.length > 0 ;
    return enable;
}

+ (void)saveGestureResetEnableByTouchID:(BOOL)enable
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    [us setBool:enable forKey:@"isCanUseTouchIDToResetGesture"];
}

+ (BOOL)isGestureResetEnableByTouchID
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    
    return [[us objectForKey:@"isCanUseTouchIDToResetGesture"] boolValue];
}

+ (void)saveGestureErrorTimes:(NSInteger)errorTimes
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *errorTimesKey = NSStringFormat(@"%@errorTmies",usrName);
    [us setInteger:errorTimes forKey:errorTimesKey];
}

+ (NSInteger)getGestureErrorTimes
{
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *errorTimesKey = NSStringFormat(@"%@errorTmies",usrName);
    NSInteger errorTimes = [df integerForKey:errorTimesKey];
    return errorTimes;
}

+ (void)deleteGestureErrorTimes
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    NSString *usrName = [[YKUserManager sharedUser] username];
    NSString *errorTimesKey = NSStringFormat(@"%@errorTmies",usrName);
    [us setInteger:0 forKey:errorTimesKey];
}
@end
