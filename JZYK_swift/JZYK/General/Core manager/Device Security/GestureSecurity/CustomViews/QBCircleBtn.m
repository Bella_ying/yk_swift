//
//  PCCircleBtn.m
//  LZAccount
//
//  Created by zhaoying on 2017/11/7.
//  Copyright © 2017年 Artup. All rights reserved.
//

#import "QBCircleBtn.h"
#import "QBCircleViewConst.h"

@implementation QBCircleBtn

- (instancetype)init
{
    if (self = [super init]) {

        self.userInteractionEnabled = NO;
    }
    return self;
}
- (void)drawRect:(CGRect)rect
{
    switch (self.btnState) {
        case CircleStateNormal:
            [self setImage:[UIImage imageNamed:@"mine_gesture_normal"] forState:UIControlStateNormal];
            break;
        case CircleStateSelected:
        {
            if (self.type == CircleTypeDefult) {
                [self setImage:[UIImage imageNamed:@"mine_gesture_selected"] forState:UIControlStateNormal];
            }else if (self.type == CircleTypeBrithday){
                [self setImage:[UIImage imageNamed:@"mine_gesture_ birthday"] forState:UIControlStateNormal];
            }
        }
            break;
        case CircleStateError:

            [self setImage:[UIImage imageNamed:@"mine_gesture_error"] forState:UIControlStateNormal];
            break;
        case CircleStateLastOneSelected:
        {
            if (self.type == CircleTypeDefult) {
                [self setImage:[UIImage imageNamed:@"mine_gesture_selected"] forState:UIControlStateNormal];
            }else if (self.type == CircleTypeBrithday){
                [self setImage:[UIImage imageNamed:@"mine_gesture_ birthday"] forState:UIControlStateNormal];
            }
        }
            break;
        case CircleStateLastOneError:
            [self setImage:[UIImage imageNamed:@"mine_gesture_error"] forState:UIControlStateNormal];
            break;
        default:
             [self setImage:[UIImage imageNamed:@"mine_gesture_normal"] forState:UIControlStateNormal];
            break;
    }
    
}
/**
 *  重写angle的setter
 */
- (void)setAngle:(CGFloat)angle
{
    _angle = angle;
    
    [self setNeedsDisplay];
}

/**
 *  重写state Setter
 */
- (void)setBtnState:(CircleState)btnState
{
    _btnState  = btnState;
    
    [self setNeedsDisplay];
}
@end
