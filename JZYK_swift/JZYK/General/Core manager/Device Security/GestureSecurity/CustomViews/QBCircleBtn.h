//
//  PCCircleBtn.h
//  LZAccount
//
//  Created by zhaoying on 2017/11/7.
//  Copyright © 2017年 Artup. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  单个圆的各种状态
 */
typedef enum{
    CircleStateNormal = 1,
    CircleStateSelected,
    CircleStateError,
    CircleStateLastOneSelected,
    CircleStateLastOneError
}CircleState;

/**
 *  选中状态展示的图片
 CircleTypeDefult ，正常
 CircleTypeBrithday ，生日
 */
typedef enum
{
    CircleTypeDefult = 1,
    CircleTypeBrithday
}CircleType;

@interface QBCircleBtn : UIButton

/**
 *  所处的状态
 */
@property (nonatomic, assign) CircleState btnState;

/**
 *  类型
 */
@property (nonatomic, assign) CircleType type;


//
///** 角度 */
@property (nonatomic,assign) CGFloat angle;

@end
