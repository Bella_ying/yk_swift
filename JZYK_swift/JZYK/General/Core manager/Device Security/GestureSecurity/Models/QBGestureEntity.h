//
//  QBGestureEntity.h
//  KDFDApp
//
//  Created by zhaoying on 2017/11/10.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QBGestureEntity : NSObject
//日期，几号
@property (nonatomic, copy) NSString *day;
//日期，月份
@property (nonatomic, copy) NSString *month;
//当天是否是用户生日，0不是，1是
@property (nonatomic, assign) NSInteger is_birthday;
//文案
@property (nonatomic, copy) NSString *msg;
//头像是男还是女
@property (nonatomic, copy) NSString *avatar;

@end
