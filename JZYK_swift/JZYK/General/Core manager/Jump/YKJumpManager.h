//
//  YKJumpManager.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JumpModel : NSObject

@property (nonatomic, copy) NSString *skip_code;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *data;//installment_type 的值
@property (nonatomic,copy) void(^bindBankCompletion)(void);

@end

@interface YKJumpManager : NSObject

SingletonH(YKJumpManager)

- (void)yk_jumpWithParamUrl:(NSString *)url;

- (void)yk_jumpWithParamDic:(NSDictionary *)dic;

- (void)yk_jumpWithParamModel:(JumpModel *)model ;

/**
 * 如果是H5调用原生登录页面，那么先回到tabbar控制器，然后再调登录，为了让H5重新进入
 */
- (void)yk_jumpWithParamDic:(NSDictionary *)dic fromH5:(BOOL)isFromH5;

- (void)yk_jumpWithParamModel:(JumpModel *)model fromH5:(BOOL)isFromH5;

@end
