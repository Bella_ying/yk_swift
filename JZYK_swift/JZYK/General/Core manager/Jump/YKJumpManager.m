//
//  YKJumpManager.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKJumpManager.h"
#import "YKOrderListVC.h"
#import "YKDiscountTicketVC.h"
#import "YKVerifyCenterListVC.h"
#import "YKVerifyGuideViewController.h"
#import "YKBankCardListVC.h"
#import "YKAddBankCardViewController.h"
#import "YKAmountManageVC.h"
#import "YKNeedRepayBillVC.h"
#import "YKContactsViewController.h"
#import "YKMallViewController.h"
#import "QiYuManager.h"
#import "YKMallDetailViewController.h"
#import "YKMallShowViewController.h"
#import "YKAccountRemainingSumVC.h"

@implementation JumpModel

@end

@implementation YKJumpManager

SingletonM(YKJumpManager)

- (void)yk_jumpWithParamUrl:(NSString *)url
{
    [self yk_jumpWithParamDic:[url urlParam] fromH5:NO];
}

- (void)yk_jumpWithParamDic:(NSDictionary *)dic
{
    [self yk_jumpWithParamModel:[JumpModel yy_modelWithDictionary:dic] fromH5:NO];
}

- (void)yk_jumpWithParamDic:(NSDictionary *)dic fromH5:(BOOL)isFromH5{
    [self yk_jumpWithParamModel:[JumpModel yy_modelWithDictionary:dic] fromH5:isFromH5];
}

- (void)yk_jumpWithParamModel:(JumpModel *)model{
    [self yk_jumpWithParamModel:model fromH5:NO];
}

/*
 _HOME = "101";//借款首页
 _REPAY = "102";//取现
 _ACCOUNT = "103";//我的
 _COUPON = "104";//优惠券
 _REDPACK = "105";//现金红包
 _LOGIN = "106";//登录
 _CERTIFICATION_CENTER = "107";//认证中心
 _WEB  = "108"//跳转到h5
 _FeedBack = "110" //意见反馈
 _FeedBack = "111" //催收投诉
 _customerService = “112” //七鱼客服
 _VerifyGuidance  = "113" //5步认证向导
 _SetTradingPasswordVC = “114” //设置交易密码
            115_index  //商城首页,index表示的是要在
            116  //商品列表页
            117  //订单列表页
            120  //我的银行卡
            109  //绑卡
            119 //添加联系人
            121  //我的额度
            122 商品详情
            123 商城配置活动界面
            124 待还账单
            125 借款详情页
            126 找回交易密码
            127 账户余额

 */
- (void)yk_jumpWithParamModel:(JumpModel *)model fromH5:(BOOL)isFromH5
{
    dispatch_async(dispatch_get_main_queue(), ^{
        YKTabBarController *tabVc = [YKTabBarController yk_shareTabController];
        YKUserManager *user = [YKUserManager sharedUser];
        if (!user.yk_isLogin) {//如果未登录，且登录页挂起
            [tabVc dismissViewControllerAnimated:NO completion:nil];
        }
        NSString *installmentTypekey = @"";
        //从首页中点击“充值卡券”进来，model.skip_code = 115_czx
        if ([model.skip_code containsString:@"_"]) {
            NSArray *array = [model.skip_code componentsSeparatedByString:@"_"];
            if (array && [array isKindOfClass:[NSArray class]] && array.count == 2) {
                model.skip_code = array[0];
                installmentTypekey = array[1];
            }
        }
        switch ([model.skip_code integerValue]) {
                
            case 101:  //借款首页
            {
                [tabVc yk_setSelectedIndex:YKTabSelectedIndexHome viewController:nil];
            }
                break;
                
/****************此处需要先验证登录，不登录直接穿透到登录展示。弹出登录页*******************************/
                
            case 102:   //取现
            {
                [tabVc yk_setSelectedIndex:YKTabSelectedIndexWithDraw viewController:nil];
                break;
            }
                
            case 103:   //我的
            {
                [tabVc yk_setSelectedIndex:YKTabSelectedIndexMine viewController:nil];
                break;
            }
                
            case 104:   //优惠券
            {
                if (user.yk_isLogin) {
                    [tabVc yk_setSelectedIndex:YKTabSelectedIndexMine viewController:[YKDiscountTicketVC new]];
                    break;
                }
            }
                
            case 105:  //现金红包
            {
                if (user.yk_isLogin) {
                    break;
                }
            }
                
            case 107:  //认证列表
            {
                if (user.yk_isLogin) {
                    [tabVc yk_pushToViewController:[YKVerifyCenterListVC new]];
                    break;
                }
            }
            case 110:  //意见反馈
            {
                if (user.yk_isLogin) {
                    break;
                }
            }
            case 111: //催收投诉
            {
                if (user.yk_isLogin) {
                    break;
                }
            }
            case 112: //七鱼客服
            {
                if (user.yk_isLogin) {
                    [QiYuManager yk_sharedQiYuManager].commonQuestionTemplateId = [[ConfigManager config] getQiyuTemplateIDForKey:model.url ? model.url : @"index"];
                    [[QiYuManager yk_sharedQiYuManager] yk_qiYuService];
                    [[QiYuManager yk_sharedQiYuManager] yk_qiYuDelegate];
                    break;
                }
            }
            case 113:  //5步认证向导
            {
                if (user.yk_isLogin) {
                    [tabVc yk_pushToViewController:[YKVerifyGuideViewController new]];
                    break;
                }
            }
            case 114:  //设置交易密码
            {
                if (user.yk_isLogin) {
                    TransactionCodeController * vc = [TransactionCodeController new];
                    vc.typeNum = 1;
                    [tabVc yk_pushToViewController:vc];
                    break;
                }
            }
            case 117:  //订单列表页
            {
                if (user.yk_isLogin) {
                    [tabVc yk_setSelectedIndex:YKTabSelectedIndexMine viewController:[YKOrderListVC new]];
                    break;
                }
            }
            case 120: //我的银行卡
            {
                if (user.yk_isLogin) {
                    [tabVc yk_setSelectedIndex:YKTabSelectedIndexMine viewController:[YKBankCardListVC new]];
                    break;
                }
            }
            case 109: //绑卡
            {
                if (user.yk_isLogin) {
                    YKAddBankCardViewController *view =  [YKAddBankCardViewController new];
                    [view setBankBindComplete:^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
                        if (model.bindBankCompletion) {
                            model.bindBankCompletion();
                        }
                    }];
                    [tabVc yk_pushToViewController:[YKAddBankCardViewController new]];
                    break;
                }
            }
            case 121: //我的额度
            {
                if (user.yk_isLogin) {
                    [tabVc yk_setSelectedIndex:YKTabSelectedIndexMine viewController:[YKAmountManageVC new]];
                    break;
                }
            }
            case 119: //添加联系人
            {
                if (user.yk_isLogin) {
                    [tabVc yk_pushToViewController:[YKContactsViewController new]];
                    break;
                }
            }
            case 124: //待还账单
            {
                if (user.yk_isLogin) {
                    [tabVc yk_pushToViewController:[YKNeedRepayBillVC new]];
                    break;
                }
            }
            case 125: //借款详情
            {
                if (user.yk_isLogin) {
                    [tabVc yk_popToViewController:@"YKLoanApplyViewController"];
                    break;
                }
            }
            case 126: //找回交易密码
            {
                if (user.yk_isLogin) {
                    YKBaseViewController *baseVC = [tabVc yk_getCurrentViewController];
                    __weak typeof(baseVC) weakBaseVC = baseVC;
                    [baseVC findTransactionCodeToController:^(YKBaseViewController * _Nonnull findVC) {
                        [weakBaseVC.navigationController pushViewController:findVC animated:YES];
                    }];
                    break;
                }
            }
            case 127: //账户余额
            {
                if (user.yk_isLogin) {
                    [tabVc yk_pushToViewController:[[YKAccountRemainingSumVC alloc] init]];
                    break;
                }
            }
            case 106: //登录
            {
                if (!user.yk_isLogin) {
                    if (isFromH5) {
                        YKBaseViewController * vc = [tabVc yk_getCurrentViewController];
                        [vc.navigationController popToRootViewControllerAnimated:NO];
                        YKTabBarController *tabVc = [YKTabBarController yk_shareTabController];
                        [tabVc presentViewController:[LoginViewController login] animated:YES completion:nil];
                        
                    }else{
                        YKBaseViewController * vc = [tabVc yk_getCurrentViewController];
                        [vc presentViewController:[LoginViewController login] animated:YES completion:nil];
                    }
                }
            }
                break;
                
            case 108: //H5跳转
            {
                if (!model.url || [model.url isEqualToString:@""]) {
                    return;
                }
                YKBrowseWebController *web = [[YKBrowseWebController alloc] init];
                web.url = model.url;
                [tabVc yk_pushToViewController:web];
            }
                break;
            case 115:  //商城首页
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [YK_NOTIFICATION_CENTER postNotificationName:YK_MALL_INSTALLMENT_TYPE_KEY object:@{@"installment_type":installmentTypekey}];
                });
                [tabVc yk_setSelectedIndex:YKTabSelectedIndexMall viewController:nil];
            }
                 break;
            case 116:  //商城列表页
            {
                
                    [tabVc yk_setSelectedIndex:YKTabSelectedIndexMall viewController:nil];
            }
             break;
                
            case 122: // 商品详情
            {
                YKMallDetailViewController *goodsInfoVc = [YKMallDetailViewController new];
                goodsInfoVc.shopList_id = [model.url integerValue];
                [tabVc yk_pushToViewController:goodsInfoVc];
            }
                break;
            case 123:  //商城活动页
            {
                if (model.data.length) {
                    YKMallShowViewController *showVC = [YKMallShowViewController new];
                    showVC.installment_type = model.data;
                    [tabVc yk_pushToViewController:showVC];
                }
            }
                break;
            default:
            {
                if (!model.url || [model.url isEqualToString:@""]) {
                    return;
                }
                YKBrowseWebController *web = [[YKBrowseWebController alloc] init];
                web.url = model.url;
                [tabVc yk_pushToViewController:web];
            }
                break;
        }
    });
}


@end
