//
//  YKTabbarItem.m
//  youka
//
//  Created by hongyu on 2018/2/8.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKTabbarItem.h"

@implementation YKTabbarItem

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
    self.backgroundColor = [UIColor clearColor];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.titleLabel sizeToFit];
    CGSize titleSize = self.titleLabel.frame.size;
    CGSize imageSize = [self imageForState:UIControlStateNormal].size;
    if (imageSize.width != 0 && imageSize.height != 0)
    {
        CGFloat imageCenterY = CGRectGetHeight(self.frame) - 3 - titleSize.height - imageSize.height / 2 - 5;
        if (self.titleLabel.text.length) {
            self.imageView.center = CGPointMake(CGRectGetWidth(self.frame) / 2, imageCenterY);
        } else {
            self.imageView.center = CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame) - imageSize.height);
        }
    } else {
        CGPoint imageViewCenter  = self.imageView.center;
        imageViewCenter.x        = CGRectGetWidth(self.frame) / 2;
        imageViewCenter.y        = (CGRectGetHeight(self.frame) - titleSize.height) / 2;
        self.imageView.center    = imageViewCenter;
    }
    CGPoint labelCenter = CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame) - 3 - titleSize.height / 2);
    self.titleLabel.center = labelCenter;
}

#pragma mark- 复写系统的高量实现，禁止按钮高亮 此函数里不用写任何代码
- (void)setHighlighted:(BOOL)highlighted
{
    
}

@end
