//
//  YKTabBarModel.h
//  JZYK
//
//  Created by hongyu on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKTabBarModel : NSObject

@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *sel_image;
@property (nonatomic, copy) NSString *skip_code;
@property (nonatomic, copy) NSString *tag;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *url;

+ (YKTabBarModel *)yk_shareTabBarModel;

@end
