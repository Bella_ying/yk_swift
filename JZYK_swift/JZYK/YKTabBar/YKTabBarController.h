//
//  YKTabBarController.h
//  youka
//
//  Created by hongyu on 05/02/2018.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKTabBar.h"

typedef NS_ENUM(NSInteger, YKTabSelectedIndex) {
    YKTabSelectedIndexHome     = 0,
    YKTabSelectedIndexMall     = 1,
    YKTabSelectedIndexWithDraw = 2,
    YKTabSelectedIndexMine     = 3
};
@interface YKTabBarController : UITabBarController

@property (nonatomic, strong) YKTabBar *ykTabBar;
//@property (nonatomic, assign) NSInteger verifyGuideStatus;

+ (instancetype)yk_shareTabController;

/**
 获取当前显示的视图控制器
 */
- (id)yk_getCurrentViewController;
/**
 跳转到任意视图控制器
 */
- (void)yk_pushToViewController:(UIViewController *)vc;
/**
 返回到指定栈控制器
 */
- (void)yk_popToViewController:(NSString *)vcName;
/**
 跳转到指定TabBar
 */
- (void)yk_jumpTabBarIndex:(YKTabSelectedIndex)index;
/**
 动态更新tabBar icon title
 */
- (void)yk_refreshTabBarIconWithTitle;

- (void)yk_setSelectedIndex:(YKTabSelectedIndex)selectedIndex viewController:(UIViewController *)controller;

@end
