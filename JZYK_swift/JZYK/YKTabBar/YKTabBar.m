//
//  YKTabBar.m
//  youka
//
//  Created by hongyu on 2018/2/8.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKTabBar.h"
#import "YKTabbarItem.h"

@implementation YKTabBar

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [WHITE_COLOR colorWithAlphaComponent:0.8];
        // 打开阴影效果
        [YKTools yk_prepareShadowWithView:self
                              shadowColor:[UIColor yk_colorWithHexString:@"6B6B6B"]
                             shadowOffset:CGSizeMake(0, -2)
                            shadowOpacity:.1
                             shadowRadius:2];
    }
    return self;
}

- (void)setSelectedIndex:(NSInteger)index
{
    for (YKTabbarItem *item in self.tabBarItems) {
        if (item.tag == index) {
            item.selected = YES;
        } else {
            item.selected = NO;
        }
    }
    UIWindow *keyWindow = [[[UIApplication sharedApplication] delegate] window];
    UITabBarController *tabarController = (UITabBarController *)keyWindow.rootViewController;
    if (tabarController) {
        tabarController.selectedIndex = index;
    }
}

- (void)yk_selectedItemWithIndex:(NSInteger)index
{
    for (YKTabbarItem *item in self.tabBarItems) {
        if ([item isEqual:self.tabBarItems[index]]) {
            item.selected = YES;
        } else {
            item.selected = NO;
        }
    }
}

#pragma mark- Setter
- (void)setTabBarItems:(NSArray *)tabBarItems
{
    _tabBarItems = tabBarItems;
    NSInteger itemTag = 0;
    for (id item in tabBarItems) {
        if ([item isKindOfClass:[YKTabbarItem class]]) {
            if (itemTag == 0) {
                ((YKTabbarItem *)item).selected = YES;
            }
            [((YKTabbarItem *)item) addTarget:self action:@selector(itemSelected:) forControlEvents:UIControlEventTouchDown];
            [self addSubview:item];
            if (((YKTabbarItem *)item).tabBarItemType != YKTabBarItemTypeRise) {
                ((YKTabbarItem *)item).tag = itemTag;
                itemTag++;
            }
        }
    }
}

#pragma mark - Touch Event
- (void)itemSelected:(YKTabbarItem *)sender
{
    if (sender.tabBarItemType != YKTabBarItemTypeRise) {
        [self setSelectedIndex:sender.tag];
        if (self.delegate) {
            [self.delegate yk_tabBarSelectedWithIndex:sender.tag];
        }
    } else {
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(yk_tabBarDidSelectedRiseButton)]) {
                [self.delegate yk_tabBarDidSelectedRiseButton];
            }
        }
    }
}

@end
