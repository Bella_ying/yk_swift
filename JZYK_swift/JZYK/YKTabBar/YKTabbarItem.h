//
//  YKTabbarItem.h
//  youka
//
//  Created by hongyu on 2018/2/8.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, YKTabBarItemType) {
    YKTabBarItemTypeNormal = 0,
    YKTabBarItemTypeRise,
};

@interface YKTabbarItem : UIButton

@property (nonatomic, assign) YKTabBarItemType tabBarItemType;

@end
