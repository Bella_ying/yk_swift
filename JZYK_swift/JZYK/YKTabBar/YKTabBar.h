//
//  YKTabBar.h
//  youka
//
//  Created by hongyu on 2018/2/8.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YKTabBarDelegate <NSObject>

- (void)yk_tabBarDidSelectedRiseButton;
- (void)yk_tabBarSelectedWithIndex:(NSInteger)index;

@end

@interface YKTabBar : UIView

@property (nonatomic, copy) NSArray *tabBarItems;
@property (nonatomic, weak) id <YKTabBarDelegate> delegate;

- (void)yk_selectedItemWithIndex:(NSInteger)index;

@end
