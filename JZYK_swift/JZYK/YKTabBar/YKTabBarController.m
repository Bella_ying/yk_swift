//
//  YKTabBarController.m
//  youka
//
//  Created by hongyu on 05/02/2018.
//  Copyright © 2018 JSQB. All rights reserved.
//

#import "YKTabBarController.h"
#import "YKTabbarItem.h"
#import "YKHomeViewController.h"
#import "YKMallViewController.h"
#import "YKWithDrawViewController.h"
#import "YKMineViewController.h"
#import "YKNavigationController.h"
#import "YKTabBarModel.h"

static const CGFloat tabBarTitleFont = 11;

@interface YKTabBarController ()<UITabBarControllerDelegate, YKTabBarDelegate>

@property (nonatomic, strong) YKTabbarItem   *homeItem;
@property (nonatomic, strong) YKTabbarItem   *mallItem;
@property (nonatomic, strong) YKTabbarItem   *withDrawItem;
@property (nonatomic, strong) YKTabbarItem   *mineItem;
@property (nonatomic, strong) NSMutableArray *itemModelArray;

@end

@implementation YKTabBarController

+ (instancetype)yk_shareTabController
{
    static dispatch_once_t onceToken;
    static YKTabBarController *tabBarController;
    dispatch_once(&onceToken, ^{
        tabBarController = [YKTabBarController new];
    });
    return tabBarController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareTabBarControllers];
    self.delegate = self;
}

- (void)prepareTabBarControllers
{
    YKHomeViewController *homeVc         = [[YKHomeViewController alloc] init];
    YKMallViewController *mallVc         = [[YKMallViewController alloc] init];
    YKWithDrawViewController *withDrawVc = [[YKWithDrawViewController alloc] init];
    YKMineViewController *mineVc         = [[YKMineViewController alloc] init];
    
    YKNavigationController *homeNav     = [[YKNavigationController alloc] initWithRootViewController:homeVc];
    YKNavigationController *mallNav     = [[YKNavigationController alloc] initWithRootViewController:mallVc];
    mallNav.isHaveLineView = YES;
    
    YKNavigationController *withDrawNav = [[YKNavigationController alloc] initWithRootViewController:withDrawVc];
    YKNavigationController *mineNav     = [[YKNavigationController alloc] initWithRootViewController:mineVc];
   
    
    self.viewControllers = @[homeNav, mallNav, withDrawNav, mineNav];
    self.ykTabBar = [[YKTabBar alloc] initWithFrame:self.tabBar.bounds];
    self.ykTabBar.delegate = self;
    NSInteger tabBarNum = 4;
    CGFloat normalBarItemWidth = WIDTH_OF_SCREEN / tabBarNum;
    CGFloat tabBarHeight       = CGRectGetHeight(self.ykTabBar.frame);
    
    self.homeItem = [self tabBarItemWithFrame:CGRectMake(0, 0, normalBarItemWidth, tabBarHeight)
                                        title:@"首页"
                              normalImageName:@"home_tabBar_n"
                            selectedImageName:@"home_tabBar_s"
                               tabBarItemType:YKTabBarItemTypeNormal];
    
    self.mallItem = [self tabBarItemWithFrame:CGRectMake(normalBarItemWidth, 0, normalBarItemWidth, tabBarHeight)
                                        title:@"商城"
                              normalImageName:@"shop_tabBar_n"
                            selectedImageName:@"shop_tabBar_s"
                               tabBarItemType:YKTabBarItemTypeNormal];
    
    self.withDrawItem = [self tabBarItemWithFrame:CGRectMake(normalBarItemWidth * 2, 0, normalBarItemWidth, tabBarHeight)
                                            title:@"提现"
                                  normalImageName:@"cash_tabBar_n"
                                selectedImageName:@"cash_tabBar_s"
                                   tabBarItemType:YKTabBarItemTypeNormal];
    
    self.mineItem = [self tabBarItemWithFrame:CGRectMake(normalBarItemWidth * 3, 0, normalBarItemWidth, tabBarHeight)
                                        title:@"我的"
                              normalImageName:@"mine_tabBar_n"
                            selectedImageName:@"mine_tabBar_s"
                               tabBarItemType:YKTabBarItemTypeNormal];
    
    self.ykTabBar.tabBarItems = @[self.homeItem, self.mallItem, self.withDrawItem, self.mineItem];
    self.tabBar.translucent = IS_iPhoneX ? NO : YES;
    UIBlurEffect *blurEffect       = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *visualView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, TABBAR_HEIGHT);
    [self.tabBar addSubview:visualView];
    [self.tabBar addSubview:self.ykTabBar];
    
    [[UITabBar appearance] setShadowImage:[UIImage new]];
    [[UITabBar appearance] setBackgroundImage:[UIImage new]];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    // 检测是否需要更新tabBar
    [self yk_refreshTabBarIconWithTitle];
}



- (YKTabbarItem *)tabBarItemWithFrame:(CGRect)frame
                                title:(NSString *)title
                      normalImageName:(NSString *)normalImageName
                    selectedImageName:(NSString *)selectedImageName
                       tabBarItemType:(YKTabBarItemType)tabBarItemType
{
    YKTabbarItem *item = [[YKTabbarItem alloc] initWithFrame:frame];
    [item setTitle:title forState:UIControlStateNormal];
    [item setTitle:title forState:UIControlStateSelected];
    item.titleLabel.font   = [UIFont systemFontOfSize:tabBarTitleFont];
    UIImage *normalImage   = [UIImage imageNamed:normalImageName];
    UIImage *selectedImage = [UIImage imageNamed:selectedImageName];
    [item setImage:normalImage forState:UIControlStateNormal];
    [item setImage:selectedImage forState:UIControlStateSelected];
    [item setImage:selectedImage forState:UIControlStateHighlighted];
    [item setTitleColor:GRAY_COLOR_AD  forState:UIControlStateNormal];
    [item setTitleColor:BLACK_COLOR_33 forState:UIControlStateSelected];
    item.tabBarItemType = YKTabBarItemTypeNormal;
    return item;
}

- (id)yk_getCurrentViewController
{
    YKTabBarController *tabBar  = (YKTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    YKNavigationController *nav = (YKNavigationController *)tabBar.viewControllers[tabBar.selectedIndex];
    return [nav.viewControllers objectAtIndex:[nav.viewControllers count] - 1];
}

- (void)yk_pushToViewController:(UIViewController *)vc
{
    YKTabBarController *tabBar  = (YKTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    YKNavigationController *nav = (YKNavigationController *)tabBar.viewControllers[tabBar.selectedIndex];
    vc.hidesBottomBarWhenPushed = YES;
    [nav pushViewController:vc animated:YES];
}

- (void)yk_popToViewController:(NSString *)vcName
{
    YKTabBarController *tabBar  = (YKTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    YKNavigationController *nav = (YKNavigationController *)tabBar.viewControllers[tabBar.selectedIndex];
    for (UIViewController *vc in nav.viewControllers) {
        if ([vc isKindOfClass:[NSClassFromString(vcName) class]]) {
            [nav popToViewController:vc animated:YES];
        }
    }
}

- (void)yk_jumpTabBarIndex:(YKTabSelectedIndex)index
{
    YKTabBarController *tabBar  = (YKTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    YKNavigationController *nav = (YKNavigationController *)tabBar.viewControllers[tabBar.selectedIndex];
    nav.tabBarController.selectedIndex = index;
    [self.ykTabBar yk_selectedItemWithIndex:index];
}

//tab跳转，再进行nav push 跳转
- (void)yk_setSelectedIndex:(YKTabSelectedIndex)selectedIndex viewController:(UIViewController *)controller
{
    if (selectedIndex != self.selectedIndex) {
        UIViewController *navController = self.viewControllers[self.selectedIndex];
        if ([navController respondsToSelector:@selector(popToRootViewControllerAnimated:)]) {
            [(YKNavigationController *)navController popToRootViewControllerAnimated:NO];
        }
        if (selectedIndex >= YKTabSelectedIndexHome && selectedIndex <= YKTabSelectedIndexMine) {
            self.selectedIndex = selectedIndex;
            [self.ykTabBar yk_selectedItemWithIndex:selectedIndex];
        }
        
    } else if (!controller) {
        UIViewController *navController = self.viewControllers[self.selectedIndex];
        if ([navController respondsToSelector:@selector(popToRootViewControllerAnimated:)]) {
            [(YKNavigationController *)navController popToRootViewControllerAnimated:NO];
        }
    }
    if (controller) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self yk_pushToViewController:controller];
        });
    }
}

- (void)yk_refreshTabBarIconWithTitle
{
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kCreditTabBarList showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString * _Nonnull msg) {
        NSInteger mark = [json[@"mark"] integerValue];
        if ([YK_NSUSER_DEFAULT integerForKey:@"is_upDate_tabBar"]) {
            // 本地有更新mark数值，进行对比如果相同直接取出本地数据
            NSInteger oldMark = [YK_NSUSER_DEFAULT integerForKey:@"is_upDate_tabBar"];
            if (mark == oldMark) {
                NSArray *array = [YKTools yk_getDataToDocumentPathWithFileName:@"tabBarModelArray"];
                if (array.count) {
                    [weakSelf prepareTabBarWithData:array];
                }
            } else {
                // 不同缓存mark数值并且进行新数据
                [YK_NSUSER_DEFAULT setInteger:mark forKey:@"is_upDate_tabBar"];
                [weakSelf prepareTabBarWithData:json[@"item"]];
                [YKTools yk_writeToDocumentPathWithData:json[@"item"] fileName:@"tabBarModelArray"];
            }
        } else {
            // 本地没有需要存储更新mark数值，每次数值不同就更新tabBar & 并把数据缓存本地
            [YK_NSUSER_DEFAULT setInteger:mark forKey:@"is_upDate_tabBar"];
            [weakSelf prepareTabBarWithData:json[@"item"]];
            [YKTools yk_writeToDocumentPathWithData:json[@"item"] fileName:@"tabBarModelArray"];
        }
    } failure:^(NSString * _Nonnull msg, BOOL isLink) {
        
    }];
}

- (void)prepareTabBarWithData:(NSArray *)data
{
    for (NSDictionary *dic in data) {
        YKTabBarModel *model = [YKTabBarModel yy_modelWithDictionary:dic];
        [self.itemModelArray addObject:model];
    }
    if (self.itemModelArray.count) {
        for (YKTabBarModel *model in self.itemModelArray) {
            if ([model.tag integerValue] == 1) {
                [self prepareTabBarItem:self.homeItem title:model.title icon:model.image selIcon:model.sel_image];
            }
            if ([model.tag integerValue] == 2) {
                [self prepareTabBarItem:self.mallItem title:model.title icon:model.image selIcon:model.sel_image];
            }
            if ([model.tag integerValue] == 3) {
                [self prepareTabBarItem:self.withDrawItem title:model.title icon:model.image selIcon:model.sel_image];
            }
            if ([model.tag integerValue] == 4) {
                [self prepareTabBarItem:self.mineItem title:model.title icon:model.image selIcon:model.sel_image];
            }
        }
    }
}

- (void)prepareTabBarItem:(YKTabbarItem *)item title:(NSString *)title icon:(NSString *)icon selIcon:(NSString *)selIcon
{
    [item setTitle:title forState:UIControlStateNormal];
    [item setTitle:title forState:UIControlStateSelected];
    [item sd_setImageWithURL:[NSURL URLWithString:icon] forState:UIControlStateNormal];
    [item sd_setImageWithURL:[NSURL URLWithString:selIcon] forState:UIControlStateSelected];
    [item sd_setImageWithURL:[NSURL URLWithString:selIcon] forState:UIControlStateHighlighted];
    [item setTitleColor:GRAY_COLOR_AD  forState:UIControlStateNormal];
    [item setTitleColor:BLACK_COLOR_33 forState:UIControlStateSelected];
}

#pragma mark--DNTabBarDelegate
- (void)yk_tabBarDidSelectedRiseButton
{
    // 处理自定义tabBar图标点击方法
}

- (void)yk_tabBarSelectedWithIndex:(NSInteger)index
{
    // 点击tabBar响应方法
    
}

#pragma mark UITabBarControllerDelegate methods
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    YKNavigationController *nav = (YKNavigationController *)viewController;
    /*
     登录态逻辑，从首页获得登录状态后，清空登录信息，然后根据isLogin判断是否登录;
     如果没有登录只能访问第一个TAB信息，点击其余tab会弹出登录状态失效，并弹出登录页
     */
    if([nav.viewControllers.firstObject isKindOfClass:[YKWithDrawViewController class]]
       || [nav.viewControllers.firstObject isKindOfClass:[YKMineViewController class]]
       ){
        if(![[YKUserManager sharedUser] yk_isLogin]){
            [YKPageManager yk_presentLoginPageAndReturn];
            return NO;
        }
    }
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    
}

- (NSMutableArray *)itemModelArray
{
    if (!_itemModelArray) {
        _itemModelArray = [@[] mutableCopy];
    }
    return _itemModelArray;
}

@end
