//
//  YKAppStarter.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKAppStarter.h"
#import "YKLocationManager.h"
#import <Bugtags/Bugtags.h>
#import <UMMobClick/MobClick.h>
#import <UShareUI/UShareUI.h>
#import "YKPushManager.h"
#import "YKCrashHandler.h"
#import <QYSDK.h>

@interface YKAppStarter ()


@end

@implementation YKAppStarter

+ (void)yk_regist3rdServiceWithOptions:(NSDictionary *)launchOptions
{
    //定位
    [YKLocationManager yk_registAmapServices];
    //Bug统计
    [Bugtags startWithAppKey:kBugTagsAppKey invocationEvent:BTGInvocationEventNone];
    //埋点
    [YKAppStarter initUMMobClick];
    //分享
    [YKAppStarter initUmenShare];
    //推送
    [[YKPushManager shareYKPushManager] yk_initJpushServiceWithOptions:launchOptions];
    //七鱼
    [[QYSDK sharedSDK] registerAppId:kQiYuAppKey appName:appDisplayname()];
    
    [self customizeWebviewUserAgent];
}

+ (void)customizeWebviewUserAgent
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self yk_generateUserAgent];
    });
}

+ (NSString *)yk_generateUserAgent
{
    NSString* userAgent = [[[UIWebView alloc] initWithFrame:CGRectZero] stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    NSString *bundle = [DeviceInfo bundleID];
    if ([[DeviceInfo bundleID] containsString:@"ent"]) {
        bundle = @"com.DaBaiMall.JZYK";
    }
    userAgent = [NSString stringWithFormat:UA_FORMAT_STRING,userAgent,bundle,[QBProject yk_currentProject].companyName,[QBProject yk_currentProject].shortName,[DeviceInfo appVersion]];
    [[NSUserDefaults standardUserDefaults] setObject:userAgent forKey:kUserAgentResult];
    NSLog(@"userAgent ---------- %@",userAgent);
    return userAgent;
}

//Mark: UM埋点统计
+ (void)initUMMobClick
{
    //------UMeng初始化
    UMConfigInstance.appKey = kUmengAppKey;
    UMConfigInstance.ePolicy = BATCH;
    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！
    [MobClick setLogEnabled:YES];
}

//Mark: 初始化Umen分享
+ (void)initUmenShare
{
    //umeng分享注册SDK
    [[UMSocialManager defaultManager] setUmSocialAppkey:kUmengAppKey];
#ifdef DEBUG
    [[UMSocialManager defaultManager] openLog:YES];
#endif
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession
                                          appKey:kWxAppKey
                                       appSecret:kWxAppSecret
                                     redirectURL:@"http://mobile.umeng.com/social"];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ
                                          appKey:kQQAppID
                                       appSecret:nil
                                     redirectURL:@"http://mobile.umeng.com/social"];
}

+ (BOOL)yk_handleShareOpenURL:(NSURL *)url
{
    return [[UMSocialManager defaultManager] handleOpenURL:url];
}

+ (void)yk_installCrashHandlerUnderDebug
{
#ifdef DEBUG
    dispatch_async(dispatch_get_main_queue(), ^{
        InstallCrashExceptionHandler();
    });
#endif
}

@end
