//
//  YKAppStarter.h
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/6.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKAppStarter : NSObject

/**
 注册第三方服务
 */
+ (void)yk_regist3rdServiceWithOptions:(NSDictionary *)launchOptions;

/**
 *  获得从sso或者web端回调到本app的回调
 *
 *  @param url 第三方sdk的打开本app的回调的url
 *
 *  @return 是否处理  YES代表处理成功，NO代表不处理
 */
+ (BOOL)yk_handleShareOpenURL:(NSURL *)url;

/**
 在debug环境下安装crash handler
 */
+ (void)yk_installCrashHandlerUnderDebug;

+ (NSString *)yk_generateUserAgent;

@end
