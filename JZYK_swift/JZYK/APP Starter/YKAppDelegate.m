//
//  YKAppDelegate.m
//  JZYK
//
//  Created by Jeremy Wang on 2018/6/4.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

#import "YKAppDelegate.h"
#import "YKTabBarController.h"
#import "YKAppStarter.h"
#import "YKPushManager.h"
#import "YKJumpManager.h"
#import "YKGuidePageViewController.h"
#import "YKDeviceSecurity.h"

@interface YKAppDelegate()

@end

@implementation YKAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [YKAppStarter yk_regist3rdServiceWithOptions:launchOptions];
    [YKAppStarter yk_installCrashHandlerUnderDebug];
    [self prepareGuideView];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    return YES;
}

- (void)prepareGuideView
{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = [UIColor whiteColor];
    WEAK_SELF
    if (![YK_NSUSER_DEFAULT boolForKey:@"ykFirstStart"]) {
        YKGuidePageViewController *guideVc = [YKGuidePageViewController startVcWithImageArray:IS_iPhoneX ? @[@"welcome_iphone_x_one", @"welcome_iphone_x_two", @"welcome_iphone_x_three"] : @[@"welcome_iphone_one", @"welcome_iphone_two", @"welcome_iphone_three"] enterBlock:^{
            [YK_NSUSER_DEFAULT setBool:YES forKey:@"ykFirstStart"];
            [weakSelf setupAppRootViewController];
        }];
        self.window.rootViewController = guideVc;
    } else {
        [self setupAppRootViewController];
    }
}

- (void)setupAppRootViewController
{
#ifdef DEBUG
    DebugView *debugView = [DebugView debug];
    debugView.chooseComplete = ^{
        [self prepareTabBarRootVc];
    };
    YKBaseViewController *vc = [[YKBaseViewController alloc]init];
    vc.isStatusLight = YES;
    [vc.view addSubview:debugView];
    self.window.rootViewController = vc;
#else
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(){
        //release环境直接拉取线上接口
        [[ConfigManager config] getConfigWithURL:kReleaseURL];
    });
   [self prepareTabBarRootVc];
#endif
   
}
- (void)prepareTabBarRootVc
{
    YKTabBarController *tabBar = [YKTabBarController yk_shareTabController];
    self.window.rootViewController = tabBar;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[InterfaceReport defaultReport] reportInfoWithTimeStringReport:YES];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 推送回调函数
// 向苹果注册推送，在调用 registerForRemoteNotificationTypes “成功”后会触发这个回调函数
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [[YKPushManager shareYKPushManager] yk_registerDeviceToken:deviceToken];
}

- (void)handleRemoteNotification:(NSDictionary * _Nonnull)userInfo {
    NSString *msgID = [userInfo[@"_j_msgid"] stringValue];
    DLog(@"msgID == %@",msgID);
    if (![msgID isEqualToString:[YKPushManager shareYKPushManager].messageID]) {
        [[YKPushManager shareYKPushManager] yk_handleRemoteNotification:userInfo];
        [YKPushManager shareYKPushManager].messageID = msgID;
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler{
    DLog(@"收到通知: user info:%@",userInfo);
    [self handleRemoteNotification:userInfo];
    
    completionHandler(UIBackgroundFetchResultNewData);
}

#pragma mark -iOS 10 以前的推送系统处理方法
// 消息推送: 消息推送时应用正在运行，会进入到这里，
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    DLog(@"didReceiveRemoteNotification is %@",userInfo);
    [self handleRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    DLog(@"didReceiveLocalNotification received message is %@",notification);
    [[YKPushManager shareYKPushManager] yk_showLocalNotification:notification];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    BOOL result = [YKAppStarter yk_handleShareOpenURL:url];
    if (!result) {
        NSString * UrlStr = [url absoluteString];
        if ([UrlStr hasPrefix:[[QBProject yk_currentProject] scheme_key]]){//短信拉起浏览器后跳转
            [[YKJumpManager shareYKJumpManager] yk_jumpWithParamUrl:UrlStr];
        }
    }
    return YES;
}

@end
