//
//  YKMallListModel.m
//  JZYK
//
//  Created by zhaoying on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallListModel.h"
@implementation YKMallBannerModel

@end

@implementation YKMallListShopListActivityLabel

@end

@implementation YKMallListShopListItemModel
+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{@"shopList_id":@"id"};
}
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"activity_label":[YKMallListShopListActivityLabel class]};
}
@end

@implementation YKMallListModel
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"shop_list":[YKMallListShopListItemModel class]};
}
@end
