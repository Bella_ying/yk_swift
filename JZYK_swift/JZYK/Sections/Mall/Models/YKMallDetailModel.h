//
//  YKMallDetailModel.h
//  JZYK
//
//  Created by zhaoying on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKMallShopInfoRebateActivityModel : NSObject
@property (nonatomic,copy) NSString *name;//返利活动的标题
@property (nonatomic,copy) NSString *color;//返利活动的背景色
@end

@interface YKMallShopInfoModel : NSObject
@property (nonatomic, strong) NSArray <NSString *>*big_img_url;//大图url数组
@property (nonatomic,copy) NSString *title;//标题
@property (nonatomic,copy) NSString *desc;//描述
@property (nonatomic,assign) NSInteger installment_buy_count;//分期人数
@property (nonatomic,assign) CGFloat installment_price;//价格
@property (nonatomic,assign) NSInteger min_buy_num ;//最小购买数量
@property (nonatomic, strong) NSArray <YKMallShopInfoRebateActivityModel *>*activity_label;//活动标签数组
@property (nonatomic,assign) NSInteger shop_id;//商品id
@end

@interface YKMallOptionShopIdModel : NSObject
@property (nonatomic,copy) NSString *optionName;
@property (nonatomic,assign) NSInteger shopId;
@property (nonatomic,assign) BOOL isSelect;//增加一个选项是否选中
@end

@interface YKMallInstallmentOptionModel : NSObject
@property (nonatomic,copy) NSString *name ;
@property (nonatomic,strong) NSArray *options;
@property (nonatomic,strong) NSArray *shop_id;
@end

@interface YKMallMonthPayAmountModel : NSObject
@property (nonatomic,copy) NSString *month ;
@property (nonatomic,copy) NSString *pay_amount;
@property (nonatomic,assign) BOOL isMonthPayAmountSelect;//增加一个参数是否选中

@end

@interface YKMallMoreShopInfoServiceModel : NSObject
@property (nonatomic,copy) NSString *name;//服务的标题
@property (nonatomic,copy) NSString *img;//服务的图片的url
@property (nonatomic,copy) NSString *body;//服务的内容
@end

@interface YKMallMoreShopInfoModel : NSObject
@property (nonatomic, strong) NSArray <YKMallMoreShopInfoServiceModel *>*service_label;//服务正品保证
@property (nonatomic,assign) BOOL is_show_address;//是否展示地址
@property (nonatomic,assign) BOOL is_show_rebate;//是否展示返利
@property (nonatomic,copy) NSString *h5_title;//返利活动的标题，可以用这个字段是否为空去判断是否有返利，或者
@property (nonatomic,copy) NSString *h5_url;//返利活动跳转的url链接
@property (nonatomic, strong) NSArray <YKMallInstallmentOptionModel *>*installment_option_arr;//规格
@property (nonatomic, strong) NSArray <YKMallMonthPayAmountModel *>*month_pay_amount_arr;//月供
@property (nonatomic,copy) NSString *default_address;//用户登录时的默认地址

@property (nonatomic,copy) NSString *paymentStr;//自己新增一个参数估算

@end

@interface YKMallShareInfoModel : NSObject
@property (nonatomic,copy) NSString *share_title;//分享标题
@property (nonatomic,copy) NSString *share_body;//分享描述
@property (nonatomic,copy) NSString *share_url;//分享url
@property (nonatomic,copy) NSString *platform;//分享平台
@property (nonatomic, copy) NSString *share_data_type;////0：默认分享方式  1：分享大图
@property (nonatomic, copy) NSString *share_logo;//分享图片
@end


@interface YKMallDetailModel : NSObject
@property (nonatomic,strong) YKMallShopInfoModel *shop_info;//上半部分商品的信息
@property (nonatomic,strong) YKMallMoreShopInfoModel *more_shop_info;//下半部分商品的信息
@property (nonatomic,strong) YKMallShareInfoModel *share_info;//分享的信息
@property (nonatomic,copy) NSString *installment_detail;//商品介绍url
//=========================下面的字段是自己添加的===============================//
@property (nonatomic,copy) NSString *optionStr ;//规格的字符拼接（不包括数量）校验的时候向后台传的
@property (nonatomic,copy) NSString *optionNumberStr ;//规格的字符拼接（包括数量)前端展示用
@property (nonatomic,assign) NSInteger buyNum;//购买的数量
@property (nonatomic,assign) BOOL isUpdateMonthlyPayments;//是否更新月供详情


@property (nonatomic,strong) NSMutableArray *mallOptionShopIdModelArray;//装有(YKMallOptionShopIdModel)的规格数组

@property (nonatomic,strong) NSMutableArray *mallMonthPayAmountModelArray;//装有(YKMallMonthPayAmountModel)的规格数组

@end
