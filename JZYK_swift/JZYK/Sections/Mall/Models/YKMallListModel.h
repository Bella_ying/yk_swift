//
//  YKMallListModel.h
//  JZYK
//
//  Created by zhaoying on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface YKMallBannerModel : NSObject
@property (nonatomic,copy) NSString *url;//跳转的链接
@property (nonatomic,copy) NSString *pic;//图片的链接
@property (nonatomic,copy) NSString *skip_code;// 跳转的code
@property (nonatomic,copy) NSString *data;//installment_type 对应installment_type的值
@end

@interface YKMallListShopListActivityLabel : NSObject
@property (nonatomic,copy) NSString *name;//内容
@property (nonatomic,copy) NSString *color;//颜色
@end


@interface YKMallListShopListItemModel : NSObject
@property (nonatomic, assign) NSInteger shopList_id;
@property (nonatomic,copy) NSString *title;//标题
@property (nonatomic,copy) NSString *img_url;//图片url
@property (nonatomic,copy) NSString *installment_price;//价格
@property (nonatomic,copy) NSString *installment_type;//对应tab_type的key值
@property (nonatomic,copy) NSString *month_pay;//月供
@property (nonatomic, strong) NSArray <YKMallListShopListActivityLabel *>*activity_label;//活动标签数组
@end

@interface YKMallListModel : NSObject
@property (nonatomic, strong) NSArray <YKMallListShopListItemModel *>*shop_list;//商品列表
@property (nonatomic, strong) YKMallBannerModel *banner;//banner
@property (nonatomic,copy) NSString *shop_list_title;//标题
@property (nonatomic,assign) NSInteger page_size;//每页默认显示多少条数据
@property (nonatomic,assign) NSInteger shop_list_count;//每页有多少条数据
@property (nonatomic,copy) NSString *title;//跳转到活动页的标题
@end
