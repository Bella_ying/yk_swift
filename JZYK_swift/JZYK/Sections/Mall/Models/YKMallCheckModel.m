//
//  YKMallCheckModel.m
//  JZYK
//
//  Created by zhaoying on 2018/6/19.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallCheckModel.h"
@implementation YKMallCheckPaymentMethodModel

@end

@implementation YKMallCheckPaymentDetailModel

@end

@implementation YKMallCheckMonthPayAmountModel
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"pay_amount_detail":[YKMallCheckPaymentDetailModel class]};
}
@end

@implementation YKMallCheckMoreInfoModel
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"payment_method":[YKMallCheckPaymentMethodModel class],@"month_pay_amount_arr":[YKMallCheckMonthPayAmountModel class]};
}
@end

@implementation YKMallCheckAgreementInfoModel

@end
@implementation YKMallCheckShopDetailModel

@end

@implementation YKMallCheckModel
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"agreement_info":[YKMallCheckAgreementInfoModel class]};
}
@end
