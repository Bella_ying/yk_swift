//
//  YKMallCheckModel.h
//  JZYK
//
//  Created by zhaoying on 2018/6/19.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"
@interface YKMallCheckPaymentMethodModel : YKBaseModel
@property (nonatomic,copy) NSString *key ;//分期付款key
@property (nonatomic,copy) NSString *text ;//付款方式
@end

@interface YKMallCheckPaymentDetailModel : YKBaseModel
@property (nonatomic,assign) NSInteger period ;//期数
@property (nonatomic,copy) NSString *per_payment ;//每期应还款
@property (nonatomic,copy) NSString *bj ;//本金
@property (nonatomic,copy) NSString *lx ;//利息

@end

@interface YKMallCheckMonthPayAmountModel : YKBaseModel
@property (nonatomic,copy) NSString *month ;//分期数
@property (nonatomic,copy) NSString *pay_amount ;//价格
@property (nonatomic, strong) NSArray <YKMallCheckPaymentDetailModel *>*pay_amount_detail;//还款详情的数组

@property (nonatomic,assign) BOOL isSelect;//增加一个属性，是否选中

@end

@interface YKMallCheckMoreInfoModel : YKBaseModel
@property (nonatomic,assign) BOOL real_pay_pwd_status ;//是否设置过交易密码
@property (nonatomic, strong) NSArray <YKMallCheckPaymentMethodModel *>*payment_method;//付款方式数组
@property (nonatomic, strong) NSArray <YKMallCheckMonthPayAmountModel *>*month_pay_amount_arr;//分期数和金额数组
@property (nonatomic,copy) NSString *user_card;//银行卡

@property (nonatomic,assign) BOOL isShow;//增加一个属性，是否展开
@end

@interface YKMallCheckAgreementInfoModel : YKBaseModel
@property (nonatomic,copy) NSString *url;//跳转的链接
@property (nonatomic,copy) NSString *text;//协议的名字
@end

@interface YKMallCheckShopDetailModel : YKBaseModel
@property (nonatomic,assign) NSInteger shop_id;//商品的id
@property (nonatomic,copy) NSString *img_url ;//图片的url
@property (nonatomic,copy) NSString *title ;//标题
@property (nonatomic,copy) NSString *installment_option;//规格
@property (nonatomic,assign) NSInteger  buy_num;//购买数量
@property (nonatomic,assign) CGFloat  installment_price;//商品总价
@property (nonatomic,assign) CGFloat  freight;//运费
@property (nonatomic,assign) CGFloat  order_money;//订单总额

@end

@interface YKMallCheckModel : YKBaseModel
@property (nonatomic, strong) YKMallCheckShopDetailModel*shop_detail;//商品的信息
@property (nonatomic, strong) YKMallCheckMoreInfoModel*more_info;//更多信息
@property (nonatomic, strong) NSArray <YKMallCheckAgreementInfoModel *>*agreement_info;//服务协议

@end
