//
//  YKMallDetailModel.m
//  JZYK
//
//  Created by zhaoying on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallDetailModel.h"
@implementation YKMallShopInfoRebateActivityModel
@end

@implementation YKMallShopInfoModel
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"activity_label":[YKMallShopInfoRebateActivityModel class]};
}
@end

@implementation YKMallOptionShopIdModel
@end

@implementation YKMallInstallmentOptionModel
@end

@implementation YKMallMonthPayAmountModel
@end

@implementation YKMallMoreShopInfoServiceModel
@end

@implementation YKMallMoreShopInfoModel
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"installment_option_arr":[YKMallInstallmentOptionModel class],@"month_pay_amount_arr":[YKMallMonthPayAmountModel class],@"service_label":[YKMallMoreShopInfoServiceModel class]};
}
@end

@implementation YKMallShareInfoModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"share_body" : @"share_desc"};
}
@end

@implementation YKMallDetailModel
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"shop_info":[YKMallShopInfoModel class],@"more_shop_info":[YKMallMoreShopInfoModel class]};
}
- (NSMutableArray*)mallOptionShopIdModelArray
{
    if (!_mallOptionShopIdModelArray) {
        _mallOptionShopIdModelArray = @[].mutableCopy;
    }
    return _mallOptionShopIdModelArray;
}

- (NSMutableArray*)mallMonthPayAmountModelArray
{
    if (!_mallMonthPayAmountModelArray) {
        _mallMonthPayAmountModelArray = @[].mutableCopy;
    }
    return _mallMonthPayAmountModelArray;
}

@end
