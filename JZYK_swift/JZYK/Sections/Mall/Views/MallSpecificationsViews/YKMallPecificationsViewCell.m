//
//  YKMallPecificationsViewCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallPecificationsViewCell.h"
#import "YKMallDetailModel.h"
@interface YKMallPecificationsViewCell ()<UITextFieldDelegate>
- (IBAction)minusBtnClick:(id)sender;
- (IBAction)addBtnClick:(id)sender;
@property (nonatomic,assign) NSInteger  min_buy_num;//最少购买数量
@end

@implementation YKMallPecificationsViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.numberTf.delegate = self;
    self.numberTf.text = @"1";
}
- (void)yk_renderMallPecificationsViewCell:(YKMallShopInfoModel*)model
{
    self.min_buy_num = model.min_buy_num;
    self.numberTf.text = [NSString stringWithFormat:@"%ld",model.min_buy_num];
}
- (IBAction)minusBtnClick:(id)sender
{
    int number = [self.numberTf.text intValue];
    if (number <= self.min_buy_num) {
        [MBProgressHUD showHudWithMessage:[NSString stringWithFormat:@"购买商品的数量至少为%ld哦",self.min_buy_num]];
        return;
    }
    number --;
    self.numberTf.text = [NSString stringWithFormat:@"%d",number];
    //减号
    if (self.minusBtnClickBlock) {
        self.minusBtnClickBlock();
    }
}

- (IBAction)addBtnClick:(id)sender
{
    int number = [self.numberTf.text intValue];
    number ++;
    self.numberTf.text = [NSString stringWithFormat:@"%d",number];
    
    //加号
    if (self.addBtnClickBlock) {
        self.addBtnClickBlock();
    }
}

@end
