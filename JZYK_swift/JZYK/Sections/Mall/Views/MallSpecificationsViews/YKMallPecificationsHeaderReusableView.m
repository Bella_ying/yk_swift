//
//  YKMallPecificationsHeaderReusableView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallPecificationsHeaderReusableView.h"
@interface YKMallPecificationsHeaderReusableView ()
@property (weak, nonatomic) IBOutlet UILabel *tiltleLabel;

@end

@implementation YKMallPecificationsHeaderReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)yk_renderKMallPecificationsHeaderReusableView:(NSString*)title
{
    self.tiltleLabel.text = title;
}
@end
