//
//  YKMallPecificationsHeaderReusableView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKMallPecificationsHeaderReusableView : UICollectionReusableView
- (void)yk_renderKMallPecificationsHeaderReusableView:(NSString*)title;
@end
