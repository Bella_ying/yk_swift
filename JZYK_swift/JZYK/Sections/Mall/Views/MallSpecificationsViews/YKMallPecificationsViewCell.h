//
//  YKMallPecificationsViewCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallShopInfoModel;
typedef void(^ykMinusBtnClickBlock)(void);
typedef void(^ykAddBtnClickBlock)(void);

@interface YKMallPecificationsViewCell : UITableViewCell
@property (nonatomic,copy) ykMinusBtnClickBlock minusBtnClickBlock;
@property (nonatomic,copy) ykAddBtnClickBlock addBtnClickBlock;
@property (weak, nonatomic) IBOutlet UITextField *numberTf;
- (void)yk_renderMallPecificationsViewCell:(YKMallShopInfoModel*)model;
@end
