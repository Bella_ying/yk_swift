//
//  YKMallPecificationsView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallPecificationsView.h"
#import "YKMallDetailModel.h"
#import "YKMallPecificationsCollectionCell.h"
#import "YKMallPecificationsModel.h"
#import "YKMallPecificationsHeaderReusableView.h"
#import "YKMallPecificationsViewHeaderReusableView.h"
#import "UICollectionViewLeftAlignedLayout.h"

@interface YKMallPecificationsView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *pricelLabel;
- (IBAction)closeBtnClick:(id)sender;
- (IBAction)buyBtnClick:(id)sender;
@property (nonatomic,strong) YKMallDetailModel *mallDetailModel;
@property (nonatomic,assign) NSInteger shopId;//选中的shopId,先默认给一个0值，用户点击的时候，重新赋值
@property(nonatomic,assign) BOOL  isEqual;//判断用户输入的商品数量是否等于上次的

@end


static NSString * const kMallPecificationsCollectionCell  = @"YKMallPecificationsCollectionCell";
static NSString * const kYKMallPecificationsHeaderReusableView  = @"YKMallPecificationsHeaderReusableView";
static NSString * const kYKMallPecificationsViewHeaderReusableView  = @"YKMallPecificationsViewHeaderReusableView";



@implementation YKMallPecificationsView

- (void)awakeFromNib
{
    [super awakeFromNib];
   
    [self prepareUI];
    self.isEqual = YES;
}

- (void)prepareUI
{
    UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    [self.collectionView setCollectionViewLayout:layout];
   
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKMallPecificationsCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:kMallPecificationsCollectionCell];
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKMallPecificationsHeaderReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kYKMallPecificationsHeaderReusableView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKMallPecificationsViewHeaderReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kYKMallPecificationsViewHeaderReusableView];
    self.collectionView.showsVerticalScrollIndicator = NO;
    
}

# pragma mark - 协议方法
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.mallDetailModel.mallOptionShopIdModelArray.count + 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == self.mallDetailModel.mallOptionShopIdModelArray.count) {
        //购买数量
        return 0;
    }else{
        return [self.mallDetailModel.mallOptionShopIdModelArray[section] count];
        
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    YKMallPecificationsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kMallPecificationsCollectionCell forIndexPath:indexPath];
    if (indexPath.row < [self.mallDetailModel.mallOptionShopIdModelArray[indexPath.section] count] ) {
        YKMallOptionShopIdModel *model = self.mallDetailModel.mallOptionShopIdModelArray[indexPath.section][indexPath.row];
        [cell yk_renderCollectionCellContent:model];
    }
    return cell;
}
//行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}
//水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    //上左下右
    return UIEdgeInsetsMake(0, 0, 0,0);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    YKMallPecificationsCollectionCell *cell = [[NSBundle mainBundle]loadNibNamed:@"YKMallPecificationsCollectionCell" owner:nil options:nil][0];;
    if (indexPath.row < [self.mallDetailModel.mallOptionShopIdModelArray[indexPath.section] count] ) {
        YKMallOptionShopIdModel *model = self.mallDetailModel.mallOptionShopIdModelArray[indexPath.section][indexPath.row];
        [cell yk_renderCollectionCellContent:model];
    }
    CGSize mysize = [cell sizeForCell];
    return mysize;
}

//header的size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == self.mallDetailModel.mallOptionShopIdModelArray.count) {
        return CGSizeMake(WIDTH_OF_SCREEN, 93);
    }else{
        return CGSizeMake(WIDTH_OF_SCREEN, 50);
    }
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        if (indexPath.section == self.mallDetailModel.mallOptionShopIdModelArray.count) {
            YKMallPecificationsViewHeaderReusableView *headerView = (YKMallPecificationsViewHeaderReusableView *) [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kYKMallPecificationsViewHeaderReusableView forIndexPath:indexPath];
            [headerView yk_renderMallPecificationsViewHeaderReusableView:self.mallDetailModel];
            WEAK_SELF
            [headerView setBuyNumBlock:^(NSInteger buyNum) {
                if (weakSelf.mallDetailModel.buyNum == buyNum) {
                    weakSelf.isEqual = YES;
                }else{
                     weakSelf.isEqual = NO;
                }
                weakSelf.mallDetailModel.buyNum = buyNum;
                weakSelf.pricelLabel.text = [NSString stringWithFormat:@"¥%.2f",self.mallDetailModel.shop_info.installment_price * buyNum];
                [weakSelf goodsPecificationsStr];
                [weakSelf isUpdateMonthlyPayments];
            }];
            return headerView;
        }else {
                YKMallPecificationsHeaderReusableView *headerView = (YKMallPecificationsHeaderReusableView *) [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kYKMallPecificationsHeaderReusableView forIndexPath:indexPath];
            if (indexPath.section < self.mallDetailModel.more_shop_info.installment_option_arr.count) {
                 [headerView yk_renderKMallPecificationsHeaderReusableView: self.mallDetailModel.more_shop_info.installment_option_arr[indexPath.section].name];
            }
                return headerView;
            }
        
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [self.mallDetailModel.mallOptionShopIdModelArray[indexPath.section]count] ) {
        
        //同一分区，不是选中的 ， model.isSelect = NO
        [self.mallDetailModel.mallOptionShopIdModelArray enumerateObjectsUsingBlock:^(NSArray *optionArray, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx == indexPath.section) {
                //是选中分区的
                [optionArray enumerateObjectsUsingBlock:^(YKMallOptionShopIdModel *optionShopIdModel, NSUInteger optionIdx, BOOL * _Nonnull stop) {
                    if (optionIdx == indexPath.row) {
                        optionShopIdModel.isSelect = YES;
                    }else{
                        optionShopIdModel.isSelect = NO;
                    }
                }];
            }
        }];
        
        YKMallOptionShopIdModel *model = self.mallDetailModel.mallOptionShopIdModelArray[indexPath.section][indexPath.row];
        if (model.shopId != 0) {
            //如果用户最后一次点击的shopId不为0，则将self.mallDetailModel.shop_info.shop_id重新赋值；否则还是原来的
            self.mallDetailModel.shop_info.shop_id = model.shopId;
            if (self.changeBannerImageBlock) {
                self.changeBannerImageBlock(YES, model.shopId, self.mallDetailModel.buyNum);
            }
        }else{
            [self goodsPecificationsStr];
            [collectionView reloadData];
        }
        self.shopId = model.shopId;
    }
}

- (void)yk_renderModels:(YKMallDetailModel*)model
{
    self.shopId = 0;
    self.mallDetailModel = model;
    self.pricelLabel.text = [NSString stringWithFormat:@"¥%.2f",self.mallDetailModel.shop_info.installment_price * self.mallDetailModel.buyNum];
    [self.collectionView reloadData];
}

#pragma mark- 关闭按钮点击事件
- (IBAction)closeBtnClick:(id)sender
{
    [self goodsPecificationsStr];
    if (self.closeBtnClickBlock) {
        self.closeBtnClickBlock([self isUpdateMonthlyPayments], self.mallDetailModel.shop_info.shop_id, self.mallDetailModel.buyNum);
    }
}
#pragma mark- 购买按钮点击事件
- (IBAction)buyBtnClick:(id)sender
{
    [self goodsPecificationsStr];
    if (self.buyBtnClickBlock) {
        self.buyBtnClickBlock([self isUpdateMonthlyPayments],self.mallDetailModel.optionStr, self.mallDetailModel.shop_info.shop_id, self.mallDetailModel.buyNum);
    }
}
//MARK:是否更新月供估算，当选择的用户点击的是不影响价格的（shopid 为0）时 和 购买的数量跟赏析相等同时满足时不去更新月供估算的；否则都去更新
- (BOOL)isUpdateMonthlyPayments
{
    BOOL isUpdateMonthlyPayments = NO;
    if (self.shopId == 0 && self.isEqual == YES) {
        //当选择的用户点击的是不影响价格的（shopid 为0）时 和 购买的数量跟赏析相等同时满足时不去更新月供估算的；否则都去更新
        isUpdateMonthlyPayments = NO;
    }else{
        isUpdateMonthlyPayments = YES;
    }
    self.mallDetailModel.isUpdateMonthlyPayments = isUpdateMonthlyPayments;
    return isUpdateMonthlyPayments;
}
- (void)goodsPecificationsStr
{
    //遍历出isSelect的然后拼接成字符
    NSMutableArray *array = @[].mutableCopy;
    [self.mallDetailModel.mallOptionShopIdModelArray enumerateObjectsUsingBlock:^(NSArray *otionArray, NSUInteger idx, BOOL * _Nonnull stop) {
        [otionArray enumerateObjectsUsingBlock:^( YKMallOptionShopIdModel *mallOptionShopIdModel, NSUInteger optionIdx, BOOL * _Nonnull stop) {
            if (mallOptionShopIdModel.isSelect) {
                [array addObject:mallOptionShopIdModel.optionName];
            }
        }];
    }];
    self.mallDetailModel.optionNumberStr =  [NSString stringWithFormat:@"%@/数量%ld",[array componentsJoinedByString:@"/"],self.mallDetailModel.buyNum];
    self.mallDetailModel.optionStr =  [array componentsJoinedByString:@"/"];
    DLog(@"optionStr====%@", self.mallDetailModel.optionStr);
    DLog(@"optionStr====%ld", self.mallDetailModel.buyNum);
    DLog(@"optionNumberStr====%@", self.mallDetailModel.optionNumberStr);
    DLog(@"shop_id=====%ld", self.mallDetailModel.shop_info.shop_id);
}

@end
