//
//  YKConfirmOrderInstallmentlCollectionCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallOptionShopIdModel;
@class YKMallMonthPayAmountModel;
@interface YKMallPecificationsCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
- (void)yk_renderCollectionCellContent:(YKMallOptionShopIdModel*)model;
- (void)yk_renderWithModel:(YKMallMonthPayAmountModel*)model;
- (CGSize)sizeForCell;
@end
