//
//  YKConfirmOrderInstallmentlCollectionCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallPecificationsCollectionCell.h"
#import "YKMallDetailModel.h"
#define itemHeight 32
@interface YKMallPecificationsCollectionCell ()

@end

@implementation YKMallPecificationsCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderColor = LINE_COLOR.CGColor;
    self.layer.borderWidth = 1;
    self.layer.cornerRadius = 2;
    self.clipsToBounds = YES;
}
- (void)yk_renderCollectionCellContent:(YKMallOptionShopIdModel*)model
{
    self.contentLabel.text = model.optionName;
    if (model.isSelect) {
        self.layer.borderColor = MAIN_THEME_COLOR.CGColor;
        self.contentLabel.textColor = MAIN_THEME_COLOR;
    }else{
        self.layer.borderColor = LINE_COLOR.CGColor ;
        self.contentLabel.textColor = LABEL_TEXT_COLOR;
    }
    
}
- (void)yk_renderWithModel:(YKMallMonthPayAmountModel*)model
{
     self.contentLabel.text = [NSString stringWithFormat:@"%@期", model.month];
    if (model.isMonthPayAmountSelect) {
        self.contentLabel.textColor = MAIN_THEME_COLOR;
        self.layer.borderColor = MAIN_THEME_COLOR.CGColor ;
    }else{
        self.contentLabel.textColor = LABEL_TEXT_COLOR;
        self.layer.borderColor = LINE_COLOR.CGColor;
    }
}
- (CGSize)sizeForCell
{
    //宽度加 itemHeight 为了label两边有距离。
    return CGSizeMake([self.contentLabel sizeThatFits:CGSizeMake(MAXFLOAT, MAXFLOAT)].width + itemHeight, itemHeight);
}

//#pragma mark — 实现自适应文字宽度的关键步骤:item的layoutAttributes
//- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes{
//
//    UICollectionViewLayoutAttributes *attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
//    CGRect rect = [self.contentLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, itemHeight) options:NSStringDrawingTruncatesLastVisibleLine|   NSStringDrawingUsesFontLeading |NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} context:nil];
//    rect.size.width +=itemHeight;
//    rect.size.height = itemHeight;
//    attributes.frame = rect;
//    return attributes;
//
//}

@end
