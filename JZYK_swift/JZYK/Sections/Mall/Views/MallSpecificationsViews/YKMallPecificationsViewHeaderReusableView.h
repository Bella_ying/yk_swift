//
//  YKMallPecificationsViewHeaderReusableView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallDetailModel;
typedef void(^ykBuyNumBlock)(NSInteger buyNum);
@interface YKMallPecificationsViewHeaderReusableView : UICollectionReusableView
- (void)yk_renderMallPecificationsViewHeaderReusableView:(YKMallDetailModel*)model;
@property (nonatomic,copy) ykBuyNumBlock buyNumBlock;
@end
