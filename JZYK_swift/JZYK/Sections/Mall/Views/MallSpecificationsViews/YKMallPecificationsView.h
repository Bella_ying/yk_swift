//
//  YKMallPecificationsView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallDetailModel;
typedef void(^ykPecificationsCloseBtnClickBlock)(BOOL isUpdateMonthlyPayments , NSInteger shopId,NSInteger buyNum);
typedef void(^ykBuyBtnClickBlock)(BOOL isUpdateMonthlyPayments,NSString *pecificationsStr,NSInteger shopId,NSInteger buyNum);
typedef void(^ykPecificationsChangeBannerImageBlock)(BOOL isUpdateMonthlyPayments , NSInteger shopId,NSInteger buyNum);

@interface YKMallPecificationsView : UIView

@property (nonatomic,copy) ykPecificationsCloseBtnClickBlock closeBtnClickBlock;
@property (nonatomic,copy) ykBuyBtnClickBlock buyBtnClickBlock;
@property (nonatomic,copy) ykPecificationsChangeBannerImageBlock changeBannerImageBlock;

- (void)yk_renderModels:(YKMallDetailModel*)model;
@end
