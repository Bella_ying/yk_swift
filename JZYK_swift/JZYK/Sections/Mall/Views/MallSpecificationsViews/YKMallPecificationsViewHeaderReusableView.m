//
//  YKMallPecificationsViewHeaderReusableView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallPecificationsViewHeaderReusableView.h"
#import "YKMallDetailModel.h"
@interface YKMallPecificationsViewHeaderReusableView ()
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *minusBtn;
@property (weak, nonatomic) IBOutlet UITextField *numberTf;
@property (nonatomic,strong) YKMallDetailModel *detailModel;
@end

@implementation YKMallPecificationsViewHeaderReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.addBtn addTarget:self action:@selector(addBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.minusBtn addTarget:self action:@selector(minusBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
- (void)addBtnClick
{
    NSInteger number = [self.numberTf.text integerValue];
    number ++;
    self.numberTf.text = [NSString stringWithFormat:@"%ld",(long)number];
    if (self.buyNumBlock) {
        self.buyNumBlock(number);
    }

}
- (void)minusBtnClick
{
    NSInteger number = [self.numberTf.text integerValue];
    if (number <= self.detailModel.shop_info.min_buy_num) {
        [[iToast makeText:[NSString stringWithFormat:@"购买商品的数量至少为%ld哦",(long)self.detailModel.shop_info.min_buy_num]] show];
        return;
    }
    number --;
    self.numberTf.text = [NSString stringWithFormat:@"%ld",(long)number];
    if (self.buyNumBlock) {
        self.buyNumBlock(number);
    }
}
- (void)yk_renderMallPecificationsViewHeaderReusableView:(YKMallDetailModel*)model
{
    self.detailModel = model;
    self.numberTf.text = [NSString stringWithFormat:@"%ld",(long)self.detailModel.buyNum];
   
}

@end
