//
//  YKMallDetailNavigationView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ykMallDetailNavigationBackBlock)(void);
typedef void(^ykMallDetailNavigationShareBlock)(void);

@interface YKMallDetailNavigationView : UIView
@property (nonatomic, strong) UILabel *navTitleLabel;
@property (nonatomic, copy) ykMallDetailNavigationBackBlock backBlock;
@property (nonatomic, copy) ykMallDetailNavigationShareBlock shareBlock;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *shareButton;
@end
