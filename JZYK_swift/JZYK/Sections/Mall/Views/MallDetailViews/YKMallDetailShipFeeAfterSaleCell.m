//
//  YKMallDetailShipFeeAfterSaleCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallDetailShipFeeAfterSaleCell.h"
#import "YKMallDetailModel.h"
@interface YKMallDetailShipFeeAfterSaleCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLayout;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView1;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel1;
@property (weak, nonatomic) IBOutlet UIImageView *imgView2;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel2;

@end

@implementation YKMallDetailShipFeeAfterSaleCell

- (void)awakeFromNib {
    [super awakeFromNib];
   self.selectionStyle  = UITableViewCellSelectionStyleNone;
   self.layer.masksToBounds = YES;//自动遮罩不可见区域，超出不显示
}
- (void)yk_renderModel:(YKMallMoreShopInfoModel*)model
{
    if (model.service_label.count) {
        self.widthLayout.constant = WIDTH_OF_SCREEN/model.service_label.count;
        [model.service_label enumerateObjectsUsingBlock:^(YKMallMoreShopInfoServiceModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx == 0) {
                [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.img] placeholderImage:[UIImage imageNamed:@"mall_service_disable"]];
                self.titleLabel.text = obj.name;
            }else if (idx == 1){
                [self.imgView1 sd_setImageWithURL:[NSURL URLWithString:obj.img] placeholderImage:[UIImage imageNamed:@"mall_service_disable"]];
                self.titleLabel1.text = obj.name;
            }else{
                [self.imgView2 sd_setImageWithURL:[NSURL URLWithString:obj.img] placeholderImage:[UIImage imageNamed:@"mall_service_disable"]];
                self.titleLabel2.text = obj.name;
            }
        }];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
