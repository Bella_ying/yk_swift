//
//  YKProductItemBannerCollectionCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKProductItemBannerCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bannerImgView;

@end
