//
//  YKMallDetailProductItemInfoCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallDetailProductItemInfoCell.h"
#import "YKMallDetailModel.h"
@interface YKMallDetailProductItemInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *preferentiaLabel;
@property (weak, nonatomic) IBOutlet UILabel *cashBackLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *describeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *personNumberLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preferentiaLabelWidthlayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cashBackLabelWidthLayout;

@end

@implementation YKMallDetailProductItemInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle  = UITableViewCellSelectionStyleNone;
    self.cashBackLabel.layer.cornerRadius = 9;
    self.cashBackLabel.clipsToBounds = YES;
    
    self.preferentiaLabel.layer.cornerRadius = 9;
    self.preferentiaLabel.clipsToBounds = YES;
    
}
- (void)yk_renderShopInfo:(YKMallShopInfoModel *)shopInfoModel
{
    self.titleLabel.text = shopInfoModel.title;
    self.describeLabel.text = shopInfoModel.desc;
    
    NSString * price = [NSString stringWithFormat:@"单价 ¥%.2f",shopInfoModel.installment_price];
    NSMutableAttributedString *priceStr = [[NSMutableAttributedString alloc] initWithString:price];
    [priceStr addAttribute:NSForegroundColorAttributeName
                        value:LABEL_TEXT_COLOR
                        range:NSMakeRange(0,2)];
    [priceStr addAttribute:NSFontAttributeName
                     value:[UIFont systemFontOfSize:14.0f]
                     range:NSMakeRange(0,2)];
     self.priceLabel.attributedText = priceStr;
    
    self.personNumberLabel.text = [NSString stringWithFormat:@"%ld人分期",(long)shopInfoModel.installment_buy_count];
    //从左往右排，测试要求的
    [shopInfoModel.activity_label enumerateObjectsUsingBlock:^(YKMallShopInfoRebateActivityModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) {
            self.cashBackLabel.backgroundColor = [UIColor yk_colorWithHexString:obj.color];
            self.cashBackLabel.text = obj.name;
            CGSize size =  [self labelRectWithSize:CGSizeMake(0, 18) LabelText:self.cashBackLabel.text FontSize:10.0f];
            self.cashBackLabelWidthLayout.constant = size.width;
        }else{
            self.preferentiaLabel.text = obj.name;
            self.preferentiaLabel.backgroundColor = [UIColor yk_colorWithHexString:obj.color];
            CGSize size =  [self labelRectWithSize:CGSizeMake(0, 18) LabelText:self.preferentiaLabel.text FontSize:10.0f];
            self.preferentiaLabelWidthlayout.constant = size.width;
        }
    }];
}

- (CGSize)labelRectWithSize:(CGSize)size
                  LabelText:(NSString *)labelText
                   FontSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    CGSize actualsize = [labelText boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    actualsize.width  += 20;
    actualsize.height += 10;
    return actualsize;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
