//
//  YKMallDetailBottomBarView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallDetailBottomBarView.h"
#import "YKMallDetailModel.h"
@interface YKMallDetailBottomBarView ()
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation YKMallDetailBottomBarView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.buyBtn addTarget:self action:@selector(buyBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
- (void)buyBtnClick
{
    if (self.buyBtnClickBlock) {
        self.buyBtnClickBlock();
    }
}
- (void)yk_renderMallDetailBottomBarViewWithInstallmentPrice:(CGFloat)installmentPrice buyNum:(NSInteger)buyNum
{
    NSString *price = [NSString stringWithFormat:@"¥%.2f",installmentPrice * buyNum];
    self.priceLabel.text = price;
}
@end
