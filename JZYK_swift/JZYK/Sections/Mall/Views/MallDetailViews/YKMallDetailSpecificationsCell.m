//
//  YKMallDetailSpecificationsCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallDetailSpecificationsCell.h"
#import "YKMallDetailModel.h"
@interface YKMallDetailSpecificationsCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation YKMallDetailSpecificationsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle  = UITableViewCellSelectionStyleNone;
}
- (void)yk_renderModel:( YKMallDetailModel*)model index:(NSIndexPath*)indexPath
{
    NSString *title = @"";
    switch (indexPath.row) {
        case 0:
            title = @"请选择规格";
            self.contentLabel.text = model.optionNumberStr;
            break;
        case 1:
        {
            title = @"月供测算";
            self.contentLabel.text = model.more_shop_info.paymentStr;
        }
            break;
        case 2:
        {
            title = @"配送";
            self.contentLabel.text = model.more_shop_info.default_address;
        }
            break;
        default:
            break;
    }
     self.titleLabel.text = title;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
