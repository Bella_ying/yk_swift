//
//  YKMallDetailBottomBarView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ykBuyBtnClickBlock)(void);
@interface YKMallDetailBottomBarView : UIView
@property (nonatomic,copy) ykBuyBtnClickBlock buyBtnClickBlock;
- (void)yk_renderMallDetailBottomBarViewWithInstallmentPrice:(CGFloat)installmentPrice buyNum:(NSInteger)buyNum;
@end
