//
//  YKMallDetailBannerCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallDetailBannerCell.h"
#import "YKProductItemBannerCollectionCell.h"
#import "YKMallDetailModel.h"
@interface YKMallDetailBannerCell ()<UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UILabel *currentPageLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewLayout;
@property (nonatomic,strong) NSArray *array;
@property (nonatomic,assign) int currentPage;//当前页
@end

static NSString * const kProductItemBannerCollectionCell = @"ProductItemBannerCollectionCell";

@implementation YKMallDetailBannerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.currentPageLabel.layer.cornerRadius = 10;
    self.currentPageLabel.clipsToBounds = YES;
    
    CGSize itemSize = CGSizeMake(WIDTH_OF_SCREEN, WIDTH_OF_SCREEN);
    self.collectionViewLayout.itemSize = itemSize;
    self.collectionViewLayout.minimumInteritemSpacing = 0.0f;
    self.collectionViewLayout.minimumLineSpacing = 0.0f;
    self.collectionViewLayout.sectionInset = UIEdgeInsetsZero;
    self.collectionViewLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;

    self.collectionView.alwaysBounceHorizontal = YES;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKProductItemBannerCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:kProductItemBannerCollectionCell];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
}

#pragma mark - UICollectionViewDataSource
//定义展示的Section的个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

////定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.array.count;
}

//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YKProductItemBannerCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kProductItemBannerCollectionCell forIndexPath:indexPath];
    if (indexPath.row < self.array.count) {
        [cell.bannerImgView sd_setImageWithURL:[NSURL URLWithString:[self.array objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"mall_big_zw"]];
    }
    return cell;
}
#pragma mark UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)yk_renderShopInfo:(YKMallShopInfoModel *)shopInfoModel
{
    self.array = shopInfoModel.big_img_url;
    self.currentPageLabel.text = [NSString stringWithFormat:@"1/%ld",(long)shopInfoModel.big_img_url.count];
    
    if (self.currentPage >= shopInfoModel.big_img_url.count) {
        self.currentPage = 0;
        [self.collectionView setContentOffset:CGPointMake(0.0f, 0.0f) animated:NO];
    }
    [self.collectionView reloadData];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.collectionView) {
        self.currentPage = floor((self.collectionView.contentOffset.x - WIDTH_OF_SCREEN ) / WIDTH_OF_SCREEN) + 2;
        self.currentPageLabel.text = [NSString stringWithFormat:@"%d/%ld",self.currentPage,(long)self.array.count];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
