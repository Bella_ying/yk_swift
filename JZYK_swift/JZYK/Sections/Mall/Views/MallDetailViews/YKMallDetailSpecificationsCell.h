//
//  YKMallDetailSpecificationsCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallDetailModel;
@interface YKMallDetailSpecificationsCell : UITableViewCell
- (void)yk_renderModel:( YKMallDetailModel*)model index:(NSIndexPath*)indexPath;
@end
