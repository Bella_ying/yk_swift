//
//  YKMallDetailNavigationView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallDetailNavigationView.h"
@interface YKMallDetailNavigationView ()


@end

@implementation YKMallDetailNavigationView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self prepareUI];
    }
    return self;
}

- (void)prepareUI
{

    [self addSubview:self.navTitleLabel];
    [self.navTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.offset(-10);
        make.width.mas_offset(100 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(24);
    }];
    [self addSubview:self.shareButton];
    [self.shareButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-7);
        make.bottom.offset(-4);
        make.width.height.offset(40);
    }];
    self.shareButton.imageEdgeInsets = UIEdgeInsetsMake(8,8,8,8);
    [self.shareButton addTarget:self action:@selector(shareButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.backButton];
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(2);
        make.bottom.offset(-3);
        make.width.offset(40);
        make.height.offset(40);
    }];
    self.backButton.imageEdgeInsets = UIEdgeInsetsMake(9,13,9,13);
    [self.backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark- 点击方法
- (void)shareButtonAction:(UIButton *)button
{
    if (self.shareBlock) {
        self.shareBlock();
    }
}

- (void)backButtonAction:(UIButton *)button
{
    if (self.backBlock) {
        self.backBlock();
    }
}

- (UILabel *)navTitleLabel
{
    if (!_navTitleLabel) {
        _navTitleLabel = [UILabel new];
        _navTitleLabel.textColor = WHITE_COLOR;
        _navTitleLabel.font = [UIFont boldSystemFontOfSize:18];
        _navTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _navTitleLabel;
}

- (UIButton *)backButton
{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    }
    return _backButton;
}

- (UIButton *)shareButton
{
    if (!_shareButton) {
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shareButton setImage:[UIImage imageNamed:@"mall_share"] forState:UIControlStateNormal];
    }
    return _shareButton;
}

@end
