//
//  YKMallDetailRebateCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallDetailRebateCell.h"
#import "YKMallDetailModel.h"
@interface YKMallDetailRebateCell ()
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation YKMallDetailRebateCell
- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle  = UITableViewCellSelectionStyleNone;
}
- (void)yk_renderModel:(YKMallMoreShopInfoModel*)model
{
    self.contentLabel.text = model.h5_title;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
