//
//  YKMallPaymentsView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallPaymentsView.h"
#import "YKMallPecificationsCollectionCell.h"
#import "YKMallDetailModel.h"
@interface YKMallPaymentsView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
- (IBAction)closeBtnClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (nonatomic,strong) YKMallMoreShopInfoModel*mallMoreShopInfoModel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@end

static NSString * const cellId  = @"PecificationsCollectionCell";
static CGFloat  const kJianGe  = 20.0f;
static NSInteger const kGeShu  = 4;
static CGFloat  const kCellHeight  = 32.0f;

@implementation YKMallPaymentsView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self prepareUI];
   
}

- (void)prepareUI
{
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKMallPecificationsCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellId];
}
- (void)yk_renderMallPaymentsViewWtihModel:(YKMallMoreShopInfoModel*)model
{
    self.mallMoreShopInfoModel = model;
    [self.mallMoreShopInfoModel.month_pay_amount_arr enumerateObjectsUsingBlock:^(YKMallMonthPayAmountModel * _Nonnull monthPayAmountModel, NSUInteger idx, BOOL * _Nonnull stop) {
        if (monthPayAmountModel.isMonthPayAmountSelect) {
            //如果有选中的，就取选中的
            NSString *pay_amount = [NSString stringWithFormat:@"%@元",monthPayAmountModel.pay_amount];
            NSString *amount = [NSString stringWithFormat:@"%@元 x %@期",monthPayAmountModel.pay_amount,monthPayAmountModel.month];
            
            NSMutableAttributedString *amountStr = [[NSMutableAttributedString alloc] initWithString:amount];
            [amountStr addAttribute:NSForegroundColorAttributeName
                              value:MAIN_THEME_COLOR
                              range:NSMakeRange(0,pay_amount.length)];
            self.amountLabel.attributedText = amountStr;
        }
    }];
    CGFloat height = 0;
    NSInteger number = self.mallMoreShopInfoModel.month_pay_amount_arr.count;
    if (number % kGeShu == 0) {
        height = (number / kGeShu) * (kCellHeight+kJianGe);
    }else{
        height = ((number /kGeShu) +1) * (kCellHeight+kJianGe);
    }
    self.collectionViewHeight.constant = height;
}
#pragma mark- 协议方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.mallMoreShopInfoModel.month_pay_amount_arr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YKMallPecificationsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    if (indexPath.row < self.mallMoreShopInfoModel.month_pay_amount_arr.count) {
        YKMallMonthPayAmountModel *model = [self.mallMoreShopInfoModel.month_pay_amount_arr objectAtIndex:indexPath.row];
        [cell yk_renderWithModel:model];
    }
    return cell;
}
//定义每个UICollectionViewCell 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
     return CGSizeMake((WIDTH_OF_SCREEN - kJianGe*(kGeShu+1)) / kGeShu, kCellHeight);
}
//定义每个Section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0,0,0, 0);
}
//每个section中不同的行之间的行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 20;
}
//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 20;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    WEAK_SELF
    if (indexPath.row < self.mallMoreShopInfoModel.month_pay_amount_arr.count) {
        [self.mallMoreShopInfoModel.month_pay_amount_arr enumerateObjectsUsingBlock:^(YKMallMonthPayAmountModel * _Nonnull monthPayAmountModel, NSUInteger idx, BOOL * _Nonnull stop) {
            //是选中的
            if ( idx == indexPath.row) {
                monthPayAmountModel.isMonthPayAmountSelect = YES;
                weakSelf.mallMoreShopInfoModel.paymentStr = [NSString stringWithFormat:@"%@元 x %@期",monthPayAmountModel.pay_amount,monthPayAmountModel.month];
                NSString *pay_amount = [NSString stringWithFormat:@"%@元",monthPayAmountModel.pay_amount];
                NSString *amount = [NSString stringWithFormat:@"%@元 x %@期",monthPayAmountModel.pay_amount,monthPayAmountModel.month];
                
                NSMutableAttributedString *amountStr = [[NSMutableAttributedString alloc] initWithString:amount];
                [amountStr addAttribute:NSForegroundColorAttributeName
                                  value:MAIN_THEME_COLOR
                                  range:NSMakeRange(0,pay_amount.length)];
                self.amountLabel.attributedText = amountStr;
            }else{
                monthPayAmountModel.isMonthPayAmountSelect = NO;
            }
        }];
         [collectionView reloadData];
    }
}
- (IBAction)closeBtnClick:(id)sender
{
    if (self.closeBtnClickBlock) {
        self.closeBtnClickBlock();
    }
}
@end
