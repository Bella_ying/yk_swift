//
//  YKMallPaymentDetailsCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallCheckPaymentDetailModel;
@interface YKMallPaymentDetailsCell : UITableViewCell
- (void)yk_mallPaymentDetailsCell:(YKMallCheckPaymentDetailModel*)model;
@end
