//
//  YKMallPaymentDetailsView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallPaymentDetailsView.h"
#import "YKMallPaymentDetailsCell.h"
#import "YKMallCheckModel.h"

@interface YKMallPaymentDetailsView ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)closeBtnClick:(id)sender;
@property (nonatomic,strong) NSArray *dataArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightLayout;

@end
static NSString * const cellId  = @"MallPaymentDetailsCell";

@implementation YKMallPaymentDetailsView
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self prepareUI];
}
- (void)prepareUI
{
    WEAK_SELF
    [self.tableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            sectionMaker.hd_dataArr(^(){
                return weakSelf.dataArray;
            });
            [sectionMaker hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKMallPaymentDetailsCell));
                cellMaker.hd_rowHeight(40);
                cellMaker.hd_adapter(^(YKMallPaymentDetailsCell *cell, id data, NSIndexPath *indexPath) {
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    YKMallCheckPaymentDetailModel *model = weakSelf.dataArray[indexPath.row];
                    [cell yk_mallPaymentDetailsCell:model];
                });
                
            }];
        }];
    }];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}
- (void)yk_renderMallPaymentDetailsView:(YKMallCheckMonthPayAmountModel *)model
{
    self.dataArray = model.pay_amount_detail;
    self.tableViewHeightLayout.constant = self.dataArray.count *40;
    [self.tableView reloadData];
}

- (IBAction)closeBtnClick:(id)sender
{
    if (self.closeBtnClickBlock) {
        self.closeBtnClickBlock();
    }
}
@end
