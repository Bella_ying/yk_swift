//
//  YKMallPaymentsView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallMoreShopInfoModel;

typedef void(^ykCloseBtnClickBlock)(void);

@interface YKMallPaymentsView : UIView
@property (nonatomic,copy) ykCloseBtnClickBlock closeBtnClickBlock;

- (void)yk_renderMallPaymentsViewWtihModel:(YKMallMoreShopInfoModel*)model;

@end
