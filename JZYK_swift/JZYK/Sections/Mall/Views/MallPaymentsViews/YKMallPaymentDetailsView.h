//
//  YKMallPaymentDetailsView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallCheckMonthPayAmountModel;
typedef void(^ykCloseBtnClickBlock)(void);
@interface YKMallPaymentDetailsView : UIView
@property (nonatomic,copy) ykCloseBtnClickBlock closeBtnClickBlock;
- (void)yk_renderMallPaymentDetailsView:(YKMallCheckMonthPayAmountModel *)model;
@end
