//
//  YKMallPaymentDetailsCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallPaymentDetailsCell.h"
#import "YKMallCheckModel.h"
@interface YKMallPaymentDetailsCell ()
@property (weak, nonatomic) IBOutlet UILabel *periodLabel;
@property (weak, nonatomic) IBOutlet UILabel *perPaymentLabel;
@property (weak, nonatomic) IBOutlet UILabel *lxLabel;
@property (weak, nonatomic) IBOutlet UILabel *bjLabel;

@end

@implementation YKMallPaymentDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)yk_mallPaymentDetailsCell:(YKMallCheckPaymentDetailModel*)model
{
    self.periodLabel.text = [NSString stringWithFormat:@"%ld",(long)model.period];
    self.perPaymentLabel.text = model.per_payment;
    self.lxLabel.text = model.lx;
    self.bjLabel.text = model.bj;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
