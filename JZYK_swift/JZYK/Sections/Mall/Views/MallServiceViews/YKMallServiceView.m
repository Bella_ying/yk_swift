//
//  YKMallServiceView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallServiceView.h"
#import "YKMallServiceCell.h"
#import "YKMallDetailModel.h"
@interface YKMallServiceView ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *modelArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightLayout;

@end
static NSString * const cellId  = @"MallServiceCell";
@implementation YKMallServiceView
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKMallServiceCell" bundle:nil] forCellReuseIdentifier:cellId];
    self.tableView.estimatedRowHeight = 80;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}
- (void)yk_renderMallServiceWithModel:(YKMallMoreShopInfoModel*)model
{
    self.modelArray = model.service_label;
    [self.tableView reloadData];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YKMallServiceCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    if (indexPath.row < self.modelArray.count) {
        YKMallMoreShopInfoServiceModel *model = [self.modelArray objectAtIndex:indexPath.row];
        [cell yk_renderMallServiceCellWtihModel:model];
    }
  
    return cell;
}

- (IBAction)closeBtnClick:(id)sender
{
    if (self.closeBtnClickBlock) {
        self.closeBtnClickBlock();
    }
}
@end
