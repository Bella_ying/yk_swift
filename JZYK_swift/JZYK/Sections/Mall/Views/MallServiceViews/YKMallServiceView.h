//
//  YKMallServiceView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallMoreShopInfoModel;
typedef void(^ykCloseBtnClickBlock)(void);
@interface YKMallServiceView : UIView
- (IBAction)closeBtnClick:(id)sender;
@property (nonatomic,copy) ykCloseBtnClickBlock closeBtnClickBlock;
- (void)yk_renderMallServiceWithModel:(YKMallMoreShopInfoModel*)model;
@end
