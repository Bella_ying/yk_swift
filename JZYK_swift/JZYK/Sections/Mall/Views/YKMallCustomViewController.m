//
//  YKMallCustomViewController.m
//  JZYK
//
//  Created by zhaoying on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallCustomViewController.h"
#import "YKMallPecificationsView.h"
#import "YKMallPecificationsView.h"
#import "YKMallPaymentsView.h"
#import "YKMallPaymentDetailsView.h"
#import "YKMallServiceView.h"
#import "YKMallDetailModel.h"
#import "YKMallCheckModel.h"
#import "YKLocationPicker.h"
#import "YKMallDetailViewController.h"

@interface YKMallCustomViewController ()
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

@implementation YKMallCustomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *clickTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGRAction)];
    [self.bgView addGestureRecognizer:clickTapGR];
    
}

- (void)tapGRAction
{
    switch (self.type) {
        case YKMallTypesPecifications:
        {
            //规格
            [self dismiss];
            if (self.mallPecificationsCloseBlock) {
                self.mallPecificationsCloseBlock(self.detailModel.isUpdateMonthlyPayments, self.detailModel.shop_info.shop_id, self.detailModel.buyNum);
            }
        }
            break;
        case YKMallTypePayments:
        {
            //月供
            [self dismiss];
            if (self.customViewBlock) {
                self.customViewBlock();
            }
        }
            break;
        case YKMallTypeService: case YKMallTypePaymentDetails:
        {
            //月供详情 或者服务
            [self dismiss];
        }
            break;
        default:
            break;
    }
   
    
}

- (void)yk_showInViewController:(YKMallDetailViewController *)mallDetailVC type:(YKMallType)type
{
    self.type = type;
    self.modalPresentationStyle = UIModalPresentationCustom;
    
    //解决present过慢
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [mallDetailVC presentViewController:self animated:YES completion:^{
            self.bgView.hidden = NO;
        }];
    });
    switch (type) {
        case YKMallTypesPecifications:
            [self mallPecifications:mallDetailVC];
            break;
        case YKMallTypePayments:
            [self mallPayments];
            break;
        case YKMallTypePaymentDetails:
            [self mallPaymentDetails];
            break;
        case YKMallTypeService:
            [self mallService];
            break;
        default:
            break;
    }
}
//MARK:消失
- (void)dismiss
{
    self.bgView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}
//MARK:商品规格
- (void)mallPecifications:(YKMallDetailViewController *)mallDetailVC
{
    YKMallPecificationsView *specificationsView =  [[[NSBundle mainBundle] loadNibNamed:@"YKMallPecificationsView" owner:nil options:nil] firstObject];
    [self.view addSubview:specificationsView];
    [specificationsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(390);
        make.bottom.offset(-BOTTOM_HEIGHT);
    }];
    WEAK_SELF
    //关闭按钮的回调
    [specificationsView setCloseBtnClickBlock:^(BOOL isUpdateMonthlyPayments, NSInteger shopId, NSInteger buyNum) {
        [weakSelf dismiss];
        if (weakSelf.mallPecificationsCloseBlock) {
            weakSelf.mallPecificationsCloseBlock(isUpdateMonthlyPayments, shopId, buyNum);
        }
    }];
    //更改商品详情banner的回调
    [specificationsView setChangeBannerImageBlock:^(BOOL isUpdateMonthlyPayments, NSInteger shopId, NSInteger buyNum) {
        if (weakSelf.mallChangeBannerImageBlock) {
            weakSelf.mallChangeBannerImageBlock(isUpdateMonthlyPayments, shopId, buyNum);
        }
    }];
    
    //立即购买的回调
    [specificationsView setBuyBtnClickBlock:^(BOOL isUpdateMonthlyPayments, NSString *pecificationsStr, NSInteger shopId, NSInteger buyNum) {
        if (weakSelf.mallPecificationsBlock) {
            weakSelf.mallPecificationsBlock(isUpdateMonthlyPayments, pecificationsStr, shopId, buyNum);
        }
        [weakSelf dismiss];
    }];
    [specificationsView yk_renderModels:self.detailModel];
    
    //关闭或者更新banner回调在商品详情，请求接口成功之后，将detailModel回传过来
    [mallDetailVC setMallDetailLoadDataSuccessBlock:^(YKMallDetailModel *detailModel) {
        [specificationsView yk_renderModels:detailModel];
        self.detailModel = detailModel;
    }];
}
//MARK:月供
- (void)mallPayments
{
    YKMallPaymentsView  *paymentsView =  [[[NSBundle mainBundle] loadNibNamed:@"YKMallPaymentsView" owner:nil options:nil] firstObject];
    [self.view addSubview: paymentsView];
    [paymentsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.mas_lessThanOrEqualTo(HEIGHT_OF_SCREEN *4/5);
        make.height.mas_greaterThanOrEqualTo(266);
        make.bottom.offset(-BOTTOM_HEIGHT);
    }];
    WEAK_SELF
    [paymentsView setCloseBtnClickBlock:^{
        [weakSelf dismiss];
        if (weakSelf.customViewBlock) {
            weakSelf.customViewBlock();
        }
    }];
    [paymentsView yk_renderMallPaymentsViewWtihModel:self.detailModel.more_shop_info];
    
}
//MARK:还款详情
- (void)mallPaymentDetails
{
    YKMallPaymentDetailsView  *view =  [[[NSBundle mainBundle] loadNibNamed:@"YKMallPaymentDetailsView" owner:nil options:nil] firstObject];
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.mas_lessThanOrEqualTo(344);
        make.bottom.offset(-BOTTOM_HEIGHT);
    }];
    WEAK_SELF
    [view setCloseBtnClickBlock:^{
        [weakSelf dismiss];
    }];
    [view yk_renderMallPaymentDetailsView:self.checkMonthPayAmountModel];
}
//MARK:商品服务
- (void)mallService
{
   YKMallServiceView  *serviceView =  [[[NSBundle mainBundle] loadNibNamed:@"YKMallServiceView" owner:nil options:nil] firstObject];
    [serviceView yk_renderMallServiceWithModel:self.detailModel.more_shop_info];
    [self.view addSubview:serviceView];
    [serviceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(320);
        make.bottom.offset(-BOTTOM_HEIGHT);
    }];
    WEAK_SELF
    [serviceView setCloseBtnClickBlock:^{
        [weakSelf dismiss];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
