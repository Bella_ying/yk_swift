//
//  YKMallCustomViewController.h
//  JZYK
//
//  Created by zhaoying on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallDetailModel;
@class YKMallCheckMonthPayAmountModel;
@class YKMallDetailViewController;

typedef void(^ykCustomViewBlock)(void);
typedef void(^ykMallPecificationsBlock)(BOOL isUpdateMonthlyPayments,NSString *pecificationsStr,NSInteger shopId,NSInteger buyNum);
typedef void(^ykMallPecificationsCloseBlock)(BOOL isUpdateMonthlyPayments, NSInteger shopId, NSInteger buyNum) ;
typedef void(^ykMallChangeBannerImageBlock)(BOOL isUpdateMonthlyPayments, NSInteger shopId, NSInteger buyNum) ;


typedef NS_ENUM(NSInteger, YKMallType) {
    YKMallTypesPecifications = 0,//规格
    YKMallTypePayments = 1,//月供估算
    YKMallTypePaymentDetails = 2,//还款详情
    YKMallTypeService = 3,//服务
    YKMallTypeAddress = 4,//配送地址
};

@interface YKMallCustomViewController : UIViewController

@property (nonatomic, assign) YKMallType type;
@property (nonatomic,strong) YKMallDetailModel *detailModel;
@property (nonatomic,strong) YKMallCheckMonthPayAmountModel *checkMonthPayAmountModel;

@property (nonatomic,copy) ykCustomViewBlock customViewBlock;
@property (nonatomic,copy) ykMallPecificationsBlock mallPecificationsBlock;

//商品规格关闭的回调，如果需要更新月供估算就在商品详情重新请求接口，然后刷新列表；如果不需要更新月供估算，直接刷新商品详情列表；
@property (nonatomic,copy) ykMallPecificationsCloseBlock mallPecificationsCloseBlock;
//商品规格点击的是shop不为0时，也是更新商品详情后banner图片回调
@property (nonatomic,copy) ykMallChangeBannerImageBlock mallChangeBannerImageBlock;

- (void)yk_showInViewController:(YKMallDetailViewController *)mallDetailVC type:(YKMallType)type;

@end
