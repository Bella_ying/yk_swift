//
//  YKMallBannerCollectionReusableView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallBannerCollectionReusableView.h"
#import "YKMallListModel.h"
@interface YKMallBannerCollectionReusableView ()
@property (weak, nonatomic) IBOutlet UIImageView *bannerImgView;
@property(nonatomic,strong) YKMallBannerModel *bannerModel;
@end

@implementation YKMallBannerCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bannerImgView.layer.cornerRadius = 2.5;
    self.bannerImgView.clipsToBounds = YES;
    
    UITapGestureRecognizer *tapGesturRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    [self.bannerImgView addGestureRecognizer:tapGesturRecognizer];
}
-(void)tapAction:(id)tap
{
    if (self.bannerClickBlock) {
        self.bannerClickBlock(self.bannerModel);
    }
}
- (void)yk_renderBannerModels:(YKMallBannerModel*)model
{
    DLog(@"====%@",model.pic);
    self.bannerModel = model;
    [self.bannerImgView sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"mall_banner_zw"]];
}
@end
