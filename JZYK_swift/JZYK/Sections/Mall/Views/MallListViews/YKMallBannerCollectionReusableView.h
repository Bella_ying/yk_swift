//
//  YKMallBannerCollectionReusableView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallBannerModel;
typedef void(^ykBannerClickBlock)(YKMallBannerModel *bannerModel);
@interface YKMallBannerCollectionReusableView : UICollectionReusableView
@property (nonatomic,copy) ykBannerClickBlock bannerClickBlock;
- (void)yk_renderBannerModels:(YKMallBannerModel*)model;

@end
