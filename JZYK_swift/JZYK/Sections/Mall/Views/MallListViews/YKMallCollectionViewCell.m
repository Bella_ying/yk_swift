//
//  YKMallCollectionViewCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallCollectionViewCell.h"
#import "YKMallListModel.h"


@interface YKMallCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *payLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityLabelWidthLayout;

@end

@implementation YKMallCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 3;
    self.clipsToBounds = YES;
    self.activityLabel.layer.cornerRadius = 8;
    self.activityLabel.clipsToBounds = YES;
    self.activityLabel.layer.borderWidth = 0.5;
}
- (void)yk_renderCollectionViewCellWithMallListShopListItemModel:(YKMallListShopListItemModel*)model
{
    self.titleLabel.text = model.title;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.img_url] placeholderImage:[UIImage imageNamed:@"mall_list_image"]];
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",model.installment_price];
    self.payLabel.text = [NSString stringWithFormat:@"月供：最低%@",model.month_pay];
    if (model.activity_label.count) {
        self.activityLabel.hidden = NO;
        YKMallListShopListActivityLabel *  obj = [model.activity_label objectAtIndex:0];
        self.activityLabel.text = [NSString stringWithFormat:@"%@",obj.name];
        self.activityLabel.textColor = [UIColor yk_colorWithHexString:obj.color];
        self.activityLabel.layer.borderColor = [UIColor yk_colorWithHexString:obj.color].CGColor;
        CGSize size =  [self labelRectWithSize:CGSizeMake(0, 16) LabelText:self.activityLabel.text FontSize:10.0f];
        self.activityLabelWidthLayout.constant = size.width;
    }else{
      self.activityLabel.hidden = YES;
    }
}

- (CGSize)labelRectWithSize:(CGSize)size
                  LabelText:(NSString *)labelText
                   FontSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    CGSize actualsize = [labelText boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    actualsize.width  += 20;
    actualsize.height += 10;
    return actualsize;
}
@end
