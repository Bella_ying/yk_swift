//
//  YKMallCollectionViewCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallListShopListItemModel;
@interface YKMallCollectionViewCell : UICollectionViewCell
- (void)yk_renderCollectionViewCellWithMallListShopListItemModel:(YKMallListShopListItemModel*)model;
@end
