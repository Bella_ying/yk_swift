//
//  YKMallHeaderCollectionReusableView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallListModel;
@interface YKMallHeaderCollectionReusableView : UICollectionReusableView
- (void)yk_renderShopTitle:(YKMallListModel*)model;

@end
