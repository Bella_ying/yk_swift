//
//  YKMallHeaderCollectionReusableView.m
//  JZYK
//
//  Created by zhaoying on 2018/6/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallHeaderCollectionReusableView.h"
#import "YKMallListModel.h"
@interface YKMallHeaderCollectionReusableView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation YKMallHeaderCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)yk_renderShopTitle:(YKMallListModel*)model
{
    self.titleLabel.text = model.shop_list_title;
}
@end
