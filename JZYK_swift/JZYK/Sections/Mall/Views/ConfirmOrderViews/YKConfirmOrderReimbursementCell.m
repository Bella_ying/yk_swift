//
//  YKConfirmOrderReimbursementCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKConfirmOrderReimbursementCell.h"
#import "YKProtocolView.h"
#import "YKMallCheckModel.h"
@interface YKConfirmOrderReimbursementCell ()
@property (weak, nonatomic) IBOutlet YKProtocolView *protocolView;
@property (weak, nonatomic) IBOutlet UIButton *confirmOrderBtn;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIButton *tipBtn;
@property (nonatomic,strong)  NSMutableArray *protocolArray;
- (IBAction)confirmOrderBtnClick:(id)sender;
- (IBAction)tipBtnClick:(id)sender;

@end

@implementation YKConfirmOrderReimbursementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
   self.selectionStyle = UITableViewCellSelectionStyleNone;
   self.protocolView = YKProtocolView.makeProtocol(self, self.confirmOrderBtn);
   self.protocolView.btnSelected = YES;
}
//每月应该还的金额
- (void)yk_renderPayAmount:(YKMallCheckMonthPayAmountModel*)model
{
    self.amountLabel.text = [NSString stringWithFormat:@"%@元",model.pay_amount];
}
//服务协议
- (void)yk_renderAgreementInfo:(YKMallCheckModel*)model
{
    [self.protocolArray removeAllObjects];
    WEAK_SELF
     //因为此处使用的协议比较特殊，不能使用模型数组；故将model转化为字典,再添加到数组中
    [model.agreement_info enumerateObjectsUsingBlock:^(YKMallCheckAgreementInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *jsonDic = [obj yy_modelToJSONObject];
        [weakSelf.protocolArray addObject:jsonDic];
    }];
    
    [self.protocolView yk_refreshProtocolTextEntity:weakSelf.protocolArray protocolString:@"text" url:@"url" tapAction:^(NSAttributedString * _Nonnull text, NSDictionary * _Nonnull obj, NSUInteger idx) {
         DLog(@"======%@",obj);
        if (weakSelf.selectProtocolIndexBlock) {
            weakSelf.selectProtocolIndexBlock(obj[@"url"]);
        }
    }];
}
- (NSMutableArray *)protocolArray
{
    if (!_protocolArray) {
        _protocolArray =[@[]mutableCopy];
    }
    return _protocolArray;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)confirmOrderBtnClick:(id)sender
{
    if (self.confirmBtnClickBlock) {
        self.confirmBtnClickBlock();
    }
}

- (IBAction)tipBtnClick:(id)sender
{
    if (self.tipBtnClickBlock) {
        self.tipBtnClickBlock();
    }
}
@end
