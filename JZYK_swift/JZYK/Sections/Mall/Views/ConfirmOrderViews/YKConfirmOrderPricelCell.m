//
//  YKConfirmOrderPricelCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKConfirmOrderPricelCell.h"
#import "YKMallCheckModel.h"
@interface YKConfirmOrderPricelCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightImgView;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@end

@implementation YKConfirmOrderPricelCell

- (void)awakeFromNib {
    [super awakeFromNib];
     self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1.0/[UIScreen mainScreen].scale);
    }];
}
//商品价格、运费、总价的cell
- (void)yk_renderConfirmOrderPricelCell:(YKMallCheckShopDetailModel*)model indexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
           self.contentLabel.text = [NSString stringWithFormat:@"%.2f元",model.installment_price];
            self.titleLabel.text = @"商品总价";
        }else if (indexPath.row == 1){
            self.titleLabel.text = @"运费";
            self.contentLabel.text = [NSString stringWithFormat:@"%.2f元",model.freight];
        }else{
            self.titleLabel.text = @"订单总额";
            self.contentLabel.text = [NSString stringWithFormat:@"%.2f元",model.order_money];
            self.lineView.hidden = YES;
        }
    }
}
//还款方式cell
- (void)yk_renderPaymentMethodCell:(YKMallCheckMoreInfoModel*)model indexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section == 3) {
        self.titleLabel.text = @"支付方式";
        self.contentLabel.text = @"分期支付";
        //在1.0.0的版本中支付方式，只有分期支付；以后的版本再添加别的支付方式，下面的代码解注释即可
//        [model.payment_method enumerateObjectsUsingBlock:^(YKMallCheckPaymentMethodModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            if (idx == 0) {
//                self.contentLabel.text = obj.text;
//            }
//        }];
          self.rightImgView.hidden = NO;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
