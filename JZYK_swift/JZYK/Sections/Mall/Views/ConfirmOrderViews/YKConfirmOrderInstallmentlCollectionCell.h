//
//  YKConfirmOrderInstallmentlCollectionCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallCheckMoreInfoModel;
@interface YKConfirmOrderInstallmentlCollectionCell : UICollectionViewCell

- (void)yk_renderModel:(YKMallCheckMoreInfoModel*)checkMoreInfoModel indexPath:(NSIndexPath*)indexPath;
@end
