//
//  YKConfirmOrderMallCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallCheckShopDetailModel;
@interface YKConfirmOrderMallCell : UITableViewCell
- (void)yk_renderConfirmOrderMallCell:(YKMallCheckShopDetailModel*)model;
@end
