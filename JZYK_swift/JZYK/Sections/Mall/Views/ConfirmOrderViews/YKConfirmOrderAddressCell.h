//
//  YKConfirmOrderAddressCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKAddressListModel;
@interface YKConfirmOrderAddressCell : UITableViewCell
- (void)yk_renderConfirmOrderAddress:(YKAddressListModel*)model;
@end
