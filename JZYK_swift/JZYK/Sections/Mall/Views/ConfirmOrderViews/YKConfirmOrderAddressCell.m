//
//  YKConfirmOrderAddressCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKConfirmOrderAddressCell.h"
#import "YKAddressListModel.h"
@interface YKConfirmOrderAddressCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end
@implementation YKConfirmOrderAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}
- (void)yk_renderConfirmOrderAddress:(YKAddressListModel*)model
{
    self.nameLabel.text = model.name;
    self.phoneNumberLabel.text = model.mobile;
    self.addressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",model.province,model.city,model.county,model.town,model.address];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
