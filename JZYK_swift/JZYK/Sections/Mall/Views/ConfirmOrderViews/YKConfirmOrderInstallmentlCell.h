//
//  YKConfirmOrderInstallmentlCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallCheckMoreInfoModel;
@class YKMallCheckMonthPayAmountModel;
typedef void(^ykMoreBlock)(void);
typedef void(^ykSelectBlock)(YKMallCheckMonthPayAmountModel *model);

@interface YKConfirmOrderInstallmentlCell : UITableViewCell
@property (nonatomic,copy) ykMoreBlock moreBlock;
@property (nonatomic,copy) ykSelectBlock selectBlock;

- (void)yk_renderConfirmOrderInstallmentlCell:(YKMallCheckMoreInfoModel*)model;
@end
