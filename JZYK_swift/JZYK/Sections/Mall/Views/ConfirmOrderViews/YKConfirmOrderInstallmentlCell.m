//
//  YKConfirmOrderInstallmentlCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKConfirmOrderInstallmentlCell.h"
#import "YKConfirmOrderInstallmentlCollectionCell.h"
#import "YKMallPecificationsCollectionCell.h"
#import "YKMallCheckModel.h"
@interface YKConfirmOrderInstallmentlCell ()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) NSArray *installmentlModelArray;
@property (nonatomic,strong) YKMallCheckMoreInfoModel*checkMoreInfoModel;
@end

static NSString * const cellId  = @"ConfirmOrderInstallmentlCollectionCell";

static NSString * const pecificationsCollectionCellId  = @"YKMallPecificationsCollectionCell";


@implementation YKConfirmOrderInstallmentlCell

- (void)awakeFromNib {
    [super awakeFromNib];
     self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self prepareUI];
}
- (void)prepareUI
{
    self.collectionView.scrollEnabled = NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKConfirmOrderInstallmentlCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellId];
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKMallPecificationsCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:pecificationsCollectionCellId];

}
- (void)yk_renderConfirmOrderInstallmentlCell:(YKMallCheckMoreInfoModel*)model
{
    self.installmentlModelArray = model.month_pay_amount_arr;
    self.checkMoreInfoModel = model;
}
# pragma mark - 协议方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.installmentlModelArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YKConfirmOrderInstallmentlCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    [cell yk_renderModel:self.checkMoreInfoModel indexPath:indexPath];
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    WEAK_SELF
    //如果当前没有展开并且点击的是第3个的话，就展开，原来点击的是第几个，还保持在第几个
    if (!weakSelf.checkMoreInfoModel.isShow && indexPath.row == 2) {
        weakSelf.checkMoreInfoModel.isShow = YES;
        //点击了“更多”按钮
        if (weakSelf.moreBlock) {
            weakSelf.moreBlock();
        }
        [collectionView reloadData];
        return;
    }
    //如果当前展开了，点击的选中，没点击的不选中
    [self.checkMoreInfoModel.month_pay_amount_arr enumerateObjectsUsingBlock:^(YKMallCheckMonthPayAmountModel * _Nonnull checkMonthPayAmountModel, NSUInteger idx, BOOL * _Nonnull stop) {
        //是选中的
        if (idx == indexPath.row) {
            checkMonthPayAmountModel.isSelect = YES;
            if (self.selectBlock) {
                self.selectBlock(checkMonthPayAmountModel);
            }
        }else{
            checkMonthPayAmountModel.isSelect = NO;
        }
    }];
    [collectionView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
