//
//  YKConfirmOrderInstallmentlCollectionCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKConfirmOrderInstallmentlCollectionCell.h"
#import "YKMallCheckModel.h"
@interface YKConfirmOrderInstallmentlCollectionCell ()
@property (weak, nonatomic) IBOutlet UILabel *installmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *payLabel;
@property (weak, nonatomic) IBOutlet UILabel *moreLabel;

@end

@implementation YKConfirmOrderInstallmentlCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderColor = LINE_COLOR.CGColor ;
    self.layer.borderWidth = 1;
    self.layer.cornerRadius = 2;
    self.clipsToBounds = YES;
}
- (void)yk_renderModel:(YKMallCheckMoreInfoModel*)checkMoreInfoModel indexPath:(NSIndexPath*)indexPath
{
    if (indexPath.row < checkMoreInfoModel.month_pay_amount_arr.count) {
        YKMallCheckMonthPayAmountModel *model = checkMoreInfoModel.month_pay_amount_arr[indexPath.row];
        
        self.payLabel.text = [NSString stringWithFormat:@"月供%@元",model.pay_amount];
        self.installmentLabel.text = [NSString stringWithFormat:@"%@期",model.month];
        
        if (!checkMoreInfoModel.isShow && indexPath.row == 2) {
            //没有展开，显示“更多”
            self.payLabel.hidden = YES;
            self.installmentLabel.hidden = YES;
            self.moreLabel.hidden = NO;
        }else{
            self.payLabel.hidden = NO;
            self.installmentLabel.hidden = NO;
            self.moreLabel.hidden = YES;
        }
        
        if (model.isSelect) {
            self.payLabel.textColor = MAIN_THEME_COLOR;
            self.installmentLabel.textColor = MAIN_THEME_COLOR;
            self.moreLabel.textColor = MAIN_THEME_COLOR;
            self.layer.borderColor = MAIN_THEME_COLOR.CGColor ;
        }else{
            self.payLabel.textColor = LABEL_TEXT_COLOR;
            self.installmentLabel.textColor = LABEL_TEXT_COLOR;
            self.moreLabel.textColor = LABEL_TEXT_COLOR;
            self.layer.borderColor = LINE_COLOR.CGColor ;
        }
    }
}
@end
