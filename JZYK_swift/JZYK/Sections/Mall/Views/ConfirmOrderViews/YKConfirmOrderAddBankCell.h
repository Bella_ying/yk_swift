//
//  YKConfirmOrderAddBankCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/22.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKConfirmOrderAddBankCell : UITableViewCell
- (void)yk_renderConfirmOrderAddBankCell:(NSString*)userCard;
@end
