//
//  YKConfirmOrderMallCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKConfirmOrderMallCell.h"
#import "YKMallCheckModel.h"
@interface YKConfirmOrderMallCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
@implementation YKConfirmOrderMallCell

- (void)awakeFromNib {
    [super awakeFromNib];
     self.selectionStyle = UITableViewCellSelectionStyleNone;
}
- (void)yk_renderConfirmOrderMallCell:(YKMallCheckShopDetailModel*)model
{
    self.titleLabel.text = model.title;
    self.descLabel.text = [NSString stringWithFormat:@"%@/%ld件",model.installment_option,model.buy_num];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.img_url] placeholderImage:[UIImage imageNamed:@"mall_confirmOrder_zw"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
