//
//  YKConfirmOrderAddBankCell.m
//  JZYK
//
//  Created by zhaoying on 2018/6/22.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKConfirmOrderAddBankCell.h"
@interface YKConfirmOrderAddBankCell ()
@property (weak, nonatomic) IBOutlet UILabel *addBankLabel;

@end

@implementation YKConfirmOrderAddBankCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}
- (void)yk_renderConfirmOrderAddBankCell:(NSString*)userCard
{
    if (userCard.length) {
        self.addBankLabel.text = userCard;
        self.addBankLabel.textColor = GRAY_COLOR_45;
    }else{
        self.addBankLabel.text = @"添加银行卡";
        self.addBankLabel.textColor = MAIN_THEME_COLOR;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
