//
//  YKConfirmOrderPricelCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallCheckShopDetailModel;
@class YKMallCheckMoreInfoModel;
@interface YKConfirmOrderPricelCell : UITableViewCell
- (void)yk_renderConfirmOrderPricelCell:(YKMallCheckShopDetailModel*)model indexPath:(NSIndexPath*)indexPath;
- (void)yk_renderPaymentMethodCell:(YKMallCheckMoreInfoModel*)model indexPath:(NSIndexPath*)indexPath;

@end
