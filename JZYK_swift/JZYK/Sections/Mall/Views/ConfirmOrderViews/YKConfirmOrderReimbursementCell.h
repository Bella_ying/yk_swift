//
//  YKConfirmOrderReimbursementCell.h
//  JZYK
//
//  Created by zhaoying on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKMallCheckModel;
@class YKMallCheckMonthPayAmountModel;

typedef void(^ykTipBtnClickBlock)(void);
typedef void(^ykConfirmBtnClickBlock)(void);
typedef void(^ykSelectProtocolIndexBlock)(NSString *url);

@interface YKConfirmOrderReimbursementCell : UITableViewCell
@property (nonatomic,copy) ykTipBtnClickBlock tipBtnClickBlock;
@property (nonatomic,copy) ykConfirmBtnClickBlock confirmBtnClickBlock;
@property (nonatomic ,copy) ykSelectProtocolIndexBlock selectProtocolIndexBlock;

- (void)yk_renderPayAmount:(YKMallCheckMonthPayAmountModel*)model;
- (void)yk_renderAgreementInfo:(YKMallCheckModel*)model;
@end
