//
//  YKMallShowViewController.h
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"

@interface YKMallShowViewController : YKBaseViewController
@property (nonatomic,copy) NSString *installment_type;//选项卡对应的key值
@end
