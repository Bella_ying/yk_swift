//
//  YKMallCollectionView.h
//  JZYK
//
//  Created by zhaoying on 2018/6/25.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKMallCollectionView : UICollectionView
@property (nonatomic,strong) UINavigationController *targetController;
/**
  加载商城首页数据

 @param installmentType 选项卡对应的key值
 @param isLoadMore 是否是上拉加载更多
 */
-(void)fetchMallHomeShopListData:(NSString *)installmentType isLoadMore:(BOOL)isLoadMore;
@end
