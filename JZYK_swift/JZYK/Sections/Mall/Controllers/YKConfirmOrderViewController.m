//
//  YKConfirmOrderViewController.m
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKConfirmOrderViewController.h"
#import "YKConfirmOrderAddressCell.h"
#import "YKConfirmOrderNoAddressCell.h"
#import "YKConfirmOrderMallCell.h"
#import "YKConfirmOrderPricelCell.h"
#import "YKConfirmOrderInstallmentlCell.h"
#import "YKConfirmOrderReimbursementCell.h"
#import "YKMallCustomViewController.h"
#import "YKAddressListVC.h"
#import "YKTradePswPopup.h"
#import "YKAddressListModel.h"
#import "YKMallCheckModel.h"
#import "YKOrderListVC.h"
#import "YKAddBankCardViewController.h"
#import "YKConfirmOrderAddBankCell.h"
#import "YKVerifyGuideViewController.h"
#import "YKVerifyCenterListVC.h"
#import "YKCunGunTools.h"

static const CGFloat   confirmOrderInstallmentlHeight = 42.f;
static const CGFloat   confirmOrderInstallmentlLineHeight = 10.f;
static const CGFloat   confirmOrderInstallmentHeight = 56;

@interface YKConfirmOrderViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,assign)   CGFloat   confirmOrderInstallmentlSectionHeight ;
@property (nonatomic,strong) YKAddressListModel *addresslistModel;
@property (nonatomic,strong) YKMallCheckMonthPayAmountModel *selectCheckMonthPayAmountModel;//选中分期的model
@property (nonatomic,strong) NSMutableArray *payMethodArray;
@property (nonatomic,strong) YKTradePswPopup *tradePswPopup;//交易密码的弹框
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomLayout;


@end

static NSString * const kAddressCell = @"AddressCell";
static NSString * const kNoAddressCell = @"NoAddressCell";
static NSString * const kMallCell = @"MallCell";
static NSString * const kPricelCell = @"PricelCell";
static NSString * const kInstallmentlCell = @"InstallmentlCell";
static NSString * const kReimbursementCell = @"ReimbursementCell";
static NSString * const kAddBankCell = @"AddBankCell";

@implementation YKConfirmOrderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"确认订单";
    self.tableViewBottomLayout.constant = BOTTOM_HEIGHT;
    self.confirmOrderInstallmentlSectionHeight = 100;
    if (self.mallCheckModel.more_info.month_pay_amount_arr.count) {
        self.selectCheckMonthPayAmountModel = self.mallCheckModel.more_info.month_pay_amount_arr[0];
    }
    [self registerCell];
    [self fetchUserAddressDefaultData];
    [self monthPayAmountArrryDefaultSelect];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
- (void)registerCell
{
    [self.tableView registerNib:[UINib nibWithNibName:@"YKConfirmOrderAddressCell" bundle:nil] forCellReuseIdentifier:kAddressCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKConfirmOrderNoAddressCell" bundle:nil] forCellReuseIdentifier:kNoAddressCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKConfirmOrderMallCell" bundle:nil] forCellReuseIdentifier:kMallCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKConfirmOrderPricelCell" bundle:nil] forCellReuseIdentifier:kPricelCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKConfirmOrderInstallmentlCell" bundle:nil] forCellReuseIdentifier:kInstallmentlCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKConfirmOrderAddBankCell" bundle:nil] forCellReuseIdentifier:kAddBankCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKConfirmOrderReimbursementCell" bundle:nil] forCellReuseIdentifier:kReimbursementCell];

    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
}

//MARK:tableviewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0 :case 1:
            return 1;
            break;
        case 2 :
            return 3;
            break;
        case 3:
            return 4;
            break;
        default:
            break;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     WEAK_SELF
    switch (indexPath.section) {
        case 0:
        {
            if (self.addresslistModel) {
                //有地址
                YKConfirmOrderAddressCell  *cell = [tableView dequeueReusableCellWithIdentifier:kAddressCell forIndexPath:indexPath];
                [cell yk_renderConfirmOrderAddress:self.addresslistModel];
                return cell;
            }else{
                //没有地址
                YKConfirmOrderNoAddressCell  *cell = [tableView dequeueReusableCellWithIdentifier:kNoAddressCell forIndexPath:indexPath];
                return cell;
            }
        }
            break;
        case 1:
        {
            YKConfirmOrderMallCell  *cell = [tableView dequeueReusableCellWithIdentifier:kMallCell forIndexPath:indexPath];
            [cell yk_renderConfirmOrderMallCell:self.mallCheckModel.shop_detail];
            return cell;
        }
            break;
        case 2:
        {
            
            YKConfirmOrderPricelCell  *cell = [tableView dequeueReusableCellWithIdentifier:kPricelCell forIndexPath:indexPath];
            [cell yk_renderConfirmOrderPricelCell:self.mallCheckModel.shop_detail indexPath:indexPath];
            return cell;
        }
            break;
        case 3:
        {
            if (indexPath.row == 0) {
                YKConfirmOrderPricelCell  *cell = [tableView dequeueReusableCellWithIdentifier:kPricelCell forIndexPath:indexPath];
                [cell yk_renderPaymentMethodCell:self.mallCheckModel.more_info indexPath:indexPath];
                return cell;
            }else if (indexPath.row == 1){
                YKConfirmOrderInstallmentlCell  *cell = [tableView dequeueReusableCellWithIdentifier:kInstallmentlCell forIndexPath:indexPath];
                [cell yk_renderConfirmOrderInstallmentlCell:self.mallCheckModel.more_info];
                [cell setMoreBlock:^{
                    //点击了更多
                    NSInteger number = weakSelf.mallCheckModel.more_info.month_pay_amount_arr.count;
                    if (number %3 == 0) {
                        self.confirmOrderInstallmentlSectionHeight = (number / 3) * (confirmOrderInstallmentlHeight+confirmOrderInstallmentlLineHeight) + confirmOrderInstallmentHeight;
                    }else{
                        self.confirmOrderInstallmentlSectionHeight = ((number /3) +1) * (confirmOrderInstallmentlHeight+confirmOrderInstallmentlLineHeight) + confirmOrderInstallmentHeight;
                    }
                    [weakSelf.tableView reloadData];
                }];
                [cell setSelectBlock:^(YKMallCheckMonthPayAmountModel *model) {
                    weakSelf.selectCheckMonthPayAmountModel = model;
                    [weakSelf.tableView reloadData];
                }];
                return cell;
            }else if (indexPath.row == 2){
                YKConfirmOrderAddBankCell  *cell = [tableView dequeueReusableCellWithIdentifier:kAddBankCell forIndexPath:indexPath];
                [cell yk_renderConfirmOrderAddBankCell:self.mallCheckModel.more_info.user_card];
                return cell;
            }else{
                YKConfirmOrderReimbursementCell  *cell = [tableView dequeueReusableCellWithIdentifier:kReimbursementCell forIndexPath:indexPath];
                [cell yk_renderPayAmount:self.selectCheckMonthPayAmountModel];
                [cell yk_renderAgreementInfo:self.mallCheckModel];
                [cell setTipBtnClickBlock:^{
                    YKMallCustomViewController *VC = [YKMallCustomViewController new];
                    VC.checkMonthPayAmountModel = weakSelf.selectCheckMonthPayAmountModel;
                    [VC yk_showInViewController:self type:YKMallTypePaymentDetails];
                }];
                
                [cell setConfirmBtnClickBlock:^{
                    //生成订单
                    [weakSelf fetchOrderData];
                }];
               
                [cell setSelectProtocolIndexBlock:^(NSString *url) {
                    YKBrowseWebController *webVC = [YKBrowseWebController new];
                    webVC.url = url;
                    [weakSelf.navigationController pushViewController:webVC animated:YES];
                }];
                return cell;
            }
            
        }
            break;
        default:
            break;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            if (self.addresslistModel) {
                return 80;
            }else{
                return 66;
            }
        }
            break;
        case 1:
        {
            NSString *title = self.mallCheckModel.shop_detail.title;
            CGSize titleSize =   [YKTools yk_labelRectWithSize:CGSizeMake(WIDTH_OF_SCREEN - 141, 0) LabelText:title font:[UIFont boldSystemFontOfSize:16.0f]];

            NSString *option = [NSString stringWithFormat:@"%@/%ld件",self.mallCheckModel.shop_detail.installment_option,self.mallCheckModel.shop_detail.buy_num];
            CGSize optionSize =   [YKTools yk_labelRectWithSize:CGSizeMake(WIDTH_OF_SCREEN - 141, 0) LabelText:option font:[UIFont systemFontOfSize:14.0f]];
            if (titleSize.height + optionSize.height + 57 < 122) {
                return 122;
            }
            return titleSize.height + optionSize.height + 57;
           
        }
            break;
        case 2:
        {
            return 45;
        }
            break;
        case 3:
        {
            if (indexPath.row == 0 || indexPath.row == 2) {
                return 45;
            }else if (indexPath.row == 1){
               return self.confirmOrderInstallmentlSectionHeight;
            }else{
                return 200;
            }
        }
            break;
        default:
            break;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section != 0) {
        return 20;
    }
    return 0;
}

//MARK:tableviewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            //选择地址
            YKAddressListVC *addressVC = [YKAddressListVC new];
            addressVC.isFromMallConfirmOrder = YES;
            WEAK_SELF
            [addressVC setAddressListBlock:^(YKAddressListModel *model) {
                weakSelf.addresslistModel = model;
                //刷新地址行
                [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }];
            [self.navigationController pushViewController:addressVC animated:YES];
        }
            break;
        case 3:
        {
            if (indexPath.row == 0) {
                //选择还款方式
                [self.payMethodArray removeAllObjects];
                WEAK_SELF
                [self.mallCheckModel.more_info.payment_method enumerateObjectsUsingBlock:^(YKMallCheckPaymentMethodModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *jsonDic = [obj yy_modelToJSONObject];
                    [weakSelf.payMethodArray addObject:jsonDic[@"text"]];
                }];
                [UIAlertController ba_actionSheetShowInViewController:self
                                                                title:nil
                                                              message:nil
                                                     buttonTitleArray:weakSelf.payMethodArray
                                                buttonTitleColorArray:@[GRAY_COLOR_45,
                                                                        GRAY_COLOR_45]
                                   popoverPresentationControllerBlock:nil
                                                                block:^(UIAlertController * _Nonnull alertController, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                                                                    
                                                                    
                                                                }];
            }else if (indexPath.row == 2){
                //还款银行卡,user_card字符串为空时,请求接口，判断一下用户是否实名认证，如果接口code= 0，获取银行卡信息成功；code = 1，调用存管接口；如果code= 1008，用户未实名认证，跳转到实名认证页面，让用户认证
                if (![self.mallCheckModel.more_info.user_card yk_isValidString]) {
                    [self fetchUserCardData];
                }
            }
        }
            break;
        default:
            break;
    }
   
}
//MARK:判断一下用户是否实名认证，如果接口code= 0，用户实名认证了，直接跳转到添加银行卡页面；如果code= 1008，用户未实名认证，跳转到实名认证页面，让用户认证；如果code= 1011，授信未完成,跳转至认证向导
- (void)fetchUserCardData
{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kMallGetUserCard showLoading:NO param:@{} succeed:^(NSDictionary * _Nonnull jsonDic, NSInteger code, BOOL success, NSString *message) {
        DLog(@"===%@",jsonDic);
        if (code == 0 && success) {
           //获取银行卡信息成功
        }else if(code == 1){
            //调用存管的接口
            [self fetchMallCardGetDepositOpenInfoData:NO isSetTradePsw:NO];
        }else if(code == 1011){
            //授信未完成,跳转至认证向导
            if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[YKVerifyGuideViewController class]]) {
                YKVerifyGuideViewController *guideVC = [YKVerifyGuideViewController new];
                [weakSelf.navigationController pushViewController:guideVC animated:YES];
            }
        }else if(code == 1008){
            //认证过期或缺失必要认证项,若有过期则跳转认证中心
            if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[YKVerifyCenterListVC class]]) {
                YKVerifyCenterListVC *centerVC = [YKVerifyCenterListVC new];
                [self.navigationController pushViewController:centerVC animated:YES];
            }
        }else{
            [[iToast makeText:message] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}
//MARK:生成订单
- (void)fetchOrderData
{
    if (!self.addresslistModel) {
        [[QLAlert alert] showWithMessage:@"请添加收货地址哦" singleBtnTitle:@"确定"];
        return;
    }
    [self commitOrderRequestWithPsw:@"" tradePswErrorLab:nil isCheckOrder:YES];
}
//MARK:获取默认地址
- (void)fetchUserAddressDefaultData
{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kAddressUserAddressDefault showLoading:NO param:@{} succeed:^(NSDictionary * _Nonnull jsonDic, NSInteger code, BOOL success, NSString *message) {
        DLog(@"===%@",jsonDic);
        if (code == 0) {
            if (!success) {
                //解析成功,但数据为空
                weakSelf.addresslistModel = nil;
            }else{
                //解析成功,但数据不为空
                weakSelf.addresslistModel = [YKAddressListModel yy_modelWithJSON:jsonDic];
            }
            [weakSelf.tableView reloadData];
        }else{
            [[iToast makeText:message] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
        
    }];
}

//MARK:校验/确认订单
/*
 先校验订单，再调用存管的存管的接口，再生成订单
1、点击“确认订单”按钮，先校验，校验就是不传“交易密码”字段，
 如果code = 2，表示没有设置过交易密码，则跳转到设置交易密码页面，让用户设置交易密码，用户设置成功之后，将设置成功的交易密码回传，接着进行确认订单逻辑；
 如果code = 3，表示设置过交易密码，调用存管的接口，走存管的逻辑；如果存管的逻辑返回code = 0 ，则弹出交易密码弹框；
 如果code = 1008，表示认证过期或缺失必要认证项,若有过期则跳转认证中心；
 如果code = 1011，表示授信未完成,跳转至认证向导；
 2、校验通过之后下单传“交易密码”字段；如果code = 3，表示输入的交易密码错误，后台返回剩余设置交易密码次数；
 */
- (void)commitOrderRequestWithPsw:(NSString *)psw tradePswErrorLab:(UILabel *)errorLabel isCheckOrder:(BOOL)isCheckOrder
{
    NSDictionary *param = @{};
    if (isCheckOrder) {
        //如果是订单校验的话，不传交易密码
       param = @{@"shop_id":@(self.mallCheckModel.shop_detail.shop_id),
                                @"installment_option":self.mallCheckModel.shop_detail.installment_option,
                                @"buy_num":@(self.mallCheckModel.shop_detail.buy_num),
                                @"installment_month":self.selectCheckMonthPayAmountModel.month,
                                @"shipping_address_id":@(self.addresslistModel.addressID)};
    }else{
        //如果不是订单校验的话，传交易密码
       param = @{@"shop_id":@(self.mallCheckModel.shop_detail.shop_id),
                                @"installment_option":self.mallCheckModel.shop_detail.installment_option,
                                @"buy_num":@(self.mallCheckModel.shop_detail.buy_num),
                                @"installment_month":self.selectCheckMonthPayAmountModel.month,
                                @"shipping_address_id":@(self.addresslistModel.addressID),
                                @"pay_password":psw};
    }
    
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kMallShopInstallmentPost showLoading:YES param:param succeed:^(NSDictionary * _Nonnull jsonDic, NSInteger code, BOOL success, NSString *message) {
        if (code == 0 && success) {
            //下单成功，跳转到订单详情web页,点击订单详情返回到商城首页
            DLog(@"======%@",jsonDic);
            [weakSelf.tradePswPopup animateHide];
            [[iToast makeText:message] show];
            NSString *url = jsonDic[@"redirect_url"];
            YKBrowseWebController *webVC = [[YKBrowseWebController alloc] init];
            webVC.url = url;
            webVC.popToDesignatedPage = PopToDesignatedPageMall;
            [weakSelf.navigationController pushViewController:webVC animated:YES];
           
        }else if(code == 2){
            //先调用存管的逻辑，存管逻辑成功之后，再去设置交易密码
            [weakSelf fetchMallCardGetDepositOpenInfoData:NO isSetTradePsw:YES];
        }else if(code == 3){
            if (isCheckOrder) {
                //表示设置过交易密码，调用存管的接口，走存管的逻辑；
                [weakSelf fetchMallCardGetDepositOpenInfoData:YES isSetTradePsw:NO];
            }else{
                //输入的交易密码错误，后台返回剩余设置交易密码次数
                errorLabel.text = message;
            }
        }else if(code == 1011){
           //授信未完成,跳转至认证向导
            [weakSelf.tradePswPopup animateHide];
            if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[YKVerifyGuideViewController class]]) {
                YKVerifyGuideViewController *guideVC = [YKVerifyGuideViewController new];
                [weakSelf.navigationController pushViewController:guideVC animated:YES];
            }
        }else if(code == 1008){
            [weakSelf.tradePswPopup animateHide];
           //认证过期或缺失必要认证项,若有过期则跳转认证中心
            if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[YKVerifyCenterListVC class]]) {
                YKVerifyCenterListVC *centerVC = [YKVerifyCenterListVC new];
                [weakSelf.navigationController pushViewController:centerVC animated:YES];
            }
        }else{
            [[iToast makeText:message] show];
            [weakSelf.tradePswPopup animateHide];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        if (!isConnect) {
            [[iToast makeText:errMsg] show];
            [weakSelf.tradePswPopup animateHide];
        }
    }];
}
//MARK:获取用户是否开通存管
- (void)fetchMallCardGetDepositOpenInfoData:(BOOL)isCheckOrder isSetTradePsw:(BOOL)isSetTradePsw
{
    WEAK_SELF
    [YKCunGunTools yk_featchCunGuanDataWithCunguanFinishBlock:^{
        if (isCheckOrder) {
            //是校验订单，开通存管成功，弹出交易密码的弹框
            [weakSelf  popUpTradePsw];
        }
        if (isSetTradePsw) {
            //表示没有设置过交易密码，则跳转到设置交易密码页面，让用户设置交易密码，用户设置成功之后，将设置成功的交易密码回传，接着进行确认订单逻辑
            [weakSelf toSetTradePsw];
        }
    } bindBankComplete:^(BOOL bankPageBlock,NSString * card_desc, NSString *card_id, NSString *bank_name) {
        weakSelf.mallCheckModel.more_info.user_card = card_desc;
        [weakSelf.tableView reloadData];
    }];
}
//MARK:弹出交易密码弹框
- (void)popUpTradePsw
{
    WEAK_SELF
        //设置过交易密码，弹出交易密码的弹框
        self.tradePswPopup =   [YKTradePswPopup makeTradePswPopWithInputCompletion:^(NSString * _Nonnull inputStr, UILabel * _Nullable errorLab) {
            DLog(@"====%@",inputStr);
            [weakSelf commitOrderRequestWithPsw:inputStr tradePswErrorLab:errorLab isCheckOrder:NO];
        }](@"订单总额",[NSString stringWithFormat:@"%.2f",self.mallCheckModel.shop_detail.order_money]).animateShow();
}
//MARK:去设置交易密码
- (void)toSetTradePsw
{
    WEAK_SELF
    //没有设置过交易密码,去交密码中设置
    TransactionCodeController *transacVc = [TransactionCodeController new];
    transacVc.typeNum = 1;
    transacVc.isShowAlert = NO;
    [self.navigationController pushViewController:transacVc animated:YES];
    [transacVc setTransactionSetSuccess:^(NSString * _Nonnull pwd) {
        DLog(@"====%@",pwd);
        [weakSelf commitOrderRequestWithPsw:pwd tradePswErrorLab:nil isCheckOrder:NO];
        
    }];
}
- (void)monthPayAmountArrryDefaultSelect
{
    [self.mallCheckModel.more_info.month_pay_amount_arr enumerateObjectsUsingBlock:^(YKMallCheckMonthPayAmountModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) {
            //默认选中第1个
            obj.isSelect = YES;
        }else{
            obj.isSelect = NO;
        }
    }];
}
- (NSMutableArray*)payMethodArray
{
    if (!_payMethodArray) {
        _payMethodArray = [@[]mutableCopy];
    }
    return _payMethodArray;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
