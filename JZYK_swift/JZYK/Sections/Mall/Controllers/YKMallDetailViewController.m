//
//  YKMallDetailViewController.m
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallDetailViewController.h"
#import "YKMallDetailBannerCell.h"
#import "YKMallDetailSpecificationsCell.h"
#import "YKMallDetailProductItemInfoCell.h"
#import "YKMallDetailRebateCell.h"
#import "YKMallDetailShipFeeAfterSaleCell.h"
#import "YKMallDetailBottomBarView.h"
#import "YKMallCustomViewController.h"
#import "YKConfirmOrderViewController.h"
#import "YKMallDetailModel.h"
#import "YKMallDetailNavigationView.h"
#import "YKMallCheckModel.h"
#import "YKLocationManager.h"
#import "YKLocationPicker.h"
#import "HYProgressHUD.h"

@interface YKMallDetailViewController ()<UIWebViewDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic,strong) YKMallDetailModel *detailModel;
@property (nonatomic,strong) YKMallDetailNavigationView *mallDetailNavigationView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewTopLayout;
@property (nonatomic,strong)  YKMallDetailBottomBarView *bottomBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBtnBottomLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewBottomLayout;
- (IBAction)toTopBtnClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *tabViewBgView;

@end

static  NSInteger const sectionCount = 2;

static NSString * const kBannerCell = @"BannerCell";
static NSString * const kProductItemInfoCell = @"ProductItemInfoCell";
static NSString * const kSpecificationsCell = @"SpecificationsCell";
static NSString * const kRebateCell = @"RebateCell";
static NSString * const kShipFeeAfterSaleCell = @"ShipFeeAfterSaleCell";


static  NSInteger const tableViewRowHeight = 45;
static  NSInteger const bottomViewHeight = 46;


static NSString * const kDefaultAddress = @"北京市 海淀区";//若获取不到定位，则默认一个定位。（北京市 海淀区）
static NSString * const kRefreshFooterTitle = @"上拉加载商品详情";//上拉时提示文案
static NSString * const kRefreshHeaderTitle = @"下拉关闭商品详情";//下拉时提示文案

@implementation YKMallDetailViewController
//MARK:life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.showNavigationBar = NO;
    [self prepareNavView];
    [self registerCell];
    [self loadBottomBarView];
    [self setContentInsetAdjustmentBehavior];
    [self setRefreshHeaderFooter];
    [self fetchMallDetailDataWithShopId:self.shopList_id buyNum:1 mallOptionShopIdModelArray:nil defaultAddress:@""];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height *2);

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}
//MARK:private methods
- (void)registerCell
{
    [self.tableView registerNib:[UINib nibWithNibName:@"YKMallDetailBannerCell" bundle:nil] forCellReuseIdentifier:kBannerCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKMallDetailProductItemInfoCell" bundle:nil] forCellReuseIdentifier:kProductItemInfoCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKMallDetailSpecificationsCell" bundle:nil] forCellReuseIdentifier:kSpecificationsCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKMallDetailRebateCell" bundle:nil] forCellReuseIdentifier:kRebateCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"YKMallDetailShipFeeAfterSaleCell" bundle:nil] forCellReuseIdentifier:kShipFeeAfterSaleCell];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
}

- (void)loadBottomBarView
{
    self.bottomBarView =[[[NSBundle mainBundle] loadNibNamed:@"YKMallDetailBottomBarView" owner:nil options:nil] firstObject];
    [self.view addSubview:self.bottomBarView];
    [self.bottomBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(bottomViewHeight);
        make.bottom.offset(-BOTTOM_HEIGHT);
    }];
    self.topBtnBottomLayout.constant = BOTTOM_HEIGHT + 12;
    self.webViewBottomLayout.constant = BOTTOM_HEIGHT;
}
- (void)reloadWebView:(NSString*)url
{
    //获取url之后，重新加载webview
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}
- (void)reloadBottomBarView:(CGFloat)installmentPrice buyNumber:(NSInteger)buyNum
{
    WEAK_SELF
    [self.bottomBarView yk_renderMallDetailBottomBarViewWithInstallmentPrice:installmentPrice buyNum:buyNum];
    self.detailModel.buyNum = buyNum;
    
    if (self.detailModel.mallOptionShopIdModelArray.count) {
        //规格弹框里，有数据之后，需要将那些选中的状态保存下来，所以就直接跳出去了
        return;
    }
    [self.detailModel.more_shop_info.installment_option_arr enumerateObjectsUsingBlock:^(YKMallInstallmentOptionModel * optionModel, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableArray *otionArray = @[].mutableCopy;
        [optionModel.options enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YKMallOptionShopIdModel *mallOptionShopIdModel = [YKMallOptionShopIdModel new];
            mallOptionShopIdModel.optionName = obj;
            mallOptionShopIdModel.shopId = [optionModel.shop_id[idx] integerValue];
            if (idx==0) {
                mallOptionShopIdModel.isSelect = YES;
            }
            [otionArray addObject:mallOptionShopIdModel];
        }];
        [weakSelf.detailModel.mallOptionShopIdModelArray addObject:otionArray];
    }];
    
    [self goodsPecificationsStr:weakSelf.detailModel.buyNum];

    [self.bottomBarView setBuyBtnClickBlock:^{
        [weakSelf fetchCheckOrderData: weakSelf.detailModel.optionStr shopId:weakSelf.detailModel.shop_info.shop_id buyNum:weakSelf.detailModel.buyNum];
    }];
}
- (void)goodsPecificationsStr:(NSInteger)buyNum
{
    //遍历出isSelect的然后拼接成字符
    NSMutableArray *array = @[].mutableCopy;
    [self.detailModel.mallOptionShopIdModelArray enumerateObjectsUsingBlock:^(NSArray *otionArray, NSUInteger idx, BOOL * _Nonnull stop) {
        [otionArray enumerateObjectsUsingBlock:^( YKMallOptionShopIdModel *mallOptionShopIdModel, NSUInteger optionIdx, BOOL * _Nonnull stop) {
            if (mallOptionShopIdModel.isSelect) {
                [array addObject:mallOptionShopIdModel.optionName];
            }
        }];
    }];
    
    self.detailModel.optionNumberStr =  [NSString stringWithFormat:@"%@/数量%ld",[array componentsJoinedByString:@"/"],buyNum];
    self.detailModel.optionStr =  [array componentsJoinedByString:@"/"];
    DLog(@"optionStr====%@", self.detailModel.optionStr);
    DLog(@"optionNumberStr====%@", self.detailModel.optionNumberStr);
    DLog(@"shop_id=====%ld", self.detailModel.shop_info.shop_id);
}

- (void)setRefreshHeaderFooter
{
    WEAK_SELF
    MJRefreshBackNormalFooter *refreshFooter = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf.scrollView scrollRectToVisible:CGRectMake(0, self.scrollView.frame.size.height, WIDTH_OF_SCREEN, self.scrollView.frame.size.height) animated:YES];
        [weakSelf reloadWebView:weakSelf.detailModel.installment_detail];
        [weakSelf.tableView.mj_footer endRefreshing];
        
        [weakSelf defaultColorNavigationView];
        
    }];
    refreshFooter.stateLabel.textColor = [UIColor yk_colorWithHexString:@"#ADADAD"];
    refreshFooter.stateLabel.font = [UIFont systemFontOfSize:13.0f];
    [refreshFooter setTitle:kRefreshFooterTitle forState:MJRefreshStateIdle];
    [refreshFooter setTitle:kRefreshFooterTitle forState:MJRefreshStatePulling];
    [refreshFooter setTitle:kRefreshFooterTitle forState:MJRefreshStateRefreshing];
    [refreshFooter setTitle:kRefreshFooterTitle forState:MJRefreshStateWillRefresh];
    [refreshFooter setTitle:kRefreshFooterTitle forState:MJRefreshStateNoMoreData];
    refreshFooter.labelLeftInset = -115;

    self.tableView.mj_footer = refreshFooter;
    
    MJRefreshNormalHeader *refreshHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
       
        [weakSelf.scrollView scrollRectToVisible:CGRectMake(0, 0, WIDTH_OF_SCREEN, self.scrollView.frame.size.height) animated:YES];
        [weakSelf.webView.scrollView.mj_header endRefreshing];
        
        [weakSelf clearColorNavigationView];
    }];
    [refreshHeader setTitle:kRefreshHeaderTitle forState:MJRefreshStateIdle];
    [refreshHeader setTitle:kRefreshHeaderTitle forState:MJRefreshStatePulling];
    [refreshHeader setTitle:kRefreshHeaderTitle forState:MJRefreshStateRefreshing];
    [refreshHeader setTitle:kRefreshHeaderTitle forState:MJRefreshStateWillRefresh];
    [refreshHeader setTitle:kRefreshHeaderTitle forState:MJRefreshStateNoMoreData];
    refreshHeader.stateLabel.textColor = [UIColor yk_colorWithHexString:@"#ADADAD"];
    refreshHeader.stateLabel.font = [UIFont systemFontOfSize:13.0f];
    refreshHeader.lastUpdatedTimeLabel.hidden = YES;
    refreshHeader.labelLeftInset = -115;
    self.webView.scrollView.mj_header = refreshHeader;
}
- (void)setContentInsetAdjustmentBehavior
{
    //在iOS11上，出现了状态栏空出来的问题，用下面的方法解决
    if (@available(iOS 11.0, *)) {
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}
//月供测算默认选中第一个
- (void)reloadPaymentView
{
    WEAK_SELF
    [self.detailModel.more_shop_info.month_pay_amount_arr enumerateObjectsUsingBlock:^(YKMallMonthPayAmountModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) {
            obj.isMonthPayAmountSelect = YES;
            weakSelf.detailModel.more_shop_info.paymentStr =  [NSString stringWithFormat:@"%@元 x %@期",obj.pay_amount,obj.month];
        }else{
            obj.isMonthPayAmountSelect = NO;
        }
    }];
}

//加载地址
- (void)reloadAddress
{
    /*
     - 先判断用户是否有收获地址，若有则自动带出；带出的地址最多显示一行，多余则用“..."代替
     - 若无收货地址，则自动获取定位，带出当前定位，
     - 若获取不到定位，则默认一个定位。（北京市 海淀区）
     */
    if (![self.detailModel.more_shop_info.default_address yk_isValidString]) {
        //如果不存在
        self.detailModel.more_shop_info.default_address = kDefaultAddress;
        if ([[YKUserManager sharedUser].locationStr yk_isValidString]) {
            self.detailModel.more_shop_info.default_address = [YKUserManager sharedUser].locationStr;
        }
    }
    [self.tableView reloadData];
}
//弹出地址选项弹框
- (void)popUpLocationPicker
{
    WEAK_SELF
    [YKLocationPicker shared].hiddenNavView = YES;
    [[YKLocationPicker shared] yk_showInView:self.view];
    [[YKLocationPicker shared] setPickerDidSelect_blk_t:^(NSInteger provinceId, NSInteger cityId, NSInteger districtId, NSInteger townId, NSString *fullAddress) {
        STRONG_SELF
        strongSelf.detailModel.more_shop_info.default_address = fullAddress;
        [strongSelf.tableView reloadData];
    }];
}

#pragma mark - CustomerCell
//banner图片
- (UITableViewCell *)tableView:(UITableView *)tableView bannerCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YKMallDetailBannerCell *cell = [tableView dequeueReusableCellWithIdentifier:kBannerCell];
    [cell yk_renderShopInfo:self.detailModel.shop_info];
    return cell;
}
//商品信息
- (UITableViewCell *)tableView:(UITableView *)tableView productItemInfoCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YKMallDetailProductItemInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:kProductItemInfoCell];
    [cell yk_renderShopInfo:self.detailModel.shop_info];
    return cell;
}
//规格、月供测算、配送
- (UITableViewCell *)tableView:(UITableView *)tableView specificationsCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YKMallDetailSpecificationsCell *cell = [tableView dequeueReusableCellWithIdentifier:kSpecificationsCell];
    [cell yk_renderModel:self.detailModel index:indexPath];
    return cell;
}

//返利
- (UITableViewCell *)tableView:(UITableView *)tableView rebateCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YKMallDetailRebateCell *cell = [tableView dequeueReusableCellWithIdentifier:kRebateCell];
    [cell yk_renderModel:self.detailModel.more_shop_info];
    return cell;
}

//放心品质,包邮，售后
- (UITableViewCell *)tableView:(UITableView *)tableView shipFeeAfterSaleCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YKMallDetailShipFeeAfterSaleCell *cell = [tableView dequeueReusableCellWithIdentifier:kShipFeeAfterSaleCell];
    [cell yk_renderModel:self.detailModel.more_shop_info];
    return cell;
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    switch (section) {
        case 0:
            rows = 2;
            break;
        case 1:
        {
            if (self.detailModel.more_shop_info.is_show_address && self.detailModel.more_shop_info.is_show_rebate) {
                rows = 5;
            }else if (!self.detailModel.more_shop_info.is_show_address && self.detailModel.more_shop_info.is_show_rebate){
                rows = 4;
            }else if (self.detailModel.more_shop_info.is_show_address && !self.detailModel.more_shop_info.is_show_rebate){
                rows = 4;
            }else{
                rows = 3;
            }
        }
            break;
        default:
            break;
    }

    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if (indexPath.section == 0) {
       if (indexPath.row == 0) {
           //banner
           return [self tableView:tableView bannerCellForRowAtIndexPath:indexPath];
       }else{
          //商品信息
           return [self tableView:tableView productItemInfoCellForRowAtIndexPath:indexPath];
       }
    } else {
        if (self.detailModel.more_shop_info.is_show_address && self.detailModel.more_shop_info.is_show_rebate) {
            if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
                //规格、月供测算、配送
                return [self tableView:tableView specificationsCellForRowAtIndexPath:indexPath];
            }else if (indexPath.row == 3){
                //返利
                return [self tableView:tableView rebateCellForRowAtIndexPath:indexPath];
            }else{
                //放心品质,包邮，售后
                return [self tableView:tableView shipFeeAfterSaleCellForRowAtIndexPath:indexPath];
            }
        }else if (!self.detailModel.more_shop_info.is_show_address && self.detailModel.more_shop_info.is_show_rebate){
            //没有地址，有返利活动
            if (indexPath.row == 0 || indexPath.row == 1) {
                //规格、月供测算、配送
                return [self tableView:tableView specificationsCellForRowAtIndexPath:indexPath];
            }else if (indexPath.row == 2){
                //返利
                return [self tableView:tableView rebateCellForRowAtIndexPath:indexPath];
            }else{
                //放心品质,包邮，售后
                return [self tableView:tableView shipFeeAfterSaleCellForRowAtIndexPath:indexPath];
            }
            
        }else if ( self.detailModel.more_shop_info.is_show_address && !self.detailModel.more_shop_info.is_show_rebate){
            //有地址，没有返利活动
            if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
                //规格、月供测算、配送
                return [self tableView:tableView specificationsCellForRowAtIndexPath:indexPath];
            }else{
                //放心品质,包邮，售后
                return [self tableView:tableView shipFeeAfterSaleCellForRowAtIndexPath:indexPath];
            }
        }else{
           //即没有地址，又没有返利活动
            if (indexPath.row == 0 || indexPath.row == 1) {
                //规格、月供测算、配送
                return [self tableView:tableView specificationsCellForRowAtIndexPath:indexPath];
            }else{
                //放心品质,包邮，售后
                return [self tableView:tableView shipFeeAfterSaleCellForRowAtIndexPath:indexPath];
            }
        }
       
    }
}


#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0;
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0) {
                height = WIDTH_OF_SCREEN;
            }else{
             
                NSString *title = self.detailModel.shop_info.title;
                CGSize titleSize =   [YKTools yk_labelRectWithSize:CGSizeMake(WIDTH_OF_SCREEN - 30, 0) LabelText:title font:[UIFont boldSystemFontOfSize:16.0f]];
                NSString *desc = self.detailModel.shop_info.desc;
                CGSize descSize =   [YKTools yk_labelRectWithSize:CGSizeMake(WIDTH_OF_SCREEN - 30, 0) LabelText:desc font:[UIFont systemFontOfSize:14.0f]];
                if (titleSize.height + descSize.height + 67 < 103) {
                    height = 103;
                }else{
                    height = titleSize.height + descSize.height + 77;
                }
            }
        }
            break;
        case 1:
        {
             height = tableViewRowHeight;
        }
            break;
        default:
            break;
    }
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 0.01;
    if (section == 1) {
        height = 10;
    }
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WEAK_SELF
    if (indexPath.section == 1) {
        YKMallCustomViewController * VC = [YKMallCustomViewController new];
        switch (indexPath.row) {
            case 0:
            {
                //规格
                VC.detailModel = self.detailModel;
                [VC yk_showInViewController:self type:YKMallTypesPecifications];
                [VC setMallPecificationsBlock:^(BOOL isUpdateMonthlyPayments, NSString *pecificationsStr, NSInteger shopId, NSInteger buyNum) {
                    if (isUpdateMonthlyPayments) {
                        NSMutableArray *array = weakSelf.detailModel.mallOptionShopIdModelArray;
                        [weakSelf fetchMallDetailDataWithShopId:shopId buyNum:buyNum mallOptionShopIdModelArray:array defaultAddress:weakSelf.detailModel.more_shop_info.default_address];
                    }else{
                        [tableView reloadData];
                    }
                    //选择的规格参数
                    [weakSelf fetchCheckOrderData:pecificationsStr shopId:shopId buyNum:buyNum];
                }];
                [VC setMallPecificationsCloseBlock:^(BOOL isUpdateMonthlyPayments, NSInteger shopId, NSInteger buyNum) {
                    if (isUpdateMonthlyPayments) {
                        NSMutableArray *array = weakSelf.detailModel.mallOptionShopIdModelArray;
                        [weakSelf fetchMallDetailDataWithShopId:shopId buyNum:buyNum mallOptionShopIdModelArray:array defaultAddress:weakSelf.detailModel.more_shop_info.default_address];
                    }else{
                        [tableView reloadData];
                    }
                }];
                [VC setMallChangeBannerImageBlock:^(BOOL isUpdateMonthlyPayments, NSInteger shopId, NSInteger buyNum) {
                    //此时点击的shop不为零，重新请求数据，并且不保留原来选中的,肯定要更新月供详情
                    [weakSelf fetchMallDetailDataWithShopId:shopId buyNum:buyNum mallOptionShopIdModelArray:nil defaultAddress:weakSelf.detailModel.more_shop_info.default_address];
                }];
            }
                break;
            case 1:
            { 
                //月供测算
                VC.detailModel = self.detailModel;
                [VC yk_showInViewController:self type:YKMallTypePayments];
                [VC setCustomViewBlock:^{
                    [tableView reloadData];
                }];
            }
                break;
        }
        if (self.detailModel.more_shop_info.is_show_address && self.detailModel.more_shop_info.is_show_rebate) {
           //既有地址，又有返利
             switch (indexPath.row) {
                case 2:
                {
                    [self popUpLocationPicker];
                }
                    break;
                case 3:
                {
                    //返利
                    YKBrowseWebController *webVC = [YKBrowseWebController new];
                    webVC.url = self.detailModel.more_shop_info.h5_url;
                    [self.navigationController pushViewController:webVC animated:YES];
                }
                    break;
                case 4:
                {
                    //正品保障
                    VC.detailModel = self.detailModel;
                    [VC yk_showInViewController:self type:YKMallTypeService];
                }
                    break;
                    
                default:
                    break;
            }
            
        }else if (!self.detailModel.more_shop_info.is_show_address && self.detailModel.more_shop_info.is_show_rebate){
            //没有地址，有返利活动
            switch (indexPath.row) {
                case 2:
                {
                    //返利
                    YKBrowseWebController *webVC = [YKBrowseWebController new];
                    webVC.url = self.detailModel.more_shop_info.h5_url;
                    [self.navigationController pushViewController:webVC animated:YES];
                }
                    break;
                case 3:
                {
                    //正品保障
                    VC.detailModel = self.detailModel;
                    [VC yk_showInViewController:self type:YKMallTypeService];
                }
                    break;
                    
                default:
                    break;
            }
            
        }else if ( self.detailModel.more_shop_info.is_show_address && !self.detailModel.more_shop_info.is_show_rebate){
            //有地址，没有返利活动
            switch (indexPath.row) {
                case 2:
                {
                    //配送
                     [self popUpLocationPicker];
                }
                    break;
                case 3:
                {
                    //正品保障
                    VC.detailModel = self.detailModel;
                    [VC yk_showInViewController:self type:YKMallTypeService];
                }
                    break;
                    
                default:
                    break;
            }
        }else{
            //即没有地址，又没有返利活动
            switch (indexPath.row) {
                case 2:
                {
                    //正品保障
                    VC.detailModel = self.detailModel;
                    [VC yk_showInViewController:self type:YKMallTypeService];
                }
                    break;
                    
                default:
                    break;
            }
        }
        
    }
}

//MARK:private methods
- (void)prepareNavView
{
    WEAK_SELF
    [self.view addSubview:self.mallDetailNavigationView];
    [self.mallDetailNavigationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.offset(NAV_HEIGHT);
    }];
    [self.mallDetailNavigationView setBackBlock:^{
        //返回上一层
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.mallDetailNavigationView setShareBlock:^{
        YKShareEntity * entity = (YKShareEntity*) weakSelf.detailModel.share_info;
        [[YKShareManager shareManager] yk_showWithShareEntity:entity];
    }];
    self.webViewTopLayout.constant = NAV_HEIGHT;
}
- (IBAction)toTopBtnClick:(id)sender
{
    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, WIDTH_OF_SCREEN, self.scrollView.frame.size.height) animated:YES];
    [self clearColorNavigationView];
}
- (void)clearColorNavigationView
{
    self.mallDetailNavigationView.backgroundColor = [UIColor clearColor];
    self.mallDetailNavigationView.navTitleLabel.text = @"";
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.mallDetailNavigationView.backButton setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    [self.mallDetailNavigationView.shareButton setImage:[UIImage imageNamed:@"mall_share"] forState:UIControlStateNormal];
}
- (void)defaultColorNavigationView
{
    self.mallDetailNavigationView.backgroundColor = MAIN_THEME_COLOR;
    self.mallDetailNavigationView.navTitleLabel.text = @"商品详情";
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.mallDetailNavigationView.backButton setImage:[UIImage imageNamed:@"system_back_white"] forState:UIControlStateNormal];
    [self.mallDetailNavigationView.shareButton setImage:[UIImage imageNamed:@"mall_share_white"] forState:UIControlStateNormal];
}
//MARK:网络请求
- (void)fetchMallDetailDataWithShopId:(NSInteger)shopId buyNum:(NSInteger)buyNum mallOptionShopIdModelArray:(NSMutableArray*)mallOptionShopIdModelArray  defaultAddress:(NSString*)defaultAddress
{
    /*
     buy_num购买数量的字段，默认传1，如果传的值小于最小购买数量min_buy_num的话，后台按照最小购买数量min_buy_num计算
     如果传的值大于最小购买数量min_buy_num时，后台按照传入的值计算
     */
    NSDictionary *param = @{@"shop_id":[NSString stringWithFormat:@"%ld",shopId],
                             @"buy_num":[NSString stringWithFormat:@"%ld",buyNum]
                            };
    [[HYProgressHUD yk_shareProgressHUD ] yk_showSpecificationsHudWithMessage:nil];
    [[HTTPManager session] getRequestForKey:kMallGetShopDetail showLoading:NO param:param succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDic, NSInteger code , BOOL success, NSString *msg) {
        [self.tabViewBgView df_hideHud];
        self.bottomBarView.hidden = NO;
        self.tableView.hidden = NO;
        if (code == 0 && success) {
            self.detailModel = [YKMallDetailModel yy_modelWithJSON:jsonDic];
            if ([defaultAddress yk_isValidString]) {
             self.detailModel.more_shop_info.default_address = defaultAddress;
            }
            if (mallOptionShopIdModelArray && [mallOptionShopIdModelArray isKindOfClass:[NSMutableArray class]] && mallOptionShopIdModelArray.count) {
                /*
                 将规格弹框选中的
                 规格字符（不带数量的）
                 规格拼接字符（带数量的）前端展示用
                 默认地址
                 保存下来
                 */
                self.detailModel.mallOptionShopIdModelArray = mallOptionShopIdModelArray;
                [self goodsPecificationsStr:buyNum];
                [self reloadBottomBarView:self.detailModel.shop_info.installment_price buyNumber:buyNum];
               
            }else{
                [self reloadBottomBarView:self.detailModel.shop_info.installment_price buyNumber:self.detailModel.shop_info.min_buy_num];
                if (self.mallDetailLoadDataSuccessBlock) {
                    self.mallDetailLoadDataSuccessBlock(self.detailModel);
                }
            }
            [self reloadAddress];
            [self reloadWebView:self.detailModel.installment_detail];
            [self reloadPaymentView];
            [self.tableView reloadData];
            DLog(@"====%@", jsonDic);
            [[HYProgressHUD yk_shareProgressHUD] yk_hideAnimated];
        }else if (code != 0){
             [[iToast makeText:msg] show];
            [[HYProgressHUD yk_shareProgressHUD] yk_hideAnimated];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
        //没有网络连接
        if (!isConnect) {
            self.tabViewBgView.df_hud = [UIView df_defaultNoNetHudViewWithTitle:@"暂无网络，请点击重试" imageTitleMargin:0 btnMargin:59 btnBlock:^(UIButton *btn) {
                [self fetchMallDetailDataWithShopId:self.shopList_id buyNum:1 mallOptionShopIdModelArray:nil defaultAddress:@""];
            }];
            self.bottomBarView.hidden = YES;
            self.tableView.hidden = YES;
            [[HYProgressHUD yk_shareProgressHUD] yk_hideAnimated];
        }
    }];
}
//MARK:校验订单
- (void)fetchCheckOrderData:(NSString*)installmentOption shopId:(NSInteger)shopId buyNum:(NSInteger)buyNum
{
    NSDictionary *param = @{@"shop_id":[NSString stringWithFormat:@"%ld",shopId],
                            @"installment_option":installmentOption,
                            @"buy_num":[NSString stringWithFormat:@"%ld",buyNum]};
    [[HTTPManager session] postRequestForKey:kMallShopInstallmentPost showLoading:YES param:param succeed:^(NSDictionary * _Nonnull jsonDic, NSInteger code, BOOL success, NSString *message) {
        if (code == 0 && success) {
            DLog(@"=====%@",jsonDic);
             YKMallCheckModel *mallCheckmodel = [YKMallCheckModel yy_modelWithJSON:jsonDic];
             if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[YKConfirmOrderViewController class]]) {
                 YKConfirmOrderViewController *mallDetailVC = [YKConfirmOrderViewController new];
                 mallDetailVC.mallCheckModel = mallCheckmodel;
                 [self.navigationController pushViewController:mallDetailVC animated:YES];
             }
        } else if (code != 0){
           [[iToast makeText:message] show];
        }
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
        
    }];
    
}
//MARK:Lozying load
- (YKMallDetailNavigationView *)mallDetailNavigationView
{
    if (!_mallDetailNavigationView) {
        _mallDetailNavigationView = [YKMallDetailNavigationView new];
        _mallDetailNavigationView.backgroundColor = [UIColor clearColor];
    }
    return _mallDetailNavigationView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
