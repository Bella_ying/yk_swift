//
//  YKMallDetailViewController.h
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"
@class YKMallDetailModel;
typedef void(^ykMallDetailLoadDataSuccessBlock)(YKMallDetailModel *detailModel);
@interface YKMallDetailViewController : YKBaseViewController
@property (nonatomic, assign) NSInteger shopList_id;
@property (nonatomic,copy) ykMallDetailLoadDataSuccessBlock mallDetailLoadDataSuccessBlock;
@end
