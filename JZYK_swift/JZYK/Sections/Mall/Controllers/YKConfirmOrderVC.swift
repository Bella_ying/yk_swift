//
//  YKConfirmOrderVC.swift
//  JZYK
//
//  Created by zhaoying on 2018/7/27.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

import UIKit


let  confirmOrderInstallmentlHeight = 42.0
let  confirmOrderInstallmentlLineHeight = 42.0
let  confirmOrderInstallmentHeight = 42.0
let kAddressCell = "AddressCell"
let kNoAddressCell = "NoAddressCell"
let kMallCell = "MallCell"
let kPricelCell = "PricelCell"
let kInstallmentlCell = "InstallmentlCell"
let kReimbursementCell = "ReimbursementCell"
let kAddBankCell = "AddBankCell"



class YKConfirmOrderVC: YKBaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomLayout: NSLayoutConstraint!
    private var confirmOrderInstallmentlSectionHeight: CGFloat = 0.0
    private var addresslistModel : YKAddressListModel?
    private var selectCheckMonthPayAmountModel : YKMallCheckMonthPayAmountModel?//选中分期的model
    
    private var tradePswPopup :YKTradePswPopup?
    
    public var mallCheckModel :YKMallCheckModel?
      
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "确认订单"
        self.tableViewBottomLayout.constant = CGFloat(BOTTOM_HEIGHT)
        self.confirmOrderInstallmentlSectionHeight = 100
        if ((self.mallCheckModel?.more_info.month_pay_amount_arr) != nil) {
            self.selectCheckMonthPayAmountModel = self.mallCheckModel?.more_info.month_pay_amount_arr[0]
        }
        self.registerCell()
        // Do any additional setup after loading the view.
    }
    private func registerCell(){
        self.tableView .register( UINib (nibName: "YKConfirmOrderAddressCell", bundle: nil) , forCellReuseIdentifier: kAddressCell)
        self.tableView.register(UINib(nibName: "YKConfirmOrderNoAddressCell", bundle: nil) , forCellReuseIdentifier: kNoAddressCell)
        self.tableView.register(UINib (nibName: "YKConfirmOrderMallCell", bundle: nil) , forCellReuseIdentifier: kMallCell)
        self.tableView.register(UINib (nibName: "YKConfirmOrderPricelCell", bundle: nil) , forCellReuseIdentifier: kPricelCell)
        self.tableView.register(UINib(nibName: "YKConfirmOrderInstallmentlCell", bundle: nil) , forCellReuseIdentifier: kInstallmentlCell)
        self.tableView.register(UINib(nibName: "YKConfirmOrderAddBankCell", bundle: nil) , forCellReuseIdentifier: kAddBankCell)
        self.tableView.register(UINib(nibName: "YKConfirmOrderReimbursementCell", bundle: nil) , forCellReuseIdentifier: kReimbursementCell)
        self.tableView.tableFooterView = UIView.init()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.showsVerticalScrollIndicator = false
    }
    
    //pragma mark - UITableViewDataSource
    
    //返回表格视图应该显示的数据的段数
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 4
        
    }
    
    //返回表格视图上每段应该显示的数据的行数
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 3
        case 3:
            return 4
        default:
            return 0
        }
        
    }
    
    //返回某行上应该显示的单元格
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
        do {
            if ((self.addresslistModel) != nil) {
                // 有地址
                let cell  = tableView.dequeueReusableCell(withIdentifier: kAddressCell) as? YKConfirmOrderAddressCell
                cell?.yk_renderConfirmOrderAddress(self.addresslistModel)
                return cell!
            }else{
                // 没有地址
                let cell = tableView.dequeueReusableCell(withIdentifier: kNoAddressCell) as? YKConfirmOrderNoAddressCell
                return cell!
            }
            
        }
      case 1:
       do {
        let cell  = tableView.dequeueReusableCell(withIdentifier: kMallCell) as? YKConfirmOrderMallCell
         cell?.yk_renderConfirmOrderMallCell(self.mallCheckModel?.shop_detail)
        return cell!
        }
        case 2:
            do {
                let  cell  = tableView.dequeueReusableCell(withIdentifier: kPricelCell) as? YKConfirmOrderPricelCell
                cell?.yk_renderConfirmOrderPricelCell(self.mallCheckModel?.shop_detail, indexPath: indexPath as IndexPath?)
                return cell!
            }
        case 3:
            do {
                if indexPath.row == 0 {
                    let  cell  = tableView.dequeueReusableCell(withIdentifier: kPricelCell) as? YKConfirmOrderPricelCell
                    cell?.yk_renderPaymentMethodCell(self.mallCheckModel?.more_info, indexPath: indexPath as IndexPath?)
                    return cell!
                }else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: kInstallmentlCell) as? YKConfirmOrderInstallmentlCell
                    cell?.yk_renderConfirmOrderInstallmentlCell(self.mallCheckModel?.more_info)
                    cell?.moreBlock = {[weak self] () in
                        let number  = self?.mallCheckModel?.more_info.month_pay_amount_arr.count
                        if number! % 3 == 0 {
                            self?.confirmOrderInstallmentlSectionHeight = CGFloat(Double ((number! / 3)) * (confirmOrderInstallmentHeight + confirmOrderInstallmentlLineHeight)  + confirmOrderInstallmentHeight)
                        }else{
                            self?.confirmOrderInstallmentlSectionHeight = CGFloat(Double (((number! / 3) + 1)) * (confirmOrderInstallmentHeight + confirmOrderInstallmentlLineHeight)  + confirmOrderInstallmentHeight)
                        }
                        self?.tableView.reloadData()
                    }
                    cell?.selectBlock = {[weak self]  (model : YKMallCheckMonthPayAmountModel) in
                        self?.selectCheckMonthPayAmountModel = model
                        self?.tableView.reloadData()
                        
                    } as! ykSelectBlock
                    return cell!
                }else if indexPath.row == 2{
                    let cell  = tableView.dequeueReusableCell(withIdentifier: kAddBankCell) as? YKConfirmOrderAddBankCell
                    cell?.yk_renderConfirmOrderAddBankCell(self.mallCheckModel?.more_info.user_card)
                    return cell!
                    
                }else{
                    let cell  = tableView.dequeueReusableCell(withIdentifier: kReimbursementCell) as? YKConfirmOrderReimbursementCell
                    cell?.yk_renderPayAmount(self.selectCheckMonthPayAmountModel)
                    cell?.yk_renderAgreementInfo(self.mallCheckModel)
                    cell?.tipBtnClickBlock = {[weak self] () in
                        let VC = YKMallCustomViewController.init()
                        VC.checkMonthPayAmountModel = self?.selectCheckMonthPayAmountModel
                       
                    }
                    cell?.confirmBtnClickBlock = {[weak self] () in
                        self?.fetchOrderData()
                    }
                    
                    cell?.selectProtocolIndexBlock = {[weak self] (url : NSString) in
                        let webVC = YKBrowseWebController.init()
                        webVC.url = url as String?
                        self?.navigationController?.pushViewController(webVC, animated: true)
                    } as! ykSelectProtocolIndexBlock
                    
                    return cell!
                    
                }
            }
        default:
            break
        }

        return nil!
    }
    //pragma mark - UITableViewDelegate
    //返回每一行的行高
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        
        return 60;
        
    }
    
    
    
    //返回每一段段头的高度
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        
        return 40;
        
    }
    //已经选中某一行
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        
    }
    
    func fetchOrderData ()  {
        if (self.addresslistModel == nil){
           QLAlert().show(withMessage: "请添加收货地址哦", singleBtnTitle: "确定")
            return
        }
        self.commitOrderRequestWithPsw(psw: "", errorLabel: nil!, isCheckOrder: true)
    }
    func commitOrderRequestWithPsw(psw:String,errorLabel:UILabel?,isCheckOrder:Bool) -> Void {
        var param = [String : Any?]()
        if isCheckOrder {
            param = ["shop_id":self.mallCheckModel?.shop_detail.shop_id,
                     "installment_option":self.mallCheckModel?.shop_detail.installment_option,
                     "buy_num":self.mallCheckModel?.shop_detail.buy_num,
                     "installment_month":self.selectCheckMonthPayAmountModel?.month,
                     "shipping_address_id":self.addresslistModel?.addressID]
        }else{
            param = ["shop_id":self.mallCheckModel?.shop_detail.shop_id,
                     "installment_option":self.mallCheckModel?.shop_detail.installment_option,
                     "buy_num":self.mallCheckModel?.shop_detail.buy_num,
                     "installment_month":self.selectCheckMonthPayAmountModel?.month,
                     "shipping_address_id":self.addresslistModel?.addressID,
                     "pay_password":psw]
        }
        
        HTTPManager.session.postRequest(forKey: kMallShopInstallmentPost, param: param, succeed: {[weak self] (json , code, unwrapNormal, msg) in
            if code == 0 {
                self?.tradePswPopup?.animateHide()
                iToast.makeText(msg).show()
                let url = json["redirect_url"]
                let webVC = YKBrowseWebController.init()
                webVC.url = url as! String
//                PopToDesignatedPage?.self
                self?.navigationController?.pushViewController(webVC, animated: true)
                
            } else if code == 2{
                //先调用存管的逻辑，存管逻辑成功之后，再去设置交易密码
                self?.fetchMallCardGetDepositOpenInfoData(isCheckOrder: false, isSetTradePsw: true)
            } else if code == 3{
                if isCheckOrder{
                    //表示设置过交易密码，调用存管的接口，走存管的逻辑；
                     self?.fetchMallCardGetDepositOpenInfoData(isCheckOrder: true, isSetTradePsw: false)
                }else{
                    //输入的交易密码错误，后台返回剩余设置交易密码次数
                    errorLabel?.text = msg
                }
               
            } else if code == 1011{
                 //授信未完成,跳转至认证向导
                self?.tradePswPopup?.animateHide()
                if (!(YKTabBarController.yk_shareTab().yk_getCurrentViewController() as AnyObject).isKind(of: YKVerifyGuideViewController.self)){
                    let guideVC = YKVerifyGuideViewController.init()
                    self?.navigationController?.pushViewController(guideVC, animated: true)
                }
            } else if code == 1008{
                 ///认证过期或缺失必要认证项,若有过期则跳转认证中心
                self?.tradePswPopup?.animateHide()
                if (!(YKTabBarController.yk_shareTab().yk_getCurrentViewController() as AnyObject).isKind(of: YKVerifyCenterListVC.self)){
                    let centerVC =  YKVerifyCenterListVC.init()
                    self?.navigationController?.pushViewController(centerVC, animated: true)
                }
            }else{
                self?.tradePswPopup?.animateHide()
                iToast.makeText(msg).show()
            }
        }) { (errMsg, isConnect) in
            if !isConnect {
                iToast.makeText(errMsg).show()
                self.tradePswPopup?.animateHide()
            }
        }
        
    }
    //MARK:获取用户是否开通存管
    func fetchMallCardGetDepositOpenInfoData(isCheckOrder:Bool,isSetTradePsw:Bool) -> Void {
        YKCunGunTools.yk_featchCunGuanData( cunguanFinish: {
            if isCheckOrder{
                //是校验订单，开通存管成功，弹出交易密码的弹框
                self.popUpTradePsw()
            }
            if isSetTradePsw{
                //表示没有设置过交易密码，则跳转到设置交易密码页面，让用户设置交易密码，用户设置成功之后，将设置成功的交易密码回传，接着进行确认订单逻辑
                self.toSetTradePsw()
            }
        }) { (bankPageBlock, card_desc, card_id, bank_name) in
            self.mallCheckModel?.more_info.user_card = card_desc
            self.tableView.reloadData()
        }
        
    }
    func popUpTradePsw()  {
//      self.tradePswPopup =  YKTradePswPopup.makeTradePswPop(inputCompletion: {(inputStr, errorLab) in
//            self.commitOrderRequestWithPsw(psw: inputStr, errorLabel: errorLab, isCheckOrder: false)
//        })("订单总额",String(format: "%.2f", arguments:self.mallCheckModel?.shop_detail?.order_money)).animateShow()
    }
        
    func toSetTradePsw () {
        //没有设置过交易密码,去交密码中设置
        let transacVc = TransactionCodeController.init()
        transacVc.typeNum = 1
        transacVc.isShowAlert = false
        self.navigationController?.pushViewController(transacVc, animated: true)
        transacVc.transactionSetSuccess = { [weak self] (pwd : String) in
            self?.commitOrderRequestWithPsw(psw: pwd, errorLabel: nil, isCheckOrder: false)
        }
    }
    func monthPayAmountArrryDefaultSelect() {

        let array : NSArray = (self.mallCheckModel?.more_info?.month_pay_amount_arr as NSArray?)!
        array.enumerateObjects({ obj, idx, stop in
            let model = obj as? YKMallCheckMonthPayAmountModel
            if idx == 0 {
                //默认选中第1个
                model?.isSelect = true
            }else{
                model?.isSelect = false
            }
        })
        
    }
    lazy var payMethodArray: NSMutableArray = {
        let payMethodArray = NSMutableArray.init()
        return payMethodArray;
    }()
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
