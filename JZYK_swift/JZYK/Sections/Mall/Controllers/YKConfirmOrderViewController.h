//
//  YKConfirmOrderViewController.h
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"
@class YKMallCheckModel;
@interface YKConfirmOrderViewController : YKBaseViewController
//从上个页面传过来的商品校验的model
@property (nonatomic,strong) YKMallCheckModel *mallCheckModel;
@end
