//
//  YKMallVC.swift
//  JZYK
//
//  Created by zhaoying on 2018/8/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//


import UIKit

class YKMallVC: SwipeViewController {
    private var installmentTypekey: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "分期商城"
        self.openLocation()
        self.prepareNotificationCenter()
        self.setupLoginControllers()
    }
    
    func setupLoginControllers() {
        setViewControllerArray(self.classArray as! [UIViewController])
        setSelectionBar(23, height: 2, color: Color.main)
        setButtonsWithSelectedColor(UIFont.systemFont(ofSize: 14),
                                    color: Color.color_66_C3,
                                    selectedColor: Color.main)
        
        equalSpaces = false
    }
    
    func prepareNotificationCenter () {
        NotificationCenter.default.addObserver(self, selector: #selector(notificationAction), name: NSNotification.Name(rawValue: "ykInstallmentType"), object: nil)
    }
    // 接受到通知后的方法回调
    @objc private func notificationAction(noti: Notification) {
        let dic = noti.userInfo! as NSDictionary
        self.installmentTypekey = (dic.object(forKey: "installmentTypekey") as! String)
        
        /*
         从别的页面跳转到商城的推荐，或者其他的key对应的商品标签，
         1、如果用户已经点击过“商城tab”，说明self.keyArray是有值的，那么就执行下面的jumpPageAtIndex方法
         2、如果用户没有点击过“商城tab”或者已经点击过“商城tab”，但是商城tab的商品标签发生了变化，就会先执行下面的jumpPageAtIndex方法，然后执行prepareUI方法中的jumpPageAtIndex
         */
        self.jumpPageAtIndex()
    }
    func jumpPageAtIndex()  {
        
    }
    func openLocation () {
        if YKTools.yk_permissionsWithLocation().rawValue == 2 {
            YKAccessAlertView.sharedAlertManager().showAlertType(YKShowAlertType(rawValue: 5)!, contentText: "")
        }else{
            YKLocationManager.location().yk_enableLocationService { (location,regeoCode ) in
                YKUserManager.sharedUser().locationStr = String(format: "%@ %@ %@", (regeoCode?.province)!,(regeoCode?.city)!,(regeoCode?.district)!)
            }
        }
        
    }
    func fetchMallHomeData()  {
        HTTPManager.session.getRequest(forKey: kMallAppMallIndex, showLoading: false, param: [:], succeed: {[weak self] (jsonDic, code, success, msg) in
            if code == 0 && success == true{
                let typeArray :NSArray = jsonDic["tab_type"] as! NSArray
                if (typeArray != nil) && typeArray is NSArray {
                    self?.newkeyArray.removeAllObjects()
                    typeArray.enumerateObjects { (dic , idx, stop) -> Void in
                        let dic = dic as? NSDictionary
                        let key = String(format: "%@", (dic?.object(forKey: "key") as! String))
                        self?.newkeyArray.add(key)
                    }
                    var  isUploadKaQuanUI = self?.filterArr(array1: (self?.kaQuanKeyArray)!, andArr2: (self?.newkaQuanKeyArray)!)
                    if isUploadKaQuanUI!{
                       //充值卡券的商品标签这次请求的数据跟上次不同
                        self?.kaQuanTitleArray.removeAllObjects()
                        self?.kaQuanKeyArray.removeAllObjects()
                        typeArray.enumerateObjects({ (dic, idx, stop) in
                            let dic :NSDictionary = dic as! NSDictionary
                            
                            let key = String(format: "%@", (dic.object(forKey: "key") as! String))
                            let value =  String(format: "%@",  (dic.object(forKey: "value") as! String))
                            self?.kaQuanKeyArray.add(key)
                            self?.kaQuanTitleArray.add(value)
                        })
                        self?.prepareUI()
                    }else{
                        if isUploadKaQuanUI!{
                            self?.prepareUI()
                        }
                    }
                }
            }else{
                iToast.makeText(msg).show()
            }
        }) { (errMsg, isConnect) in
            if !isConnect{
                
            }
        }
    }
    
    func filterArr(array1:NSArray,andArr2 array2 :NSArray) -> Bool {
        if array1.count != array2.count{
            return true
        }else{
            var hasSame : Int = 0
            array1.enumerateObjects { (objc  , idx, stop) -> Void in
                let picUrl1  = array1[idx] as? String
                let picUrl2  = array2[idx] as? String
                if picUrl1 == picUrl2{
                    hasSame += 1
                }
            }
            if hasSame < array1.count{//至少有一个不同
                return true
            }else{//两个元素相同，hasSame不可能等于arr1.count
                return false
            }
        }
    }
    func prepareUI() {
        
    }
    func clearkaQuanKeyTheLastData() {
        self.kaQuanTitleArray.removeAllObjects()
        self.kaQuanKeyArray.removeAllObjects()
    }
    func clearKeyTheLastData() {
        self.titleArray.removeAllObjects()
        self.keyArray.removeAllObjects()
    }
    lazy var titleArray :NSMutableArray = {
        let titleArray = NSMutableArray.init()
        return titleArray
    }()
    lazy var classArray :NSMutableArray = {
        let classArray = NSMutableArray.init()
        return classArray
    }()
    lazy var keyArray :NSMutableArray = {
        let keyArray = NSMutableArray.init()
        return keyArray
    }()
   //新一次请求的key数组
    lazy var newkeyArray :NSMutableArray = {
        let newkeyArray = NSMutableArray.init()
        return newkeyArray
    }()
    
    //充值卡券
    lazy var kaQuanTitleArray :NSMutableArray = {
        let kaQuanTitleArray = NSMutableArray.init()
        return kaQuanTitleArray
    }()
    lazy var kaQuanClassArray :NSMutableArray = {
        let kaQuanClassArray = NSMutableArray.init()
        return kaQuanClassArray
    }()
    lazy var kaQuanKeyArray :NSMutableArray = {
        let kaQuanKeyArray = NSMutableArray.init()
        return kaQuanKeyArray
    }()
    //新一次请求的卡券key数组
    lazy var newkaQuanKeyArray :NSMutableArray = {
        let newkaQuanKeyArray = NSMutableArray.init()
        return newkaQuanKeyArray
    }()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchMallHomeData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /// 析构函数.类似于OC的 dealloc
    deinit {
        /// 移除通知
        NotificationCenter.default.removeObserver(self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
