//
//  YKMallShowViewController.m
//  JZYK
//
//  Created by zhaoying on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMallShowViewController.h"
#import "YKMallCollectionViewCell.h"
#import "YKMallHeaderCollectionReusableView.h"
#import "YKMallBannerCollectionReusableView.h"
#import "YKMallDetailViewController.h"
#import "YKMallListModel.h"
#import "YKEmptyView.h"


@interface YKMallShowViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewLayout;
@property (nonatomic, assign) NSInteger currentPage;//当前页
@property (nonatomic,strong) NSMutableArray *shopLisModelArray;
@property (nonatomic,strong) YKMallListModel * mallLisModel;
@end

static NSString *const cellId = @"YKMallCollectionViewCell";
static NSString *const headerId = @"YKMallHeaderCollectionReusableView";
static NSString *const bannerId = @"YKMallBannerCollectionReusableView";
static NSString *const intervalId = @"intervalId";

@implementation YKMallShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareUI];
    [self fetchMallActivityShopListData:self.installment_type isLoadMore:NO];
}
- (void)prepareUI
{
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKMallCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:cellId];
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKMallHeaderCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerId];
    [self.collectionView registerNib:[UINib nibWithNibName:@"YKMallBannerCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:bannerId];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:intervalId];
    
    self.collectionViewLayout.itemSize = CGSizeMake(WIDTH_OF_SCREEN / 2 - 4,95 + WIDTH_OF_SCREEN / 2 - 4);
    self.collectionViewLayout.minimumLineSpacing = 8;
    self.collectionViewLayout.minimumInteritemSpacing = 8;
    self.collectionViewLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    WEAK_SELF
    self.collectionView.mj_header = [YKRefreshHeader headerWithRefreshingBlock:^{
        //拉取信息
        [weakSelf fetchMallActivityShopListData:weakSelf.installment_type isLoadMore:NO];
    }];
  
    self.collectionView.mj_footer = [YKRefreshFooter footerWithRefreshingBlock:^{
        [weakSelf fetchMallActivityShopListData:weakSelf.installment_type isLoadMore:YES];
    }];
}
//MARK: UICollectionViewDataSource
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.mallLisModel.banner && [self.mallLisModel.shop_list_title yk_isValidString]) {
        return 2;
    }
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.mallLisModel.banner && [self.mallLisModel.shop_list_title yk_isValidString]) {
        //有banner
        if (section == 0) {
            return 0;
        }
    }
    return self.shopLisModelArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YKMallCollectionViewCell *cell = (YKMallCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    if (indexPath.row < self.shopLisModelArray.count) {
        YKMallListShopListItemModel * shopListModel = [self.shopLisModelArray objectAtIndex:indexPath.row];
        [cell yk_renderCollectionViewCellWithMallListShopListItemModel:shopListModel];
    }
    return cell;
}


//header的size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{

    if (self.mallLisModel.banner && [self.mallLisModel.shop_list_title yk_isValidString]) {
        if (section == 0) {
            return CGSizeMake(WIDTH_OF_SCREEN, 20 + 95*ASPECT_RATIO_WIDTH);
        }else{
            return CGSizeMake(WIDTH_OF_SCREEN, 36);
        }
    }else if (self.mallLisModel.banner && ![self.mallLisModel.shop_list_title yk_isValidString]){
        return CGSizeMake(WIDTH_OF_SCREEN, 20 + 95*ASPECT_RATIO_WIDTH);
    }else if (!self.mallLisModel.banner && [self.mallLisModel.shop_list_title yk_isValidString]){
        return CGSizeMake(WIDTH_OF_SCREEN, 36);
    }else{
        //既没有标题也没有banner
        return CGSizeMake(WIDTH_OF_SCREEN, 10);
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        if (self.mallLisModel.banner && [self.mallLisModel.shop_list_title yk_isValidString]) {
            if (indexPath.section == 0 ) {
                YKMallBannerCollectionReusableView *headerView = (YKMallBannerCollectionReusableView *) [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:bannerId forIndexPath:indexPath];
                [headerView yk_renderBannerModels:self.mallLisModel.banner];
                [headerView setBannerClickBlock:^(YKMallBannerModel *bannerModel) {
                    JumpModel *jumpModel = [JumpModel new];
                    jumpModel.skip_code = bannerModel.skip_code;
                    jumpModel.url = bannerModel.url;
                    jumpModel.data = bannerModel.data;
                    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:jumpModel];
                }];
                return headerView;
            } else {
                YKMallHeaderCollectionReusableView *headerView = (YKMallHeaderCollectionReusableView *) [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerId forIndexPath:indexPath];
                [headerView yk_renderShopTitle:self.mallLisModel];
                return headerView;
            }
            
        }else if (self.mallLisModel.banner && ![self.mallLisModel.shop_list_title yk_isValidString]){
            //有banner，无title
            YKMallBannerCollectionReusableView *headerView = (YKMallBannerCollectionReusableView *) [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:bannerId forIndexPath:indexPath];
            [headerView yk_renderBannerModels:self.mallLisModel.banner];
            [headerView setBannerClickBlock:^(YKMallBannerModel *bannerModel) {
                JumpModel *jumpModel = [JumpModel new];
                jumpModel.skip_code = bannerModel.skip_code;
                jumpModel.url = bannerModel.url;
                jumpModel.data = bannerModel.data;
                [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:jumpModel];
            }];
            return headerView;
            
        }else if (!self.mallLisModel.banner && [self.mallLisModel.shop_list_title yk_isValidString]){
            //无banner，有title
            YKMallHeaderCollectionReusableView *headerView = (YKMallHeaderCollectionReusableView *) [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerId forIndexPath:indexPath];
            return headerView;
        }else{
            UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:intervalId forIndexPath:indexPath];
            headerView.backgroundColor = [Color backgroundColor];
            return headerView;
        }
    }
    return nil;
}

//点击item方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.shopLisModelArray.count) {
        YKMallListShopListItemModel * shopListModel = [self.shopLisModelArray objectAtIndex:indexPath.row];
        //这里做一个判断如果当前页面不是YKMallDetailViewController的时候才跳转到商品详情，防止商品列表页快速点击商品，会进入多个商品详情页
        if (![[[YKTabBarController yk_shareTabController] yk_getCurrentViewController] isKindOfClass:[YKMallDetailViewController class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                YKMallDetailViewController *mallDetailVC = [YKMallDetailViewController new];
                mallDetailVC.shopList_id = shopListModel.shopList_id;
                [self.navigationController pushViewController:mallDetailVC animated:YES];
            });
        }
    }
}
- (void)fetchMallActivityShopListData:(NSString *)installmentType isLoadMore:(BOOL)isLoadMore
{
    if (!isLoadMore) {
        self.currentPage = 1;
    }else{
        self.currentPage ++;
    }
    NSString *pageStr = [NSString stringWithFormat:@"%ld",self.currentPage?:1];
    NSDictionary *param = @{@"banner_id":installmentType,@"page":pageStr};
    [[HTTPManager session] getRequestForKey:kMallGetShopListById showLoading:NO param:param succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDic, NSInteger code , BOOL success, NSString *msg) {
         [self.collectionView df_hideHud];
         self.collectionView.mj_footer.hidden = YES;
        if (code == 0 && success) {
            DLog(@" ===%@ ",jsonDic);
            if (isLoadMore) {
                //是上拉加载更多
                YKMallBannerModel *banner = self.mallLisModel.banner;
                NSString *shop_list_title = self.mallLisModel.shop_list_title;
                self.mallLisModel = [YKMallListModel yy_modelWithJSON:jsonDic];
                self.mallLisModel.banner = banner;
                self.mallLisModel.shop_list_title = shop_list_title;
            }else{
                //如果不是上拉加载更多,是下拉刷新
                [self.shopLisModelArray removeAllObjects];
                self.mallLisModel = [YKMallListModel yy_modelWithJSON:jsonDic];
            }
              self.navigationItem.title = self.mallLisModel.title;
            if (self.mallLisModel.shop_list.count) {
                [self.shopLisModelArray addObjectsFromArray:self.mallLisModel.shop_list];
                if (self.shopLisModelArray.count > 10) {
                    self.collectionView.mj_footer.hidden = NO;
                }
                if (self.mallLisModel.shop_list_count < self.mallLisModel.page_size) {
                    [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                }else if (self.mallLisModel.shop_list_count == self.mallLisModel.page_size){
                    [self.collectionView.mj_footer endRefreshing];
                }
               
            }else{
                if (!isLoadMore) {
                    self.collectionView.df_hud = [UIView df_normalHudViewWithTitle:@"暂无商品哦~" textColor:[UIColor yk_colorWithHexString:@"#999999"] font:[UIFont systemFontOfSize:14.0f] andImage:[UIImage imageNamed:@"img_wushop"] titleImageMargin:0];
                }
                if (self.mallLisModel.shop_list_count <= self.mallLisModel.page_size) {
                    [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            [self.collectionView reloadData];
        }else if (code !=0 ){
            [[iToast makeText:msg] show];
        }
        [self.collectionView.mj_header endRefreshing];
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
         [[iToast makeText:errMsg] show];
        if (!isConnect) {
            self.collectionView.mj_footer.hidden = YES;
            [self.collectionView.mj_footer endRefreshing];
            [self.collectionView.mj_header endRefreshing];
            [self.shopLisModelArray removeAllObjects];
            self.mallLisModel.banner = nil;
            self.mallLisModel.shop_list_title = nil;
            [self.collectionView reloadData];
            self.collectionView.df_hud = [UIView df_defaultNoNetHudViewWithTitle:@"暂无网络，请点击重试" imageTitleMargin:0 btnMargin:59 btnBlock:^(UIButton *btn) {
                [self reloadThisData:installmentType];
            }];
        }
    }];
}
- (void)reloadThisData:(NSString *)installmentType
{
    [self fetchMallActivityShopListData:installmentType isLoadMore:NO];
}
- (NSMutableArray *)shopLisModelArray
{
    if (!_shopLisModelArray) {
        _shopLisModelArray = [NSMutableArray array];
    }
    return _shopLisModelArray;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
