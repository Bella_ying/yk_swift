//
//  ModifyPwdController.swift
//  JZYK
//
//  Created by Jeremy on 2018/6/17.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class ModifyPwdController: YKBaseViewController {
    private var isRequesting: Bool = false;   //阻止出现两个连续的网络请求发生flag
    @IBOutlet weak var oldPwdView: UIView!
    @IBOutlet weak var newPwdView: UIView!
    @IBOutlet weak var newPwdView02: UIView!
    @IBOutlet weak var nextButton: Button!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var phoneNumLabel: UILabel!
    
    private lazy var oldPwdInput = InputView.input
    private lazy var newPwdInput = InputView.input
    private lazy var confirmNewPwdInput = InputView.input
    
    var phoneNum: String?
    
    @IBAction func forgetPwd(_ sender: UIButton) {
        guard let phone = phoneNum, phone.count == 11 else {
            iToast.makeText("请正确输入手机号码，再次点击").show();
            return;
        }
        CodeSecure.md5Encrypt(phoneNum: phone, encryptedCallBack: { (sign, random) in
            let dict = ["phone": phone,
                        "random": random,
                        "sign": sign,
                        "type" : "find_pwd"]
            HTTPManager.session.postRequest(forKey: kUserResetPwdCode, param: dict, succeed: { (json, code, unwrap, msg) in
                if code == 0 {
                    guard let isVerify: Bool = json["real_verify_status"] as? Bool else {
                        return;
                    }
                    let findVC = FindLoginPwdViewController()
                    findVC.phoneNum = phone;
                    findVC.isUserVerified = isVerify;
                    self.navigationController?.pushViewController(findVC, animated: true)
                } else {
                    iToast.makeText(msg).show();
                }
            }, failure: { (errMsg, isConnect) in
                
            })
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideNavigationBarLine()
        self.view.backgroundColor = UIColor.white
        setupInputZone()

        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        self.containerView.addGestureRecognizer(tap)
        
        self.nextButton.onClick = {[weak self] (button) in
            self?.sendModifyRequest()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        if let user = YKCacheManager.shared().yk_getCacheObject(forKey: kUserLoginCacheKey) as? YKUserModel {
            self.phoneNumLabel.text = user.username.phoneSecurityNum;
            phoneNum = user.username;
        } else {
            if YKUserManager.sharedUser().yk_isLogin() {
                self.phoneNumLabel.text = YKUserManager.sharedUser().username.phoneSecurityNum;
            }            
        }
        self.nextButton.switchButtonStatus(false);
    }
    
    @objc func tapAction() {
        self.containerView.endEditing(true)
    }
}


private extension ModifyPwdController {
    func setupInputZone() {
        oldPwdInput.inputType = InputType.loginPwd
        oldPwdInput.removeAllButtons()
        oldPwdInput.textField.placeholder = "请输入旧密码"
        oldPwdInput.textField.isSecureTextEntry = false;
        oldPwdView.addSubview(oldPwdInput)
        
        newPwdInput.inputType = InputType.loginPwd
        newPwdInput.removeAllButtons()
        newPwdInput.textField.placeholder = "请输入新密码"
        newPwdInput.textField.isSecureTextEntry = false;
        newPwdView.addSubview(newPwdInput)
        
        confirmNewPwdInput.inputType = InputType.loginPwd
        confirmNewPwdInput.removeAllButtons()
        confirmNewPwdInput.textField.placeholder = "请确认新密码"
        confirmNewPwdInput.textField.isSecureTextEntry = false;
        newPwdView02.addSubview(confirmNewPwdInput)
        changeControllerButton(nextButton, firstInput: oldPwdInput, secondInput: confirmNewPwdInput);
    }
    
    func sendModifyRequest() {
        if self.isRequesting {
            return;
        }else {
            self.isRequesting = true;
        }
        guard let oldPwd = oldPwdInput.textField.text else {
            self.isRequesting = false;
            return
        }
        guard let newPwd = confirmNewPwdInput.textField.text else {
            self.isRequesting = false;
            return
        }
        if newPwd != newPwdInput.textField.text! {
             iToast.makeText("您两次输入的密码不统一，请检查输入").show()
            self.isRequesting = false;
            return
        }
        let dict = ["new_pwd": newPwd,
                    "old_pwd": oldPwd]
        HTTPManager.session.postRequest(forKey: kUserChangePwd, param: dict, succeed: { (json, code, unwrap, msg) in
            if code == 0 {
                QLAlert().show(withMessage: msg, singleBtnTitle: "确定", btnClicked: { (_) in
                    self.navigationController?.popViewController(animated: true);
                })
            } else {
                iToast.makeText(msg).show()
            }
            self.isRequesting = false;
        }, failure: { (errMsg, isConnect) in
            
        })
    }
}
