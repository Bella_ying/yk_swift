//
//  CodeAndPwdViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class CodeAndPwdViewController: YKBaseViewController {
     private var isRequesting: Bool = false;   //阻止出现两个连续的网络请求发生flag
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var pwdView: UIView!
    @IBOutlet weak var mainButton: Button!
    
    lazy var codeInputView = InputView.input
    lazy var pwdInputView = InputView.input
    private var isProtocolSelect: Bool = true;
    var phoneNum: String?
    public var isFromRegist: Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideNavigationBarLine()
        
        setupInputZone()
        
        self.mainButton.onClick = { [weak self] button in
            if !(self?.isProtocolSelect)! {
                iToast.makeText("您需要同意下列协议才能登录").show()
            } else {
                self?.registAction()
            }
        }
        
        setupProtocolView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor.white
        if isFromRegist {
            CodeButton.timerCountDown(forButton: codeInputView.codeButton, title: "秒") { }
            self.mainButton.switchButtonStatus(false);
            isFromRegist = false;
        }
        
        changeControllerButton(mainButton, firstInput: codeInputView, secondInput: pwdInputView)
    }

}

private extension CodeAndPwdViewController {
    func setupInputZone() {
        
        codeInputView.inputType = InputType.code
        codeInputView.secureButton.removeFromSuperview()
        codeInputView.codeButton.codeType = .registCode
        codeInputView.codeButton.codeAction = { [weak self] button in
            return self?.phoneNum
        }
        codeView.addSubview(codeInputView)
        
        pwdInputView.inputType = InputType.loginPwd
        pwdInputView.textField.isSecureTextEntry = false;
        pwdInputView.secureButton.isSelected = true;
        
        pwdInputView.textField.placeholder = "请设置6~16位登录密码"
        
        pwdInputView.codeButton.removeFromSuperview()
        pwdView.addSubview(pwdInputView)
        
    }
    
    func setupProtocolView() {
        let protocolView = ProtocolView(frame: .zero,
                                        protocolStartSting: "我已阅读并同意下列协议",
                                        protocolStrings: ["《注册协议》","《用户隐私保护协议》"],
                                        selectedIconName: "choice_pressed",
                                        unselectedIconName: "choice_normal",
                                        isCheckBoxSelect: true,
                                        checkBoxSelection: { (isSelect) in
                                            
                                            self.isProtocolSelect = isSelect;
        }) { (index) in
            
            let web = YKBrowseWebController()
            if index == 0 {
                web.url = ConfigManager.config.registProtocolText
            } else if index == 1 {
                web.url = ConfigManager.config.privateProtocolText
            }
            self.navigationController?.pushViewController(web, animated: true);
        }
        
        self.view.addSubview(protocolView);
        protocolView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(mainButton.snp.bottom).offset(15);
            make.left.equalToSuperview().offset(50);
            make.right.equalToSuperview().offset(-50);
            make.height.equalTo(70);
        }
    }
}

//MARK:
private extension CodeAndPwdViewController {
    func registAction() {
        if self.isRequesting {
            return;
        }else {
            self.isRequesting = true;
        }
        guard let phone = self.phoneNum, !phone.isEmpty else {
            self.isRequesting = false;
            return
        }
        guard let code = codeInputView.textField.text, !code.isEmpty else {
            self.isRequesting = false;
            return
        }
        guard let password = pwdInputView.textField.text, !password.isEmpty else {
            self.isRequesting = false;
            return
        }
        
        let dict = ["phone": phone,
                    "code": code,
                    "password":password,
                    "source":"21"] //后台修改  14 ->21
        HTTPManager.session.postRequest(forKey: kKDZhuceKey, param: dict, succeed: { (json, code, unwrap, msg) in
            if code == 0 {
                guard let userModel = YKUserModel.yy_model(with: json) else {
                    return
                }
                
                BrAgentManager.yk_shared().yk_initBrAgent(with: BRAGENTTYPE.BRREGISTER)
                YKUserManager.sharedUser().yk_loginSuccess(toUpdateUserInfo: userModel)
                let toast = iToast.makeText(msg);
                toast?.toastDidDisappear_blk = {
                    self.navigationController?.popToRootViewController(animated: false)
                    LoginViewController.login.loginSuccess()
                };
                toast?.show()
            } else {
                iToast.makeText(msg).show()
            }
            self.isRequesting = false;
        }) { (errMsg, isConnect) in
            
        }
    }
    
    func loginSuccess() {
        self.dismiss(animated: true, completion: nil)
    }
}
