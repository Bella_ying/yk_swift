//
//  LoginViewController.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

enum SighInType {
    case quickLogin
    case pwdLogin
}

class LoginViewController: SwipeViewController {
    private var isRequesting: Bool = false;   //阻止出现两个连续的网络请求发生flag
    
    lazy var quickLoginVc: QuickLoginViewController = {
        let quickLoginVc = QuickLoginViewController()
        quickLoginVc.title = "快捷登录"
        quickLoginVc.view.backgroundColor = Color.backgroundColor
        quickLoginVc.quickPhoneNumInput.textFieldClearCallBack = {[weak self] (tf) in
            self?.pwdLoginVc.pwdPhoneNumInput.textField.text = "";
        }
        return quickLoginVc
    }()
    
    lazy var pwdLoginVc: PwdLoginViewController = {
        let pwdLoginVc = PwdLoginViewController()
        pwdLoginVc.title = "账号密码登录"
        pwdLoginVc.view.backgroundColor = Color.backgroundColor
        pwdLoginVc.pwdPhoneNumInput.textFieldClearCallBack = {[weak self] (tf) in
            self?.quickLoginVc.quickPhoneNumInput.textField.text = "";
        }
        return pwdLoginVc
    }()
    
    @objc public static let login: LoginViewController = {
        let pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        let navigationController = LoginViewController(rootViewController: pageController)
        navigationController.setFirstViewController(0)
        return navigationController
    }()
    
    public static func showLoginPage() -> LoginViewController {
        let pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        let navigationController = LoginViewController(rootViewController: pageController)
        navigationController.setFirstViewController(0)
        return navigationController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationTitle = "登录"
        self.setupLoginControllers()
        
        quickLoginVc.loginButton.onClick = {[weak self] (button) in
            
            self?.loginAction(button, type: .quickLogin)
        }
        
        pwdLoginVc.loginButton.onClick = { (button) in
            self.loginAction(button, type: .pwdLogin)
        }
        
        quickLoginVc.quickPhoneNumInput.textField.addTarget(self, action: #selector(textChange(textField:)), for: .editingChanged);
        pwdLoginVc.pwdPhoneNumInput.textField.addTarget(self, action: #selector(textChange(textField:)), for: .editingChanged);
    }
    
    @objc func textChange(textField: UITextField) {
        self.quickLoginVc.quickPhoneNumInput.textField.text = textField.text;
        self.pwdLoginVc.pwdPhoneNumInput.textField.text = textField.text;
        
        guard let count = textField.text?.count else { return }
        let isTrue: Bool = (count >= 11);
        if !(self.quickLoginVc.smsCodeInput.codeButton.titleLabel?.text?.contains("秒"))! {
            self.quickLoginVc.smsCodeInput.codeButton.switchButtonStatus(isTrue)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        if let user = YKCacheManager.shared().yk_getCacheObject(forKey: kUserLoginCacheKey) as? YKUserModel {
            quickLoginVc.quickPhoneNumInput.textField.text = user.username;
            pwdLoginVc.pwdPhoneNumInput.textField.text = user.username;
        } else {
            pwdLoginVc.pwdPhoneNumInput.textField.text = "";
            quickLoginVc.quickPhoneNumInput.textField.text = "";
        }
        
        if let text = UserDefaults.standard.value(forKey: "oldUser") as? String, !text.isEmpty {
            quickLoginVc.quickPhoneNumInput.textField.text = text;
        }
        
        handleUIForSuccessLogin()
        setFirstController(0)
        guard let count = quickLoginVc.quickPhoneNumInput.textField.text?.count else { return }
        if !(quickLoginVc.smsCodeInput.codeButton.titleLabel?.text?.contains("秒"))! {
            quickLoginVc.smsCodeInput.codeButton.switchButtonStatus(count >= 11)            
        }
        
        if (quickLoginVc.smsCodeInput.codeButton.titleLabel?.text?.contains("重新"))! {
            self.quickLoginVc.smsCodeInput.codeButton.setTitle("获取", for: .normal);
        }
    }
    
    func loginSuccess() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: view
private extension LoginViewController {
    func setupLoginControllers() {
        setViewControllerArray([quickLoginVc, pwdLoginVc])
        setSelectionBar(23, height: 2, color: Color.main)
        setButtonsWithSelectedColor(UIFont.systemFont(ofSize: 14),
                                    color: Color.color_66_C3,
                                    selectedColor: Color.main)
        
        equalSpaces = false
    }
}

//MARK: network
private extension LoginViewController {
    func loginAction(_ sender: UIButton, type: SighInType) {
        if self.isRequesting {
            return;
        }else {
            self.isRequesting = true;
        }
        var dict: Dictionary<String, Any> = [:]
        var key = ""
        switch type {
        case .quickLogin:
            guard let userName = self.quickLoginVc.phoneNumText, !userName.isEmpty else {return}
            guard let code = self.quickLoginVc.codeText, !code.isEmpty else { return }
            dict = ["username": userName,
                    "code": code]
            key = kUserQuickLoginBySms
        case .pwdLogin:
            guard let userName = self.pwdLoginVc.phoneNumText, !userName.isEmpty else { return }
            guard let password = self.pwdLoginVc.pwdText, !password.isEmpty else { return }
            dict = ["username": userName,
                    "password": password]
            key = kKDLoginKey
        }
        
        HTTPManager.session.postRequest(forKey: key, param: dict, succeed: { (json, code, unwrapNormal, msg) in
            if code == 0 {
                guard let userModel = YKUserModel.yy_model(with: json) else {
                    return
                }
                YKUserManager.sharedUser().yk_loginSuccess(toUpdateUserInfo: userModel)
                
                self.loginSuccess()
            }
            else if code == -4 {
                QLAlert().show(withMessage: msg, btnTitleArray: ["知道了"], btnClicked: { (_) in
                    self.swipeController(to: 0)
                    self.pwdLoginVc.pwdInput.textField.text = ""   //清空密码
                    self.pwdLoginVc.loginButton.switchButtonStatus(false);  //button置灰
                })
            }
            else {
                print(msg)
                iToast.makeText(msg).show()
            }
            self.isRequesting = false;
        }) { (errMsg, isConnect) in
            
        }
    }
    
    func handleUIForSuccessLogin() {
        self.pwdLoginVc.pwdInput.textField.text = "";
        self.quickLoginVc.smsCodeInput.textField.text = "";
        self.quickLoginVc.loginButton.switchButtonStatus(false);
        self.pwdLoginVc.loginButton.switchButtonStatus(false);
    }
}
