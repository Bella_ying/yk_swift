//
//  QuickLoginViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class QuickLoginViewController: YKBaseViewController {
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var loginButton: Button!
    @IBOutlet weak var codeView: UIView!
    public lazy var quickPhoneNumInput = InputView.input
    public lazy var smsCodeInput = InputView.input
    var buttonCanClick: Bool {
        get {
            if (quickPhoneNumInput.textField.text?.count == 11 && smsCodeInput.textField.text?.count == 6) {
                loginButton.backgroundColor = Color.main
                loginButton.isEnabled = true
                return true
            }
            loginButton.backgroundColor = Color.mainButtonDisabledColor
            loginButton.isEnabled = false
            return false
        }
    }
    
    
    var phoneNumText: String? {
        return quickPhoneNumInput.textField.text
    }
    var codeText: String? {
        return smsCodeInput.textField.text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginButton.switchButtonStatus(false)
        loginButton.setTitle("登录", for: .normal);
        setupInputZone()
        setupProtocolView()
        changeControllerButton(loginButton, firstInput: quickPhoneNumInput, secondInput: smsCodeInput);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        if let text = UserDefaults.standard.value(forKey: "oldUser") as? String, !text.isEmpty {
            self.quickPhoneNumInput.textField.text = text;
        }
    }
    
}

private extension QuickLoginViewController {
    
    func setupInputZone() {
        quickPhoneNumInput.inputType = InputType.phoneNum
        quickPhoneNumInput.removeAllButtons()
        phoneView.addSubview(quickPhoneNumInput)
        
        smsCodeInput.inputType = InputType.code
        smsCodeInput.secureButton.removeFromSuperview()
        //点击发送验证码
        smsCodeInput.codeButton.codeType = .quckLogin
        smsCodeInput.codeButton.codeAction = { [weak self] button in
            return self?.phoneNumText
        }
        codeView.addSubview(smsCodeInput)
        
    }
    
    func setupProtocolView() {
        let protocolView = ProtocolView(frame: .zero,
                                        protocolStartSting: "点击“登录”即表示您已同意",
                                        protocolStrings: ["《注册协议》","《用户隐私保护协议》"],
                                        selectedIconName: "",
                                        unselectedIconName: "",
                                        isCheckBoxSelect: true,
                                        checkBoxSelection: { (isSelect) in
        }) { (index) in
            let web = YKBrowseWebController()
            if index == 0 {
                web.url = ConfigManager.config.registProtocolText
            } else if index == 1 {
                web.url = ConfigManager.config.privateProtocolText
            }
            self.navigationController?.pushViewController(web, animated: true);
        }
        
        self.view.addSubview(protocolView);
        protocolView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(loginButton.snp.bottom).offset(15);
            make.left.equalToSuperview().offset(50);
            make.right.equalToSuperview().offset(-50);
            make.height.equalTo(70);
        }
    }
    
}
