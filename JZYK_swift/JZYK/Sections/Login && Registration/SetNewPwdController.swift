//
//  SetNewPwdController.swift
//  JZYK
//
//  Created by Jeremy on 2018/6/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class SetNewPwdController: YKBaseViewController {
    private var isRequesting: Bool = false;   //阻止出现两个连续的网络请求发生flag
    @IBOutlet weak var newPwd: UIView!
    @IBOutlet weak var oldPwd: UIView!
    @IBOutlet weak var finishButton: Button!
    private lazy var newPwdInput = InputView.input;
    private lazy var oldPwdInput = InputView.input;
    public var phoneNumText: String = "";
    public var codeText: String = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.finishButton.switchButtonStatus(false);
        setupInputZone()
        
        finishButton.onClick = {[weak self] (button) in
            self?.sendFinalRequest()
        }
    }
    
}

private extension SetNewPwdController {
    func setupInputZone() {
        newPwdInput.inputType = InputType.loginPwd
        newPwdInput.textField.placeholder = "请输入新密码";
        newPwdInput.codeButton.removeFromSuperview()
        newPwdInput.secureButton.removeFromSuperview();
        newPwd.addSubview(newPwdInput)
        
        oldPwdInput.inputType = InputType.loginPwd
        oldPwdInput.textField.placeholder = "请确认新密码";
        oldPwdInput.codeButton.removeFromSuperview()
        oldPwdInput.secureButton.removeFromSuperview();
        oldPwd.addSubview(oldPwdInput)
        changeControllerButton(finishButton, firstInput: newPwdInput, secondInput: oldPwdInput)
    }
    
    func sendFinalRequest() {
        if self.isRequesting {
            return;
        }else {
            self.isRequesting = true;
        }
        if newPwdInput.textField.text != oldPwdInput.textField.text {
            iToast.makeText("您两次输入的密码不统一，请检查输入").show()
            self.isRequesting = false;
            return
        }
        
        guard let pwd = newPwdInput.textField.text else {
            return
        }
        let dict = ["code": self.codeText,
                    "password": pwd,
                    "phone": self.phoneNumText]
        HTTPManager.session.postRequest(forKey: kUserResetPassword, param: dict, succeed: { (json, code, unwrap, msg) in
            if code == 0 {
                QLAlert().show(withMessage: msg, singleBtnTitle: "确定", btnClicked: { (_) in
                    self.navigationController?.popToRootViewController(animated: true)
                })
            } else {
                iToast.makeText(msg).show();
            }
            self.isRequesting = false;
        }, failure: { (errMsg, isConnect) in
            
        })

    }
}
