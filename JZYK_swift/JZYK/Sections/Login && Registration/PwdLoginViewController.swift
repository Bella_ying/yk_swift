//
//  PwdLoginViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class PwdLoginViewController: YKBaseViewController {
    @IBOutlet weak var loginButton: Button!
    @IBOutlet weak var numView: UIView!
    @IBOutlet weak var pwdView: UIView!
    public lazy var pwdPhoneNumInput = InputView.input
    public lazy var pwdInput = InputView.input
    @IBOutlet weak var forgetButton: UIButton!
    @IBOutlet weak var registButton: UIButton!
    @IBOutlet weak var bottonConstraint: NSLayoutConstraint!
    
    var phoneNumText: String? {
        return pwdPhoneNumInput.textField.text
    }
    var pwdText: String? {
        return pwdInput.textField.text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginButton.switchButtonStatus(false)
        forgetButton.setTitleColor(Color.main, for: .normal);
        registButton.setTitleColor(Color.main, for: .normal);
        setupInputZone()
        if isIphoneX() {
            bottonConstraint.constant += 34;
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
       
        if let text = UserDefaults.standard.value(forKey: "oldUser") as? String, !text.isEmpty {
            self.pwdPhoneNumInput.textField.text = text;
        }
        
        
        changeControllerButton(loginButton, firstInput: pwdPhoneNumInput, secondInput: pwdInput)
    }
    
    @IBAction func forgetPwdAction(_ sender: UIButton) {
        guard let phone = pwdPhoneNumInput.textField.text, phone.count == 11 else {
            iToast.makeText("请正确输入手机号码，再次点击").show();
            return;
        }
        CodeSecure.md5Encrypt(phoneNum: phone, encryptedCallBack: { (sign, random) in
            let dict = ["phone": phone,
                        "random": random,
                        "sign": sign,
                        "type" : "find_pwd"]
            HTTPManager.session.postRequest(forKey: kUserResetPwdCode, param: dict, succeed: { (json, code, unwrap, msg) in
                if code == 0 && unwrap {
                    guard let isVerify: Bool = json["real_verify_status"] as? Bool else {
                        return;
                    }
                    let findVC = FindLoginPwdViewController()
                    findVC.phoneNum = phone;
                    findVC.isUserVerified = isVerify;
                    self.navigationController?.pushViewController(findVC, animated: true)
                } else {
                    iToast.makeText(msg).show();
                }
            }, failure: { (errMsg, isConnect) in
                
            })
            
        })
        
        
    }
    
    @IBAction func registAction(_ sender: UIButton) {
        
        let regist = RegistViewController();
        self.navigationController?.pushViewController(regist, animated: true)
    }
}

private extension PwdLoginViewController {
    
    func setupInputZone() {
        
        pwdPhoneNumInput.inputType = InputType.phoneNum
        pwdPhoneNumInput.removeAllButtons()
        numView.addSubview(pwdPhoneNumInput)
        
        
        pwdInput.inputType = InputType.loginPwd
        pwdInput.codeButton.removeFromSuperview()
        pwdView.addSubview(pwdInput)

        
    }
}
