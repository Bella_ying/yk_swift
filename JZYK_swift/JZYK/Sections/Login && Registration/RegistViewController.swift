//
//  RegistViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class RegistViewController: YKBaseViewController {
    private var isRequesting: Bool = false;   //阻止出现两个连续的网络请求发生flag
    @IBOutlet weak var inputField: UIView!
    @IBOutlet weak var mainButton: Button!
    private lazy var phoneNumInput = InputView.input
    var phoneNum: String = ""
    var oldUserCallBack: ((String)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.view.backgroundColor = UIColor.white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideNavigationBarLine()
        self.mainButton.switchButtonStatus(false);
        phoneNumInput.inputType = InputType.phoneNum
        phoneNumInput.textField.placeholder = "请输入注册/登录手机号";
        phoneNumInput.removeAllButtons()
        self.inputField.addSubview(phoneNumInput)
        phoneNumInput.textFieldDidChangeCallBack = {[weak self] (tf) in
            guard let count = tf.text?.count else { return }
            if count >= 11{
                self?.mainButton.switchButtonStatus(true);
            }else{
                self?.mainButton.switchButtonStatus(false);
            }
        }
        
        self.mainButton.onClick = { [weak self] button in
            guard let strongSelf = self else { return }
            let next = CodeAndPwdViewController()
            next.phoneNum = strongSelf.phoneNumInput.textField.text
            next.isFromRegist = true;
            if let phone = next.phoneNum, phone.count == 11 {
                self?.phoneNum = phone;
                CodeSecure.md5Encrypt(phoneNum: phone, encryptedCallBack: { (sign, random) in
                    let dict = ["phone": phone,
                                "random": random,
                                "sign": sign]
                    strongSelf.checkRegistStatus(param: dict, check: {
                        strongSelf.navigationController?.pushViewController(next, animated: true)
                    })
                })
            } else {
                iToast.makeText("手机号码应该是合法的11位数字").show();
            }
        }
    }
}


private extension RegistViewController {
    func checkRegistStatus(param: Dictionary<String, Any> , check: @escaping()->Void) {
        if self.isRequesting {
            return;
        }else {
            self.isRequesting = true;
        }
        HTTPManager.session.postRequest(forKey: KUserRegGetCode, param: param, succeed: { (json, code, unwrap, msg) in

            if code == 0 {
                check()
            }
           else if code == 1000 {  //老用户，弹窗
                QLAlert().show(withTitle: "", message: "您已经是老用户了 直接去登录吧", btnTitleArray: ["确认","取消"], btnClicked: { (index) in
                    if index == 0 {
                        self.navigationController?.popViewController(animated: true);
                        UserDefaults.standard.set(self.phoneNum, forKey: "oldUser");
                    }
                })
            } else {
                iToast.makeText(msg).show();
            }
            self.isRequesting = false;
        }) { (errMsg, isConnect) in
        }
    }
}
