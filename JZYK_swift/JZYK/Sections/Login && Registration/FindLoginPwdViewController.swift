//
//  FindLoginPwdViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class FindLoginPwdViewController: YKBaseViewController {
    private var isRequesting: Bool = false;   //阻止出现两个连续的网络请求发生flag
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var IDView: UIView!
    @IBOutlet weak var codeView: UIView!
    
    @IBOutlet weak var nextButton: Button!
    @IBOutlet weak var botConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var containerView: UIView!
    private lazy var phoneNumInput = InputView.input
    private lazy var smsCodeInput = InputView.input
    private lazy var IDNumInput = InputView.input
    private lazy var nameInput = InputView.input
    public var isUserVerified: Bool = false;

    
    var phoneNum: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideNavigationBarLine()
        self.view.backgroundColor = UIColor.white
        setupInputZone()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        self.containerView.addGestureRecognizer(tap)
        
        self.nextButton.onClick = { [weak self] (button) in
            self?.sendRequestForNextPage(isUserVerified: (self?.isUserVerified)!)
        }
        if !isUserVerified {
            self.middleView.removeFromSuperview();
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        var phone = "";
        if let num = self.phoneNum {
            phone = num;
        } else {
            guard let user = YKCacheManager.shared().yk_getCacheObject(forKey: kUserLoginCacheKey) as? YKUserModel else { return
            }
            phone = user.username!
        }
        phoneNumInput.textField.text = phone.phoneSecurityNum;

         CodeButton.timerCountDown(forButton: smsCodeInput.codeButton, title: "秒") { }
       
        self.nextButton.switchButtonStatus(false);
    }
    
    @objc func tapAction() {
        self.containerView.endEditing(true)
    }
}


private extension FindLoginPwdViewController {
    func setupInputZone() {
        
        phoneNumInput.removeAllButtons()
        phoneNumInput.textField.placeholder = ""
        phoneNumInput.isUserInteractionEnabled = false
        phoneView.addSubview(phoneNumInput)
        
        nameInput.inputType = InputType.name
        nameInput.removeAllButtons()
        nameView.addSubview(nameInput)
        
        IDNumInput.inputType = InputType.IDNum
        IDNumInput.removeAllButtons()
        IDView.addSubview(IDNumInput)
        
        smsCodeInput.inputType = InputType.code
        smsCodeInput.secureButton.removeFromSuperview()
        smsCodeInput.codeButton.codeType = .resetPwd
        smsCodeInput.codeButton.codeAction = { [weak self] button in
            return self?.phoneNum
        }
        codeView.addSubview(smsCodeInput)
        smsCodeInput.textFieldDidChangeCallBack = {[weak self] (tf) in
            guard let count = tf.text?.count else { return }
            if count >= 6 {
                self?.nextButton.switchButtonStatus(true)
            } else {
                self?.nextButton.switchButtonStatus(false)
            }
        }
    }
    
    func sendRequestForNextPage(isUserVerified: Bool) {
        if self.isRequesting {
            return;
        }else {
            self.isRequesting = true;
        }
        
        var phone = "";
        if let num = self.phoneNum {
            phone = num;
        } else {
            guard let user = YKCacheManager.shared().yk_getCacheObject(forKey: kUserLoginCacheKey) as? YKUserModel else {
                self.isRequesting = false;
                return
            }
            phone = user.username!
        }
        
        guard let smsCode = smsCodeInput.textField.text else {
            iToast.makeText("请输入正确的验证码").show()
            self.isRequesting = false;
            return
        }
        var param = ["phone": phone,
                    "code": smsCode,
                    "type" : "find_pwd"]
        if isUserVerified {
            guard let IDNum = IDNumInput.textField.text else {
                iToast.makeText("请输入正确的身份证号码").show()
                return
            }
            guard let name = nameInput.textField.text else {
                iToast.makeText("姓名不能为空").show()
                return
            }
            param = ["phone": phone,
                     "id_card": IDNum,
                     "realname": name,
                     "code": smsCode,
                     "type" : "find_pwd"]
        }
        
        HTTPManager.session.postRequest(forKey: kUserVerifyResetPassword, param: param, succeed: { (json, code, unwrap, msg) in
            if code == 0 {
                let setNewVC = SetNewPwdController();
                setNewVC.phoneNumText = phone;
                setNewVC.codeText = smsCode;
                self.navigationController?.pushViewController(setNewVC, animated: true)
            } else {
                QLAlert().show(withMessage: msg, singleBtnTitle: "确定")
            }
            self.isRequesting = false;
        }, failure: { (errMsg, isConnect) in
            
        })
    }
}
