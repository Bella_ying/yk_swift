//
//  YKLoanPeriodsView.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKLoanPeriodsView : UIView

@property (nonatomic, assign) NSString * selectValue;

- (instancetype)initWithTitle:(NSString *)title;

- (void)renderViewsWithEntitys:(NSArray *)dataArr;

@end
