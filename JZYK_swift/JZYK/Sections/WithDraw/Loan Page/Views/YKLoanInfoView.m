//
//  YKLoanInfoView.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKLoanInfoView.h"
#import "YKSlide.h"

@interface YKLoanInfoView ()

//金额滑动杆
@property (nonatomic, strong) YKSlide *sliderView;
//滑竿最小值
@property (nonatomic, strong) UILabel *sliderMinValueLabel;
//滑竿最大值
@property (nonatomic, strong) UILabel *sliderMaxValueLabel;

@property (nonatomic, assign) BOOL alertAppear;
@property (nonatomic, assign) NSInteger month_amount;  //月份
@property (nonatomic, strong) UILabel *monthLabel;

@end

@implementation YKLoanInfoView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupLoanInfoView];
    }
    return self;
}

- (void)setupLoanInfoView{
    //借款金额(元)
    UILabel *borrowMoneyDesc = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_8D_C4 superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.top.offset(30);
        make.centerX.offset(0);
        
        label.text = @"借款金额（元）";
    }];
    
    //借款金额数据
    UILabel *amountLabel = [UILabel yk_labelWithFontSize:adaptFontSize(35) textColor:Color.color_45_C2 superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.top.equalTo(borrowMoneyDesc.mas_bottom).offset(10);
        make.centerX.offset(0);
        
        label.text = @"0";
    }];
    self.borrowMoneyLabel = amountLabel;
    
    //标尺
    UIImageView * rulerImgV = [UIImageView yk_imageViewWithImageName:@"ruler" superView:self masonrySet:^(UIImageView *imageView, MASConstraintMaker *make) {
        make.left.offset(15);
        make.right.offset(-15);
        make.top.equalTo(amountLabel.mas_bottom).offset(47);
    }];
    
    //金额滑动杆
    self.sliderView = [[YKSlide alloc] init];
    [self.sliderView setThumbImage:[UIImage imageNamed:@"slideThumb"] forState:UIControlStateNormal];
    self.sliderView.minimumValue = 0.0f;
    self.sliderView.maximumValue = 9.0f;
    self.sliderView.minimumTrackTintColor = Color.color_slideMiniTrack;
    self.sliderView.maximumTrackTintColor = Color.color_E6_C6;//滑轮右边的颜色
    [self.sliderView addTarget:self action:@selector(sliderMove:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.sliderView];
    [self.sliderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(rulerImgV.mas_left);
        make.right.equalTo(rulerImgV.mas_right);
        make.bottom.equalTo(rulerImgV.mas_top).offset(-5);
        make.height.offset(10);
    }];
    
    //滑块左边lab
    self.sliderMinValueLabel = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_8D_C4 superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.left.equalTo(self.sliderView.mas_left);
        make.top.equalTo(rulerImgV.mas_bottom).offset(3);
    }];
    
    //滑块右边lab
    self.sliderMaxValueLabel = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_8D_C4 superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.right.equalTo(self.sliderView.mas_right);
        make.centerY.equalTo(self.sliderMinValueLabel.mas_centerY);
    }];
    
    //借款用途
    UILabel * loanPurposeTitle = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_66_C3 superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.left.offset(25);
        make.top.equalTo(self.sliderMinValueLabel.mas_bottom).offset(28);
        
        label.text = @"借款用途";
    }];
    
    //借款用途lab
    UILabel * loanPurposeLab = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_CC_C8 superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.right.offset(-32);
        make.centerY.equalTo(loanPurposeTitle.mas_centerY);
        
        label.text = @"请选择";
        label.textAlignment = NSTextAlignmentRight;
    }];
    self.loanPurposeLab = loanPurposeLab;
    
    UIButton * loanPurposeBtn = [UIButton yk_buttonWithImageName:@"system_right_arrrow" superView:self masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        make.centerY.equalTo(loanPurposeTitle.mas_centerY);
        make.right.offset(-15);
        make.width.offset(60);
        make.height.offset(30);
        
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [button addTarget:self action:@selector(loanPurposeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    //分期数
    YKLoanPeriodsView * loanPeriodsView = [[YKLoanPeriodsView alloc] initWithTitle:@"分期数"];
    [self addSubview:loanPeriodsView];
    [loanPeriodsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(loanPurposeBtn.mas_bottom).offset(20);
    }];
    self.loanPeriodsView = loanPeriodsView;
    
    //极速申请按钮
    self.applyBtn = [UIButton yk_buttonFontSize:adaptFontSize(18) textColor:Color.whiteColor backGroundImage:@"btn_bg_image" imageName:@"" cornerRadius:29.0f superView:self masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        
        make.left.offset(34);
        make.right.offset(-34);
        make.top.equalTo(loanPeriodsView.mas_bottom).offset(20);
        make.height.offset(58);
        make.bottom.offset(-26);
        [button setTitle:@"立即申请" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(commitBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }];
}

#pragma mark 数据刷UI
- (void)setPageEntity:(YKLoanEntity *)pageEntity{
    _pageEntity = pageEntity;
    //滑竿
    double miniNum = [pageEntity.amounts_min doubleValue];
    double maxNum = [pageEntity.amounts_max doubleValue];
    self.sliderView.minimumValue = 0;
    self.sliderView.maximumValue = (maxNum - miniNum) / ([pageEntity.amounts_step doubleValue]);
    if (maxNum == miniNum) {
        self.sliderView.maximumValue = 10;
    }
    self.sliderMinValueLabel.text = pageEntity.amounts_min;
    self.sliderMaxValueLabel.text = pageEntity.amounts_max;
    self.sliderView.value = self.sliderView.maximumValue;
    self.borrowMoneyLabel.text = pageEntity.amounts_max;
    //立即分期
    [self.applyBtn setTitle:([pageEntity.button_text yk_isValidString] ? pageEntity.button_text : @"立即申请") forState:UIControlStateNormal];
    //分期数
    [self.loanPeriodsView renderViewsWithEntitys:pageEntity.period_num];
    self.loanPurposeLab.text = @"请选择";
    self.loanPurposeLab.textColor = Color.color_CC_C8;
}

#pragma mark 滑轮滑动响应
- (void)sliderMove:(UISlider *)slider{
    NSInteger value = round(slider.value);
    double money = value * ([self.pageEntity.amounts_step doubleValue]) + [self.pageEntity.amounts_min doubleValue];
    if ([self.pageEntity.amounts_min doubleValue] == [self.pageEntity.amounts_max doubleValue]) {
        money = [self.pageEntity.amounts_min doubleValue];
    }
    self.borrowMoneyLabel.text = [NSString stringWithFormat:@"%.0lf",(double)money];
}

#pragma mark -- 借款用途
- (void)loanPurposeBtnClick:(UIButton *)sender {
    if (self.loanPurposeBlock) {
        self.loanPurposeBlock();
    }
}

#pragma mark 快速申请点击
- (void)commitBtnClick:(UIButton *)button{
    if (self.loanApplyBlock) {
        self.loanApplyBlock();
    }
}

@end

