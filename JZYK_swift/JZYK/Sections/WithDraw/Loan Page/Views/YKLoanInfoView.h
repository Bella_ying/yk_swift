//
//  YKLoanInfoView.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKLoanEntity.h"
#import "YKLoanPeriodsView.h"

typedef void (^ykLoanApplyBlock)(void);
typedef void (^ykLoanPurposeBlock)(void);

@interface YKLoanInfoView : UIView

@property (nonatomic, copy) ykLoanApplyBlock loanApplyBlock;
@property (nonatomic, copy) ykLoanPurposeBlock loanPurposeBlock;
@property (nonatomic, strong) UILabel *borrowMoneyLabel;//借款金额
@property (nonatomic, strong) YKLoanPeriodsView * loanPeriodsView;//借款期限
@property (nonatomic, strong) UILabel *loanPurposeLab;//借款用途
@property (nonatomic, strong) UIButton *applyBtn;

@property (nonatomic, strong) YKLoanEntity *pageEntity;

@end
