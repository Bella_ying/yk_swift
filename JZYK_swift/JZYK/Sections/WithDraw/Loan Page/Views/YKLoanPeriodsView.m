//
//  YKLoanPeriodsView.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKLoanPeriodsView.h"
#import "YKLoanEntity.h"

#define itemHeadOffSet 20
#define itemTrailOffSet 20
#define itemTopOffSet 11 * ASPECT_RATIO_WIDTH
#define itemHeight 34 * ASPECT_RATIO_WIDTH

@interface YKLoanPeriodsView ()

@property (nonatomic, assign) CGFloat itemWidth;
@property (nonatomic, assign) CGFloat itemMidSpace;
@property (nonatomic, assign) NSInteger numsLine;

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIButton *lastSelectBtn;
@property (nonatomic, strong) NSMutableArray *itemBtnArr;
@property (nonatomic, strong) NSArray *dataArr;

@end

@implementation YKLoanPeriodsView

-(instancetype)initWithTitle:(NSString *)title {
    
    if (self = [super init]) {
        
        self.numsLine = 4;
        self.itemWidth = 74 * ASPECT_RATIO_WIDTH;
        self.itemMidSpace = (WIDTH_OF_SCREEN - itemHeadOffSet - itemTrailOffSet - self.itemWidth * self.numsLine)/(self.numsLine - 1);
        
        self.itemBtnArr = [NSMutableArray array];
        
        [self configUIWithTitle:title];
    }
    return self;
}

-(void)configUIWithTitle:(NSString *)title
{
    UILabel * titleLab = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_66_C3 superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.left.offset(25);
        make.top.offset(0);
        
        label.text = title;
    }];
    self.titleLab = titleLab;
}

- (void)renderViewsWithEntitys:(NSArray *)dataArr;
{
    if (!dataArr || dataArr.count == 0) return;
    self.dataArr = dataArr;
    [self updateSelfHeightConstraintWithItemsCount:dataArr.count];

    //重用rightItemBtn
    unsigned long miniCount = MIN(self.itemBtnArr.count, dataArr.count);
    unsigned long maxCount = MAX(self.itemBtnArr.count, dataArr.count);
    for (NSUInteger j = 0; j < miniCount; j++ ) {
        UIButton * itemBtn = self.itemBtnArr[j];
        itemBtn.hidden = NO;
        
        YKPeriodNumModel * entityModel = dataArr[j];
        [self renderItem:itemBtn withItemEntity:entityModel];
    }

    if (self.itemBtnArr.count >= dataArr.count) {
        for (NSUInteger j = miniCount; j < maxCount; j++ ){
            UIButton * itemBtn = self.itemBtnArr[j];
            itemBtn.hidden = YES;
        }
        return;
    }

    //创建rightItemBtn
    for (NSUInteger i = self.itemBtnArr.count; i < dataArr.count; i++) {

        UIButton * itemBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        itemBtn.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
        [itemBtn setTitleColor:Color.color_66_C3 forState:UIControlStateNormal];
        [itemBtn setBackgroundColor:Color.whiteColor];
        itemBtn.layer.cornerRadius = 3.0;
        itemBtn.layer.borderWidth = 1.0;
        itemBtn.layer.borderColor = Color.color_E8_C6.CGColor;
        itemBtn.selected = NO;
        itemBtn.tag = 1000 + i;
        [itemBtn addTarget:self action:@selector(itemBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.itemBtnArr addObject:itemBtn];
        [self addSubview:itemBtn];
        [itemBtn mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.offset(itemHeadOffSet + (self.itemWidth + self.itemMidSpace) * (i % self.numsLine));
            make.top.equalTo(self.titleLab.mas_bottom).offset(itemTopOffSet + (i / self.numsLine) * (itemHeight + self.itemMidSpace));
            make.width.offset(self.itemWidth);
            make.height.offset(itemHeight);
        }];
        YKPeriodNumModel * entityModel = dataArr[i];
        [self renderItem:itemBtn withItemEntity:entityModel];
    }
}

#pragma mark -- 渲染item数据
- (void)renderItem:(UIButton *)itemBtn withItemEntity:(YKPeriodNumModel *)entityModel
{
    [itemBtn setTitle:entityModel.pv forState:UIControlStateNormal];
    if (itemBtn.tag == 1000) {
        itemBtn.selected = YES;
        itemBtn.backgroundColor = Color.main;
        itemBtn.layer.borderColor = Color.main.CGColor;
        [itemBtn setTitleColor:Color.whiteColor forState:UIControlStateNormal];
        self.lastSelectBtn = itemBtn;
        self.selectValue = entityModel.pk;
    }else{
        itemBtn.selected = NO;
        itemBtn.backgroundColor = WHITE_COLOR;
        itemBtn.layer.borderColor = Color.color_E8_C6.CGColor;
        [itemBtn setTitleColor:Color.color_66_C3 forState:UIControlStateNormal];
    }
}

#pragma mark -- 跟新self的高度
- (void)updateSelfHeightConstraintWithItemsCount:(NSInteger )count
{
    CGFloat lines = (int)ceilf(count/(self.numsLine * 1.0));
    CGFloat totalH = 35 * ASPECT_RATIO_WIDTH + (lines * itemHeight + (lines - 1) * self.itemMidSpace);
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(totalH);
    }];
}

- (void)itemBtnClick:(UIButton *)sender
{
    if (self.lastSelectBtn != sender) {
        self.lastSelectBtn.selected = NO;
        self.lastSelectBtn.backgroundColor = Color.whiteColor;
        self.lastSelectBtn.layer.borderColor = Color.color_E8_C6.CGColor;
        [self.lastSelectBtn setTitleColor:Color.color_66_C3 forState:UIControlStateNormal];
        
        sender.selected = YES;
        sender.backgroundColor = Color.main;
        sender.layer.borderColor = Color.main.CGColor;
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        YKPeriodNumModel * entityModel = self.dataArr[sender.tag - 1000];
        self.selectValue = entityModel.pk;
        self.lastSelectBtn = sender;
    }
}


@end
