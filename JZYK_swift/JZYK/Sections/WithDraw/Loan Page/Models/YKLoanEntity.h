//
//  YKLoanEntity.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKLoanBannerModel : NSObject

@property (nonatomic, copy) NSString *active_url;
@property (nonatomic, copy) NSString *color;
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *data;
@property (nonatomic, copy) NSString *bannerId;
@property (nonatomic, copy) NSString *img_url;
@property (nonatomic, copy) NSString *order;
@property (nonatomic, copy) NSString *position;
@property (nonatomic, copy) NSString *skip_code;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *updated_at;

@end


@interface YKLoanPurposeModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *value;

@end

@interface YKPeriodNumModel : NSObject

@property (nonatomic, copy) NSString *pk;//12
@property (nonatomic, copy) NSString *pv;//12期

@end

@interface YKNoticeModel : NSObject

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *noticeID;

@end

@interface YKLoanEntity : NSObject

@property (nonatomic, copy) NSString *amounts_max;
@property (nonatomic, copy) NSString *amounts_min;
@property (nonatomic, copy) NSString *amounts_step;
@property (nonatomic, copy) NSString *bottom_notice;
@property (nonatomic, copy) NSString *button_text;
@property (nonatomic ,strong) NSArray<YKLoanBannerModel *> *banner; //底部活动位
@property (nonatomic ,strong) NSArray<YKLoanPurposeModel *> *loan_use_list;//借款用途
@property (nonatomic, copy) NSString *loan_use_title;//"请选择实际资金用途,禁止用于非消费场景"
@property (nonatomic ,strong) NSArray<YKPeriodNumModel *> *period_num;//借款期限
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *title_url;
@property (nonatomic, strong) YKNoticeModel *top_msg; //公告
@property (nonatomic, copy) NSString *transition;  //1:打开借款详情弹框 0:不打开
@property (nonatomic, copy) NSString *transition_kd_logo;
@property (nonatomic, copy) NSString *transition_tips; //"借款资金由口袋理财提供"
@property (nonatomic, copy) NSString *transition_yk_logo;
@property (nonatomic, copy) NSString *unused_amount;
@property (nonatomic, copy) NSString *credit_status; //0:正常 1:未授信 2:黑名单 3:有进行中的订单 4:认证过期
@property (nonatomic, copy) NSString *credit_msg; //"未完成授信"
@property (nonatomic, copy) NSString *jhjj_url;//速贷超人链接

@end
