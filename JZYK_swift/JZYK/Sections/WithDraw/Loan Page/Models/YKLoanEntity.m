//
//  YKLoanEntity.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKLoanEntity.h"

@implementation YKLoanBannerModel

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{
             @"bannerId":@"id",
             };
}
@end

@implementation YKLoanPurposeModel
@end

@implementation YKPeriodNumModel
@end

@implementation YKNoticeModel
+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{
             @"noticeID":@"id",
             };
}
@end

@implementation YKLoanEntity

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"banner":          [YKLoanBannerModel class],
             @"loan_use_list":   [YKLoanPurposeModel class],
             @"period_num":      [YKPeriodNumModel class],
             };
}
@end
