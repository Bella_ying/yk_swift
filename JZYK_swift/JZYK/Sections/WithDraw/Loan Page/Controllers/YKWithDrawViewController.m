//
//  YKWithDrawViewController.m
//  JZYK
//
//  Created by hongyu on 2018/6/1.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKWithDrawViewController.h"
#import "YKPaoMaNoticeView.h"
#import "YKLoanInfoView.h"
#import "SDCycleScrollView.h"
#import "YKLoanApplyViewController.h"
#import "SDCycleScrollView.h"
#import "YKLoanPurposeAlert.h"
#import "YKLoanEntity.h"
#import "YKVerifyGuideViewController.h"
#import "YKVerifyCenterListVC.h"
#import "YKAccessAlertView.h"
#import "YKLocationManager.h"

@interface YKWithDrawViewController ()<SDCycleScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *bgScrollV;
@property (nonatomic, strong) YKPaoMaNoticeView *noticeView;
@property (nonatomic, strong) YKLoanInfoView *loanInfoView;
@property (nonatomic, strong) SDCycleScrollView *bannerView;
@property (nonatomic, strong) YKLoanEntity *loanEntity;
@property (nonatomic, strong) YKLoanPurposeAlert *loanPurposeAlert;
@property (nonatomic, strong) YKLoanPurposeModel *selectLoanPurpose;
//@property (nonatomic, copy) void (^clickApplyFeatchFinishBlock)(void);

@end

@implementation YKWithDrawViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"提现";
    [self configUI];
    [self featchData:NO];
    WEAK_SELF
    [[YKUserManager sharedUser] setUserLoginSuccess_blk:^{
        [weakSelf featchData:NO]; //登录成功与否对借款金额影响较大，所以拉下最新数据
    }];
}

- (void)configUI {
    [self.view addSubview:self.bgScrollV];
    ADJUST_SCROLL_VIEW_INSET(self, self.bgScrollV)
    self.bgScrollV.mj_header = [YKRefreshHeader headerWithRefreshingBlock:^{
        [self featchData:NO];
    }];
    [self.bgScrollV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(1);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.mas_bottomLayoutGuideTop);
    }];
    
    [self.bgScrollV addSubview:self.noticeView];
    [self.noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.offset(0);
        make.height.offset(30);
    }];
    
    [self.bgScrollV addSubview:self.loanInfoView];
    [self.loanInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(self.noticeView.mas_bottom);
    }];
    
    [self.bgScrollV addSubview:self.bannerView];
    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(self.loanInfoView.mas_bottom).offset(10);
        make.height.offset(75 * ASPECT_RATIO_WIDTH);
        make.bottom.offset(-10);
        make.width.equalTo(self.bgScrollV.mas_width);
    }];
}

- (void)updateUI {
    
    self.navigationItem.title = self.loanEntity.title.length > 0 ? self.loanEntity.title : @"提现";
    //公告
    [self.noticeView refreshNoticeWithData:self.loanEntity.top_msg complete:nil];
    
    //loanInfo
    self.loanInfoView.pageEntity = self.loanEntity;
    self.loanPurposeAlert.lastSelectModel = nil; //想用户选择不复原，直接去掉这两句 和 loanInfoView的@“请选择”
    self.selectLoanPurpose = nil;
    
    //banner
    if ([self.loanEntity.banner isKindOfClass:[NSArray class]] && [self.loanEntity.banner count] > 0) {
        NSMutableArray * bannerTempArr = [NSMutableArray array];
        for (YKLoanBannerModel * bannerModel in self.loanEntity.banner) {
            [bannerTempArr addObject:bannerModel.img_url];
        }
        self.bannerView.imageURLStringsGroup = bannerTempArr;
        self.bannerView.hidden = NO;
        [self.bannerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.offset(75 * ASPECT_RATIO_WIDTH);
        }];
    } else {
        self.bannerView.hidden = YES;
        [self.bannerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.offset(0);
        }];
    }
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    YKLoanBannerModel * bannerModel = self.loanEntity.banner[index];
    JumpModel *model = [JumpModel new];
    model.skip_code = bannerModel.skip_code;
    model.url = bannerModel.active_url;
    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:model];
}

#pragma mark -- 拉取数据
- (void)featchData:(BOOL)isClickApply {
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kCreditAppLoanIndex showLoading:isClickApply param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        STRONG_SELF
        [strongSelf.bgScrollV.mj_header endRefreshing];
        strongSelf.loanInfoView.applyBtn.userInteractionEnabled = YES;
        if (code == 0 && success) {
            YKLoanEntity * loanEntity = [YKLoanEntity yy_modelWithJSON:json];
            if (!isClickApply) {
                strongSelf.loanEntity = loanEntity;
                [strongSelf updateUI];
            } else {
                NSString * lastCreditStatus = strongSelf.loanEntity.credit_status;
                strongSelf.loanEntity = loanEntity;
                if (lastCreditStatus != loanEntity.credit_status) {
                    [strongSelf updateUI];
                }
                [strongSelf judgeApplyStatusCommitOrder];
            }
            
        } else if(code != 0) {
            [[iToast makeText:msg] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        strongSelf.loanInfoView.applyBtn.userInteractionEnabled = YES;
        [strongSelf.bgScrollV.mj_header endRefreshing];
        [[iToast makeText:errMsg] show];
    }];
}

#pragma mark -- 立即申请
- (void)applyConfirmRequest {
    if (![[YKUserManager sharedUser] yk_isLogin]){
        [self logIn];
        return;
    }
    self.loanInfoView.applyBtn.userInteractionEnabled = NO;
    [self featchData:YES];
}

#pragma mark -- 立即申请点击后判断状态
- (void)judgeApplyStatusCommitOrder {
    NSString * loanMoney = self.loanInfoView.borrowMoneyLabel.text;
    NSString * loanPeriod = self.loanInfoView.loanPeriodsView.selectValue;
    if ([self.loanEntity.credit_status intValue] == 1) { // 未授信
        [self.navigationController pushViewController:[YKVerifyGuideViewController new] animated:YES];
        return;
    }
    if ([self.loanEntity.credit_status intValue] == 2) { // 黑名单
        [[QLAlert alert] showWithMessage:self.loanEntity.credit_msg btnTitleArray:@[@"去看看",@"取消",] btnClicked:^(NSInteger index) {
            if (index == 0) {
                YKBrowseWebController * webVC = [YKBrowseWebController new];
                webVC.url = self.loanEntity.jhjj_url;
                [self.navigationController pushViewController:webVC animated:YES];
            }
        }];
        return;
    }
    if ([self.loanEntity.credit_status intValue] == 3 || [self.loanEntity.credit_status intValue] == 7) { //3有进行中的订单  7审核被拒,请$天后再申请
        [[iToast makeText:self.loanEntity.credit_msg] show];
        return;
    }
    if ([self.loanEntity.credit_status intValue] == 4) { // 认证过期
        [[iToast makeText:self.loanEntity.credit_msg] show];
        [self.navigationController pushViewController:[YKVerifyCenterListVC new] animated:YES];
        return;
    }
    
    if ([self.loanEntity.credit_status intValue] == 5) { // 授信中
        [[iToast makeText:self.loanEntity.credit_msg] show];
        [self.navigationController pushViewController:[YKVerifyCenterListVC new] animated:YES];
        return;
    }
    if ([self.loanEntity.credit_status intValue] == 6) { // 登录失效
        [[YKUserManager sharedUser] yk_logoutAndDeleteUserInfo];
        [self logIn];
        return;
    }
    if ([self.loanEntity.credit_status intValue] != 0) { //扩展用，其他未知的错误；
        [[iToast makeText:self.loanEntity.credit_msg] show];
        return;
    }
    if ([loanMoney doubleValue] > [self.loanEntity.unused_amount doubleValue]) {
        [[iToast makeText:@"可用额度不足"] show];
        return;
    }
    
    if (!self.selectLoanPurpose.value) {
        [self showLoanPurposeAlert];
        return;
    }
    if ([YKTools yk_permissionsWithLocation] == YKPermissionStatusNotDenied) {
        [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeLocation contentText:@""];
        return;
    }
    if ([YKTools yk_permissionsWithLocation] == YKPermissionStatusAuthorized) {
        [[YKLocationManager location] yk_enableLocationServiceWithCompletion:nil];
    }
    
    YKLoanApplyViewController * vc = [YKLoanApplyViewController new];
    vc.loanMoney = loanMoney;
    vc.period = loanPeriod;
    vc.loan_use = self.selectLoanPurpose.value;
    vc.isShowMoneyComingAlert = [self.loanEntity.transition intValue] == 1 ? YES : NO;
    vc.transition_kd_logo = self.loanEntity.transition_kd_logo;
    vc.transition_tips = self.loanEntity.transition_tips;
    vc.transition_yk_logo = self.loanEntity.transition_yk_logo;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark -- 展示进款用途弹框
- (void)showLoanPurposeAlert {
    self.loanPurposeAlert.updateAlertView(self.loanEntity.loan_use_list,self.loanEntity.loan_use_title).showAlert();
}

- (YKLoanPurposeAlert *)loanPurposeAlert {
    if (!_loanPurposeAlert) {
        _loanPurposeAlert = [YKLoanPurposeAlert new];
        WEAK_SELF
        _loanPurposeAlert.selectLoanPurpose = ^(YKLoanPurposeModel * selectModel) {
            STRONG_SELF
            strongSelf.loanInfoView.loanPurposeLab.text = selectModel.name;
            strongSelf.loanInfoView.loanPurposeLab.textColor = Color.color_45_C2;
            strongSelf.selectLoanPurpose = selectModel;
        };
    }
    return _loanPurposeAlert;
}

- (SDCycleScrollView *)bannerView
{
    if (!_bannerView) {
        _bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:[UIImage imageNamed:@"quxian_banner_zw"]];
        _bannerView.backgroundColor     = Color.backgroundColor;
        _bannerView.pageControlBottomOffset = -5;
        _bannerView.currentPageDotImage = [UIImage imageNamed:@"home_banner_select"];
        _bannerView.pageDotImage        = [UIImage imageNamed:@"home_banner_noSelect"];
        _bannerView.hidden = YES;
    }
    return _bannerView;
}

- (UIScrollView *)bgScrollV {
    if (!_bgScrollV) {
        _bgScrollV = [UIScrollView new];
        _bgScrollV.backgroundColor = Color.backgroundColor;
        _bgScrollV.showsVerticalScrollIndicator = NO;
    }
    return _bgScrollV;
}

- (YKPaoMaNoticeView *)noticeView {
    if (!_noticeView) {
        _noticeView = [[YKPaoMaNoticeView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 30) pageType:PageTypeLoanCash];
        _noticeView.backgroundColor = Color.whiteColor;
    }
    return _noticeView;
}

- (YKLoanInfoView *)loanInfoView {
    if (!_loanInfoView) {
        WEAK_SELF
        _loanInfoView = [YKLoanInfoView new];
        _loanInfoView.loanApplyBlock = ^() {
            STRONG_SELF
            [strongSelf applyConfirmRequest];
        };
        _loanInfoView.loanPurposeBlock = ^{
            STRONG_SELF
            [strongSelf showLoanPurposeAlert];
        };
    }
    return _loanInfoView;
}

@end
