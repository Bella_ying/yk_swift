//
//  YKMoneyOriginAlertView.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMoneyOriginAlertView.h"
#import "UIImageView+WebCache.h"
#import "UIImage+GIF.h"

@interface YKMoneyOriginAlertView ()

@property (nonatomic, strong) UIView      *contentV;
@property (nonatomic, strong) UIImageView *fromImgV;
@property (nonatomic, strong) UIImageView *toImgV;
@property (nonatomic, strong) UIImageView *arrowImgV;
@property (nonatomic, strong) UILabel *tipLab;

@property (nonatomic, copy) NSString * detailStr;
@property (nonatomic, copy) NSString * fromImgURL;
@property (nonatomic, copy) NSString * toImgURL;
@property (nonatomic, assign) NSTimeInterval minTime;
@property (nonatomic, strong) NSDate * showStart;
@property (nonatomic, copy) AlertComplicationBlock alertCompletion;

@end

@implementation YKMoneyOriginAlertView

+ (YKMoneyOriginAlertView *_Nonnull(^_Nonnull)(UIView * _Nonnull superView, NSString * _Nonnull detailStr,NSString * _Nonnull fromImgURL, NSString * _Nonnull toImgURL))makeAlertViewWithCompletion:(AlertComplicationBlock _Nullable )alertCompletion
{
    return ^(UIView * superView, NSString * detailStr, NSString * fromImgURL, NSString * toImgURL){
        YKMoneyOriginAlertView * alertV = [YKMoneyOriginAlertView new];
        alertV.detailStr = detailStr;
        alertV.toImgURL = toImgURL;
        alertV.fromImgURL = fromImgURL;
        alertV.alertCompletion = alertCompletion;
        [superView addSubview:alertV];
        [alertV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.right.offset(0);
        }];
        [alertV updateUI];
        return alertV;
    };
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self configSubView];
    }
    return self;
}

- (void)updateUI
{
    self.tipLab.text = self.detailStr;
    [self.fromImgV sd_setImageWithURL:[NSURL URLWithString:self.fromImgURL] placeholderImage:nil];
    [self.toImgV sd_setImageWithURL:[NSURL URLWithString:self.toImgURL] placeholderImage:[UIImage imageNamed:@"moneyOriginAlertTo"]];
}

- (void)configSubView
{
    [self addSubview:self.contentV];
    [self.contentV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(46 * ASPECT_RATIO_WIDTH);
        make.right.offset(-46 * ASPECT_RATIO_WIDTH);
        make.centerY.offset(0);
        make.height.mas_equalTo(219);
    }];
    
    [self.contentV addSubview:self.fromImgV];
    [self.fromImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(43 * ASPECT_RATIO_WIDTH);
        make.top.offset(39 * ASPECT_RATIO_WIDTH);
        make.width.mas_equalTo(67 * ASPECT_RATIO_WIDTH);
        make.height.mas_equalTo(89 * ASPECT_RATIO_WIDTH);
    }];
    
    [self.contentV addSubview:self.toImgV];
    [self.toImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-43 * ASPECT_RATIO_WIDTH);
        make.top.offset(39 * ASPECT_RATIO_WIDTH);
        make.width.mas_equalTo(67 * ASPECT_RATIO_WIDTH);
        make.height.mas_equalTo(89 * ASPECT_RATIO_WIDTH);
    }];
    
    [self.contentV addSubview:self.arrowImgV];
    [self.arrowImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentV.mas_centerX);
        make.top.offset(66 * ASPECT_RATIO_WIDTH);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(20);
    }];
    
    [self.contentV addSubview:self.tipLab];
    [self.tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentV.mas_centerX);
        make.bottom.offset(-29 * ASPECT_RATIO_WIDTH);
    }];
}

- (YKMoneyOriginAlertView *_Nonnull (^_Nonnull)(NSTimeInterval showMinTime))minShowTime
{
    return ^(NSTimeInterval showMinTime)
    {
        self.minTime = showMinTime;
        self.showStart = [NSDate date];
        
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
                             self.contentV.layer.opacity = 1.0f;
                             self.contentV.layer.transform = CATransform3DMakeScale(1, 1, 1);
                         }
                         completion:^(BOOL finished) {
                         }
         ];
        return self;
    };
}

- (void)showMinTime:(NSTimeInterval)minShowTime
{
    self.showStart = [NSDate date];
    self.minTime = minShowTime;
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
                         self.contentV.layer.opacity = 1.0f;
                         self.contentV.layer.transform = CATransform3DMakeScale(1, 1, 1);
                     }
                     completion:^(BOOL finished) {
                         
                     }
     ];
}

- (void)dismiss
{
    if (self.minTime > 0.0 && self.showStart) {
        NSTimeInterval interv = [[NSDate date] timeIntervalSinceDate:self.showStart];
        if (interv < self.minTime) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((self.minTime - interv) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self closeAlert];
            });
            return ;
        }
    }
    [self closeAlert];
}

- (void)closeAlert
{
    CATransform3D currentTransform = self.contentV.layer.transform;
    self.contentV.layer.opacity = 1.0f;
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.contentV.layer.transform = CATransform3DConcat(currentTransform, CATransform3DMakeScale(0.6f, 0.6f, 1.0));
                         self.contentV.layer.opacity = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         self.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
                         for (UIView *v in [self.contentV subviews]) {
                             [v removeFromSuperview];
                         }
                         for (UIView *v in [self subviews]) {
                             [v removeFromSuperview];
                         }
                         [self.contentV removeFromSuperview];
                         
                         if (self.alertCompletion) {
                             self.alertCompletion();
                         }
                         
                         self.detailStr = nil;
                         [self removeFromSuperview];
                     }
     ];
}

- (UIView *)contentV
{
    if (!_contentV) {
        _contentV = [UIView new];
        _contentV.backgroundColor = [UIColor whiteColor];
        _contentV.layer.cornerRadius = 5.f;
        _contentV.layer.shouldRasterize = YES;
        _contentV.layer.rasterizationScale = [[UIScreen mainScreen] scale];
        _contentV.layer.opacity = 0.5f;
        _contentV.layer.transform = CATransform3DMakeScale(1.3f, 1.3f, 1.0);
    }
    return _contentV;
}
- (UIImageView *)fromImgV
{
    if (!_fromImgV) {
        _fromImgV = [UIImageView new];
        _fromImgV.image = [UIImage imageNamed:@"moneyOriginAlertTo"];
    }
    return _fromImgV;
}

- (UIImageView *)toImgV
{
    if (!_toImgV) {
        _toImgV = [UIImageView new];
        _toImgV.image = [UIImage imageNamed:@"moneyOriginAlertTo"];
    }
    return _toImgV;
}

- (UIImageView *)arrowImgV
{
    if (!_arrowImgV) {
        
        _arrowImgV = [UIImageView new];
        NSString *path = [[NSBundle mainBundle] pathForResource:@"moneyOriginAlert" ofType:@"gif"];
        NSData *data = [NSData dataWithContentsOfFile:path];
        if (data) {
            _arrowImgV.image = [UIImage sd_animatedGIFWithData:data];
        }
    }
    return _arrowImgV;
}

- (UILabel *)tipLab
{
    if (!_tipLab) {
        _tipLab = [UILabel new];
        _tipLab.font = [UIFont systemFontOfSize:adaptFontSize(14)];
        _tipLab.textColor = LABEL_TEXT_COLOR;
        _tipLab.textAlignment = NSTextAlignmentCenter;
    }
    return _tipLab;
}

@end
