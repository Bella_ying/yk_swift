//
//  YKMoneyOriginAlertView.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^AlertComplicationBlock)(void);

@interface YKMoneyOriginAlertView : UIView

+ (YKMoneyOriginAlertView *_Nonnull(^_Nonnull)(UIView * _Nonnull superView, NSString * _Nonnull detailStr,NSString * _Nonnull fromImgURL, NSString * _Nonnull toImgURL))makeAlertViewWithCompletion:(AlertComplicationBlock _Nullable )alertComplication;

//最少展示时间
- (YKMoneyOriginAlertView *_Nonnull(^_Nonnull)(NSTimeInterval showMinTime))minShowTime;

//最少展示时间---同上
- (void)showMinTime:(NSTimeInterval)minShowTime;

- (void)dismiss;

@end
