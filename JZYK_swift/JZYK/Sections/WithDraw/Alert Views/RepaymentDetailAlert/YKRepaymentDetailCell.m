//
//  YKRepaymentDetailCell.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKRepaymentDetailCell.h"

@interface YKRepaymentDetailCell()
@property (weak, nonatomic) IBOutlet UILabel *perionNumLab;
@property (weak, nonatomic) IBOutlet UILabel *repaymentMoneyLab;
@property (weak, nonatomic) IBOutlet UILabel *bjLab;
@property (weak, nonatomic) IBOutlet UILabel *feeLab;

@end

@implementation YKRepaymentDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)yk_rederCellDataWithModel:(YKRepaymnetDetailModel *)model {
    
    self.perionNumLab.text = model.period;
    self.repaymentMoneyLab.text = model.per_payment;
    self.bjLab.text = model.bj;
    self.feeLab.text = model.lx;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
