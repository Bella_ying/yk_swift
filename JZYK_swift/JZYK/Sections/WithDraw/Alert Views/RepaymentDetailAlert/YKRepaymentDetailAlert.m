//
//  YKRepaymentDetailAlert.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKRepaymentDetailAlert.h"
#import "YKRepaymentDetailCell.h"
#import "YKRepaymentDetailHeardV.h"

@interface YKRepaymentDetailAlert()

@property (nonatomic, strong) UIView *alertBack;
@property (nonatomic, strong) NSArray  *dataArr;
@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) YKRepaymentDetailHeardV *heardV;

@end

@implementation YKRepaymentDetailAlert

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configUI];
    }
    return self;
}

- (void)configUI
{
    [self.alertBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.offset(0);
    }];
    
    //标题
    UILabel *  titleLab = [UILabel yk_labelWithFontSize:adaptFontSize(18) textColor:Color.color_45_C2 superView:self.alertBack masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.offset(0);
        make.height.offset(62 * ASPECT_RATIO_WIDTH);
        label.text = @"还款计划";
    }];
    
    //关闭
    [UIButton yk_buttonWithImageName:@"webview_close" superView:self.alertBack masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        make.top.offset(5 * ASPECT_RATIO_WIDTH);
        make.right.offset(-5 * ASPECT_RATIO_WIDTH);
        make.width.offset(33);
        make.height.offset(32);
        
        [button addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    //tabelV
    [self.alertBack addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.offset(0);
        make.top.equalTo(titleLab.mas_bottom);
        make.height.mas_equalTo(10);
    }];
    WEAK_SELF
    [self.tableV hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        [tableMaker.hd_sectionCount(1).hd_tableViewHeaderView(^() {
            return weakSelf.heardV;
        }) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            [sectionMaker.hd_dataArr(^() {
                return weakSelf.dataArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKRepaymentDetailCell))
                .hd_rowHeight(40)
                .hd_adapter(^(YKRepaymentDetailCell *cell, id data, NSIndexPath *indexPath) {
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell yk_rederCellDataWithModel:weakSelf.dataArr[indexPath.row]];
                });
            }];
        }];
    }];
    [self.heardV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WIDTH_OF_SCREEN);
        make.top.offset(0);
        make.height.mas_equalTo(40);
    }];

}
- (void)updateUI:(NSArray *)dataArr
{
    self.dataArr = dataArr;
    [self.tableV reloadData];
}

- (void)setDataArr:(NSArray *)dataArr {
    _dataArr = dataArr;
    [self.tableV mas_updateConstraints:^(MASConstraintMaker *make) { //改成只显示6条数据
//        make.height.mas_equalTo((dataArr.count + 1) * 40 < (HEIGHT_OF_SCREEN - NAV_HEIGHT - 62 * ASPECT_RATIO_WIDTH) ? (dataArr.count + 1) * 40 : (HEIGHT_OF_SCREEN - NAV_HEIGHT - 62 * ASPECT_RATIO_WIDTH));
        make.height.mas_equalTo((dataArr.count + 1) * 40 < 7 * 40 ? (dataArr.count + 1) * 40 : 7 * 40);
    }];
}

- (void)show
{
    if (!self.superview) {
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
        [[UIApplication sharedApplication].keyWindow layoutSubviews];
        [self layoutSubviews];
    }
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    self.alertBack.alpha = 0.0f;
    self.hidden = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
        self.alertBack.alpha = 1.0f;
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
        self.alertBack.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (UITableView *)tableV
{
    if (!_tableV) {
        _tableV= [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableV;
}

- (YKRepaymentDetailHeardV *)heardV
{
    if (!_heardV) {
        _heardV =[[[NSBundle mainBundle] loadNibNamed:@"YKRepaymentDetailHeardV" owner:nil options:nil] firstObject];
        _heardV.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 40);
    }
    return _heardV;
}

- (UIView *)alertBack
{
    if (!_alertBack) {
        _alertBack = [UIView new];
        _alertBack.backgroundColor = [UIColor whiteColor];
        [self addSubview:_alertBack];
    }
    return _alertBack;
}

@end
