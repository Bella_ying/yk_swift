//
//  YKRepaymentDetailCell.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKLoanInfoModel.h"

@interface YKRepaymentDetailCell : UITableViewCell

- (void)yk_rederCellDataWithModel:(YKRepaymnetDetailModel *)model;

@end
