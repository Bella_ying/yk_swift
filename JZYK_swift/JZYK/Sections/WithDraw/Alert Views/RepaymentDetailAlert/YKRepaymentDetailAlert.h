//
//  YKRepaymentDetailAlert.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKRepaymentDetailAlert : UIView

- (void)updateUI:(NSArray *)dataArr;
- (void)show;

@end
