//
//  YKBorrowFeeAlertView.m
//  KDFDApp
//
//  Created by Kiran on 2018/6/21.
//  Copyright © 2018年 Kiran. All rights reserved.
//  总利息说明弹框

#import "YKBorrowFeeAlertView.h"
#import "YKLoanInfoModel.h"

@interface YKBorrowFeeAlertView ()

@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UILabel *interestAmountLabel;
@property (nonatomic, strong) UILabel *serviceAmountLabel;

@end

@implementation YKBorrowFeeAlertView

- (void)configUI
{
    [self.alertBackGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(37.5 * ASPECT_RATIO_WIDTH);
        make.right.offset(-37.5 * ASPECT_RATIO_WIDTH);
        make.centerY.offset(-10);
    }];
    
    UILabel * titleLabel = [UILabel yk_labelWithFontSize:adaptFontSize(17) textColor:Color.color_45_C2 superView:self.alertBackGroundView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.offset(25);
        label.text = @"服务费用";
    }];
    
    UIButton * closeBtn = [UIButton yk_buttonWithImageName:@"webview_close" superView:self.alertBackGroundView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        make.top.right.offset(0);
        make.width.mas_equalTo(43);
        make.height.mas_equalTo(42);
    }];
    [closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel * interestLab = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_66_C3 superView:self.alertBackGroundView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.left.offset(30);
        make.top.equalTo(titleLabel.mas_bottom).offset(13);
        label.text = @"借款利息";
    }];
    self.interestAmountLabel = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_66_C3 superView:self.alertBackGroundView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.centerY.equalTo(interestLab.mas_centerY);
        make.right.offset(-28);
    }];
    
    UILabel * serviceLab = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_66_C3 superView:self.alertBackGroundView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.left.offset(30);
        make.top.equalTo(interestLab.mas_bottom).offset(10);
        label.text = @"平台服务费";
    }];
    self.serviceAmountLabel = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_66_C3 superView:self.alertBackGroundView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.centerY.equalTo(serviceLab.mas_centerY);
        make.right.offset(-28);
    }];
    
    self.descLabel = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.main superView:self.alertBackGroundView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.left.offset(30);
        make.right.offset(-28);
        make.top.equalTo(serviceLab.mas_bottom).offset(12);
        make.bottom.offset(-36);
        label.numberOfLines = 0;
    }];
    
}

- (void)updateUI:(YKFeeModel *)model
{
    self.interestAmountLabel.text = [NSString stringWithFormat:@"%@元",model.interest];
    self.serviceAmountLabel.text = [NSString stringWithFormat:@"%@元",model.other_fee];
    self.descLabel.text = [NSString stringWithFormat:@"%@",model.desc];
}

@end
