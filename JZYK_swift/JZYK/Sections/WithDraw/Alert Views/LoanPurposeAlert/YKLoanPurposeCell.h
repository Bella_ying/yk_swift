//
//  YKLoanPurposeCell.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKLoanPurposeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end
