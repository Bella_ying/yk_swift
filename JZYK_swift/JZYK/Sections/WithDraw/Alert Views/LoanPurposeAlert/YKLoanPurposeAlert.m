//
//  YKLoanPurposeAlert.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKLoanPurposeAlert.h"
#import "YKLoanPurposeCell.h"

@interface YKLoanPurposeAlert()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, assign) CGFloat itemW;
@property (nonatomic, assign) CGFloat itemH;

@property (nonatomic, strong) UIView *alertBack;
@property (nonatomic, strong) UICollectionView  *collectionView;
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) NSArray  *dataArr;

@end

@implementation YKLoanPurposeAlert

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.itemW = (WIDTH_OF_SCREEN - 16 * 5)/4; //行、列间距都是16
        self.itemH =  34 * ASPECT_RATIO_WIDTH;

        [self configUI];
    }
    return self;
}

- (void)configUI
{
    [self.alertBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.offset(0);
    }];
    
    //标题
    UILabel * titleLab = [UILabel yk_labelWithFontSize:adaptFontSize(15) textColor:Color.color_45_C2 superView:self.alertBack masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.offset(29 * ASPECT_RATIO_WIDTH);
        
        label.text = @"请选择实际资金用途，禁止用于非消费场景";
        label.font = [UIFont boldSystemFontOfSize:adaptFontSize(15)];
    }];
    self.titleLab = titleLab;
    
    //关闭
    [UIButton yk_buttonWithImageName:@"webview_close" superView:self.alertBack masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        make.top.offset(3);
        make.right.offset(2);
        make.width.offset(33);
        make.height.offset(32);
        
        [button addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    //collectionV
    [self.alertBack addSubview:self.collectionView];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLab.mas_bottom);
        make.left.right.offset(0);
        make.height.mas_offset(220 * ASPECT_RATIO_WIDTH);
        make.bottom.offset(0);
    }];
}

- (YKLoanPurposeAlert * (^)(NSArray * dataArr,NSString * title))updateAlertView {
    return ^(NSArray * dataArr,NSString * title){
        [self updateUI:dataArr alertTitle:title];
        return self;
    };
}

- (void)updateUI:(NSArray *)dataArr alertTitle:(NSString *)title
{
    self.dataArr = dataArr;
    [self.collectionView reloadData];
    self.titleLab.text = [title yk_isValidString] ? title : @"请选择实际资金用途，禁止用于非消费场景";
}

- (void(^)(void))showAlert {
    return ^{
        [self show];
    };
}

- (void)show
{
    if (!self.superview) {
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
        [[UIApplication sharedApplication].keyWindow layoutSubviews];
        [self layoutSubviews];
    }
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    self.alertBack.alpha = 0.0f;
    self.hidden = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
        self.alertBack.alpha = 1.0f;
    }];
}   

- (void)dismiss
{
    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
        self.alertBack.alpha = 0.0f;
    } completion:^(BOOL finished) {
//        [self removeFromSuperview];
        self.hidden = YES;
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YKLoanPurposeModel * model = self.dataArr[indexPath.row];
    YKLoanPurposeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YKLoanPurposeCell" forIndexPath:indexPath];
    cell.titleLab.text = model.name;
    if (model.value == self.lastSelectModel.value) {
        cell.titleLab.backgroundColor = Color.color_loanPorpose;
        cell.titleLab.layer.borderColor = Color.color_loanPorpose.CGColor;
        cell.titleLab.textColor = Color.whiteColor;
    } else {
        cell.titleLab.backgroundColor = Color.whiteColor;
        cell.titleLab.textColor = Color.color_66_C3;
        cell.titleLab.layer.borderColor = Color.color_E8_C6.CGColor;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    YKLoanPurposeModel * model = self.dataArr[indexPath.row];
    if (model.value != self.lastSelectModel.value) {
        if ([self.dataArr containsObject:self.lastSelectModel]) {
            YKLoanPurposeCell * lastCell = (YKLoanPurposeCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:[self.dataArr indexOfObject:self.lastSelectModel] inSection:0]];
            lastCell.titleLab.backgroundColor = Color.whiteColor;
            lastCell.titleLab.textColor = Color.color_66_C3;
            lastCell.titleLab.layer.borderColor = Color.color_E8_C6.CGColor;
        }
        
        YKLoanPurposeCell * selectCell = (YKLoanPurposeCell *)[collectionView cellForItemAtIndexPath:indexPath];
        selectCell.titleLab.backgroundColor = Color.color_loanPorpose;
        selectCell.titleLab.layer.borderColor = Color.color_loanPorpose.CGColor;
        selectCell.titleLab.textColor = Color.whiteColor;
        self.lastSelectModel = model;
    }
    [self dismiss];
    if (self.selectLoanPurpose) {
        self.selectLoanPurpose(model);
    }
}

- (void)setDataArr:(NSArray *)dataArr {
    
    _dataArr = dataArr;
    [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_offset(44 * ASPECT_RATIO_WIDTH +
                               ceilf(dataArr.count/4.0) * (self.itemH + 16) +
                               (42 * ASPECT_RATIO_WIDTH - 16)
                               );
    }];}

//懒加载
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake(self.itemW, self.itemH);

        layout.minimumLineSpacing = 16;
        layout.minimumInteritemSpacing = 16;
        layout.sectionInset = UIEdgeInsetsMake(44 * ASPECT_RATIO_WIDTH, 15, 42 * ASPECT_RATIO_WIDTH, 15);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = WHITE_COLOR;
        [_collectionView registerNib:[UINib nibWithNibName:@"YKLoanPurposeCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"YKLoanPurposeCell"];
    }
    return _collectionView;
}

- (UIView *)alertBack
{
    if (!_alertBack) {
        _alertBack = [UIView new];
        _alertBack.backgroundColor = [UIColor whiteColor];
        [self addSubview:_alertBack];
    }
    return _alertBack;
}


@end
