//
//  YKLoanPurposeAlert.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKLoanEntity.h"

typedef void (^ykSelectLoanPurpose)(YKLoanPurposeModel * selectModel);

@interface YKLoanPurposeAlert : UIView

@property (nonatomic, copy) ykSelectLoanPurpose selectLoanPurpose;
@property (nonatomic, strong) YKLoanPurposeModel *lastSelectModel;

- (void)updateUI:(NSArray *)dataArr alertTitle:(NSString *)title;
- (void)show;

- (YKLoanPurposeAlert * (^)(NSArray * dataArr,NSString * title))updateAlertView;
- (void(^)(void))showAlert;


@end
