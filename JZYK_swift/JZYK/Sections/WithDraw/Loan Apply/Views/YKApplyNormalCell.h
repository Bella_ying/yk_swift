//
//  YKApplyNormalCell.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKLoanInfoModel.h"

typedef void(^YKtotalInterestClickBlock) (void);

@interface YKApplyNormalCell : UITableViewCell

@property (nonatomic, copy) YKtotalInterestClickBlock totalInterestBlock;

- (void)yk_rederCellDataWithModel:(YKListDataModel *)model;

@end
