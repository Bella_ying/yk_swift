//
//  YKFooterView.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKFooterView.h"
#import "YKProtocolView.h"

@interface YKFooterView()
@property (nonatomic, strong) UIButton *commitBtn;
@property (nonatomic, strong) YKProtocolView *protocolView;
@property (nonatomic, strong) UIImageView *logoImgV;
@property (nonatomic, strong) YKContentView *contentView;

@end

@implementation YKFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self configUI];
    }
    return self;
}

- (void)configUI {
    WEAK_SELF
    YKContentView * contentV = [YKContentView new];
    [self addSubview:contentV];
    [contentV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.offset(0);
    }];
    self.contentView = contentV;
    [self.contentView setXLayoutCallback:^(CGRect rect){
        STRONG_SELF
        strongSelf.XLayoutCallback(rect);
    }];
    
    UIButton *commitBtn = [UIButton yk_buttonFontSize:adaptFontSize(17) textColor:Color.whiteColor backGroundImage:@"btn_bg_image" imageName:@"" cornerRadius:29.0f superView:contentV masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        make.width.mas_equalTo(WIDTH_OF_SCREEN - 68);
        make.centerX.equalTo(self);
        make.top.offset(26);
        make.height.mas_equalTo(58);
        [button setTitle:@"立即分期" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(commitBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }];
    self.commitBtn = commitBtn;
    
    self.protocolView = YKProtocolView.makeProtocol(contentV, commitBtn);
    self.protocolView.SelectStatusDidChangeBlock = ^(BOOL isSelected) {
        STRONG_SELF
        if (!isSelected) {
            [strongSelf.commitBtn setBackgroundImage:[UIImage imageNamed:@"btn_disabled_bg_image"] forState:UIControlStateNormal];
            strongSelf.commitBtn.enabled = NO;
        } else {
            [strongSelf.commitBtn setBackgroundImage:[UIImage imageNamed:@"btn_bg_image"] forState:UIControlStateNormal];
            strongSelf.commitBtn.enabled = YES;
        }
    };

    self.logoImgV = [UIImageView yk_imageViewWithImageName:@"koudai_icon" superView:contentV masonrySet:^(UIImageView *imageView, MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.equalTo(self.protocolView.mas_bottom).offset(84);
        make.width.mas_equalTo(177 * ASPECT_RATIO_WIDTH);
        make.height.mas_equalTo(16 * ASPECT_RATIO_WIDTH);
        make.bottom.mas_equalTo(-20);
        imageView.contentMode = UIViewContentModeScaleToFill;
    }];
}

- (void)commitBtnClick:(UIButton *)sender {
    if (self.commitBlock) {
        self.commitBlock(sender);
    }
}

- (void)updateUIWithModel:(YKLoanInfoModel *)model {
    [self.logoImgV sd_setImageWithURL:[NSURL URLWithString:model.bottom_tip_logo] placeholderImage:[UIImage imageNamed:@"koudai_icon"]];
    [self.protocolView yk_refreshProtocolTextEntity:model.agreement protocolString:@"title" url:@"url"];
}

@end

@implementation YKContentView

-(void)layoutSubviews{
    [super layoutSubviews];
    if (self.XLayoutCallback) {
        self.XLayoutCallback(self.frame);
    }
}

@end
