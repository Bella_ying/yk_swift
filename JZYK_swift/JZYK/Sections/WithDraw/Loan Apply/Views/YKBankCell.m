//
//  YKBankCell.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBankCell.h"
#import "UIView+ViewController.h"

@interface YKBankCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bankBtnTrailConstaint;

@end

@implementation YKBankCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)yk_rederCellDataWithModel:(YKListDataModel *)model {
    
    self.titleLab.text = model.name;
    [self.bankBtn setTitle:model.value forState:UIControlStateNormal];
    
    if ([model.tag intValue] == 22) {
        self.addIconBtn.hidden = YES;
    } else {
        self.addIconBtn.hidden = NO;
    }
}

- (IBAction)seeDetailBtnClick:(UIButton *)sender {
    if (self.bankBtnBlock) {
        self.bankBtnBlock();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
