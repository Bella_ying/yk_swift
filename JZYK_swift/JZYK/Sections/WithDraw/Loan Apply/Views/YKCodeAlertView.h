//
//  YKCodeAlertView.h
//  JZYK
//
//  Created by wangguanjun on 2018/7/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, YKCodeAlertViewStyle) {
    YKCodeAlertViewStyleTransactionCode,
    YKCodeAlertViewStyleSwitchBankCardMessageCode,
};

typedef void (^TransactionCodeViewFinishBlock)(NSString *contentText);
typedef void (^CommitMessageCodeBlock)(NSString *contentText);
typedef void (^GetMessageCodeBlock)(NSString *contentText);
@interface YKCodeAlertView : UIView

- (instancetype)initWithAlertStyle:(YKCodeAlertViewStyle)alertStyle;

@property (nonatomic, strong, setter=yk_setBackgroundColor:) UIColor *yk_backgroundColor;
/**
 * 默认点击背景不消失 yk_isHiddenClickBackground : NO
 */
@property (nonatomic, assign, setter=yk_setIsHiddenClickBackground:) BOOL yk_isHiddenClickBackground;


//----------
@property (nonatomic, copy) TransactionCodeViewFinishBlock transactionCodeViewFinishBlock;
@property (nonatomic, copy) CommitMessageCodeBlock commitMessageCodeBlock;
@property (nonatomic, copy) GetMessageCodeBlock getMessageCodeBlock;

- (void)yk_setBankCardId:(NSString *)bank_id card_no:(NSString *)card_no;

@end
