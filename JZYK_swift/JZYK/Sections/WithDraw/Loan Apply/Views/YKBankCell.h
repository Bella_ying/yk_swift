//
//  YKBankCell.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKLoanInfoModel.h"

typedef void(^YKBankCellClickBlock) (void);

@interface YKBankCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *addIconBtn;
@property (weak, nonatomic) IBOutlet UIButton *bankBtn;
@property (nonatomic, copy) YKBankCellClickBlock bankBtnBlock;

- (void)yk_rederCellDataWithModel:(YKListDataModel *)model;

@end
