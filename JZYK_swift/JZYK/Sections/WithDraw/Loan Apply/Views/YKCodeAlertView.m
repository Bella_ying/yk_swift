//
//  YKCodeAlertView.m
//  JZYK
//
//  Created by wangguanjun on 2018/7/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKCodeAlertView.h"

#define kAlertWidth (300)
#define kAlertTopMargin (150)
@interface YKCodeAlertView ()<UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UIView *alertV;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *closeBtn;
@property (nonatomic, strong) UIView *titleSpaceLineView;
@property (nonatomic, strong) UITapGestureRecognizer *backgroundTagG;

@property (nonatomic, strong) UILabel *desLabel;
@property (nonatomic, strong) UIView *grayView;
@property (nonatomic, strong) UITextField *messageCodeTextField;
@property (nonatomic, strong) UIButton *messageCodeBtn;
@property (nonatomic, strong) UIView *secondLineView;
@property (nonatomic, strong) UIButton *commitMessageCodeBtn;

@property (nonatomic, strong) TransactionCodeView *codeView;

@property (nonatomic, copy) NSString *bank_id;
@property (nonatomic, copy) NSString *card_no;

@end

@implementation YKCodeAlertView

- (instancetype)initWithAlertStyle:(YKCodeAlertViewStyle)alertStyle
{
    if (self = [super init]) {
        [self setupWithStyle:alertStyle];
    }
    return self;
}

- (void)setupWithStyle:(YKCodeAlertViewStyle)alertStyle{
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    self.frame = CGRectMake(0, 0, screenW, screenH);
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [self addGestureRecognizer:self.backgroundTagG];
    self.backgroundTagG.enabled = NO;
    
    [self addSubview:self.alertV];
    [self.alertV addSubview:self.titleLabel];
    [self.alertV addSubview:self.closeBtn];
    [self.alertV addSubview:self.titleSpaceLineView];
    
    [self.alertV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(kAlertTopMargin * ASPECT_RATIO_WIDTH);
        make.width.mas_equalTo(kAlertWidth);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.alertV);
        make.top.mas_equalTo(12);
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.height.mas_equalTo(35);
    }];
    
    [self.titleSpaceLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(12);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(1.0/[UIScreen mainScreen].scale);
    }];
    
    switch (alertStyle) {
        case YKCodeAlertViewStyleTransactionCode:
        {
            [self prepareUIForTransactionCode];
        }
            break;
        case YKCodeAlertViewStyleSwitchBankCardMessageCode:
        {
            [self prepareUIForSwitchBankCardMessageCode];
            [self.messageCodeTextField becomeFirstResponder];
            self.titleLabel.text = @"安全认证";
            self.desLabel.text = @"为了您的账户安全，请填写收到的验证码";
        }
            break;
            
        default:
            break;
    }



}


- (void)prepareUIForTransactionCode{
//    [self.alertV addSubview:self.desLabel];
//    [self.alertV addSubview:self.codeView];
//    [self.alertV addSubview:self.messageCodeBtn];
//
//    [self.desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.titleSpaceLineView.mas_bottom).offset(15);
//        make.centerX.equalTo(self.alertV);
//        make.left.mas_greaterThanOrEqualTo(15);
//        make.right.mas_lessThanOrEqualTo(-15);
//    }];
//
//    [self.codeView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self.alertV);
//        make.top.equalTo(self.desLabel.mas_bottom).offset(20);
//        make.width.mas_equalTo(kAlertWidth - 30);
//        make.height.mas_equalTo(44);
//    }];
//
//    [self.messageCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.codeView.mas_bottom).offset(5);
//        make.right.mas_equalTo(- 35);
//        make.bottom.equalTo(self.alertV.mas_bottom).offset(-20);
//        make.width.mas_equalTo(95);
//        make.height.mas_equalTo(30);
//    }];
}

- (void)prepareUIForSwitchBankCardMessageCode{
    [self.alertV addSubview:self.desLabel];
    [self.alertV addSubview:self.grayView];
    [self.grayView addSubview:self.messageCodeTextField];
    [self.grayView addSubview:self.messageCodeBtn];
    [self.alertV addSubview:self.secondLineView];
    [self.alertV addSubview:self.commitMessageCodeBtn];
    
    [self.desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleSpaceLineView.mas_bottom).offset(20);
        make.centerX.equalTo(self.alertV);
        make.left.mas_greaterThanOrEqualTo(15);
        make.right.mas_lessThanOrEqualTo(-15);
    }];

    [self.grayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.desLabel.mas_bottom).offset(25);
        make.left.mas_equalTo(25);
        make.right.mas_equalTo(-25);
        make.height.mas_equalTo(40);
    }];
    
    [self.messageCodeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(5);
        make.centerY.equalTo(self.grayView);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(100);
    }];
    
    [self.messageCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-5);
        make.centerY.equalTo(self.grayView);
        make.width.mas_equalTo(95);
        make.height.mas_equalTo(30);
    }];
    
    [self.secondLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.grayView.mas_bottom).offset(30);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(1.0/[UIScreen mainScreen].scale);
    }];
    
    [self.commitMessageCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.secondLineView.mas_bottom);
        make.left.right.equalTo(self.alertV);
        make.bottom.equalTo(self.alertV.mas_bottom);
        make.height.mas_equalTo(60);
    }];

}

#pragma -mark - textFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
    NSMutableString *resultStr = [NSMutableString stringWithString:textField.text];
    [resultStr replaceCharactersInRange:range withString:string];
    if (textField == self.messageCodeTextField) {
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        if (resultStr.length <= 6) {
            return YES;
        }else{
            return NO;
        }
    }
    
    return YES;
}

#pragma -mark -- aciton
- (void)closeBtnAction{
    [self yk_hiddenAlertView];
}

- (void)clickSelfBackground{
    [self yk_hiddenAlertView];
}

- (void)yk_hiddenAlertView{
    [self removeFromSuperview];
}

- (void)yk_setBankCardId:(NSString *)bank_id card_no:(NSString *)card_no{
    self.bank_id = bank_id;
    self.card_no = card_no;
}

- (void)getMessageCodeAction:(UIButton *)btn{

    NSString *nameStr = [YKUserManager sharedUser].username;
    if (nameStr.length != 11) {
        return;
    }
    
    [CodeSecure md5EncryptWithPhoneNum:nameStr encryptedCallBack:^(NSString * _Nonnull sign, NSString * _Nonnull random) {
        NSDictionary * parm = @{@"type": @"set-main-card",
                                @"bank_id": self.bank_id,
                                @"card_no": self.card_no,
                                @"phone": nameStr,
                                @"sign": sign,
                                @"random": random};
        [CodeSecure bindCardCodeSendWithParam:parm sendComplete:^(BOOL isSuccess) {
            if (isSuccess) {
                [CodeButton timerCountDownForButton:btn elapsedTime:90 title:@"s" finish:^{
                }];
            }
        }];
    }];
}

- (void)commitMessageCodeAction{
    //点击确认
    DLog(@"点击确认");
    if (self.commitMessageCodeBlock) {
        self.commitMessageCodeBlock(self.messageCodeTextField.text);
    }
}

#pragma -mark -- tapDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if (touch.view == self.alertV){
        return NO;
    }
    return YES;
}

#pragma -mark - setter
- (void)yk_setIsHiddenClickBackground:(BOOL)yk_isHiddenClickBackground
{
    _yk_isHiddenClickBackground = yk_isHiddenClickBackground;
    if (yk_isHiddenClickBackground) {
        //点击背景可隐藏
        self.backgroundTagG.enabled = YES;
    }else{
        self.backgroundTagG.enabled = NO;
    }
}

- (void)yk_setBackgroundColor:(UIColor *)yk_backgroundColor
{
    _yk_backgroundColor = yk_backgroundColor;
    self.backgroundColor = yk_backgroundColor;
}

#pragma -mark -- getter
- (UIView *)alertV
{
    if (!_alertV) {
        _alertV = [[UIView alloc] init];
        _alertV.layer.cornerRadius = 3;
        _alertV.layer.masksToBounds = YES;
        _alertV.backgroundColor = WHITE_COLOR;
    }
    return _alertV;
}
- (UITapGestureRecognizer *)backgroundTagG
{
    if (!_backgroundTagG) {
        _backgroundTagG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSelfBackground)];
        self.backgroundTagG.delegate = self;
    }
    return _backgroundTagG;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = GRAY_COLOR_45;
        _titleLabel.font = [UIFont systemFontOfSize:15];
        
    }
    return _titleLabel;
}

- (UIButton *)closeBtn
{
    if (!_closeBtn) { //w:35 h:35
        _closeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_closeBtn setImage:[UIImage imageNamed:@"webview_close"] forState:(UIControlStateNormal)];
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _closeBtn;
}


- (UIView *)titleSpaceLineView
{
    if (!_titleSpaceLineView) {
        _titleSpaceLineView = [[UIView alloc] init];
        _titleSpaceLineView.backgroundColor = LINE_COLOR;
        
    }
    return _titleSpaceLineView;
}

- (UILabel *)desLabel
{
    if (!_desLabel) {
        _desLabel = [[UILabel alloc] init];
        _desLabel.textColor = LABEL_TEXT_COLOR;
        _desLabel.font = [UIFont systemFontOfSize:13];
        
    }
    return _desLabel;
}

- (UIButton *)messageCodeBtn
{
    if (!_messageCodeBtn) {
        _messageCodeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_messageCodeBtn setTitle:@"获取验证码" forState:(UIControlStateNormal)];
        [_messageCodeBtn setTitleColor:MAIN_THEME_COLOR forState:(UIControlStateNormal)];
        _messageCodeBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _messageCodeBtn.layer.borderWidth = 1;
        _messageCodeBtn.layer.borderColor = MAIN_THEME_COLOR.CGColor;
        _messageCodeBtn.layer.cornerRadius = 5;
        _messageCodeBtn.layer.masksToBounds = YES;
        [_messageCodeBtn addTarget:self action:@selector(getMessageCodeAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _messageCodeBtn;
}

- (TransactionCodeView *)codeView {
    if (!_codeView) {
        _codeView = [[TransactionCodeView alloc] init];
        _codeView.frame = CGRectMake(0, 0, 270, 44);
        WEAK_SELF
        _codeView.inputEndCallBack = ^(NSString * _Nonnull input) {
            DLog(@"输入的code:%@",input);
            if (weakSelf.transactionCodeViewFinishBlock) {
                weakSelf.transactionCodeViewFinishBlock(input);
            }
            [weakSelf yk_hiddenAlertView];
            
        };
    }
    return _codeView;
}

- (UIView *)grayView
{
    if (!_grayView) {
        _grayView = [[UIView alloc] init];
        _grayView.backgroundColor = [UIColor yk_colorWithHexString:@"#F5F5F7"];
        _grayView.layer.cornerRadius = 3;
        _grayView.layer.masksToBounds = YES;
    }
    return _grayView;
}

- (UITextField *)messageCodeTextField
{
    if (!_messageCodeTextField) {
        _messageCodeTextField = [[UITextField alloc] init];
        _messageCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
        _messageCodeTextField.borderStyle = UITextBorderStyleNone;
        _messageCodeTextField.tintColor = MAIN_THEME_COLOR;
        _messageCodeTextField.textColor = GRAY_COLOR_45;
        _messageCodeTextField.font = [UIFont systemFontOfSize:14];
        _messageCodeTextField.delegate = self;
    }
    return _messageCodeTextField;
}
- (UIView *)secondLineView
{
    if (!_secondLineView) {
        _secondLineView = [[UIView alloc] init];
        _secondLineView.backgroundColor = LINE_COLOR;
    }
    return _secondLineView;
}
- (UIButton *)commitMessageCodeBtn
{
    if (!_commitMessageCodeBtn) {
        _commitMessageCodeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_commitMessageCodeBtn setTitleColor:LABEL_TEXT_COLOR forState:(UIControlStateNormal)];
        _commitMessageCodeBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [_commitMessageCodeBtn setTitle:@"确认" forState:(UIControlStateNormal)];
        [_commitMessageCodeBtn addTarget:self action:@selector(commitMessageCodeAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _commitMessageCodeBtn;
}

@end
