//
//  YKApplyNormalCell.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKApplyNormalCell.h"

@interface YKApplyNormalCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLab;
@property (weak, nonatomic) IBOutlet UIButton *helpBtn;

@end

@implementation YKApplyNormalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)yk_rederCellDataWithModel:(YKListDataModel *)model
{
    if ([model.tag intValue] == 3) { //总利息
        self.helpBtn.hidden = NO;
    } else {
        self.helpBtn.hidden = YES;
    }
    self.titleLab.text = model.name;
    self.subTitleLab.text = model.value;
}

- (IBAction)helpBtnClick:(UIButton *)sender {
    if (self.totalInterestBlock) {
        self.totalInterestBlock();
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
