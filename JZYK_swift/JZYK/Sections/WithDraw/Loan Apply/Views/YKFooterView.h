//
//  YKFooterView.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKLoanInfoModel.h"

@interface YKContentView : UIView

@property (nonatomic ,copy) void(^XLayoutCallback)(CGRect rect);

@end


@interface YKFooterView : UIView

@property (nonatomic ,copy) void(^XLayoutCallback)(CGRect rect);
@property (nonatomic ,copy) void(^commitBlock)(UIButton *);

- (void)updateUIWithModel:(YKLoanInfoModel *)model;

@end

