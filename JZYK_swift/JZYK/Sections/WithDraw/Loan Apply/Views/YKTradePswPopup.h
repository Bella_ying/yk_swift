//
//  YKTradePswPopup.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/16.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKTradePswPopup : UIView

@property (nonatomic ,copy) void(^ _Nullable forgetPswBlock)(UIButton *_Nullable);

+ (YKTradePswPopup *_Nonnull(^_Nonnull)(NSString * _Nonnull orderTitle,NSString * _Nonnull orderNum))makeTradePswPopWithInputCompletion:(void (^_Nullable)(NSString * _Nonnull inputStr,UILabel*_Nullable errorLab))inputCompletion;

- (YKTradePswPopup *_Nonnull(^_Nonnull)(void))animateShow;

- (void)animateHide;

- (void)clearAllInput;//密码输入错误可能会用；

@end
