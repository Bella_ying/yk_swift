//
//  YKTradePswPopup.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/16.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKTradePswPopup.h"

@interface YKTradePswPopup()

@property (nonatomic, strong) UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *ordertTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLab;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UILabel *errorLab;
@property (weak, nonatomic) IBOutlet UIView *blackBgV;
@property (nonatomic, strong) TransactionCodeView *codeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTopConstraint;

@property (nonatomic ,copy) void(^ _Nullable inputCompletion)(NSString *,UILabel *_Nullable);

@end

@implementation YKTradePswPopup

+ (YKTradePswPopup *_Nonnull(^_Nonnull)(NSString * _Nonnull orderTitle,NSString * _Nonnull orderNum))makeTradePswPopWithInputCompletion:(void (^_Nullable)(NSString * _Nonnull inputStr,UILabel*_Nullable errorLab))inputCompletion {
    
    return ^(NSString * _Nonnull orderTitle,NSString * _Nonnull orderNum) {
        YKTradePswPopup * alertV = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
        alertV.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, HEIGHT_OF_SCREEN);
        alertV.ordertTitleLab.text = orderTitle;
        alertV.orderNumLab.text = orderNum;
        alertV.inputCompletion = inputCompletion;
        alertV.blackBgV.backgroundColor = [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:0.6];
        alertV.backgroundColor = [UIColor clearColor];
        [alertV.inputView addSubview:alertV.codeView];
        return alertV;
    };
}

- (void)awakeFromNib {
    [super awakeFromNib];
    if (IS_iPhone4 || IS_iPhone5) {
        self.contentViewTopConstraint.constant = -50;
    }
}

- (void)clearAllInput {
    [self.codeView clearAllInputContent];
}

- (IBAction)forgetBtnClick:(UIButton *)sender {
    
    [self animateHide];
    TransactionCodeFindController * vc = [TransactionCodeFindController new];
    vc.phoneText = [YKUserManager sharedUser].username;
    [[YKTabBarController yk_shareTabController] yk_pushToViewController:vc];
    
    if (self.forgetPswBlock) {
        self.forgetPswBlock(sender);
    }
}

- (IBAction)closeBtnClick:(UIButton *)sender {
    [self animateHide];
}

- (YKTradePswPopup *_Nonnull(^_Nonnull)(void))animateShow {
    WEAK_SELF
    return ^(void){
        STRONG_SELF
        [[[UIApplication sharedApplication] keyWindow] addSubview:self];
        [strongSelf.codeView.textField becomeFirstResponder];
        
        return self;
    };
}

- (void)animateHide {
    self.alpha = 0;;
    [self removeFromSuperview];
}

- (TransactionCodeView *)codeView {
    if (!_codeView) {
        WEAK_SELF
        _codeView = [[TransactionCodeView alloc] init];
        _codeView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN - 114, 44);
        _codeView.inputEndCallBack = ^(NSString * _Nonnull input) {
            STRONG_SELF
            if (strongSelf.inputCompletion) {
                strongSelf.inputCompletion(input,strongSelf.errorLab);
            }
        };
    }
    return _codeView;
}

@end
