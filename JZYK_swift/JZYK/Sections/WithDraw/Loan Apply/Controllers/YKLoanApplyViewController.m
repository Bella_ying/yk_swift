//
//  YKLoanApplyViewController.m
//  JZYK
//
//  Created by 吴春艳 on 2018/7/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKLoanApplyViewController.h"
#import "YKBorrowSuccessViewController.h"
#import "YKApplyNormalCell.h"
#import "YKBankCell.h"
#import "YKMoneyOriginAlertView.h"
#import "YKAddBankCardViewController.h"
#import "YKRepaymentDetailAlert.h"
#import "YKFooterView.h"
#import "YKTradePswPopup.h"
#import "YKBorrowFeeAlertView.h"
#import "YKLoanInfoModel.h"
#import "YKVerifyBindingCardModel.h"
#import "YKCunGunTools.h"

static NSString * const loanDetailCommonCell = @"loanDetailCommonCell";
static NSString * const bankCell = @"loanAddBankCell";

@interface YKLoanApplyViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) YKFooterView *footerVV;
@property (nonatomic, strong) YKMoneyOriginAlertView *moneyOrigiAlertV;
@property (nonatomic, strong) YKRepaymentDetailAlert *repaymentPlanAlert;
@property (nonatomic, strong) YKTradePswPopup * pswPop;
@property (nonatomic, strong) YKLoanInfoModel *pageModel;
@property (nonatomic, strong) YKVerifyBindingCardModel *cunguanModel;

@end

@implementation YKLoanApplyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"分期详情";
    [self configUI];
//    [self featchPageData];
    
    if (!self.isShowMoneyComingAlert) {
        return;
    }
    NSString * tips = self.transition_tips.length > 0 ? self.transition_tips : @"资金由口袋理财投资人提供";
    NSString * fromImgURL = self.transition_yk_logo;
    NSString * toImgURL = self.transition_kd_logo;
    WEAK_SELF
    self.moneyOrigiAlertV = [YKMoneyOriginAlertView makeAlertViewWithCompletion:^{
        STRONG_SELF
        strongSelf.tableV.hidden = NO;
    }](KEY_WINDOW,tips,fromImgURL,toImgURL).minShowTime(1);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self featchPageData];
}

- (void)configUI {
    [self.view addSubview:self.tableV];
    if (self.isShowMoneyComingAlert == YES) {
        self.tableV.hidden = YES;
    }
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.tableV.tableFooterView = self.footerVV;
    WEAK_SELF
    self.footerVV.XLayoutCallback = ^(CGRect rect) {
        STRONG_SELF
        strongSelf.footerVV.frame = CGRectMake(strongSelf.footerVV.x, strongSelf.footerVV.y, rect.size.width, rect.size.height);
        strongSelf.tableV.tableFooterView = strongSelf.footerVV;
    };
    self.footerVV.commitBlock = ^(UIButton *sender) {
        STRONG_SELF
        [strongSelf addBankOrCommitClickWithIsCommitOrder:YES];
    };
    
    [self.tableV registerNib:[UINib nibWithNibName:@"YKApplyNormalCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:loanDetailCommonCell];
    [self.tableV registerNib:[UINib nibWithNibName:@"YKBankCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:bankCell];
}

- (void)updatePageUI {
    [self.tableV reloadData];
    [self.footerVV updateUIWithModel:self.pageModel];
}

#pragma mark -- 拉取数据
- (void)featchPageData {
    
    NSDictionary * parm = @{@"money":self.loanMoney,
                            @"period":self.period,
                            @"loan_use":self.loan_use
                            };
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditLoanGetConfirmLoan_new showLoading:NO param:parm succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        STRONG_SELF
        if (!strongSelf.pageModel) {
            [strongSelf.moneyOrigiAlertV dismiss];
        }
        if (code == 0 && success) {
            strongSelf.pageModel = [YKLoanInfoModel yy_modelWithJSON:json[@"item"]];
            [strongSelf updatePageUI];
        } else if(code != 0){
            [[iToast makeText:msg] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        if (!strongSelf.pageModel) {
            [strongSelf.moneyOrigiAlertV dismiss];
        }
        [[iToast makeText:errMsg] show];
    }];
}

#pragma mark -- 添加银行卡 or 立即分期点击 判断存管状态
- (void)addBankOrCommitClickWithIsCommitOrder:(BOOL)isCommitOrder {
    [YKCunGunTools yk_featchCunGuanDataWithCunguanFinishBlock:^{
        if (isCommitOrder) {
            [self inputPayPswForCommitOrder];
        }
    } bindBankComplete:^(BOOL bankPageBlock,NSString * card_desc, NSString *card_id, NSString *bank_name) {
        if (bankPageBlock == NO) {
            [self featchPageData];
        }
    }];
}

#pragma mark -- 立即分期
- (void)inputPayPswForCommitOrder {
    if ([self.pageModel.real_pay_pwd_status intValue] == 1) {
        WEAK_SELF
        self.pswPop = [YKTradePswPopup makeTradePswPopWithInputCompletion:^(NSString * _Nonnull inputStr, UILabel * _Nullable errorLab) {
            STRONG_SELF
            [strongSelf commitOrderRequestWithPsw:inputStr tradePswErrorLab:errorLab];
        }](@"借款金额",self.loanMoney).animateShow();
    } else {
        //未设置交易密码
        TransactionCodeController * vc = [TransactionCodeController new];
        vc.typeNum = 1;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark -- 借款 和 交易密码校验 请求
- (void)commitOrderRequestWithPsw:(NSString *)psw tradePswErrorLab:(UILabel *)error {
    
    NSDictionary * parm = @{@"money":self.loanMoney,
                            @"period":self.period,
                            @"pay_password":psw,
                            @"loan_use_value":self.loan_use
                            };
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditLoanApplyLoan showLoading:NO param:parm succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        STRONG_SELF
        if (code == 0 && success) {
            [strongSelf.pswPop animateHide];
            YKBorrowSuccessViewController * vc = [YKBorrowSuccessViewController new];
            vc.order_id = json[@"item"][@"order_id"];
            [strongSelf.navigationController pushViewController:vc animated:YES];
            [[BrAgentManager yk_sharedManager] yk_initBrAgentWithType:BRBORROW];
        } else if(code == 3){//3交易密码错误但<错误最大值
            error.text = msg;
            [strongSelf.pswPop clearAllInput];
        } else if(code == 13){ //13错误次数达上限
            [strongSelf.pswPop animateHide];
            [[iToast makeText:msg] show];
        } else {
            [strongSelf.pswPop animateHide];
            [[iToast makeText:msg] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        if (!isConnect) {
            [strongSelf.pswPop animateHide];
            [[iToast makeText:errMsg] show];
        }
    }];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.pageModel.sectionLists.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.pageModel.sectionLists[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YKListDataModel *model = self.pageModel.sectionLists[indexPath.section][indexPath.row];
    if ([model.tag intValue] == 31 || [model.tag intValue] == 22) {//31银行卡、22还款计划
        YKBankCell *cell = [tableView dequeueReusableCellWithIdentifier:bankCell];
        [cell yk_rederCellDataWithModel:model];
        [cell.bankBtn setTitleColor:Color.main forState:UIControlStateNormal];
        if ([model.tag intValue] == 31 && self.pageModel.bank_link.length > 0) {
            cell.addIconBtn.hidden = NO;
        } else {
            cell.addIconBtn.hidden = YES;
        }
        if ([model.tag intValue] == 31 && self.pageModel.bank_link.length == 0) {
            [cell.bankBtn setTitleColor:Color.color_45_C2 forState:UIControlStateNormal];
        }else {
            [cell.bankBtn setTitleColor:Color.main forState:UIControlStateNormal];
        }
        WEAK_SELF
        cell.bankBtnBlock = ^() {
            STRONG_SELF
            if ([model.tag intValue] == 22) {
                //点击还款计划
                [strongSelf.repaymentPlanAlert updateUI:strongSelf.pageModel.repaymnet_detail];
                [strongSelf.repaymentPlanAlert show];
                
            } else if(strongSelf.pageModel.bank_link.length > 0) {
                //点击添加银行卡
                [strongSelf addBankOrCommitClickWithIsCommitOrder:NO];
            }
        };
        return cell;
    }
    
    YKApplyNormalCell *cell = [tableView dequeueReusableCellWithIdentifier:loanDetailCommonCell];
    [cell yk_rederCellDataWithModel:model];
    WEAK_SELF
    cell.totalInterestBlock = ^{ //总利息
        STRONG_SELF
        YKBorrowFeeAlertView *alertView = [YKBorrowFeeAlertView new];
        [alertView updateUI:strongSelf.pageModel.fees];
        [alertView show];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (UITableView *)tableV {
    if (!_tableV) {
        _tableV = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.allowsSelection = NO;
        _tableV.separatorColor = Color.backgroundColor;
    }
    return _tableV;
}

- (YKFooterView *)footerVV {
    if (!_footerVV) {
        _footerVV = [[YKFooterView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 5)];
    }
    return _footerVV;
}

- (YKRepaymentDetailAlert *)repaymentPlanAlert
{
    if (!_repaymentPlanAlert) {
        _repaymentPlanAlert = [YKRepaymentDetailAlert new];
    }
    return _repaymentPlanAlert;
}

@end
