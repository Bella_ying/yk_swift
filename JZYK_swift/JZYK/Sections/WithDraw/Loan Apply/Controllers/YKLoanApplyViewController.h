//
//  YKLoanApplyViewController.h
//  JZYK
//
//  Created by 吴春艳 on 2018/7/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"

@interface YKLoanApplyViewController : YKBaseViewController

//---------资金来源弹框---------
@property (nonatomic, assign) BOOL isShowMoneyComingAlert;
@property (nonatomic, copy) NSString *transition_kd_logo;
@property (nonatomic, copy) NSString *transition_tips;
@property (nonatomic, copy) NSString *transition_yk_logo;
//---------资金来源弹框---------

@property (nonatomic, copy) NSString * loanMoney;
@property (nonatomic, copy) NSString * period;
@property (nonatomic, copy) NSString * loan_use;

@end
