//
//  YKLoanInfoModel.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YKCunGunEntity.h"

@interface YKFeeModel : NSObject
@property (nonatomic, copy) NSString *interest;
@property (nonatomic, copy) NSString *other_fee;
@property (nonatomic, copy) NSString *desc;//"如您未及时还款，会产生逾期滞纳金，用于弥补账户管理费用，计算标准为2.5元/天/100元"
@end

@interface YKListDataModel : NSObject
@property (nonatomic, copy) NSString *tag;
@property (nonatomic, copy) NSString *group;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *value;
@end


@interface YKRepaymnetDetailModel : NSObject
@property (nonatomic, copy) NSString *period;
@property (nonatomic, copy) NSString *per_payment;
@property (nonatomic, copy) NSString *bj;
@property (nonatomic, copy) NSString *lx;
@end

@interface YKLoanInfoModel : NSObject
@property (nonatomic, strong) YKFeeModel *fees;
@property (nonatomic, copy) NSString *period;
@property (nonatomic, copy) NSString *money;
@property (nonatomic, copy) NSString *bank_link;//空@“”是已绑卡  109是跳绑卡
@property (nonatomic, copy) NSString *bank_name;
@property (nonatomic, copy) NSString *card_no;
@property (nonatomic, copy) NSString *verify_loan_pass;//是否可借款
@property (nonatomic, copy) NSString *real_pay_pwd_status;
@property (nonatomic, copy) NSString *bottom_tip_logo;
@property (nonatomic, strong) NSArray *agreement;
@property (nonatomic, strong) NSArray <YKRepaymnetDetailModel *>*repaymnet_detail;//还款详情数据
@property (nonatomic, strong) NSArray <YKListDataModel*>*order_params; //页面数据，用sectionLists为整理好的数据。
@property (nonatomic, strong) NSArray <NSArray<YKListDataModel *>*>*sectionLists;//整理好的order_params数据源:[[],[]]
@property (nonatomic, copy) NSString *bank_status;//1:其它平台开户 2:开过理财户 3:开户卡发生变更 4:未绑卡 5:未开通存管
@property (nonatomic, copy) NSString *tipMsg;
@property (nonatomic, strong) YKCunGunEntity *cunguan_alert;//存管弹框


@end
