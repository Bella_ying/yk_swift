//
//  YKLoanInfoModel.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKLoanInfoModel.h"
@implementation YKFeeModel
@end

@implementation YKListDataModel
@end

@implementation YKRepaymnetDetailModel

@end

@implementation YKLoanInfoModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"order_params":       [YKListDataModel class],
             @"repaymnet_detail":   [YKRepaymnetDetailModel class],
             };
}

- (void)setOrder_params:(NSArray<YKListDataModel *> *)order_params {
    _order_params = order_params;
    
    NSMutableArray * tempArr = [NSMutableArray arrayWithCapacity:order_params.count];
    [order_params enumerateObjectsUsingBlock:^(YKListDataModel*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) {
            [tempArr addObject:[NSMutableArray arrayWithObjects:obj, nil]];
        } else {
            NSMutableArray<YKListDataModel*> * lastGropArr = tempArr[tempArr.count - 1];
            if (obj.group != lastGropArr[0].group) {
                [lastGropArr addObject:obj];
            } else {
                [tempArr addObject:[NSMutableArray arrayWithObjects:obj, nil]];
            }
        }
    }];
    _sectionLists = tempArr;
}

@end
