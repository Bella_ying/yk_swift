//
//  YKBorrowSuccessEntity.h
//  KDFDApp
//
//  Created by wangjeremy on 01/06/2017.
//  Copyright © 2017 cailiang. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "YQHomeEntity.h"

@interface YKBorrowSuccessEntity : NSObject

/*
 "money_amount": 50000,
 "counter_fee": 7500,
 "loan_term": 14,
 "apply_date": "17-06-01"
 */

@property (nonatomic, copy) NSString *money_amount;
@property (nonatomic, copy) NSString *counter_fee; //总利息
@property (nonatomic, copy) NSString *interest_fee; // 借款利息
@property (nonatomic, copy) NSString *service_fee;  // 服务费用
@property (nonatomic, copy) NSString *loan_term;
@property (nonatomic, copy) NSString *apply_date;
@property (nonatomic, copy) NSString *repayment_date;
@property (nonatomic, copy) NSString *detail_url;
@property (nonatomic, copy) NSString *term_num; //分期数
@property (nonatomic, copy) NSString *cold_time; //冷静期 @“1”
@property (nonatomic, copy) NSString *cold_unit; //@"h"
@property (nonatomic, copy) NSString *tips; //冷静期 @“1h”

//弹框
//@property (nonatomic, retain) YQHomeDialog *dialog;

@end
