//
//  YKBorrowSuccessViewController.h
//  KDFDApp
//
//  Created by wangjeremy on 01/06/2017.
//  Copyright © 2017 cailiang. All rights reserved.
//

#import "YKBaseViewController.h"

@interface YKBorrowSuccessViewController : YKBaseViewController

@property (nonatomic, copy) NSString * order_id;

@end
