//
//  YKBorrowSuccessViewController.m
//  KDFDApp
//
//  Created by wangjeremy on 01/06/2017.
//  Copyright © 2017 cailiang. All rights reserved.
//

#import "YKBorrowSuccessViewController.h"
#import "YKBorrowSuccessView.h"
#import "BqsDeviceFingerManager.h"
#import "YKNeedRepayBillVC.h"
//#import "YQEventStore.h"
//#import "YKAAlertView.h"

@interface YKBorrowSuccessViewController ()

@property (nonatomic, strong) YKBorrowSuccessView *borrowSuccessView;
@property (nonatomic, retain) YKBorrowSuccessEntity *entity;
@end

@implementation YKBorrowSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"分期";
    [self.navigationItem setLeftBarButtonItem:nil];
    [self.navigationItem setHidesBackButton:YES];
    
    [self.view addSubview:self.borrowSuccessView];
    [self.borrowSuccessView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.offset(0);
    }];
//    [YQEventStore evaluateEventStorePerssion];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //白骑士指纹识别初始化，SDK文档建议在viewWillAppear：中进行初始化，不要篡改
    [[BqsDeviceFingerManager yk_sharedWhiteKnightManager] yk_initBqsDFSDKAndSubmitTokenkeyToBackend];
    
    if (!self.entity) {
        [self loadBorrowSuccessData];
    }
    
//    [BuriedPointManager reportWithKey:@"loan_success_pageview" andArray:@[@"借款成功页"]];
    // 检测日历权限
//    [self checkPermissionWithCalender];
}

#pragma mark- 日历权限检测
//- (void)checkPermissionWithCalender
//{
//    if ([YQTools qb_permissionsWithCalendar] == QBPermissionStatusNotDenied) {
//        [[YKAAlertView sharedAlertManager] showAlertType:QBShowAlertTypePermissions contentText:kCalander];
//    }
//}

- (YKBorrowSuccessView *)borrowSuccessView{
    if (!_borrowSuccessView) {
        _borrowSuccessView = [YKBorrowSuccessView new];
        _borrowSuccessView.confirmBtnAction = ^{
            YKNeedRepayBillVC *billVC = [[YKNeedRepayBillVC alloc] init];
            [[YKTabBarController yk_shareTabController] yk_setSelectedIndex:YKTabSelectedIndexMine viewController:billVC];
        };
    }
    return _borrowSuccessView;
}

- (void)loadBorrowSuccessData {
    
    NSDictionary * parm = @{@"order_id":self.order_id
                            };
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditLoanAchieveLoan showLoading:NO param:parm succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        if (code == 0 && success) {
            YKBorrowSuccessEntity *entity = [YKBorrowSuccessEntity yy_modelWithJSON:json];
            weakSelf.entity = entity;
            weakSelf.borrowSuccessView.entity = entity;
//            if (entity.dialog) {
//                [YQBorrowSuccessADView prepareData:entity.dialog];
//            }
        } else if(code != 0){
            [[iToast makeText:msg] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];

}

@end
