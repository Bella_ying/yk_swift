//
//  YKBorrowSuccessView.h
//  KDFDApp
//
//  Created by wangjeremy on 01/06/2017.
//  Copyright © 2017 cailiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKBorrowSuccessEntity.h"

@interface YKBorrowSuccessView : UIView

@property (nonatomic, strong) YKBorrowSuccessEntity *entity;
@property (nonatomic, copy) void (^confirmBtnAction)(void) ;

@end
