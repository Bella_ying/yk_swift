//
//  YKBorrowSuccessView.m
//  KDFDApp
//
//  Created by wangjeremy on 01/06/2017.
//  Copyright © 2017 cailiang. All rights reserved.
//

#import "YKBorrowSuccessView.h"

@interface YKBorrowSuccessView ()

@property (nonatomic, strong) UILabel *amount;
@property (nonatomic, strong) UILabel *borrowTime;
@property (nonatomic, strong) UILabel *applyTime;
@property (nonatomic, strong) UILabel *totalInterest;
@property (nonatomic, strong) UILabel *thinkTimeTextLabel;
@property (nonatomic, strong) UILabel *thinkTimeLab;
@property (nonatomic, strong) UILabel *thinkTipTimeLab;

@end

@implementation YKBorrowSuccessView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = Color.backgroundColor;
        [self setupBorrowSuccessView];
    }
    return self;
}

- (void)setupBorrowSuccessView{
    UIView *upView = [UIView yk_viewWithColor:Color.whiteColor superView:self masonrySet:^(UIView *view, MASConstraintMaker *make) {
        
        UIImageView *imageview = [UIImageView yk_imageViewWithImageName:@"borrow_success" superView:view masonrySet:^(UIImageView *imageView, MASConstraintMaker *make) {
            
            make.top.offset(29 * ASPECT_RATIO_HEIGHT);
            make.centerX.offset(0);
        }];
        UILabel * successLab = [UILabel yk_labelWithFontSize:adaptFontSize(18) textColor:Color.color_45_C2 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            
            make.top.equalTo(imageview.mas_bottom).offset(21 * ASPECT_RATIO_HEIGHT);
            make.centerX.offset(0);
            label.text = @"申请成功";
        }];
        
        [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_8D_C4 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            make.top.equalTo(successLab.mas_bottom).offset(10 * ASPECT_RATIO_HEIGHT);
            make.centerX.offset(0);
            make.bottom.offset(-30 * ASPECT_RATIO_HEIGHT);
            label.text = @"请耐心等待审核结果";
        }];
        
        make.left.right.offset(0);
        make.top.offset(1);
    }];
    
    UIView *middleView = [UIView yk_viewWithColor:Color.whiteColor superView:self masonrySet:^(UIView *view, MASConstraintMaker *make) {

        UILabel *amountLabel = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
           make.top.offset(15 * ASPECT_RATIO_HEIGHT);
           make.left.offset(15);
           label.text = @"借款金额";
       }];
        
       self.amount = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
           
            make.centerY.equalTo(amountLabel.mas_centerY);
            make.right.offset(-15);
        }];
        
        UILabel *totalInterestLabel = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            make.top.equalTo(amountLabel.mas_bottom).offset(13 * ASPECT_RATIO_HEIGHT);
            make.left.equalTo(amountLabel.mas_left);
            label.text = @"总利息";
        }];
        
        self.totalInterest = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {

            make.centerY.equalTo(totalInterestLabel.mas_centerY);
            make.right.offset(-15);
        }];
        
        UILabel *borrowTimeLabel = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {

            make.top.equalTo(totalInterestLabel.mas_bottom).offset(13 * ASPECT_RATIO_HEIGHT);
            make.left.equalTo(amountLabel.mas_left);
            label.text = @"借款期限";
        }];
        
        self.borrowTime = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            
            make.centerY.equalTo(borrowTimeLabel.mas_centerY);
            make.right.offset(-15);
        }];
        
        UILabel *applyTimeLabel = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {

            make.top.equalTo(borrowTimeLabel.mas_bottom).offset(13 * ASPECT_RATIO_HEIGHT);
            make.left.equalTo(amountLabel.mas_left);
            label.text = @"申请日期";
            
        }];
        
        self.applyTime = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            
            make.centerY.equalTo(applyTimeLabel.mas_centerY);
            make.right.offset(-15);
        }];
        
        UILabel *thinkTimeTextLabel = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            
            make.top.equalTo(applyTimeLabel.mas_bottom).offset(13 * ASPECT_RATIO_HEIGHT);
            make.left.equalTo(applyTimeLabel.mas_left);
            label.text = @"冷静期";
        }];
        self.thinkTimeTextLabel = thinkTimeTextLabel;
        
        self.thinkTimeLab = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            make.centerY.equalTo(thinkTimeTextLabel.mas_centerY);
            make.right.offset(-15);
        }];
        
        make.left.right.offset(0);
        make.top.equalTo(upView.mas_bottom).offset(2);
        make.bottom.equalTo(thinkTimeTextLabel.mas_bottom).offset(14 * ASPECT_RATIO_HEIGHT);
        make.bottom.equalTo(applyTimeLabel.mas_bottom).offset(14 * ASPECT_RATIO_HEIGHT).priority(300);
    }];
    
    //descLabel "冷静期结束...."
    self.thinkTipTimeLab = [UILabel yk_labelWithFontSize:adaptFontSize(13) textColor:Color.color_AD_C5 superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {

        make.top.equalTo(middleView.mas_bottom).offset(18 * ASPECT_RATIO_HEIGHT);
        make.centerX.offset(0);
        
        label.text = @"冷静期结束，订单开始审核，买家可随时取消借款";
    }];
    
    [UIButton yk_buttonFontSize:adaptFontSize(17) textColor:Color.whiteColor backGroundImage:@"btn_bg_image" imageName:@"" cornerRadius:29.0f superView:self masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        
        make.left.offset(34);
        make.right.offset(-34);
        make.height.offset(58);
        make.top.equalTo(middleView.mas_bottom).offset(59 * ASPECT_RATIO_HEIGHT);
        
        [button setTitle:@"确 定" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(confirmBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (void)confirmBtnClick:(UIButton *)sender {
    if (self.confirmBtnAction) {
        self.confirmBtnAction();
    }
}

- (void)setEntity:(YKBorrowSuccessEntity *)entity{
    _entity = entity;
    self.amount.text = [NSString stringWithFormat:@"%@元",entity.money_amount];
    self.totalInterest.text = [NSString stringWithFormat:@"%@元",entity.counter_fee];
    self.borrowTime.text = [NSString stringWithFormat:@"%@",entity.loan_term];
    self.applyTime.text = entity.apply_date;
    self.thinkTipTimeLab.text = entity.tips;
    
    if ([entity.cold_time doubleValue] > 0) {
        self.thinkTimeLab.text = [NSString stringWithFormat:@"%@%@",entity.cold_time,entity.cold_unit];
    }else {
        [self.thinkTimeTextLabel removeFromSuperview];
        self.thinkTimeLab.hidden = YES;
    }
    
}

@end
