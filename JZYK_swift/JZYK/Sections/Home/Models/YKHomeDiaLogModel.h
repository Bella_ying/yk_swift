//
//  YKHomeDiaLogModel.h
//  JZYK
//
//  Created by hongyu on 2018/7/3.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKHomeDiaLogFrameModel : NSObject

@property (nonatomic, copy) NSString *x;
@property (nonatomic, copy) NSString *y;

@end

@interface YKHomeDiaLogModel : NSObject

//弹框标识id
@property (nonatomic, copy) NSString *dialog_id;
//弹框图片
@property (nonatomic, copy) NSString *img_url;
//跳转url
@property (nonatomic, strong) JumpModel *active;
//关闭按钮坐标
@property (nonatomic, strong) YKHomeDiaLogFrameModel *close_origin;
//是否展示
@property (nonatomic, copy) NSString *is_show;

@end
