//
//  YKHomeDiaLogModel.m
//  JZYK
//
//  Created by hongyu on 2018/7/3.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKHomeDiaLogModel.h"

@implementation YKHomeDiaLogFrameModel

@end

@implementation YKHomeDiaLogModel

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{
             @"dialog_id" : @"id"
             };
}

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{
             @"active" : [JumpModel class],
             @"close_origin" : [YKHomeDiaLogFrameModel class]
             };
}

@end
