//
//  YKHomeModel.h
//  JZYK
//
//  Created by hongyu on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YKHomeBannerModel;
@class YKHomeColumnModel;
@class YKHomeActivityModel;
@class YKHomeShopGrupModel;
@class YKHomeDiaLogModel;
@class YKHomeMessageModel;

@interface YKHomeModel : NSObject

@property (nonatomic, copy) NSArray <YKHomeBannerModel *>   *app_index;
@property (nonatomic, copy) NSArray <YKHomeColumnModel *>   *fast_link;
@property (nonatomic, copy) NSArray <YKHomeActivityModel *> *small_index;
@property (nonatomic, copy) NSArray <YKHomeActivityModel *> *group_left;
@property (nonatomic, copy) NSArray <YKHomeActivityModel *> *group_right;
@property (nonatomic, copy) NSArray <YKHomeShopGrupModel *> *shop_grup;

@property (nonatomic, strong) YKHomeDiaLogModel  *dia_log;
@property (nonatomic, strong) YKHomeMessageModel *message;

@end
