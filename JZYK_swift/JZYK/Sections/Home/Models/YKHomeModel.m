//
//  YKHomeModel.m
//  JZYK
//
//  Created by hongyu on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKHomeModel.h"
#import "YKHomeBannerModel.h"
#import "YKHomeColumnModel.h"
#import "YKHomeActivityModel.h"
#import "YKHomeShopGrupModel.h"
#import "YKHomeDiaLogModel.h"
#import "YKHomeMessageModel.h"

@implementation YKHomeModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{
             @"app_index"   : [YKHomeBannerModel class],
             @"fast_link"   : [YKHomeColumnModel class],
             @"small_index" : [YKHomeActivityModel class],
             @"group_left"  : [YKHomeActivityModel class],
             @"group_right" : [YKHomeActivityModel class],
             @"shop_grup"   : [YKHomeShopGrupModel class]
             };
}

@end
