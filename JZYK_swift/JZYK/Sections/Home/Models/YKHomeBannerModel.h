//
//  YKHomeBannerModel.h
//  JZYK
//
//  Created by hongyu on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKHomeBannerModel : NSObject

@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *skip_code;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *data;
@end
