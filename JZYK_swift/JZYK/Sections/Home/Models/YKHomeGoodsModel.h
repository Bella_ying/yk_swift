//
//  YKHomeGoodsModel.h
//  JZYK
//
//  Created by hongyu on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YKHomeGoodsLabelModel;

@interface YKHomeGoodsModel : NSObject

@property (nonatomic, copy) NSString *installment_type;
@property (nonatomic, copy) NSString *goods_id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *img_url;
@property (nonatomic, copy) NSString *installment_price;
@property (nonatomic, copy) NSString *installment_month;
@property (nonatomic, copy) NSString *month_pay;
@property (nonatomic, copy) NSArray <YKHomeGoodsLabelModel *> *activity_label;

@end
