//
//  YKHomeGoodsModel.m
//  JZYK
//
//  Created by hongyu on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKHomeGoodsModel.h"
#import "YKHomeGoodsLabelModel.h"

@implementation YKHomeGoodsModel

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{
             @"goods_id" : @"id"
             };
}

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{
             @"activity_label" : [YKHomeGoodsLabelModel class]
             };
}

@end
