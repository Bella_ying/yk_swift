//
//  YKHomeShopGrupModel.m
//  JZYK
//
//  Created by hongyu on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKHomeShopGrupModel.h"
#import "YKHomeGoodsModel.h"

@implementation YKHomeShopGrupModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{
             @"item" : [YKHomeGoodsModel class]
             };
}

@end
