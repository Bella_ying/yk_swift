//
//  YKHomeMessageModel.h
//  JZYK
//
//  Created by hongyu on 2018/7/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKHomeMessageModel : NSObject

@property (nonatomic, copy) NSString *num;
@property (nonatomic, copy) NSString *url;

@end
