//
//  YKHomeShopGrupModel.h
//  JZYK
//
//  Created by hongyu on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YKHomeGoodsModel;

@interface YKHomeShopGrupModel : NSObject

@property (nonatomic, copy) NSString *sort_number;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *title_color;
@property (nonatomic, copy) NSArray <YKHomeGoodsModel *> *item;

@end
