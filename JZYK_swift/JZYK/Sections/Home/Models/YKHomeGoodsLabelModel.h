//
//  YKGoodsLabelModel.h
//  JZYK
//
//  Created by hongyu on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKHomeGoodsLabelModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *color;

@end
