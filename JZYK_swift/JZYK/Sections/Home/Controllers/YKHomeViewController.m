//
//  YKHomeViewController.m
//  JZYK
//
//  Created by hongyu on 2018/6/1.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKHomeViewController.h"
#import "YKHomeNavigationView.h"
#import "YKHomeHeaderView.h"
#import "YKHomeSectionView.h"
#import "YKHomeGoodsCell.h"
#import "YKHomeModel.h"
#import "YKHomeGoodsModel.h"
#import "YKHomeShopGrupModel.h"
#import "YKHomeMessageModel.h"
#import "YKHomeDiaLogModel.h"
#import "QiYuManager.h"
#import "UIButton+category.h"
#import "YKMallDetailViewController.h"
#import "XHLaunchAd.h"
#import "YKHomeInfoManager.h"
#import "YKHomeAdView.h"
#import "QBGestureViewController.h"
#import "QBGestureTool.h"

static const CGFloat homeHeaderBannerHeight = 191.f;
static const CGFloat homeSectionViewHeight  = 15.f;
static const CGFloat homeCellHeight         = 106.f;
static const CGFloat durationTime           = 3.f;
static NSString *launchTwoAdStr             = @"http://oji6h5o73.bkt.clouddn.com/welcome@2x.gif";
static NSString *launchThreeAdStr           = @"http://oji6h5o73.bkt.clouddn.com/welcome@3x.gif";

@interface YKHomeViewController ()<XHLaunchAdDelegate, QBGestureViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView   *homeTableView;
@property (strong, nonatomic) YKHomeNavigationView *homeNavigationView;
@property (strong, nonatomic) YKHomeHeaderView     *homeHeaderView;
@property (strong, nonatomic) UIView               *homeFooterView;
@property (strong, nonatomic) YKHomeSectionView    *homeSectionView;
@property (assign, nonatomic) CGFloat              homeHeaderViewHeight;
@property (strong, nonatomic) NSMutableArray       *goodsSectionArray;
@property (strong, nonatomic) JumpModel            *launchAdModel;
@property (strong, nonatomic) YKHomeDiaLogModel    *diaLigModel;

@end

@implementation YKHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareLaunchAd];
    [self prepareUI];
    [self prepareTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self fetchRedPointData];
    if (!iOS_VERSION_11) {
        [self.homeTableView setContentOffset:CGPointMake(0, 0) animated:NO];
    }
    [self fetchData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)fetchData
{
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kMallHomeIndex showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        DLog(@"%@", json);
        if (success && code == 0) {
            weakSelf.homeTableView.hidden = NO;
            [weakSelf.homeTableView hd_coverDismiss];
            [weakSelf.goodsSectionArray removeAllObjects];
            //header
            YKHomeModel *superHomeModel = [YKHomeModel yy_modelWithDictionary:json];
            YKHomeModel *homeModel = [YKHomeModel yy_modelWithDictionary:json[@"banner_list"]];
            [weakSelf.homeHeaderView yk_renderBannerModels:homeModel.app_index];
            [weakSelf.homeHeaderView yk_renderColumnModels:homeModel.fast_link];
            [weakSelf.homeHeaderView yk_renderActivityModels:homeModel.small_index];
            NSArray *noobs;
            if (homeModel.group_left.count && homeModel.group_right.count) {
                noobs = @[homeModel.group_left[0], homeModel.group_right[0], homeModel.group_right[1]];
                [weakSelf.homeHeaderView yk_renderNoobModels:noobs];
            }
            weakSelf.homeHeaderViewHeight = 170 + (homeModel.fast_link.count ? 90 : 0) + (homeModel.small_index.count ? 85 : 0) + (homeModel.group_left.count && homeModel.group_right.count ? 120 : 0);
            [weakSelf.homeHeaderView setHeight:weakSelf.homeHeaderViewHeight * ASPECT_RATIO_WIDTH];
            //goods
            for (NSDictionary *dic in json[@"shop_grup"]) {
                YKHomeShopGrupModel *model = [YKHomeShopGrupModel yy_modelWithDictionary:dic];
                [weakSelf.goodsSectionArray addObject:model];
            }
            //message
            if (superHomeModel.message && [superHomeModel.message.num integerValue] > 0) {
                [weakSelf.homeNavigationView.messageButton yk_showRedPointOnButtonWithCount:[superHomeModel.message.num integerValue]];
            } else {
                [weakSelf.homeNavigationView.messageButton yk_hideRedPointOnButton];
            }
            //ad launch
            if (superHomeModel.dia_log) {
                weakSelf.diaLigModel = superHomeModel.dia_log;
            }
            
            [weakSelf.homeTableView reloadData];
            [weakSelf.homeTableView hd_endFreshing:YES];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
        [weakSelf.homeTableView hd_endFreshing:YES];
    }];
}

// 页面渲染
- (void)prepareUI
{
    [YKClickManager yk_reportWithKey:@"main_pageview" array:@[@"首页"]];
    self.navigationItem.title = @"首页";
    self.showNavigationBar = NO;
    self.homeTableView.hidden = YES;
    self.homeTableView.showsVerticalScrollIndicator = NO;
    self.homeTableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    self.homeTableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    [self.view addSubview:self.homeNavigationView];
    [self.homeNavigationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.mas_offset(NAV_HEIGHT);
    }];
}
// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.homeTableView);
    WEAK_SELF
    [[self.homeTableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        
        [tableMaker.hd_tableViewHeaderView(^() {
            return weakSelf.homeHeaderView;
        }).hd_tableViewFooterView(^() {
            return weakSelf.homeFooterView;
        }).hd_sectionCountBk(HDSectionCount(return weakSelf.goodsSectionArray.count;)) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            sectionMaker.hd_headerView(^() {
                YKHomeShopGrupModel *shopModel = weakSelf.goodsSectionArray[sectionMaker.section];
                return [weakSelf prepareHomeSectionViewWithData:shopModel];
            });
            
            [sectionMaker.hd_dataArr(^() {
                YKHomeShopGrupModel *shopModel = weakSelf.goodsSectionArray[sectionMaker.section];
                return shopModel.item;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKHomeGoodsCell))
                .hd_rowHeight(homeCellHeight)
                .hd_adapter(^(YKHomeGoodsCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                    YKHomeShopGrupModel *shopModel = weakSelf.goodsSectionArray[indexPath.section];
                    YKHomeGoodsModel *goodsModel = shopModel.item[indexPath.row];
                    [cell renderModel:goodsModel];
                })
                .hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    YKHomeShopGrupModel *shopModel = weakSelf.goodsSectionArray[indexPath.section];
                    YKHomeGoodsModel *goodsModel = shopModel.item[indexPath.row];
                    YKMallDetailViewController *goodsInfoVc = [YKMallDetailViewController new];
                    goodsInfoVc.shopList_id = [goodsModel.goods_id integerValue];
                    [weakSelf.navigationController pushViewController:goodsInfoVc animated:YES];
                });
            }];
        }];
        tableMaker.hd_scrollViewDidScroll(^(UIScrollView * scrollView) {
            CGFloat offsetY = scrollView.contentOffset.y;
            CGFloat slideOffsetY = homeHeaderBannerHeight - NAV_HEIGHT;
            weakSelf.homeNavigationView.backgroundColor = [MAIN_THEME_COLOR colorWithAlphaComponent:offsetY / slideOffsetY];
            weakSelf.homeNavigationView.navImageView.alpha = offsetY / slideOffsetY;
            weakSelf.homeNavigationView.hidden = offsetY < 0 ? YES : NO;
        });
    }] hd_addFreshHeader:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf fetchData];
        });
    }];
    
    self.homeNavigationView.serviceBlock = ^{
        
        if (![[YKUserManager sharedUser] yk_isLogin]) {
            LoginViewController *login = [LoginViewController login];
            [weakSelf presentViewController:login animated:YES completion:nil];
        } else {
            [QiYuManager yk_sharedQiYuManager].commonQuestionTemplateId = [[ConfigManager config] getQiyuTemplateIDForKey:@"index"];
            [[QiYuManager yk_sharedQiYuManager] yk_qiYuService];
            [[QiYuManager yk_sharedQiYuManager] yk_qiYuDelegate];
        }
    };
    
    self.homeNavigationView.messageBlock = ^{
        [weakSelf.homeNavigationView.messageButton yk_hideRedPointOnButton];
        YKBrowseWebController *webVC = [YKBrowseWebController new];
        webVC.url = [[ConfigManager config] getURLWithKey:kCreditMsgCenter];
        [weakSelf.navigationController pushViewController:webVC animated:YES];
    };
}

- (YKHomeSectionView *)prepareHomeSectionViewWithData:(YKHomeShopGrupModel *)model
{
    YKHomeSectionView *sectionView = [[YKHomeSectionView alloc] init];
    sectionView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, model.item.count ? homeSectionViewHeight * ASPECT_RATIO_WIDTH : 0);
    [sectionView renderModel:model];
    return sectionView;
}

#pragma mark- 广告页
- (void)prepareLaunchAd
{
    XHLaunchImageAdConfiguration *imgAdConfiguration = [XHLaunchImageAdConfiguration new];
    imgAdConfiguration.duration = durationTime;
    imgAdConfiguration.frame    = CGRectMake(0, 0, WIDTH_OF_SCREEN, HEIGHT_OF_SCREEN);
    imgAdConfiguration.imageOption = XHLaunchAdImageRefreshCached;
    imgAdConfiguration.contentMode = UIViewContentModeScaleToFill;
    imgAdConfiguration.showFinishAnimate = ShowFinishAnimateLite;
    imgAdConfiguration.skipButtonType = SkipTypeTimeText;
    imgAdConfiguration.showEnterForeground = NO;
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kNoticebounced showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString * _Nonnull msg) {
        weakSelf.homeTableView.hidden = NO;
        if (code == 0 && success) {
            NSArray *dataArray = json[@"item"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (dataArray.count) {
                        imgAdConfiguration.imageNameOrURLString = [NSString stringWithFormat:@"%@", [dataArray firstObject][@"img_url"]];
                        weakSelf.launchAdModel = [JumpModel new];
                        weakSelf.launchAdModel.skip_code = [NSString stringWithFormat:@"%@", [dataArray firstObject][@"skip_code"]];
                        weakSelf.launchAdModel.url       = [NSString stringWithFormat:@"%@", [dataArray firstObject][@"jump_url"]];
                        [XHLaunchAd imageAdWithImageAdConfiguration:imgAdConfiguration delegate:self];
                    } else {
                        // 去掉跳动启动页
                        // imgAdConfiguration.imageNameOrURLString = IS_iPhoneX ? launchThreeAdStr : launchTwoAdStr;
                    }
                    // 广告 -> 手势密码 -> 弹窗(更新->通知->手势)
                    if ([QBGestureTool isGestureEnableByUser] && [YKUserManager sharedUser].yk_isLogin) {
                        QBGestureViewController *gestureVC = [[QBGestureViewController alloc]init];
                        gestureVC.delegate = self;
                        [gestureVC showInViewController:self type:QBGestureTypeVerifying];
                    } else {
                        [weakSelf prepareHomeInfoManager];
                    }
                });
          
        }
        
    } failure:^(NSString * _Nonnull msg, BOOL isConnect) {
        weakSelf.homeTableView.hidden = NO;
    }];
}

- (void)gestureViewVerifiedSuccess:(QBGestureViewController *)vc
{
    [self prepareHomeInfoManager];
}

- (void)xhLaunchAd:(XHLaunchAd *)launchAd clickAndOpenModel:(id)openModel clickPoint:(CGPoint)clickPoint
{
    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:self.launchAdModel];
}

#pragma mark- 红点逻辑
- (void)fetchRedPointData
{
    // 七鱼未读数
    NSInteger count = [[[QYSDK sharedSDK] conversationManager] allUnreadCount];
    if (count > 0) {
        [self.homeNavigationView.serviceButton yk_showRedPointOnButtonWithCount:count];
    } else {
        [self.homeNavigationView.serviceButton yk_hideRedPointOnButton];
    }
    [[HTTPManager session] getRequestForKey:kCreditHotDot showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString * _Nonnull msg) {
        if (code == 0 && success) {
            if ([json[@"load"][@"is_show"] integerValue] == 1) {
                [[YKTabBarController yk_shareTabController].ykTabBar yk_showBadgeOnItemIndex:3];
            } else {
                [[YKTabBarController yk_shareTabController].ykTabBar yk_hideBadgeOnItemIndex:3];
            }
        }
    } failure:^(NSString * _Nonnull msg, BOOL isConnect) {
        
    }];
}

#pragma mark- 首页权限相关
- (void)prepareHomeInfoManager
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [YKHomeInfoManager yk_sharedHomeInfoManager].adViewBlock = ^{
            [[YKHomeAdView yk_sharedHomeAdView] yk_renderAdModel:self.diaLigModel];
        };
        [[YKHomeInfoManager yk_sharedHomeInfoManager] yk_checkoutVersion];
    });
}

#pragma mark- 懒加载
- (YKHomeNavigationView *)homeNavigationView
{
    if (!_homeNavigationView) {
        _homeNavigationView = [YKHomeNavigationView new];
        _homeNavigationView.backgroundColor = [UIColor clearColor];
    }
    return _homeNavigationView;
}

- (YKHomeHeaderView *)homeHeaderView
{
    if (!_homeHeaderView) {
        _homeHeaderView = [YKHomeHeaderView new];
        _homeHeaderView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 460 * ASPECT_RATIO_WIDTH);
    }
    return _homeHeaderView;
}

- (UIView *)homeFooterView
{
    if (!_homeFooterView) {
        _homeFooterView = [UIView new];
        _homeFooterView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, IS_iPhoneX ? 15 : 64);
        _homeFooterView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _homeFooterView;
}

- (NSMutableArray *)goodsSectionArray
{
    if (!_goodsSectionArray) {
        _goodsSectionArray = [@[] mutableCopy];
    }
    return _goodsSectionArray;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
