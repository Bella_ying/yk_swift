//
//  YKHomeGoodsCell.m
//  JZYK
//
//  Created by hongyu on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKHomeGoodsCell.h"
#import "YKHomeGoodsModel.h"
#import "YKHomeGoodsLabelModel.h"

@interface YKHomeGoodsCell ()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel     *goodsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel     *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel     *saleLabel;
@property (weak, nonatomic) IBOutlet UILabel     *returnMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel     *monthlyLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saleLabelWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *returnMoneyLabelWidth;

@end

@implementation YKHomeGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)renderModel:(YKHomeGoodsModel *)model
{
    [self.goodsImage sd_setImageWithURL:[NSURL URLWithString:model.img_url] placeholderImage:YK_IMAGE(@"home_goods_placeHolder")];
    self.goodsImage.layer.cornerRadius  = 3.f;
    self.goodsImage.layer.masksToBounds = YES;
    self.goodsTitleLabel.text = model.title;
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@", model.installment_price];
    self.priceLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(18)];
    self.monthlyLabel.text = [NSString stringWithFormat:@"月供：最低%@", model.month_pay];
    if (model.activity_label.count) {
        if (model.activity_label.count == 1) {
            YKHomeGoodsLabelModel *labelModel = model.activity_label[0];
            self.saleLabelWidth.constant = [self labelRectWithSize:CGSizeMake(0, 16) LabelText:labelModel.name FontSize:adaptFontSize(11)].width;
            [self prepareColorWithLabel:self.saleLabel color:[UIColor yk_colorWithHexString:labelModel.color]];
            self.saleLabel.text = labelModel.name;
            self.returnMoneyLabelWidth.constant = 0;
        }
        if (model.activity_label.count == 2) {
            YKHomeGoodsLabelModel *onelabelModel = model.activity_label[0];
            YKHomeGoodsLabelModel *twoLabelModel = model.activity_label[1];
            self.saleLabelWidth.constant = [self labelRectWithSize:CGSizeMake(0, 16) LabelText:onelabelModel.name FontSize:adaptFontSize(11)].width;
            self.returnMoneyLabelWidth.constant = [self labelRectWithSize:CGSizeMake(0, 16) LabelText:twoLabelModel.name FontSize:adaptFontSize(11)].width;
            self.saleLabel.text = onelabelModel.name;
            self.returnMoneyLabel.text = twoLabelModel.name;
            [self prepareColorWithLabel:self.saleLabel color:[UIColor yk_colorWithHexString:onelabelModel.color]];
            [self prepareColorWithLabel:self.returnMoneyLabel color:[UIColor yk_colorWithHexString:twoLabelModel.color]];
        }
    } else {
        self.saleLabelWidth.constant = 0;
        self.returnMoneyLabelWidth.constant = 0;
    }
}

- (CGSize)labelRectWithSize:(CGSize)size
                  LabelText:(NSString *)labelText
                   FontSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    CGSize actualsize = [labelText boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    actualsize.width  += 10;
    actualsize.height += 10;
    return actualsize;
}

- (void)prepareColorWithLabel:(UILabel *)label color:(UIColor *)color
{
    label.textColor = color;
    [YKTools yk_prepareRadiusWithView:label masksToBounds:YES cornerRadius:8.f borderWidth:.5f borderColor:color];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
