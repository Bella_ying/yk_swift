//
//  YKHomeGoodsCell.h
//  JZYK
//
//  Created by hongyu on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YKHomeGoodsModel;

@interface YKHomeGoodsCell : UITableViewCell

- (void)renderModel:(YKHomeGoodsModel *)model;

@end
