//
//  YKHomeNavigationView.h
//  JZYK
//
//  Created by hongyu on 2018/6/6.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ykHomeNavigationServiceBlock)(void);
typedef void (^ykHomeNavigationMessageBlock)(void);

@interface YKHomeNavigationView : UIView

@property (nonatomic, strong) UIImageView *navImageView;
@property (nonatomic, strong) UIButton    *serviceButton;
@property (nonatomic, strong) UIButton    *messageButton;

@property (nonatomic, copy) ykHomeNavigationServiceBlock serviceBlock;
@property (nonatomic, copy) ykHomeNavigationMessageBlock messageBlock;

@end
