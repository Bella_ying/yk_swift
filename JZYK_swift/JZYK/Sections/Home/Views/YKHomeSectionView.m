
//
//  YKHomeSectionView.m
//  JZYK
//
//  Created by hongyu on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKHomeSectionView.h"
#import "YKHomeShopGrupModel.h"

@interface YKHomeSectionView ()

@property (nonatomic, strong) UIImageView *labelImageView;
@property (nonatomic, strong) UILabel     *titleLabel;

@end

@implementation YKHomeSectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self prepareUI];
        self.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return self;
}

- (void)prepareUI
{
    [self addSubview:self.labelImageView];
    [self.labelImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5 * ASPECT_RATIO_WIDTH);
        make.left.offset(0);
        make.width.mas_offset(90 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(26 * ASPECT_RATIO_WIDTH);
    }];
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5 * ASPECT_RATIO_WIDTH);
        make.left.offset(15 * ASPECT_RATIO_WIDTH);
        make.width.mas_offset(90 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(26 * ASPECT_RATIO_WIDTH);
    }];
}

- (void)renderModel:(YKHomeShopGrupModel *)model
{
    UIImage *img;
    switch ([model.title_color integerValue]) {
        case 1:
            img = YK_IMAGE(@"home_mark_one");
            break;
        case 2:
            img = YK_IMAGE(@"home_mark_two");
            break;
        case 3:
            img = YK_IMAGE(@"home_mark_three");
            break;
        case 4:
            img = YK_IMAGE(@"home_mark_four");
            break;
        case 5:
            img = YK_IMAGE(@"home_mark_five");
            break;
    }
    self.labelImageView.image = img;
    self.titleLabel.text = model.title;
}

- (UIImageView *)labelImageView
{
    if (!_labelImageView) {
        _labelImageView = [UIImageView new];
    }
    return _labelImageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = WHITE_COLOR;
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(15)];
    }
    return _titleLabel;
}

@end
