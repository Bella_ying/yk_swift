//
//  YKHomeAdView.m
//  JZYK
//
//  Created by hongyu on 2018/7/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKHomeAdView.h"
#import "YKHomeDiaLogModel.h"

@interface YKHomeAdView ()

@property (nonatomic, strong) UIImageView *adImageView;
@property (nonatomic, strong) UIButton    *closeButton;
@property (nonatomic, strong) UIView      *bgView;
@property (nonatomic, strong) JumpModel   *jumpModel;

@end

@implementation YKHomeAdView

+ (instancetype)yk_sharedHomeAdView
{
    static dispatch_once_t onceToken;
    static YKHomeAdView *adView;
    dispatch_once(&onceToken, ^{
        adView = [YKHomeAdView new];
    });
    return adView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self prepareUI];
    }
    return self;
}

- (void)prepareUI
{
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = [WHITE_COLOR colorWithAlphaComponent:0];
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.adImageView];
    [self.adImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.offset(0);
    }];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideAdViewAction:)];
    [self.adImageView addGestureRecognizer:tap];
    [self.bgView addSubview:self.closeButton];
}

- (void)hideAdViewAction:(UITapGestureRecognizer *)tap
{
    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:self.jumpModel];
    [self dismissAlertView];
}

- (void)closeButtonAction
{
    [self dismissAlertView];
}

#pragma mark- load data
- (void)yk_renderAdModel:(YKHomeDiaLogModel *)model
{
    // 首页弹屏广告 一天展示一次 第一次启动直接展示并保存状态，下次启动校验时间差天数如若大于0则代表明天之后打开从新展示并记录状态 当天时间差为0不展示了。
    if ([model.is_show integerValue] == 1 && model.img_url.length) {
        if ([YK_NSUSER_DEFAULT objectForKey:@"yk_ad_view_time"]) {
            NSInteger day = [[YK_NSUSER_DEFAULT objectForKey:@"yk_ad_view_time"] yk_distanceInDaysToDate:[NSDate dateNow]];
            if (day > 0) {
                [self showAdViewWithData:model];
            }
        } else {
            [self showAdViewWithData:model];
        }
    }
}

- (void)showAdViewWithData:(YKHomeDiaLogModel *)data
{
    [YK_NSUSER_DEFAULT setObject:[NSDate dateNow] forKey:@"yk_ad_view_time"];
    self.jumpModel = data.active;
    [self.adImageView sd_setImageWithURL:[NSURL URLWithString:data.img_url]];
    self.closeButton.frame = CGRectMake(WIDTH_OF_SCREEN * ([data.close_origin.x integerValue] * 0.01) - 28, HEIGHT_OF_SCREEN * ([data.close_origin.y integerValue] * 0.01) - 28, 56, 56);
    [self showAdView];
}

- (void)showAdView
{
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgView];
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 1;
    } completion:^(BOOL finished) {
        if (self.closeBlock) {
            self.closeBlock();
        }
    }];
}

- (void)dismissAlertView
{
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [self.closeButton removeFromSuperview];
    }];
}

- (UIImageView *)adImageView
{
    if (!_adImageView) {
        _adImageView = [UIImageView new];
        _adImageView.userInteractionEnabled = YES;
    }
    return _adImageView;
}

- (UIButton *)closeButton
{
    if (!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        [_closeButton addTarget:self action:@selector(closeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _bgView.backgroundColor = [GRAY_COLOR_45 colorWithAlphaComponent:0.5];
        _bgView.alpha = 0;
    }
    return _bgView;
}

@end
