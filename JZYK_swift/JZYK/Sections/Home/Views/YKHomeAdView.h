//
//  YKHomeAdView.h
//  JZYK
//
//  Created by hongyu on 2018/7/5.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YKHomeDiaLogModel;

typedef void(^ykSelectedCloseBlock)(void);

@interface YKHomeAdView : UIView

@property (nonatomic, copy) ykSelectedCloseBlock closeBlock;

+ (instancetype)yk_sharedHomeAdView;

- (void)yk_renderAdModel:(YKHomeDiaLogModel *)model;

@end
