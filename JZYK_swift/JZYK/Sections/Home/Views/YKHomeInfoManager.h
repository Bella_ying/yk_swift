//
//  YKHomeInfoManager.h
//  JZYK
//
//  Created by hongyu on 2018/6/27.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ykCheckoutAdViewBlock)(void);

@interface YKHomeInfoManager : NSObject

@property (nonatomic, copy) ykCheckoutAdViewBlock adViewBlock;

+ (instancetype)yk_sharedHomeInfoManager;

- (void)yk_checkoutVersion;

@end
