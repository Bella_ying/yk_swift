//
//  YKHomeHeaderView.m
//  JZYK
//
//  Created by hongyu on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//  header 高度组成 banner 170 collection 90 - 5 activity 85 noob 120

#import "YKHomeHeaderView.h"
#import "SDCycleScrollView.h"
#import "YKHomeColumnCell.h"
#import "YKHomeBannerModel.h"
#import "YKHomeActivityModel.h"
#import "YKHomeColumnModel.h"

@interface YKHomeHeaderView ()<SDCycleScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) SDCycleScrollView *bannerView;
@property (nonatomic, strong) UICollectionView  *columnCollectionView;
@property (nonatomic, strong) UIView            *activityImageView;
@property (nonatomic, strong) UIButton          *activityImageButton;
@property (nonatomic, strong) UIView            *noobActivityImageView;
@property (nonatomic, strong) UIButton          *noobLeftImageButton;
@property (nonatomic, strong) UIButton          *noobRightTopImageButton;
@property (nonatomic, strong) UIButton          *noobRightBottomImageButton;
@property (nonatomic, strong) NSMutableArray    *bannerModelArray;
@property (nonatomic, strong) NSMutableArray    *columnModelArray;
@property (nonatomic, strong) YKHomeActivityModel *activityTopModel;
@property (nonatomic, strong) YKHomeActivityModel *activityLeftModel;
@property (nonatomic, strong) YKHomeActivityModel *activityRightTopModel;
@property (nonatomic, strong) YKHomeActivityModel *activityRightBottomModel;

@end

@implementation YKHomeHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = WHITE_COLOR;
        [self prepareHeaderView];
    }
    return self;
}

- (void)prepareHeaderView
{
    // 广告视图
    [self addSubview:self.bannerView];
    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.mas_offset(170 * ASPECT_RATIO_WIDTH + iPhoneX_ADD_NAV_HEIGHT);
    }];
    // 流式布局
    [self addSubview:self.columnCollectionView];
    self.columnCollectionView.delegate = self;
    self.columnCollectionView.dataSource = self;
    [self.columnCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bannerView.mas_bottom).offset(-5);
        make.left.right.offset(0);
        make.height.mas_offset(90 * ASPECT_RATIO_WIDTH);
    }];
    // 活动视图
    [self addSubview:self.activityImageView];
    [self.activityImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.columnCollectionView.mas_bottom).offset(0);
        make.left.right.offset(0);
        make.height.mas_offset(85 * ASPECT_RATIO_WIDTH);
    }];
    [self.activityImageView addSubview:self.activityImageButton];
    [self.activityImageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.offset(0);
    }];
    // 新手活动视图
    [self addSubview:self.noobActivityImageView];
    [self.noobActivityImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.activityImageView.mas_bottom).offset(0);
        make.left.right.bottom.offset(0);
    }];
    [self.noobActivityImageView addSubview:self.noobLeftImageButton];
    [self.noobLeftImageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(2);
        make.left.offset(0);
        make.bottom.offset(0);
        make.width.equalTo(self.noobActivityImageView.mas_width).multipliedBy(0.49);
    }];
    [self.noobActivityImageView addSubview:self.noobRightTopImageButton];
    [self.noobRightTopImageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(2);
        make.right.offset(0);
        make.left.equalTo(self.noobLeftImageButton.mas_right).offset(2);
        make.height.equalTo(self.noobActivityImageView.mas_height).multipliedBy(0.49);
    }];
    [self.noobActivityImageView addSubview:self.noobRightBottomImageButton];
    [self.noobRightBottomImageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.noobRightTopImageButton.mas_bottom).offset(2);
        make.left.equalTo(self.noobLeftImageButton.mas_right).offset(2);
        make.right.offset(0);
        make.bottom.offset(0);
    }];
}

- (void)yk_renderBannerModels:(NSArray *)models
{
    if (models.count) {
        self.bannerModelArray = [NSMutableArray arrayWithArray:models];
        NSMutableArray *pictureUrlArray = [NSMutableArray array];
        for (YKHomeBannerModel *model in models) {
            [pictureUrlArray addObject:model.pic];
        }
        self.bannerView.imageURLStringsGroup = pictureUrlArray;
    }
}

- (void)yk_renderColumnModels:(NSArray *)models
{
    if (models.count) {
        self.columnModelArray = [NSMutableArray arrayWithArray:models];
        [self.columnCollectionView reloadData];
    }
    [self.columnCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_offset(models.count ? 90 * ASPECT_RATIO_WIDTH : 0);
    }];
}

- (void)yk_renderActivityModels:(NSArray *)models
{
    if (models.count) {
        if (models.firstObject) {
            self.activityTopModel = models.firstObject;
            if (self.activityTopModel.pic.length) {
                [self.activityImageButton sd_setImageWithURL:[NSURL URLWithString:self.activityTopModel.pic] forState:UIControlStateNormal];
                [self.activityImageButton setBackgroundImage:YK_IMAGE(@"") forState:UIControlStateNormal];
            } else {
                [self.activityImageButton setBackgroundImage:YK_IMAGE(@"home_activity_placeHolder") forState:UIControlStateNormal];
            }
        }
    }
    [self.activityImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_offset(models.count ? 85 * ASPECT_RATIO_WIDTH : 0);
    }];
}

- (void)yk_renderNoobModels:(NSArray *)models
{
    if (models.count == 3) {
        self.activityLeftModel = models[0];
        self.activityRightTopModel = models[1];
        self.activityRightBottomModel = models[2];
        [self.noobLeftImageButton sd_setImageWithURL:[NSURL URLWithString:self.activityLeftModel.pic] forState:UIControlStateNormal placeholderImage:YK_IMAGE(@"home_left_placeHolder")];
        [self.noobRightTopImageButton sd_setImageWithURL:[NSURL URLWithString:self.activityRightTopModel.pic] forState:UIControlStateNormal placeholderImage:YK_IMAGE(@"home_right_placeHolder")];
        [self.noobRightBottomImageButton sd_setImageWithURL:[NSURL URLWithString:self.activityRightBottomModel.pic] forState:UIControlStateNormal placeholderImage:YK_IMAGE(@"home_right_placeHolder")];
    } else {
        [self.noobLeftImageButton setBackgroundImage:YK_IMAGE(@"home_left_placeHolder") forState:UIControlStateNormal];
        [self.noobRightTopImageButton setBackgroundImage:YK_IMAGE(@"home_right_placeHolder") forState:UIControlStateNormal];
        [self.noobRightBottomImageButton setBackgroundImage:YK_IMAGE(@"home_right_placeHolder") forState:UIControlStateNormal];
    }
    self.noobActivityImageView.hidden = models.count ? NO : YES;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.columnModelArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YKHomeColumnCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YKHomeColumnCell" forIndexPath:indexPath];
    [cell yk_renderModel:self.columnModelArray[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    YKHomeColumnModel *columnModel = self.columnModelArray[indexPath.row];
    JumpModel *model = [JumpModel new];
    model.skip_code = columnModel.skip_code;
    model.url = columnModel.url;
    model.data = columnModel.data;
    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:model];
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    YKHomeBannerModel *bannerModel = self.bannerModelArray[index];
    JumpModel *model = [JumpModel new];
    model.skip_code = bannerModel.skip_code;
    model.url = bannerModel.url;
    model.data = bannerModel.data;
    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:model];
}

#pragma mark- 活动点击方法
- (void)activityImageAction:(UIButton *)button
{
    JumpModel *model = [JumpModel new];
    model.skip_code = self.activityTopModel.skip_code;
    model.url = self.activityTopModel.url;
    model.data = self.activityTopModel.data;
    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:model];
}

- (void)noobLeftAction:(UIButton *)button
{
    JumpModel *model = [JumpModel new];
    model.skip_code = self.activityLeftModel.skip_code;
    model.url = self.activityLeftModel.url;
    model.data = self.activityLeftModel.data;
    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:model];
}

- (void)noobRightTopAction:(UIButton *)button
{
    JumpModel *model = [JumpModel new];
    model.skip_code = self.activityRightTopModel.skip_code;
    model.url = self.activityRightTopModel.url;
    model.data = self.activityRightTopModel.data;
    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:model];
}

- (void)noobRightBottomAction:(UIButton *)button
{
    JumpModel *model = [JumpModel new];
    model.skip_code = self.activityRightBottomModel.skip_code;
    model.url = self.activityRightBottomModel.url;
    model.data = self.activityRightBottomModel.data;
    [[YKJumpManager shareYKJumpManager] yk_jumpWithParamModel:model];
}

- (SDCycleScrollView *)bannerView
{
    if (!_bannerView) {
        _bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:[UIImage imageNamed:@"home_banner_placeHolder"]];
        _bannerView.backgroundColor     = WHITE_COLOR;
        _bannerView.currentPageDotImage = [UIImage imageNamed:@"home_banner_select"];
        _bannerView.pageDotImage        = [UIImage imageNamed:@"home_banner_noSelect"];
    }
    return _bannerView;
}

- (UICollectionView *)columnCollectionView
{
    if (!_columnCollectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        // item不能超过父视图，父视图圆角距上-5 原height90-5 = 85
        layout.itemSize = CGSizeMake(WIDTH_OF_SCREEN / 4, 85 * ASPECT_RATIO_WIDTH);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _columnCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _columnCollectionView.showsHorizontalScrollIndicator = NO;
        _columnCollectionView.backgroundColor = WHITE_COLOR;
        [_columnCollectionView registerNib:[UINib nibWithNibName:@"YKHomeColumnCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"YKHomeColumnCell"];
        [YKTools yk_prepareRadiusWithView:_columnCollectionView masksToBounds:YES cornerRadius:5.f borderWidth:0 borderColor:WHITE_COLOR];
    }
    return _columnCollectionView;
}

- (UIView *)activityImageView
{
    if (!_activityImageView) {
        _activityImageView = [UIView new];
        _activityImageView.backgroundColor = WHITE_COLOR;
    }
    return _activityImageView;
}

- (UIButton *)activityImageButton
{
    if (!_activityImageButton) {
        _activityImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_activityImageButton addTarget:self action:@selector(activityImageAction:) forControlEvents:UIControlEventTouchUpInside];
        [_activityImageButton setAdjustsImageWhenHighlighted:NO];
        
    }
    return _activityImageButton;
}

- (UIView *)noobActivityImageView
{
    if (!_noobActivityImageView) {
        _noobActivityImageView = [UIView new];
        _noobActivityImageView.backgroundColor = GRAY_BACKGROUND_COLOR;
        
    }
    return _noobActivityImageView;
}

- (UIButton *)noobLeftImageButton
{
    if (!_noobLeftImageButton) {
        _noobLeftImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _noobLeftImageButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [_noobLeftImageButton addTarget:self action:@selector(noobLeftAction:) forControlEvents:UIControlEventTouchUpInside];
        [_noobLeftImageButton setAdjustsImageWhenHighlighted:NO];
    }
    return _noobLeftImageButton;
}

- (UIButton *)noobRightTopImageButton
{
    if (!_noobRightTopImageButton) {
        _noobRightTopImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _noobRightTopImageButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [_noobRightTopImageButton addTarget:self action:@selector(noobRightTopAction:) forControlEvents:UIControlEventTouchUpInside];
        [_noobRightTopImageButton setAdjustsImageWhenHighlighted:NO];
    }
    return _noobRightTopImageButton;
}

- (UIButton *)noobRightBottomImageButton
{
    if (!_noobRightBottomImageButton) {
        _noobRightBottomImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _noobRightBottomImageButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [_noobRightBottomImageButton addTarget:self action:@selector(noobRightBottomAction:) forControlEvents:UIControlEventTouchUpInside];
        [_noobRightBottomImageButton setAdjustsImageWhenHighlighted:NO];

    }
    return _noobRightBottomImageButton;
}

- (NSMutableArray *)bannerModelArray
{
    if (!_bannerModelArray) {
        _bannerModelArray = [@[] mutableCopy];
    }
    return _bannerModelArray;
}

- (NSMutableArray *)columnModelArray
{
    if (!_columnModelArray) {
        _columnModelArray = [@[] mutableCopy];
    }
    return _columnModelArray;
}

@end
