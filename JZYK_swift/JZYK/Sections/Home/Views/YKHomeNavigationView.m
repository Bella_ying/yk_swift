//
//  YKHomeNavigationView.m
//  JZYK
//
//  Created by hongyu on 2018/6/6.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKHomeNavigationView.h"
#import "JWPickerView.h"

@interface YKHomeNavigationView ()

@end

@implementation YKHomeNavigationView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self prepareUI];
    }
    return self;
}

- (void)prepareUI
{
    self.backgroundColor = MAIN_THEME_COLOR;
    [self addSubview:self.navImageView];
    [self.navImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.offset(-14 * ASPECT_RATIO_WIDTH);
        make.width.mas_offset(80 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(20 * ASPECT_RATIO_WIDTH);
    }];
    [self addSubview:self.messageButton];
    [self.messageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-14 * ASPECT_RATIO_WIDTH);
        make.bottom.offset(-12 * ASPECT_RATIO_WIDTH);
        make.width.height.mas_offset(24 * ASPECT_RATIO_WIDTH);
    }];
    [self.messageButton addTarget:self action:@selector(messageButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.serviceButton];
    [self.serviceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.messageButton.mas_left).offset(-10 * ASPECT_RATIO_WIDTH);
        make.bottom.offset(-12 * ASPECT_RATIO_WIDTH);
        make.width.height.mas_offset(24 * ASPECT_RATIO_WIDTH);
    }];
    [self.serviceButton addTarget:self action:@selector(serviceButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark- 点击方法
- (void)messageButtonAction:(UIButton *)button
{
    if (self.messageBlock) {
        self.messageBlock();
    }
}

- (void)serviceButtonAction:(UIButton *)button
{
    if (self.serviceButton) {
        self.serviceBlock();
    }
}

- (void)testPicker {
    
}

- (UIImageView *)navImageView
{
    if (!_navImageView) {
        _navImageView = [[UIImageView alloc] initWithImage:YK_IMAGE(@"home_nav_title")];
        _navImageView.alpha = 0;
    }
    return _navImageView;
}

- (UIButton *)serviceButton
{
    if (!_serviceButton) {
        _serviceButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_serviceButton setImage:[UIImage imageNamed:@"home_navi_service"] forState:UIControlStateNormal];
    }
    return _serviceButton;
}

- (UIButton *)messageButton
{
    if (!_messageButton) {
        _messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_messageButton setImage:[UIImage imageNamed:@"home_navi_message"] forState:UIControlStateNormal];
    }
    return _messageButton;
}

@end
