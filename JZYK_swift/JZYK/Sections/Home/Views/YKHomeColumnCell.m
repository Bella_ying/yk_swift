//
//  YKHomeColumnCell.m
//  JZYK
//
//  Created by hongyu on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKHomeColumnCell.h"
#import "YKHomeColumnModel.h"

@interface YKHomeColumnCell ()

@property (weak, nonatomic) IBOutlet UIImageView *columnImage;
@property (weak, nonatomic) IBOutlet UILabel     *columnLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iamgeWithTop;


@end

@implementation YKHomeColumnCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.iamgeWithTop.constant = 17.5 * ASPECT_RATIO_WIDTH;
}

- (void)yk_renderModel:(YKHomeColumnModel *)model
{
    [self.columnImage sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:YK_IMAGE(@"home_item_placeHolder")];
    self.columnLabel.text = model.title;
}

@end
