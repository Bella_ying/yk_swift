//
//  YKHomeInfoManager.m
//  JZYK
//
//  Created by hongyu on 2018/6/27.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKHomeInfoManager.h"
#import "YKAccessAlertView.h"
#import "QBGestureTool.h"
#import "YKLocationManager.h"

@implementation YKHomeInfoManager

+ (instancetype)yk_sharedHomeInfoManager
{
    static dispatch_once_t onceToken;
    static YKHomeInfoManager *manager;
    dispatch_once(&onceToken, ^{
        manager = [YKHomeInfoManager new];
    });
    return manager;
}

#pragma mark- 检测更新
- (void)yk_checkoutVersion
{
    NSDictionary *iosVersion = [[ConfigManager config] configForKey:@"update_msg"];
    if (iosVersion && [iosVersion[@"has_upgrade"] integerValue] == 1) {
        NSString *compareStr = [YKTools yk_compareTheSizeWithFirstStr:iosVersion[@"new_version"] secondStr:YK_APP_VERSION];
        if ([compareStr isEqualToString:@"small"]) {
            //第一次展示 || 展示相隔一天
            if (![YK_NSUSER_DEFAULT objectForKey:@"yk_versions_update"] || [[NSDate dateNow] yk_isLaterThanDate:[[YK_NSUSER_DEFAULT objectForKey:@"yk_versions_update"] yk_dateByAddingDays:1]]) {
                [self showQBAlertWithContent:iosVersion[@"new_features"]];
            } else {
                // 校验登录态 展示手势或通知
                [self checkoutLogin];
            }
        } else {
            [self checkoutLogin];
        }
    } else {
        [self checkoutLogin];
    }
}

#pragma mark- 展示更新提示
- (void)showQBAlertWithContent:(NSString *)content
{
    // 存储更新时间
    [YK_NSUSER_DEFAULT setObject:[NSDate dateNow] forKey:@"yk_versions_update"];
    [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeUpdate contentText:content];
    [YKAccessAlertView sharedAlertManager].upDateCloseBlock = ^{
        [self checkoutLogin];
    };
}

#pragma mark- 校验是否登录
- (void)checkoutLogin
{
    if ([[YKUserManager sharedUser] yk_isLogin]) {
        if (![[YK_NSUSER_DEFAULT objectForKey:@"yk_first_gesture_show"] isEqualToString:@"yk_gesture_show"]) {
            [YK_NSUSER_DEFAULT setObject:@"yk_gesture_show" forKey:@"yk_first_gesture_show"];
            [self checkoutGesture];
        } else {
            [self checkoutNotification];
        }
        // 上报位置信息
        [self checkoutLocationStatus];
    } else {
        [self checkoutNotification];
    }
}

#pragma mark- 检测手势密码
- (void)checkoutGesture
{
    // 校验手势密码
    if (![QBGestureTool isGestureEnableByUser]) {
        [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeHandGesture contentText:@""];
        [YKAccessAlertView sharedAlertManager].openGestureBlock = ^{
            id setVc = [NSClassFromString(@"YKSettingViewController") new];
            [[YKTabBarController yk_shareTabController] yk_pushToViewController:setVc];
        };
        [YKAccessAlertView sharedAlertManager].selectedCloseGestureBlock = ^{
            [self checkoutNotification];
        };
    }
}

#pragma mark- 检测通知
- (void)checkoutNotification
{
    if ([YKTools yk_permissionsWithNotifications] == YKPermissionStatusNotDenied) {
        [self upLoadUMDataWithStatus:NO];
        [self prepareNotification];
    } else {
        [self upLoadUMDataWithStatus:YES];
        if (self.adViewBlock) {
            self.adViewBlock();
        }
    }
}

- (void)prepareNotification
{
    if (![YK_NSUSER_DEFAULT objectForKey:@"yk_system_notification"]) {
        [self showAlertNotificationView];
    } else {
        if ([[NSDate dateNow] yk_isLaterThanDate:[[YK_NSUSER_DEFAULT objectForKey:@"yk_system_notification"] yk_dateByAddingDays:3]]) {
            [self showAlertNotificationView];
        } else {
            if (self.adViewBlock) {
                self.adViewBlock();
            }
        }
    }
}

#pragma mark- 上报位置信息
- (void)checkoutLocationStatus
{
    if ([YKTools yk_permissionsWithLocation] == YKPermissionStatusAuthorized) {
        [[YKLocationManager location] yk_enableLocationServiceWithCompletion:^(CLLocation *location, AMapLocationReGeocode *regeoCode) {
            DLog(@"%@", location);
        }];
    }
}

- (void)showAlertNotificationView
{
    [YK_NSUSER_DEFAULT setObject:[NSDate dateNow] forKey:@"yk_system_notification"];
    [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypePush contentText:@"推送"];
    [YKAccessAlertView sharedAlertManager].selectedPushCloseBlock = ^{
        if (self.adViewBlock) {
            self.adViewBlock();
        }
    };
}

- (void)upLoadUMDataWithStatus:(BOOL)status
{
    // 只上报一次
    if (![[YK_NSUSER_DEFAULT objectForKey:@"main_push_status"] isEqualToString:@"save"]) {
        [YK_NSUSER_DEFAULT setObject:@"open" forKey:@"save"];
        if (status) {
            [YKClickManager yk_reportWithKey:@"main_push_status" array:@[@"开启"]];
        } else {
            [YKClickManager yk_reportWithKey:@"main_push_status" array:@[@"关闭"]];
        }
    }
}

@end
