//
//  YKHomeSectionView.h
//  JZYK
//
//  Created by hongyu on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YKHomeShopGrupModel;

@interface YKHomeSectionView : UIView

- (void)renderModel:(YKHomeShopGrupModel *)model;

@end
