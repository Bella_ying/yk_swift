//
//  YKHomeHeaderView.h
//  JZYK
//
//  Created by hongyu on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface YKHomeHeaderView : UIView

- (void)yk_renderBannerModels:(NSArray *)models;
- (void)yk_renderColumnModels:(NSArray *)models;
- (void)yk_renderActivityModels:(NSArray *)models;
- (void)yk_renderNoobModels:(NSArray *)models;

@end
