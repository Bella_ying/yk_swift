//
//  YKMineHeadView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKMineAmountView.h"
#import "YKMineMenuView.h"
#import "YKMineHeadCardModel.h"

static NSString *kEventMyAmount = @"kEventMyAmount";
static NSString *kEventIntoAmountStatusManager = @"kEventIntoAmountStatusManager";
@interface YKMineHeadView : UIView
@property (nonatomic, strong) YKMineHeadCardModel *headCardModel;
@property (nonatomic, strong) NSMutableArray *menuArr;
//redPointStatus 0 为正常 1 为逾期红色日期
- (void)refreshDataWithAmountCanPop:(NSString *)amount orderDate:(NSString *)orderDate orderMoney:(NSString *)orderMoney redPointStatus:(NSInteger)status;
@end
