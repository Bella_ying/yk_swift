//
//  YKMineAmountView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMineAmountView.h"

@interface YKMineAmountView ()

@property (weak, nonatomic) IBOutlet UIView *firstItemBackView;
@property (weak, nonatomic) IBOutlet UIView *secondItembackView;

@property (weak, nonatomic) IBOutlet UILabel *accountNumTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *billTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *billAmountLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondItemBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondItemLabelMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewHeightConstraint;

@end

@implementation YKMineAmountView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

- (void)setAmountCanPop:(NSString *)amount orderDate:(NSString *)date orderMoney:(NSString *)orderMoney redPointStatus:(NSInteger)status{
    
    self.accountNumLabel.text = amount;
    NSString *titleRepayStr = [NSString stringWithFormat:@"%@ %@",@"待还账单",date?:@""];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:titleRepayStr];
    if (status == 1) {
        [attStr addAttribute:NSForegroundColorAttributeName value:[UIColor yk_colorWithHexString:@"#E96763"] range:NSMakeRange(4, titleRepayStr.length - 4)];
    }else{
        [attStr addAttribute:NSForegroundColorAttributeName value:[UIColor yk_colorWithHexString:@"#ADADAD"] range:NSMakeRange(4, titleRepayStr.length - 4)];
    }

    self.billTitleLabel.attributedText = attStr;
    
    self.billAmountLabel.text = orderMoney;
}

- (void)setup{
    self.bottomConstraint.constant = 20 * ASPECT_RATIO_WIDTH;
    self.labelMarginConstraint.constant = 5 * ASPECT_RATIO_WIDTH;
    self.secondItemBottomConstraint.constant = 20 * ASPECT_RATIO_WIDTH;
    self.secondItemLabelMarginConstraint.constant = 5 * ASPECT_RATIO_WIDTH;
    self.lineViewHeightConstraint.constant = 35 * ASPECT_RATIO_WIDTH;
    
    self.accountNumTitleLabel.font = [UIFont systemFontOfSize:adaptFontSize(12)];
    self.billTitleLabel.font = [UIFont systemFontOfSize:adaptFontSize(12)];
    if (@available(iOS 8.2, *)) {
        self.accountNumLabel.font = [UIFont systemFontOfSize:adaptFontSize(18) weight:(UIFontWeightMedium)];
        self.billAmountLabel.font = [UIFont systemFontOfSize:adaptFontSize(18) weight:(UIFontWeightMedium)];
    } else {
        self.accountNumLabel.font = [UIFont systemFontOfSize:adaptFontSize(18)];
        self.billAmountLabel.font = [UIFont systemFontOfSize:adaptFontSize(18)];
    }
    
    UIGestureRecognizer *tapG1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickfirstItem)];
    [self.firstItemBackView addGestureRecognizer:tapG1];
    
    UIGestureRecognizer *tapG2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSecondItem)];
    [self.secondItembackView addGestureRecognizer:tapG2];

}

- (void)clickfirstItem{
    DLog(@"clickfirstItem");
    [self routerEventWithName:kEventAccountRemanining userInfo:nil];
    
}
- (void)clickSecondItem{
    DLog(@"clickSecondItem");
    [self routerEventWithName:kEventBillNeedRepay userInfo:nil];
    
}

@end
