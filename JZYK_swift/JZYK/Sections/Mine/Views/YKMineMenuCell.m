//
//  YKMineMenuCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMineMenuCell.h"
#import "DFImageLabel.h"

#define kTitleFont 13
#define kImageViewWidth 23
#define kTitleImageMargin 8
#define kItemHeight 60

@interface YKMineMenuCell ()

@property (nonatomic, strong) DFImageLabel *itemControl;

@end

@implementation YKMineMenuCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self =[super initWithFrame:frame]) {
        [self setup];
        
    }
    return self;
}

- (void)setup{
    [self.contentView addSubview:self.itemControl];
    [self.itemControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
    }];
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    
}

- (void)setModel:(YKMineMenuModel *)model
{
    _model = model;
    self.itemControl.label.text = model.text?:@"---";
    [self.itemControl.imageView sd_setImageWithURL:[NSURL URLWithString:model.image]];
}
////手动计算宽度
//+ (CGSize)itemSizeOfModel:(YKMineMenuModel *)model{
//    CGFloat width = 0;
//    CGSize size = CGSizeMake(0, 0);
//    if (model && model.title) {
//        size = [model.title sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:adaptFontSize(14)]}];
//    }
//
//    width += MAX(kImageViewWidth, size.width);
//
//    return CGSizeMake(width, kItemHeight);
//}

- (DFImageLabel *)itemControl
{
    if (!_itemControl) {
        
        _itemControl = [[DFImageLabel alloc] initWithImageLabelArrangeStyle:(DFImageLArrangeStyleUpDown) imageViewSize:CGSizeMake(25 * ASPECT_RATIO_WIDTH, 25 * ASPECT_RATIO_WIDTH)];
        _itemControl.label.text = @"----";
        _itemControl.label.textColor = [UIColor yk_colorWithHexString:@"#666666"];
        _itemControl.label.font = [UIFont systemFontOfSize:adaptFontSize(14)];
        _itemControl.imageLabelMargin = kTitleImageMargin * ASPECT_RATIO_WIDTH;
        _itemControl.userInteractionEnabled = NO;
        //        @weakify(self);
        //        _itemControl.clickBlock = ^(DFImageLabel *control) {
        //            @strongify(self);
        ////            [self df_routerEvent:kEventLoansKindItemAction userInfo:self.model];
        //        };
        
    }
    return _itemControl;
}
@end
