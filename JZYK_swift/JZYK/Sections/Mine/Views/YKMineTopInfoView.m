//
//  YKMineTopInfoView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMineTopInfoView.h"
#import "DFImageLabel.h"
#import "YKPaoMaNoticeView.h"

@interface YKMineTopInfoView ()

@property (nonatomic, strong) UIView *topBackView;
@property (nonatomic, strong) DFImageLabel *imageL;
@property (nonatomic, strong) UIButton *settingBtn;

//@property (nonatomic, strong) UIView *tipView;
@property (nonatomic, strong) YKPaoMaNoticeView *noticeView;

@end

@implementation YKMineTopInfoView

- (instancetype)init{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (void)yk_setPhoneStr:(NSString *)phoneStr isLogin:(BOOL)isLogin{
    if (isLogin) {
        self.imageL.imageView.image = [UIImage imageNamed:@"mine_normal_icon"];
    }else{
        self.imageL.imageView.image = [UIImage imageNamed:@"mine_disable_icon"];
    }
    self.imageL.label.text = phoneStr;
}


- (void)setup{
    [self addSubview:self.topBackView];
//    [self addSubview:self.tipView];
//    [self addSubview:self.noticeView];
    [self.topBackView addSubview:self.imageL];
    [self.topBackView addSubview:self.settingBtn];
    
    [self.topBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.right.equalTo(self);
        make.height.mas_greaterThanOrEqualTo(64);
        make.bottom.equalTo(self);
    }];

    self.imageL.imageView.image = [UIImage imageNamed:@"mine_disable_icon"]; //mine_normal_icon
    self.imageL.label.text = @"152****2345";
    [self.imageL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-6);
        make.left.equalTo(@15);
    }];
    WEAK_SELF
    self.imageL.clickBlock = ^(DFImageLabel *control) {
//        NSLog(@"点击哦图像");
        [weakSelf routerEventWithName:kEventLoginIcon userInfo:nil];
        
    };
    
    [self.settingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.imageL);
        make.right.mas_equalTo(-10);
        make.width.height.mas_equalTo(35);
    }];
    [self.settingBtn addTarget:self action:@selector(clickSettingBtn) forControlEvents:(UIControlEventTouchUpInside)];
    
//    //tip提醒
//    [self.noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.topBackView.mas_bottom);
//        make.left.right.equalTo(self);
//        make.height.mas_equalTo(30);
//        make.bottom.equalTo(self);
//    }];
//    self.noticeView.anouncementInfoBlk = ^(NSString *pageInfoURL) {
//        DLog(@"公告详情");
//    };
//    self.noticeView.clickCloseBLk = ^(UIView *view) {
//        CGFloat heightG = weakSelf.bounds.size.height;
//        [weakSelf.noticeView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(0);
//        }];
//        [weakSelf mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(heightG - 30);
//        }];
//    };

}

- (void)clickSettingBtn{
    DLog(@"点击设置");
    [self routerEventWithName:kEventSetting userInfo:nil];
}

- (UIView *)topBackView
{
    if (!_topBackView) {
        _topBackView = [[UIView alloc] init];
        _topBackView.backgroundColor = WHITE_COLOR;
    }
    return _topBackView;
}

- (DFImageLabel *)imageL
{
    if (!_imageL) {
        _imageL = [[DFImageLabel alloc] initWithImageLabelArrangeStyle:DFImageLArrangeStyleLeftRight imageViewSize:CGSizeMake(30, 30)];
        _imageL.imageView.layer.cornerRadius = 15;
        _imageL.imageView.layer.masksToBounds = YES;
        _imageL.label.font = [UIFont systemFontOfSize:adaptFontSize(18)];
        _imageL.label.textColor = [UIColor yk_colorWithHexString:@"#454545"];
        _imageL.imageLabelMargin = 10;
    }
    return _imageL;
}

- (UIButton *)settingBtn{
    if (!_settingBtn) {
        _settingBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_settingBtn setImage:[UIImage imageNamed:@"mine_setting"] forState:(UIControlStateNormal)];
    }
    return _settingBtn;
}

//- (UIView *)tipView
//{
//    if (!_tipView) {
//        _tipView = [[UIView alloc] init];
//    }
//    return _tipView;
//}

//- (YKPaoMaNoticeView *)noticeView {
//    if (!_noticeView) {
//        _noticeView = [[YKPaoMaNoticeView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 30) pageType:PageTypeMine];
//        _noticeView.backgroundColor = WHITE_COLOR;
//    }
//    return _noticeView;
//}


@end
