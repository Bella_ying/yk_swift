//
//  YKMineTopInfoView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kEventSetting = @"kEventSetting";
static NSString *kEventLoginIcon = @"kEventLoginIcon";
@interface YKMineTopInfoView : UIView

- (void)yk_setPhoneStr:(NSString *)phoneStr isLogin:(BOOL)isLogin;

@end
