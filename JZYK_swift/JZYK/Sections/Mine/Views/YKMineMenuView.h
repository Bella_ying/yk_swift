//
//  YKMineMenuView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
static NSString *kEventMenuFirstItem = @"kEventMenuFirstItem";
static NSString *kEventMenuSecondItem = @"kEventMenuSecondItem";
static NSString *kEventMenuThirdItem = @"kEventMenuThirdItem";
static NSString *kEventMenuForthItem = @"kEventMenuForthItem";
static NSString *kEventMenuItem = @"kEventMenuItem";

static NSString *kItemModelKey = @"itemModel";

@interface YKMineMenuView : UIView

@property (nonatomic, strong) NSMutableArray *itemsArr;

@end
