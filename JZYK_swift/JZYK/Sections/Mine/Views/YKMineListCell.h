//
//  YKMineListCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKMineMenuModel.h"
@interface YKMineListCell : UITableViewCell
@property (nonatomic, strong) YKMineListModel *listModel;

@end
