//
//  YKMineAmountView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kEventAccountRemanining = @"kEventAccountRemanining";
static NSString *kEventBillNeedRepay = @"kEventBillNeedRepay";
@interface YKMineAmountView : UIView

- (void)setAmountCanPop:(NSString *)amount orderDate:(NSString *)date orderMoney:(NSString *)orderMoney redPointStatus:(NSInteger)status;

@end
