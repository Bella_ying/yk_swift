//
//  YKAccountNavView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAccountNavView.h"

@interface YKAccountNavView ()



@end

@implementation YKAccountNavView

- (instancetype)initCustomStyle:(YKRightItemStyle)style{
    
    if (self = [super init]){
        [self prepareUIWithStyle:style];
    }
    return self;
}

- (void)prepareUIWithStyle:(YKRightItemStyle)style
{
    self.backgroundColor = MAIN_THEME_COLOR;
    [self addSubview:self.navTitleLabel];
    [self.navTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.offset(-10);
        make.width.mas_offset(100 * ASPECT_RATIO_WIDTH);
        make.height.mas_offset(24);
    }];
    [self addSubview:self.backBtn];
    self.backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(14 * ASPECT_RATIO_WIDTH);
        make.bottom.offset(-12);
        make.width.mas_equalTo(30);
    }];
    [self.backBtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.rightBtn];
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-14 * ASPECT_RATIO_WIDTH);
        make.bottom.offset(-5);
    }];
    [self.rightBtn addTarget:self action:@selector(drawRecordBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (style == YKRightItemStyleAmount){ //额度
        [self.rightBtn setTitle:@"额度说明" forState:(UIControlStateNormal)];
    }else if (style == YKRightItemStyleAccountRemain){ //账户余额
        [self.rightBtn setTitle:@"提现记录" forState:(UIControlStateNormal)];
    }else if (style == YKRightItemStyleBillNeedRepay){ //待还账单
        UIImage *image = [UIImage imageNamed:@"home_navi_service"];
        [self.rightBtn setImage:image forState:(UIControlStateNormal)];
        self.rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [self.rightBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(image.size.width + 16);
            make.bottom.offset(-10);
        }];
    }
}

#pragma mark- 点击方法
- (void)backButtonAction:(UIButton *)button
{
    if (self.yk_backBlock) {
        self.yk_backBlock();
    }
}

- (void)drawRecordBtnAction:(UIButton *)button
{
    if (self.yk_rightBlock) {
        self.yk_rightBlock();
    }
}

- (UILabel *)navTitleLabel
{
    if (!_navTitleLabel) {
        _navTitleLabel = [UILabel new];
        _navTitleLabel.textColor = WHITE_COLOR;
        _navTitleLabel.font = [UIFont systemFontOfSize:18];
        _navTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _navTitleLabel;
}

- (UIButton *)backBtn
{
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [UIImage imageNamed:@"system_back_white"];
        [_backBtn setImage:image forState:UIControlStateNormal];
    }
    return _backBtn;
}

- (UIButton *)rightBtn
{
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn setTitle:@"" forState:(UIControlStateNormal)];
        _rightBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    }
    return _rightBtn;
}

@end
