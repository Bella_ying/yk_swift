//
//  YKAccountRemainView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//  账户余额和待还账单的head是共用的

#import <UIKit/UIKit.h>
//  账户余额和待还账单的head是共用的
@interface YKAccountRemainView : UIView

@property (nonatomic, strong) UILabel *availableTitleLabel;
@property (nonatomic, strong) UILabel *availableNumLabel;

@end
