//
//  YKAccountRemainCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKMineAccountRemainModel.h"

@interface YKAccountRemainCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (nonatomic, strong) YKMineAccountRemainModel *accountRemainListModel;

@end
