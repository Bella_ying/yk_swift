//
//  YKDrawMoneyCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kEventInstallmentBtn = @"kEventInstallmentBtn"; //额度分期
static NSString *kEventActivityBtn = @"kEventActivityBtn"; //活动奖励
@interface YKDrawMoneyCell : UITableViewCell

@end
