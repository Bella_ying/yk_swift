//
//  YKDrawMoneyCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKDrawMoneyCell.h"

@interface YKDrawMoneyCell () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *installmentBtn; //额度分期
@property (weak, nonatomic) IBOutlet UIButton *activityBtn; //活动奖励
@property (weak, nonatomic) IBOutlet UITextField *drawAmountTextF; //提现金额
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *selectBtnCollection;

@end

@implementation YKDrawMoneyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.selectBtnCollection enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *btn = (UIButton *)obj;
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 3);
        NSString *str1 = @"额度分期";
        CGFloat sizeW1 =  [str1 yk_sizeWithFontSize:14].width;
        [btn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(sizeW1 + 10);
        }];
        btn.layer.cornerRadius = 5;
        btn.layer.masksToBounds = YES;
        btn.layer.borderWidth = 1;
        btn.layer.borderColor = GRAY_BACKGROUND_COLOR.CGColor;
        [btn setTitleColor:WHITE_COLOR forState:(UIControlStateSelected)];
        [btn setTitleColor:LABEL_TEXT_COLOR forState:(UIControlStateNormal)];
    }];
    
    [self.installmentBtn setBackgroundColor:MAIN_THEME_COLOR];
    self.installmentBtn.selected = YES;
    self.activityBtn.selected = NO;
    [self.activityBtn setBackgroundColor:[UIColor clearColor]];
    
    
    //textField
    self.drawAmountTextF.tintColor = [UIColor clearColor];
    self.drawAmountTextF.text = @"0.00";
    self.drawAmountTextF.font = [UIFont boldSystemFontOfSize:28];
    self.drawAmountTextF.delegate = self;
    NSString *holderText = @"请输入";
    NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
    [placeholder addAttribute:NSForegroundColorAttributeName
                        value:LINE_COLOR
                        range:NSMakeRange(0, holderText.length)];
    [placeholder addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:16]
                        range:NSMakeRange(0, holderText.length)];
    self.drawAmountTextF.attributedPlaceholder = placeholder;
    
}
- (IBAction)installmentBtnAction:(id)sender {
    self.installmentBtn.selected = YES;
    [self.installmentBtn setBackgroundColor:MAIN_THEME_COLOR];
    self.activityBtn.selected = NO;
    [self.activityBtn setBackgroundColor:[UIColor clearColor]];
    self.drawAmountTextF.tintColor = [UIColor clearColor];
    self.drawAmountTextF.text = @"0.00";
    
    [self routerEventWithName:kEventInstallmentBtn userInfo:nil];
    
}
- (IBAction)activityBtnAction:(id)sender {
    self.installmentBtn.selected = NO;
    [self.installmentBtn setBackgroundColor:[UIColor clearColor]];
    self.activityBtn.selected = YES;
    [self.activityBtn setBackgroundColor:MAIN_THEME_COLOR];
    self.drawAmountTextF.tintColor = MAIN_THEME_COLOR;
    self.drawAmountTextF.text = nil;

     [self routerEventWithName:kEventActivityBtn userInfo:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789.\b"];
    if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
        return NO;
    }
    
    NSString *result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([result length] == 0) return YES; // Allow delete all character which are entered.
    NSMutableString * futureString = [NSMutableString stringWithString:textField.text];
    [futureString  insertString:string atIndex:range.location];
    NSInteger flag=0;
    const NSInteger limited = 2;//小数点后需要限制的个数
    for (int i = futureString.length-1; i>=0; i--) {
        if ([futureString characterAtIndex:i] == '.') {
            if (flag > limited) {
                return NO;
            }
            break;
        }
        flag++;
    }
    
    BOOL resultBool = NO;
    int pointSimpleNum = 0;
    //判断如果有两个小数点则错误
    for (int i = futureString.length-1; i>=0; i--) {
        if ([futureString characterAtIndex:i] == '.') {
            pointSimpleNum++;
        }
    }
    if(pointSimpleNum>=2){
        return NO;
    }
    
    //转换成double进行判断是否小于999999，如果大于999999false
    if(result.doubleValue >= 999999){
        return NO;
    }
    resultBool = YES;
    return resultBool;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
