//
//  YKAccountRemainView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//  账户余额和待还账单的head是共用的

#import "YKAccountRemainView.h"

@interface YKAccountRemainView ()
@property (nonatomic, strong) UIImageView *imageView;



@end

@implementation YKAccountRemainView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)setup{
    
    [self addSubview:self.imageView];
    [self.imageView addSubview:self.availableTitleLabel];
    [self.imageView addSubview:self.availableNumLabel];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.availableNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.imageView);
        make.bottom.equalTo(self.imageView.mas_bottom).offset(-48 * ASPECT_RATIO_WIDTH);
        make.height.mas_equalTo(35 * ASPECT_RATIO_WIDTH); //设置高度，为了数据为空的时候，title位置不向下
    }];
    [self.availableTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.imageView);
        make.left.mas_greaterThanOrEqualTo(15);
        make.right.mas_greaterThanOrEqualTo(-15);
        make.bottom.equalTo(self.availableNumLabel.mas_top).offset(-3 * ASPECT_RATIO_WIDTH);
    }];
}

- (UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.image = [UIImage imageNamed:@"bg_oragne"];
    }
    return _imageView;
}

- (UILabel *)availableTitleLabel
{
    if (!_availableTitleLabel) {
        _availableTitleLabel = [[UILabel alloc] init];
        _availableTitleLabel.textColor = WHITE_COLOR;
        _availableTitleLabel.font = [UIFont systemFontOfSize:13];
        _availableTitleLabel.text = @"";
        _availableTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _availableTitleLabel;
}

- (UILabel *)availableNumLabel
{
    if (!_availableNumLabel) {
        _availableNumLabel = [[UILabel alloc] init];
        _availableNumLabel.textColor = WHITE_COLOR;
        _availableNumLabel.font = [UIFont systemFontOfSize:35];
        _availableNumLabel.text = @"";
    }
    return _availableNumLabel;
}

@end
