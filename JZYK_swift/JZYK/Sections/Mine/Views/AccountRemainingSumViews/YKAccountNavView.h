//
//  YKAccountNavView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, YKRightItemStyle) {
    YKRightItemStyleAmount, //我的额度
    YKRightItemStyleAccountRemain, //账户余额
    YKRightItemStyleBillNeedRepay, //待还账单
};

typedef void (^YKAccountNavigationBackBlock)(void);
typedef void (^YKAccountNavigationRightBlock)(void);

@interface YKAccountNavView : UIView

@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, strong) UILabel *navTitleLabel;
@property (nonatomic, copy) YKAccountNavigationBackBlock yk_backBlock;
@property (nonatomic, copy) YKAccountNavigationRightBlock yk_rightBlock;

- (instancetype)initCustomStyle:(YKRightItemStyle)style;

@end
