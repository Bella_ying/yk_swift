//
//  YKAccountRemainCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAccountRemainCell.h"
#import "DFImageLabel.h"

@interface YKAccountRemainCell ()
@property (weak, nonatomic) IBOutlet UILabel *mainTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
//@property (nonatomic, strong) DFImageLabel *remainTimeControl;
@property (weak, nonatomic) IBOutlet UILabel *remainTimeLabel;

@end

@implementation YKAccountRemainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
//    self.remainTimeLabel.text = @" 剩余11小时38分钟 ";
    self.remainTimeLabel.layer.cornerRadius = 3;
    self.remainTimeLabel.layer.masksToBounds = YES;
    
//    [self.contentView addSubview:self.remainTimeControl];
//    [self.remainTimeControl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mainTitleLabel.mas_right).offset(12);
//        make.centerY.equalTo(self.mainTitleLabel);
//    }];
//
//    self.remainTimeControl.label.textColor = WHITE_COLOR;
//    self.remainTimeControl.label.font = [UIFont systemFontOfSize:12];
//    self.remainTimeControl.label.text = @"剩余11小时38分钟";
//    self.remainTimeControl.backgroundColor = MAIN_THEME_COLOR;
}

- (void)setAccountRemainListModel:(YKMineAccountRemainModel *)accountRemainListModel
{
    _accountRemainListModel = accountRemainListModel;
    self.mainTitleLabel.text = accountRemainListModel.type_name?:@"";
    self.tipTextLabel.text = accountRemainListModel.desc?:@"";
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2lf",[accountRemainListModel.amount floatValue]];
    if (![accountRemainListModel.tag yk_isEmpty]) {
        self.remainTimeLabel.text = [NSString stringWithFormat:@" %@ ",accountRemainListModel.tag];
    }else{
        self.remainTimeLabel.text = @"";
    }
}

//- (DFImageLabel *)remainTimeControl
//{
//    if (!_remainTimeControl) {
//        _remainTimeControl = [[DFImageLabel alloc] initWithImageLabelArrangeStyle:DFImageLArrangeStyleLeftRight leftMargin:10 rightMargin:10];
//        _remainTimeControl.layer.cornerRadius = 3;
//        _remainTimeControl.layer.masksToBounds = YES;
//    }
//    return _remainTimeControl;
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
