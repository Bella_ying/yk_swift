//
//  YKWithDrawRecordListCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKWithDrawRecordModel.h"
#import "YKWithDrawRecordActivityModel.h"
@interface YKWithDrawRecordListCell : UITableViewCell

@property (nonatomic, strong) YKWithDrawRecordModel *withDrawModel;
@property (nonatomic, strong) YKWithDrawRecordActivityModel *withDrawActivityModel;

@end
