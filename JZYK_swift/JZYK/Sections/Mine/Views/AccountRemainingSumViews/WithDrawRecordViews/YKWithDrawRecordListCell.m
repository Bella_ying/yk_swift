//
//  YKWithDrawRecordListCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKWithDrawRecordListCell.h"

@interface YKWithDrawRecordListCell ()
@property (weak, nonatomic) IBOutlet UILabel *bankCardLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@end

@implementation YKWithDrawRecordListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setWithDrawModel:(YKWithDrawRecordModel *)withDrawModel
{
    _withDrawModel = withDrawModel;
    self.bankCardLabel.text = withDrawModel.title?:@"";
    self.dateLabel.text = withDrawModel.withdraw_time?:@"";
    self.amountLabel.text = withDrawModel.amount?:@"";
    self.stateLabel.attributedText = [NSString yk_addAttributeWithHtml5String:withDrawModel.desc?:@""];
}

- (void)setWithDrawActivityModel:(YKWithDrawRecordActivityModel *)withDrawActivityModel
{
    _withDrawActivityModel = withDrawActivityModel;
    self.bankCardLabel.text = [NSString stringWithFormat:@"%@%@",withDrawActivityModel.bank_name?:@"",withDrawActivityModel.card_no?[NSString stringWithFormat:@"（%@）",withDrawActivityModel.card_no]:@""];
    self.dateLabel.text = withDrawActivityModel.created_at?:@"";
    self.amountLabel.text = withDrawActivityModel.amount?:@"";
    self.stateLabel.attributedText = [NSString yk_addAttributeWithHtml5String:withDrawActivityModel.status_name?:@""];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
