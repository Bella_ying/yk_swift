//
//  YKOrderListCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKOrderListCell.h"

@interface YKOrderListCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *subTitleL;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *countDownLabel; //剩余取消时间的label

@end

@implementation YKOrderListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //取消借款只点击这文案部分区域，此时详情不能点
    UIGestureRecognizer *tapG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickStatus)];
    [self.stateLabel addGestureRecognizer:tapG];
    self.stateLabel.userInteractionEnabled = YES;
    
    self.titleL.numberOfLines = 2;
    
    
}

- (void)setOrderModel:(YKOrderListModel *)orderModel
{
    _orderModel = orderModel;
    [self.imageV sd_setImageWithURL:[NSURL URLWithString:orderModel.pic_url] placeholderImage:[UIImage imageNamed:@"mine_order_default"]];
    self.titleL.text = orderModel.title?:@"";
    self.subTitleL.text = [NSString stringWithFormat:@"￥ %@ × %@期",orderModel.money,orderModel.period];
    self.dateLabel.text = orderModel.order_time?:@"";
    if (orderModel.can_cancel) {
        self.countDownLabel.attributedText = [NSString yk_addAttributeWithHtml5String:orderModel.count_down];
    }else{
        self.countDownLabel.attributedText = nil;
    }
    
    self.stateLabel.attributedText = [NSString yk_addAttributeWithHtml5String:orderModel.status];
}

- (void)clickStatus{
    if (self.orderModel.can_cancel) {
        DLog(@"取消借款");
        [self routerEventWithName:kEventOrderCancel userInfo:self.orderModel];
    }else{
        DLog(@"不能取消");
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
