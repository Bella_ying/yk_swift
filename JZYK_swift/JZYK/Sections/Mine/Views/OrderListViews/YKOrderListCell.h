//
//  YKOrderListCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKOrderListModel.h"

static NSString *kEventOrderCancel = @"kEventOrderCancel";
@interface YKOrderListCell : UITableViewCell
@property (nonatomic, strong) YKOrderListModel *orderModel;
@end
