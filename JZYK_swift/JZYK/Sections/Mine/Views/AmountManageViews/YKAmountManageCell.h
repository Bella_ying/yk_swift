//
//  YKAmountManageCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKMyAmountModel.h"
@interface YKAmountManageCell : UITableViewCell

@property (nonatomic, strong) YKIncreasedLimitListModel *listModel;

@end
