//
//  YKAmountManageHeadView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKAmountManageHeadView : UIView

- (void)yk_setTotal_quota:(NSString *)total_quota total_incre:(NSString *)total_incre temp_quota:(NSString *)temp_quota;

@end
