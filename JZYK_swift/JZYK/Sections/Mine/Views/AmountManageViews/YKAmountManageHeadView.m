//
//  YKAmountManageHeadView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAmountManageHeadView.h"

@interface YKAmountManageHeadView ()
@property (weak, nonatomic) IBOutlet UIView *amountFirstItemBackView; //累计额度提升的背景
@property (weak, nonatomic) IBOutlet UIView *amountSecondItemBackView; //临时额度的背景

@property (weak, nonatomic) IBOutlet UILabel *amountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountPromoteNumLabel; //累计额度提升
@property (weak, nonatomic) IBOutlet UILabel *temporaryAmountNumLabel; //临时额度
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tipLabelMarginAmountConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tipLabelMarginBottomContraint;

@end

@implementation YKAmountManageHeadView

- (void)awakeFromNib{
    [super awakeFromNib];
    //防止iphone5，上面文字重叠
    if (IS_iPhone5) {
        self.tipLabelMarginAmountConstraint.constant = 6;
        self.tipLabelMarginBottomContraint.constant = 5;
    }
    
}

- (void)yk_setTotal_quota:(NSString *)total_quota total_incre:(NSString *)total_incre temp_quota:(NSString *)temp_quota{
    self.amountNumLabel.text = total_quota?[NSString yk_numberToCommaSymbolformat:total_quota]:@"---";
    self.amountPromoteNumLabel.text = total_incre?:@"---";
    self.temporaryAmountNumLabel.text = temp_quota?:@"---";
    self.tipLabel.text = @"点滴额度积累不易 请珍惜您的信用";
}


@end
