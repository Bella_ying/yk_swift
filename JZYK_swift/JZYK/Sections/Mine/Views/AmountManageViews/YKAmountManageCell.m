//
//  YKAmountManageCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAmountManageCell.h"

@interface YKAmountManageCell ()
@property (weak, nonatomic) IBOutlet UILabel *monthLabel; //月份
@property (weak, nonatomic) IBOutlet UILabel *dayLabel; //日
@property (weak, nonatomic) IBOutlet UILabel *mainTitleLabel; //title
@property (weak, nonatomic) IBOutlet UILabel *subTipLabel; //subTitle
@property (weak, nonatomic) IBOutlet UILabel *subAmountlabel; //金额

@end

@implementation YKAmountManageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setListModel:(YKIncreasedLimitListModel *)listModel
{
    _listModel = listModel;
    self.monthLabel.text = [NSString stringWithFormat:@"%@月",listModel.date_month?:@""];
    self.dayLabel.text = [NSString stringWithFormat:@"%@",listModel.date_day?:@""];
    self.mainTitleLabel.text = listModel.title;
    self.subTipLabel.text = listModel.desc;
    if ([listModel.symbol isEqualToString:@"+"]) {
        self.subAmountlabel.textColor = MAIN_THEME_COLOR;
    }else{
        self.subAmountlabel.textColor = GRAY_COLOR_8D;
    }
    self.subAmountlabel.text = [NSString stringWithFormat:@"%@%@",listModel.symbol,listModel.amount];
//    self.subAmountlabel.attributedText = [NSString yk_addAttributeWithHtml5String:listModel.amount];
//    self.symbolLabel.attributedText = [NSString yk_addAttributeWithHtml5String:listModel.symbol];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
