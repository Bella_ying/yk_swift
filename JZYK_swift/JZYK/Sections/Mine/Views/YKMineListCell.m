//
//  YKMineListCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMineListCell.h"

@interface YKMineListCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *subTitleL;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrow;

@end

@implementation YKMineListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setListModel:(YKMineListModel *)listModel
{
    _listModel = listModel;
    self.titleL.text = listModel.text?:@"";
    self.subTitleL.text = listModel.textAlias?:@"";
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
