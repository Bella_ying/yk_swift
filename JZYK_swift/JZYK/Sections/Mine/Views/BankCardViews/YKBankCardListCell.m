//
//  YKBankCardListCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBankCardListCell.h"

#define kCardSideMargin 0
#define kContentSideMargin 35
@interface YKBankCardListCell ()
@property (nonatomic, strong) UIImageView *cardImageView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *cardNameLabel;
@property (nonatomic, strong) UILabel *cardSubNameLabel;
@property (nonatomic, strong) UILabel *cardRightLabel;
@property (nonatomic, strong) UIButton *cardRightBtn;
@property (nonatomic, strong) UILabel *cardNumLabel;

@end



@implementation YKBankCardListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setup];
    }
    return self;
}


- (void)setup{
    
    [self.contentView addSubview:self.cardImageView];
    [self.cardImageView addSubview:self.iconImageView];
    [self.cardImageView addSubview:self.cardNameLabel];
    [self.cardImageView addSubview:self.cardSubNameLabel];
    [self.cardImageView addSubview:self.cardNumLabel];
    [self.cardImageView addSubview:self.cardRightLabel];
    [self.cardImageView addSubview:self.cardRightBtn];
    
    [self.cardImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20 * ASPECT_RATIO_WIDTH);
        make.left.mas_equalTo(kCardSideMargin * ASPECT_RATIO_WIDTH);
        make.right.mas_equalTo(-kCardSideMargin * ASPECT_RATIO_WIDTH);
        make.height.mas_equalTo(126 * ASPECT_RATIO_WIDTH);
    }];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15 * ASPECT_RATIO_WIDTH);
        make.left.mas_equalTo(kContentSideMargin * ASPECT_RATIO_WIDTH);
        make.width.height.mas_equalTo(50 * ASPECT_RATIO_WIDTH);
    }];
    [self.cardNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(22 * ASPECT_RATIO_WIDTH);
        make.left.equalTo(self.iconImageView.mas_right).offset(10 * ASPECT_RATIO_WIDTH);
    }];
    [self.cardSubNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.cardNameLabel.mas_bottom).offset(5 * ASPECT_RATIO_WIDTH);
        make.left.equalTo(self.cardNameLabel);
    }];
    [self.cardNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-25 * ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(self.cardImageView);
    }];
    [self.cardRightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.cardNameLabel);
        make.right.mas_equalTo(-kContentSideMargin * ASPECT_RATIO_WIDTH);
    }];
    [self.cardRightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.cardNameLabel);
        make.right.mas_equalTo(-kContentSideMargin * ASPECT_RATIO_WIDTH);
    }];
    
//    [self.cardImageView setImage:[UIImage imageNamed:@"bankCard_bgImage"]];

    [self.cardRightBtn addTarget:self action:@selector(clickRightWarnBtn) forControlEvents:(UIControlEventTouchUpInside)];
   
}

- (void)setBankCardModel:(YKBankCardModel *)bankCardModel
{
    _bankCardModel = bankCardModel;
    [self.cardImageView sd_setImageWithURL:[NSURL URLWithString:bankCardModel.bank_background] placeholderImage:[UIImage imageNamed:@"bankCard_bgImage"]];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:bankCardModel.bank_logo] placeholderImage:nil];
    self.cardNameLabel.text = bankCardModel.bank_name;
    self.cardSubNameLabel.text = bankCardModel.card_type;
    self.cardNumLabel.text = bankCardModel.card_no;
    if (bankCardModel.tag == 1) {
        //显示小图标，点击提示信息tag_msg
        self.cardRightBtn.hidden = NO;
    }else{
        self.cardRightBtn.hidden = YES;
        if (bankCardModel.is_main_card) {
            self.cardRightLabel.text = @"主卡";
        }else{
            self.cardRightLabel.text = @"";
        }
    }
}

- (void)clickRightWarnBtn{
    DLog(@"展示tag_msg");
    [self routerEventWithName:kEventWarningAction userInfo:self.bankCardModel];
}

- (UIImageView *)cardImageView
{
    if (!_cardImageView) {
        _cardImageView = [[UIImageView alloc] init];
    }
    return _cardImageView;
}
- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
    }
    return _iconImageView;
}
- (UILabel *)cardNameLabel
{
    if (!_cardNameLabel) {
        _cardNameLabel = [[UILabel alloc] init];
        _cardNameLabel.font = [UIFont systemFontOfSize:18];
        _cardNameLabel.textColor = WHITE_COLOR;
    }
    return _cardNameLabel;
}
- (UILabel *)cardSubNameLabel
{
    if (!_cardSubNameLabel) {
        _cardSubNameLabel = [[UILabel alloc] init];
        _cardSubNameLabel.font = [UIFont systemFontOfSize:14];
        _cardSubNameLabel.textColor = WHITE_COLOR;
        
    }
    return _cardSubNameLabel;
}
- (UILabel *)cardRightLabel
{
    if (!_cardRightLabel) {
        _cardRightLabel = [[UILabel alloc] init];
        _cardRightLabel.textColor = WHITE_COLOR;
        _cardRightLabel.font = [UIFont systemFontOfSize:13];
    }
    return _cardRightLabel;
}

- (UIButton *)cardRightBtn
{
    if (!_cardRightBtn) {
        _cardRightBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"bankCard_warning"];
        [_cardRightBtn setImage:image forState:(UIControlStateNormal)];
    }
    return _cardRightBtn;
}
- (UILabel *)cardNumLabel
{
    if (!_cardNumLabel) {
        _cardNumLabel = [[UILabel alloc] init];
        _cardNumLabel.font = [UIFont systemFontOfSize:16];
        _cardNumLabel.textColor = WHITE_COLOR;
    }
    return _cardNumLabel;
}

@end
