//
//  YKBankCardListCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKBankCardModel.h"

static NSString *const kEventWarningAction = @"kEventWarningAction";
@interface YKBankCardListCell : UITableViewCell

@property (nonatomic, strong) YKBankCardModel *bankCardModel;
@end
