//
//  YKMineMenuCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKMineMenuModel.h"
static NSString *kIdMineMenuCollectionCell = @"kIdYKMineMenuCell";
@interface YKMineMenuCell : UICollectionViewCell
@property (nonatomic, strong) YKMineMenuModel *model;

//+ (CGSize)itemSizeOfModel:(YKMineMenuModel *)model;
@end
