//
//  YKMineHeadView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMineHeadView.h"
#import "DFImageLabel.h"

@interface YKMineHeadView ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIImageView *creditCardImageV;
@property (nonatomic, strong) UILabel *amountTitleL;
@property (nonatomic, strong) UILabel *availableAmountLabel;
@property (nonatomic, strong) UIButton *cardRightTipBtn;
@property (nonatomic, strong) UIImageView *updateTipIV; //待更新、已更新的小图标
@property (nonatomic, strong) UILabel *creditingLabel; //授信中...
@property (nonatomic, strong) UILabel *cardBottomTipLabel; //认证已过期等提示文案
@property (nonatomic, strong) DFImageLabel *amountManageIL; //底部文案右边-额度管理按钮

@property (nonatomic, strong) UIImageView *creditUpdateAlertIV; //提升额度突出的弹框
//突出提醒内容
@property (nonatomic, strong) UILabel *alertTipDetailLabel;
@property (nonatomic, strong) DFImageLabel *alertImageL;

@property (nonatomic, strong) YKMineAmountView *amountView;
@property (nonatomic, strong) YKMineMenuView *menuView;

@property (nonatomic, assign) BOOL isR; //test

@end

@implementation YKMineHeadView

- (instancetype)init{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (void)setHeadCardModel:(YKMineHeadCardModel *)headCardModel
{
    _headCardModel = headCardModel;
    //设置样式
    switch (headCardModel.style) {
        case 1:
        {
            [self.creditCardImageV setImage:[UIImage imageNamed:@"mine_quota_disable"]];
            self.cardRightTipBtn.hidden = YES;
            self.updateTipIV.hidden = YES;
            self.cardBottomTipLabel.hidden = YES;
            self.amountManageIL.hidden = YES;
            self.creditingLabel.hidden = YES;
            
            self.creditUpdateAlertIV.hidden = NO;
            self.creditUpdateAlertIV.image = [UIImage imageNamed:@"tips_bg_disable"];
            self.alertTipDetailLabel.text = headCardModel.quotaFooterTextModel.text;
            self.alertImageL.label.text = headCardModel.quotaStatusModel.text;
            
            self.alertImageL.label.textColor = WHITE_COLOR;
            self.alertImageL.imageView.image = [UIImage imageNamed:@"system_right_white_arrrow"];
            self.alertImageL.backgroundColor = MAIN_THEME_COLOR;
        }
            break;
        case 2:
        {
            [self.creditCardImageV setImage:[UIImage imageNamed:@"mine_quota"]];
            self.cardRightTipBtn.hidden = NO;
            self.updateTipIV.hidden = NO;
            [self.updateTipIV sd_setImageWithURL:[NSURL URLWithString:headCardModel.quotaTotalModel.status]];
            self.cardBottomTipLabel.hidden = YES;
            self.amountManageIL.hidden = YES;
            self.creditingLabel.hidden = YES;
            
            self.creditUpdateAlertIV.hidden = NO;
            self.creditUpdateAlertIV.image = [UIImage imageNamed:@"tips_bg"];
            self.alertTipDetailLabel.text = headCardModel.quotaFooterTextModel.text;
            self.alertImageL.label.text = headCardModel.quotaStatusModel.text;
            
            self.alertImageL.label.textColor = MAIN_THEME_COLOR;
            self.alertImageL.imageView.image = [UIImage imageNamed:@"mine_netx_orange"];
            self.alertImageL.backgroundColor = WHITE_COLOR;
        }
            break;
        case 3:
        {
            [self.creditCardImageV setImage:[UIImage imageNamed:@"mine_quota"]];
            self.cardRightTipBtn.hidden = NO;
            self.updateTipIV.hidden = NO;
            [self.updateTipIV sd_setImageWithURL:[NSURL URLWithString:headCardModel.quotaTotalModel.status]];
            self.cardBottomTipLabel.hidden = NO;
            self.amountManageIL.hidden = NO;
            self.creditingLabel.hidden = YES;
            self.cardBottomTipLabel.text = headCardModel.quotaFooterTextModel.text;
            self.amountManageIL.label.text = headCardModel.quotaStatusModel.text;
            
            self.creditUpdateAlertIV.hidden = YES;
        }
            break;
        case 4:
        {
            [self.creditCardImageV setImage:[UIImage imageNamed:@"mine_quota"]];
            self.cardRightTipBtn.hidden = YES;
            self.updateTipIV.hidden = YES;
            self.cardBottomTipLabel.hidden = NO;
            self.amountManageIL.hidden = YES;
            self.creditingLabel.hidden = NO;
            self.cardBottomTipLabel.text = headCardModel.quotaFooterTextModel.text;
            self.creditingLabel.text = headCardModel.quotaStatusModel.text;
            
            self.creditUpdateAlertIV.hidden = YES;
        }
            break;
            
        default:
            break;
            
    }
    self.amountTitleL.text = headCardModel.quotaHeaderTextModel.text;
    self.availableAmountLabel.text = [NSString yk_numberToCommaSymbolformat:headCardModel.quotaTotalModel.text];
}

- (void)setMenuArr:(NSMutableArray *)menuArr
{
    _menuArr = menuArr;
    self.menuView.itemsArr = menuArr;
}
- (void)refreshDataWithAmountCanPop:(NSString *)amount orderDate:(NSString *)orderDate orderMoney:(NSString *)orderMoney redPointStatus:(NSInteger)status
{
    //账户余额和待还账单
    [self.amountView setAmountCanPop:amount orderDate:orderDate orderMoney:orderMoney redPointStatus:status];
}

- (void)setup{
    
    //---认证可用额度卡片及内容
    [self addSubview:self.creditCardImageV];
    self.creditCardImageV.userInteractionEnabled = YES;
    UIGestureRecognizer *tapCardGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickCardToAmount)];
    [self.creditCardImageV addGestureRecognizer: tapCardGes];
    [self.creditCardImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
//        make.left.mas_greaterThanOrEqualTo(5);
//        make.right.mas_greaterThanOrEqualTo(-5);
        make.centerX.equalTo(self.mas_centerX);
        make.height.mas_equalTo(ceil(139 * ASPECT_RATIO_WIDTH));
        make.width.mas_equalTo(353 * ASPECT_RATIO_WIDTH);
    }];
    [self.creditCardImageV setImage:[UIImage imageNamed:@"mine_quota"]];
    //额度Title
    [self.creditCardImageV addSubview:self.amountTitleL];
    [self.amountTitleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(29 * ASPECT_RATIO_WIDTH);
        make.left.mas_equalTo(19);
    }];
    self.amountTitleL.text = @"可用额度（元）";
    //额度数值
    [self.creditCardImageV addSubview:self.availableAmountLabel];
    [self.availableAmountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.amountTitleL.mas_left);
        make.top.equalTo(self.amountTitleL.mas_bottom).offset(10 * ASPECT_RATIO_WIDTH);
    }];
    self.availableAmountLabel.text = @"0.00";
    //待更新、已更新的小tip
    [self.creditCardImageV addSubview:self.updateTipIV];
    [self.updateTipIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.availableAmountLabel);
        make.left.equalTo(self.availableAmountLabel.mas_right).offset(10);
    }];
    [self.updateTipIV setImage:[UIImage imageNamed:@"mine_tobeupdated"]];
    //右边的可点小图标
    [self.creditCardImageV addSubview:self.cardRightTipBtn];
    [self.cardRightTipBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.availableAmountLabel);
        make.right.mas_equalTo(-15);
    }];
    //授信中...
    [self.creditCardImageV addSubview:self.creditingLabel];
    [self.creditingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.creditCardImageV).offset(-15);
        make.bottom.equalTo(self.creditCardImageV).offset(-20);
    }];
    
    //card底部的tip
    [self.creditCardImageV addSubview:self.cardBottomTipLabel];
    self.cardBottomTipLabel.text = @"您的认证已过期，请及时更新";
    [self.cardBottomTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(19);
        make.bottom.mas_equalTo(-20);
    }];
    
    [self.creditCardImageV addSubview:self.amountManageIL];
    [self.amountManageIL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.cardBottomTipLabel);
        make.left.equalTo(self.cardBottomTipLabel.mas_right).offset(20);
        make.height.mas_equalTo(40);
    }];
    self.amountManageIL.label.text = @"额度管理";
    self.amountManageIL.imageView.image = [UIImage imageNamed:@"system_right_white_arrrow"];
//    [self.amountManageIL addTarget:self action:@selector(cl) forControlEvents:UIControlEventTouchUpInside];
    UIGestureRecognizer *tapG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickAmountManage)];
    [self.amountManageIL addGestureRecognizer:tapG];
    
    //----账户余额信息
    self.amountView = [[NSBundle mainBundle] loadNibNamed:@"YKMineAmountView" owner:nil options:nil][0];
    [self addSubview:self.amountView];
    [self.amountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.creditCardImageV.mas_bottom);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(ceil(90 * ASPECT_RATIO_WIDTH));
    }];
    
    //-----突出的图片-放到账户余额后面，避免被遮盖
    [self addSubview:self.creditUpdateAlertIV];
    self.creditUpdateAlertIV.userInteractionEnabled = YES;
    [self.creditUpdateAlertIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.creditCardImageV.mas_bottom).offset(-35);
        make.left.mas_equalTo(18);
        make.right.mas_equalTo(-18);
    }];
    [self.creditUpdateAlertIV addSubview:self.alertTipDetailLabel];
    [self.creditUpdateAlertIV addSubview:self.alertImageL];
    [self.alertTipDetailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(31);
        make.top.mas_equalTo(18);
        make.right.lessThanOrEqualTo(self.alertImageL.mas_left).offset(-2);
    }];
    [self.alertImageL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.alertTipDetailLabel);
        make.right.mas_equalTo(-17);
        make.height.mas_equalTo(30);
    }];
    WEAK_SELF
    self.alertImageL.clickBlock = ^(DFImageLabel *control) {
        DLog(@"点我提升额度");
        [weakSelf routerEventWithName:kEventIntoAmountStatusManager userInfo:weakSelf.headCardModel.quotaStatusModel];
    };
    self.alertTipDetailLabel.text = @"您的认证已过期，请及时更新";
    self.alertImageL.label.text = @"提升额度";
    
    //-----菜单
    [self addSubview:self.menuView];
    [self.menuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.amountView.mas_bottom);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(ceil(90 * ASPECT_RATIO_WIDTH));
        make.bottom.equalTo(self);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = BORDER_DISABLED_COLOR;
    [self.menuView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.menuView);
        make.height.mas_equalTo(1.0/[UIScreen mainScreen].scale);
    }];
    
    self.cardBottomTipLabel.hidden = YES;
    self.amountManageIL.hidden = YES;

}

- (void)clickAmountManage{
//    DLog(@"mmmmmm");
    //底部的额度管理
    [self routerEventWithName:kEventIntoAmountStatusManager userInfo:self.headCardModel.quotaStatusModel];
}

- (void)clickCardToAmount{
    DLog(@"点击调到我的额度");
    [self routerEventWithName:kEventMyAmount userInfo:self.headCardModel.quotaTotalModel];
}

- (UIImageView *)creditCardImageV
{
    if (!_creditCardImageV) {
        _creditCardImageV = [[UIImageView alloc] init];
        _creditCardImageV.userInteractionEnabled = YES;
    }
    return _creditCardImageV;
}

- (UILabel *)amountTitleL
{
    if (!_amountTitleL) {
        _amountTitleL = [[UILabel alloc] init];
        _amountTitleL.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        _amountTitleL.textColor = [UIColor yk_colorWithHexString:@"#ffffff" alpha:0.6];
    }
    return _amountTitleL;
}

- (UILabel *)availableAmountLabel
{
    if (!_availableAmountLabel) {
        _availableAmountLabel = [[UILabel alloc] init];
        _availableAmountLabel.textColor = [UIColor whiteColor];
        _availableAmountLabel.font = [UIFont systemFontOfSize:adaptFontSize(35)];
    }
    return _availableAmountLabel;
}

- (UIImageView *)updateTipIV
{
    if (!_updateTipIV) {
        _updateTipIV = [[UIImageView alloc] init];
    }
    return _updateTipIV;
}

- (UIButton *)cardRightTipBtn{
    if (!_cardRightTipBtn) {
        _cardRightTipBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
//        [_cardRightTipBtn setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
//        _cardRightTipBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_cardRightTipBtn setImage:[UIImage imageNamed:@"mine_next"] forState:(UIControlStateNormal)];
        _cardRightTipBtn.userInteractionEnabled = NO;
    }
    return _cardRightTipBtn;
}

- (UILabel *)creditingLabel
{
    if (!_creditingLabel) {
        _creditingLabel = [[UILabel alloc] init];
        _creditingLabel.textColor = WHITE_COLOR;
        _creditingLabel.font = [UIFont systemFontOfSize:13];
    }
    return _creditingLabel;
}

- (UILabel *)cardBottomTipLabel
{
    if (!_cardBottomTipLabel) {
        _cardBottomTipLabel = [[UILabel alloc] init];
        _cardBottomTipLabel.textColor = [UIColor yk_colorWithHexString:@"#DE7715"];
        _cardBottomTipLabel.font = [UIFont systemFontOfSize:13];
    }
    return _cardBottomTipLabel;
}

- (DFImageLabel *)amountManageIL
{
    if (!_amountManageIL) {
        _amountManageIL = [[DFImageLabel alloc] initWithImageLabelArrangeStyle:(DFImageLArrangeStyleRightLeft) imageViewSize:CGSizeZero];
        _amountManageIL.label.font = [UIFont systemFontOfSize:13];
        _amountManageIL.label.textColor = WHITE_COLOR;
        _amountManageIL.imageLabelMargin = 5;
    }
    return _amountManageIL;
}

- (UIImageView *)creditUpdateAlertIV{
    if (!_creditUpdateAlertIV) {
        _creditUpdateAlertIV = [[UIImageView alloc] init];
        _creditUpdateAlertIV.image = [UIImage imageNamed:@"tips_bg"];
    }
    return _creditUpdateAlertIV;
}

- (UILabel *)alertTipDetailLabel
{
    if (!_alertTipDetailLabel) {
        _alertTipDetailLabel = [[UILabel alloc] init];
        _alertTipDetailLabel.textColor  = [UIColor yk_colorWithHexString:@"#8d8d8d"];
        _alertTipDetailLabel.font = [UIFont systemFontOfSize:13];
    }
    return _alertTipDetailLabel;
}
- (DFImageLabel *)alertImageL{
    if (!_alertImageL) {
        _alertImageL = [[DFImageLabel alloc] initWithImageLabelArrangeStyle:(DFImageLArrangeStyleRightLeft) leftMargin:10 rightMargin:5];
        [_alertImageL setContentCompressionResistancePriority:(UILayoutPriorityRequired) forAxis:(UILayoutConstraintAxisHorizontal)];
        _alertImageL.imageLabelMargin = 8;
        _alertImageL.label.textColor = MAIN_THEME_COLOR;
        _alertImageL.label.font = [UIFont systemFontOfSize:13];
        _alertImageL.imageView.image = [UIImage imageNamed:@"mine_netx_orange"];
        _alertImageL.layer.cornerRadius = 15;
        _alertImageL.layer.masksToBounds = YES;
    }
    return _alertImageL;
}

- (YKMineMenuView *)menuView
{
    if (!_menuView) {
        _menuView = [[YKMineMenuView alloc] init];
    }
    return _menuView;
}


@end
