//
//  YKBillDetailHeadView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBillDetailHeadView.h"
#import "DFImageLabel.h"

@interface YKBillDetailHeadView ()

@property (nonatomic, strong) UILabel *repay_dayLabel; //还款日
@property (nonatomic, strong) UILabel *repayStatusLabel; //待还款
@property (nonatomic, strong) DFImageLabel *overdueLabel; //已逾期
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UILabel *amountLabel;

//@property (nonatomic, strong) MASConstraint *repayStatusLabelRight;
//@property (nonatomic, strong) MASConstraint *repayStatusLabelOverdueLeft;

@end

@implementation YKBillDetailHeadView


- (instancetype)init{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)setup{
    [self addSubview:self.repay_dayLabel];
    [self addSubview:self.repayStatusLabel];
    [self addSubview:self.overdueLabel];
    [self addSubview:self.lineView];
    [self addSubview:self.amountLabel];
    
    [self.repay_dayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(13 * ASPECT_RATIO_WIDTH);
    }];
    [self.overdueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.repay_dayLabel);
        make.right.mas_equalTo(-15);
    }];
    [self.repayStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.overdueLabel);
        make.right.equalTo(self.overdueLabel.mas_left).offset(-10);
//        self.repayStatusLabelOverdueLeft = make.right.equalTo(self.overdueLabel.mas_left).offset(-10);
//        self.repayStatusLabelRight = make.right.equalTo(self.mas_right).offset(-15);

    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.5);
        make.top.equalTo(self.repay_dayLabel.mas_bottom).offset(13 * ASPECT_RATIO_WIDTH);
    }];
    
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.lineView.mas_bottom).offset(40 * ASPECT_RATIO_WIDTH);
    }];
    
    self.repay_dayLabel.text = @"还款日 00-00";
//    self.repayStatusLabel.text = @"待还款";
//    self.overdueLabel.attributedText = [NSString yk_addAttributeWithHtml5String:@"已逾期"];
    self.overdueLabel.label.text = @"已逾期";
    self.amountLabel.text = @"0.00元";
    
}

- (void)yk_setRepayDay:(NSString *)day amount:(NSString *)amount isOverdue:(BOOL)isOverdue orderState:(NSString *)orderState{
    self.repay_dayLabel.text = [NSString stringWithFormat:@"还款日 %@",day?:@""];
    self.amountLabel.text = amount?:@"";
    self.repayStatusLabel.attributedText = [NSString yk_addAttributeWithHtml5String:orderState];
    if (isOverdue) {
        self.overdueLabel.label.text = @"已逾期";
        self.overdueLabel.hidden = NO;
//        [self.repayStatusLabelOverdueLeft activate];
//        [self.repayStatusLabelRight deactivate];
    }else{
        self.overdueLabel.label.text = nil;
        self.overdueLabel.hidden = YES;
//        [self.repayStatusLabelOverdueLeft deactivate];
//        [self.repayStatusLabelRight activate];
    }
}

- (UILabel *)repay_dayLabel
{
    if (!_repay_dayLabel) {
        _repay_dayLabel = [[UILabel alloc] init];
        _repay_dayLabel.textColor = [UIColor yk_colorWithHexString:@"#666666"];
        _repay_dayLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
    }
    return _repay_dayLabel;
}

- (UILabel *)repayStatusLabel
{
    if (!_repayStatusLabel) {
        _repayStatusLabel = [[UILabel alloc] init];
        _repayStatusLabel.textColor = [UIColor yk_colorWithHexString:@"#666666"];
        _repayStatusLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
    }
    return _repayStatusLabel;
}
- (DFImageLabel *)overdueLabel
{
    if (!_overdueLabel) {
        _overdueLabel = [[DFImageLabel alloc] initWithImageLabelArrangeStyle:(DFImageLArrangeStyleRightLeft) leftMargin:3 rightMargin:3 topMargin:3 bottomMargin:3];
        _overdueLabel.label.textColor = [UIColor yk_colorWithHexString:@"#E96763"];
        _overdueLabel.label.font = [UIFont systemFontOfSize:adaptFontSize(14)];
        _overdueLabel.layer.borderWidth = 1;
        _overdueLabel.layer.borderColor = [UIColor yk_colorWithHexString:@"#E96763"].CGColor;
        _overdueLabel.layer.cornerRadius = 3;
        _overdueLabel.layer.masksToBounds = YES;
    }
    return _overdueLabel;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = LINE_COLOR;
    }
    return _lineView;
}
- (UILabel *)amountLabel
{
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc] init];
        _amountLabel.textColor = GRAY_COLOR_45;
        _amountLabel.font = [UIFont systemFontOfSize:adaptFontSize(30)];
    }
    return _amountLabel;
}

@end


