//
//  YKBillTipCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBillTipCell.h"

@interface YKBillTipCell ()


@end

@implementation YKBillTipCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setup];
    }
    return self;
}

- (void)setup{
    [self.contentView addSubview:self.tipView];
    [self.contentView addSubview:self.bottomView];
    [self.tipView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(30);
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipView.mas_bottom);
        make.height.mas_equalTo(10);
        make.left.right.bottom.equalTo(self.contentView);
    }];
    self.bottomView.backgroundColor = GRAY_BACKGROUND_COLOR;
}

- (UIView *)tipView
{
    if (!_tipView) {
        _tipView = [[UIView alloc] init];
    }
    return _tipView;
}

- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
    }
    return _bottomView;
}

@end
