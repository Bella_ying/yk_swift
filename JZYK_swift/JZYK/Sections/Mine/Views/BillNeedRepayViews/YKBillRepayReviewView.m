//
//  YKBillRepayReviewView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/22.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBillRepayReviewView.h"

static NSString *const titleApplyingStr = @"申请中...";
static NSString *const titleApplyFailStr = @"申请失败";
static NSString *const detailApplyingStr = @"您的借款申请已通过初审进入人工复审，由于现在为非工作时间，审核时间可能较长，请耐心等待审核结果";
static NSString *const detailApplyFailStr = @"很遗憾，你的信用评分不足，本次借款未能通过";
@interface YKBillRepayReviewView ()

@property (nonatomic, strong) UIImageView *imageView;
//@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, assign) YKRepaymentReviewStatusType statusType;

@end

@implementation YKBillRepayReviewView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = WHITE_COLOR;
        [self setup];
    }
    return self;
}

- (void)setup{
    
    
    [self addSubview:self.imageView];
//    [self addSubview:self.titleLabel];
    [self addSubview:self.detailLabel];
    [self addSubview:self.button];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(80 * ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(self);
    }];
    
//    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self);
//        make.top.equalTo(self.imageView.mas_bottom).offset(18 * ASPECT_RATIO_WIDTH);
//        make.height.mas_equalTo(25);
//    }];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.width.mas_lessThanOrEqualTo(250 * ASPECT_RATIO_WIDTH);
        make.top.equalTo(self.imageView.mas_bottom).offset(10 * ASPECT_RATIO_WIDTH);
    }];
    
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.detailLabel.mas_bottom).offset(100 * ASPECT_RATIO_WIDTH);
        make.bottom.equalTo(self);
    }];
    [self.button addTarget:self action:@selector(clickBtn) forControlEvents:(UIControlEventTouchUpInside)];
  
}

- (void)clickBtn{
    DLog(@"点击申请");
//    [[UserManager sharedUser] checkLoginStatus:^(BOOL sucOrFail) {
//        if (sucOrFail) {
//            if (self.buttonActionBlk) {
//                self.buttonActionBlk(self.statusType,self.detailUrl,self.helpUrl);
//            }
//        }
//    }];
    if (self.buttonActionBlk) {
        self.buttonActionBlk(self.statusType);
    }
}
- (void)yk_updateWithRepayBillType:(YKRepaymentReviewStatusType)type message:(NSString *)message{
    return [self yk_updateWithRepayBillType:type message:message can_loan_time:0];
}

- (void)yk_updateWithRepayBillType:(YKRepaymentReviewStatusType)type  message:(NSString *)message can_loan_time:(NSInteger)can_loan_time
{
    self.statusType = type;
    switch (type) {
        case YKRepaymentReviewStatusTypeApplying:
        {
            self.imageView.image = [UIImage imageNamed:@"repay_examining"];
//            self.titleLabel.text = titleApplyingStr;
            self.detailLabel.text = message; //detailApplyingStr
            [self.button setTitle:@"查看详情" forState:(UIControlStateNormal)];
        }
            break;
        case YKRepaymentReviewStatusTypeApplyFail:
        {
            self.imageView.image = [UIImage imageNamed:@"repay_examine_failure"];
//            self.titleLabel.text = titleApplyFailStr;
            self.detailLabel.text = message; //detailApplyFailStr
            [self.button setTitle:@"再次申请" forState:(UIControlStateNormal)];
        }
            break;
            
        default:
            break;

    }
    
    if (can_loan_time > 0) {
        UIImage *image = [UIImage imageNamed:@"btn_disabled_bg_image"];
        [self.button setBackgroundImage:image forState:(UIControlStateNormal)];
        self.button.enabled = NO;
        [self.button setTitle:[NSString stringWithFormat:@"%@（%ld天）",self.button.titleLabel.text,can_loan_time] forState:(UIControlStateNormal)];
    }else if (can_loan_time == -1){
        UIImage *image = [UIImage imageNamed:@"btn_disabled_bg_image"];
        [self.button setBackgroundImage:image forState:(UIControlStateNormal)];
        self.button.enabled = NO;
    }else{
        UIImage *image = [UIImage imageNamed:@"system_commit_btn"];
        [self.button setBackgroundImage:image forState:(UIControlStateNormal)];
        self.button.enabled = YES;
    }
}


#pragma -mark getter

- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.image = [UIImage imageNamed:@"repay_examining"];//repay_examine_failure
    }
    return _imageView;
}

//- (UILabel *)titleLabel
//{
//    if (!_titleLabel) {
//        _titleLabel  = [[UILabel alloc] init];
//        _titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(18)];
//        _titleLabel.textColor = GRAY_COLOR_45;
//        _titleLabel.text = @"";
//    }
//    return _titleLabel;
//}

- (UILabel *)detailLabel
{
    if (!_detailLabel) {
        _detailLabel  = [[UILabel alloc] init];
        _detailLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        _detailLabel.textColor = GRAY_COLOR_AD;
        _detailLabel.text = @"";
        _detailLabel.numberOfLines = 0;
        _detailLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _detailLabel;
}

- (UIButton *)button
{
    if (!_button) {
        _button  = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"system_commit_btn"];
        [_button setBackgroundImage:image forState:(UIControlStateNormal)];
        _button.layer.cornerRadius = image.size.height * 0.5;
        _button.layer.masksToBounds = YES;
        [_button setTitle:@"" forState:(UIControlStateNormal)];
        _button.titleLabel.font = [UIFont systemFontOfSize:17];
        [_button setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
    }
    return _button;
}

@end
