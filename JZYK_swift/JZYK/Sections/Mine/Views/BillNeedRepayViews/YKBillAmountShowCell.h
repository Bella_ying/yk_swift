//
//  YKBillAmountShowCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKBillAmountShowCell : UITableViewCell

- (void)yk_setNeedRepayAmount:(NSString *)amount period:(NSString *)period;

@end
