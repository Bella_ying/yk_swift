//
//  YKBillDetailCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const kIdYKBillDetailCell = @"YKBillDetailCell";

@interface YKBillDetailCell : UITableViewCell

- (void)yk_setTitle:(NSString *)title subText:(NSString *)subText;

@end
