//
//  YKBillRepayNoRecordView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/22.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBillRepayNoRecordView.h"

@interface YKBillRepayNoRecordView ()
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel  *textLabel;
@property (nonatomic, strong) UIButton *button;

@end

@implementation YKBillRepayNoRecordView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setup];
    }
    return self;
}

- (void)setup{
    
    [self addSubview:self.imageView];
    [self addSubview:self.textLabel];
    [self addSubview:self.button];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(80 *ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(self);
//        make.width.offset(100*ASPECT_RATIO_WIDTH);
//        make.height.offset(100*ASPECT_RATIO_WIDTH);
    }];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(10 * ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(self);
    }];
    
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textLabel.mas_bottom).offset(90 * ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(self);
        make.bottom.equalTo(self);
    }];

    [_button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)buttonAction:(UIButton *)button{
    if (self.applyActionBlk) {
        self.applyActionBlk(self.isCreditReviewing);
    }
}

#pragma mark- 根据数据更改展示样式
//- (void)prepareRecordContentWithStatus:(NSInteger)status
//{
//    // 0 未借款 1 已有成功借款记录
//    self.textLabel.text = status ? @"您的账单已经还清啦~" : @"您目前没有待还款的账单哦~";
//    [self.button setTitle:status ? @"再次申请" : @"马上申请" forState:UIControlStateNormal];
//}
- (void)yk_updateWithRepayNoRecordType:(YKRepaymentNoRecordType)type{
    if (type == YKRepaymentNoRecordTypeHaveRepay) {
        self.textLabel.text = @"您的账单已经还清啦~";
        [self.button setTitle:@"再次申请" forState:(UIControlStateNormal)];
    }else{
        self.textLabel.text = @"您目前还没有待还款的账单哦~";
        [self.button setTitle:@"申请分期" forState:(UIControlStateNormal)];
    }
}


#pragma -mark getter

- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];//repay_noRecord
        _imageView.image = [UIImage imageNamed:@"repay_noRecord"];
    }
    return _imageView;
}

- (UILabel *)textLabel
{
    if (!_textLabel) {
        _textLabel  = [[UILabel alloc] init];
        _textLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        _textLabel.textColor = GRAY_COLOR_AD;
        _textLabel.text = @"您目前还没有待还款的账单哦~";
    }
    return _textLabel;
}

- (UIButton *)button
{
    if (!_button) {
        _button  = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"system_commit_btn"];
        [_button setBackgroundImage:image forState:(UIControlStateNormal)];
        _button.layer.cornerRadius = image.size.height * 0.5;
        _button.layer.masksToBounds = YES;
        [_button setTitle:@"申请分期" forState:(UIControlStateNormal)];
        _button.titleLabel.font = [UIFont systemFontOfSize:17];
        [_button setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
    }
    return _button;
}

@end
