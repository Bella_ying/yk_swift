//
//  YKBillNeedRepayListCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBillNeedRepayListCell.h"
#import "DFImageLabel.h"
@interface YKBillNeedRepayListCell ()
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *subInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *subKindStateLabel;
@property (nonatomic, strong) DFImageLabel *imageLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentMonthLabel;

@end

@implementation YKBillNeedRepayListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.currentMonthLabel.layer.cornerRadius = 3;
    self.currentMonthLabel.layer.masksToBounds = YES;
    
    [self.contentView addSubview:self.imageLabel];
    [self.imageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.subKindStateLabel);
        make.right.equalTo(self.subKindStateLabel.mas_left);
    }];
    self.imageLabel.userInteractionEnabled = NO;
    self.imageLabel.hidden = YES;
}

- (void)setListModel:(YKRepayListModel *)listModel
{
    _listModel = listModel;
//    self.amountLabel.attributedText = [NSString yk_addAttributeWithHtml5String:[NSString stringWithFormat:@"%@元",listModel.repay_money]];
    self.amountLabel.text = [NSString stringWithFormat:@"%@元",listModel.repay_money];
    
//    self.subInfoLabel.attributedText = [NSString yk_addAttributeWithHtml5String:listModel.repay_desc?:@""];
    self.subInfoLabel.text = listModel.repay_desc?:@"";
    
    if (listModel.is_overdue){
        //已逾期
        self.imageLabel.hidden = NO;
        self.subKindStateLabel.text = nil;
    }else{
        self.imageLabel.hidden = YES;
        self.subKindStateLabel.attributedText = [NSString yk_addAttributeWithHtml5String:listModel.status_zh];
    }
    
    if (listModel.cur_order == 1) {
        //显示当月账单
        self.amountLabel.textColor = MAIN_THEME_COLOR;
        self.currentMonthLabel.text = [NSString stringWithFormat:@" %@ ",@"当月账单"];
        self.currentMonthLabel.hidden = NO;
    }else{
        //不显示
        self.amountLabel.textColor = GRAY_COLOR_45;
        self.currentMonthLabel.text = @"";
        self.currentMonthLabel.hidden = YES;
    }
    
}

- (DFImageLabel *)imageLabel
{
    if (!_imageLabel) {
        _imageLabel = [[DFImageLabel alloc] initWithImageLabelArrangeStyle:(DFImageLArrangeStyleLeftRight) leftMargin:3 rightMargin:3 topMargin:3 bottomMargin:3];
        _imageLabel.label.font = [UIFont systemFontOfSize:14];
        _imageLabel.label.textColor = [UIColor yk_colorWithHexString:@"#E96763"];
        _imageLabel.label.text = @"已逾期";
        _imageLabel.layer.borderWidth = 1;
        _imageLabel.layer.borderColor = [UIColor yk_colorWithHexString:@"#E96763"].CGColor;
        _imageLabel.layer.cornerRadius = 3;
        _imageLabel.layer.masksToBounds = YES;
    }
    return _imageLabel;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
