//
//  YKBillDetailCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBillDetailCell.h"

@interface YKBillDetailCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;

@end

@implementation YKBillDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)yk_setTitle:(NSString *)title subText:(NSString *)subText
{
    self.titleL.text = title;
    self.subLabel.text = subText;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
