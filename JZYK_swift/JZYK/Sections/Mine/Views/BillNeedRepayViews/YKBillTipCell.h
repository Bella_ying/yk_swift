//
//  YKBillTipCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKBillTipCell : UITableViewCell

@property (nonatomic, strong) UIView *tipView;
@property (nonatomic, strong) UIView *bottomView;
@end
