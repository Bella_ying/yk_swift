//
//  YKBillAmountShowCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBillAmountShowCell.h"

@interface YKBillAmountShowCell ()
@property (weak, nonatomic) IBOutlet UILabel *needRepayMoney;
@property (weak, nonatomic) IBOutlet UILabel *loanDate;

@end

@implementation YKBillAmountShowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)yk_setNeedRepayAmount:(NSString *)amount period:(NSString *)period{
    
    self.needRepayMoney.text = amount?:@"---";
    self.loanDate.text = period?:@"---";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
