//
//  YKBillRepayNoRecordView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/22.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

//借款审核状态
typedef NS_ENUM(NSInteger, YKRepaymentNoRecordType) {
    YKRepaymentNoRecordTypeNewUser,         //没有借款记录
    YKRepaymentNoRecordTypeHaveRepay,     //账单已换清-暂没有借款记录
};

@interface YKBillRepayNoRecordView : UIView

@property (nonatomic, assign) BOOL isCreditReviewing; //是否授信完成
@property(nonatomic,copy) void(^applyActionBlk)(BOOL isCreditReviewing);

//- (void)prepareRecordContentWithStatus:(NSInteger)status;
- (void)yk_updateWithRepayNoRecordType:(YKRepaymentNoRecordType)type;

@end
