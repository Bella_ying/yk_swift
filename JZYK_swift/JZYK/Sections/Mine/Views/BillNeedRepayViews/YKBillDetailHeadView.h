//
//  YKBillDetailHeadView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKBillDetailHeadView : UIView
- (void)yk_setRepayDay:(NSString *)day amount:(NSString *)amount isOverdue:(BOOL)isOverdue orderState:(NSString *)orderState;

@end
