//
//  YKBillRepayReviewView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/22.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

//借款审核状态
typedef NS_ENUM(NSInteger, YKRepaymentReviewStatusType) {
    YKRepaymentReviewStatusTypeApplying,         //申请中
    YKRepaymentReviewStatusTypeApplyFail,     //申请失败
};

@interface YKBillRepayReviewView : UIView

@property(nonatomic,copy) void(^detailBlk)(BOOL needsHelp,NSString *helpUrl);

@property(nonatomic,copy) void(^buttonActionBlk)(YKRepaymentReviewStatusType type);
@property (nonatomic, copy) NSString *detailUrl;
@property(nonatomic,copy) NSString *friendsHelp;
@property(nonatomic,copy) NSString *helpUrl;

- (void)yk_updateWithRepayBillType:(YKRepaymentReviewStatusType)type message:(NSString *)message;
- (void)yk_updateWithRepayBillType:(YKRepaymentReviewStatusType)type message:(NSString *)message can_loan_time:(NSInteger)can_loan_time;

@end
