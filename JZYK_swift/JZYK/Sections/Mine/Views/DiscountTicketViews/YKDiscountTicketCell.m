//
//  YKDiscountTicketCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKDiscountTicketCell.h"

#define kTicketSideMargin 15
#define kContentFirstCenterOffset (-115) //左边内容中心相对背景中心的偏移
#define kContentSecondCenterOffset (-40) //右边内容左侧相对背景中心的偏移

@interface YKDiscountTicketCell ()
@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UILabel *amountNumLabel; //抵扣金额
@property (nonatomic, strong) UILabel *ticketKindInfoLabel; //种类（借款抵扣券）
@property (nonatomic, strong) UILabel *useLimitLabel; //使用限制，满多少使用
@property (nonatomic, strong) UILabel *timeLimitLabel; //有效期
@property (nonatomic, strong) UILabel *ticketSourceLabel; //本券是..活动所送券
@property (nonatomic, strong) UIImageView *daysRemainImageView;
@property (nonatomic, strong) UILabel     *daysLabel;

@property (nonatomic, strong) UIImageView *rightExpireImageView;
@end

@implementation YKDiscountTicketCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setup];
    }
    return self;
}

- (void)setup{
    
    [self.contentView addSubview:self.backImageView];
    [self.backImageView addSubview:self.amountNumLabel];
    [self.backImageView addSubview:self.ticketKindInfoLabel];
    [self.backImageView addSubview:self.useLimitLabel];
    [self.backImageView addSubview:self.timeLimitLabel];
    [self.backImageView addSubview:self.ticketSourceLabel];
    [self.backImageView addSubview:self.daysRemainImageView];
    [self.backImageView addSubview:self.daysLabel];
    
    [self.backImageView addSubview:self.rightExpireImageView];
    
    [self.backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20 * ASPECT_RATIO_WIDTH);
        make.left.mas_equalTo(kTicketSideMargin * ASPECT_RATIO_WIDTH);
        make.right.mas_equalTo(-kTicketSideMargin * ASPECT_RATIO_WIDTH);
        make.height.mas_equalTo(110 * ASPECT_RATIO_WIDTH);
    }];
    [self.amountNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(27 * ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(self.backImageView).offset(kContentFirstCenterOffset * ASPECT_RATIO_WIDTH);
    }];
    [self.ticketKindInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.amountNumLabel.mas_bottom).offset(6 * ASPECT_RATIO_WIDTH);
        make.centerX.equalTo(self.amountNumLabel);
    }];
    [self.useLimitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(12 * ASPECT_RATIO_WIDTH);
        make.left.equalTo(self.backImageView.mas_centerX).offset(kContentSecondCenterOffset * ASPECT_RATIO_WIDTH);
    }];
    [self.timeLimitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.useLimitLabel.mas_bottom).offset(2 * ASPECT_RATIO_WIDTH);
        make.left.equalTo(self.useLimitLabel);
    }];
    [self.ticketSourceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.useLimitLabel);
        make.bottom.mas_equalTo(-12 * ASPECT_RATIO_WIDTH);
    }];
    
    //黄色背景图
    [self.daysRemainImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backImageView.mas_top);
        make.left.equalTo(self.backImageView.mas_left);
        make.width.height.mas_offset(53 * ASPECT_RATIO_WIDTH);
    }];
    
    //剩2天
    [self.daysLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backImageView.mas_top).offset(-6 * ASPECT_RATIO_WIDTH);
        make.left.equalTo(self.backImageView.mas_left).offset(-9 * ASPECT_RATIO_WIDTH);
        make.width.height.mas_offset(53 * ASPECT_RATIO_WIDTH);
    }];
    
    [self.rightExpireImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.equalTo(self.backImageView);
    }];

    
    [self.backImageView setImage:[UIImage imageNamed:@"ticketBack_image"]];

}

- (void)setTicketModel:(YKDiscountTicketModel *)ticketModel
{
    _ticketModel = ticketModel;
//    self.amountNumLabel.text = [NSString stringWithFormat:@"%@元",ticketModel.amount_v3];
    self.amountNumLabel.text = [NSString stringWithFormat:@"%@",ticketModel.amount_v3];
    self.ticketKindInfoLabel.text = ticketModel.coupon_type_name;
    self.useLimitLabel.text = ticketModel.loan_amount_new;
    self.timeLimitLabel.text = ticketModel.time_v3;
    self.ticketSourceLabel.text = ticketModel.coupon_name;
    self.daysLabel.text = ticketModel.coupon_status;
    
    [self prepareForStatus:[ticketModel.status integerValue] coupon_status:ticketModel.coupon_status];
}

//现金劵状态 0 未使用，1已使用 2已过期  其中已使用和已过期是历史优惠券
- (void)prepareForStatus:(NSInteger)status coupon_status:(NSString *)coupon_status
{
    if (status) {
        // 历史优惠券
        [self.backImageView setImage:[UIImage imageNamed:@"ticketHistoryBack_image"]];
        self.ticketKindInfoLabel.textColor = LINE_COLOR;
        self.useLimitLabel.textColor = LINE_COLOR;
        self.timeLimitLabel.textColor = LINE_COLOR;
        self.ticketSourceLabel.textColor = LINE_COLOR;
        if (status == 1) {
            [self.rightExpireImageView setImage:[UIImage imageNamed:@"ticket_haveUse"]];
        }else{
            [self.rightExpireImageView setImage:[UIImage imageNamed:@"ticket_expire_icon"]];
        }
        self.daysRemainImageView.hidden = YES;
        self.daysLabel.hidden = YES;
//        [self.daysRemainImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self.backImageView.mas_top).offset(0);
//            make.right.equalTo(self.backImageView.mas_right).offset(0);
//            make.height.offset(57*ASPECT_RATIO_WIDTH);
//            make.width.offset(55*ASPECT_RATIO_WIDTH);
//        }];
//        self.daysLabel.hidden = YES;
    } else {
        // 我的优惠券
        [self.backImageView setImage:[UIImage imageNamed:@"ticketBack_image"]];

//        self.daysRemainImageView.image = [UIImage imageNamed:@"icon_days_remain"];
//        [self.daysRemainImageView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.width.height.mas_offset(53 * ASPECT_RATIO_WIDTH);
//        }];
    
        if ([coupon_status isEqualToString:@"0"]) {
            self.daysLabel.hidden = YES;
            self.daysRemainImageView.hidden = YES;
        } else if ([coupon_status isEqualToString:@"新"]) {
            self.daysLabel.hidden = NO;
            self.daysRemainImageView.hidden = NO;
//            self.daysLabel.textColor = [UIColor colorWithHex:@"#FF4141"];
        } else {
            self.daysLabel.hidden = NO;
            self.daysRemainImageView.hidden = NO;
//            self.daysLabel.textColor = [UIColor colorWithHex:@"#CBA53F"];
        }
    }
}


- (UIImageView *)backImageView
{
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc] init];
    }
    return _backImageView;
}

- (UILabel *)amountNumLabel
{
    if (!_amountNumLabel) {
        _amountNumLabel = [[UILabel alloc] init];
        _amountNumLabel.font = [UIFont systemFontOfSize:adaptFontSize(25)];
        _amountNumLabel.textColor = WHITE_COLOR;
    }
    return _amountNumLabel;
}
- (UILabel *)ticketKindInfoLabel
{
    if (!_ticketKindInfoLabel) {
        _ticketKindInfoLabel = [[UILabel alloc] init];
        _ticketKindInfoLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
        _ticketKindInfoLabel.textColor = WHITE_COLOR;
        
    }
    return _ticketKindInfoLabel;
}
- (UILabel *)useLimitLabel
{
    if (!_useLimitLabel) {
        _useLimitLabel = [[UILabel alloc] init];
        _useLimitLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
        _useLimitLabel.textColor = LABEL_TEXT_COLOR;
        
    }
    return _useLimitLabel;
}

- (UILabel *)timeLimitLabel
{
    if (!_timeLimitLabel) {
        _timeLimitLabel = [[UILabel alloc] init];
        _timeLimitLabel.font = [UIFont systemFontOfSize:adaptFontSize(14)];
        _timeLimitLabel.textColor = LABEL_TEXT_COLOR;
    }
    return _timeLimitLabel;
}
- (UILabel *)ticketSourceLabel
{
    if (!_ticketSourceLabel) {
        _ticketSourceLabel = [[UILabel alloc] init];
        _ticketSourceLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        _ticketSourceLabel.textColor = GRAY_COLOR_8D;
    }
    return _ticketSourceLabel;
}

- (UIImageView *)daysRemainImageView
{
    if (!_daysRemainImageView) {
        _daysRemainImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_days_remain"]];
        _daysRemainImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _daysRemainImageView;
}

- (UILabel *)daysLabel
{
    if (!_daysLabel) {
        _daysLabel = [[UILabel alloc] init];
        _daysLabel.textColor = [UIColor yk_colorWithHexString:@"#F28200"];
        _daysLabel.font = [UIFont boldSystemFontOfSize:adaptFontSize(13)];
        _daysLabel.textAlignment = NSTextAlignmentCenter;
        _daysLabel.transform = CGAffineTransformMakeRotation(-M_PI_4);
    }
    return _daysLabel;
}

- (UIImageView *)rightExpireImageView
{
    if (!_rightExpireImageView) {
        _rightExpireImageView = [[UIImageView alloc] init];
    }
    return _rightExpireImageView;
}

@end
