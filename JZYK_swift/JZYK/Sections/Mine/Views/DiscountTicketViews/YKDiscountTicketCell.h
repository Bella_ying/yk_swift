//
//  YKDiscountTicketCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKDiscountTicketModel.h"

@interface YKDiscountTicketCell : UITableViewCell

@property (nonatomic, strong) YKDiscountTicketModel *ticketModel;

- (void)testFirstKindCell;
- (void)testSecondKindCell;


@end
