//
//  YKMineMenuView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMineMenuView.h"
#import "DFImageLabel.h"
#import "YKMineMenuCell.h"
#import "YKMineMenuModel.h"

#define kItemVMargin 30
#define kItemHMargin 20
#define kItemSideMargin 15

@interface YKMineMenuView ()<UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

//@property (nonatomic, strong) DFImageLabel *firstItem;
//@property (nonatomic, strong) DFImageLabel *secondItem;
//@property (nonatomic, strong) DFImageLabel *thirdItem;
//@property (nonatomic, strong) DFImageLabel *forthItem;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;


@end

@implementation YKMineMenuView

- (instancetype)init{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

- (void)setItemsArr:(NSMutableArray *)itemsArr
{
    _itemsArr = itemsArr;
    [self.collectionView reloadData];
}

- (void)setup{
    
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
//    self.itemsArr = @[].mutableCopy;
//    for (int i = 0; i< 4; i++) {
//        YKMineMenuModel *model = [[YKMineMenuModel alloc] init];
//        switch (i) {//我的订单mine_list 邀请好友 mine_share 优惠券mine_coupon 积分mine_integral
//            case 0:
//            {
//                model.title = @"我的订单";
//                model.imageStr = @"mine_list";
//            }
//                break;
//            case 1:
//            {
//                model.title = @"邀请好友";
//                model.imageStr = @"mine_share";
//            }
//                break;
//            case 2:
//            {
//                model.title = @"优惠券";
//                model.imageStr = @"mine_coupon";
//            }
//                break;
//            case 3:
//            {
//                model.title = @"积分";
//                model.imageStr = @"mine_integral";
//            }
//                break;
//            default:
//                break;
//        }
//
//        [self.itemsArr addObject:model];
//    }
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    //    return 18;
//    NSLog(@"wgj_%lf",collectionView.contentSize.width);
    return self.itemsArr.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YKMineMenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kIdMineMenuCollectionCell forIndexPath:indexPath];
    YKMineMenuModel *itemModel = self.itemsArr[indexPath.item];
    [cell setModel:itemModel];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //    CGFloat width = 50;
    //    CGFloat height = 50;
    //    CGSize size = CGSizeMake(width, height);
    
//    CGSize size = CGSizeMake(0, 0);
//    if (self.itemsArr && self.itemsArr.count != 0) {
//        YKMineMenuModel *model = self.itemsArr[indexPath.item];
//        size = [YKMineMenuCell itemSizeOfModel:model];
//    }
//    return size;
    
    return CGSizeMake(WIDTH_OF_SCREEN/4.0, ceil(90 * ASPECT_RATIO_WIDTH));
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    if (indexPath.item == 0) {
//        DLog(@"点我0");
//        [self routerEventWithName:kEventMenuFirstItem userInfo:nil];
//    }else if (indexPath.item == 1){
//        DLog(@"点我1");
//        [self routerEventWithName:kEventMenuSecondItem userInfo:nil];
//    }else if (indexPath.item == 2){
//        DLog(@"点我2");
//        [self routerEventWithName:kEventMenuThirdItem userInfo:nil];
//    }else if (indexPath.item == 3){
//        DLog(@"点我3");
//        [self routerEventWithName:kEventMenuForthItem userInfo:nil];
//    }
    
    [self routerEventWithName:kEventMenuItem userInfo:@{kItemModelKey:self.itemsArr[indexPath.item]}];
}


- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
        self.flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.flowLayout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;
        self.flowLayout.minimumLineSpacing = 0;
        //        if (iOS9) {
        //            self.flowLayout.estimatedItemSize = CGSizeMake(50, 50);
        //        }else{
        //            self.flowLayout.estimatedItemSize = CGSizeMake(100, 100);
        //        }
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView registerClass:[YKMineMenuCell class] forCellWithReuseIdentifier:kIdMineMenuCollectionCell];
        _collectionView.scrollEnabled = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        
    }
    return _collectionView;
}

//- (void)setup{
//    [self addSubview:self.firstItem];
//    [self addSubview:self.secondBtn];
//    [self addSubview:self.thirdBtn];
//    [self addSubview:self.forthBtn];
//
//    [self.firstBtn mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.centerX.mas_equalTo(self.mas_centerX).offset(-WIDTH_OF_SCREEN/10.0 * 3);
//        make.left.mas_equalTo(30);
//        make.centerY.equalTo(self);
//    }];
//    [self.secondBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(self.mas_centerX).offset(-WIDTH_OF_SCREEN/10.0 * 1);
//        make.centerY.equalTo(self);
//    }];
//    [self.thirdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(self.mas_centerX).offset(WIDTH_OF_SCREEN/10.0 * 1);
//        make.centerY.equalTo(self);
//    }];
//    [self.forthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(self.mas_centerX).offset(WIDTH_OF_SCREEN/10.0 * 3);
//        make.centerY.equalTo(self);
//    }];
//}
//
//- (DFImageLabel *)firstBtn
//{
//    if (!_firstBtn) {
//        _firstBtn = [[DFImageLabel alloc] initWithImageLabelArrangeStyle:(DFImageLArrangeStyleUpDown)];
//        _firstBtn.imageView.image = [UIImage imageNamed:@"mine_list"];
//        _firstBtn.imageView
//        [_firstBtn setTitle:@"我的订单" forState:(UIControlStateNormal)];
//        [_firstBtn setImage:[UIImage imageNamed:@"mine_list"] forState:(UIControlStateNormal)];
//        [_firstBtn setTitleColor:[UIColor yk_colorWithHexString:@"#666666"] forState:(UIControlStateNormal)];
//        _firstBtn.titleLabel.font = [UIFont systemFontOfSize:14];
////        [_firstBtn df_setImagePositionWithType:(DFImagePositionTypeTop) spacing:5];
//    }
//    return _firstBtn;
//}
//- (UIButton *)secondBtn
//{
//    if (!_secondBtn) {
//        _secondBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
//        [_secondBtn setTitle:@"邀请好友" forState:(UIControlStateNormal)];
//        [_secondBtn setImage:[UIImage imageNamed:@"mine_share"] forState:(UIControlStateNormal)];
//        [_secondBtn setTitleColor:[UIColor yk_colorWithHexString:@"#666666"] forState:(UIControlStateNormal)];
//        _secondBtn.titleLabel.font = [UIFont systemFontOfSize:14];
////        [_secondBtn df_setImagePositionWithType:(DFImagePositionTypeTop) spacing:5];
//    }
//    return _secondBtn;
//}
//- (UIButton *)thirdBtn
//{
//    if (!_thirdBtn) {
//        _thirdBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
//        [_thirdBtn setTitle:@"优惠券" forState:(UIControlStateNormal)];
//        [_thirdBtn setImage:[UIImage imageNamed:@"mine_coupon"] forState:(UIControlStateNormal)];
//        [_thirdBtn setTitleColor:[UIColor yk_colorWithHexString:@"#666666"] forState:(UIControlStateNormal)];
//        _thirdBtn.titleLabel.font = [UIFont systemFontOfSize:14];
////        [_thirdBtn df_setImagePositionWithType:(DFImagePositionTypeTop) spacing:5];
//    }
//    return _thirdBtn;
//}
//- (UIButton *)forthBtn
//{
//    if (!_forthBtn) {
//        _forthBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
//        [_forthBtn setTitle:@"积分" forState:(UIControlStateNormal)];
//        [_forthBtn setImage:[UIImage imageNamed:@"mine_integral"] forState:(UIControlStateNormal)];
//        [_forthBtn setTitleColor:[UIColor yk_colorWithHexString:@"#666666"] forState:(UIControlStateNormal)];
//        _forthBtn.titleLabel.font = [UIFont systemFontOfSize:14];
////        [_forthBtn df_setImagePositionWithType:(DFImagePositionTypeTop) spacing:5];
//    }
//    return _forthBtn;
//}

@end
