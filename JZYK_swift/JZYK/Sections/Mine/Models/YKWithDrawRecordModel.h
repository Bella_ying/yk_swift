//
//  YKWithDrawRecordModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/19.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKWithDrawRecordModel : YKBaseModel

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *withdraw_time;
@property (nonatomic, copy) NSString *desc;

@end
