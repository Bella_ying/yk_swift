//
//  YKMineAccountRemainModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/19.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKMineAccountRemainModel : YKBaseModel

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *type_name;
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *tag; //"剩余11小时38分钟",
@property (nonatomic, copy) NSString *desc;

@end
