//
//  YKDiscountTicketModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKDiscountTicketModel : YKBaseModel

/**现金劵金额*/
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *amount_v3; //YK--金额
/**限免金额*/
@property (nonatomic, copy) NSString * loan_amount;
/**限免期限*/
@property (nonatomic, copy) NSString * loan_term;
/**现金劵状态 0 未使用，1已使用 2已过期*/
@property (nonatomic, copy) NSString *status;  //YK---
/**有效期*/
@property (nonatomic, copy) NSString * time;
/**说明文案*/
@property (nonatomic, copy) NSString * use_name;
/**说明文案*/
@property (nonatomic, copy) NSString * coupon_name; //YK--本券是**活动送券
/**现金劵唯一标示*/
@property (nonatomic, copy) NSString * coupon_id;
/**抵扣劵类型标志位*/
@property (nonatomic, copy) NSString * use_case;
/**免息劵标题*/
@property (nonatomic, copy) NSString * title;
/**免息天数*/
@property (nonatomic, copy) NSString * day;
/**底图片*/
@property (nonatomic, copy) NSString * color;
/**右上角图片*/
@property (nonatomic, copy) NSString * color_shang;
/**cell的类型*/
@property (nonatomic, copy) NSString * type;

//是否在使用中 0：非选择状态    1：已选中   2：未选中
@property (nonatomic, copy) NSString *isSelected;

@property (nonatomic ) NSInteger days_remain;//剩余天数
// 3.0.0 剩余天数
@property (nonatomic, copy) NSString *coupon_status; //YK---左上角，剩三天
// 3.0.0 限制金额
@property (nonatomic, copy) NSString *loan_amount_new; //YK--借1000元及以上使用
// 3.0.0 优惠券有效期
@property (nonatomic, copy) NSString *time_v3; //YK——有效期
// 3.0.0 卷说明
@property (nonatomic, copy) NSString *explain;
/*
 1，则需要展示 coupon_type_name 字段
 0, 需要展示amount
 */
@property (nonatomic) BOOL amount_is_float;

@property (nonatomic, copy) NSString *coupon_type_name; //YK--借款抵扣券（kind）

@end
