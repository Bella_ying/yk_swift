//
//  YKOrderListModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/19.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKOrderListModel : YKBaseModel
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *pic_url;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *money;
@property (nonatomic, copy) NSString *period;
@property (nonatomic, copy) NSString *order_time;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *count_down; //"00:34:25"
@property (nonatomic, copy) NSString *link_url;
@property (nonatomic, assign) BOOL can_cancel; //是否可以取消借款
@property (nonatomic, assign) BOOL is_show_help; //是否到账单详情，账单详情是有帮助按钮的
@end
