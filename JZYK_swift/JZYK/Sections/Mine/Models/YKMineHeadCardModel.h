//
//  YKMineHeadCardModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/18.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKMineCardDetailModel : YKBaseModel
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *goSkilpCode;
@property (nonatomic, copy) NSString *goH5;
@property (nonatomic, copy) NSString *status; //url更新，待更新

@end

@interface YKMineHeadCardModel : YKBaseModel

@property (nonatomic, strong) YKMineCardDetailModel *quotaHeaderTextModel;
@property (nonatomic, strong) YKMineCardDetailModel *quotaStatusModel;
@property (nonatomic, strong) YKMineCardDetailModel *quotaTotalModel;
@property (nonatomic, strong) YKMineCardDetailModel *quotaFooterTextModel;
@property (nonatomic, assign) NSInteger style; //1a  2b 3c  4d   授信中为4d  对应原型图里的

@end
