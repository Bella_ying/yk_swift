//
//  YKWithDrawRecordActivityModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/7/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKWithDrawRecordActivityModel : YKBaseModel

@property (nonatomic, copy) NSString *bank_name;
@property (nonatomic, copy) NSString *card_no;
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *status_name;

@end
