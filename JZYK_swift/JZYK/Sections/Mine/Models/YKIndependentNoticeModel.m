//
//  YKIndependentNoticeModel.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/22.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKIndependentNoticeModel.h"

@implementation YKIndependentNoticeModel

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{@"noticeId":@"id"};
}
@end
