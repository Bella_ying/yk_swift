//
//  YKMyAmountModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/20.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKIncreasedLimitListModel : NSObject
/*"date": "3-11",
 "title": "成功还款",
 "desc": "还款500元",
 "amount": 1000
 */
//@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *date_month;
@property (nonatomic, copy) NSString *date_day;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *symbol;

@end

@interface YKMyAmountModel : YKBaseModel

@property (nonatomic, copy) NSString *total_quota;
@property (nonatomic, copy) NSString *total_incre;
@property (nonatomic, copy) NSString *temp_quota;
@property (nonatomic, assign) NSInteger page_count;
@property (nonatomic, copy) NSString *about_quota; // 额度说明url
@property (nonatomic, copy) NSArray <YKIncreasedLimitListModel *>*list;

@end
