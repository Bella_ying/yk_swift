//
//  YKMineMenuModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/6.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKMineListModel : YKBaseModel
//list
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *textAlias;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *skipCode;
@property (nonatomic, copy) NSString *skipH5;
@end

@interface YKMineMenuModel : YKBaseModel
//diyList
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *skipCode;
@property (nonatomic, copy) NSString *skipH5;

@end
