//
//  YKMyAmountModel.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/20.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMyAmountModel.h"

@implementation YKIncreasedLimitListModel


@end

@implementation YKMyAmountModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"list":[YKIncreasedLimitListModel class]};
}

@end
