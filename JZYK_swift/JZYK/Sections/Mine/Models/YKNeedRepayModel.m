//
//  YKNeedRepayModel.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/19.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKNeedRepayModel.h"

@implementation YKRepayListModel

@end

@implementation Reimbursement

@end

@implementation YKRepayTotalModel

@end

@implementation YKNeedRepayModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"list":[YKRepayListModel class],
             @"pay_type":[Reimbursement class]};
}

@end
