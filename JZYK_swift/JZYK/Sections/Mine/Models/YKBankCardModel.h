//
//  YKBankCardModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/17.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKBankCardModel : YKBaseModel

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *card_no;
@property (nonatomic, assign) long bank_id;
@property (nonatomic, copy) NSString *bank_name;
@property (nonatomic, copy) NSString *bank_background;
@property (nonatomic, copy) NSString *bank_logo;
@property (nonatomic, assign) BOOL is_main_card;
@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, copy) NSString *tag_msg;
@property (nonatomic, copy) NSString *card_type; //储蓄卡等（str）

@end
