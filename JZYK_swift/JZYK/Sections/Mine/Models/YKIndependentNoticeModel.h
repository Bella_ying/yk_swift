//
//  YKIndependentNoticeModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/22.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//  公告接口的model

#import "YKBaseModel.h"

@interface YKIndependentNoticeModel : YKBaseModel

@property (nonatomic, copy) NSString *noticeId; //存到本地判断的Id
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *position_id;
@property (nonatomic, copy) NSString *url;

@end
