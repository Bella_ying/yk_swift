//
//  YKNeedRepayModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/19.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKRepayListModel : NSObject

@property (nonatomic, copy) NSString *debt;         //应还金额
@property (nonatomic, copy) NSString *principal;    //本金
@property (nonatomic, copy) NSString *late_fee;     //滞纳金
@property (nonatomic, copy) NSString *plan_repayment_time; //还款日
@property (nonatomic, copy) NSString *repayment_time; //还款日期 插入日历用的
@property (nonatomic, copy) NSString *coupon_fee;    //抵扣金额
@property (nonatomic, copy) NSString *true_total_money; //已还金额
@property (nonatomic)       BOOL is_overdue;        //是否逾期  1逾期 0未逾期
@property (nonatomic, copy) NSString *overdue_day;  //逾期天数
@property (nonatomic, copy) NSString *service_fee; // 服务费
@property (nonatomic, copy) NSString *interests; // 利息
@property (nonatomic, assign) BOOL pay_state; //3.0 账单状态
//@property (nonatomic, copy) NSString *url;          //立即还款跳转链接
//@property (nonatomic, copy) NSString *detail_url;    //

@property (nonatomic, copy) NSString *counter_fee;
@property (nonatomic, copy) NSString *receipts;
@property (nonatomic, copy) NSString *plan_fee_time;
@property (nonatomic, copy) NSString *text_tip;

@property (nonatomic, copy) NSString *current_period; //分期数
@property (nonatomic, copy) NSString *repay_desc;     //还款日 yk- subDesc
@property (nonatomic, copy) NSString *repay_id;       //账单id
@property (nonatomic, copy) NSString *repay_time;     //还款时间
@property (nonatomic, copy) NSString *repay_money;    //当期金额  YK列表--金额（需要自己加单位）
@property (nonatomic, copy) NSString *status;         //账单状态
@property (nonatomic, copy) NSString *status_zh;      //账单状态名称 YK--状态
@property (nonatomic, assign) NSInteger cur_order; //1则显示当月账单

@property (nonatomic, copy) NSString *debit_money; //YK明细--金额（需要自己加单位）
@property (nonatomic, copy) NSString *repay_day; //YK明细--还款日
@property (nonatomic, copy) NSString *use_type;       //用途
@property (nonatomic, copy) NSString *repay_period;
@property (nonatomic, copy) NSString *order_state; //4.0 账单状态

@end

@interface Reimbursement : NSObject
/**图片*/
@property (nonatomic ,copy) NSString * img_url;
/**说明文字*/
@property (nonatomic ,copy) NSString * title;
/**头部说明文字*/
@property (nonatomic ,copy) NSString * link_url;

@end

@interface YKRepayTotalModel : NSObject

@property (nonatomic, copy) NSString *late_money; //yk--待还金额
@property (nonatomic, copy) NSString *money_amount; //总金额
@property (nonatomic, copy) NSString *total_period;  //*个月
@property (nonatomic, copy) NSString *order_title;
@property (nonatomic, copy) NSString *order_id;

@end



@interface YKNeedRepayModel : YKBaseModel

@property (nonatomic, copy) NSArray <YKRepayListModel *> *list;
@property (nonatomic, copy) NSArray <Reimbursement *> *pay_type;
@property (nonatomic, copy) NSString *pay_title;
@property (nonatomic, copy) NSDictionary *banner;
@property (nonatomic, strong) YKRepayTotalModel *total_data;
@property (nonatomic, copy) NSString *detail_url;    //详情
@property (nonatomic, copy) NSString *repay_url;
@property (nonatomic, copy) NSString *notice;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *repayment_url;//立即还款跳转链接
@property (nonatomic, assign) BOOL is_show_help; //是否到账单详情，账单详情是有帮助按钮的
@property (nonatomic, assign) BOOL is_overdue; //是否已逾期，用于判断添加已逾期的边框

@end
