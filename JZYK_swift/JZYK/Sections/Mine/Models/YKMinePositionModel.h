//
//  YKMinePositionModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/25.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKMinePositionModel : YKBaseModel

@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *skipCode;
@property (nonatomic, copy) NSString *skipH5;

//"position": {
//    "image": "http://jzyk-app-v1.dev.cs.jisuqianbao.com/res/JZYK/images/credit/user_info/position.png",
//    "skipCode": 4,
//    "skipH5": "http://www.baidu.com"
//}

@end
