//
//  DFImageLabel.m
//  WGJDFKit
//
//  Created by wangguanjun on 2017/12/8.
//  Copyright © 2017年 wangguanjun. All rights reserved.
//

#import "DFImageLabel.h"

#define kDefaultImageLArrangeStyle DFImageLArrangeStyleUpDown
#define kDefaultImageViewSize CGSizeZero
#define kDefaultLeftMargin 0
#define kDefaultRightMargin 0
#define kDefaultTopMargin 0
#define kDefaultBottomMargin 0

@interface DFImageLabel ()

@property (nonatomic, assign) DFImageLArrangeStyle privateImageLabelArrangeStyle;
@property (nonatomic, assign) CGSize privateImageViewSize;

@property (nonatomic, assign) CGFloat privateLeftMargin;
@property (nonatomic, assign) CGFloat privateRightMargin;
@property (nonatomic, assign) CGFloat privateTopMargin;
@property (nonatomic, assign) CGFloat privateBottomMargin;

@end

@implementation DFImageLabel

- (instancetype)init
{
    return [self initWithImageLabelArrangeStyle:kDefaultImageLArrangeStyle];
}

- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageViewLabelArrangeStyle{
    return [self initWithImageLabelArrangeStyle:imageViewLabelArrangeStyle imageViewSize:kDefaultImageViewSize];
}

- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageLabelArrangeStyle imageViewSize:(CGSize)imageViewSize {
    if (self = [super init]) {
        [self setupWithImageViewLabelArrangeStyle:imageLabelArrangeStyle imageViewSize:imageViewSize leftMargin:kDefaultLeftMargin rightMargin:kDefaultRightMargin topMargin:kDefaultTopMargin bottomMargin:kDefaultBottomMargin];
    }
    return self;
}

- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageViewLabelArrangeStyle leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin{
    return [self initWithImageLabelArrangeStyle:imageViewLabelArrangeStyle imageViewSize:kDefaultImageViewSize leftMargin:leftMargin rightMargin:rightMargin];
}
- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageLabelArrangeStyle imageViewSize:(CGSize)imageViewSize leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin{
    return [self initWithImageLabelArrangeStyle:imageLabelArrangeStyle imageViewSize:imageViewSize leftMargin:leftMargin rightMargin:rightMargin topMargin:kDefaultTopMargin bottomMargin:kDefaultBottomMargin];
}

- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageLabelArrangeStyle leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin{
    return [self initWithImageLabelArrangeStyle:imageLabelArrangeStyle imageViewSize:kDefaultImageViewSize leftMargin:leftMargin rightMargin:rightMargin topMargin:topMargin bottomMargin:bottomMargin];
}
- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageLabelArrangeStyle imageViewSize:(CGSize)imageViewSize leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin{
    if (self = [super init]) {
        [self setupWithImageViewLabelArrangeStyle:imageLabelArrangeStyle imageViewSize:imageViewSize leftMargin:leftMargin rightMargin:rightMargin topMargin:topMargin bottomMargin:bottomMargin];
    }
    return self;
}

- (void)setupWithImageViewLabelArrangeStyle:(DFImageLArrangeStyle)imageLabelArrangeStyle imageViewSize:(CGSize)imageViewSize leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin{
    
    self.privateImageLabelArrangeStyle = imageLabelArrangeStyle;
    self.privateImageViewSize = imageViewSize;
    self.privateLeftMargin = leftMargin;
    self.privateRightMargin = rightMargin;
    self.privateTopMargin = topMargin;
    self.privateBottomMargin = bottomMargin;
    [self addSubview:self.imageView];
    [self addSubview:self.label];
    
    [self addTarget:self action:@selector(clickSelf:) forControlEvents:(UIControlEventTouchUpInside)];
    
    //设置约束
    [self setupAllViewConstraintsWithLeftMargin:leftMargin rightMargin:rightMargin topMargin:topMargin bottomMargin:bottomMargin];
}

- (void)setupAllViewConstraintsWithLeftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin{
    switch (self.privateImageLabelArrangeStyle) {
        case DFImageLArrangeStyleUpDown:
        {
            [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.top.equalTo(self);
                if (self.privateImageViewSize.width > 0 && self.privateImageViewSize.height > 0) {
                    make.width.mas_equalTo(self.privateImageViewSize.width);
                    make.height.mas_equalTo(self.privateImageViewSize.height);
                }
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
            [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.top.equalTo(self.imageView.mas_bottom);
                make.bottom.equalTo(self);
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
        }
            break;
        case DFImageLArrangeStyleDownUp:
        {
            [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.top.equalTo(self);
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
            [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.top.equalTo(self.label.mas_bottom);
                if (self.privateImageViewSize.width > 0 && self.privateImageViewSize.height > 0) {
                    make.width.mas_equalTo(self.privateImageViewSize.width);
                    make.height.mas_equalTo(self.privateImageViewSize.height);
                }
                make.bottom.equalTo(self);
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
        }
            break;
        case DFImageLArrangeStyleLeftRight:
        {
            [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                CGFloat offsetY = (self.privateTopMargin - self.privateBottomMargin) * 0.5;
                make.centerY.equalTo(self).offset(offsetY);
                make.left.equalTo(self).offset(leftMargin);
                
                if (self.privateImageViewSize.width > 0 && self.privateImageViewSize.height > 0) {
                    make.width.equalTo(@(self.privateImageViewSize.width));
                    make.height.equalTo(@(self.privateImageViewSize.height));
                }
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
            [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.imageView.mas_right);//
                make.centerY.equalTo(self.imageView);
                make.right.mas_lessThanOrEqualTo(self);
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
        }
            break;
        case DFImageLArrangeStyleRightLeft:
        {
            [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
                CGFloat offsetY = (self.privateTopMargin - self.privateBottomMargin) * 0.5;
                make.left.equalTo(self).offset(leftMargin);
                make.centerY.equalTo(self).offset(offsetY);
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
            [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.label.mas_right);
                make.centerY.equalTo(self.label);
                make.right.mas_lessThanOrEqualTo(self);
                if (self.privateImageViewSize.width > 0 && self.privateImageViewSize.height > 0) {
                    make.width.equalTo(@(self.privateImageViewSize.width));
                    make.height.equalTo(@(self.privateImageViewSize.height));
                }
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
        }
            break;
            
        default:
            break;
    }
}

- (void)setImageLabelMargin:(CGFloat)imageLabelMargin
{
    _imageLabelMargin = imageLabelMargin;
    [self updateAllViewConstraints];
}


- (void)updateAllViewConstraints{
    switch (self.privateImageLabelArrangeStyle) {
        case DFImageLArrangeStyleUpDown:
        {
            [self.label mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.top.equalTo(self.imageView.mas_bottom).offset(self.imageLabelMargin);
                make.bottom.equalTo(self);
            }];
        }
            break;
        case DFImageLArrangeStyleDownUp:
        {
            [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.top.equalTo(self.label.mas_bottom).offset(self.imageLabelMargin);
                if (self.privateImageViewSize.width > 0 && self.privateImageViewSize.height > 0) {
                    make.width.mas_equalTo(self.privateImageViewSize.width);
                    make.height.mas_equalTo(self.privateImageViewSize.height);
                }
                make.bottom.equalTo(self);
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
        }
            break;
        case DFImageLArrangeStyleLeftRight:
        {
            [self.label mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.imageView.mas_right).offset(self.imageLabelMargin);
//                make.centerY.equalTo(self);
                make.right.mas_lessThanOrEqualTo(self);
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
        }
            break;
        case DFImageLArrangeStyleRightLeft:
        {
            [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.label.mas_right).offset(self.imageLabelMargin);
//                make.centerY.equalTo(self);
                make.right.mas_lessThanOrEqualTo(self);
                if (self.privateImageViewSize.width > 0 && self.privateImageViewSize.height > 0) {
                    make.width.equalTo(@(self.privateImageViewSize.width));
                    make.height.equalTo(@(self.privateImageViewSize.height));
                }
                make.width.mas_lessThanOrEqualTo(self);
                make.height.mas_lessThanOrEqualTo(self);
            }];
        }
            break;
            
        default:
            break;
    }

}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self invalidateIntrinsicContentSize];
}

- (CGSize)intrinsicContentSize
{
    switch (self.privateImageLabelArrangeStyle) {
        case DFImageLArrangeStyleUpDown:
        case DFImageLArrangeStyleDownUp:
        {
            CGFloat width = 0;
            CGFloat height = 0;

            height += self.imageLabelMargin;
            height += [self.label intrinsicContentSize].height;
            if (self.privateImageViewSize.height > 0 && self.privateImageViewSize.width > 0) {
                width += MAX(self.privateImageViewSize.width, [self.label intrinsicContentSize].width);
                height += self.privateImageViewSize.height;
            }else{
                width += MAX([self.imageView intrinsicContentSize].width, [self.label intrinsicContentSize].width);
                height += [self.imageView intrinsicContentSize].height;
            }
            return CGSizeMake(width, height);

        }
            break;

        case DFImageLArrangeStyleLeftRight:
        case DFImageLArrangeStyleRightLeft:
        {
            CGFloat heigth = 0;
            CGFloat width = 0;

            width += self.imageLabelMargin;
            width += [self.label intrinsicContentSize].width;
            if (self.privateImageViewSize.height > 0 && self.privateImageViewSize.width > 0) {
                width += self.privateImageViewSize.height;
                heigth += MAX(self.privateImageViewSize.height, [self.label intrinsicContentSize].height);
            }else{
                width += [self.imageView intrinsicContentSize].height;
                heigth += MAX([self.imageView intrinsicContentSize].height, [self.label intrinsicContentSize].height);
            }
            width += (self.privateLeftMargin + self.privateRightMargin);
            heigth += (self.privateTopMargin + self.privateBottomMargin);
            return CGSizeMake(width, heigth);
        }
            break;
            
        default:
            break;
    }
}

- (UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        
    }
    return _imageView;
}

- (UILabel *)label
{
    if (!_label) {
        _label = [[UILabel alloc] init];
    }
    return _label;
}

- (void)clickSelf:(DFImageLabel *)control{
    if (self.clickBlock) {
        self.clickBlock(control);
    }
}

@end
