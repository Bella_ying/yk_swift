//
//  DFImageLabel.h
//  WGJDFKit
//
//  Created by wangguanjun on 2017/12/8.
//  Copyright © 2017年 wangguanjun. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, DFImageLArrangeStyle) {
    DFImageLArrangeStyleUpDown, //默认
    DFImageLArrangeStyleDownUp,
    DFImageLArrangeStyleLeftRight,
    DFImageLArrangeStyleRightLeft
};
/**
 *主要针对固定的imageView的大小，或者自适应大小的 一个Image 和一个label的组合, 更多的其他的效果，可用已写好的分类
 *如果image label其中一个为空，则显示margin大小的空白，若固定imageView的size,size也会显示
 */
@class DFImageLabel;
typedef void (^ClickEvent)(DFImageLabel *control);
@interface DFImageLabel : UIControl

- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageViewLabelArrangeStyle;

- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageLabelArrangeStyle imageViewSize:(CGSize)imageViewSize;

/**
 * 目前只针对DFImageLArrangeStyleLeftRight或者DFImageLArrangeStyleRightLeft 设置左右边距
 */
- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageViewLabelArrangeStyle leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin;
/**
 * 目前只针对DFImageLArrangeStyleLeftRight或者DFImageLArrangeStyleRightLeft 设置左右边距
 * 固定图片大小
 */
- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageLabelArrangeStyle imageViewSize:(CGSize)imageViewSize leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin;
/**
 * 目前只针对DFImageLArrangeStyleLeftRight或者DFImageLArrangeStyleRightLeft 设置左右上下边距
 */
- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageLabelArrangeStyle leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin;
/**
 * 目前只针对DFImageLArrangeStyleLeftRight或者DFImageLArrangeStyleRightLeft 设置左右上下边距
 * 固定图片大小
 */
- (instancetype)initWithImageLabelArrangeStyle:(DFImageLArrangeStyle)imageLabelArrangeStyle imageViewSize:(CGSize)imageViewSize leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin;

@property (nonatomic, copy) ClickEvent clickBlock;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, assign) CGFloat imageLabelMargin;

@end
