//
//  YKPlaceholderTextView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKPlaceholderTextView.h"

@interface YKPlaceholderTextView ()
@property (nonatomic, assign) CGFloat originX;
@property (nonatomic, assign) CGFloat holderWidth;

@end

@implementation YKPlaceholderTextView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

- (void)textDidChange:(NSNotification *)note
{
    // 会重新调用drawRect:方法
    [self setNeedsDisplay];
}

- (void)setup{
    // 设置默认字体
    self.font = [UIFont systemFontOfSize:14];
    
    // 设置默认颜色
    self.placeholderColor = [UIColor grayColor];
    
    // 使用通知监听文字改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextViewTextDidChangeNotification object:self];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)yk_setOriginX:(CGFloat)x holderWidth:(CGFloat)width
{
    self.originX = x;
    self.holderWidth = width;
}

/**
 * 每次调用drawRect:方法，都会将以前画的东西清除掉
 */
- (void)drawRect:(CGRect)rect
{
    // 如果有文字，就直接返回，不需要画占位文字
    if (self.hasText) return;
    
    // 属性
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = self.font;
    attrs[NSForegroundColorAttributeName] = self.placeholderColor;
    
    // 画文字
    if (self.originX) {
        rect.origin.x = self.originX;
    }else{
        rect.origin.x = 5;
    }
    if (self.holderWidth) {
        rect.size.width = self.holderWidth;
    }else{
        rect.size.width -= 2 * rect.origin.x;
    }
//    rect.origin.x = 38;
    rect.origin.y = 8;
////    rect.size.width -= 2 * rect.origin.x;
//    rect.size.width -= 2 * 5;
    [self.placeholder drawInRect:rect withAttributes:attrs];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self setNeedsDisplay];
}

#pragma mark - setter
- (void)setPlaceholder:(NSString *)placeholder
{
    _placeholder = [placeholder copy];
    
    [self setNeedsDisplay];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor
{
    _placeholderColor = placeholderColor;
    
    [self setNeedsDisplay];
}

- (void)setFont:(UIFont *)font
{
    [super setFont:font];
    
    [self setNeedsDisplay];
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    
    [self setNeedsDisplay];
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    [super setAttributedText:attributedText];
    
    [self setNeedsDisplay];
}

@end
