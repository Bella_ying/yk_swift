//
//  UIView+YKHud.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/20.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DFImageLabel.h"
@interface UIView (YKHud)

@property (nonatomic, strong, setter=df_setHud:) UIView *df_hud;
//设置显示的图片等移动的位置
@property (nonatomic, assign, setter=df_setHudCenterOffset:) CGPoint df_hudCenterOffset;

//隐藏hud_view
- (void)df_hideHud;
//自定义的hud_view
+ (DFImageLabel *)df_normalHudViewWithTitle:(NSString*)title andImage:(UIImage*)image;
+ (DFImageLabel *)df_normalHudViewWithTitle:(NSString *)title textColor:(UIColor *)textColor font:(UIFont *)font andImage:(UIImage *)image titleImageMargin:(CGFloat)margin;
//默认的hud_view
+ (DFImageLabel *)df_defaultNoDataHudViewWithTitle:(NSString *)title;
+ (DFImageLabel *)df_defaultNoNetHudViewWithTitle:(NSString *)title;
/**
 * 完全自定义下部内容的空视图
 * 外部自定义view设置约束时，要设置类似make.bottom.equalTo(allHudView.mas_bottom)代码;
 */
+ (UIView *)df_normalHudViewWithTitle:(NSString *)title textColor:(UIColor *)textColor font:(UIFont *)font andImage:(UIImage *)image titleImageMargin:(CGFloat)imageTitleMargin addCustomView:(void (^)(UIView *allHudView, UIView *topHudView))customBlock;
/**
 * 自定义下部内容的空视图(不用设置约束，默认左右居中)
 * isLess ： 自定义的view的宽度是否需要小于最大300的backView的宽度，设置NO则不限制，customView可以出父视图的边界(可以保持设置的尺寸)
 */
+ (UIView *)df_normalHudViewWithTitle:(NSString *)title textColor:(UIColor *)textColor font:(UIFont *)font andImage:(UIImage *)image titleImageMargin:(CGFloat)imageTitleMargin customViewTopMargin:(CGFloat)customViewMargin customWidthLessAllHudView:(BOOL)isLess addCenterXCustomView:(UIView * (^)(void))customBlock;

//带有button的hud_view
+ (UIView *)df_defaultNoNetHudViewWithTitle:(NSString *)title btnBlock:(void(^)(UIButton *btn))btnAction;
+ (UIView *)df_defaultNoNetHudViewWithTitle:(NSString *)title imageTitleMargin:(CGFloat)imageTitleMargin btnMargin:(CGFloat)btnMargin btnBlock:(void(^)(UIButton *btn))btnAction;
@end

NS_INLINE DFImageLabel *UIMakeImageLabel(NSString *title, UIImage *image, UIColor *titleColor, UIFont *titleFont) {
    DFImageLabel *imageLabel = [[DFImageLabel alloc] initWithImageLabelArrangeStyle:(DFImageLArrangeStyleUpDown)];
    imageLabel.imageLabelMargin = 5;
    imageLabel.imageView.image = image;
    imageLabel.label.text = title;
    imageLabel.label.textColor = titleColor;
    imageLabel.label.font = titleFont;
    imageLabel.label.numberOfLines = 0;
    imageLabel.label.textAlignment = NSTextAlignmentCenter;

    return imageLabel;
}
NS_INLINE UIButton *UIMakeButton(NSString *title, UIColor *titleColor, UIFont *titleFont, CGFloat borderWidth,UIColor *borderColor, CGFloat cornerRadius) {
    UIButton *button = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [button setTitle:title forState:(UIControlStateNormal)];
    button.titleLabel.font = titleFont;
    [button setTitleColor:titleColor forState:(UIControlStateNormal)];
    if (borderWidth > 0) {
        button.layer.borderWidth = borderWidth;
        button.layer.borderColor = borderColor.CGColor;
    }
    if (cornerRadius > 0) {
        button.layer.cornerRadius = cornerRadius;
        button.layer.masksToBounds = YES;
    }
    return button;
}
