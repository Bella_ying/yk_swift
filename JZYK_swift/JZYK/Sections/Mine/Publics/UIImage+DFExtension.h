//
//  UIImage+DFExtension.h
//  WGJDFKit
//
//  Created by wangguanjun on 2017/9/12.
//  Copyright © 2017年 wangguanjun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (DFExtension)
/**
 *制作image
 */
+ (UIImage *)df_imageWithColor:(UIColor *)color size:(CGSize)size alpha:(float)alpha;

/**
 *把image上的颜色做修改
 */
- (UIImage *)df_tintedImageWithColor:(UIColor*)color;

- (UIImage *)df_tintedImageWithColor:(UIColor*)color alpha:(CGFloat)alpha;

- (UIImage *)df_tintedImageWithColor:(UIColor*)color rect:(CGRect)rect;

- (UIImage *)df_tintedImageWithColor:(UIColor*)color rect:(CGRect)rect alpha:(CGFloat)alpha;

@end
