//
//  YKPlaceholderTextView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKPlaceholderTextView : UITextView
/** 占位文字 */
@property (nonatomic, copy) NSString *placeholder;
/** 占位文字颜色 */
@property (nonatomic, strong) UIColor *placeholderColor;

- (void)yk_setOriginX:(CGFloat)x holderWidth:(CGFloat)width;
@end
