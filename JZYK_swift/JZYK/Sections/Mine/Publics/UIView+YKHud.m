//
//  UIView+YKHud.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/20.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "UIView+YKHud.h"
#import "UIButton+category.h"
@implementation UIView (YKHud)

#pragma -mark setter-----
- (void)df_setHud:(UIView *)df_hud {
    if (self.df_hud) {
        [self.df_hud removeFromSuperview];
    }
    objc_setAssociatedObject(self, @selector(df_hud), df_hud, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self addSubview:self.df_hud];
    [self.df_hud mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self).centerOffset(self.df_hudCenterOffset);
        make.width.mas_lessThanOrEqualTo(300);
    }];
    
}

- (void)df_setHudCenterOffset:(CGPoint)df_hudCenterOffset {
    objc_setAssociatedObject(self, @selector(df_hudCenterOffset), [NSValue valueWithCGPoint:df_hudCenterOffset], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.df_hud.superview) {
        [self.df_hud mas_updateConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self).centerOffset(self.df_hudCenterOffset);
        }];
    }
}

#pragma -mark getter-----

- (UIView *)df_hud {
    return objc_getAssociatedObject(self, _cmd);
}

- (CGPoint)df_hudCenterOffset {
    return [objc_getAssociatedObject(self, _cmd) CGPointValue];
}

#pragma -mark other-----

- (void)df_hideHud {
    [self.df_hud removeFromSuperview];
}

+ (DFImageLabel *)df_defaultNoDataHudViewWithTitle:(NSString *)title
{
    return [UIView df_normalHudViewWithTitle:title textColor:LABEL_TEXT_COLOR font:[UIFont systemFontOfSize:adaptFontSize(13)] andImage:[UIImage imageNamed:@"img_wuyouhuiquan"] titleImageMargin:5];
}
+ (DFImageLabel *)df_defaultNoNetHudViewWithTitle:(NSString *)title
{
    return [UIView df_normalHudViewWithTitle:title textColor:LABEL_TEXT_COLOR font:[UIFont systemFontOfSize:adaptFontSize(13)] andImage:[UIImage imageNamed:@"img_wuwifi"] titleImageMargin:5];
}

+ (DFImageLabel *)df_normalHudViewWithTitle:(NSString *)title andImage:(UIImage *)image {
    
    DFImageLabel *emptyHud = UIMakeImageLabel(title, image, [UIColor yk_colorWithHexString:@"#666666"], [UIFont systemFontOfSize:adaptFontSize(13)]);
    return emptyHud;
}

+ (DFImageLabel *)df_normalHudViewWithTitle:(NSString *)title textColor:(UIColor *)textColor font:(UIFont *)font andImage:(UIImage *)image titleImageMargin:(CGFloat)margin{
    DFImageLabel *emptyHud = UIMakeImageLabel(title, image, textColor, font);
    emptyHud.imageLabelMargin = margin;
    return emptyHud;
}
+ (UIView *)df_normalHudViewWithTitle:(NSString *)title textColor:(UIColor *)textColor font:(UIFont *)font andImage:(UIImage *)image titleImageMargin:(CGFloat)imageTitleMargin addCustomView:(void (^)(UIView *allHudView, UIView *topHudView))customBlock{
    
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = [UIColor clearColor];
    DFImageLabel *emptyHud = UIMakeImageLabel(title, image, textColor, font);
    emptyHud.imageLabelMargin = imageTitleMargin;
    [backView addSubview:emptyHud];
    [emptyHud mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView.mas_top);
        make.centerX.equalTo(backView);
        make.width.lessThanOrEqualTo(backView);
    }];
    
    if (customBlock) {
        customBlock(backView,emptyHud);
    }else{
        [emptyHud mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(backView.mas_bottom);
        }];
    }
    return backView;
}
+ (UIView *)df_normalHudViewWithTitle:(NSString *)title textColor:(UIColor *)textColor font:(UIFont *)font andImage:(UIImage *)image titleImageMargin:(CGFloat)imageTitleMargin customViewTopMargin:(CGFloat)customViewMargin customWidthLessAllHudView:(BOOL)isLess addCenterXCustomView:(UIView * (^)(void))customBlock
{
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = [UIColor clearColor];
    DFImageLabel *emptyHud = UIMakeImageLabel(title, image, textColor, font);
    emptyHud.imageLabelMargin = imageTitleMargin;
    [backView addSubview:emptyHud];
    [emptyHud mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView.mas_top);
        make.centerX.equalTo(backView);
        make.width.lessThanOrEqualTo(backView);
    }];
    
    if (customBlock) {
        UIView *customView = customBlock();
        [backView addSubview:customView];
        [customView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(emptyHud.mas_bottom).offset(customViewMargin);
            make.centerX.equalTo(backView);
            if (isLess) {
                make.width.lessThanOrEqualTo(backView);
            }
            make.bottom.equalTo(backView);
        }];
    }else{
        [emptyHud mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(backView.mas_bottom);
        }];
    }
    return backView;
}


+ (UIView *)df_normalHudViewWithTitle:(NSString *)title textColor:(UIColor *)textColor font:(UIFont *)font andImage:(UIImage *)image titleImageMargin:(CGFloat)imageTitleMargin btnMargin:(CGFloat)btnMargin btnAction:(void(^)(UIButton *))btnAction{
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = [UIColor clearColor];
    DFImageLabel *emptyHud = UIMakeImageLabel(title, image, textColor, font);
    emptyHud.imageLabelMargin = imageTitleMargin;
    [backView addSubview:emptyHud];
    [emptyHud mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView.mas_top);
        make.centerX.equalTo(backView);
        make.width.lessThanOrEqualTo(backView);
    }];
    
    CGFloat btnHeight = 30;
    CGFloat btnWidth = 130;
    UIButton *btn = UIMakeButton(@"点击刷新", MAIN_THEME_COLOR, [UIFont systemFontOfSize:15], 1, MAIN_THEME_COLOR, btnHeight * 0.5);
    [backView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(btnHeight);
        make.width.mas_equalTo(btnWidth);
        make.top.equalTo(emptyHud.mas_bottom).offset(btnMargin);
        make.bottom.equalTo(backView);
        make.centerX.equalTo(backView);
    }];
    [btn yk_handleControlEvent:(UIControlEventTouchUpInside) withBlock:^(UIButton *button) {
        if (btnAction) {
            btnAction(button);
        }
    }];
    return backView;
}
+ (UIView *)df_defaultNoNetHudViewWithTitle:(NSString *)title btnBlock:(void(^)(UIButton *btn))btnAction
{
    return [self df_defaultNoNetHudViewWithTitle:title imageTitleMargin:5 btnMargin:15 btnBlock:btnAction ];
}
+ (UIView *)df_defaultNoNetHudViewWithTitle:(NSString *)title imageTitleMargin:(CGFloat)imageTitleMargin btnMargin:(CGFloat)btnMargin btnBlock:(void(^)(UIButton *btn))btnAction
{
    return [UIView df_normalHudViewWithTitle:title textColor:GRAY_COLOR_8D font:[UIFont systemFontOfSize:adaptFontSize(13)] andImage:[UIImage imageNamed:@"img_wuwifi"] titleImageMargin:imageTitleMargin btnMargin:btnMargin btnAction:btnAction];
}


- (void)df_showDefaultHudWithTitle:(NSString *)title defaultImage:(UIImage *)image hudCenter:(CGPoint)point dataArray:(NSArray *)dataArray{
    if ([self.class isSubclassOfClass:[UITableView class]]) {
        UITableView *tableView = (UITableView *)self;
        if ([dataArray isKindOfClass:[NSMutableArray class]]) {
            if (dataArray.count > 0) {
                [((NSMutableArray *)dataArray) removeAllObjects];
            }
        }
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
        if (tableView.df_hud) {
            [tableView df_hideHud];
        }
        tableView.df_hud = [UIButton df_normalHudViewWithTitle:title andImage:image];
        tableView.df_hudCenterOffset = point;
        
    }
    
}


@end
