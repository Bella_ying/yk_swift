//
//  YKDiscountTicketVC.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"

@interface YKDiscountTicketVC : YKBaseViewController

//页面类型：0：未使用    1：已使用或已过期（历史券）
@property (nonatomic, assign) NSInteger type;

@end
