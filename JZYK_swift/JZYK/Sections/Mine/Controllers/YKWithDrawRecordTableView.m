//
//  YKWithDrawRecordTableView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/29.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKWithDrawRecordTableView.h"
#import "YKWithDrawRecordListCell.h"

#define kPageSize 10
@interface YKWithDrawRecordTableView ()

@property (nonatomic, strong) UIView *tableViewHeadView;

@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, assign) NSInteger page;
@end

@implementation YKWithDrawRecordTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    if (self = [super initWithFrame:frame style:style]) {
        [self setup];
    }
    return self;
}

// 列表渲染
- (void)setup
{
    self.listArr = @[].mutableCopy;
    self.page = 1;
    self.backgroundColor = GRAY_BACKGROUND_COLOR;
    //    ADJUST_SCROLL_VIEW_INSET(self, self.drawListTableView);
    WEAK_SELF
    [self hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        tableMaker.hd_tableViewHeaderView(^(){
            return weakSelf.tableViewHeadView;
        });
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            
            [sectionMaker.hd_dataArr(^() {
                return weakSelf.listArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKWithDrawRecordListCell))
                .hd_rowHeight(68)
                .hd_adapter(^(YKWithDrawRecordListCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                     if (self.recordStyle == YKWithDrawRecordStyleActivity) { //活动
                         YKWithDrawRecordActivityModel *model = weakSelf.listArr[indexPath.row];
                         [cell setWithDrawActivityModel:model];
                     }else{
                         YKWithDrawRecordModel *model = weakSelf.listArr[indexPath.row];
                         [cell setWithDrawModel:model];
                     }

                });
                
                cellMaker.hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    DLog(@"%ld",indexPath.row);
                });
            }];
        }];
    }];
    
    [self hd_addFreshHeader:^{
        self.page = 1;
        [self fetchWithDrawRecordDataWithRefreshMore:NO];
    }];

    self.mj_footer = [YKRefreshFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self fetchWithDrawRecordDataWithRefreshMore:YES];
    }];
    
}

- (void)fetchWithDrawRecordDataWithRefreshMore:(BOOL)isFreshMore
{
    NSDictionary *paramDict = @{@"page":[NSString stringWithFormat:@"%@",@(self.page)],@"page_size":[NSString stringWithFormat:@"%@",@(kPageSize)]};
    if (self.recordStyle == YKWithDrawRecordStyleActivity) { //活动
        DLog(@"wgj_活动");
        WEAK_SELF
        [[HTTPManager session] getRequestForKey:kUserInviteRedPackPopMoneyList showLoading:NO param:paramDict succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
            STRONG_SELF
            [strongSelf df_hideHud];
            if (code == 0 && success) {
                DLog(@"====%@",jsonDict);
                if (!isFreshMore) {
                    [self.listArr removeAllObjects];
                }
                
                NSArray *arr = jsonDict[@"list"];
                for (int i = 0; i < arr.count; i++) {
                    YKWithDrawRecordActivityModel *model = [YKWithDrawRecordActivityModel yy_modelWithJSON:arr[i]];
                    [strongSelf.listArr addObject:model];
                }

                if (strongSelf.listArr.count == 0) {
                    strongSelf.df_hud = [UIView df_normalHudViewWithTitle:@"你还没有提现记录哦~" textColor:GRAY_COLOR_AD font:[UIFont systemFontOfSize:adaptFontSize(13)] andImage:[UIImage imageNamed:@"img_wujilu"] titleImageMargin:5];
                    strongSelf.mj_footer.hidden = YES;
                }else{
                    if (arr.count < kPageSize) {
                        strongSelf.mj_footer.hidden = YES;
                    }else{
                        strongSelf.mj_footer.hidden = NO;
                    }
                }
            }else{
                [[iToast makeText:message] show];
            }

            [strongSelf reloadData];
            [strongSelf hd_endFreshing:YES];
        } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
            STRONG_SELF
            [strongSelf hd_endFreshing:YES];
            [[iToast makeText:errMsg] show];
        }];
    }else{ //取现
        DLog(@"wgj_取现");
        WEAK_SELF
        [[HTTPManager session] getRequestForKey:kCreditUserWithdrawLog showLoading:NO param:paramDict succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
            STRONG_SELF
            [strongSelf df_hideHud];
            if (code == 0 && success) {
                DLog(@"====%@",jsonDict);
                if (!isFreshMore) {
                    [self.listArr removeAllObjects];
                }
                
                NSArray *arr = jsonDict[@"log_list"];
                for (int i = 0; i < arr.count; i++) {
                    YKWithDrawRecordModel *model = [YKWithDrawRecordModel yy_modelWithJSON:arr[i]];
                    [strongSelf.listArr addObject:model];
                }

                if (strongSelf.listArr.count == 0) {
                    strongSelf.df_hud = [UIView df_normalHudViewWithTitle:@"你还没有提现记录哦~" textColor:GRAY_COLOR_AD font:[UIFont systemFontOfSize:adaptFontSize(13)] andImage:[UIImage imageNamed:@"img_wujilu"] titleImageMargin:5];
                    strongSelf.mj_footer.hidden = YES;
                }else{
                    if (arr.count < kPageSize) {
                        strongSelf.mj_footer.hidden = YES;
                    }else{
                        strongSelf.mj_footer.hidden = NO;
                    }
                }
            }else{
                [[iToast makeText:message] show];
            }
            
            [strongSelf reloadData];
            [strongSelf hd_endFreshing:YES];
        } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
            STRONG_SELF
            [strongSelf hd_endFreshing:YES];
            [[iToast makeText:errMsg] show];
        }];
    }

    
}



- (UIView *)tableViewHeadView
{
    if (!_tableViewHeadView){
        _tableViewHeadView = [[UIView alloc] init];
        _tableViewHeadView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 10);
        _tableViewHeadView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _tableViewHeadView;
}


@end
