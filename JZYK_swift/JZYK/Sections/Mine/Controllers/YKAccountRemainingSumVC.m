//
//  YKAccountRemainingSumVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAccountRemainingSumVC.h"
#import "YKAccountNavView.h"
#import "YKAccountRemainView.h"
#import "YKAccountRemainCell.h"
#import "YKDrawMoneyVC.h"
//#import "YKWithDrawRecordList.h"
#import "YKWithDrawRecordPageVC.h"

static const CGFloat accountHeaderViewHeight   = 186.f;

@interface YKAccountRemainingSumVC ()
//@property (weak, nonatomic) IBOutlet UITableView *accountTableView;
@property (nonatomic, strong) UITableView *tableView;
@property (strong, nonatomic) YKAccountNavView *accountNavigationView;
@property (strong, nonatomic) YKAccountRemainView     *accountHeadView;

@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *drawBtn;
@property (nonatomic, strong) UIButton *drawIntroduceBtn;

@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, copy) NSString *withdraw_url;
@property (nonatomic, copy) NSString *withdraw_instructions;

@property (nonatomic, strong) YKEmptyView *emptyView;
@end

@implementation YKAccountRemainingSumVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isStatusLight = YES;
    
    self.listArr = @[].mutableCopy;
    
    [self prepareUI];
    [self prepareTableView];

    [self.tableView.mj_header beginRefreshing];
}

- (void)fetchAccountListData
{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kcreditUserAccountBalance showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        [strongSelf.tableView df_hideHud];
        if (code == 0 && success) {
            DLog(@"====%@",jsonDict);
            [strongSelf.listArr removeAllObjects];
            NSArray *arr = jsonDict[@"balance_list"];
            for (int i = 0; i < arr.count; i++) {
                YKMineAccountRemainModel *model = [YKMineAccountRemainModel yy_modelWithJSON:arr[i]];
                [strongSelf.listArr addObject:model];
            }
            NSString *total_balance = [NSString stringWithFormat:@"%@",jsonDict[@"total_balance"]?:@""];
            strongSelf.accountHeadView.availableNumLabel.text = [NSString yk_numberToCommaSymbolformat:total_balance];
            strongSelf.withdraw_url = jsonDict[@"withdraw_url"]?:@"";
            strongSelf.withdraw_instructions = jsonDict[@"withdraw_instructions"]?:@"";
            //缺省页
            if (strongSelf.listArr.count == 0) {
                strongSelf.tableView.df_hud = [UIView df_normalHudViewWithTitle:@"一分钱也没有了~" textColor:GRAY_COLOR_AD font:[UIFont systemFontOfSize:adaptFontSize(13)] andImage:[UIImage imageNamed:@"img_noMoney"] titleImageMargin:5];
                strongSelf.tableView.df_hudCenterOffset = CGPointMake(0, 70 * ASPECT_RATIO_WIDTH);
                strongSelf.bottomView.hidden = YES;
            }else{
                strongSelf.bottomView.hidden = NO;
            }
        }else{
            [[iToast makeText:message] show];
        }
        
        [strongSelf.tableView reloadData];
        [strongSelf.tableView hd_endFreshing:YES];
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        [strongSelf.tableView hd_endFreshing:YES];
        [[iToast makeText:errMsg] show];
        
    }];
    
}


// 页面渲染
- (void)prepareUI
{
    self.showNavigationBar = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.accountNavigationView];
    [self.accountNavigationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.mas_offset(NAV_HEIGHT);
    }];
    self.accountNavigationView.navTitleLabel.text = @"账户余额";

    [self.bottomView addSubview:self.drawBtn];
    [self.bottomView addSubview:self.drawIntroduceBtn];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(100 + 50 * ASPECT_RATIO_WIDTH);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    [self.drawIntroduceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bottomView);
        make.bottom.mas_equalTo(self.bottomView).offset(-30 * ASPECT_RATIO_WIDTH);
    }];
    [self.drawBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bottomView);
        make.bottom.mas_equalTo(self.drawIntroduceBtn.mas_top).offset(-20 * ASPECT_RATIO_WIDTH);
    }];
    [self.drawBtn addTarget:self action:@selector(drawBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.drawIntroduceBtn addTarget:self action:@selector(drawInfoAction:) forControlEvents:(UIControlEventTouchUpInside)];
    

}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.tableView);
    WEAK_SELF
    [self.tableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        [tableMaker.hd_sectionCount(1).hd_tableViewHeaderView(^() {
            return weakSelf.accountHeadView;
        }) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            [sectionMaker.hd_dataArr(^() {
                return weakSelf.listArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKAccountRemainCell))
                .hd_rowHeight(68)
                .hd_adapter(^(YKAccountRemainCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                    YKMineAccountRemainModel *model = self.listArr[indexPath.row];
                    [cell setAccountRemainListModel:model];
                    if (indexPath.row == self.listArr.count - 1) {
                        cell.lineView.hidden = YES;
                    }else{
                        cell.lineView.hidden = NO;
                    }
                })
                .hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    
                });
            }];
        }];
        
        tableMaker.hd_scrollViewDidScroll(^(UIScrollView * scrollView) {
            CGFloat offsetY = scrollView.contentOffset.y;
            if (offsetY < 0) {
                weakSelf.accountNavigationView.hidden = YES;
            }else if(offsetY == 0){
                weakSelf.accountNavigationView.hidden = NO;
                weakSelf.accountNavigationView.navTitleLabel.text = @"账户余额";
                weakSelf.accountNavigationView.backgroundColor = [UIColor clearColor];
            }else{
                weakSelf.accountNavigationView.hidden = NO;
                 weakSelf.accountNavigationView.backgroundColor = MAIN_THEME_COLOR;
            }

        });
    }];
    [self.tableView hd_addFreshHeader:^{
        [self fetchAccountListData];
    }];
    
    self.accountNavigationView.yk_backBlock = ^{
        DLog(@"111");
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    self.accountNavigationView.yk_rightBlock = ^{
        DLog(@"222");
        //调到提现记录页面
//        YKWithDrawRecordList *drawListVC = [[YKWithDrawRecordList alloc] init];
//        [weakSelf.navigationController pushViewController:drawListVC animated:YES];
        YKWithDrawRecordPageVC *drawListVC = [[YKWithDrawRecordPageVC alloc] init];
        [weakSelf.navigationController pushViewController:drawListVC animated:YES];
    };
}

#pragma mark- aciton
 - (void)drawBtnAction:(UIButton *)btn{
     DLog(@"drawBtnAction");
//     YKDrawMoneyVC *drawVC = [[YKDrawMoneyVC alloc] init];
//     [self.navigationController pushViewController:drawVC animated:YES];
     YKBrowseWebController *webVC = [[YKBrowseWebController alloc] init];
     webVC.url = self.withdraw_url;
     [self.navigationController pushViewController:webVC animated:YES];
 }
 - (void)drawInfoAction:(UIButton *)btn{
     DLog(@"drawInfoAction——提现说明");
     [[QLAlert alert] prepareUIWithTitle:@"提现说明" titleIsHtml:NO contentText:[NSString yk_addAttributeWithHtml5String:self.withdraw_instructions] contentIsHtml:YES buttonTitleArray:@[@"知道了"] buttonClicked:^(NSInteger index) {
     }];
//     [[QLAlert alert] prepareUIWithContentText:[NSString stringWithFormat:@"%@\n",self.withdraw_instructions] buttonTitleArray:@[@"知道了"] buttonClicked:^(NSInteger index) {
//     }];
     [[QLAlert alert] setContentTextAlignement:NSTextAlignmentLeft];
     [[QLAlert alert] show];

 }

#pragma mark- 懒加载
- (YKAccountNavView *)accountNavigationView
{
    if (!_accountNavigationView) {
        _accountNavigationView = [[YKAccountNavView alloc] initCustomStyle:(YKRightItemStyleAccountRemain)];
        _accountNavigationView.backgroundColor = [UIColor clearColor];
    }
    return _accountNavigationView;
}

- (YKAccountRemainView *)accountHeadView{
    if (!_accountHeadView) {
        _accountHeadView = [[YKAccountRemainView alloc] init];
        _accountHeadView.availableTitleLabel.text = @"可用金额（元）";
        _accountHeadView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, accountHeaderViewHeight * ASPECT_RATIO_WIDTH + iPhoneX_ADD_NAV_HEIGHT);
    }
    return _accountHeadView;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        
    }
    return _tableView;
}

- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _bottomView;
}

- (UIButton *)drawBtn
{
    if (!_drawBtn) {
        _drawBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"system_commit_btn"];
        [_drawBtn setBackgroundImage:image forState:(UIControlStateNormal)];
        [_drawBtn setTitle:@"立即提现" forState:(UIControlStateNormal)];
        _drawBtn.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(17)];
        [_drawBtn setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
        _drawBtn.layer.cornerRadius = image.size.height * 0.5;
        _drawBtn.layer.masksToBounds = YES;
    }
    return _drawBtn;
}
- (UIButton *)drawIntroduceBtn
{
    if (!_drawIntroduceBtn) {
        _drawIntroduceBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_drawIntroduceBtn setTitle:@"提现说明" forState:(UIControlStateNormal)];
        _drawIntroduceBtn.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        [_drawIntroduceBtn setTitleColor:MAIN_THEME_COLOR forState:(UIControlStateNormal)];
    }
    return _drawIntroduceBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
