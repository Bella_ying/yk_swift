//
//  YKBankCardListVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBankCardListVC.h"
#import "YKBankCardListCell.h"
#import "YKAddBankCardViewController.h"
#import "YKPaoMaNoticeView.h"
#import "YKCunGunTools.h"
#import "YKCodeAlertView.h"

@interface YKBankCardListVC ()
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *tableFootView;
@property (nonatomic, strong) UIButton *addBankCardBtn;
@property (nonatomic, strong) UIButton *emptyAddBankCardBtn;
@property (nonatomic, strong) NSMutableArray *bankCardListArr;
//
@property (nonatomic, strong) YKPaoMaNoticeView *noticeView;

@end

@implementation YKBankCardListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的银行卡";
    
    self.bankCardListArr = @[].mutableCopy;
    
    [self prepareUI];
    [self prepareTableView];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(id)userInfo
{
    if ([eventName isEqualToString:kEventWarningAction]) {
        //异常弹框
        YKBankCardModel *model = userInfo;
        [[QLAlert alert] showWithMessage:@"本卡存在异常，导致无法给您顺利打款，建议更换其他银行卡" btnTitleArray:@[@"解除绑定",@"取消"] btnClicked:^(NSInteger index) {
            if (index == 0) {
                //解除绑定接口
                [self yk_relieveCardRequestWithID:model.ID];

            }
        }];
    }
}

#pragma -mark 设为主卡
- (void)yk_setMainCardRequestWithID:(NSString *)cardId cardNo:(NSString *)card_no{
    
    YKCodeAlertView *alertView = [[YKCodeAlertView alloc] initWithAlertStyle:YKCodeAlertViewStyleSwitchBankCardMessageCode];
    [self.view.window addSubview:alertView];
    [alertView yk_setBankCardId:cardId card_no:card_no];
    alertView.commitMessageCodeBlock = ^(NSString *contentText) {
        DLog(@"mess:%@",contentText);
        if (![contentText yk_isValidString]) {
            [[iToast makeText:@"请输入验证码"] show];
            return ;
        }
        NSDictionary *paramDic = @{@"card_id":cardId,@"code":[contentText yk_isValidString]?contentText:@""};
        [[HTTPManager session] postRequestForKey:kCreditCardSetMainCard showLoading:YES param:paramDic succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
            WEAK_SELF
            if (code == 0) {
                [weakSelf fetchBankCardListData];
            }else{
                [[iToast makeText:message] show];
            }
            
        } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
            [[iToast makeText:errMsg] show];
        }];
    };
    

}
#pragma -mark 解除绑定
- (void)yk_relieveCardRequestWithID:(NSString *)cardId{
    WEAK_SELF
    NSDictionary *paramDic = @{@"card_id":cardId};
    [[HTTPManager session] postRequestForKey:kCreditCardUnbindCard showLoading:YES param:paramDic succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        if (code == 0) {
            [weakSelf fetchBankCardListData];
        }else{
            [[iToast makeText:message] show];
        }
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

- (void)fetchBankCardListData
{
    [[HTTPManager session] postRequestForKey:kCreditCardCardList showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        [self.tableView df_hideHud];
        if (code == 0 && success) {
            DLog(@"====%@",jsonDict);
//            //test
//            jsonDict = @{@"card_list":@[@{ @"id": @(161706),
//                @"card_no": @"**** **** **** 8931",
//                @"bank_id": @(2),
//                @"bank_name": @"农业银行",
//                @"bank_background": @"http://local.jzyk.com/res/JZYK/images/credit/bank/background_2_2.png",
//                @"bank_logo": @"http://local.jzyk.com/res/JZYK/images/credit/bank/logo_2_1.png",
//                @"is_main_card": @(1),
//                @"tag": @(0),
//                @"tag_msg": @""}]};
            [self.bankCardListArr removeAllObjects];
            NSArray *itemArr = jsonDict[@"card_list"];
            for (int i = 0; i < itemArr.count; i++) {
                YKBankCardModel *model = [YKBankCardModel yy_modelWithJSON:itemArr[i]];
                [self.bankCardListArr addObject:model];
            }
            
            if (self.bankCardListArr.count <= 0) {
                self.tableView.df_hud = [UIView df_normalHudViewWithTitle:@"您还没有添加银行卡" textColor:GRAY_COLOR_AD font:[UIFont systemFontOfSize:14] andImage:[UIImage imageNamed:@"img_noBankCard"] titleImageMargin:5 customViewTopMargin:80 * ASPECT_RATIO_WIDTH customWidthLessAllHudView:NO addCenterXCustomView:^UIView *{
                    
                    [self.emptyAddBankCardBtn addTarget:self action:@selector(clickEmptyAddCard) forControlEvents:(UIControlEventTouchUpInside)];
                    return self.emptyAddBankCardBtn;
                }];
                self.tableFootView.hidden = YES;
            }else{
                self.tableFootView.hidden = NO;
            }
            
            //公告
            BOOL isShowNotice = [jsonDict[@"is_show_notice"] boolValue];
            NSString *noticeMsg = [NSString stringWithFormat:@"%@",jsonDict[@"notice_msg"]];
            if (isShowNotice) {
                [self.noticeView refreshNoticeWithContent:noticeMsg isShow:YES complete:nil];
            }else{
                [self.noticeView refreshNoticeWithContent:noticeMsg isShow:NO complete:nil];
            }
        
            //存管弹框
            if ([jsonDict[@"cunguan_alert"] isKindOfClass:[NSDictionary class]]) {
                if ([(NSDictionary *)jsonDict[@"cunguan_alert"] allKeys].count > 0) {
                    YKCunGunEntity *cunGuanModel = [YKCunGunEntity yy_modelWithDictionary:jsonDict[@"cunguan_alert"]];
                    [YKCunGunTools yk_cunguanAlertWithCunGunEntity:cunGuanModel isCommitOrder:NO bindBankComplete:nil];
                }
            }
            
        }else{
            [[iToast makeText:message] show];
        }
        [self.tableView reloadData];
         [self.tableView hd_endFreshing:YES];
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [self.tableView hd_endFreshing:YES];
        [[iToast makeText:errMsg] show];
    }];
    
}

// 页面渲染
- (void)prepareUI
{
    [self.view addSubview:self.noticeView];
    [self.view addSubview:self.tableView];
    
    self.noticeView.hidden = YES;
    [self.noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(30);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.noticeView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    
    [self.tableFootView addSubview:self.addBankCardBtn];
    self.tableFootView.hidden = YES;
    [self.addBankCardBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.tableFootView);
    }];
    
    [self.addBankCardBtn addTarget:self action:@selector(clickAddCard) forControlEvents:(UIControlEventTouchUpInside)];
//    WEAK_SELF
    self.noticeView.clickCloseBLk = ^(UIView *view) {
    };
}

- (void)clickAddCard{
    DLog(@"点击添加银行卡");
    [self addBankCardAciton];
}
- (void)clickEmptyAddCard{
    [self addBankCardAciton];
}
- (void)addBankCardAciton{
    WEAK_SELF
    YKAddBankCardViewController *addCardVC = [[YKAddBankCardViewController alloc] init];
    addCardVC.bankBindComplete = ^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
        [weakSelf fetchBankCardListData];
    };
    [self.navigationController pushViewController:addCardVC animated:YES];
}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.tableView);
    WEAK_SELF
    [self.tableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        tableMaker.hd_tableViewFooterView(^(){
            return self.tableFootView;
        });
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            
            [sectionMaker.hd_dataArr(^() {
                return self.bankCardListArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClass(HDCellClass(YKBankCardListCell))
                .hd_rowHeight(146 * ASPECT_RATIO_WIDTH)
                .hd_adapter(^(YKBankCardListCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor clearColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                    YKBankCardModel *bankCardModel = self.bankCardListArr[indexPath.row];
                    [cell setBankCardModel:bankCardModel];
                });
                
                cellMaker.hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    DLog(@"%ld",indexPath.row);
                    YKBankCardModel *model = self.bankCardListArr[indexPath.row];
                    if (!model.is_main_card) {
                        //不是主卡，才能点击弹框解绑或设置主卡
                        [self yk_cellWithSelectActionWithID:model];
                    }
                });
            }];
        }];
    }];
    [self.tableView hd_addFreshHeader:^{
        [weakSelf fetchBankCardListData];
    }];
    
}

- (void)yk_cellWithSelectActionWithID:(YKBankCardModel *)bankModel
{
    [UIAlertController ba_actionSheetShowInViewController:self
                                                    title:nil
                                                  message:nil
                                         buttonTitleArray:@[@"设为主卡",@"解除绑定"]
                                    buttonTitleColorArray:@[GRAY_COLOR_45,
                                                            GRAY_COLOR_45]
                       popoverPresentationControllerBlock:nil
                                                    block:^(UIAlertController * _Nonnull alertController, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                                                        if (buttonIndex == 0) {
                                                            //设为主卡
                                                            [self yk_setMainCardRequestWithID:bankModel.ID cardNo:bankModel.card_no];
                                                        }else if(buttonIndex == 1){
                                                            //解除绑定
                                                            [self yk_relieveCardRequestWithID:bankModel.ID];
                                                        }
                                                        
                                                    }];
}



- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];

    }
    return _tableView;
}

- (UIView *)tableFootView
{
    if (!_tableFootView) {
        _tableFootView = [[UIView alloc] init];
        _tableFootView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 106 * ASPECT_RATIO_WIDTH);
    }
    return _tableFootView;
}
- (UIButton *)addBankCardBtn
{
    if (!_addBankCardBtn) {
        _addBankCardBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"add_bankCard"];
        [_addBankCardBtn setBackgroundImage:image forState:(UIControlStateNormal)];
//        _addBankCardBtn.layer.masksToBounds = YES;
//        [_addBankCardBtn setTitle:@"" forState:(UIControlStateNormal)];
//        _addBankCardBtn.titleLabel.font = [UIFont systemFontOfSize:17];
//        [_addBankCardBtn setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
        
    }
    return _addBankCardBtn;
}

- (UIButton *)emptyAddBankCardBtn
{
    if (!_emptyAddBankCardBtn) {
        _emptyAddBankCardBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"system_commit_btn"];
        [_emptyAddBankCardBtn setBackgroundImage:image forState:(UIControlStateNormal)];
        [_emptyAddBankCardBtn setTitle:@"添加新的银行卡" forState:(UIControlStateNormal)];
        _emptyAddBankCardBtn.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(17)];
        [_emptyAddBankCardBtn setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
        _emptyAddBankCardBtn.layer.cornerRadius = image.size.height * 0.5;
        _emptyAddBankCardBtn.layer.masksToBounds = YES;
    }
    return _emptyAddBankCardBtn;
}

- (YKPaoMaNoticeView *)noticeView {
    if (!_noticeView) {
        _noticeView = [[YKPaoMaNoticeView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 30) pageType:PageTypeMine];
        _noticeView.backgroundColor = WHITE_COLOR;
    }
    return _noticeView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
