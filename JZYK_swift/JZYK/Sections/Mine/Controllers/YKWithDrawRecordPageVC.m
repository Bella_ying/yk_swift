//
//  YKWithDrawRecordPageVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/29.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKWithDrawRecordPageVC.h"
#import "MXSegmentedPager.h"
#import "YKWithDrawRecordTableView.h"

@interface YKWithDrawRecordPageVC ()<MXSegmentedPagerDelegate, MXSegmentedPagerDataSource>

@property (nonatomic,strong) MXSegmentedPager *segmentedPager;
@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) NSMutableArray *classArray;

@property (nonatomic, strong) YKWithDrawRecordTableView *drawListTableView;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation YKWithDrawRecordPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"提现记录";
    
    self.titleArray = @[@"取现",@"活动"].mutableCopy;
    self.classArray = @[].mutableCopy;
    
    [self prepareSegmentedPager];
}


- (void)prepareSegmentedPager
{
    [self.view addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(1.0/[UIScreen mainScreen].scale);
    }];
    [self.view addSubview:self.segmentedPager];
    self.segmentedPager.delegate = self;
    self.segmentedPager.dataSource = self;
    [self.segmentedPager mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.bottom.right.equalTo(self.view);
        make.top.equalTo(self.lineView.mas_bottom);
    }];
}


#pragma mark <MXSegmentedPagerDelegate>
- (CGFloat)heightForSegmentedControlInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 50.f;
}

#pragma mark <MXSegmentedPagerControllerDataSource>
- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager
{
    return self.titleArray.count;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index {
    
    return self.titleArray [index];
}
- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index
{
//    if (segmentedPager == self.segmentedPager) {
//        if (self.classArray.count) {
//            return self.classArray[index];
//        }
//    }else{
//        if (self.kaQuanClassArray.count) {
//            return self.kaQuanClassArray[index];
//        }
//    }
//    return [YKEmptyView dn_emptyViewReloadClick:^{
//
//    }](segmentedPager,YKEmptyViewCoverTypeNoShop);;
    
//    return self.classArray[index];
    
    YKWithDrawRecordTableView *listView = [[YKWithDrawRecordTableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    if (index == 0) {
        listView.recordStyle = YKWithDrawRecordStyleEnchashment;
    }else {
        listView.recordStyle = YKWithDrawRecordStyleActivity;
    }
    [listView.mj_header beginRefreshing];
    return listView;
    
}


#pragma mark - Lazy loading
- (MXSegmentedPager *)segmentedPager
{
    if (!_segmentedPager) {
        _segmentedPager = [[MXSegmentedPager alloc] initWithFrame:CGRectMake(0, 0,WIDTH_OF_SCREEN, HEIGHT_OF_SCREEN - NAV_HEIGHT - BOTTOM_HEIGHT)];
        _segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        _segmentedPager.segmentedControl.backgroundColor = [UIColor whiteColor];
        _segmentedPager.segmentedControl.titleTextAttributes =  @{NSForegroundColorAttributeName : LABEL_TEXT_COLOR,
                                                                  NSFontAttributeName : [UIFont systemFontOfSize:adaptFontSize(14)] };
        _segmentedPager.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : MAIN_THEME_COLOR,
                                                                         NSFontAttributeName :[UIFont systemFontOfSize:adaptFontSize(14)] };
        _segmentedPager.segmentedControl.selectionIndicatorHeight = 2;// 底部是否需要横条指示器，0的话就没有了
        _segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
        _segmentedPager.segmentedControl.selectionIndicatorColor =  MAIN_THEME_COLOR;
        _segmentedPager.segmentedControlEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        _segmentedPager.delegate    = self;
        _segmentedPager.dataSource  = self;
        _segmentedPager.segmentedControl.selectedSegmentIndex = 0;
    }
    return _segmentedPager;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = LINE_COLOR;
    }
    return _lineView;
}

#pragma -mark - tableView-----


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
