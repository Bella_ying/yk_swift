//
//  YKRepayBillDetailVC.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"

@interface YKRepayBillDetailVC : YKBaseViewController

@property (nonatomic, copy) NSString *billId;

@end
