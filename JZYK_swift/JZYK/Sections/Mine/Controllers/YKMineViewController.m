//
//  YKMineViewController.m
//  JZYK
//
//  Created by hongyu on 2018/6/1.
//  Copyright © 2018年 JSQB. All rights reserved.
//

#import "YKMineViewController.h"
#import "YKMineTopInfoView.h"
#import "YKMineHeadView.h"
#import "YKMineListCell.h"
#import "KDAttachView.h"
#import "YKMineMenuModel.h"
#import "YKMineHeadCardModel.h"
#import "YKLoanEntity.h"

#import "YKAccountRemainingSumVC.h"
#import "YKNeedRepayBillVC.h"
#import "YKAmountManageVC.h"
#import "YKSettingViewController.h"
#import "YKVerifyCenterListVC.h"
#import "YKOrderListVC.h"
#import "YKBankCardListVC.h"
#import "YKVerifyCenterListVC.h"
#import "YKDiscountTicketVC.h"
#import "YKPaoMaNoticeView.h"
#import "YKMinePositionModel.h"

@interface YKMineViewController ()

@property (nonatomic, strong) UITableView *mineTableView;
@property (nonatomic, strong) YKMineTopInfoView *topInfoView;
@property (nonatomic, strong) YKPaoMaNoticeView *noticeView;
@property (nonatomic, strong) YKMineHeadView *mineHeadView;
@property (nonatomic, strong) KDAttachView *attachView; //悬浮球
@property (nonatomic, strong) YKMinePositionModel *positionModel;
@property (nonatomic, strong) NSMutableArray *itemMenuArr;
@property (nonatomic, strong) NSMutableArray *listArr;

@end

@implementation YKMineViewController
//
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.showNavigationBar = NO;
    self.itemMenuArr = @[].mutableCopy;
    self.listArr = @[].mutableCopy;
//    self.itemListArr = @[@"银行卡",@"帮助中心",@"咨询客服",@"认证中心（暂时放这里）"];
    
    [self prepareUI];
    [self prepareTableView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //刷新页面
    [self requestUserInfoDataShowLoading:NO];
    
    YKUserManager *userManager = [YKUserManager sharedUser];
    if (userManager.yk_isLogin) {
        NSString *nameStr = userManager.username;
        if (nameStr.length == 11) {
            nameStr = [NSString yk_phoneNumberHiddenMiddle:nameStr];
        }
        [self.topInfoView yk_setPhoneStr:nameStr isLogin:YES];
    }else{
        [self.topInfoView yk_setPhoneStr:@"未登录" isLogin:NO];
    }
}

#pragma mark - UIResponder+Router
-(void)routerEventWithName:(NSString *)eventName userInfo:(id)userInfo{
    if ([eventName isEqualToString:kEventLoginIcon]) {
        //点击头部的登录
        if (![YKUserManager sharedUser].yk_isLogin) {
            [self logIn];
        }
    }else if ([eventName isEqualToString:kEventIntoAmountStatusManager]) {
        DLog(@"点击到认证中心");
        YKMineCardDetailModel *detailModel = userInfo;
        if ([detailModel.status integerValue] == 2) {
            [self yk_amountManageAction];
        }else{
            //暂时写死去认证中心
//            YKVerifyCenterListVC *vc = [[YKVerifyCenterListVC alloc] init];
//            [self.navigationController pushViewController:vc animated:YES];
            //根据后台跳转
            YKMineCardDetailModel *detailModel = userInfo;
            YKJumpManager *manager = [YKJumpManager shareYKJumpManager];
            JumpModel *jumpModel = [[JumpModel alloc] init];
            jumpModel.skip_code = detailModel.goSkilpCode;
            jumpModel.url = detailModel.goH5;
            [manager yk_jumpWithParamModel:jumpModel];
        }
        

    }else if ([eventName isEqualToString:kEventAccountRemanining]) {
        //点击账户余额
        if ([[YKUserManager sharedUser] yk_isLogin]){
            YKAccountRemainingSumVC *accountVC = [[YKAccountRemainingSumVC alloc] init];
            [self.navigationController pushViewController:accountVC animated:YES];
        }else{
            [self logIn];
        }

    }else if ([eventName isEqualToString:kEventBillNeedRepay]){
        //点击待还账单
        if ([[YKUserManager sharedUser] yk_isLogin]){
            YKNeedRepayBillVC *billVC = [[YKNeedRepayBillVC alloc] init];
            [self.navigationController pushViewController:billVC animated:YES];
        }else{
            [self logIn];
        }
    }else if ([eventName isEqualToString:kEventMyAmount]){
        //根据后台跳转
        YKMineCardDetailModel *detailModel = userInfo;
        YKJumpManager *manager = [YKJumpManager shareYKJumpManager];
        JumpModel *jumpModel = [[JumpModel alloc] init];
        jumpModel.skip_code = detailModel.goSkilpCode;
        jumpModel.url = detailModel.goH5;
        [manager yk_jumpWithParamModel:jumpModel];
    }else if ([eventName isEqualToString:kEventSetting]){
        if ([[YKUserManager sharedUser] yk_isLogin]) {
            //点击设置
            YKSettingViewController *setVc = [[YKSettingViewController alloc] initWithNibName:@"YKSettingViewController" bundle:nil];
            [self.navigationController pushViewController:setVc animated:YES];
        } else {
            [self logIn];
        }
    }else if ([eventName isEqualToString:kEventMenuItem]){
        //menu菜单的跳转
        if ([[YKUserManager sharedUser] yk_isLogin]){
            YKMineMenuModel *menuModel = userInfo[kItemModelKey];
            //根据后台跳转
            if ([menuModel.skipCode yk_isEmpty] && [menuModel.skipH5 yk_isEmpty]) {
                [[iToast makeText:@"敬请期待"] show];
            }else{
                YKJumpManager *manager = [YKJumpManager shareYKJumpManager];
                JumpModel *jumpModel = [[JumpModel alloc] init];
                jumpModel.skip_code = menuModel.skipCode;
                jumpModel.url = menuModel.skipH5;
                [manager yk_jumpWithParamModel:jumpModel];
            }
        }else{
            [self logIn];
        }
    }
    
}

- (void)yk_amountManageAction{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditAppUserCreditTop showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        if (code == 0 && success) {
            [strongSelf requestUserInfoDataShowLoading:YES];
        }else{
            [[iToast makeText:message] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

- (void)requestUserInfoDataShowLoading:(BOOL)isShowLoading{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kUserGetInfo showLoading:isShowLoading param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        if (code == 0 && success) {
            [self.listArr removeAllObjects];
            [self.itemMenuArr removeAllObjects];
            NSArray *listArr = jsonDict[@"list"];
            for (int i = 0; i< listArr.count; i++) {
                YKMineListModel *listModel = [YKMineListModel yy_modelWithDictionary:listArr[i]];
                [self.listArr addObject:listModel];
            }
            NSArray *menuItemArr = jsonDict[@"diyList"];
            for (int i = 0; i< menuItemArr.count; i++) {
                YKMineMenuModel *menuModel = [YKMineMenuModel yy_modelWithDictionary:menuItemArr[i]];
                [self.itemMenuArr addObject:menuModel];
            }
            
            //设置数据
                //公告
            YKNoticeModel *noticeModel = [YKNoticeModel yy_modelWithDictionary:jsonDict[@"notice"]];
            [self.noticeView refreshNoticeWithData:noticeModel complete:nil];
               //card样式
            YKMineHeadCardModel *headModel = [[YKMineHeadCardModel alloc] init];
            headModel.style = [jsonDict[@"header"][@"style"] integerValue];
            headModel.quotaHeaderTextModel = [YKMineCardDetailModel yy_modelWithDictionary:jsonDict[@"header"][@"quotaHeaderText"]];
            headModel.quotaStatusModel = [YKMineCardDetailModel yy_modelWithDictionary:jsonDict[@"header"][@"quotaStatus"]];
            headModel.quotaTotalModel = [YKMineCardDetailModel yy_modelWithDictionary:jsonDict[@"header"][@"quotaTotal"]];
            headModel.quotaFooterTextModel = [YKMineCardDetailModel yy_modelWithDictionary:jsonDict[@"header"][@"quotaFooterText"]];
            self.mineHeadView.headCardModel = headModel;
               //菜单等
            self.mineHeadView.menuArr = self.itemMenuArr;
            [self.mineHeadView refreshDataWithAmountCanPop:[NSString stringWithFormat:@"%@",jsonDict[@"amountCanPop"]?:@"---"] orderDate:[NSString stringWithFormat:@"%@",jsonDict[@"order"][@"date"]?:@""] orderMoney:[NSString stringWithFormat:@"%@",jsonDict[@"order"][@"money"]?:@"---"] redPointStatus:[jsonDict[@"order"][@"hot_dot"] integerValue]];
                //展示mine红点
            if ([jsonDict[@"order"][@"hot_dot"] integerValue] == 1) {
                [[YKTabBarController yk_shareTabController].ykTabBar yk_showBadgeOnItemIndex:3];
            } else {
                [[YKTabBarController yk_shareTabController].ykTabBar yk_hideBadgeOnItemIndex:3];
            }
               //悬浮按钮
            self.positionModel = [YKMinePositionModel yy_modelWithDictionary:jsonDict[@"position"]];
            if (![self.positionModel.image yk_isEmpty]) {
                self.attachView.hidden = NO;
                [self.attachView sd_setImageWithURL:[NSURL URLWithString:self.positionModel.image] forState:(UIControlStateNormal)];
            }else{
                self.attachView.hidden = YES;
            }
        }else{
            [[iToast makeText:message] show];
        }
        [strongSelf.mineTableView reloadData];
        [strongSelf.mineTableView hd_endFreshing:YES];
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        [strongSelf.mineTableView hd_endFreshing:YES];
        [[iToast makeText:errMsg] show];
    }];
}

- (void)prepareUI{
    
    [self.view addSubview:self.topInfoView];
    [self.view addSubview:self.noticeView];
    [self.view addSubview:self.mineTableView];
    
    //顶部固定nav_view
    [self.topInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(64 + iPhoneX_ADD_NAV_HEIGHT);
    }];
    //公告
    [self.noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topInfoView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(30);
    }];
    
    self.mineTableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    [self.mineTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.noticeView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.mas_bottomLayoutGuideTop);
    }];
    
    //悬浮球
    [self.view addSubview:self.attachView];
    [self.attachView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (IS_iPhone5) {
            make.bottom.mas_equalTo(-60);
        }else{
            make.bottom.mas_equalTo(-60 * ASPECT_RATIO_WIDTH);
        }
        make.right.equalTo(self.view.mas_right).offset(27);
        make.width.height.mas_equalTo(54);
    }];
    self.attachView.isAllShow = NO;
    WEAK_SELF
    self.attachView.clickSelfAticon = ^{
        DLog(@"点我悬浮");
        YKJumpManager *manager = [YKJumpManager shareYKJumpManager];
        JumpModel *jumpModel = [[JumpModel alloc] init];
        jumpModel.skip_code = weakSelf.positionModel.skipCode;
        jumpModel.url = weakSelf.positionModel.skipH5;
        [manager yk_jumpWithParamModel:jumpModel];
    };
}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.mineTableView);
    WEAK_SELF
    [self.mineTableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
//        tableMaker.hd_tableViewHeaderView(^(){
//            return weakSelf.mineHeadView;
//        }); //在下面已设置
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {

            [sectionMaker.hd_dataArr(^() {
                return weakSelf.listArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKMineListCell))
                .hd_rowHeight(60)
                .hd_adapter(^(YKMineListCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
//                    cell.titleL.text = self.listArr[indexPath.row];
                    YKMineListModel *listModel = self.listArr[indexPath.row];
                    [cell setListModel:listModel];
                });
                
                cellMaker.hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    DLog(@"%ld",indexPath.row);
                    
                    YKMineListModel *listModel = self.listArr[indexPath.row];
                    YKJumpManager *manager = [YKJumpManager shareYKJumpManager];
                    JumpModel *jumpModel = [[JumpModel alloc] init];
                    jumpModel.skip_code = listModel.skipCode;
                    jumpModel.url = listModel.skipH5;
                    [manager yk_jumpWithParamModel:jumpModel];
                    
//                    if (![listModel.skipH5 yk_isEmpty]) {
//                        //h5
//                        YKBrowseWebController *webVC = [[YKBrowseWebController alloc] init];
//                        webVC.url = listModel.skipH5;
//                        [self.navigationController pushViewController:webVC animated:YES];
//                    }else{
//                        //原生
//                        NSInteger skipCode = [listModel.skipCode integerValue];
//                        switch (skipCode) {
//                            case 0:
//                            {
//                                DLog(@"0000");
//                            }
//                                break;
//                            case 1:
//                            {
//                                //根据后台跳转
//                                //        YKJumpManager *manager = [YKJumpManager shareYKJumpManager];
//                                //        JumpModel *jumpModel = [[JumpModel alloc] init];
//                                //        jumpModel.skip_code = listModel.skipCode;
//                                //        jumpModel.url = listModel.skipH5;
//                                //        [manager yk_jumpWithParamModel:jumpModel];
//                            }
//                                break;
//                            case 3:
//                            {
//                                DLog(@"点击银行卡");
//                                YKBankCardListVC *bankVC = [[YKBankCardListVC alloc] init];
//                                [weakSelf.navigationController pushViewController:bankVC animated:YES];
//                            }
//                                break;
//                            case 4:
//                            {
//                                DLog(@"点击帮助中心");
//                            }
//                                break;
//                            default:
//                                break;
//                        }
//                    }
                });
            }];
        }];
        
        tableMaker.hd_scrollViewDidScroll(^(UIScrollView * scrollView) {
//            DLog(@"滑动");
            if (!weakSelf.attachView.hidden) {
                [weakSelf.attachView yk_hiddenView];
            }
        });

    }];
    //tableView--Header头部
    self.mineTableView.tableHeaderView = self.mineHeadView;
    [self.mineHeadView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
    }];

    [self.mineTableView hd_addFreshHeader:^{
        [weakSelf requestUserInfoDataShowLoading:NO];
    }];

}

- (UITableView *)mineTableView
{
    if (!_mineTableView) {
        _mineTableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _mineTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _mineTableView;
}

- (YKMineTopInfoView *)topInfoView
{
    if (!_topInfoView) {
        _topInfoView = [[YKMineTopInfoView alloc] init];
    }
    return _topInfoView;
}

- (YKMineHeadView *)mineHeadView{
    if (!_mineHeadView) {
        _mineHeadView = [[YKMineHeadView alloc] init];
        _mineHeadView.backgroundColor = WHITE_COLOR;
    }
    return _mineHeadView;
}

- (KDAttachView *)attachView
{
    if (!_attachView) {
        _attachView = [[KDAttachView alloc] initWithDirection:AttachNatural andCutTabBar:YES];
        _attachView.backgroundColor = [UIColor clearColor];
        _attachView.layer.cornerRadius = 23;
        _attachView.layer.masksToBounds = YES;
//        [_attachView setTitle:@"悬浮" forState:(UIControlStateNormal)];
//        [_attachView setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
//        _attachView.titleLabel.font = [UIFont systemFontOfSize:13];
    }
    return _attachView;
}
- (YKPaoMaNoticeView *)noticeView {
    if (!_noticeView) {
        _noticeView = [[YKPaoMaNoticeView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 30) pageType:PageTypeMine];
        _noticeView.backgroundColor = WHITE_COLOR;
    }
    return _noticeView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
