//
//  YKOrderListVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/12.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
// 有分页

#import "YKOrderListVC.h"
#import "YKOrderListCell.h"
#import "QiYuManager.h"
#define kPageSize 20
@interface YKOrderListVC ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *listArr;

@property (nonatomic, assign) NSInteger page;
@end

@implementation YKOrderListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的订单";
    self.listArr = @[].mutableCopy;
    self.page = 1;
    
    [self prepareUI];
    [self prepareTableView];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(id)userInfo
{
    if ([eventName isEqualToString:kEventOrderCancel]) {
        //取消借款
        WEAK_SELF
        YKOrderListModel *model = userInfo;
        [[QLAlert alert] showWithMessage:@"是否需要取消您的借款申请" btnTitleArray:@[@"再考虑一下",@"我要取消"] btnClicked:^(NSInteger index) {
            if (index == 1) {
                //取消
                DLog(@"取消%@",model.order_id);
                [weakSelf cancelOrderRequestWithOrderId:model.order_id];
            }
        }];
    }
}

- (void)cancelOrderRequestWithOrderId:(NSString *)order_id{
//    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kcreditLoanCancelColdTime showLoading:YES param:@{@"order_id":order_id} succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
//        STRONG_SELF
        if (code == 0) {
            DLog(@"====%@",jsonDict);
            [[iToast makeText:message] show];
        }else{
            [[iToast makeText:message] show];
        }
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
        
    }];
}

- (void)fetchOrderListDataIsFresh:(BOOL)isFresh
{
    NSDictionary *paramDic = @{@"page":@(self.page),@"pageSize":@(kPageSize)};
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kUserLoanGetMyOrders showLoading:NO param:paramDic succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        [strongSelf.tableView df_hideHud];
        if (code == 0) {
            DLog(@"====%@",jsonDict);
            if (isFresh) {
                [strongSelf.listArr removeAllObjects];
            }
            
            NSArray *arr = jsonDict[@"data"];
            for (int i = 0; i < arr.count; i++) {
                YKOrderListModel *model = [YKOrderListModel yy_modelWithJSON:arr[i]];
                [strongSelf.listArr addObject:model];
            }
            
            if (arr.count <= 0) {
                strongSelf.tableView.mj_footer.hidden = YES;
            }else{
                strongSelf.tableView.mj_footer.hidden = NO;
            }

            if (strongSelf.listArr.count == 0) {
                strongSelf.tableView.df_hud = [UIView df_normalHudViewWithTitle:@"暂无订单哦~" textColor:GRAY_COLOR_AD font:[UIFont systemFontOfSize:adaptFontSize(13)] andImage:[UIImage imageNamed:@"img_wusdindan"] titleImageMargin:5];
            }
        }else{
            [[iToast makeText:message] show];
        }
        
        [strongSelf.tableView reloadData];
        [strongSelf.tableView hd_endFreshing:YES];
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        [strongSelf.tableView hd_endFreshing:YES];
        [[iToast makeText:errMsg] show];

    }];
    
}

// 页面渲染
- (void)prepareUI
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    
}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.tableView);
    WEAK_SELF
    [self.tableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
//        tableMaker.hd_tableViewHeaderView(^(){
//            return self.tableViewHeadView;
//        });
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            
            [sectionMaker.hd_dataArr(^() {
                return self.listArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKOrderListCell))
                .hd_rowHeight(146)
                .hd_adapter(^(YKOrderListCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                    YKOrderListModel *model = self.listArr[indexPath.row];
                    [cell setOrderModel:model];
                });
                
                cellMaker.hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    DLog(@"%ld",indexPath.row);
                    YKOrderListModel *model = self.listArr[indexPath.row];
                    if (model.can_cancel) {
                        //可以取消借款--cell就不能点击进入详情
                    }else{
                        YKBrowseWebController *webVC = [[YKBrowseWebController alloc] init];
                        webVC.url = model.link_url;
                        if (model.is_show_help) { //这里不用了，直接在YKJSHandler里面做了操作
//                            UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"帮助" style:(UIBarButtonItemStylePlain) target:self action:@selector(rightItemAction)];
//                            rightItem.tintColor = LABEL_TEXT_COLOR;
//                            [rightItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateNormal];
//                            webVC.rightBarButtonItem = rightItem;
                        }
                        [self.navigationController pushViewController:webVC animated:YES];
                    }
                });
            }];
        }];
    }];
    [self.tableView hd_addFreshHeader:^{
        self.page = 1;
        [weakSelf fetchOrderListDataIsFresh:YES];
    }];
    
    self.tableView.mj_footer = [YKRefreshFooter footerWithRefreshingBlock:^{
//        weakSelf.isLoadMore = YES;
        self.page++;
        [self fetchOrderListDataIsFresh:NO];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
