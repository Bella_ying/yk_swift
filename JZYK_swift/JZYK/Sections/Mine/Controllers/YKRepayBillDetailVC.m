//
//  YKRepayBillDetailVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/21.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKRepayBillDetailVC.h"
#import "YKBillDetailCell.h"
#import "YKBillDetailHeadView.h"
#import "YKNeedRepayModel.h"
@interface YKRepayBillDetailVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) YKBillDetailHeadView *headView;
@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) UIButton *commitBtn;

@property (nonatomic, strong) NSMutableArray *titleArr;
@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, copy) NSString *notice;
@property (nonatomic, copy) NSString *repay_url; //立即还款url
@property (nonatomic, assign) NSInteger status; //判断按钮显示隐藏

@property (nonatomic, strong) YKRepayListModel *detailModel;

@end

@implementation YKRepayBillDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"账单明细";
    
//    self.titleArr = @[@"还款期数",@"本金",@"利息",@"服务费",@"优惠券",@"已还款",@"逾期",@"用途"].mutableCopy;
    self.titleArr = @[@"还款期数",@"本金",@"利息",@"服务费",@"已还款",@"逾期"].mutableCopy;
    self.dataArr = @[].mutableCopy;
    [self prepareUI];
    
    [self fetchRepayDetailDataWithId:self.billId];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //赋值
    //期数 repay_period 本金：principal 利息：interests 服务费：service_fee 优惠券：coupon_fee； 已还款：true_total_money，逾期：overdue_day，用途：use_type
}

- (void)fetchRepayDetailDataWithId:(NSString *)repayId
{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditLoanApplyRepay showLoading:YES param:@{@"id":repayId} succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        if (code == 0 && success) {
            DLog(@"====%@",jsonDict);
            strongSelf.detailModel = [YKRepayListModel yy_modelWithJSON:jsonDict[@"item"]];
            
            strongSelf.titleArr = @[@"还款期数",@"本金",@"利息",@"服务费",@"已还款"].mutableCopy;
            
            strongSelf.dataArr = @[self.detailModel.repay_period?:@"",
                                   self.detailModel.principal?[NSString stringWithFormat:@"%@元",self.detailModel.principal]:@"",
                                   self.detailModel.interests?[NSString stringWithFormat:@"%@元",self.detailModel.interests]:@"",
                                   self.detailModel.service_fee?[NSString stringWithFormat:@"%@元",self.detailModel.service_fee]:@"",
                                   self.detailModel.true_total_money?[NSString stringWithFormat:@"%@元",self.detailModel.true_total_money]:@""
                                   ].mutableCopy;
            
            NSString *overdueTitleStr = @"";
            NSString *overdueSubStr = @"";
            if (strongSelf.detailModel.is_overdue) { //如果已逾期，则显示
                overdueTitleStr = [NSString stringWithFormat:@"逾期(%@天)", self.detailModel.overdue_day];
                overdueSubStr = [NSString stringWithFormat:@"%@元", self.detailModel.late_fee];
                [strongSelf.titleArr addObject:overdueTitleStr];
                [strongSelf.dataArr addObject:overdueSubStr];

            }
            
            //其他数据
            strongSelf.notice = jsonDict[@"notice"]?:@"";
            strongSelf.repay_url = jsonDict[@"repay_url"]?:@"";
            strongSelf.status = [jsonDict[@"status"] integerValue];
            if (strongSelf.status == 2) {
                self.commitBtn.hidden = YES;
            }else{
                self.commitBtn.hidden = NO;
            }
            //head
            [strongSelf.headView yk_setRepayDay:strongSelf.detailModel.repay_day amount:[NSString stringWithFormat:@"%@元",strongSelf.detailModel.debit_money?:@"0"] isOverdue:strongSelf.detailModel.is_overdue orderState:strongSelf.detailModel.order_state?:@""];

        }else{
            [[iToast makeText:message] show];
        }
        
        [strongSelf.tableView reloadData];
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
//        STRONG_SELF
        [[iToast makeText:errMsg] show];
        
    }];
    
}

- (void)prepareUI{
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.tableView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.equalTo(@120);
    }];
    [self.bottomView addSubview:self.commitBtn];
    [self.commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bottomView);
        make.bottom.equalTo(self.bottomView.mas_bottom).offset(-50);
    }];
    [self.commitBtn addTarget:self action:@selector(commitAction) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    UIImageView *imageV = [[UIImageView alloc] init];
    imageV.image = [UIImage imageNamed:@"billDetailBottom"];
    [self.footerView addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.footerView);
        make.width.equalTo(self.footerView);
        make.top.equalTo(self.footerView.mas_bottom);
    }];
    self.headView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 150 * ASPECT_RATIO_WIDTH);
    self.footerView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 25);
    self.tableView.tableHeaderView = self.headView;
    self.tableView.tableFooterView = self.footerView;
//    self.tableView.mj_header = [YKRefreshHeader headerWithRefreshingBlock:^{
//        if (self.isttt) {
//            self.isttt = NO;
//        }else{
//            self.isttt = YES;
//        }
//        [self fetchRepayDetailDataWithId:self.billId];
//        [self.tableView.mj_header endRefreshing];
//    }];
    
}
- (void)commitAction{
    if (self.repay_url.length) {
        YKBrowseWebController *web = [[YKBrowseWebController alloc] init];
        web.url = self.repay_url;
        [self.navigationController pushViewController:web animated:YES];
    } else {
        if (self.notice.length) [[iToast makeText:self.notice] show];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YKBillDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdYKBillDetailCell forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *titleStr = self.titleArr.count > indexPath.row ? self.titleArr[indexPath.row] : @"";
    NSString *subStr = self.dataArr.count > indexPath.row ? self.dataArr[indexPath.row] : @"";
    
    [cell yk_setTitle:titleStr subText:subStr];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 25 * ASPECT_RATIO_WIDTH;
}


- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.backgroundColor = GRAY_BACKGROUND_COLOR;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource  = self;
        [_tableView registerNib:[UINib nibWithNibName:@"YKBillDetailCell" bundle:nil] forCellReuseIdentifier:kIdYKBillDetailCell];
    }
    return _tableView;
}

- (YKBillDetailHeadView *)headView
{
    if (!_headView) {
        _headView = [[YKBillDetailHeadView alloc] init];
        _headView.backgroundColor = WHITE_COLOR;
    }
    return _headView;
}
- (UIView *)footerView
{
    if (!_footerView) {
        _footerView = [[UIView alloc] init];
        _footerView.backgroundColor = WHITE_COLOR;
    }
    return _footerView;
}

- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _bottomView;
}

- (UIButton *)commitBtn
{
    if (!_commitBtn) {
        _commitBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"system_commit_btn"];
        [_commitBtn setBackgroundImage:image forState:(UIControlStateNormal)];
        _commitBtn.layer.cornerRadius = image.size.height * 0.5;
        _commitBtn.layer.masksToBounds = YES;
        [_commitBtn setTitle:@"立即还款" forState:(UIControlStateNormal)];
        _commitBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [_commitBtn setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
        
    }
    return _commitBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
