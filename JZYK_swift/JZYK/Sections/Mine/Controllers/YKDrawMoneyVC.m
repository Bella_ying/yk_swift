//
//  YKDrawMoneyVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKDrawMoneyVC.h"
#import "YKDrawMoneyCell.h"

@interface YKDrawMoneyVC ()
@property (weak, nonatomic) IBOutlet UITableView *drawTableView;
@property (nonatomic, strong) UIView *tableFootView;
@property (nonatomic, strong) UIButton *commitBtn;

@end

@implementation YKDrawMoneyVC

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.title = @"提现";
    
    [self prepareUI];
    [self prepareTableView];
}


#pragma mark - UIResponder+Router
-(void)routerEventWithName:(NSString *)eventName userInfo:(id)userInfo{
    if ([eventName isEqualToString:kEventInstallmentBtn]) {
        DLog(@"额度分期");
    }else if ([eventName isEqualToString:kEventActivityBtn]){
        DLog(@"活动奖励");
    }
    
}

// 页面渲染
- (void)prepareUI
{
    self.drawTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.drawTableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    
    [self.tableFootView addSubview:self.commitBtn];
    [self.commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.tableFootView);
        make.bottom.equalTo(self.tableFootView);
    }];
    
}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.drawTableView);
    WEAK_SELF
    [self.drawTableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        tableMaker.hd_tableViewFooterView(^(){
            return self.tableFootView;
        });
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {

            [sectionMaker.hd_dataArr(^() {
                return @[@""];
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKDrawMoneyCell))
                .hd_rowHeight(264)
                .hd_adapter(^(YKDrawMoneyCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                });
            }];
        }];
    }];

}

- (UIView *)tableFootView
{
    if (!_tableFootView) {
        _tableFootView = [[UIView alloc] init];
        _tableFootView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 65 * ASPECT_RATIO_HEIGHT + 45);
    }
    return _tableFootView;
}
- (UIButton *)commitBtn
{
    if (!_commitBtn) {
        _commitBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"system_commit_btn"];
        [_commitBtn setBackgroundImage:image forState:(UIControlStateNormal)];
        _commitBtn.layer.cornerRadius = image.size.height * 0.5;
        _commitBtn.layer.masksToBounds = YES;
        [_commitBtn setTitle:@"确认提现" forState:(UIControlStateNormal)];
        _commitBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [_commitBtn setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
        
    }
    return _commitBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
