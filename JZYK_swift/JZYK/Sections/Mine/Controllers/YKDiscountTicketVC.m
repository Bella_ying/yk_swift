//
//  YKDiscountTicketVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKDiscountTicketVC.h"
#import "YKDiscountTicketCell.h"

@interface YKDiscountTicketVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *bottomBtn;

@property (nonatomic, strong) NSMutableArray *couponListArr;

@end

@implementation YKDiscountTicketVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationItem.title = @"我的优惠券";
    self.couponListArr = @[].mutableCopy;
    
    [self prepareUI];
    [self prepareTableView];
    
    [self.tableView.mj_header beginRefreshing];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.type == 1) {
        self.navigationItem.title = @"历史优惠券";
        self.navigationItem.rightBarButtonItem = nil;
    } else {
        self.navigationItem.title  = @"我的优惠券";
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ticket_history_icon"] style:(UIBarButtonItemStylePlain) target:self action:@selector(clickRightItem)];
        self.navigationItem.rightBarButtonItem.tintColor = [UIColor grayColor];
    }
}


- (void)fetchCouponListData
{
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kMyCoupon showLoading:NO param:@{@"type":[NSString stringWithFormat:@"%ld",self.type]} succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        [strongSelf.tableView df_hideHud];
        if (code == 0 && success) {
            DLog(@"====%@",jsonDict);
            [strongSelf.couponListArr removeAllObjects];
            NSArray *itemArr = jsonDict[@"item"];
            for (int i = 0; i < itemArr.count; i++) {
                YKDiscountTicketModel *model = [YKDiscountTicketModel yy_modelWithJSON:itemArr[i]];
                [strongSelf.couponListArr addObject:model];
            }
        }else{
            [[iToast makeText:message] show];
        }
        
        if (strongSelf.couponListArr.count == 0) {
            strongSelf.tableView.df_hud = [UIView df_normalHudViewWithTitle:@"暂无优惠券～" textColor:GRAY_COLOR_8D font:[UIFont systemFontOfSize:adaptFontSize(13)] andImage:[UIImage imageNamed:@"ticket_no"] titleImageMargin:5];
            strongSelf.bottomView.hidden = YES;
        }else{
            strongSelf.bottomView.hidden = NO;
        }
        [strongSelf.tableView reloadData];
        [strongSelf.tableView hd_endFreshing:YES];
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        [strongSelf.tableView hd_endFreshing:YES];
        [[iToast makeText:errMsg] show];
        
        //无网络的时候，去掉数据然后加载缺省页(完整版)
        [strongSelf.couponListArr removeAllObjects];
        [strongSelf.tableView reloadData];
        strongSelf.tableView.df_hud = [UIView df_defaultNoNetHudViewWithTitle:@"网络连接失败~" imageTitleMargin:5 btnMargin:15 btnBlock:^(UIButton *btn) {
            [strongSelf.tableView.mj_header beginRefreshing];
        }];
        strongSelf.bottomView.hidden = YES;

    }];

}

// 页面渲染
- (void)prepareUI
{
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(55 * ASPECT_RATIO_WIDTH);
    }];
    [self.bottomView addSubview:self.bottomBtn];
    [self.bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bottomView);
        make.bottom.equalTo(self.bottomView).offset(-25 * ASPECT_RATIO_WIDTH);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    
    [self.bottomBtn addTarget:self action:@selector(clickBottomBtn) forControlEvents:(UIControlEventTouchUpInside)];
}
- (void)clickRightItem{
    DLog(@"点击历史优惠券");
    YKDiscountTicketVC *ticketVC = [[YKDiscountTicketVC alloc] init];
    ticketVC.type = 1;
    [self.navigationController pushViewController:ticketVC animated:YES];
}
- (void)clickBottomBtn{
    DLog(@"点击查看券规则");
    NSString *urlStr = [[ConfigManager config] configForKey:@"voucher_rules_url"];
    YKBrowseWebController *web = [[YKBrowseWebController alloc] init];
    web.url = urlStr;
    [self.navigationController pushViewController:web animated:YES];
}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.tableView);
    WEAK_SELF
    [self.tableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            
            [sectionMaker.hd_dataArr(^() {
                return weakSelf.couponListArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClass(HDCellClass(YKDiscountTicketCell))
                .hd_rowHeight(130 * ASPECT_RATIO_WIDTH)
                .hd_adapter(^(YKDiscountTicketCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor clearColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                    YKDiscountTicketModel *model = self.couponListArr[indexPath.row];
                    [cell setTicketModel:model];
                });
                
                cellMaker.hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    DLog(@"%ld",indexPath.row);
                });
            }];
        }];
    }];
    
    [self.tableView hd_addFreshHeader:^{
        [weakSelf fetchCouponListData];
    }];
    
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        
    }
    return _tableView;
}

- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _bottomView;
}
- (UIButton *)bottomBtn{
    if (!_bottomBtn) {
        _bottomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bottomBtn setTitle:@"查看券规则" forState:(UIControlStateNormal)];
        [_bottomBtn setTitleColor:MAIN_THEME_COLOR forState:(UIControlStateNormal)];
        _bottomBtn.titleLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
    }
    return _bottomBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
