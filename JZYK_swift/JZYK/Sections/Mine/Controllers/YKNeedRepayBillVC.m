//
//  YKNeedRepayBillVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKNeedRepayBillVC.h"
#import "YKAccountNavView.h"
#import "YKAccountRemainView.h"
#import "YKBillAmountShowCell.h"
#import "YKBillNeedRepayListCell.h"
#import "YKNeedRepayModel.h"
#import "YKRepayBillDetailVC.h"
#import "YKPaoMaNoticeView.h"
#import "YKBillRepayNoRecordView.h"
#import "YKBillRepayReviewView.h"
#import "YKVerifyGuideViewController.h"
#import "YKIndependentNoticeModel.h"
#import "QiYuManager.h"
#import "UIButton+category.h"

static const CGFloat billHeaderViewHeight   = 186.f;
static const CGFloat firstSectionViewHeight   = 40;
static const CGFloat billAmountShowViewHeight   = 88;
@interface YKNeedRepayBillVC ()
@property (weak, nonatomic) IBOutlet UITableView *billTableView;
@property (strong, nonatomic) YKAccountNavView *billNavigationView;
@property (strong, nonatomic) YKAccountRemainView *billHeadView;
@property (nonatomic, strong) UIView *firstSectionHeadView;
@property (nonatomic, strong) YKPaoMaNoticeView *noticeView;
@property (nonatomic, strong) NSMutableArray *sectionArr;

@property (nonatomic, strong) YKBillRepayNoRecordView *noRecordView;
@property (nonatomic, strong) YKBillRepayReviewView *reviewStatusView;
@property (nonatomic, strong) YKNeedRepayModel *repayModel;

@property (nonatomic, strong) NSArray *listArr;
@property (nonatomic, strong) YKRepayTotalModel *totalModel;
@end

static NSString *const TitleStr = @"待还账单";
static NSString *const AmountNormalTitleStr = @"借款金额（元）";
static NSString *const AmountNoDatalTitleStr = @"暂无信息";
@implementation YKNeedRepayBillVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isStatusLight = YES;
    
    self.sectionArr = @[].mutableCopy;
    [self prepareUI];
    [self prepareTableView];
    
    [self.billTableView.mj_header beginRefreshing];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

#pragma mark - UIResponder+Router
-(void)routerEventWithName:(NSString *)eventName userInfo:(id)userInfo{
//    if ([eventName isEqualToString:kEventBillTipClose]) {

//    }
}

- (void)fetchRepayNoticeData{
    WEAK_SELF
//    NSDictionary *parmDic = @{@"p":@[@"9"],@"st":@"1"};
    NSString *pValue = [NSString stringWithFormat:@"[%@]",@"9"];
    [[HTTPManager session] getRequestForKey:kAnouncementList showLoading:NO param:@{@"p":pValue} succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        if (code == 0 && success) {
            NSArray *noticesArr = jsonDict[@"notices"];
            YKNoticeModel *noticeModel = [[YKNoticeModel alloc] init];
            if (noticesArr.count > 0) {
               YKIndependentNoticeModel *originNoticeModel = [YKIndependentNoticeModel yy_modelWithJSON:noticesArr[0]];
                noticeModel.content = originNoticeModel.title;
                noticeModel.noticeID = originNoticeModel.noticeId;
            }
            [strongSelf.noticeView refreshNoticeWithData:noticeModel complete:nil];
            if (strongSelf.noticeView.hidden) {
                strongSelf.firstSectionHeadView.frame = CGRectZero;
            }else{
                strongSelf.firstSectionHeadView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, firstSectionViewHeight);
            }
            [strongSelf.billTableView reloadData];

        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
//        [[iToast makeText:errMsg] show];
    }];
}

- (void)fetchRepayBillData
{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditLoanGetPayOrder showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        if (code == 0 && success) {
            [self fetchRepayNoticeData];
            DLog(@"====%@",jsonDict);
            YKNeedRepayModel *repayModel = [YKNeedRepayModel yy_modelWithJSON:jsonDict[@"item"]];
            self.repayModel = repayModel;
            self.listArr = repayModel.list;
            self.totalModel = repayModel.total_data;
            
            /*
             "banner": {
             "button": 1, //授信状态 1  // 认证完成  2 // 认证中
             "status": 2, //审核状态 0授信完成，没有借款 1申请中  2驳回 3授信完成，没有借款 4还款成功再次借款
             "can_loan_time":  0 随时可借  -1不可在借 其他是可再借时间
             },
             */
            if (self.listArr.count > 0) {
                //有数据
                self.noRecordView.hidden = YES;
                self.reviewStatusView.hidden = YES;
                self.sectionArr = @[@""].mutableCopy;
                [self yk_setHeadViewInfoShow:YES amountStr:self.totalModel.money_amount?:@"0" orderTitle:self.totalModel.order_title];
            }else{
                //无数据---判断各种缺省页
                NSInteger statusValue = [repayModel.banner[@"status"] integerValue]; //status状态值
//                NSInteger buttonValue = [repayModel.banner[@"button"] integerValue]; //button状态值
                NSInteger can_loan_timeValue = [repayModel.banner[@"can_loan_time"] integerValue]; //can_loan_time状态值
                NSString *tipMessage = repayModel.banner[@"message"]?:@"";
                [self.sectionArr removeAllObjects];
                [self yk_setHeadViewInfoShow:NO amountStr:@"" orderTitle:AmountNoDatalTitleStr];
                if (statusValue == 1) {
                    //申请中
                    self.reviewStatusView.hidden = NO;
                    [self.reviewStatusView yk_updateWithRepayBillType:(YKRepaymentReviewStatusTypeApplying) message:tipMessage];
                    self.noRecordView.hidden = YES;
                }else if (statusValue == 2){
                    //驳回
                    self.reviewStatusView.hidden = NO;
                    [self.reviewStatusView yk_updateWithRepayBillType:(YKRepaymentReviewStatusTypeApplyFail) message:tipMessage can_loan_time:can_loan_timeValue];
                    self.noRecordView.hidden = YES;
                }else if (statusValue == 0 || statusValue == 3 || statusValue ==4){
                    self.reviewStatusView.hidden = YES;
                    self.noRecordView.hidden = NO;
                    if (statusValue == 4) {
                        [self.noRecordView yk_updateWithRepayNoRecordType:(YKRepaymentNoRecordTypeHaveRepay)];
                    }else{
                        [self.noRecordView yk_updateWithRepayNoRecordType:(YKRepaymentNoRecordTypeNewUser)];
                    }
                }
            }
//            self.noRecordView.isCreditReviewing = NO;
//            if (buttonValue == 2) {
//                self.noRecordView.isCreditReviewing = YES;
//                [self.sectionArr removeAllObjects];
//                self.reviewStatusView.hidden = NO;
//                self.noRecordView.hidden = YES;
//                [self yk_setHeadViewInfoShow:NO amountStr:@""];
//            }else if (buttonValue == 1){
//                if (statusValue == 2) { //驳回
//                    [self.sectionArr removeAllObjects];
//                    self.noRecordView.hidden = YES;
//                    self.reviewStatusView.hidden = NO;
//                    [self.reviewStatusView yk_updateWithRepayBillType:(YKRepaymentReviewStatusTypeApplyFail)];
//                    [self yk_setHeadViewInfoShow:NO amountStr:@""];
//                }else if (statusValue == 0 || statusValue == 3 || statusValue ==4){//无记录
//                    self.noRecordView.hidden = NO;
//                    self.reviewStatusView.hidden = YES;
//                    [self yk_setHeadViewInfoShow:NO amountStr:@""];
//                }
//            }else{
//                self.noRecordView.hidden = YES;
//                self.reviewStatusView.hidden = YES;
//                self.sectionArr = @[@""].mutableCopy;
//                [self yk_setHeadViewInfoShow:YES amountStr:self.totalModel.money_amount?:@"0"];
//            }
            
        }else{
            [[iToast makeText:message] show];
        }
        
        [strongSelf.billTableView reloadData];
        [strongSelf.billTableView hd_endFreshing:YES];
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        [strongSelf.billTableView hd_endFreshing:YES];
        [[iToast makeText:errMsg] show];
        
    }];
    
}

- (void)yk_setHeadViewInfoShow:(BOOL) isShow amountStr:(NSString *)amountStr orderTitle:(NSString *)orderTitle{
    if (isShow) {
        self.billNavigationView.rightBtn.hidden = NO;
        self.billHeadView.availableNumLabel.text = amountStr;
        self.billHeadView.availableTitleLabel.text = orderTitle;
    }else{
        self.billNavigationView.rightBtn.hidden = YES;
        self.billHeadView.availableNumLabel.text = amountStr;
        self.billHeadView.availableTitleLabel.text = orderTitle;
    }
}

// 页面渲染
- (void)prepareUI
{
    self.showNavigationBar = NO;
    self.billTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.billTableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    
    [self.view addSubview:self.billNavigationView];
    [self.billNavigationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.mas_offset(NAV_HEIGHT);
    }];
    self.billNavigationView.navTitleLabel.text = TitleStr;
    
    self.firstSectionHeadView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 0);
    [self.firstSectionHeadView addSubview:self.noticeView];
    [self.noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.firstSectionHeadView);
        make.height.mas_equalTo(30);
    }];
    WEAK_SELF
    self.noticeView.clickCloseBLk = ^(UIView *view) {
        weakSelf.firstSectionHeadView.frame = CGRectZero;
        [weakSelf.billTableView reloadData];
    };
    
    //缺省和审核状态的view
    [self.billTableView addSubview:self.noRecordView];
    self.noRecordView.hidden = YES;
    [self.noRecordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.billTableView);
        make.top.equalTo(self.billTableView.mas_top).offset(billHeaderViewHeight * ASPECT_RATIO_WIDTH + iPhoneX_ADD_NAV_HEIGHT);
        make.centerX.equalTo(self.billTableView);
    }];
    [self.billTableView addSubview:self.reviewStatusView];
    self.reviewStatusView.hidden = YES;
    [self.reviewStatusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.billTableView);
        make.top.equalTo(self.billTableView.mas_top).offset(billHeaderViewHeight * ASPECT_RATIO_WIDTH + iPhoneX_ADD_NAV_HEIGHT);
        make.centerX.equalTo(self.billTableView);
    }];
    self.noRecordView.applyActionBlk = ^(BOOL isCreditReviewing){
      //申请分期
//        if (isCreditReviewing) {//授信未完成
//            [[YKTabBarController yk_shareTabController] yk_pushToViewController:[[YKVerifyGuideViewController alloc] init]];
//        }else{
           //不用判断直接去取现一级页面，那里有授信状况的判断
            [[YKTabBarController yk_shareTabController] yk_setSelectedIndex:(YKTabSelectedIndexWithDraw) viewController:nil];
//        }
    };
    self.reviewStatusView.buttonActionBlk = ^(YKRepaymentReviewStatusType type) {
        if (type == YKRepaymentReviewStatusTypeApplying) {
            //查看详情
            YKBrowseWebController *webVC = [[YKBrowseWebController alloc] init];
            webVC.url = weakSelf.repayModel.detail_url;
            if (weakSelf.repayModel.is_show_help) {//这里不用了，直接在YKJSHandler里面做了操作
//                UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"帮助" style:(UIBarButtonItemStylePlain) target:weakSelf action:@selector(rightItemAction)];
//                rightItem.tintColor = LABEL_TEXT_COLOR;
//                [rightItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateNormal];
//                webVC.rightBarButtonItem = rightItem;
            }
            [weakSelf.navigationController pushViewController:webVC animated:YES];
        }else if (type == YKRepaymentReviewStatusTypeApplyFail){
            //申请失败，再次申请
            [[YKTabBarController yk_shareTabController] yk_setSelectedIndex:(YKTabSelectedIndexWithDraw) viewController:nil];
        }
    };
}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.billTableView);
    WEAK_SELF
    [self.billTableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        [tableMaker.hd_sectionCount(2).hd_tableViewHeaderView(^() {
            return weakSelf.billHeadView;
        }) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            switch (sectionMaker.section) {
                case 0:
                    {
                        sectionMaker.hd_headerView(^() {
                            return weakSelf.firstSectionHeadView;
                        });
                        [sectionMaker.hd_dataArr(^(){
                            return self.sectionArr;
                        }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                                cellMaker.hd_cellClassXib(HDCellClass(YKBillAmountShowCell));
                                cellMaker.hd_rowHeight(billAmountShowViewHeight);
                                cellMaker.hd_adapter(^(YKBillAmountShowCell *cell, id data, NSIndexPath *indexPath) {
                                    cell.backgroundColor = [UIColor whiteColor];
                                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                                    [cell yk_setNeedRepayAmount:self.totalModel.late_money period:self.totalModel.total_period];
                                });
                        }];
                        
                    }
                    break;
                case 1:
                {
                    [sectionMaker.hd_dataArr(^() {
                        return self.listArr;
                    }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                        cellMaker.hd_cellClassXib(HDCellClass(YKBillNeedRepayListCell))
                        .hd_rowHeight(68)
                        .hd_adapter(^(YKBillNeedRepayListCell *cell, id data, NSIndexPath *indexPath) {
                            cell.backgroundColor = [UIColor whiteColor];
                            cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                            YKRepayListModel *listModel = self.listArr[indexPath.row];
                            [cell setListModel:listModel];
                            if (indexPath.row == self.listArr.count - 1) {
                                cell.lineView.hidden = YES;
                            }else{
                                cell.lineView.hidden = NO;
                            }

                        })
                        .hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                            //点击跳转详情
                            YKRepayListModel *listModel = self.listArr[indexPath.row];
                            YKRepayBillDetailVC *detailVC = [[YKRepayBillDetailVC alloc] init];
                            detailVC.billId = listModel.repay_id;
                            [self.navigationController pushViewController:detailVC animated:YES];
                        });
                    }];
                }
                    break;
                    
                default:
                    break;
            }

        }];
        
        tableMaker.hd_scrollViewDidScroll(^(UIScrollView * scrollView) {
            CGFloat offsetY = scrollView.contentOffset.y;
            if (offsetY < 0) {
                weakSelf.billNavigationView.hidden = YES;
            }else if(offsetY == 0){
                weakSelf.billNavigationView.hidden = NO;
                weakSelf.billNavigationView.navTitleLabel.text = TitleStr;
                weakSelf.billNavigationView.backgroundColor = [UIColor clearColor];
            }else{
                weakSelf.billNavigationView.hidden = NO;
                weakSelf.billNavigationView.backgroundColor = MAIN_THEME_COLOR;
            }
            
        });
    }];
    
    [self.billTableView hd_addFreshHeader:^{
        [self fetchRepayBillData];
    }];
    
    self.billNavigationView.yk_backBlock = ^{
        DLog(@"111");
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    self.billNavigationView.yk_rightBlock = ^{
        DLog(@"222");
        if (![[YKUserManager sharedUser] yk_isLogin]) {
            [weakSelf logIn];
        } else {
            [QiYuManager yk_sharedQiYuManager].commonQuestionTemplateId = [[ConfigManager config] getQiyuTemplateIDForKey:@"repayment"];
            [[QiYuManager yk_sharedQiYuManager] yk_qiYuService];
            [[QiYuManager yk_sharedQiYuManager] yk_qiYuDelegate];
        }
    };
}

#pragma mark- 懒加载
- (YKAccountNavView *)billNavigationView
{
    if (!_billNavigationView) {
        _billNavigationView = [[YKAccountNavView alloc] initCustomStyle:(YKRightItemStyleBillNeedRepay)];
        _billNavigationView.backgroundColor = [UIColor clearColor];
    }
    return _billNavigationView;
}

- (YKAccountRemainView *)billHeadView{
    if (!_billHeadView) {
        _billHeadView = [[YKAccountRemainView alloc] init];
        _billHeadView.availableTitleLabel.text = AmountNormalTitleStr;
        _billHeadView.availableNumLabel.text = @"0.00";
        _billHeadView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, billHeaderViewHeight * ASPECT_RATIO_WIDTH + iPhoneX_ADD_NAV_HEIGHT);
    }
    return _billHeadView;
}

- (UIView *)firstSectionHeadView
{
    if (!_firstSectionHeadView) {
        _firstSectionHeadView = [[UIView alloc] init];
    }
    return _firstSectionHeadView;
}

- (YKPaoMaNoticeView *)noticeView {
    if (!_noticeView) {
        _noticeView = [[YKPaoMaNoticeView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 30) pageType:PageTypeRepayBill];
        _noticeView.backgroundColor = WHITE_COLOR;
    }
    return _noticeView;
}


- (YKBillRepayNoRecordView *)noRecordView
{
    if (!_noRecordView) {
        _noRecordView  = [[YKBillRepayNoRecordView alloc] init];
        _noRecordView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _noRecordView;
}
- (YKBillRepayReviewView *)reviewStatusView
{
    if (!_reviewStatusView) {
        _reviewStatusView = [[YKBillRepayReviewView alloc] init];
        _reviewStatusView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _reviewStatusView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
