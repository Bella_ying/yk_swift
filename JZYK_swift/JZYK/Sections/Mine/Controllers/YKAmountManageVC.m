//
//  YKAmountManageVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//  额度管理

#import "YKAmountManageVC.h"
#import "YKAmountManageCell.h"
#import "YKAmountManageHeadView.h"
#import "YKAccountNavView.h"
#import "YKMyAmountModel.h"

@interface YKAmountManageVC ()
@property (weak, nonatomic) IBOutlet UITableView *amountTableView;
@property (nonatomic, strong) YKAccountNavView *amountNavView;
@property (nonatomic, strong) YKAmountManageHeadView *amountManageHeadView;
@property (nonatomic, strong) UIView *sectionHeadView;
@property (nonatomic, strong) UILabel *sectionHeadLabel;

@property (nonatomic, strong) YKMyAmountModel *amountModel;

@end

@implementation YKAmountManageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isStatusLight = YES;
    self.showNavigationBar = NO;
    
    [self prepareUI];
    [self prepareTableView];
    
    [self.amountTableView.mj_header beginRefreshing];
    
}

- (void)fetchMyAmountData
{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kDiscoverQuotaLog showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        [strongSelf.amountTableView df_hideHud];
        if (code == 0 && success) {
            
            strongSelf.amountModel = [YKMyAmountModel yy_modelWithDictionary:jsonDict];
            [strongSelf.amountManageHeadView yk_setTotal_quota:[NSString stringWithFormat:@"%@",strongSelf.amountModel.total_quota] total_incre:[NSString stringWithFormat:@"%@",strongSelf.amountModel.total_incre] temp_quota:[NSString stringWithFormat:@"%@",strongSelf.amountModel.temp_quota]];

            if (strongSelf.amountModel.list.count == 0) {
                strongSelf.amountTableView.df_hud = [UIView df_normalHudViewWithTitle:@"完善信息，保持良好的信用行为\n有助于提升您的额度" textColor:GRAY_COLOR_AD font:[UIFont systemFontOfSize:adaptFontSize(13)] andImage:[UIImage imageNamed:@"img_wsxinxi"] titleImageMargin:5];
                strongSelf.amountTableView.df_hudCenterOffset = CGPointMake(0, 80 * ASPECT_RATIO_WIDTH);
                strongSelf.sectionHeadLabel.hidden = YES;
            }else{
                strongSelf.sectionHeadLabel.hidden = NO;
            }
        }else{
            [[iToast makeText:message] show];
        }

        [strongSelf.amountTableView reloadData];
        [strongSelf.amountTableView hd_endFreshing:YES];
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        [strongSelf.amountTableView hd_endFreshing:YES];
        [[iToast makeText:errMsg] show];
        //无网络的时候，去掉数据然后加载缺省页--暂时不要
//        strongSelf.amountModel = nil;
//        [strongSelf.amountTableView reloadData];
//        strongSelf.amountTableView.df_hud = [UIView df_defaultNoNetHudViewWithTitle:@"网络连接失败~"];
//        strongSelf.amountTableView.df_hudCenterOffset = CGPointMake(0, 80 * ASPECT_RATIO_WIDTH);
//        strongSelf.sectionHeadLabel.hidden = YES;
    }];
    
}

// 页面渲染
- (void)prepareUI
{
    self.amountTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.amountTableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    
    [self.view addSubview:self.amountNavView];
    [self.amountNavView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.mas_offset(NAV_HEIGHT);
    }];
    self.amountNavView.navTitleLabel.text = @"我的额度";
    
    self.amountManageHeadView = [[NSBundle mainBundle] loadNibNamed:@"YKAmountManageHeadView" owner:nil options:nil][0];
    self.amountManageHeadView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.amountManageHeadView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 264 * ASPECT_RATIO_WIDTH + iPhoneX_ADD_NAV_HEIGHT);
    
    [self.sectionHeadView addSubview:self.sectionHeadLabel];
    [self.sectionHeadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.equalTo(self.sectionHeadView);
     }];
    
}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.amountTableView);
    WEAK_SELF
    [self.amountTableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        tableMaker.hd_tableViewHeaderView(^(){
            return weakSelf.amountManageHeadView;
        });
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            sectionMaker.hd_headerView(^(){
                return self.sectionHeadView;
            });
            [sectionMaker.hd_dataArr(^() {
                return self.amountModel.list;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKAmountManageCell))
                .hd_rowHeight(68)
                .hd_adapter(^(YKAmountManageCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                    YKIncreasedLimitListModel *model = self.amountModel.list[indexPath.row];
                    [cell setListModel:model];
                });
                
                cellMaker.hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    DLog(@"%ld",indexPath.row);
                });
            }];
        }];
        
        tableMaker.hd_scrollViewDidScroll(^(UIScrollView * scrollView) {
            CGFloat offsetY = scrollView.contentOffset.y;
            if (offsetY < 0) {
                weakSelf.amountNavView.hidden = YES;
            }else if(offsetY == 0){
                weakSelf.amountNavView.hidden = NO;
                weakSelf.amountNavView.navTitleLabel.text = @"我的额度";
                weakSelf.amountNavView.backgroundColor = [UIColor clearColor];
            }else{
                weakSelf.amountNavView.hidden = NO;
                weakSelf.amountNavView.backgroundColor = MAIN_THEME_COLOR;
            }
            
        });
    }];
    
    [self.amountTableView hd_addFreshHeader:^{
        [weakSelf fetchMyAmountData];
    }];

    self.amountNavView.yk_backBlock = ^{
        DLog(@"111");
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };

    self.amountNavView.yk_rightBlock = ^{
        DLog(@"额度说明");
        YKBrowseWebController *webVC = [[YKBrowseWebController alloc] init];
        webVC.url = weakSelf.amountModel.about_quota;
        [weakSelf.navigationController pushViewController:webVC animated:YES];
        weakSelf.amountModel.list = @[];
    };
    
}

#pragma mark- 懒加载
- (YKAccountNavView *)amountNavView
{
    if (!_amountNavView) {
        _amountNavView = [[YKAccountNavView alloc] initCustomStyle:(YKRightItemStyleAmount)];
        _amountNavView.backgroundColor = [UIColor clearColor];
    }
    return _amountNavView;
}

- (UIView *)sectionHeadView
{
    if (!_sectionHeadView){
        _sectionHeadView = [[UIView alloc] init];
        _sectionHeadView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 30);
        _sectionHeadView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _sectionHeadView;
}
- (UILabel *)sectionHeadLabel{
    if (!_sectionHeadLabel){
        _sectionHeadLabel  = [[UILabel alloc] init];
        _sectionHeadLabel.font = [UIFont systemFontOfSize:13];
        _sectionHeadLabel.textColor = GRAY_COLOR_AD;
        _sectionHeadLabel.text= @"额度变更明细";
    }
    return _sectionHeadLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
