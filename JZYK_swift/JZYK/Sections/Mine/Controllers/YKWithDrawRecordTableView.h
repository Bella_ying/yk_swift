//
//  YKWithDrawRecordTableView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/29.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, YKWithDrawRecordStyle) {
    YKWithDrawRecordStyleEnchashment,  //取现
    YKWithDrawRecordStyleActivity, //活动
};

@interface YKWithDrawRecordTableView : UITableView

@property (nonatomic, assign) YKWithDrawRecordStyle recordStyle;

@end
