//
//  YKWithDrawRecordList.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
// 单个列表的需求用，已不使用

#import "YKWithDrawRecordList.h"
#import "YKWithDrawRecordListCell.h"

@interface YKWithDrawRecordList ()
@property (weak, nonatomic) IBOutlet UITableView *drawListTableView;
@property (nonatomic, strong) UIView *tableViewHeadView;

@property (nonatomic, strong) NSMutableArray *listArr;

@end

@implementation YKWithDrawRecordList

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"提现记录";
    self.listArr = @[].mutableCopy;
    [self prepareUI];
    [self prepareTableView];
    
    [self fetchWithDrawRecordData];
}

- (void)fetchWithDrawRecordData
{
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kCreditUserWithdrawLog showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        if (code == 0 && success) {
            DLog(@"====%@",jsonDict);
            [self.listArr removeAllObjects];
            NSArray *arr = jsonDict[@"log_list"];
            for (int i = 0; i < arr.count; i++) {
                YKWithDrawRecordModel *model = [YKWithDrawRecordModel yy_modelWithJSON:arr[i]];
                [strongSelf.listArr addObject:model];
            }
        }else{
            [[iToast makeText:message] show];
        }
        //        //test
        //        [weakSelf.listArr removeAllObjects];
        //        if (weakSelf.listArr.count == 0) {
        //            weakSelf.emptyView = [YKEmptyView dn_emptyViewReloadClick:^{
        //            }](weakSelf.tableView,YKEmptyViewCoverTypeNoCoupon);
        //        }else{
        //            [weakSelf.emptyView removeFromSuperview];
        //        }
        
        [strongSelf.drawListTableView reloadData];
        [strongSelf.drawListTableView hd_endFreshing:YES];
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        STRONG_SELF
        [strongSelf.drawListTableView hd_endFreshing:YES];
        [[iToast makeText:errMsg] show];
        
        //        weakSelf.emptyView = [YKEmptyView dn_emptyViewReloadClick:^{
        //            [weakSelf fetchAccountListData];
        //        }](weakSelf.tableView, YKEmptyViewCoverTypeNoNetwork);
    }];
    
}


// 页面渲染
- (void)prepareUI
{
//    self.drawListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.drawListTableView.backgroundColor = GRAY_BACKGROUND_COLOR;

}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.drawListTableView);
    WEAK_SELF
    [self.drawListTableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        tableMaker.hd_tableViewHeaderView(^(){
            return weakSelf.tableViewHeadView;
        });
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            
            [sectionMaker.hd_dataArr(^() {
                return weakSelf.listArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKWithDrawRecordListCell))
                .hd_rowHeight(68)
                .hd_adapter(^(YKWithDrawRecordListCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                    YKWithDrawRecordModel *model = weakSelf.listArr[indexPath.row];
                    [cell setWithDrawModel:model];
                });
                
                cellMaker.hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    DLog(@"%ld",indexPath.row);
                });
            }];
        }];
    }];
    
    [self.drawListTableView hd_addFreshHeader:^{
        [self fetchWithDrawRecordData];
    }];
    
}

- (UIView *)tableViewHeadView
{
    if (!_tableViewHeadView){
        _tableViewHeadView = [[UIView alloc] init];
        _tableViewHeadView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 10);
        _tableViewHeadView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _tableViewHeadView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
