//
//  YKRealNameDataManager.m
//  KDFDApp
//
//  Created by 闫涛 on 2017/4/5.
//  Copyright © 2017年 cailiang. All rights reserved.
//

#import "YKRealNameDataManager.h"

@implementation YKRealNameDataManager

+(NSArray *)getArrayFromModelArray:(NSArray *)array withKey:(NSString *)key
{
    NSMutableArray *tempArray = [@[] mutableCopy];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [tempArray addObject:[obj valueForKey:key]];
    }];
    return tempArray;
}

+ (instancetype)getEntityFromArray:(NSArray *)array key:(NSString *)key value:(NSString *)value
{
    __block id entity;
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([[obj valueForKey:key] isEqualToString:value]) {
            entity = obj;
            *stop = YES;
        }
    }];
    return entity;
}

@end
