//
//  UIView+JobMessage.m
//  JZYK
//
//  Created by 吴春艳 on 2018/7/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "UIView+JobMessage.h"

@implementation UIView (JobMessage)

//创建行视图
- (UIView *)configCellWithLeftLabelText:(NSString *)text height:(CGFloat)height topView:(UIView *)topView
{
    return [self configCellWithLeftLabelText:text height:height topView:topView topMargin:0];
}

- (UIView *)configCellWithLeftLabelText:(NSString *)text height:(CGFloat)height topView:(UIView *)topView topMargin:(CGFloat)margin
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    [self addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(height);
        make.width.offset(WIDTH_OF_SCREEN);
        if (topView) {
            make.top.equalTo(topView.mas_bottom).offset(margin);
        } else {
            make.top.offset(margin);
        }
        
        //给最后一行添加约束来控制scrollview的contentsize
        //处理的视图有：个人信息、工作信息
        if ([text isEqualToString:@"发薪日期"]) {
            make.bottom.offset(0);
        }
    }];
    
    if (text || [text isEqualToString:@""]) {
        [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            
            make.centerY.offset(0);
            make.left.offset(15);
            
            label.tag = 20001;
            label.text = text;
            [label setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        }];
    }
    
    //分割线
    [UIView yk_viewWithColor:Color.color_E6_C6 superView:view masonrySet:^(UIView *view, MASConstraintMaker *make) {
        make.left.offset(15);
        make.right.offset(-15);
        make.bottom.offset(0);
        make.height.offset(0.5);
        view.tag = 20002;
    }];
    return view;
}


- (UIView *)configRightBtnWithTitle:(NSString *)title superView:(UIView *)superView tapSelector:(SEL)selector haveArrow:(BOOL)have
{
    UIView *view = [UIView new];
    
    UILabel *leftLabel = nil;
    if (have) {
        UIImageView *img = [UIImageView yk_imageViewWithImageName:@"system_right_arrrow" superView:view masonrySet:^(UIImageView *imageView, MASConstraintMaker *make) {
            make.right.offset(-15.f);
            make.centerY.offset(0);
            make.width.offset(8.5f);
            make.height.offset(15.f);
        }];
        leftLabel = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_CC_C8 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            make.right.equalTo(img.mas_left).offset(-5);
            make.centerY.offset(0);
            label.tag = 10002;
            label.text = title;
        }];
    }else{
        leftLabel = [UILabel yk_labelWithFontSize:adaptFontSize(14) textColor:Color.color_45_C2 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            make.right.offset(-15.f);
            make.centerY.offset(0);
            label.tag = 10001;
            label.text = title;
        }];
        [UIImageView yk_imageViewWithImageName:@"system_help" superView:view masonrySet:^(UIImageView *imageView, MASConstraintMaker *make) {
            
            make.right.equalTo(leftLabel.mas_left).offset(-5.f);
            make.centerY.offset(0);
            make.width.offset(15.f);
            make.height.offset(15.f);
            view.contentMode = UIViewContentModeScaleAspectFill;
        }];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:selector];
    [view addGestureRecognizer:tapGesture];
    
    [superView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(0);
        make.top.offset(0);
        make.bottom.offset(-1.f);
        make.left.equalTo(leftLabel.mas_left);
    }];
    
    return view;
}

//创建选择框
- (UIView *)configRightBtnWithTitle:(NSString *)title superView:(UIView *)superView tapSelector:(SEL)selector
{
    return [self configRightBtnWithTitle:title superView:superView tapSelector:selector haveArrow:NO];
}

@end
