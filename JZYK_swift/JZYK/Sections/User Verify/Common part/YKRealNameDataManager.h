//
//  YKRealNameDataManager.h
//  KDFDApp
//
//  Created by 闫涛 on 2017/4/5.
//  Copyright © 2017年 cailiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKRealNameDataManager : NSObject


/**
 将model数组中的每个model的指定key对应的vaue抽出来作为新的数组

 @param array 原数组
 @param key 属性
 @return 新数组
 */
+ (NSArray *)getArrayFromModelArray:(NSArray *)array withKey:(NSString *)key;


/**
 匹配model数组中特定属性等于某个值的model

 @param array 原数组
 @param key 属性
 @param value 特定值
 @return 匹配出的model
 */
+ (instancetype)getEntityFromArray:(NSArray *)array key:(NSString *)key value:(NSString *)value;

@end
