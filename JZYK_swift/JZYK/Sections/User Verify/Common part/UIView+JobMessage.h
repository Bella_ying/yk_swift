//
//  UIView+JobMessage.h
//  JZYK
//
//  Created by 吴春艳 on 2018/7/9.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (JobMessage)

/**
 *  |-创建行视图
 |---创建背景view
 |---创建左边label
 |---创建底部线
 @param text 左边的文字
 @param height 高度
 @param topView 上面的视图，便于写布局约束
 @return view
 */
- (UIView *)configCellWithLeftLabelText:(NSString *)text height:(CGFloat)height topView:(UIView *)topView;
- (UIView *)configCellWithLeftLabelText:(NSString *)text height:(CGFloat)height topView:(UIView *)topView topMargin:(CGFloat)margin;



/**
 |-创建右边按钮
 |---创建右边箭头
 |---创建右边文字label
 @param title 右边按钮上文字
 @param superView 父视图
 @param selector 点击事件
 @return view
 */
- (UIView *)configRightBtnWithTitle:(NSString *)title superView:(UIView *)superView tapSelector:(SEL)selector;

/**
 创建视图
 
 @param title 右边按钮上文字
 @param superView 父视图
 @param selector 点击事件
 @param up up --> YES
 @return view
 */
- (UIView *)configRightBtnWithTitle:(NSString *)title
                          superView:(UIView *)superView
                        tapSelector:(SEL)selector
                          haveArrow:(BOOL)up;

@end
