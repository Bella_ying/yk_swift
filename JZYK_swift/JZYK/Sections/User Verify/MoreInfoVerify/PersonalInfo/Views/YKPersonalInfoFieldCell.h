//
//  YKPersonalInfoFieldCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKPersonalInfoFieldCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@end
