//
//  JSQBProvinceCityCountyView.m
//  KDFDApp
//
//  Created by zhaoying on 2017/8/28.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import "JSQBProvinceCityCountyView.h"
#import "JSQBProvinceTableViewCell.h"

#define provinceCityCountyViewHeight 400
static NSString * const YKUseButtonColor = @"#CCCCCC";

@interface JSQBProvinceCityCountyView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *citytableView;
@property (weak, nonatomic) IBOutlet UITableView *provinceTableView;
@property (weak, nonatomic) IBOutlet UITableView *cityTableView;
@property (weak, nonatomic) IBOutlet UITableView *countyTableView;
//定位的label
@property (weak, nonatomic) IBOutlet UILabel *localizeLabel;
//定位得到的省市区
@property (nonatomic,copy) NSString *provinceCityDistrictStr;
//定位得到的街道地址
@property (nonatomic,copy) NSString *streetNumberStr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *provinceTableViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityTableViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *countyTableViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *localizeViewHeight;
@property (weak, nonatomic) IBOutlet UIButton           *useButton;

//所有地址数据
@property (nonatomic, strong) NSArray *dataArray;
//省
@property (nonatomic, strong) NSMutableArray *provinceArray;
@property (assign, nonatomic) NSInteger selectedProvince;
//市
@property (nonatomic, strong) NSMutableArray *cityArray;
@property (assign, nonatomic) NSInteger selectedCity;
//区
@property (nonatomic, strong) NSMutableArray *countyArray;
@property (assign, nonatomic) NSInteger selectedCounty;

//选择得到的省市区
@property (nonatomic, copy)   NSString *provincestrCityCountyStr;

@property (nonatomic, strong) UIView *bgView;


- (IBAction)useBtnClick:(id)sender;

- (IBAction)cancelBtnClick:(id)sender;

@end

static NSString * const kJSQBProvinceTableViewCell = @"JSQBProvinceTableViewCell";

@implementation JSQBProvinceCityCountyView
- (void)awakeFromNib{
    [super awakeFromNib];
    self.provinceTableView.dataSource = self;
    self.provinceTableView.delegate = self;
    
    self.cityTableView.dataSource = self;
    self.cityTableView.delegate = self;
    
    self.countyTableView.dataSource = self;
    self.countyTableView.delegate = self;
    
    self.provinceTableViewWidth.constant = WIDTH_OF_SCREEN;
    [self.provinceTableView layoutIfNeeded];
    
    self.cityTableViewWidth.constant = 0;
    [self.cityTableView layoutIfNeeded];
    
    self.countyTableViewWidth.constant = 0;
    [self.countyTableView layoutIfNeeded];
    
    [self.provinceTableView registerNib:[UINib nibWithNibName:@"JSQBProvinceTableViewCell" bundle:nil] forCellReuseIdentifier:kJSQBProvinceTableViewCell];
    [self.cityTableView registerNib:[UINib nibWithNibName:@"JSQBProvinceTableViewCell" bundle:nil] forCellReuseIdentifier:kJSQBProvinceTableViewCell];
    [self.countyTableView registerNib:[UINib nibWithNibName:@"JSQBProvinceTableViewCell" bundle:nil] forCellReuseIdentifier:kJSQBProvinceTableViewCell];
    [self hs_setAddressMessage];
    
    self.frame = CGRectMake(0, HEIGHT_OF_SCREEN, WIDTH_OF_SCREEN, provinceCityCountyViewHeight);
   
}
#pragma 黑色遮罩
- (UIView*)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        _bgView.alpha = 0;
    }
    return _bgView;
    
}

- (void)hs_setAddressMessage
{
    NSString *str = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"city" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
    _dataArray = [NSString yk_deserializeMessageJSON:str];
    _provinceArray = [@[] mutableCopy];
    _cityArray = [@[] mutableCopy];
    _countyArray = [@[] mutableCopy];
    __weak typeof(self) weakSelf = self;
    [_dataArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [weakSelf.provinceArray addObject:obj[@"name"]];
        if (idx == 0) {
            [obj[@"city"] enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [weakSelf.cityArray addObject:obj[@"name"]];
                if (idx == 0) {
                    [weakSelf.countyArray addObjectsFromArray:obj[@"area"]];
                }
            }];
        }
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.provinceTableView) {
        return self.provinceArray.count;
    } else if (tableView == self.cityTableView) {
        return self.cityArray.count;
    } else {
        return  self.countyArray.count;
    }
    
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JSQBProvinceTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:kJSQBProvinceTableViewCell forIndexPath:indexPath];
    if (tableView == self.provinceTableView) {
        cell.contentLabel.text = [self.provinceArray objectAtIndex:indexPath.row];
        if ( indexPath.row == _selectedProvince) {
            cell.contentLabel.textColor = MAIN_THEME_COLOR;
        }else{
            cell.contentLabel.textColor = BLACK_COLOR_33;
        }
    }else if  (tableView == self.cityTableView) {
        cell.contentLabel.text = [self.cityArray objectAtIndex:indexPath.row];
        if ( indexPath.row == _selectedCity) {
            cell.contentLabel.textColor = MAIN_THEME_COLOR;
        }else{
            cell.contentLabel.textColor = BLACK_COLOR_33;
        }
    }else {
         cell.contentLabel.text = [self.countyArray objectAtIndex:indexPath.row];
        if ( indexPath.row == _selectedCounty) {
            cell.contentLabel.textColor = MAIN_THEME_COLOR;
        }else{
            cell.contentLabel.textColor = BLACK_COLOR_33;
        }

    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.provinceTableView) {
        self.provinceTableViewWidth.constant = WIDTH_OF_SCREEN/2;
        [self.provinceTableView layoutIfNeeded];
        self.cityTableViewWidth.constant = WIDTH_OF_SCREEN/2;
        [self.cityTableView layoutIfNeeded];
        
        _selectedProvince = indexPath.row;
        _selectedCity = 0;
        _selectedCounty = 0;
        [self.cityArray removeAllObjects];
        [self.countyArray removeAllObjects];
        __weak typeof(self) weakSelf = self;
        [_dataArray[indexPath.row][@"city"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [weakSelf.cityArray addObject:obj[@"name"]];
            if (idx == 0) {
                [weakSelf.countyArray addObjectsFromArray:obj[@"area"]];
            }
        }];
        
        [self.cityTableView reloadData];
        [self.countyTableView reloadData];
        [self.provinceTableView reloadData];
       
        
    } else if (tableView == self.cityTableView){
        self.provinceTableViewWidth.constant = WIDTH_OF_SCREEN/3;
        [self.provinceTableView layoutIfNeeded];
        
        self.cityTableViewWidth.constant = WIDTH_OF_SCREEN/3;
        [self.cityTableView layoutIfNeeded];
        
        self.countyTableViewWidth.constant = WIDTH_OF_SCREEN/3;
        [self.countyTableView layoutIfNeeded];
        _selectedCity = indexPath.row;
        _selectedCounty = 0;
        [self.countyArray removeAllObjects];
        [self.countyArray addObjectsFromArray:_dataArray[_selectedProvince][@"city"][indexPath.row][@"area"]];
        [self.countyTableView reloadData];
        [self.cityTableView reloadData];
    } else {
        _selectedCounty = indexPath.row;
        [self.countyTableView reloadData];
        
        NSString *pro = _provinceArray[_selectedProvince];
        NSString *city = _cityArray[_selectedCity];
        NSString *county = _countyArray[_selectedCounty];
        
        //V4.0版本要求：部分市、区名称中会有地区两字，例如“毕节地区”，在遇到地区字样时，统一去掉地区二字再上报
        city = [city stringByReplacingOccurrencesOfString:@"地区" withString:@""];
        county = [county stringByReplacingOccurrencesOfString:@"地区" withString:@""];
        
        self.provincestrCityCountyStr = [NSString stringWithFormat:@"%@ %@ %@",pro,city,county];
        
        if (self.onDismissCompletion) {
            self.onDismissCompletion(self.provincestrCityCountyStr,@"");
        }
        [self dismissProvinceCityCountyView];
        
    }

}

#pragma mark - Show/hide provinceCityCountyView  methods

- (void)showProvinceCityCountyViewWithFormattedAddress:(NSString *)provinceCityDistrictStr streetNumberStr:(NSString *)streetNumberStr{
    
    if ([YKTools yk_permissionsWithLocation] == YKPermissionStatusAuthorized) {
        self.localizeViewHeight.constant = 60 * ASPECT_RATIO_WIDTH;
        if (streetNumberStr.length && provinceCityDistrictStr.length) {
            self.provinceCityDistrictStr = provinceCityDistrictStr;
            self.streetNumberStr = streetNumberStr;
            self.localizeLabel.text = [NSString stringWithFormat:@"当前地址:%@",streetNumberStr];
            [self prepareUseButtonEnabled:YES];
        } else {
            self.localizeLabel.text = @"定位中...";
            [self prepareUseButtonEnabled:NO];
        }
    } else {
        self.localizeViewHeight.constant = 0;
    }
    [self showProvinceCityCountyView];
}

- (void)reloadLicationViewWithDistrict:(NSString *)provinceCityDistrictStr streetNumberStr:(NSString *)streetNumberStr
{
        self.provinceCityDistrictStr = provinceCityDistrictStr;
        self.streetNumberStr = streetNumberStr;
        self.localizeLabel.text = [NSString stringWithFormat:@"当前地址:%@",streetNumberStr];
        [self prepareUseButtonEnabled:YES];
}

#pragma mark- 配置使用button样式
- (void)prepareUseButtonEnabled:(BOOL)enabled
{
    self.useButton.userInteractionEnabled = enabled ? YES : NO;
    [self.useButton setTitleColor:(enabled?MAIN_THEME_COLOR:[UIColor yk_colorWithHexString:YKUseButtonColor]) forState:(UIControlStateNormal)];
    self.useButton.layer.borderColor = enabled? MAIN_THEME_COLOR.CGColor : [UIColor yk_colorWithHexString:YKUseButtonColor].CGColor;
}

- (void)showProvinceCityCountyView
{
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgView];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 1;
        self.frame = CGRectMake(0, HEIGHT_OF_SCREEN - provinceCityCountyViewHeight, WIDTH_OF_SCREEN, provinceCityCountyViewHeight);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismissProvinceCityCountyView
{
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 0;
        self.frame = CGRectMake(0, HEIGHT_OF_SCREEN, WIDTH_OF_SCREEN, provinceCityCountyViewHeight);
    } completion:^(BOOL finished) {

    }];
}

- (IBAction)useBtnClick:(id)sender {
    
    if (self.onDismissCompletion) {
        self.onDismissCompletion(self.provinceCityDistrictStr,self.streetNumberStr);
    }
     [self dismissProvinceCityCountyView];
}

- (IBAction)cancelBtnClick:(id)sender {
    
    [self dismissProvinceCityCountyView];
}
@end
