//
//  JSQBProvinceTableViewCell.h
//  KDFDApp
//
//  Created by zhaoying on 2017/8/28.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSQBProvinceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
