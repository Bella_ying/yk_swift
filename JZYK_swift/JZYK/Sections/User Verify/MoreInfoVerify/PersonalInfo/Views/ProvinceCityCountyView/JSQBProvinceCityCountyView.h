//
//  JSQBProvinceCityCountyView.h
//  KDFDApp
//
//  Created by zhaoying on 2017/8/28.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSQBProvinceCityCountyView : UIView

//将选中的省市区回传
@property (nonatomic,copy) void (^onDismissCompletion)(NSString *provinceCityDistrictStr,NSString *streetNumberStr);

//显示省市区街道地址
- (void)showProvinceCityCountyViewWithFormattedAddress:(NSString *)provinceCityDistrictStr streetNumberStr:(NSString *)streetNumberStr;
//刷新定位视图
- (void)reloadLicationViewWithDistrict:(NSString *)provinceCityDistrictStr streetNumberStr:(NSString *)streetNumberStr;

@end
