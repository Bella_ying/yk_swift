//
//  YKPersonalInfoModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface YKPersonDegreesModel : NSObject

@property (nonatomic, copy) NSString   *degrees;
@property (nonatomic, copy) NSString   *name;

@end

@interface YKPersonLiveTimeModel : NSObject

@property (nonatomic, copy) NSString   *live_time_type;
@property (nonatomic, copy) NSString   *name;

@end

@interface YKPersonMarriageModel : NSObject

@property (nonatomic, copy) NSString *marriage;
@property (nonatomic, copy) NSString   *name;

@end

@interface YKPersonSubInfoModel : NSObject

@property (nonatomic, copy) NSString *address;          // 详细地址
@property (nonatomic, copy) NSString *address_distinct; // 现居地区
@property (nonatomic, copy) NSString *address_period;   // 居住时长
@property (nonatomic, copy) NSString *degrees;          // 学历
@property (nonatomic, copy) NSString *marriage;         // 婚姻状况
@property (nonatomic, copy) NSString *debt_type;
@property (nonatomic, copy) NSString *income_type;
@property (nonatomic, copy) NSString *repayment_source_type;

@end

@interface YKPersonRepaymentModel : NSObject

@property (nonatomic, copy) NSString *repayment_source_type;
@property (nonatomic, copy) NSString *name;

@end

@interface YKPersonInComeModel : NSObject

@property (nonatomic, copy) NSString *income_type;
@property (nonatomic, copy) NSString *name;

@end

@interface YKPersonDebtModel : NSObject

@property (nonatomic, copy) NSString *debt_type;
@property (nonatomic, copy) NSString *name;

@end

@interface YKPersonalInfoModel : NSObject

@property (nonatomic, strong) YKPersonSubInfoModel *info;
@property (nonatomic, copy) NSArray <YKPersonRepaymentModel *> *repayment_source_type_all;
@property (nonatomic, copy) NSArray <YKPersonInComeModel *>    *income_type_all;
@property (nonatomic, copy) NSArray <YKPersonDebtModel *>      *debt_type_all;
@property (nonatomic, copy) NSArray <YKPersonDegreesModel *>   *degrees_all; // 学历选项
@property (nonatomic, copy) NSArray <YKPersonLiveTimeModel *>  *live_time_type_all; // 居住时长选项
@property (nonatomic, copy) NSArray <YKPersonMarriageModel *>  *marriage_all; // 婚姻状况选项

@end
