//
//  YKPersonalInfoModel.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKPersonalInfoModel.h"

@implementation YKPersonDegreesModel

@end

@implementation YKPersonLiveTimeModel

@end

@implementation YKPersonMarriageModel

@end

@implementation YKPersonSubInfoModel

@end

@implementation YKPersonRepaymentModel

@end

@implementation YKPersonInComeModel

@end

@implementation YKPersonDebtModel

@end

@implementation YKPersonalInfoModel

+ (NSDictionary<NSString *,id> *)modelContainerPropertyGenericClass
{
    return @{@"degrees_all"        : [YKPersonDegreesModel class],
             @"live_time_type_all" : [YKPersonLiveTimeModel class],
             @"marriage_all"       : [YKPersonMarriageModel class],
             @"repayment_source_type_all" : [YKPersonRepaymentModel class],
             @"income_type_all"    : [YKPersonInComeModel class],
             @"debt_type_all"      : [YKPersonDebtModel class]
             };
}

@end
