//
//  YKMineUserInfoViewController.m
//  JZYK
//
//  Created by hongyu on 2018/6/18.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKMineUserInfoViewController.h"
#import "YKPersonalInfoModel.h"
#import "QBPickerView.h"
#import "JSQBProvinceCityCountyView.h"
#import "YKLocationManager.h"

static const NSInteger unSelectedStatus = 0;
static const NSInteger selectedStatus   = 1;
static const CGFloat   goUpHeight       = 100.f;

@interface YKMineUserInfoViewController ()<UITextFieldDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView       *subView;
@property (weak, nonatomic) IBOutlet UIView             *subBottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subHeight;
@property (weak, nonatomic) IBOutlet UIButton           *refundSourceButton;
@property (weak, nonatomic) IBOutlet UIButton           *incomeButton;
@property (weak, nonatomic) IBOutlet UIButton           *debtButton;
@property (weak, nonatomic) IBOutlet UIButton           *educationButton;
@property (weak, nonatomic) IBOutlet UIButton           *marriageButton;
@property (weak, nonatomic) IBOutlet UIButton           *cityButton;
@property (weak, nonatomic) IBOutlet UITextField        *addressTextField;
@property (weak, nonatomic) IBOutlet UIButton           *liveTimeButton;
@property (weak, nonatomic) IBOutlet UIButton           *saveButton;
@property (strong, nonatomic) NSMutableArray            *refundSounrceArray;
@property (strong, nonatomic) NSMutableArray            *incomeArray;
@property (strong, nonatomic) NSMutableArray            *debtArray;
@property (strong, nonatomic) NSMutableArray            *educationArray;
@property (strong, nonatomic) NSMutableArray            *marriageArray;
@property (strong, nonatomic) NSMutableArray            *liveTimeArray;
@property (strong, nonatomic) NSMutableArray            *refundSounrceModelArray;
@property (strong, nonatomic) NSMutableArray            *incomeModelArray;
@property (strong, nonatomic) NSMutableArray            *debtModelArray;
@property (strong, nonatomic) NSMutableArray            *educationModelArray;
@property (strong, nonatomic) NSMutableArray            *marriageModelArray;
@property (strong, nonatomic) NSMutableArray            *liveTimeModelArray;
@property (nonatomic, strong) JSQBProvinceCityCountyView *cityView;
@property (nonatomic, strong) CLLocation                 *location;
@property (nonatomic, copy)   NSString                   *provinceCityDistrictStr;
@property (nonatomic, copy)   NSString                   *streetNumberStr;

@end

@implementation YKMineUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareUI];
    [self prepareLocation];
}

- (void)prepareUI
{
    self.navigationItem.title = @"个人信息";
    self.subHeight.constant   = HEIGHT_OF_SCREEN - NAV_HEIGHT - BOTTOM_HEIGHT + 1;
    self.addressTextField.delegate  = self;
    [self.addressTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.addressTextField.tintColor = MAIN_THEME_COLOR;
    self.subView.delegate = self;
    self.refundSounrceModelArray = [NSMutableArray array];
    self.incomeModelArray = [NSMutableArray array];
    self.debtModelArray = [NSMutableArray array];
    self.educationModelArray = [NSMutableArray array];
    self.marriageModelArray = [NSMutableArray array];
    self.liveTimeModelArray = [NSMutableArray array];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self prepareDataSource];
}

- (void)prepareDataSource
{
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kCreditCardGetPersonAdditionInfo showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        if (code == 0 && success) {
            YKPersonalInfoModel *infoModel = [YKPersonalInfoModel yy_modelWithDictionary:json];
            [weakSelf.refundSounrceArray removeAllObjects];
            [weakSelf.refundSounrceModelArray removeAllObjects];
            for (YKPersonRepaymentModel *model in infoModel.repayment_source_type_all) {
                [weakSelf.refundSounrceArray addObject:model.name];
                [weakSelf.refundSounrceModelArray addObject:model];
            }
            [weakSelf.incomeArray removeAllObjects];
            [weakSelf.incomeModelArray removeAllObjects];
            for (YKPersonInComeModel *model in infoModel.income_type_all) {
                [weakSelf.incomeArray addObject:model.name];
                [weakSelf.incomeModelArray addObject:model];
            }
            [weakSelf.debtArray removeAllObjects];
            [weakSelf.debtModelArray removeAllObjects];
            for (YKPersonDebtModel *model in infoModel.debt_type_all) {
                [weakSelf.debtArray addObject:model.name];
                [weakSelf.debtModelArray addObject:model];
            }
            [weakSelf.educationArray removeAllObjects];
            [weakSelf.educationModelArray removeAllObjects];
            for (YKPersonDegreesModel *model in infoModel.degrees_all) {
                [weakSelf.educationArray addObject:model.name];
                [weakSelf.educationModelArray addObject:model];
            }
            [weakSelf.marriageArray removeAllObjects];
            [weakSelf.marriageModelArray removeAllObjects];
            for (YKPersonMarriageModel *model in infoModel.marriage_all) {
                [weakSelf.marriageArray addObject:model.name];
                [weakSelf.marriageModelArray addObject:model];
            }
            [weakSelf.liveTimeArray removeAllObjects];
            [weakSelf.liveTimeModelArray removeAllObjects];
            for (YKPersonLiveTimeModel *model in infoModel.live_time_type_all) {
                [weakSelf.liveTimeArray addObject:model.name];
                [weakSelf.liveTimeModelArray addObject:model];
            }
            if (infoModel.info) {
                [weakSelf.refundSourceButton setTitle:[weakSelf typeWithRefundSource:infoModel.info.repayment_source_type] forState:UIControlStateNormal];
                [weakSelf.incomeButton setTitle:[weakSelf typeWithIncome:infoModel.info.income_type] forState:UIControlStateNormal];
                [weakSelf.debtButton setTitle:[weakSelf typeWithDebt:infoModel.info.debt_type] forState:UIControlStateNormal];
                [weakSelf.educationButton setTitle:[weakSelf typeWithDegrees:infoModel.info.degrees] forState:UIControlStateNormal];
                [weakSelf.marriageButton setTitle:[weakSelf typeWithMarriage:infoModel.info.marriage] forState:UIControlStateNormal];
                [weakSelf.liveTimeButton setTitle:[weakSelf typeWithLiveTime:infoModel.info.address_period] forState:UIControlStateNormal];
                [weakSelf.cityButton setTitle:infoModel.info.address_distinct.length ? infoModel.info.address_distinct : @"请选择" forState:UIControlStateNormal];
                weakSelf.addressTextField.text = infoModel.info.address ? infoModel.info.address : @"";
                [weakSelf changeColorWithStatus:[infoModel.info.repayment_source_type integerValue] ? selectedStatus : unSelectedStatus button:weakSelf.refundSourceButton];
                [weakSelf changeColorWithStatus:[infoModel.info.income_type integerValue] ? selectedStatus : unSelectedStatus button:weakSelf.incomeButton];
                [weakSelf changeColorWithStatus:[infoModel.info.debt_type integerValue] ? selectedStatus : unSelectedStatus button:weakSelf.debtButton];
                [weakSelf changeColorWithStatus:[infoModel.info.degrees integerValue] ? selectedStatus : unSelectedStatus button:weakSelf.educationButton];
                [weakSelf changeColorWithStatus:[infoModel.info.marriage integerValue] ? selectedStatus : unSelectedStatus button:weakSelf.marriageButton];
                [weakSelf changeColorWithStatus:[infoModel.info.address_period integerValue] ? selectedStatus : unSelectedStatus button:weakSelf.liveTimeButton];
                [weakSelf changeColorWithStatus:[infoModel.info.address_distinct integerValue] || infoModel.info.address_distinct.length > 1 ? selectedStatus : unSelectedStatus button:weakSelf.cityButton];
            }
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

#pragma mark- 还款来源
- (IBAction)refundSourceAction:(id)sender
{
    [[QBPickerView sharedPickerManager] showSinglePickerByDefaultChoiseWithData:self.refundSounrceArray confirmComplete:^(NSString * _Nonnull selectStr) {
        [self.refundSourceButton setTitle:selectStr forState:UIControlStateNormal];
        [self changeColorWithStatus:selectedStatus button:self.refundSourceButton];
    }];
}

#pragma mark- 收入情况
- (IBAction)incomeAction:(id)sender
{
    [[QBPickerView sharedPickerManager] showSinglePickerByDefaultChoiseWithData:self.incomeArray confirmComplete:^(NSString * _Nonnull selectStr) {
        [self.incomeButton setTitle:selectStr forState:UIControlStateNormal];
        [self changeColorWithStatus:selectedStatus button:self.incomeButton];
    }];
}

#pragma mark- 债务信息
- (IBAction)debtAction:(id)sender
{
    [[QBPickerView sharedPickerManager] showSinglePickerByDefaultChoiseWithData:self.debtArray confirmComplete:^(NSString * _Nonnull selectStr) {
        [self.debtButton setTitle:selectStr forState:UIControlStateNormal];
        [self changeColorWithStatus:selectedStatus button:self.debtButton];
    }];
}

#pragma mark- 学历
- (IBAction)educationAction:(id)sender
{
    [[QBPickerView sharedPickerManager] showSinglePickerByDefaultChoiseWithData:self.educationArray confirmComplete:^(NSString * _Nonnull selectStr) {
        [self.educationButton setTitle:selectStr forState:UIControlStateNormal];
        [self changeColorWithStatus:selectedStatus button:self.educationButton];
    }];
}

#pragma mark- 婚姻状况
- (IBAction)marriageAction:(id)sender
{
    [[QBPickerView sharedPickerManager] showSinglePickerByDefaultChoiseWithData:self.marriageArray confirmComplete:^(NSString * _Nonnull selectStr) {
        [self.marriageButton setTitle:selectStr forState:UIControlStateNormal];
        [self changeColorWithStatus:selectedStatus button:self.marriageButton];
    }];
}

#pragma mark- 现居城市
- (IBAction)cityAction:(id)sender
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    WEAK_SELF
    self.cityView.onDismissCompletion = ^(NSString *provinceCityDistrictStr, NSString *streetNumberStr) {
    
        [weakSelf.cityButton setTitle:provinceCityDistrictStr forState:UIControlStateNormal];
        [weakSelf changeColorWithStatus:selectedStatus button:weakSelf.cityButton];
        if (weakSelf.streetNumberStr) {
            weakSelf.addressTextField.text = weakSelf.streetNumberStr;
        }
    };
    [self.cityView showProvinceCityCountyViewWithFormattedAddress:self.provinceCityDistrictStr streetNumberStr:self.streetNumberStr];
}

#pragma mark- 详细地址
- (void)textFieldDidChange:(UITextField *)textField {
    [InputView limitTextWithLength:30 forTextField:textField];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.3f animations:^{
       [self.subBottomView setY:-goUpHeight];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:NO];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.view endEditing:NO];
    [UIView animateWithDuration:0.3f animations:^{
        [self.subBottomView setY:0];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:NO];
}

#pragma mark- 居住时长

- (IBAction)liveTimeAction:(id)sender
{
    [[QBPickerView sharedPickerManager] showSinglePickerByDefaultChoiseWithData:self.liveTimeArray confirmComplete:^(NSString * _Nonnull selectStr) {
        [self.liveTimeButton setTitle:selectStr forState:UIControlStateNormal];
        [self changeColorWithStatus:selectedStatus button:self.liveTimeButton];
    }];
}
#pragma mark- 位置信息
- (void)prepareLocation
{
    if ([YKTools yk_permissionsWithLocation] == YKPermissionStatusAuthorized) {
        WEAK_SELF
        
        [[YKLocationManager location] yk_enableLocationServiceWithCompletion:^(CLLocation *location, AMapLocationReGeocode *regeoCode) {
            weakSelf.location = location;
//            V4.0版本要求：部分市、区名称中会有地区两字，例如“毕节地区”，在遇到地区字样时，统一去掉地区二字再上报
            regeoCode.city = [regeoCode.city stringByReplacingOccurrencesOfString:@"地区" withString:@""];
            regeoCode.district = [regeoCode.district stringByReplacingOccurrencesOfString:@"地区" withString:@""];
            weakSelf.provinceCityDistrictStr = [NSString stringWithFormat:@"%@ %@ %@", regeoCode.province, regeoCode.city, regeoCode.district];
            weakSelf.streetNumberStr = [NSString stringWithFormat:@"%@%@", regeoCode.street, regeoCode.number];
            [weakSelf.cityView reloadLicationViewWithDistrict:weakSelf.provinceCityDistrictStr streetNumberStr:weakSelf.streetNumberStr];
        }];
    }
}

#pragma mark- 保存个人信息
- (IBAction)saveAction:(id)sender
{
    if ([self.refundSourceButton.titleLabel.text isEqualToString:@"请选择"]) {
        return [[iToast makeText:@"还款来源不能为空"] show];
    }
    if ([self.incomeButton.titleLabel.text isEqualToString:@"请选择"]) {
        return [[iToast makeText:@"收入情况不能为空"] show];
    }
    if ([self.debtButton.titleLabel.text isEqualToString:@"请选择"]) {
        return [[iToast makeText:@"债务信息不能为空"] show];
    }
    if ([self.educationButton.titleLabel.text isEqualToString:@"请选择"]) {
        return [[iToast makeText:@"学历不能为空"] show];
    }
    if ([self.marriageButton.titleLabel.text isEqualToString:@"请选择"]) {
        return [[iToast makeText:@"婚姻状况不能为空"] show];
    }
    if ([self.cityButton.titleLabel.text isEqualToString:@"请选择"]) {
        return [[iToast makeText:@"现居城市不能为空"] show];
    }
    if (!self.addressTextField.text.length) {
        return [[iToast makeText:@"详细地址不能为空"] show];
    }
    if ([self.liveTimeButton.titleLabel.text isEqualToString:@"请选择"]) {
        return [[iToast makeText:@"居住时长不能为空"] show];
    }
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditCardSavePersonAdditionInfo
                                 showLoading:NO
                                       param:@{@"repayment_source_type" : [self typeWithRefundSource:self.refundSourceButton.titleLabel.text],
                                               @"income_type" : [self typeWithIncome:self.incomeButton.titleLabel.text],
                                               @"debt_type" : [self typeWithDebt:self.debtButton.titleLabel.text],
                                               @"degrees" : [self typeWithDegrees:self.educationButton.titleLabel.text],
                                               @"marriage" : [self typeWithMarriage:self.marriageButton.titleLabel.text],
                                               @"live_time_type" : [self typeWithLiveTime:self.liveTimeButton.titleLabel.text],
                                               @"address" : weakSelf.addressTextField.text,
                                               @"address_distinct" : weakSelf.cityButton.titleLabel.text,
                                               @"longitude" : [NSString stringWithFormat:@"%f", self.location.coordinate.longitude],
                                               @"latitude" : [NSString stringWithFormat:@"%f", self.location.coordinate.latitude]}
                                     succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString * _Nonnull msg) {
                                         
                                         [[iToast makeText:msg] show];
                                         if ([json[@"code"] integerValue] == 0) {
                                             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                 [weakSelf.navigationController popViewControllerAnimated:YES];
                                             });
                                         }
                                         
    } failure:^(NSString * _Nonnull msg, BOOL isConnect) {
        [[iToast makeText:msg] show];
    }];
}

- (NSString *)typeWithRefundSource:(NSString *)refundSource
{
    for (YKPersonRepaymentModel *model in self.refundSounrceModelArray) {
        if ([refundSource isEqualToString:model.name]) {
            return model.repayment_source_type;
        }
        if ([refundSource isEqualToString:model.repayment_source_type]) {
            return model.name;
        }
    }
    return @"请选择";
}

- (NSString *)typeWithIncome:(NSString *)income
{
    for (YKPersonInComeModel *model in self.incomeModelArray) {
        if ([income isEqualToString:model.name]) {
            return model.income_type;
        }
        if ([income isEqualToString:model.income_type]) {
            return model.name;
        }
    }
    return @"请选择";
}

- (NSString *)typeWithDebt:(NSString *)debt
{
    for (YKPersonDebtModel *model in self.debtModelArray) {
        if ([debt isEqualToString:model.name]) {
            return model.debt_type;
        }
        if ([debt isEqualToString:model.debt_type]) {
            return model.name;
        }
    }
    return @"请选择";
}

- (NSString *)typeWithDegrees:(NSString *)degrees
{
    for (YKPersonDegreesModel *model in self.educationModelArray) {
        if ([degrees isEqualToString:model.name]) {
            return model.degrees;
        }
        if ([degrees isEqualToString:model.degrees]) {
            return model.name;
        }
    }
    return @"请选择";
}

- (NSString *)typeWithMarriage:(NSString *)marriage
{
    for (YKPersonMarriageModel *model in self.marriageModelArray) {
        if ([marriage isEqualToString:model.name]) {
            return model.marriage;
        }
        if ([marriage isEqualToString:model.marriage]) {
            return model.name;
        }
    }
    return @"请选择";
}

- (NSString *)typeWithLiveTime:(NSString *)time
{
    for (YKPersonLiveTimeModel *model in self.liveTimeModelArray) {
        if ([time isEqualToString:model.name]) {
             return model.live_time_type;
        }
        if ([time isEqualToString:model.live_time_type]) {
            return model.name;
        }
    }
    return @"请选择";
}

#pragma mark- 更改文本颜色
- (void)changeColorWithStatus:(NSInteger)status button:(UIButton *)button
{
    [button setTitleColor:status == unSelectedStatus ? GRAY_COLOR_CC : LABEL_TEXT_COLOR forState:UIControlStateNormal];
}

#pragma mark- 懒加载数据源
- (NSMutableArray *)refundSounrceArray
{
    if (!_refundSounrceArray) {
        _refundSounrceArray = [@[] mutableCopy];
    }
    return _refundSounrceArray;
}

- (NSMutableArray *)incomeArray
{
    if (!_incomeArray) {
        _incomeArray = [@[] mutableCopy];
    }
    return _incomeArray;
}

- (NSMutableArray *)debtArray
{
    if (!_debtArray) {
        _debtArray = [@[] mutableCopy];
    }
    return _debtArray;
}

- (NSMutableArray *)educationArray
{
    if (!_educationArray) {
        _educationArray = [@[] mutableCopy];
    }
    return _educationArray;
}

- (NSMutableArray *)marriageArray
{
    if (!_marriageArray) {
        _marriageArray = [@[] mutableCopy];
    }
    return _marriageArray;
}

- (NSMutableArray *)liveTimeArray
{
    if (!_liveTimeArray) {
        _liveTimeArray = [@[] mutableCopy];
    }
    return _liveTimeArray;
}

- (JSQBProvinceCityCountyView *)cityView
{
    if (!_cityView) {
        _cityView = [[[NSBundle mainBundle] loadNibNamed:@"JSQBProvinceCityCountyView"
                                                   owner:self
                                                 options:nil] firstObject];
    }
    return _cityView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
