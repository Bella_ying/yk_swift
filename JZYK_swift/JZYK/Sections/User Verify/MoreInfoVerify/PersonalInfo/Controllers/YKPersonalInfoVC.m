//
//  YKPersonalInfoVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKPersonalInfoVC.h"
#import "YKPersonalInfoCell.h"
#import "YKPersonalInfoFieldCell.h"
#import "QBPickerView.h"
#import "JSQBProvinceCityCountyView.h"
#import "YKPersonalInfoModel.h"


static const NSInteger educationTag = 1000;
static const NSInteger marriageTag  = 1001;
static const NSInteger areaTag      = 1002;
static const NSInteger addressTag   = 1003;
static const NSInteger liveTimeTag  = 1004;
static NSString * const tempString   = @"请选择";
static NSString * const changeString = @"（选填）";
static NSString * const changeColor  = @"#999999";
static const CGFloat    fontSize     = 13.f;

@interface YKPersonalInfoVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *commitBtn;

// tableView
@property (nonatomic, strong) UITableView    *infoTableView;
@property (nonatomic, strong) NSMutableArray *firstTitleArray;
@property (nonatomic, strong) NSMutableArray *secondTitleArray;
@property (nonatomic, strong) NSMutableArray *thirdTitleArray;
//@property (nonatomic, strong) CLLocation     *location;
@property (nonatomic, copy)   NSString       *provinceCityDistrictStr; // 省市区地址
@property (nonatomic, copy)   NSString       *streetNumberStr; // 街道，号码，地址
@property (nonatomic, strong) JSQBProvinceCityCountyView *cityView;
@property (nonatomic, assign) NSInteger      tag;
@property (nonatomic, strong) NSMutableArray  *degreesArray;
@property (nonatomic, strong) NSMutableArray  *liveTimeArray;
@property (nonatomic, strong) NSMutableArray  *marriageArray;
@property (nonatomic, strong) NSMutableArray  *degreesOptionArray;
@property (nonatomic, strong) NSMutableArray  *liveTimeOptionArray;
@property (nonatomic, strong) NSMutableArray  *marriageOptionArray;


@end

@implementation YKPersonalInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.navigationItem.title = @"个人信息";
    self.view.backgroundColor = GRAY_BACKGROUND_COLOR;
    [self prepareUI];
    [self yk_obtainlocation];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self prepareData];
    
}

#pragma mark- 配置UI
- (void)prepareUI
{
    
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(65 + 25 * ASPECT_RATIO_WIDTH);
    }];
    [self.bottomView addSubview:self.commitBtn];
    [self.commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bottomView);
        make.bottom.equalTo(self.bottomView).offset(-25 * ASPECT_RATIO_WIDTH);
    }];
    [self.commitBtn addTarget:self action:@selector(clickSaveBtn) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self.view addSubview:self.infoTableView];
    [self.infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
}

- (void)clickSaveBtn{
    DLog(@"点击保存");
}

#pragma mark- tableView deletage
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.firstTitleArray.count;
    } else if(section == 1) {
        return self.secondTitleArray.count;
    }else{
        return self.thirdTitleArray.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 30.f;
    } else {
        return 15.f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2 && indexPath.row == 1) {
        YKPersonalInfoFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YKPersonalInfoFieldCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.addressTextField.tag = addressTag;
        return cell;
    } else {
        YKPersonalInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YKPersonalInfoCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.section == 1) {
            if (indexPath.row == 1) {
                cell.infoTitleLabel.attributedText = [self prepareAttributedStringWithText:self.secondTitleArray[indexPath.row] changeText:changeString colorString:changeColor fontSize:fontSize];
            } else {
                cell.infoTitleLabel.text = self.secondTitleArray[indexPath.row];
            }
            if (indexPath.row == 0) cell.infoContentLabel.tag = educationTag;
            if (indexPath.row == 1) cell.infoContentLabel.tag = marriageTag;
        } else if (indexPath.section == 2){
            if (indexPath.row == 2) {
                cell.infoTitleLabel.attributedText = [self prepareAttributedStringWithText:self.thirdTitleArray[indexPath.row] changeText:changeString colorString:changeColor fontSize:fontSize];
            } else {
                cell.infoTitleLabel.text = self.thirdTitleArray[indexPath.row];
            }
            if (indexPath.row == 0) cell.infoContentLabel.tag = areaTag;
            if (indexPath.row == 2) cell.infoContentLabel.tag = liveTimeTag;
        } else { // 0

            cell.infoTitleLabel.text = self.thirdTitleArray[indexPath.row];

        }
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 30)];
    headerView.backgroundColor = GRAY_BACKGROUND_COLOR;
    if (section == 2) {
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, 6, WIDTH_OF_SCREEN, 18)];
        headerLabel.text = @"联系地址";
        headerLabel.font = [UIFont systemFontOfSize:adaptFontSize(13)];
        headerLabel.textColor = [UIColor yk_colorWithHexString:@"#8D8D8D"];
        [headerView addSubview:headerLabel];
    }
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            
            [self selectedCellWithTag:educationTag datas:self.degreesOptionArray];
        }
        if (indexPath.row == 1) {
            [self selectedCellWithTag:marriageTag datas:self.marriageOptionArray];
        }
    } else {
        if (indexPath.row == 0) {
//            [BuriedPointManager reportWithKey:@"personal" andArray:@[@"现居地址"]];
            [self showCityViewWithTag:areaTag];
        }
        if (indexPath.row == 1) {
            
        }
        if (indexPath.row == 2) {
            [self selectedCellWithTag:liveTimeTag datas:self.liveTimeOptionArray];
        }
    }
}

- (void)selectedCellWithTag:(NSInteger)tag datas:(NSArray *)datas
{
    [[QBPickerView sharedPickerManager] showSinglePickerByDefaultChoiseWithData:datas confirmComplete:^(NSString *selectStr) {
        UILabel *contentLabel = [self.infoTableView viewWithTag:tag];
        contentLabel.text     = selectStr;
    }];
}

#pragma mark- 富文本
- (NSMutableAttributedString *)prepareAttributedStringWithText:(NSString *)text
                                                    changeText:(NSString *)changeText
                                                   colorString:(NSString *)colorString
                                                      fontSize:(CGFloat)fontSize
{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:text];
    
    NSRange range = [text rangeOfString:changeText];
    [str addAttribute:NSForegroundColorAttributeName
                 value:[UIColor yk_colorWithHexString:colorString]
                 range:NSMakeRange(range.location, range.length)];
    [str addAttribute:NSFontAttributeName
                 value:[UIFont systemFontOfSize:fontSize]
                 range:NSMakeRange(range.location, range.length)];
    
    return str;
}

#pragma mark- 获取数据
- (void)prepareData
{
//    WEAK_SELF
//    [QBNetService getPersonInfoWithComplete:^(id result, QBRequestModel *requestModel, NSString *message) {
//        if (result) {
//            QBPersonInfoModel *model = [QBPersonInfoModel yy_modelWithDictionary:result[@"data"]];
//            weakSelf.degreesArray  = [model.degrees_all mutableCopy];
//            weakSelf.marriageArray = [model.marriage_all mutableCopy];
//            weakSelf.liveTimeArray = [model.live_time_type_all mutableCopy];
//            [weakSelf prepareCellForTag:[model.info.degrees integerValue] name:@"education"];
//            [weakSelf prepareCellForTag:[model.info.marriage integerValue] name:@"marriage"];
//            [weakSelf prepareCellForTag:[model.info.address_period integerValue] name:@"liveTime"];
//
//            for (QBPersonDegreesModel  *model in weakSelf.degreesArray) [weakSelf.degreesOptionArray addObject:model.name];
//            for (QBPersonMarriageModel *model in weakSelf.marriageArray) [weakSelf.marriageOptionArray addObject:model.name];
//            for (QBPersonLiveTimeModel *model in weakSelf.liveTimeArray) [weakSelf.liveTimeOptionArray addObject:model.name];
//
//            if (model.info.address_distinct.length) {
//                UILabel *areaAddress = [weakSelf.infoTableView viewWithTag:areaTag];
//                areaAddress.text = model.info.address_distinct;
//            }
//            if (model.info.address.length) {
//                UITextField *addressTf = [weakSelf.infoTableView viewWithTag:addressTag];
//                addressTf.text = model.info.address;
//            }
//        } else {
//            [[iToast makeText:message] show];
//        }
//    }];
}

- (void)prepareCellForTag:(NSInteger)tag name:(NSString *)name
{
    if ([name isEqualToString:@"education"]) {
        UILabel *educationLabel = [self.infoTableView viewWithTag:educationTag];
        educationLabel.text = [self nameWithDegrees:@(tag)];
    } else if ([name isEqualToString:@"marriage"]) {
        UILabel *marriageLabel = [self.infoTableView viewWithTag:marriageTag];
        marriageLabel.text = [self nameWithMarriage:@(tag)];
    } else {
        UILabel *liveTimeLabel = [self.infoTableView viewWithTag:liveTimeTag];
        liveTimeLabel.text = [self nameWithLiveTime:@(tag)];
    }
}

- (NSString *)nameWithDegrees:(NSNumber *)degrees
{
    for (YKPersonDegreesModel *model in self.degreesArray) {
        if ([degrees isEqualToNumber:model.degrees]) return model.name;
    }
    return tempString;
}

- (NSString *)nameWithMarriage:(NSNumber *)marriage
{
    for (YKPersonMarriageModel *model in self.marriageArray) {
        if ([marriage isEqualToNumber:model.marriage]) return model.name;
    }
    return tempString;
}

- (NSString *)nameWithLiveTime:(NSNumber *)time
{
    for (YKPersonLiveTimeModel *model in self.liveTimeArray) {
        if ([time isEqualToNumber:model.live_time_type]) return model.name;
    }
    return tempString;
}

#pragma mark- 修改数据
- (void)updateDataSource
{
    UILabel *degress  = [self.infoTableView viewWithTag:educationTag];
    UILabel *marriage = [self.infoTableView viewWithTag:marriageTag];
    UILabel *liveTime = [self.infoTableView viewWithTag:liveTimeTag];
    UILabel *area     = [self.infoTableView viewWithTag:areaTag];
    UITextField *areaTf = [self.infoTableView viewWithTag:addressTag];
//    WEAK_SELF
//    [QBNetService postPersonInfoWithDegrees:[self typeWithDegrees:degress.text]
//                                   marriage:[self typeWithMarriage:marriage.text]
//                                    address:areaTf.text
//                           address_distinct:[area.text isEqualToString:tempString] ? @"" : area.text
//                                  longitude:[NSString stringWithFormat:@"%f", self.location.coordinate.longitude]
//                                   latitude:[NSString stringWithFormat:@"%f", self.location.coordinate.latitude]
//                             live_time_type:[self typeWithLiveTime:liveTime.text]
//                                   complete:^(id result, QBRequestModel *requestModel, NSString *message) {
//                                       if (result) {
//                                           [weakSelf popAlertWithMessage:message];
//                                       } else {
//                                           [[iToast makeText:message] show];
//                                       }
//                                   }];
}

//- (void)popAlertWithMessage:(NSString *)message
//{
//    WEAK_SELF
//    if (!message) {
//        message = kServerRequestExceptionFaiture;
//    }
//    
//    [[QLAlert alert] showWithTitle:@"" message:message btnTitleArray:@[kAleartConfirm] btnClicked:^(NSInteger index) {
//        STRONG_SELF
//        [strongSelf.navigationController popViewControllerAnimated:YES];
//    }];
//}

- (NSString *)typeWithDegrees:(NSString *)degrees
{
    for (YKPersonDegreesModel *model in self.degreesArray) {
        if ([degrees isEqualToString:model.name]) return [NSString stringWithFormat:@"%@", model.degrees];
    }
    return @"";
}

- (NSString *)typeWithMarriage:(NSString *)marriage
{
    for (YKPersonMarriageModel *model in self.marriageArray) {
        if ([marriage isEqualToString:model.name]) return [NSString stringWithFormat:@"%@", model.marriage];
    }
    return @"";
}

- (NSString *)typeWithLiveTime:(NSString *)time
{
    for (YKPersonLiveTimeModel *model in self.liveTimeArray) {
        if ([time isEqualToString:model.name]) return [NSString stringWithFormat:@"%@", model.live_time_type];
    }
    return @"";
}

// 获取定位信息
- (void)yk_obtainlocation
{
    /*
    if ([QBTools qb_permissionsWithLocation] == QBPermissionStatusAuthorized) {
        WEAK_SELF
        [KDGDLocationManager shareInstance].locationSuccessBlock=^(AMapLocationReGeocode *regeoCode,NSString *errorStr,CLLocation *location) {
            STRONG_SELF
            strongSelf.location  = location;
            //V4.0版本要求：部分市、区名称中会有地区两字，例如“毕节地区”，在遇到地区字样时，统一去掉地区二字再上报
            regeoCode.city = [regeoCode.city stringByReplacingOccurrencesOfString:kArea withString:@""];
            regeoCode.district = [regeoCode.district stringByReplacingOccurrencesOfString:kArea withString:@""];
            
            strongSelf.provinceCityDistrictStr = NSStringFormat(@"%@ %@ %@",regeoCode.province,regeoCode.city,regeoCode.district);
            strongSelf.streetNumberStr = NSStringFormat(@"%@%@",regeoCode.street,regeoCode.number);
            [strongSelf.cityView reloadLicationViewWithDistrict:strongSelf.provinceCityDistrictStr streetNumberStr:strongSelf.streetNumberStr];
        };
        [[KDGDLocationManager shareInstance] startLocation];
    } else {
        //        [[QBAlertView sharedAlertManager] showAlertType:QBShowAlertTypePermissions contentText:@"定位"];
    }
     */
}

- (void)showCityViewWithTag:(NSInteger)tag
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    WEAK_SELF
    self.cityView.onDismissCompletion = ^(NSString *provinceCityDistrictStr,NSString *streetNumberStr) {
        UILabel *contentLabel = [weakSelf.infoTableView viewWithTag:areaTag];
        contentLabel.text     = provinceCityDistrictStr;
        if (streetNumberStr) {
            UITextField *addressTextField = [weakSelf.infoTableView viewWithTag:addressTag];
            addressTextField.text = streetNumberStr;
        }
    };
    [self.cityView showProvinceCityCountyViewWithFormattedAddress:self.provinceCityDistrictStr streetNumberStr:self.streetNumberStr];
}

#pragma -mark -- getter
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _bottomView;
}
- (UIButton *)commitBtn
{
    if (!_commitBtn) {
        _commitBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"system_commit_btn"];
        [_commitBtn setBackgroundImage:image forState:(UIControlStateNormal)];
        [_commitBtn setTitle:@"保存" forState:(UIControlStateNormal)];
        _commitBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [_commitBtn setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
        _commitBtn.layer.cornerRadius = image.size.height * 0.5;
        _commitBtn.layer.masksToBounds = YES;
    }
    return _commitBtn;
}


- (UITableView *)infoTableView
{
    if (!_infoTableView) {
        _infoTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _infoTableView.delegate   = self;
        _infoTableView.dataSource = self;
//        _infoTableView.scrollEnabled = NO;
        _infoTableView.estimatedRowHeight = 0;
        _infoTableView.estimatedSectionHeaderHeight = 0;
        _infoTableView.estimatedSectionFooterHeight = 0;
        _infoTableView.backgroundColor = GRAY_BACKGROUND_COLOR;
        _infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_infoTableView registerNib:[UINib nibWithNibName:@"YKPersonalInfoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"YKPersonalInfoCell"];
        [_infoTableView registerNib:[UINib nibWithNibName:@"YKPersonalInfoFieldCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"YKPersonalInfoFieldCell"];
    }
    return _infoTableView;
}

- (NSMutableArray *)firstTitleArray
{
    if (!_firstTitleArray) {
        _firstTitleArray = [@[@"还款来源", @"收入情况", @"债务信息"] mutableCopy];
    }
    return _firstTitleArray;
}

- (NSMutableArray *)secondTitleArray
{
    if (!_secondTitleArray) {
        _secondTitleArray = [@[@"学历", @"婚姻状况 (选填)"] mutableCopy];
    }
    return _secondTitleArray;
}

- (NSMutableArray *)thirdTitleArray
{
    if (!_thirdTitleArray) {
        _thirdTitleArray = [@[@"现居城市", @"详细地址", @"居住时长 (选填)"] mutableCopy];
    }
    return _thirdTitleArray;
}

- (NSMutableArray *)degreesArray
{
    if (!_degreesArray) _degreesArray = [NSMutableArray array];
    return _degreesArray;
}

- (NSMutableArray *)liveTimeArray
{
    if (!_liveTimeArray) _liveTimeArray = [NSMutableArray array];
    return _liveTimeArray;
}

- (NSMutableArray *)marriageArray
{
    if (!_marriageArray) _marriageArray = [NSMutableArray array];
    return _marriageArray;
}

- (NSMutableArray *)degreesOptionArray
{
    if (!_degreesOptionArray) {
        _degreesOptionArray = [NSMutableArray array];
    }
    return _degreesOptionArray;
}

- (NSMutableArray *)liveTimeOptionArray
{
    if (!_liveTimeOptionArray) {
        _liveTimeOptionArray = [NSMutableArray array];
    }
    return _liveTimeOptionArray;
}

- (NSMutableArray *)marriageOptionArray
{
    if (!_marriageOptionArray) {
        _marriageOptionArray = [NSMutableArray array];
    }
    return _marriageOptionArray;
}

- (JSQBProvinceCityCountyView *)cityView
{
    if (!_cityView) {
        _cityView = [[[NSBundle mainBundle] loadNibNamed:@"JSQBProvinceCityCountyView"
                                                   owner:self
                                                 options:nil] firstObject];
    }
    return _cityView;
}


@end
