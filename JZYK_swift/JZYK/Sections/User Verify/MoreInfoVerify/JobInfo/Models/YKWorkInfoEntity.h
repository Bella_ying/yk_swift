//
//  YKWorkInfoEntity.h
//  KDFDApp
//
//  Created by 吴春艳 on 18/6/18.
//  Copyright © 2018年 kiran. All rights reserved.
//



#pragma mark - 工作信息
//工作行业-制造业，建筑业。。
@interface YKIndustryTypeEntity : NSObject

@property (nonatomic, copy) NSString *business_type;
@property (nonatomic ,copy) NSString * name;
@end

//入司时长
@interface YKWorkEnterTimeEntity : NSObject

@property (nonatomic, copy) NSString *entry_time_type;
@property (nonatomic, copy) NSString *name;

@end

//工作类型
@interface YKworktypeEntity : NSObject

@property (nonatomic, copy) NSString *work_type_id;
@property (nonatomic, copy) NSString *work_type;
@property (nonatomic ,copy) NSString * name;
@end


@interface YKWorkInfoEntity : NSObject

@property (nonatomic, copy) NSString *company_name;
@property (nonatomic, copy) NSString *company_post;
@property (nonatomic, copy) NSString *company_address;
@property (nonatomic, copy) NSString *company_address_distinct;
@property (nonatomic, copy) NSString *company_phone;
@property (nonatomic, copy) NSString *company_period;
@property (nonatomic, copy) NSString *company_picture;
@property (nonatomic ,copy) NSString * company_worktype;
@property (nonatomic ,copy) NSString *company_payday;
@property (nonatomic, copy) NSString *company_latitude;
@property (nonatomic, copy) NSString *company_longitude;
@property (nonatomic ,copy) NSString *business_type;
@property (nonatomic, strong) NSArray<YKWorkEnterTimeEntity *> *company_period_list;
@property (nonatomic, strong) NSArray<YKworktypeEntity *>  *company_worktype_list;
@property (nonatomic, strong) NSArray<YKIndustryTypeEntity *> *business_type_all;
@property(nonatomic ,strong) NSArray *company_payday_list;
 @end

@interface YKWorkInfoUpEntity : NSObject

//上班类型
@property (nonatomic, retain) NSString *company_worktype;
//公司名
@property (nonatomic, retain) NSString *company_name;
//行业性质
@property (nonatomic ,copy)   NSString *business_type;//
//公司地址（选择）
@property (nonatomic, retain) NSString *company_address_distinct;
//公司地址（输入）
@property (nonatomic, retain) NSString *company_address;
//公司电话
@property (nonatomic, retain) NSString *company_phone;
//工作时长
@property (nonatomic, retain) NSString *company_period;
//发薪日
@property (nonatomic, retain) NSString *company_payday;
//经度
@property (nonatomic, retain) NSString *longitude;
//纬度
@property (nonatomic, retain) NSString *latitude;

@end
