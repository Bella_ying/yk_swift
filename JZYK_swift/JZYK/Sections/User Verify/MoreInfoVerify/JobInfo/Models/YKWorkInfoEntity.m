//
//  YKWorkInfoEntity.m
//  KDFDApp
//
//  Created by 吴春艳 on 18/6/18.
//  Copyright © 2018年 kiran. All rights reserved.
//

#import "YKWorkInfoEntity.h"

@implementation YKIndustryTypeEntity

@end

@implementation YKWorkEnterTimeEntity

@end

@implementation YKworktypeEntity

@end

@implementation YKWorkInfoEntity

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"company_period_list":[YKWorkEnterTimeEntity class],
             @"company_worktype_list":[YKworktypeEntity class],
             @"business_type_all":[YKIndustryTypeEntity class],
             };
}

@end

@implementation YKWorkInfoUpEntity

@end

