//
//  YKUpLoadWorkVC.m
//  KDIOSApp
//
//  Created by 吴春艳 on 18/6/18.
//  Copyright © 2018年 Kiran. All rights reserved.
//

#import "YKUpLoadWorkVC.h"
#import "YKUpLoadWorkEntity.h"
#import "YKUpLoadImgCollectionView.h"

static NSString * const kCardID = @"身份证";
static NSString * const kEdcationCertificate = @"学历证明";
static NSString * const kWorkCertificate = @"工作证明";
static NSString * const kIncomeCertificate = @"收入证明";
static NSString * const kPropertyCertificate = @"财产证明";
static NSString * const kWorkCardPhoto = @"工作证件照";
static NSString * const kPersonCard = @"个人名片";
static NSString * const kCeditCardInfo = @"信用卡信息";
static NSString * const kOtherCertificate = @"其它证明";
static NSString * const kSamplePhotos = @"样照";

@interface YKUpLoadWorkVC ()

@property (nonatomic, retain) UILabel *descLabel;
@property (nonatomic, retain) YKUpLoadImgCollectionView *uploadImg;
@property (assign, nonatomic) uploadImgType type;
@property (nonatomic, retain) YKUpLoadWorkEntity *pageEntity;

@end

@implementation YKUpLoadWorkVC

- (instancetype)initWithType:(uploadImgType)type
{
    self = [super init];
    if (self) {
        self.type = type;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [YKTools yk_permissionsWithCamera];
    
    UIView * descBgV = [UIView yk_viewWithColor:[UIColor whiteColor] superView:self.view masonrySet:^(UIView *view, MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.offset(1);
        make.height.mas_equalTo(80);
    }];
    self.descLabel = [UILabel yk_labelWithFontSize:13 textColor:Color.color_66_C3 superView:descBgV masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.left.offset(25);
        make.right.offset(-25);
        make.top.bottom.offset(0);
        label.numberOfLines = 0;
    }];
    
    _uploadImg = [YKUpLoadImgCollectionView getCollection];
    [self.view addSubview:_uploadImg];
    [_uploadImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(descBgV.mas_bottom);
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
}

- (void)updateView
{
    self.descLabel.text = self.pageEntity.notice;
    _uploadImg.dataArray = self.pageEntity.data;
    _uploadImg.maxNum = [self.pageEntity.max_pictures integerValue];
    _uploadImg.type = [self.pageEntity.type integerValue];
    [_uploadImg reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.view.backgroundColor = Color.whiteColor;
    
    _uploadImg.uploadImgSuccss = [self.uploadImgSuccss copy];
    //	1:身份证,2:学历证明,3:工作证明,4:收入证明,5:财产证明,6、工牌照片、7、个人名片，8、银行卡 100:其它证明
    NSString *navTitle = @"";
    switch (_type) {
        case IdCard:
            navTitle = kCardID;
            break;
        case Certificate:
            navTitle = kEdcationCertificate;
            break;
        case Work:
            navTitle = kWorkCertificate;
            break;
        case Salary:
            navTitle = kIncomeCertificate;
            break;
        case Property:
            navTitle = kPropertyCertificate;
            break;
        case workCard:
            navTitle = kWorkCardPhoto;
            break;
        case personCard:
            navTitle = kPersonCard;
            break;
        case bankCard:
            navTitle = kCeditCardInfo;
            break;
        default:
            navTitle = kOtherCertificate;
            break;
    }
    self.navigationItem.title = navTitle;
    
    if (!self.pageEntity) {
        [self getPageData];
    }
}

- (void)getPageData
{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kPictureGetPicList showLoading:YES param:@{@"type":@(_type)} succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        STRONG_SELF
        if (code == 0 && success && [json[@"item"] isKindOfClass:[NSDictionary class]]) {
            strongSelf.pageEntity = [YKUpLoadWorkEntity yy_modelWithJSON:json[@"item"]];
            [strongSelf updateView];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}


@end
