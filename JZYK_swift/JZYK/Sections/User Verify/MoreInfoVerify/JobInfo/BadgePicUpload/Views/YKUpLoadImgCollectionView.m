//
//  YKUpLoadImgCollectionView.m
//  KDIOSApp
//
//  Created by 吴春艳 on 18/6/18.
//  Copyright © 2018年 Kiran. All rights reserved.
//

#import "YKUpLoadImgCollectionView.h"
#import "ELCImagePickerController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "YKUpLoadWorkVC.h"
#import "YLProgressBar.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "YKAccessAlertView.h"

@interface YKUpLoadImgCollectionView ()<UIActionSheetDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, ELCImagePickerControllerDelegate>

@property (nonatomic, retain) NSMutableArray *pageDataArray;
@property (nonatomic, retain) NSMutableArray *uploadIndexPathArray;
@property (nonatomic, retain) UIImageView *bigImgView;
@property (assign, nonatomic) CGRect lastSelectedImgFrame;

@end

@implementation YKUpLoadImgCollectionView

+ (YKUpLoadImgCollectionView *)getCollection
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(80 * ASPECT_RATIO_WIDTH,80 * ASPECT_RATIO_WIDTH);
    layout.sectionInset = UIEdgeInsetsMake(0, 24, 0, 24);
    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = 15;
    layout.footerReferenceSize = CGSizeMake(WIDTH_OF_SCREEN, 94);
    
    YKUpLoadImgCollectionView *collectionView = [[YKUpLoadImgCollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
    collectionView.delegate = collectionView;
    collectionView.dataSource = collectionView;
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.showsVerticalScrollIndicator = NO;
    collectionView.backgroundColor = [UIColor whiteColor];
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"GradientCell"];
    [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
    
    return collectionView;
}

- (void)setDataArray:(NSArray<YKUpLoadImgEntity *> *)dataArray
{
    _dataArray = dataArray;
    if (!_pageDataArray) {
        _pageDataArray = [@[] mutableCopy];
    }
    [_pageDataArray removeAllObjects];
    [_pageDataArray addObjectsFromArray:dataArray];
    
    if (!_uploadArray) {
        _uploadArray = [@[] mutableCopy];
    }
    
    if (!_uploadIndexPathArray) {
        _uploadIndexPathArray = [@[] mutableCopy];
    }
}

- (void)uploadImages
{
    if (self.uploadArray.count == 0) {
        [[iToast makeText:@"请选择照片"] show];
        return;
    }
    
    NSIndexPath *indexPath = [self.uploadIndexPathArray objectAtIndex:0];
    UICollectionViewCell * cell;
    UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForItemAtIndexPath:indexPath];
    if (CGRectIntersectsRect(self.bounds, attributes.frame)) {
        cell = [self cellForItemAtIndexPath:indexPath];
    }
    UIButton *deleteBtn = [cell.contentView viewWithTag:1003];
    deleteBtn.hidden = YES;
    YLProgressBar *progressBarRoundedFat = (YLProgressBar *)[cell.contentView viewWithTag:1002];
    progressBarRoundedFat.hidden = NO;
    
    NSData *imgData = UIImageJPEGRepresentation([self.uploadArray firstObject], 0.5);
    WEAK_SELF
    [[HTTPManager session] uploadImageWithData:imgData serverUrlKey:kPictureUploadImage key:@"attach" fileName:@"imageFile.jpg" parameters:@{@"type":[NSString stringWithFormat:@"%ld",(long)self.type]} uploadSuccess:^(NSDictionary<NSString *,id> * _Nonnull json) {
        STRONG_SELF
        if (strongSelf.uploadArray.count > 0 && weakSelf.uploadIndexPathArray.count > 0) {
            [strongSelf.uploadArray removeObjectAtIndex:0];
            [strongSelf.uploadIndexPathArray removeObjectAtIndex:0];
        }
        
        if (strongSelf.uploadArray.count > 0) {
            [strongSelf uploadImages];
        } else {
            [(YKUpLoadWorkVC *)strongSelf.yk_viewController getPageData];
            if (weakSelf.uploadImgSuccss) {
                weakSelf.uploadImgSuccss();
            }
        }
        
    } uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
        if (cell) {
            [progressBarRoundedFat setProgress:uploadProgress.fractionCompleted animated:YES];
        }
    } uploadFailure:^(NSString * _Nonnull msg) {
        progressBarRoundedFat.hidden = YES;
        deleteBtn.hidden = NO;
        [[iToast makeText:@"当前网络断开"] show];
    }];
}

- (void)deleteImg:(UIButton *)btn
{
    UIImageView *imgView = [btn.superview viewWithTag:1001];
    if (self.pageDataArray.count>0&&self.uploadArray.count>0) {
        [self.pageDataArray removeObject:imgView.image];
        [self.uploadArray removeObject:imgView.image];
        [self reloadData];
    }
}

//显示大图
- (void)showBigImgWithIndex:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForItemAtIndexPath:indexPath];
    _lastSelectedImgFrame = [self convertRect:attributes.frame toView:KEY_WINDOW];
    if (!_bigImgView) {
        _bigImgView = [[UIImageView alloc] init];
        [KEY_WINDOW addSubview:_bigImgView];
        _bigImgView.hidden = YES;
        _bigImgView.backgroundColor = [UIColor blackColor];
        _bigImgView.userInteractionEnabled = YES;
         _bigImgView.contentMode = UIViewContentModeScaleAspectFit;
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideImg)];
        [_bigImgView addGestureRecognizer:gesture];
    }
    _bigImgView.frame = _lastSelectedImgFrame;
    id imageData = self.pageDataArray[indexPath.row];
    if ([self.dataArray indexOfObject:[self.pageDataArray objectAtIndex:indexPath.row]] <= self.dataArray.count) {
        [_bigImgView sd_setImageWithURL:[NSURL URLWithString:[(YKUpLoadImgEntity *)imageData url]]];
    } else {
        _bigImgView.image = imageData;
    }
    [self showImg];
}

- (void)showImg
{
    WEAK_SELF;
    _bigImgView.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.bigImgView.frame = KEY_WINDOW.bounds;
    }];
}

- (void)hideImg
{
    WEAK_SELF;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.bigImgView.alpha = 0.0f;
        weakSelf.bigImgView.frame = weakSelf.lastSelectedImgFrame;
    } completion:^(BOOL finished) {
        if (finished) {
            weakSelf.bigImgView.hidden = YES;
            weakSelf.bigImgView.alpha = 1.0f;
        }
    }];
}

#pragma mark collectionDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.pageDataArray.count) {
        
        if ([YKTools yk_permissionsWithCamera] == YKPermissionStatusNotDenied) {
            [[QLAlert alert] prepareUIWithContentText:@"请在设置-->隐私-->相机-->桔子优卡中打开相机使用权限" buttonTitleArray:@[@"确定"] buttonClicked:^(NSInteger index) {
            }];
            return;
        }
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", nil];
        [actionSheet setActionSheetStyle:UIActionSheetStyleDefault];
        [actionSheet showInView:self];
    } else {
        [self showBigImgWithIndex:indexPath];
    }
}

#pragma mark -- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (_pageDataArray.count < self.maxNum) {
        return _pageDataArray.count + 1;
    }
    return self.maxNum;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"GradientCell";
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (![cell.contentView viewWithTag:1001]) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80 * ASPECT_RATIO_WIDTH, 80 * ASPECT_RATIO_WIDTH)];
        imageView.tag = 1001;
        imageView.layer.cornerRadius = 3.0;
        imageView.layer.masksToBounds = YES;
        [cell.contentView addSubview:imageView];
        
        YLProgressBar *progressBarRoundedFat = [[YLProgressBar alloc] initWithFrame:CGRectMake(0, 80 * ASPECT_RATIO_WIDTH - 8, 80 * ASPECT_RATIO_WIDTH, 8)];
        NSArray *tintColors = @[Color.main];
        progressBarRoundedFat.tag = 1002;
        progressBarRoundedFat.progressTintColors       = tintColors;
        progressBarRoundedFat.stripesOrientation       = YLProgressBarStripesOrientationLeft;
        progressBarRoundedFat.indicatorTextDisplayMode = YLProgressBarIndicatorTextDisplayModeProgress;
        progressBarRoundedFat.indicatorTextLabel.font  = [UIFont systemFontOfSize:13];
        progressBarRoundedFat.progressStretch          = NO;
        progressBarRoundedFat.backgroundColor = [UIColor whiteColor];
        progressBarRoundedFat.alpha = 0.6f;
        progressBarRoundedFat.hidden = YES;
        [cell.contentView addSubview:progressBarRoundedFat];
        
        [UIButton yk_buttonWithImageName:@"deletePic" superView:cell.contentView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(20, 20));
            button.tag = 1003;
            [button addTarget:self action:@selector(deleteImg:) forControlEvents:UIControlEventTouchUpInside];
            button.contentMode = UIViewContentModeCenter;
        }];
        
        [UILabel yk_labelWithFontSize:13 textColor:Color.whiteColor superView:cell.contentView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
            make.height.mas_equalTo(24.f);
            label.tag = 1004;
            label.text = @"已上传";
            label.textAlignment = NSTextAlignmentCenter;
            label.backgroundColor = [UIColor yk_colorWithHexString:@"#000000" alpha:.3f];
        }];
    }
    UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:1001];
    YLProgressBar *progressBarRoundedFat = (YLProgressBar *)[cell.contentView viewWithTag:1002];
    UIButton *btn = [cell.contentView viewWithTag:1003];
    UILabel *label = [cell.contentView viewWithTag:1004];

    if (indexPath.row == self.pageDataArray.count) { //拍照的+占位处，
        imageView.image = [UIImage imageNamed:@"addPic"];
        btn.hidden = YES;
        label.hidden = YES;
        progressBarRoundedFat.hidden = YES;
    } else {
        id imageData = self.pageDataArray[indexPath.row];
        if ([self.dataArray indexOfObject:imageData] <= self.dataArray.count) {
            //已上传过的图片
            [imageView sd_setImageWithURL:[NSURL URLWithString:[(YKUpLoadImgEntity *)imageData url]]];
            btn.hidden = YES;
            label.hidden = NO;
            progressBarRoundedFat.hidden = YES;
        } else {
            //未上传的图片
            imageView.image = imageData;
            btn.hidden = NO;
            label.hidden = YES;
            progressBarRoundedFat.hidden = YES;
            if (![_uploadIndexPathArray containsObject:indexPath]) {
                [_uploadIndexPathArray addObject:indexPath];
            }
        }
    }
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionFooter){
       reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        if (![reusableview viewWithTag:2001]) {
            UIView *collectionFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 94)];
            collectionFooterView.tag = 2001;
            [reusableview addSubview:collectionFooterView];
            
            [UIButton yk_buttonFontSize:17 textColor:Color.whiteColor backGroundImage:@"btn_bg_image" imageName:@"" cornerRadius:23 superView:collectionFooterView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.left.offset(34);
                make.right.offset(-34);
                make.height.offset(46);
                [button setTitle:@"上传" forState:UIControlStateNormal];
                [button addTarget:self action:@selector(uploadImages) forControlEvents:UIControlEventTouchUpInside];
            }];
        }
    }
    return reusableview;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.maxNum - self.pageDataArray.count <= 0) {
        [self performSelector:@selector(showAlert:) withObject:@"图片数量已达上限" afterDelay:0.5f];
        return;
    }
    if (buttonIndex != 0 && buttonIndex != 1) {
        return;
    }
    if (buttonIndex == 0) {
        [self showCamera];
    }
//    } else {
//        [self showPhotoAlbum];
//    }
}

- (void)showCamera
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [self performSelector:@selector(showAlert:) withObject:@"相机打开失败" afterDelay:0.5f];
        return;
    }
    
    UIImagePickerController *imageVC = [[UIImagePickerController alloc] init];
    imageVC.allowsEditing = NO;
    imageVC.delegate = self;
    imageVC.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self.yk_viewController presentViewController:imageVC animated:YES completion:nil];
}

- (void)showPhotoAlbum
{
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] init];
    elcPicker.maximumImagesCount = self.maxNum - self.pageDataArray.count;
    elcPicker.imagePickerDelegate = self;
    [self.yk_viewController presentViewController:elcPicker animated:YES completion:nil];
}

#pragma mark camare
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.yk_viewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    WEAK_SELF;
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.uploadArray addObject:image];
    [self.yk_viewController dismissViewControllerAnimated:YES completion:^{
        [weakSelf.pageDataArray removeAllObjects];
        [weakSelf.pageDataArray addObjectsFromArray:weakSelf.dataArray];
        [weakSelf.pageDataArray addObjectsFromArray:weakSelf.uploadArray];
        [weakSelf reloadData];
    }];
}

#pragma mark ELCImagePickerControllerDelegate
- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    WEAK_SELF;
    [info enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                [weakSelf.uploadArray addObject:image];
                [weakSelf.uploadIndexPathArray removeAllObjects];
            }
        }
    }];
    
    [self.yk_viewController dismissViewControllerAnimated:YES completion:^{
        [weakSelf.pageDataArray removeAllObjects];
        [weakSelf.pageDataArray addObjectsFromArray:weakSelf.dataArray];
        [weakSelf.pageDataArray addObjectsFromArray:weakSelf.uploadArray];
        [weakSelf reloadData];
    }];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self.yk_viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)showAlert:(NSString *)alertText
{
    [[QLAlert alert] showWithMessage:alertText singleBtnTitle:@"确定"];
}

@end
