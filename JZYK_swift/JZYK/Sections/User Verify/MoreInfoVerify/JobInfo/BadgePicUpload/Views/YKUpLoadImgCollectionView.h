//
//  YKUpLoadImgCollectionView.h
//  KDIOSApp
//
//  Created by 吴春艳 on 18/6/18.
//  Copyright © 2018年 Kiran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKUpLoadWorkEntity.h"

@interface YKUpLoadImgCollectionView : UICollectionView

@property (nonatomic, retain) NSArray<YKUpLoadImgEntity *> *dataArray;
@property (assign, nonatomic) NSInteger maxNum;

//待上传的图片
@property (nonatomic, retain) NSMutableArray *uploadArray;

@property (assign, nonatomic) NSInteger type;

+ (YKUpLoadImgCollectionView *)getCollection;

@property (nonatomic, copy) void (^uploadImgSuccss)(void);
@end
