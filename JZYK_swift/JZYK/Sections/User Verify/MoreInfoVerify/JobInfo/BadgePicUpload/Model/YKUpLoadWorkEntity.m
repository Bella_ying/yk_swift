//
//  YKUpLoadWorkEntity.m
//  KDIOSApp
//
//  Created by 吴春艳 on 18/6/18.
//  Copyright © 2018年 Kiran. All rights reserved.
//

#import "YKUpLoadWorkEntity.h"

@implementation YKUpLoadImgEntity

+(NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"imgId":@"id"};
}
@end

@implementation YKUpLoadWorkEntity

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"data":[YKUpLoadImgEntity class]};
}

@end
