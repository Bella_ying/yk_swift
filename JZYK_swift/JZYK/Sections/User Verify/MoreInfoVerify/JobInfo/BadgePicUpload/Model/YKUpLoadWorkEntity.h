//
//  YKUpLoadWorkEntity.h
//  KDIOSApp
//
//  Created by 吴春艳 on 18/6/18.
//  Copyright © 2018年 Kiran. All rights reserved.
//

@interface YKUpLoadImgEntity : NSObject

@property (nonatomic, copy) NSString *imgId;
@property (nonatomic, copy) NSString *pic_name;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *url;

@end

@interface YKUpLoadWorkEntity : NSObject

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *notice;
@property (nonatomic, copy) NSString *max_pictures;
@property (nonatomic, copy) NSArray<YKUpLoadImgEntity *> *data;

@end
