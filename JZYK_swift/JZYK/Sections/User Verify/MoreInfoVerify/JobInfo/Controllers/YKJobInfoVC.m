//
//  YKJobInfoVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKJobInfoVC.h"
#import "YKWorkInfoView.h"
#import "YKLocationManager.h"
#import "YKAccessAlertView.h"

@interface YKJobInfoVC ()

@property (nonatomic, retain) YKWorkInfoView *workInfoView;
@property (nonatomic, retain) YKWorkInfoEntity *pageEntity;
@property (nonatomic, retain) CLLocation *location;

@end

@implementation YKJobInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.workInfoView = [YKWorkInfoView new];
    WEAK_SELF
    self.workInfoView.SaveBtnClickBlock = ^{
        STRONG_SELF
        [strongSelf saveData];
    };
    [self.view addSubview:self.workInfoView];
    [self.workInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.offset(0);
        make.height.mas_equalTo(HEIGHT_OF_SCREEN - NAV_HEIGHT- BOTTOM_HEIGHT + 1);
    }];
    [self obtainlocation:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"工作详细信息";
    self.view.backgroundColor = Color.backgroundColor;
    
    if (!self.pageEntity) {
        [self loadData];
    }
}

- (void)loadData
{
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kCreditCardGetWorkInfo showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        STRONG_SELF
        if (code == 0 && success && json[@"item"]) {
            strongSelf.pageEntity = [YKWorkInfoEntity yy_modelWithJSON:json[@"item"]];
            [strongSelf.workInfoView updateUIWithData:strongSelf.pageEntity];
        }
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];

}

- (BOOL)inputCompleteAceess
{
    YKWorkInfoUpEntity *uploadEntity = self.workInfoView.uploadEntity;
    if ([uploadEntity.company_worktype intValue] <= 0) {
        [[iToast makeText:@"请选择工作类型"] show];
        return NO;
    }
        if ([uploadEntity.business_type intValue] <= 0) {
            [[iToast makeText:@"请选择所属行业"] show];
            return NO;
        }
        if (uploadEntity.company_name.length <= 0) {
            [[iToast makeText:@"请输入单位名称"] show];
            return NO;
        }
        if (uploadEntity.company_phone.length <= 0) {
            [[iToast makeText:@"请输入单位电话"] show];
            return NO;
        }
        if (uploadEntity.company_address.length <= 0) {
            [[iToast makeText:@"请输入单位地址"] show];
            return NO;
        }
    return YES;
}

- (void)saveData
{
    YKWorkInfoUpEntity *uploadEntity = self.workInfoView.uploadEntity;
    NSDictionary * pamar;
    if (uploadEntity.company_worktype.integerValue != 2) {
        if([self inputCompleteAceess] == NO) return;
        if (self.location) {
            pamar = @{@"business_type": uploadEntity.business_type ? uploadEntity.business_type : @"",
                                         @"company_name": uploadEntity.company_name ? uploadEntity.company_name : @"",
                                         @"company_address_distinct":uploadEntity.company_address_distinct ? uploadEntity.company_address_distinct : @"",
                                         @"company_address":uploadEntity.company_address ? uploadEntity.company_address : @"",
                                         @"company_phone":uploadEntity.company_phone ? uploadEntity.company_phone : @"",
                                         @"company_period":uploadEntity.company_period ? uploadEntity.company_period : @"",
                                         @"longitude":[NSNumber numberWithFloat:self.location.coordinate.longitude],
                                         @"latitude":[NSNumber numberWithFloat:self.location.coordinate.latitude],
                                         @"company_worktype":uploadEntity.company_worktype ? uploadEntity.company_worktype : @"",
                                         @"company_payday" :uploadEntity.company_payday ? uploadEntity.company_payday : @""
                                         };
        }else{
            [self obtainlocation:YES];
            return;
        }
    }else{
        pamar = @{@"company_industrytype":@"",
                                     @"company_name":@"",
                                     @"company_address_distinct":@"",
                                     @"company_address":@"",
                                     @"company_phone":@"",
                                     @"company_period":@"",
                                     @"longitude":@"",
                                     @"latitude":@"",
                                     @"company_worktype":uploadEntity.company_worktype ? uploadEntity.company_worktype : @"",
                                     @"company_payday" : @""
                                     };
    }
    WEAK_SELF
     [[HTTPManager session] postRequestForKey:kCreditCardSaveWorkInfo showLoading:YES param:pamar succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        STRONG_SELF
        if (code == 0) {
            [[iToast makeText:msg] show];
            [strongSelf.navigationController popViewControllerAnimated:YES];
        } else {
            [[iToast makeText:msg] show];
        }
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

#pragma mark - 获取用户位置信息
-(void)obtainlocation:(BOOL)isTip
{
    if ([YKTools yk_permissionsWithLocation] == YKPermissionStatusNotDenied) {
        if (isTip) {
            [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeLocation contentText:@""];
        }
    } else {
        WEAK_SELF
        if (isTip) {
            [[iToast makeText:@"定位获取中"] show];
        }
        [[YKLocationManager location] yk_enableLocationServiceWithCompletion:^(CLLocation * location, AMapLocationReGeocode *regeoCode) {
            STRONG_SELF
            strongSelf.location = location;
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
