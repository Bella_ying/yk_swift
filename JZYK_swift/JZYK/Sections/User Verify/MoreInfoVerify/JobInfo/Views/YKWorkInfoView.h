//
//  YKWorkInfoView.h
//  KDFDApp
//
//  Created by 吴春艳 on 2018/6/18.
//  Copyright © 2018年 kiran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "YKWorkInfoEntity.h"

@interface YKWorkInfoView : TPKeyboardAvoidingScrollView

//需要保存的参数
@property (nonatomic, retain) YKWorkInfoUpEntity *uploadEntity;
@property (nonatomic, copy) void(^SaveBtnClickBlock)(void) ;

/**
 更新UI
 @param entity 页面数据
 */
- (void)updateUIWithData:(YKWorkInfoEntity *)entity;

@end
