//
//  YKWorkInfoView.m
//  KDFDApp
//
//  Created by 吴春艳 on 18/6/18.
//  Copyright © 2018年 kiran. All rights reserved.
//

#import "YKWorkInfoView.h"
#import "YKRealNameDataManager.h"
#import "IQActionSheetPickerView.h"
//#import "UIScrollView+PersonMessage.h"
#import "JWPickerView.h"
#import "YKUpLoadWorkVC.h"
#import "UIView+ViewController.h"
#import "YKAccessAlertView.h"
#import "UIView+JobMessage.h"

@interface YKWorkInfoView ()<IQActionSheetPickerViewDelegate>

//工作类型
@property (nonatomic, strong) UIView *contentBgV;
//工作类型
@property (nonatomic, strong) UIView *workTypeView;
//所属类型
@property (nonatomic, strong) UIView *workBelongView;
//单位名称
@property (nonatomic, strong) UITextField *workPlaceTf;
//单位地址
@property (nonatomic, strong) UIView *workAddressView;
//单位地址详细地址
@property (nonatomic, strong) UITextField *workAddressTf;
//单位电话输入框
@property (nonatomic, strong) UITextField *workPhoneTf;
//工作照片
@property (nonatomic, strong) UIView *workPhotoView;
//工作时长
@property (nonatomic, strong) UIView *workTimeView;
//发薪日期cell
@property (nonatomic, strong) UIView *payDateCell;
//发薪日期
@property (nonatomic, strong) UIView *payDateView;
//保存按钮
@property (nonatomic, strong) UIButton *commitBtn;
//页面数据
@property (nonatomic, strong) YKWorkInfoEntity *pageEntity;

//城市选择器
@property (nonatomic, strong) IQActionSheetPickerView *picker;

@property (nonatomic, strong) NSMutableArray *workTypeArray;
@property (nonatomic, strong) NSMutableArray *workBelongTimeArray;
@property (nonatomic, strong) NSMutableArray *workTimeArray;

@end

@implementation YKWorkInfoView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = Color.backgroundColor;
        [self configUI];
    }
    return self;
}

- (void)updateUIWithData:(YKWorkInfoEntity *)entity
{
    self.pageEntity = entity;

    //工作类型
    if ([self.pageEntity.company_worktype intValue] > 0) {
        YKworktypeEntity *workTypeEntity = (YKworktypeEntity *)[YKRealNameDataManager getEntityFromArray:self.pageEntity.company_worktype_list key:@"work_type" value:self.pageEntity.company_worktype];
        UILabel * lab = (UILabel *)[self.workTypeView viewWithTag:10002];
        lab.textColor = Color.color_45_C2;
        [lab setText:workTypeEntity.name];
    }

    //所属类型
    if ([self.pageEntity.business_type intValue] > 0) {
        YKIndustryTypeEntity *industryTypeEntity = (YKIndustryTypeEntity *)[YKRealNameDataManager getEntityFromArray:self.pageEntity.business_type_all key:@"business_type" value:self.pageEntity.business_type];
        UILabel * lab = (UILabel *)[self.workBelongView viewWithTag:10002];
        lab.textColor = Color.color_45_C2;
        [lab setText:industryTypeEntity.name];
    }

    //根据工作类型判断是否显示工作类型下面的UI
    WEAK_SELF
    if (self.pageEntity.company_worktype.integerValue == 2) {
        self.contentBgV.hidden = YES;
        [self.commitBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(34);
            make.right.offset(-34);
            make.top.equalTo(weakSelf.workTypeView.mas_bottom).offset(61);
            make.height.mas_equalTo(58.0);
            make.bottom.offset(-20);
        }];
    } else {
        self.contentBgV.hidden = NO;
        [self.commitBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(34);
            make.right.offset(-34);
            make.top.equalTo(weakSelf.contentBgV.mas_bottom).offset(61);
            make.height.mas_equalTo(58.0);
            make.bottom.offset(-20);
        }];
    }

    //单位名称
    self.workPlaceTf.text = [self.pageEntity.company_name isEqualToString:@""] ? self.workPlaceTf.text : self.pageEntity.company_name;

    //单位地址
    if (self.pageEntity.company_address_distinct && ![self.pageEntity.company_address_distinct isEqualToString:@""]) {
        UILabel * lab = (UILabel *)[self.workAddressView viewWithTag:10002];
        lab.textColor = Color.color_45_C2;
        [lab setText:self.pageEntity.company_address_distinct];
    }

    //单位地址详细地址
    self.workAddressTf.text = [self.pageEntity.company_address isEqualToString:@""] ? self.workAddressTf.text : self.pageEntity.company_address;
    //单位电话
    self.workPhoneTf.text = [self.pageEntity.company_phone isEqualToString:@""] ? self.workPhoneTf.text : self.pageEntity.company_phone;

    //工作照片
    UILabel * workPhotoLab = (UILabel *)[self.workPhotoView viewWithTag:10002];
    if ([self.pageEntity.company_picture integerValue] == 1) {
        workPhotoLab.text = @"已上传";
        workPhotoLab.textColor = Color.color_45_C2;
    } else {
        workPhotoLab.text = @"未上传";
    }

    //工作时长
    if ([self.pageEntity.company_period intValue] > 0) {
        YKWorkEnterTimeEntity *workTypeEntity = (YKWorkEnterTimeEntity *)[YKRealNameDataManager getEntityFromArray:self.pageEntity.company_period_list key:@"entry_time_type" value:self.pageEntity.company_period];
        UILabel * workLab = (UILabel *)[self.workTimeView viewWithTag:10002];
        workLab.text = workTypeEntity.name;
        workLab.textColor = Color.color_45_C2;
    }

    //发薪日期
    if ([self.pageEntity.company_payday intValue] > 0) {
        UILabel * payDateLab = (UILabel *)[self.payDateView viewWithTag:10002];
        payDateLab.text = self.pageEntity.company_payday;
        payDateLab.textColor = Color.color_45_C2;;
    }
}

- (void)configUI
{
    /***头部说明***/
    UILabel *topDescLabel = [UILabel yk_labelWithFontSize:13 textColor:Color.main superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {
   
        make.right.top.offset(0);
        make.left.offset(15);
        make.height.offset(34);
        
        label.text = @"为保证借款申请顺利通过，请务必填写真实信息";
    }];
    
    /***工作类型***/
    UIView *workTypeCell = [self configCellWithLeftLabelText:@"工作类型" height:45 topView:topDescLabel];
    self.workTypeView = [self configRightBtnWithTitle:@"请选择" superView:workTypeCell tapSelector:@selector(workTypeSelect) haveArrow:YES];
    
    UIView * contentBgV = [UIView new];
    [self addSubview:contentBgV];
    [contentBgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(workTypeCell.mas_bottom);
        make.left.right.offset(0);
        make.width.offset(WIDTH_OF_SCREEN);
    }];
    self.contentBgV = contentBgV;
    
    /***所属类型***/
    UIView *workBelongCell = [contentBgV configCellWithLeftLabelText:@"所属行业" height:45 topView:nil];
    self.workBelongView = [self configRightBtnWithTitle:@"请选择" superView:workBelongCell tapSelector:@selector(workBelongSelect) haveArrow:YES];
    
    //空格view
    UIView * sepView = [UIView yk_viewWithColor:Color.backgroundColor superView:contentBgV masonrySet:^(UIView *view, MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(workBelongCell.mas_bottom);
        make.height.mas_equalTo(8);
    }];
    
    /***单位名称***/
    UIView *workPlaceView = [contentBgV configCellWithLeftLabelText:@"单位名称" height:45 topView:sepView];
    self.workPlaceTf = [UITextField yk_textFieldWithFontSize:13 textColor:Color.color_45_C2 placeHolder:@"请输入单位名称" superView:workPlaceView masonrySet:^(UITextField *textfield, MASConstraintMaker *make) {
        make.top.bottom.offset(1);//给0会挡住底下的线，所以给1
        make.right.offset(-15);
        make.left.equalTo([workPlaceView viewWithTag:20001].mas_right);
        
        textfield.textAlignment = NSTextAlignmentRight;
        [textfield setValue:Color.color_CC_C8 forKeyPath:@"_placeholderLabel.textColor"];
    }];
    [self.workPlaceTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    /***单位地址***/
    UIView *workAddressDistinctView = [contentBgV configCellWithLeftLabelText:@"单位地址" height:45 topView:workPlaceView];
    self.workAddressView = [self configRightBtnWithTitle:@"请选择单位地址" superView:workAddressDistinctView tapSelector:@selector(workAddressSelect) haveArrow:YES];
    
    /***单位地址详细地址***/
    UIView *workAddressView = [contentBgV configCellWithLeftLabelText:@"" height:45 topView:workAddressDistinctView];
    self.workAddressTf = [UITextField yk_textFieldWithFontSize:13 textColor:Color.color_45_C2 placeHolder:@"详细街道和门牌号" superView:workAddressView masonrySet:^(UITextField *view, MASConstraintMaker *make) {
        make.top.bottom.offset(1);//给0会挡住底下的线，所以给1
        make.right.offset(-15);
        make.left.equalTo([workAddressView viewWithTag:20001].mas_right);
        view.textAlignment = NSTextAlignmentRight;
        [view setValue:Color.color_CC_C8 forKeyPath:@"_placeholderLabel.textColor"];
    }];
    [self.workAddressTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    /***单位电话***/
    UIView *workPhoneView = [contentBgV configCellWithLeftLabelText:@"单位电话" height:45 topView:workAddressView];
    self.workPhoneTf = [UITextField yk_textFieldWithFontSize:13 textColor:Color.color_45_C2 placeHolder:@"请输入单位电话" superView:workPhoneView masonrySet:^(UITextField *view, MASConstraintMaker *make) {
        make.top.bottom.offset(1);//给0会挡住底下的线，所以给1
        make.right.offset(-15);
        make.left.equalTo([workPhoneView viewWithTag:20001].mas_right);
        view.keyboardType = UIKeyboardTypeNumberPad;
        view.textAlignment = NSTextAlignmentRight;
        [view setValue:Color.color_CC_C8 forKeyPath:@"_placeholderLabel.textColor"];
    }];
    [self.workPhoneTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    //空格view
    UIView * minView = [UIView yk_viewWithColor:Color.backgroundColor superView:contentBgV masonrySet:^(UIView *view, MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(workPhoneView.mas_bottom);
        make.height.mas_equalTo(29);
        
        [UILabel yk_labelWithFontSize:13 textColor:Color.color_AD_C5 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            make.left.offset(15);
            make.top.bottom.offset(0);
            label.text = @"选填（可提高通过率）";
        }];
    }];
    
    /***工作照片***/
    UIView *workPhotoView = [contentBgV configCellWithLeftLabelText:@"工作证件照" height:45 topView:minView];
    self.workPhotoView = [self configRightBtnWithTitle:@"请上传" superView:workPhotoView tapSelector:@selector(toUploadPage) haveArrow:YES];
    
    /***工作时长***/
    UIView *workTimeView = [contentBgV configCellWithLeftLabelText:@"工作时长" height:45 topView:workPhotoView];
    self.workTimeView = [self configRightBtnWithTitle:@"请选择" superView:workTimeView tapSelector:@selector(selectWorkTime) haveArrow:YES];
    
    /***发薪日期***/
    UIView * payDateCell = [contentBgV configCellWithLeftLabelText:@"发薪日期" height:45 topView:workTimeView];
    self.payDateView = [self configRightBtnWithTitle:@"请选择" superView:payDateCell tapSelector:@selector(selectPayDate) haveArrow:YES];
    
    /***保存***/
    self.commitBtn = [UIButton yk_buttonFontSize:adaptFontSize(17) textColor:Color.whiteColor backGroundImage:@"btn_bg_image" imageName:@"" cornerRadius:23 superView:self masonrySet:^(UIButton *button, MASConstraintMaker *make) {

        make.left.offset(34);
        make.right.offset(-34);
        make.top.equalTo(contentBgV.mas_bottom).offset(61);
        make.height.mas_equalTo(58.0);
        make.bottom.offset(-20);

        [button setTitle:@"保存" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }];
}

//保存
- (void)saveBtnClick:(UIButton *)sender
{
    [KEY_WINDOW endEditing:YES];
    if (self.SaveBtnClickBlock) {
        self.SaveBtnClickBlock();
    }
}

//工作类型选择
- (void)workTypeSelect
{
    [KEY_WINDOW endEditing:YES];
    if (!self.pageEntity) return;
    self.workTypeArray = [NSMutableArray array];
    for (YKworktypeEntity *model in self.pageEntity.company_worktype_list) {
        [self.workTypeArray addObject:model.name];
    }
    WEAK_SELF
    [JWPickerView pickerWithToolBarTitle:@"" pickerType:JWPickerViewTypeSingleComponent data:self.workTypeArray cancleComplete:^{
        DLog(@"cancel");
    } confirmComplete:^(id  _Nonnull selectedIndex, id  _Nonnull selectedData) {
        STRONG_SELF
        int selIndex = [[NSString stringWithFormat:@"%@",selectedIndex] intValue];
        YKworktypeEntity * selectModel = self.pageEntity.company_worktype_list[selIndex];
        strongSelf.pageEntity.company_worktype = selectModel.work_type;
        [strongSelf updateUIWithData:self.pageEntity];
    }];
}

//工作所属行业选择
- (void)workBelongSelect
{
    [KEY_WINDOW endEditing:YES];
    if (!self.pageEntity) return;
    self.workBelongTimeArray = [NSMutableArray array];
    for (YKIndustryTypeEntity *model in self.pageEntity.business_type_all) {
        [self.workBelongTimeArray addObject:model.name];
    }
    WEAK_SELF
    [JWPickerView pickerWithToolBarTitle:@"" pickerType:JWPickerViewTypeSingleComponent data:self.workBelongTimeArray cancleComplete:^{
        DLog(@"cancel");
    } confirmComplete:^(id  _Nonnull selectedIndex, id  _Nonnull selectedData) {
        STRONG_SELF
        int selIndex = [[NSString stringWithFormat:@"%@",selectedIndex] intValue];
        YKIndustryTypeEntity * selectModel = self.pageEntity.business_type_all[selIndex];
        strongSelf.pageEntity.business_type = selectModel.business_type;
        [strongSelf updateUIWithData:self.pageEntity];
    }];
}

//单位地址选择
- (void)workAddressSelect
{
    [KEY_WINDOW endEditing:YES];
    if (!self.pageEntity) return;
    if ([YKTools yk_permissionsWithLocation] == YKPermissionStatusAuthorized) {
        if (!self.picker) {
            self.picker = [[IQActionSheetPickerView alloc] initWithTitle:@"" delegate:self];
            self.picker.toolbarButtonColor = Color.whiteColor;
            self.picker.toolbarTintColor = Color.whiteColor;
            [self.picker setTag:3];
            [self.picker setAddressMessage];
            self.picker.backgroundColor = [UIColor clearColor];
        }
        [self.picker show];
    } else {
        [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeLocation contentText:@""];
    }
}

//跳转上传照片页面
- (void)toUploadPage
{
    [KEY_WINDOW endEditing:YES];
    WEAK_SELF
    YKUpLoadWorkVC *vc = [[YKUpLoadWorkVC alloc]initWithType:workCard];
    vc.uploadImgSuccss = ^{
        STRONG_SELF
        strongSelf.pageEntity.company_picture = @"1";
        [strongSelf updateUIWithData:strongSelf.pageEntity];
    };
    [self.viewController.navigationController pushViewController:vc animated:YES];
}

//选择工作时长
- (void)selectWorkTime
{
    [KEY_WINDOW endEditing:YES];
    if (!self.pageEntity) return;
    self.workTimeArray = [NSMutableArray array];
    for (YKWorkEnterTimeEntity *model in self.pageEntity.company_period_list) {
        [self.workTimeArray addObject:model.name];
    }
    
    WEAK_SELF
    [JWPickerView pickerWithToolBarTitle:@"" pickerType:JWPickerViewTypeSingleComponent data:self.workTimeArray cancleComplete:^{
        DLog(@"cancel");
    } confirmComplete:^(id  _Nonnull selectedIndex, id  _Nonnull selectedData) {
        STRONG_SELF
        int selIndex = [[NSString stringWithFormat:@"%@",selectedIndex] intValue];
        YKWorkEnterTimeEntity * selectModel = self.pageEntity.company_period_list[selIndex];
        strongSelf.pageEntity.company_period = selectModel.entry_time_type;
        [strongSelf updateUIWithData:self.pageEntity];
    }];
   
}

//选择发薪日期
- (void)selectPayDate
{
    [KEY_WINDOW endEditing:YES];
    if (!self.pageEntity) return;
    WEAK_SELF
    [JWPickerView pickerWithToolBarTitle:@"" pickerType:JWPickerViewTypeSingleComponent data:self.pageEntity.company_payday_list cancleComplete:^{
        DLog(@"cancel");
    } confirmComplete:^(id  _Nonnull selectedIndex, id  _Nonnull selectedData) {
        STRONG_SELF
        int selIndex = [[NSString stringWithFormat:@"%@",selectedIndex] intValue];
        strongSelf.pageEntity.company_payday = self.pageEntity.company_payday_list[selIndex];;
        [strongSelf updateUIWithData:self.pageEntity];
    }];
}

//在get方法里传递参数，省事
- (YKWorkInfoUpEntity *)uploadEntity
{
    if (!_uploadEntity) {
        _uploadEntity = [YKWorkInfoUpEntity new];
    }
    _uploadEntity.business_type = self.pageEntity.business_type;
    _uploadEntity.company_worktype = self.pageEntity.company_worktype;
    _uploadEntity.company_name = self.workPlaceTf.text;
    _uploadEntity.company_address_distinct = self.pageEntity.company_address_distinct;
    _uploadEntity.company_address = self.workAddressTf.text;
    _uploadEntity.company_phone = self.workPhoneTf.text;
    _uploadEntity.company_period = self.pageEntity.company_period;
    _uploadEntity.company_payday = self.pageEntity.company_payday;
    
    return _uploadEntity;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == self.workPlaceTf || textField == self.workAddressTf) {
        [InputView limitTextWithLength:30 forTextField:textField];
    } else if (textField == self.workPhoneTf) {
        [InputView limitTextWithLength:15 forTextField:textField];
    }
}

#pragma mark IQActionSheetPickerViewDelegate
-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    NSString *city = [titles componentsJoinedByString:@" "];
    self.pageEntity.company_address_distinct = city;
    [self updateUIWithData:self.pageEntity];
}

@end
