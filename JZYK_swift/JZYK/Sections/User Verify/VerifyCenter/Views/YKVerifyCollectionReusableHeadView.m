//
//  YKVerifyCollectionReusableHeadView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKVerifyCollectionReusableHeadView.h"

@interface YKVerifyCollectionReusableHeadView ()


@end

@implementation YKVerifyCollectionReusableHeadView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = GRAY_BACKGROUND_COLOR;
}

@end
