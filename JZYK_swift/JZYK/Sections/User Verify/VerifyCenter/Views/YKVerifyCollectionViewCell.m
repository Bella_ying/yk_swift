//
//  YKVerifyCollectionViewCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKVerifyCollectionViewCell.h"
#import "UIImage+DFExtension.h"

@interface YKVerifyCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (weak, nonatomic) IBOutlet UIImageView *showVerifyStateImageV;
@property (weak, nonatomic) IBOutlet UILabel *verifyContentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWithTop;

@end

@implementation YKVerifyCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLabelWithTop.constant = 17 * ASPECT_RATIO_WIDTH;
    [self.verifyContentLabel layoutIfNeeded];
}

- (void)updateCollectionViewCellWithdata:(YKVerifyListModel *)model index:(NSIndexPath *)indexPath {
    
    DLog(@"===%@",model);
    //tag=1 个人信息 tag=2 工作信息 tag=3 紧急联系人 tag=4收款信息 tag=5 手机运营商 tag=6 卡片等级 tag＝7 更多信息 tag=8 芝麻授信 tag=9 支付宝认证 tag=10 淘宝认证 tag=12 工资卡
    //根据tag来 换掉本地显示图片
    switch ([model.tag intValue]) {
        case 1:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"verify_identity"]];
            break;
            
        case 2:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"verify_workInfo"]];
            break;
        case 3:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"verify_contactWay"]];
            break;
        case 4:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"verify_creditCard"]];
            break;
        case 5:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"verify_ mobileOperator"]];
            break;
        case 6:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo]];
            break;
        case 7:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"mine_authentication_more"]];
            break;
        case 8:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"mine_authentication_zhimashouxin"]];
            break;
        case 9:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"mine_authentication_zhifubao"]];
            break;
        case 10:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"verify_taoBao"]];
            break;
        case 11:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"verify_creditCard"]];
        default:
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"verify_creditCard"]];
            break;
    }
    
    self.titleL.text = model.title;
    if ([model.show_verify_tag integerValue] == 1) {
        self.showVerifyStateImageV.hidden = NO;
//        self.verifyContentLabel.hidden = NO;
//        self.verifyContentLabel.text = model.verify_tag_content;
//        if ([model.logo_color isEqualToString:@"red"]) {
//            self.showVerifyStateImageV.image = [UIImage imageNamed:@"label_bg_red"];
//        } else if ([model.logo_color isEqualToString:@"yellow"]) {
//            self.showVerifyStateImageV.image = [UIImage imageNamed:@"verify_guoqi"];
//        } else {
//            //            self.showVerifyImgView.hidden = YES;
//            UIImage *image = [UIImage imageNamed:@"label_bg"];
//            image = [image df_tintedImageWithColor:[UIColor yk_colorWithHexString:model.logo_color]];
//            self.showVerifyStateImageV.image = image;
//        }
        [self.showVerifyStateImageV sd_setImageWithURL:[NSURL URLWithString:model.verify_tag_image_url]];
    }else{
        self.showVerifyStateImageV.hidden = YES;
        self.verifyContentLabel.hidden = YES;
    }
    
}

@end
