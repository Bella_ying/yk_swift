//
//  YKVerifyCollectionReusableHeadView.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKVerifyCollectionReusableHeadView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
