//
//  YKVerifyHeadView.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKVerifyHeadView.h"

@interface YKVerifyHeadView ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation YKVerifyHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}
- (void)setup{
    
    self.backgroundColor = [UIColor orangeColor];
    [self addSubview:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
}

- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.image = [UIImage imageNamed:@"verify_banner"];
    }
    return _imageView;
}

@end
