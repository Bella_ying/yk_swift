//
//  YKVerifyCollectionViewCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKVerifyCenterModel.h"
@interface YKVerifyCollectionViewCell : UICollectionViewCell

- (void)updateCollectionViewCellWithdata:(YKVerifyListModel *)model index:(NSIndexPath *)indexPath;
@end
