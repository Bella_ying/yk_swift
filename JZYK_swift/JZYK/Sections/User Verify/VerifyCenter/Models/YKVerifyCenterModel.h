//
//  YKVerifyCenterModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/18.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKVerifyListModel : YKBaseModel

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *operators;
@property (nonatomic, copy) NSString *tag;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *title_mark;
@property (nonatomic, copy) NSString *config;
@property (nonatomic, copy) NSString *group;

@property (nonatomic, copy) NSString *show_verify_tag;//是否显示过期的脚标，1：显示，0 ：不显示
@property (nonatomic, copy) NSString *verify_tag_content;//脚标的内容
@property (nonatomic, copy) NSString *logo_color;//角标颜色
@property (nonatomic, copy) NSString *verify_tag_image_url;//角标图片-桔子优卡用

//芝麻授信SDK有问题，换h5
@property (nonatomic, copy) NSString *first_url;

@end

@interface YKVerifyActinfoModel : YKBaseModel

@property (nonatomic, copy) NSString *act_logo;
@property (nonatomic, copy) NSString *act_url;

@end

@interface YKVerifyAgreementModel : YKBaseModel

@property (nonatomic, copy) NSString *link;
@property (nonatomic, copy) NSString *title;

@end

@interface YKVerifyFooterModel : YKBaseModel

@property (nonatomic, copy) NSString *card_type;
@property (nonatomic, copy) NSString *status;  //3：去下单  2: 查看额度 1: 计算中.. 0：未认证完，隐藏
@property (nonatomic, copy) NSString *title;

@end

@interface YKVerifyCenterModel : YKBaseModel

@property (nonatomic, strong) YKVerifyFooterModel *footer;
@property (nonatomic, strong) YKVerifyAgreementModel *agreement;
@property (nonatomic, strong) YKVerifyActinfoModel *act_info;
@property (nonatomic, copy) NSArray<YKVerifyListModel *> *list;

@end
