//
//  YKVerifyBindingCardModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/26.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKBingingCardMessageEntity : YKBaseModel

@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *message;

@end

@interface YKBindingCardAlertEntity : YKBaseModel

@property (nonatomic, copy) NSString *alert_title;    //弹窗标题
@property (nonatomic, copy) NSString *alert_desc;     //弹窗内容描述
@property (nonatomic, copy) NSString *alert_confirm;  //弹窗按钮确认
@property (nonatomic, copy) NSString *about_title;    //关于我们的文字
//@property (nonatomic, copy) NSString *about_deposit;  //关于我们的跳转URL

@end

@interface YKVerifyBindingCardModel : YKBaseModel

@property (nonatomic, copy) NSString *about_deposit;
@property (nonatomic, strong) YKBindingCardAlertEntity *alert;
@property (nonatomic, copy) NSString *bank_name;
@property (nonatomic, copy) NSString *name; //名字
@property (nonatomic, copy) NSString *bank_id;
@property (nonatomic, copy) NSString *card_no;
@property (nonatomic, copy) NSString *card_remark;
@property (nonatomic, copy) NSString *deposit_tips;
@property (nonatomic, assign) NSInteger have_card; //0,没有银行卡，跳转到"添加银行卡";1,有银行卡，跳转到"我的银行卡"
@property (nonatomic, copy) NSString *is_deposit_open_account;  //0、存管完成 1、开通存管弹框； 2、银行卡信息变更 3、更换银行卡 ；4、已经是口袋理财的投资用户 5、认证过期：其他平台解绑
@property (nonatomic, copy) NSString *phone_num;
@property (nonatomic, copy) NSArray <NSDictionary *> *protocols;

@end
