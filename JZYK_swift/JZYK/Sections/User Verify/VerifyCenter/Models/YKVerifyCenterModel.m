//
//  YKVerifyCenterModel.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/18.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKVerifyCenterModel.h"

@implementation YKVerifyListModel

+(NSDictionary<NSString *,id> *)modelCustomPropertyMapper
{
    return @{@"operators":@"operator",
             @"config":@"init_config"};
}

@end

@implementation YKVerifyActinfoModel


@end

@implementation YKVerifyAgreementModel


@end

@implementation YKVerifyFooterModel


@end

@implementation YKVerifyCenterModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{
             @"list":[YKVerifyListModel class]};
}

@end
