//
//  YKVerifyCenterListVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/11.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKVerifyCenterListVC.h"
#import "YKVerifyHeadView.h"
#import "YKVerifyCollectionViewCell.h"
#import "YKVerifyCollectionReusableHeadView.h"
#import "JHCollectionViewFlowLayout.h"
#import "YKVerifyCenterModel.h"
#import "YKRealNameViewController.h"
#import "YKContactsViewController.h"
#import "YKMineUserInfoViewController.h"
#import "YKJobInfoVC.h"
#import "YKVerifyBindingCardModel.h"
#import "YKBankCardListVC.h"
#import "YKAddBankCardViewController.h"
#import "YKContactUploadManager.h"
#import "YKAccessAlertView.h"
#import "YKMXSdk.h"
#import "YKAccessAlertView.h"
#import "QiYuManager.h"

#define kBottomViewHeight (65 + 25 * ASPECT_RATIO_WIDTH)
@interface YKVerifyCenterListVC ()<UICollectionViewDataSource,UICollectionViewDelegate,JHCollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *commitBtn;
@property (nonatomic, strong) YKVerifyCenterModel *centerModel;
@property (nonatomic, strong) YKVerifyFooterModel *footerModel;
@property (nonatomic, strong) YKVerifyListModel *listModel;

@property (nonatomic, strong) NSMutableArray *firstSectionArray;
@property (nonatomic, strong) NSMutableArray *secondSectionArray;
@property (nonatomic, copy)   NSString *firstSectionTitle;
@property (nonatomic, copy)   NSString *secondSectionTitle;
@property (nonatomic, assign) NSInteger real_name;

@end

static NSString *const headerId = @"YKVerifyHeadView";
static NSString * const collectionCellId = @"YKVerifyCollectionViewCell";
static NSString *const collectionSecitonHeadId = @"YKVerifyCollectionReusableHeadView";

@implementation YKVerifyCenterListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"认证中心";
    [self creatBackButtonWithAction:@selector(backBtnClick)];
    [self prepareUI];
    
    self.firstSectionArray = @[].mutableCopy;
    self.secondSectionArray = @[].mutableCopy;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"帮助" style:(UIBarButtonItemStylePlain) target:self action:@selector(clickRightItem)];
    self.navigationItem.rightBarButtonItem.tintColor = LABEL_TEXT_COLOR;
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView.mj_header beginRefreshing];
}

- (void)prepareUI{
    
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(kBottomViewHeight);
    }];
    [self.bottomView addSubview:self.commitBtn];
    [self.commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bottomView);
        make.bottom.equalTo(self.bottomView).offset(-25 * ASPECT_RATIO_WIDTH);
    }];
    [self.commitBtn addTarget:self action:@selector(clickCommitBtn) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    self.collectionView.mj_header = [YKRefreshHeader headerWithRefreshingBlock:^{
        [self requestData];
    }];
    
}
#pragma mark --- aciton 点击事件
- (void)clickRightItem{
    //到7鱼
    [QiYuManager yk_sharedQiYuManager].commonQuestionTemplateId = [[ConfigManager config] getQiyuTemplateIDForKey:@"verify_center"];
    [[QiYuManager yk_sharedQiYuManager] yk_qiYuService];
    [[QiYuManager yk_sharedQiYuManager] yk_qiYuDelegate];
}
- (void)backBtnClick{
    [[YKTabBarController yk_shareTabController] yk_setSelectedIndex:YKTabSelectedIndexMine viewController:nil];
}
- (void)clickCommitBtn{
    DLog(@"点击clickCommitBtn");
    
    if ([self.centerModel.footer.status isEqual:@"1"]) {
        return;
    }
    WEAK_SELF
    if (![self.centerModel.footer.status isEqual:@"4"]) {
        if (![YK_NSUSER_DEFAULT objectForKey:@"SHOWVERIFYSTUDENT"]) {
            [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeStudent contentText:@""];
            [YK_NSUSER_DEFAULT setObject:@"show" forKey:@"SHOWVERIFYSTUDENT"];
            [YKAccessAlertView sharedAlertManager].studentBottomBlock = ^{
                [weakSelf clickCommitBtn];
                return;
            };
            return;
        }
    }

        [[HTTPManager session] postRequestForKey:kCreditAppUserCreditTop showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
            STRONG_SELF
            if (code == 0 && success) {
                strongSelf.footerModel = [YKVerifyFooterModel yy_modelWithJSON:jsonDict[@"footer"]];
                
                [[QLAlert alert] showWithTitle:nil message:message btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {
                    if ([strongSelf.footerModel.status isEqual:@"0"]) {
                        strongSelf.bottomView.hidden = YES;
                        [strongSelf.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.height.mas_equalTo(0);
                        }];
                    }else{
                        strongSelf.bottomView.hidden = NO;
                        [strongSelf.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.height.mas_equalTo(kBottomViewHeight);
                        }];
                        if ([strongSelf.footerModel.status isEqual:@"1"]) {
                            strongSelf.commitBtn.userInteractionEnabled = NO;
                        }else if ([strongSelf.footerModel.status isEqual:@"2"] || [strongSelf.footerModel.status isEqual:@"4"]) {
                            strongSelf.commitBtn.userInteractionEnabled = YES;
                        }
                    }
                    [strongSelf.commitBtn setTitle:strongSelf.footerModel.title forState:UIControlStateNormal];

                    [strongSelf.collectionView reloadData];
                }];
            }else{
                [[iToast makeText:message] show];
            }
            [weakSelf.collectionView.mj_header endRefreshing];
        } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
            [weakSelf.collectionView.mj_header endRefreshing];
            [[iToast makeText:errMsg] show];
        }];
}

- (void)requestData{
    //判断登录态是否失效
//    [[UserManager sharedUser] checkLoginStatus:^(BOOL sucOrFail) {
//        if (sucOrFail) {
    
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditCardGetVerificationInfo showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        if (code == 0 && success) {
            [weakSelf.firstSectionArray removeAllObjects];
            [weakSelf.secondSectionArray removeAllObjects];
            weakSelf.centerModel = [YKVerifyCenterModel yy_modelWithJSON:jsonDict[@"item"]];
            for (YKVerifyListModel * model in weakSelf.centerModel.list) {
                if ([model.type integerValue]==1) {
                    [weakSelf.firstSectionArray addObject:model];
                }else {
                    [weakSelf.secondSectionArray addObject:model];
                }
            }
            NSArray *list_nameArr = jsonDict[@"item"][@"list_name"];
            for (NSDictionary *dict in list_nameArr) {
                if ([dict[@"index"] integerValue] == 1) {
                    weakSelf.firstSectionTitle = [NSString stringWithFormat:@"%@",dict[@"title"]];
                }else if ([dict[@"index"] integerValue] == 3){
                    weakSelf.secondSectionTitle = [NSString stringWithFormat:@"%@",dict[@"title"]];
                }
            }
            
            
            //底部按钮新的逻辑--只用status字段判断
            if ([weakSelf.centerModel.footer.status isEqualToString:@"0"]) {
                //认证未完成，隐藏btn
                weakSelf.bottomView.hidden = YES;
                [weakSelf.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_offset(0);
                }];
//                [weakSelf.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
//                    make.bottom.equalTo(strongSelf.view).offset(-BOTTOM_HEIGHT);
//                }];
            }else{
                //显示按钮
                weakSelf.bottomView.hidden = NO;
                [weakSelf.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_offset(kBottomViewHeight);
                }];
                
                if ([weakSelf.centerModel.footer.status isEqualToString:@"2"]) {
                    //去查看额度
                    weakSelf.commitBtn.userInteractionEnabled = YES;
                    
                }else if ([weakSelf.centerModel.footer.status isEqualToString:@"1"]){
                    //正在计算额度
                    weakSelf.commitBtn.userInteractionEnabled = NO;
                }else if ([weakSelf.centerModel.footer.status isEqualToString:@"3"]){
                    //去下单
//                    weakSelf.commitBtn.backgroundColor = MAIN_THEME_COLOR;
//                    weakSelf.commitBtn.userInteractionEnabled = YES;
                }
                [weakSelf.commitBtn setTitle:weakSelf.centerModel.footer.title forState:(UIControlStateNormal)];
            }
            
            //针对footer的model赋值一次
            weakSelf.footerModel = [YKVerifyFooterModel yy_modelWithJSON:jsonDict[@"item"][@"footer"]];
            
            weakSelf.real_name = [jsonDict[@"item"][@"real_verify_status"] integerValue];

        }else{
            [[iToast makeText:message] show];
        }
        
        [weakSelf.collectionView reloadData];
        [weakSelf.collectionView.mj_header endRefreshing];
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [weakSelf.collectionView.mj_header endRefreshing];
        [[iToast makeText:errMsg] show];

    }];
    
    
    
//           }];
//    [self.collectionView.mj_header endRefreshing];
//    }];
}

#pragma mark ---- UICollectionViewDataSource

//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case 1:
            return self.firstSectionArray.count;
            break;
        case 2:
            return self.secondSectionArray.count;
            break;
        default:
            break;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YKVerifyCollectionViewCell *cell = (YKVerifyCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:collectionCellId forIndexPath:indexPath];
    if (indexPath.section == 1) {
        self.listModel = [self.firstSectionArray objectAtIndex:indexPath.row];
        
    }else if (indexPath.section == 2){
        self.listModel = [self.secondSectionArray objectAtIndex:indexPath.row];
    }
    [cell updateCollectionViewCellWithdata:self.listModel index:indexPath];
    
    return cell;
}

//header的size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return CGSizeMake(WIDTH_OF_SCREEN, 96 * ASPECT_RATIO_WIDTH);
    }else{
        return CGSizeMake(WIDTH_OF_SCREEN, 30 * ASPECT_RATIO_WIDTH);
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    if([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        if (indexPath.section == 0){
            YKVerifyHeadView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerId forIndexPath:indexPath];
            return headView;
        }else{
            YKVerifyCollectionReusableHeadView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionSecitonHeadId forIndexPath:indexPath];
            if (indexPath.section==1){
                headerView.titleLabel.attributedText = [NSString yk_addAttributeWithHtml5String:self.firstSectionTitle];
                //@"基础信息";
            }else if (indexPath.section == 2){
                headerView.titleLabel.attributedText = [NSString yk_addAttributeWithHtml5String:self.secondSectionTitle];
                //@"加分信息";
            }
            
            return headerView;
        }

    }
    
    return nil;
}

//点击item方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 1) {
        [self didSelectCollectionView:[self.firstSectionArray objectAtIndex:indexPath.row]];
    }else if (indexPath.section == 2){
        [self didSelectCollectionView:[self.secondSectionArray objectAtIndex:indexPath.row]];
    }
    
}

#pragma mark - JHCollectionViewDelegateFlowLayout
- (UIColor *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout backgroundColorForSection:(NSInteger)section
{
    return WHITE_COLOR;
}


-(UICollectionView*)collectionView{
    if (!_collectionView) {
        JHCollectionViewFlowLayout *layout = [[JHCollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(WIDTH_OF_SCREEN / 4,ASPECT_RATIO_WIDTH * 100);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.translatesAutoresizingMaskIntoConstraints=NO;
        _collectionView.backgroundColor = GRAY_BACKGROUND_COLOR;
        
        [_collectionView registerNib:[UINib nibWithNibName:@"YKVerifyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:collectionCellId];
        [_collectionView registerNib:[UINib nibWithNibName:@"YKVerifyCollectionReusableHeadView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionSecitonHeadId];
        [_collectionView registerClass:[YKVerifyHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerId];
        
        
    }
    return _collectionView;
}

//将collectionView的点击事件抽出来写，因为第一次点击时如果弹出前程提示框并不操作点击时间的响应
- (void)didSelectCollectionView:(YKVerifyListModel *)model{
    //其他项目：tag=1 个人信息 tag=2 工作信息 tag=3 紧急联系人 tag=4收款信息 tag=5 手机运营商 tag=6 卡片等级 tag＝7 更多信息 tag=8 芝麻授信 tag=9 支付宝认证 tag=10 淘宝认证
    //桔子优卡：tag:1-身份认证 2-工作信息 3-联系方式 4-银行卡 5-手机运营商 7-更多认证 10-微信认证（H5） 16-个人信息 17-信用卡（魔蝎） 18-支付宝/淘宝（H5） 19-公积金（魔蝎）
    switch ([model.tag integerValue]) {
        case 1:
        {
            //埋点（认证中心——身份认证）
            YKRealNameViewController * vc = [YKRealNameViewController new];
            vc.shouldNext = NO;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        case 2:
        {
            //埋点（认证中心——工作信息）
            YKJobInfoVC *personalVC = [[YKJobInfoVC alloc] init];
            [self.navigationController pushViewController:personalVC animated:YES];
        }
            break;
        case 3:
        {
            //埋点（认证中心——紧急联系人）
            YKContactsViewController * vc = [YKContactsViewController new];
            vc.shouldNext = NO;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 4:
        {
            //埋点（认证中心——收款银行卡）

//            if (self.real_name !=1) {
//
//                [[QLAlert alert] showWithTitle:nil message:@"亲，请先填写个人信息哦～" btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {
//
//                }];
//                return ;
//            }
//
//            WEAK_SELF
//            [[HTTPManager session] getRequestForKey:kCreditCardGetDepositOpenInfo showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
//                if (code == 0 && success) {
//
//                    YKVerifyBindingCardModel *cardModel = [YKVerifyBindingCardModel yy_modelWithJSON:jsonDict];
//                    if (cardModel.have_card) {
//                        YKBankCardListVC *cardVC = [[YKBankCardListVC alloc] init];
//                        [weakSelf.navigationController pushViewController:cardVC animated:YES];
//                    }else{
//                        YKAddBankCardViewController *addVC = [[YKAddBankCardViewController alloc] init];
//                        [weakSelf.navigationController pushViewController:addVC animated:YES];
//                    }
//
//                }else{
//                    [[iToast makeText:message] show];
//                }
//
//            } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
//                [[iToast makeText:errMsg] show];
//            }];

        }
            break;
        case 5:
        {
            //埋点（认证中心——手机运营商）

            //手机运营商之前需要先实名
            if (self.real_name !=1) {

                [[QLAlert alert] showWithTitle:nil message:@"亲，请先填写个人信息哦～" btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {

                }];
                return;
            }

            if ([YKTools yk_permissionsWithAddressBook] == YKPermissionStatusAuthorized) {
                YKBrowseWebController *vc = [[YKBrowseWebController alloc] init];
                vc.popToDesignatedPage = PopToDesignatedPageVerifyCenter;
                vc.url = model.url;
                [self.navigationController pushViewController:vc animated:YES];
                [[YKContactUploadManager shareContact] yk_uploadContactItem];
            } else if ([YKTools yk_permissionsWithAddressBook] == YKPermissionStatusNotDenied) {
                [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeContactBook contentText:@"通讯录"];
            }
        }
            break;
        case 6:
        {
            YKBrowseWebController *vc = [[YKBrowseWebController alloc]init];
            vc.popToDesignatedPage = PopToDesignatedPageVerifyCenter;
            vc.url = model.url;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 7:
        {
            //埋点（认证中心——更多）
            YKBrowseWebController *vc = [[YKBrowseWebController alloc]init];
            vc.popToDesignatedPage = PopToDesignatedPageVerifyCenter;
//            vc.navTitle = @"更多信息";
            vc.url = model.url;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 8:
        {
            //埋点（认证中心——芝麻授信）
//            [BuriedPointManager reportWithKey:@"Certification" andArray:@[@"芝麻授信"]];
//            [BuriedPointManager reportWithKey:@"Certification_zmsx_for_rate" andArray:@[@"click"]];

            /************ 不再支持芝麻授信 **************/

        }
            break;
        case 9:
        {
            [[YKMXSdk shareMxSdk] mxSdkFunctionWithFromVc:self taskType:@"alipay"];

        }break;
        case 10:
        {
            YKBrowseWebController *vc = [[YKBrowseWebController alloc]init];
            vc.popToDesignatedPage = PopToDesignatedPageVerifyCenter;
            vc.url = model.url;
            [self.navigationController pushViewController:vc animated:YES];
        }break;
        case 12:
        {
            //工资卡认证之前需要先实名
            if (self.real_name !=1) {

                [[QLAlert alert] showWithTitle:nil message:@"亲，请先填写个人信息哦～" btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {

                }];
                return ;
            }
            YKBrowseWebController *vc = [[YKBrowseWebController alloc]init];
            vc.popToDesignatedPage = PopToDesignatedPageVerifyCenter;
            vc.url = model.url;
            [self.navigationController pushViewController:vc animated:YES];
        }break;
        case 16:
        {
            //个人信息
            YKMineUserInfoViewController *userInfoVc = [[YKMineUserInfoViewController alloc] initWithNibName:@"YKMineUserInfoViewController" bundle:nil];
            [self.navigationController pushViewController:userInfoVc animated:YES];
        }break;
        case 17:
        {
            [[YKMXSdk shareMxSdk] mxSdkFunctionWithFromVc:self taskType:@"email"];
            
        }
            break;
        case 18:
        { //支付宝淘宝
            
            YKBrowseWebController *vc = [[YKBrowseWebController alloc] init];
            vc.popToDesignatedPage = PopToDesignatedPageVerifyCenter;
            vc.url = model.url;
            [self.navigationController pushViewController:vc animated:YES];
            
//            if ([YKTools yk_permissionsWithPhoto] == YKPermissionStatusAuthorized) {
//                YKBrowseWebController *vc = [[YKBrowseWebController alloc] init];
//                vc.url = model.url;
//                [self.navigationController pushViewController:vc animated:YES];
//            } else if ([YKTools yk_permissionsWithPhoto] == YKPermissionStatusNotDenied) {
//                [[QLAlert alert] showWithTitle:nil message:@"请您设置允许桔子优卡访问您的相机\n设置>隐私>相机！" btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {
//                    [YKTools yk_jumpPermissions];
//                }];
//            }
        }
            break;
        case 19:
        {
            [[YKMXSdk shareMxSdk] mxSdkFunctionWithFromVc:self taskType:@"fund"];
        }break;

        default:
        {
            YKBrowseWebController *vc = [[YKBrowseWebController alloc] init];
            vc.popToDesignatedPage = PopToDesignatedPageVerifyCenter;
            vc.url = model.url;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
    }
}

#pragma -mark -- getter
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = GRAY_BACKGROUND_COLOR;
    }
    return _bottomView;
}
- (UIButton *)commitBtn
{
    if (!_commitBtn) {
        _commitBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"system_commit_btn"];
        [_commitBtn setBackgroundImage:image forState:(UIControlStateNormal)];
        [_commitBtn setTitle:@"立即获取额度" forState:(UIControlStateNormal)];
        _commitBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [_commitBtn setTitleColor:WHITE_COLOR forState:(UIControlStateNormal)];
        _commitBtn.layer.cornerRadius = image.size.height * 0.5;
        _commitBtn.layer.masksToBounds = YES;
    }
    return _commitBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
