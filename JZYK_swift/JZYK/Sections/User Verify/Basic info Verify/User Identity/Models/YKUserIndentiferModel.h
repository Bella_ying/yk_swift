//
//  YKUserIndentiferModel.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/17.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKDegreesModel : NSObject
@property (nonatomic, copy) NSString *degrees;//1,2,...
@property (nonatomic, copy) NSString *name;//中专、本科

@end

@interface YKMarriageModel : NSObject
@property (nonatomic, copy) NSString *marriage;//1,2,...
@property (nonatomic, copy) NSString *name;//未婚、已婚未育..

@end

@interface YKLivePeriodModel : NSObject
@property (nonatomic, copy) NSString *live_time_type;//1,2,...
@property (nonatomic, copy) NSString *name;//半年以内、半年到一年..

@end


@interface YKUserIndentiferModel : NSObject

@property (nonatomic, copy) NSString *real_verify_status;//实名认证状态
@property (nonatomic, copy) NSString *userID;//179695
@property (nonatomic, copy) NSString *can_upload_img;//身份证拍照开关 1 open 0 close
@property (nonatomic, copy) NSString *name;//名字
@property (nonatomic, copy) NSString *id_number;//身份证号
@property (nonatomic, copy) NSString *address;//地址
@property (nonatomic, copy) NSString *address_distinct;//现居地区
@property (nonatomic, copy) NSString *face_recognition_picture;//人脸照片
@property (nonatomic, copy) NSString *id_number_f_picture;//身份证反面照片
@property (nonatomic, copy) NSString *id_number_z_picture;//身份证正面照片
@property (nonatomic, copy) NSString *longitude;//
@property (nonatomic, copy) NSString *latitude;//
@property (nonatomic, copy) NSString *degrees;//学历
@property (nonatomic, copy) NSString *marriage;//婚姻状态
@property (nonatomic, copy) NSString *live_period;
@property (nonatomic, copy) NSString *progress_bar_img;//进度条图片
@property (nonatomic, strong) NSArray<YKDegreesModel *> *degrees_all;
@property (nonatomic, strong) NSArray<YKMarriageModel *> *marriage_all;
@property (nonatomic, strong) NSArray<YKLivePeriodModel *> *live_time_type_all;

@end
