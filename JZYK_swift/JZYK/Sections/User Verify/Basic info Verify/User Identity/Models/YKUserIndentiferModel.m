//
//  YKUserIndentiferModel.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/17.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKUserIndentiferModel.h"

@implementation YKDegreesModel
@end

@implementation YKMarriageModel
@end

@implementation YKLivePeriodModel
@end

@implementation YKUserIndentiferModel

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{
             @"userID":@"id",
             };
}

@end
