//
//  YKRealNameViewController.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKRealNameViewController.h"
#import "YKContactsViewController.h"
#import "YKUserIndentiferModel.h"
#import <MGLivenessDetection/MGLivenessDetection.h>
#import <MGIDCard/MGIDCard.h>
#import "YKAccessAlertView.h"

@interface YKRealNameViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *progressImgV;
@property (weak, nonatomic) IBOutlet UIButton *faceBtn;
@property (weak, nonatomic) IBOutlet UIButton *idCardFrontBtn;
@property (weak, nonatomic) IBOutlet UIButton *idCardBehideBtn;
@property (weak, nonatomic) IBOutlet UIView *inputBgView;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *idCardTF;
@property (weak, nonatomic) IBOutlet UILabel *inputTipLab;
@property (weak, nonatomic) IBOutlet UIButton *stepBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBgHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *faceBtnTopConstraint;
@property (nonatomic, copy) NSString *saveRequestKey;
@property (nonatomic, strong) YKUserIndentiferModel * pageModel;

@end

@implementation YKRealNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"身份认证";
    [self creatBackButtonWithAction:@selector(backBtnClick)];
    [YKTools yk_permissionsWithCamera];
}

- (void)viewWillAppear:(BOOL)animated {
    
    if (self.shouldNext == NO) {
        [self.stepBtn setTitle:@"确 定" forState:UIControlStateNormal];
        self.progressImgV.hidden = YES;
        self.progressBgHeightConstraint.constant = 0;
        self.titleLabTopConstraint.constant = 44 * ASPECT_RATIO_WIDTH;
        self.faceBtnTopConstraint.constant = 32 * ASPECT_RATIO_WIDTH;
    }
    if (!self.pageModel) {
        [self featchPersonIndentiferInfo];
    }
}

#pragma mark -- 更新UI(updatePicture:是否拍照动作导致的刷新；防止拍照完没有点保存,再次进此页面却无法再编辑)
- (void)updateUI {
    //进度条
    [self.progressImgV sd_setImageWithURL:[NSURL URLWithString:self.pageModel.progress_bar_img] placeholderImage:[UIImage imageNamed:@"idCard_progress"]];
    //人脸
    if ([self.pageModel.face_recognition_picture yk_isValidString]) {
        [self.faceBtn setBackgroundImage:[UIImage imageNamed:@"face_yes"] forState:UIControlStateNormal];
    }else{
        [self.faceBtn setBackgroundImage:[UIImage imageNamed:@"face_no"] forState:UIControlStateNormal];
    }
    //正面
    if ([self.pageModel.id_number_z_picture yk_isValidString]) {

        [self.idCardFrontBtn setBackgroundImage:[UIImage imageNamed:@"id_front_yes"] forState:UIControlStateNormal];
        self.inputBgView.hidden = NO;
        self.inputTipLab.hidden = NO;
        self.nameTF.text = self.pageModel.name? : @"";
        self.idCardTF.text = self.pageModel.id_number? : @"";
        self.nameTF.enabled = !([self.pageModel.real_verify_status integerValue] == 1);
        self.idCardTF.enabled = !([self.pageModel.real_verify_status integerValue] == 1);
    }else{
        [self.idCardFrontBtn setBackgroundImage:[UIImage imageNamed:@"id_front_no"] forState:UIControlStateNormal];
        self.inputBgView.hidden = YES;
        self.inputTipLab.hidden = YES;
    }
    //反面
    if ([self.pageModel.id_number_f_picture yk_isValidString]) {

        [self.idCardBehideBtn setBackgroundImage:[UIImage imageNamed:@"id_behind_yes"] forState:UIControlStateNormal];
    }else{
        [self.idCardBehideBtn setBackgroundImage:[UIImage imageNamed:@"id_behind_no"] forState:UIControlStateNormal];
    }
}

#pragma mark -- 点击人脸识别
- (IBAction)faceBtnClick:(UIButton *)sender {
    if ([self.pageModel.real_verify_status integerValue] == 1 ) {
        return;
    }
    
    if ([YKTools yk_permissionsWithCamera] == YKPermissionStatusNotDenied) {
        [[QLAlert alert] showWithTitle:@"" message:@"请在设置-->隐私-->相机-->桔子优卡中打开相机使用权限" btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {
        }];
        return;
    }
    if ([YKTools yk_permissionsWithCamera] == YKPermissionStatusAuthorized) {
        //坐姿弹框
        [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeFace contentText:@""];
        [[YKAccessAlertView sharedAlertManager] setIdentificationAction:^{
            [self startFace];
        }];
    }
}
- (void)startFace
{
    if (![MGLicenseManager getLicense]) {
        [MBProgressHUD showHudWithMessage:@"联网授权中..."];
        [MGLicenseManager licenseForNetWokrFinish:^(bool License) {
            [self startFaceSDK];
        }];
    } else {
        [self startFaceSDK];
    }
}

- (void)startFaceSDK
{
    WEAK_SELF
    MGLiveManager *manager = [[MGLiveManager alloc] init];
    manager.detectionWithMovier = NO;
    manager.actionCount = 3;
    [manager startFaceDecetionViewController:self finish:^(FaceIDData *finishDic, UIViewController *viewController) {
        STRONG_SELF
        [strongSelf dismissViewControllerAnimated:YES completion:nil];
        NSData *header = [[finishDic images] valueForKey:@"image_best"];
        [strongSelf uploadImg:header type:10];
        
    } error:^(MGLivenessDetectionFailedType errorType, UIViewController *viewController) {
        [viewController dismissViewControllerAnimated:YES completion:nil];
        switch (errorType) {
            case DETECTION_FAILED_TYPE_ACTIONBLEND:
            {
                [[iToast makeText:@"人脸识别动作错误,请重试!"] show];
            }
                break;
            case DETECTION_FAILED_TYPE_TIMEOUT:
            {
                [[iToast makeText:@"人脸识别超时,请重试!"] show];
            }
                break;
                
            default:
            {
                [[iToast makeText:@"人脸识别失败,请重试!"] show];
            }
                break;
        }
    }];
}

#pragma mark -- 点击身份证正面
- (IBAction)idCardFrontBtnClick:(UIButton *)sender {
    if ([self.pageModel.real_verify_status integerValue] == 1) {
        return;
    }else{
        [self startIDCard:1];
    }
}

#pragma mark -- 点击身份证反面
- (IBAction)idCardBehideBtnClick:(UIButton *)sender {
    if ([self.pageModel.real_verify_status integerValue] == 1) {
        return;
    }else{
        [self startIDCard:2];
    }
}

- (void)startIDCard:(NSInteger)type {
    
    if ([YKTools yk_permissionsWithCamera] == YKPermissionStatusNotDenied) {
        [[QLAlert alert] showWithTitle:@"" message:@"请在设置-->隐私-->相机-->桔子优卡中打开相机使用权限" btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {
        }];
        return;
    }
    if ([YKTools yk_permissionsWithCamera] == YKPermissionStatusAuthorized) {
        if (![MGLicenseManager getLicense]) {
            [MBProgressHUD showHudWithMessage:@"联网授权中..."];
            [MGLicenseManager licenseForNetWokrFinish:^(bool License) {
                [self startFaceIdSDKWithType:type];
            }];
        } else {
            [self startFaceIdSDKWithType:type];
        }
    }
}

- (void)startFaceIdSDKWithType:(NSInteger)type
{
    WEAK_SELF
    MGIDCardManager *cardManager = [[MGIDCardManager alloc] init];
    [cardManager IDCardStartDetection:self IdCardSide:type == 1 ? IDCARD_SIDE_FRONT :IDCARD_SIDE_BACK finish:^(MGIDCardModel *model) {
        STRONG_SELF
        
        NSData *data;
        NSString *file;
        if (UIImageJPEGRepresentation([model croppedImageOfIDCard], 0.5) == nil) {
            data = UIImagePNGRepresentation([model croppedImageOfIDCard]);
            file = @"png";
        } else {
            data = UIImageJPEGRepresentation([model croppedImageOfIDCard], 0.5);
            file = @"jpg";
        }
        if (type == 1) {
            [strongSelf uploadPlusFaceImg:data imageType:file];
        } else {
            [strongSelf uploadImg:data type:12];
        }
        
    } errr:^(MGIDCardError errorType) {
        STRONG_SELF
        strongSelf.nameTF.enabled = YES;
        strongSelf.idCardTF.enabled = YES;
        self.nameTF.placeholder = @"请输入真实姓名";
        self.idCardTF.placeholder = @"请输入身份证号";
        if (errorType != MGIDCardErrorCancel) {
            [[iToast makeText:@"获取身份证图片失败，请重新识别！"] show];
        }
    }];
}

#pragma mark --上传图片到face++，解析姓名等字段
- (void)uploadPlusFaceImg:(NSData *)imgData imageType:(NSString *)imageType {
    WEAK_SELF
    [[HTTPManager session] uploadImageWithData:imgData serverUrlKey:kCreditFacePlusIdcard key:@"image_file" fileName:[@"idcard_image." stringByAppendingString:imageType] parameters:@{} uploadSuccess:^(NSDictionary<NSString *,id> * _Nonnull json) {
        STRONG_SELF
        if (json[@"data"]) {
            NSDictionary *cardDic = json[@"data"];
            strongSelf.pageModel.name = cardDic[@"name"];
            strongSelf.pageModel.id_number = cardDic[@"id_card_number"];
            //上传身份证正面到后台
            [strongSelf uploadImg:imgData type:11];
        }
    } uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
    } uploadFailure:^(NSString * _Nonnull error) {
        STRONG_SELF
        strongSelf.nameTF.enabled = YES;
        strongSelf.idCardTF.enabled = YES;
        strongSelf.nameTF.placeholder = @"请输入真实姓名";
        strongSelf.idCardTF.placeholder = @"请输入身份证号";
    }];
}

#pragma mark --上传图片到后台  type:10:头像  11：身份证正面    12：身份证反面
- (void)uploadImg:(NSData *)imgData type:(NSInteger)type
{
    WEAK_SELF
    [[HTTPManager session] uploadImageWithData:imgData serverUrlKey:kPictureUploadImage key:@"attach" fileName:@"imageFile.jpg" parameters:@{@"type":[NSString stringWithFormat:@"%ld",(long)type]} uploadSuccess:^(NSDictionary<NSString *,id> * _Nonnull json) {
        STRONG_SELF
        NSString *url = @"";
        if (json[@"data"] && json[@"data"][@"item"] && json[@"data"][@"item"][@"url"]) {
            url = json[@"data"][@"item"][@"url"];
        }
        if (type == 10) {
            strongSelf.pageModel.face_recognition_picture = url;
        }else if (type == 11)
        {
            strongSelf.pageModel.id_number_z_picture = url;
        }else
        {
            strongSelf.pageModel.id_number_f_picture = url;
        }
        [strongSelf updateUI];

    } uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
    } uploadFailure:^(NSString * _Nonnull error) {
        [[iToast makeText:error] show];
    }];
}

#pragma mark -- 拉取个人信息
- (void)featchPersonIndentiferInfo {
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kCreditCardGetPersonInfo showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg)
     {
         STRONG_SELF
         if (success && code == 0) {
             strongSelf.pageModel = [YKUserIndentiferModel yy_modelWithJSON:json[@"item"]];
             [strongSelf updateUI];
             strongSelf.saveRequestKey = [self.pageModel.real_verify_status integerValue] == 1 ? kCreditInfoSavePersonInfo : kCreditCardSavePersonInfo;
         }
     } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
         [[iToast makeText:errMsg] show];
     }];
}

#pragma mark -- 点击下一步
- (IBAction)stepBtnClick:(UIButton *)sender {
    
    if (self.nameTF.text.length > 15 || self.nameTF.text.length <= 0) {
        [[iToast makeText:@"姓名长度不合法，请检查"] show];
        return;
    }
    
    if ([NSString validateIdentityCard:self.idCardTF.text] == NO) {
        [[iToast makeText:@"身份证号不合法，请检查"] show];
        return;
    }
    
    NSDictionary * param = @{@"name":self.nameTF.text,
                             @"id_number":self.idCardTF.text,
                             };
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:self.saveRequestKey showLoading:NO param:param succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg)
     {
         if (code == 0) {
             [[YKUserManager sharedUser] yk_updateRealName:self.nameTF.text];
             iToast *toast = [iToast makeText:msg];
             [toast setDuration:0.5];
             [toast setToastDidDisappear_blk:^{
                 STRONG_SELF
                 if (strongSelf.shouldNext) {
                     YKContactsViewController * vc = [YKContactsViewController new];
                     vc.shouldNext = YES;
                     [strongSelf.navigationController pushViewController:vc animated:YES];
                 } else {
                     [strongSelf.navigationController popViewControllerAnimated:YES];
                 }
             }];
             [toast show];
         } else {
             [[iToast makeText:msg] show];
         }
         
     } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
         [[iToast makeText:errMsg] show];
     }];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.nameTF) {
        if (range.location > 14) {
            return false;
        }
    }
    
    if (textField == self.idCardTF) {
        if (range.location > 17) {
            return false;
        }
    }
    return true;
}

- (void)backBtnClick {
    if (self.shouldNext) {
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[NSClassFromString(@"YKConfirmOrderViewController") class]]) {
                [self.navigationController popToViewController:vc animated:YES];
                return;
            }
        }
        [[YKTabBarController yk_shareTabController] yk_setSelectedIndex:YKTabSelectedIndexMine viewController:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
