//
//  YKAddBankCardViewController.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"

@interface YKAddBankCardViewController : YKBaseViewController

//card_desc:中信银行(0124) card_id:161720  bank_name:中信银行
@property (nonatomic, copy) void (^bankBindComplete)(NSString * card_desc, NSString *card_id, NSString *bank_name);

@end
