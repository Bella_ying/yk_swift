//
//  YKBankListVC.h
//  KDIOSApp
//
//  Created by haoran on 16/5/10.
//  Copyright © 2016年 KDIOSApp. All rights reserved.
//

#import "YKBaseViewController.h"
#import "YKBankListCell.h"

@interface YKBankListVC : YKBaseViewController
@property (nonatomic, copy) void (^selectBlock)(YKBankListEntity *entity);
@end
