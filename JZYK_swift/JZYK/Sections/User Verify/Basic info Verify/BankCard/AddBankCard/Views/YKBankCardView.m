//
//  YKBankCardView.m
//  KDFDApp
//
//  Created by 吴春艳 on 2018/6/26.
//  Copyright © 2018年 Kiran. All rights reserved.
//

#import "YKBankCardView.h"
#import "UIView+JobMessage.h"
#import "YKBankListVC.h"
#import "YKVerifyBindingCardModel.h"
#import "YKProtocolView.h"

@interface YKBankCardView ()<UITextFieldDelegate>

@property (nonatomic, strong) UILabel *realNameLab;
@property (nonatomic, strong) UIView *bankRightView;
@property (nonatomic, strong) UITextField *cardNumTf;
@property (nonatomic, strong) UITextField *phoneNumTf;
@property (nonatomic, strong) UITextField *codeTf;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) YKProtocolView *protocolView;
@property (nonatomic, strong) UIButton *aboutDepositBtn;
@property (nonatomic, copy) NSString *selectBankId;
@property (nonatomic, strong) YKVerifyBindingCardModel *entity;

@end

@implementation YKBankCardView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = Color.backgroundColor;
        [self configUI];
    }
    return self;
}

- (void)configUI
{
    WEAK_SELF
    //头部
    UILabel *topLabel = [UILabel yk_labelWithFontSize:13 textColor:Color.color_AD_C5 superView:self masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.offset(10);
    }];
    
    //持卡人
    UIView *nameView = [self configCellWithLeftLabelText:@"持卡人" height:45 topView:topLabel];
    self.realNameLab = [UILabel yk_labelWithFontSize:14 textColor:Color.color_45_C2 superView:nameView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.right.offset(-15);
        make.centerY.offset(0);
        
        label.textAlignment = NSTextAlignmentRight;
        label.text = [YKUserManager sharedUser].realname;
    }];
    
    //选择银行
    UIView *bankView = [self configCellWithLeftLabelText:@"银行" height:45 topView:nameView];
    UIView *bankRightView = [self configRightBtnWithTitle:@"选择银行 " superView:bankView tapSelector:@selector(chooseBankName) haveArrow:YES];
    self.bankRightView = bankRightView;
    
    //卡号
    UIView *bankCardView = [self configCellWithLeftLabelText:@"卡号" height:45 topView:bankView];
    self.cardNumTf = [UITextField yk_textFieldWithFontSize:14 textColor:Color.color_45_C2 placeHolder:@"请输入银行卡号" superView:bankCardView masonrySet:^(UITextField *textfield, MASConstraintMaker *make) {
        make.width.mas_equalTo(200);
        make.right.offset(-15);
        make.top.bottom.offset(0);
        textfield.textAlignment = NSTextAlignmentRight;
        [textfield setValue:Color.color_CC_C8 forKeyPath:@"_placeholderLabel.textColor"];
        textfield.tintColor = Color.main;
    }];
    self.cardNumTf.keyboardType = UIKeyboardTypeNumberPad;
    self.cardNumTf.delegate = self;
    
    //手机号
    UIView *phoneView = [self configCellWithLeftLabelText:@"手机号" height:45 topView:bankCardView];
    self.phoneNumTf = [UITextField yk_textFieldWithFontSize:14 textColor:Color.color_45_C2 placeHolder:@"请输入本卡预留的手机号" superView:phoneView masonrySet:^(UITextField *textfield, MASConstraintMaker *make) {
        make.right.offset(-15);
        make.top.bottom.offset(0);
        make.width.mas_equalTo(200);
        textfield.textAlignment = NSTextAlignmentRight;
        [textfield setValue:Color.color_CC_C8 forKeyPath:@"_placeholderLabel.textColor"];
        textfield.tintColor = Color.main;
    }];
    self.phoneNumTf.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneNumTf.delegate = self;
    
    //验证码
    UIView *codeView = [self configCellWithLeftLabelText:@"验证码" height:45 topView:phoneView];
    UIButton * codeBtn = [UIButton yk_buttonFontSize:15 textColor:Color.main backGroundImage:@"" imageName:@"" cornerRadius:3 superView:codeView masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        STRONG_SELF
        make.right.offset(-15);
        make.centerY.offset(0);
        make.width.offset(75);
        make.height.offset(25);
        button.layer.borderColor = Color.main.CGColor;
        button.layer.borderWidth = 1.0;
        [button setTitle:@"获取" forState:UIControlStateNormal];
        [button addTarget:strongSelf action:@selector(sendCode:) forControlEvents:UIControlEventTouchUpInside];
    }];
    self.codeTf = [UITextField yk_textFieldWithFontSize:14 textColor:Color.color_45_C2 placeHolder:@"输入6位验证码" superView:codeView masonrySet:^(UITextField *textfield, MASConstraintMaker *make) {
        make.right.equalTo(codeBtn.mas_left).offset(-15);
        make.top.bottom.offset(0);
        make.width.mas_equalTo(200);
        textfield.textAlignment = NSTextAlignmentRight;
        [textfield setValue:Color.color_CC_C8 forKeyPath:@"_placeholderLabel.textColor"];
        textfield.tintColor = Color.main;
    }];
    self.codeTf.keyboardType = UIKeyboardTypeNumberPad;
    self.codeTf.delegate = self;
    
    //描述文案
    self.descLabel = [UILabel new];
    self.descLabel.font = [UIFont systemFontOfSize:13];
    self.descLabel.textColor = Color.color_66_C3;
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.right.offset(-10);
        make.top.equalTo(codeView.mas_bottom).offset(20);
    }];
    self.descLabel.hidden = YES;
    
    //保存按钮
    UIButton *saveBtn = [UIButton yk_buttonFontSize:17 textColor:Color.whiteColor backGroundImage:@"btn_bg_image" imageName:@"" cornerRadius:29.0f superView:self masonrySet:^(UIButton *button, MASConstraintMaker *make) {
        STRONG_SELF
        make.top.equalTo(strongSelf.descLabel.mas_bottom).offset(20);
        make.left.offset(35 * ASPECT_RATIO_WIDTH);
        make.right.offset(-35 * ASPECT_RATIO_WIDTH);
        make.height.offset(58);
        make.bottom.offset(0);
        
        [button setTitle:@"确认绑卡" forState:UIControlStateNormal];
        [button addTarget:strongSelf action:@selector(saveData) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    /*协议视图*/
    self.protocolView = [YKProtocolView makeProtocol](self,saveBtn);
    
    //关于存管
    self.aboutDepositBtn = [UIButton new];
    self.aboutDepositBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.aboutDepositBtn addTarget:self action:@selector(aboutDepositTouch) forControlEvents:UIControlEventTouchUpInside];
    NSMutableAttributedString *aboutTitle = [[NSMutableAttributedString alloc] initWithString:@"关于存管"];
    [aboutTitle addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, aboutTitle.length)];
    [aboutTitle addAttribute:NSForegroundColorAttributeName value:Color.main range:NSMakeRange(0, aboutTitle.length)];
    [self.aboutDepositBtn setAttributedTitle:aboutTitle forState:UIControlStateNormal];
    [self addSubview:self.aboutDepositBtn];
    [self.aboutDepositBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        STRONG_SELF
        make.centerX.offset(0);
        make.top.equalTo(strongSelf.protocolView.mas_bottom).offset(70);
    }];
    self.aboutDepositBtn.hidden = YES;
}

- (void)updateUI:(YKVerifyBindingCardModel *)entity
{
    self.entity = entity;
    UILabel * bankLab = (UILabel *)[self.bankRightView viewWithTag:10002];
    self.realNameLab.text = [entity.name yk_isValidString] ? entity.name : self.realNameLab.text;
    if ([entity.is_deposit_open_account intValue] != 1) {
        self.descLabel.hidden = YES;
        self.protocolView.hidden = YES;
        self.aboutDepositBtn.hidden = YES;
    } else {
        self.descLabel.hidden = NO;
        self.protocolView.hidden = NO;
        self.aboutDepositBtn.hidden = NO;
        bankLab.text = entity.bank_name;
        bankLab.textColor = Color.color_45_C2;
        self.cardNumTf.text = [entity.card_no isEqualToString:@"请输入银行卡"]?@"":entity.card_no;
        self.phoneNumTf.text = entity.phone_num;
        self.descLabel.text = [entity.deposit_tips stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
        self.selectBankId = entity.bank_id;
        
        //刷新协议
        [self.protocolView yk_refreshProtocolTextEntity:entity.protocols protocolString:@"protocol_name" url:@"protocol_url" tapAction:^(NSAttributedString *text, NSDictionary *obj, NSUInteger idx) {
            YKBrowseWebController *webView = [[YKBrowseWebController alloc] init];
            NSString *url = [obj[@"protocol_url"] yk_URLEncodedString];
            if (idx==1) {
                //URL需要Encoding,因为在JSON解析后已经被decoding过，因为URL还要重新发送给webView去loadRequest,不能包含中文字符
                url = [url yk_URLEncodedString];
            }
            webView.url = url;
            [self.yk_viewController.navigationController pushViewController:webView animated:YES];
        }];
    }
}

- (BOOL)checkInputStatusWithisCheckCode:(BOOL)isCheckCode {
    
    if (![self.selectBankId yk_isValidString]) {
        [[iToast makeText:@"请选择银行"] show];
        return NO;
    }
    if (![self.cardNumTf.text yk_isValidString]) {
        [[iToast makeText:@"请填写完整卡号"] show];
        return NO;
    }
    if (![self.phoneNumTf.text yk_isValidString]) {
        [[iToast makeText:@"请填写完整手机号"] show];
        return NO;
    }
    if (![self.phoneNumTf.text hasPrefix:@"1"]) {
        [[iToast makeText:@"手机号格式不正确"] show];
        return NO;
    }
    
    if (isCheckCode == YES) {
        if (![self.codeTf.text yk_isValidString]) {
            [[iToast makeText:@"请填写验证码"] show];
            return NO;
        }
        
        if (self.entity.is_deposit_open_account == 0 && !self.protocolView.isSelected) {
            [[iToast makeText:@"请先阅读并授权协议"] show];
            return NO;
        }
    }
    return YES;
}

//点击关于存管
- (void)aboutDepositTouch
{
    YKBrowseWebController *webView = [[YKBrowseWebController alloc] init];
    webView.url = self.entity.about_deposit;
    [self.yk_viewController.navigationController pushViewController:webView animated:YES];
}

#pragma mark -- 选择银行卡
- (void)chooseBankName {
    WEAK_SELF
    YKBankListVC *vc = [[YKBankListVC alloc] init];
    vc.selectBlock = ^(YKBankListEntity *entity)
    {
        STRONG_SELF
        strongSelf.selectBankId = entity.bank_id;
        UILabel * bankNameLab = (UILabel *)[self.bankRightView viewWithTag:10002];
        bankNameLab.textColor = Color.color_45_C2;
        bankNameLab.text = entity.bank_name;
    };
    [self.yk_viewController.navigationController pushViewController:vc animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.cardNumTf) {
        if (range.location > 18) {
            return false;
        }
    }
    if (textField == self.phoneNumTf) {
        if (range.location > 10) {
            return false;
        }
    }
    if (textField == self.codeTf) {
        if (range.location > 5) {
            return false;
        }
    }
    return true;
}

#pragma mark -- 发送验证码
- (void)sendCode:(UIButton *)sender {
    if ([self checkInputStatusWithisCheckCode:NO] == NO) {
        return;
    }
    [CodeSecure md5EncryptWithPhoneNum:self.phoneNumTf.text encryptedCallBack:^(NSString * _Nonnull sign, NSString * _Nonnull random) {
        NSDictionary * parm = @{@"type": @"bind-card",
                                @"bank_id": self.selectBankId,
                                @"card_no": self.cardNumTf.text,
                                @"phone": self.phoneNumTf.text,
                                @"sign": sign,
                                @"random": random};
        [CodeSecure bindCardCodeSendWithParam:parm sendComplete:^(BOOL isSuccess) {
            if (isSuccess) {
                [CodeButton timerCountDownForButton:sender elapsedTime:90 title:@"s" finish:^{
                }];
            }
        }];
    }];
}

#pragma mark -- 保存数据
- (void)saveData {
    [KEY_WINDOW endEditing:YES];
    if ([self checkInputStatusWithisCheckCode:YES] == NO) {
        return;
    }
    NSDictionary * param = @{@"card_no":[self.cardNumTf.text stringByReplacingOccurrencesOfString:@" " withString:@""],
                             @"bank_id":self.selectBankId ? self.selectBankId : @"",
                             @"phone":self.phoneNumTf.text,
                             @"code":self.codeTf.text};
    [[HTTPManager session] postRequestForKey:kCreditCardBindCard showLoading:YES param:param succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        if (code == 0 && success) {
//            if ([json[@"cunguan_bind_card_url"] yk_isValidString]) {
//                YKBrowseWebController * vc = [YKBrowseWebController new];
//                vc.url = json[@"cunguan_bind_card_url"];
//                [self.yk_viewController.navigationController pushViewController:vc animated:YES];
//            } else {
                [[iToast makeText:msg] show];
                [self.yk_viewController.navigationController popViewControllerAnimated:YES];
//            }
            //跳转
            if (self.bankBindComplete) {
                self.bankBindComplete(json[@"card_desc"],json[@"card_id"],json[@"bank_name"]);
            }
        } else if (code != 0){
            [[iToast makeText:msg] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

@end
