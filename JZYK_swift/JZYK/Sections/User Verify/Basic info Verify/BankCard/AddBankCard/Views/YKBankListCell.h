//
//  YKBankListCell.h
//  KDIOSApp
//
//  Created by haoran on 16/5/11.
//  Copyright © 2016年 KDIOSApp. All rights reserved.
//


@interface YKBankListEntity : NSObject
@property (nonatomic, copy) NSString *bank_id;
@property (nonatomic, copy) NSString *bank_name;
@property (nonatomic, copy) NSString *is_supprot_withhold; //是否支持代扣
@property (nonatomic, copy) NSString *bank_logo;
@property (nonatomic, copy) NSString *bank_background;

@end

@interface YKBankListArrayEntity : NSObject
@property (nonatomic, strong) NSArray <YKBankListEntity *>*item;
@end

@interface YKBankListCell : UITableViewCell

- (void)updateTableViewCellWithdata:(YKBankListEntity *)entity index:(NSIndexPath *)indexPath;

@end
