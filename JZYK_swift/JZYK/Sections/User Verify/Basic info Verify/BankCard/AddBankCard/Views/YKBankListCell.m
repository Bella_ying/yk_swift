//
//  YKBankListCell.m
//  KDIOSApp
//
//  Created by haoran on 16/5/11.
//  Copyright © 2016年 KDIOSApp. All rights reserved.
//

#import "YKBankListCell.h"

@implementation YKBankListEntity
@end

@implementation YKBankListArrayEntity
+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"item": [YKBankListEntity class]};
}
@end

@interface YKBankListCell ()
@property (nonatomic, strong) UIImageView *bankIcon;
@property (nonatomic, strong) UILabel *bankName;
@end

@implementation YKBankListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self configUI];
    }
    return self;
}

- (void)configUI
{
    UIImageView * bankIcon = [UIImageView yk_imageViewWithImageName:@"" superView:self.contentView masonrySet:^(UIImageView *imageView, MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(15.f);
        make.size.mas_equalTo(CGSizeMake(25.f, 25.f));
        imageView.contentMode = UIViewContentModeScaleAspectFit;
    }];
    self.bankIcon = bankIcon;
    
    self.bankName = [UILabel yk_labelWithFontSize:13 textColor:Color.color_33_C8 superView:self.contentView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.equalTo(bankIcon.mas_right).offset(35.f);
        make.height.mas_equalTo(47.f);
    }];
    
    UIView *line = [UIView yk_viewWithColor:Color.color_CC_C8 superView:self.contentView masonrySet:^(UIView *view, MASConstraintMaker *make) {
        make.height.mas_equalTo(.5f);
        make.left.right.bottom.mas_equalTo(0);
    }];
    line.hidden = NO;
    
}
- (void)updateTableViewCellWithdata:(YKBankListEntity *)entity index:(NSIndexPath *)indexPath
{
    YKBankListEntity *entitys = entity;
    [_bankIcon sd_setImageWithURL:[NSURL URLWithString:entitys.bank_logo]];
    _bankName.text = entitys.bank_name;
}
@end
