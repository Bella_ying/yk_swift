//
//  YKBankCardView.h
//  KDFDApp
//
//  Created by 闫涛 on 2017/4/7.
//  Copyright © 2017年 cailiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@class YKVerifyBindingCardModel;
@interface YKBankCardView : TPKeyboardAvoidingScrollView

@property (nonatomic, copy) void (^bankBindComplete)(NSString * card_desc, NSString *card_id, NSString *bank_name);

- (void)updateUI:(YKVerifyBindingCardModel *)entity;

@end
