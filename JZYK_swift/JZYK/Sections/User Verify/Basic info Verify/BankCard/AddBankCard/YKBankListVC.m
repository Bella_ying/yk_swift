//
//  YKBankListVC.m
//  KDIOSApp
//
//  Created by haoran on 16/5/10.
//  Copyright © 2016年 KDIOSApp. All rights reserved.
//

#import "YKBankListVC.h"
#import "YKBankListCell.h"

@interface YKBankListVC ()
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray <YKBankListEntity *>*dataArr;

@end

@implementation YKBankListVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchData];
}

- (void)setupUI
{
    [self.view addSubview:self.tableView];
    ADJUST_SCROLL_VIEW_INSET(self, self.tableView);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
    WEAK_SELF
    [[self.tableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            [sectionMaker.hd_dataArr(^() {
                return weakSelf.dataArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClass(HDCellClass(YKBankListCell))
                .hd_rowHeight(47)
                .hd_adapter(^(YKBankListCell *cell, id data, NSIndexPath *indexPath) {
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell updateTableViewCellWithdata:weakSelf.dataArr[indexPath.row] index:indexPath];
                })
                .hd_event(^(UITableView *tableView, NSIndexPath *indexPath, YKBankListEntity *entity) {
                    if (weakSelf.selectBlock) {
                        weakSelf.selectBlock(entity);
                    }
                    if ([entity.is_supprot_withhold integerValue] == 0) {
                        NSString *message = [NSString stringWithFormat:@"由于%@银行卡不支持还款代扣，建议优先选择其他银行卡",entity.bank_name];
                        [[QLAlert alert] showWithMessage:message singleBtnTitle:@"确定" btnClicked:^(NSInteger index) {
                            [weakSelf.navigationController popViewControllerAnimated:YES];
                        }];
                    } else {
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                    }
                });
            }];
        }];
    }] hd_addFreshHeader:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf fetchData];
        });
    }];
}

- (void)fetchData
{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditCardBankList showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        [weakSelf.tableView hd_endFreshing:YES];
        [weakSelf.tableView hd_coverDismiss];
        if (success && code == 0){
            YKBankListArrayEntity * entity = [YKBankListArrayEntity yy_modelWithJSON:json];
            weakSelf.dataArr = entity.item;
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [weakSelf.tableView hd_endFreshing:YES];
        [weakSelf.tableView hd_coverError];
        [weakSelf.tableView yk_coverErrorImage:YK_IMAGE(@"img_wuwifi") content:@"检查网络连接~" buttonTitle:@"点击重试" isShowButton:YES];
        [weakSelf.tableView hd_coverError:^{
            [weakSelf.tableView hd_coverDismiss];
            [weakSelf fetchData];
        }];
    }];
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView= [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

@end
