//
//  YKAddBankCardViewController.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/7.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAddBankCardViewController.h"
#import "YKBankCardView.h"
#import "YKVerifyBindingCardModel.h"

@interface YKAddBankCardViewController ()

@property (nonatomic, retain) YKBankCardView *bankCradView;
@property (nonatomic, strong) YKVerifyBindingCardModel *cunguanModel;

@end

@implementation YKAddBankCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"绑定银行卡";
    
    [self.view addSubview:self.bankCradView];
    [self.bankCradView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.offset(0);
    }];
    [self featchCunguanData];
}

- (void)featchCunguanData {
    WEAK_SELF
    [HTTPManager.session getRequestForKey:kCreditCardGetDepositOpenInfo showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        STRONG_SELF
        if (code == 0 && success) {
            strongSelf.cunguanModel = [YKVerifyBindingCardModel yy_modelWithJSON:json];
            [strongSelf.bankCradView updateUI:strongSelf.cunguanModel];
        } else if(code != 0){
            [[iToast makeText:msg] show];
        }
        
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

- (YKBankCardView *)bankCradView {
    if (!_bankCradView) {
        _bankCradView = [YKBankCardView new];
        WEAK_SELF
        _bankCradView.bankBindComplete = ^(NSString *card_desc, NSString *card_id, NSString *bank_name) {
            STRONG_SELF
            if (strongSelf.bankBindComplete) {
                strongSelf.bankBindComplete(card_desc, card_id, bank_name);
            }
        };
    }
    return _bankCradView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
