//
//  YKContactsViewController.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKContactsViewController.h"
#import "YKAddressBookVC.h"
#import "YKContactUploadManager.h"
#import "YKPersonlContactEntity.h"
#import "JWPickerView.h"
#import "YKRealNameDataManager.h"
#import "YKAccessAlertView.h"

@interface YKContactsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *progressImgV;
@property (weak, nonatomic) IBOutlet UIButton *directPersonBtn;
@property (weak, nonatomic) IBOutlet UIButton *directPhoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *commonPersonBtn;
@property (weak, nonatomic) IBOutlet UIButton *commonPhoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *stepBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBgImgHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *directSectionTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commonTitleLabTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commonSectionTopConstraint;

@property (nonatomic, strong) YKPersonlContactEntity *contactEntity;

@end

@implementation YKContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"添加联系人";
    self.view.backgroundColor = Color.backgroundColor;
    [self creatBackButtonWithAction:@selector(backBtnClick)];
    [self featchContactData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.shouldNext == NO) {
        [self.stepBtn setTitle:@"确 定" forState:UIControlStateNormal];
        self.progressImgV.hidden = YES;
        self.progressBgImgHeightConstraint.constant = 0;
        self.titleLabTopConstraint.constant = 26 * ASPECT_RATIO_WIDTH;
        self.directSectionTopConstraint.constant = 17 * ASPECT_RATIO_WIDTH;
        self.commonTitleLabTopConstraint.constant = 37 * ASPECT_RATIO_WIDTH;
        self.commonSectionTopConstraint.constant = 17 * ASPECT_RATIO_WIDTH;
    }
}

#pragma mark -- 选择关系(直系)
- (IBAction)directPersonBtnClick:(UIButton *)sender {
    NSMutableArray * directPersonNameArr = [NSMutableArray array];
    for (YKContactEntity *model in self.contactEntity.lineal_list) {
        [directPersonNameArr addObject:model.name];
    }
    if (directPersonNameArr.count <= 0) {
        return;
    }
    WEAK_SELF
    [JWPickerView pickerWithToolBarTitle:@"" pickerType:JWPickerViewTypeSingleComponent data:directPersonNameArr cancleComplete:^{
        NSLog(@"cancel");
        
    } confirmComplete:^(id  _Nonnull selectedIndex, id  _Nonnull selectedData) {
        STRONG_SELF
        [strongSelf.directPersonBtn setTitle:selectedData forState:UIControlStateNormal];
        int index = [selectedIndex intValue];
        YKContactEntity * selectModel = strongSelf.contactEntity.lineal_list[index];
        strongSelf.contactEntity.lineal_relation = selectModel.type;
    }];
}

#pragma mark -- 选择关系(其他)
- (IBAction)commonPersonBtnClick:(UIButton *)sender {
    
    NSMutableArray * commonPersonNameArr = [NSMutableArray array];
    for (YKContactEntity *model in self.contactEntity.other_list) {
        [commonPersonNameArr addObject:model.name];
    }
    if (commonPersonNameArr.count <= 0) {
        return;
    }
    WEAK_SELF
    [JWPickerView pickerWithToolBarTitle:@"" pickerType:JWPickerViewTypeSingleComponent data:commonPersonNameArr cancleComplete:^{
        NSLog(@"cancel");
        
    } confirmComplete:^(id  _Nonnull selectedIndex, id  _Nonnull selectedData) {
        STRONG_SELF
        [strongSelf.commonPersonBtn setTitle:selectedData forState:UIControlStateNormal];
        int index = [selectedIndex intValue];
        YKContactEntity * selectModel = strongSelf.contactEntity.other_list[index];
        strongSelf.contactEntity.other_relation = selectModel.type;
        
    }];
}

#pragma mark -- 选择手机号(直系)
- (IBAction)directPhoneBtnClick:(UIButton *)sender {
    [self jumpAddressBookSelectPhoneWithRelationType:@"lineal"];
}
#pragma mark -- 选择手机号(其他)
- (IBAction)commonPhoneBtn:(UIButton *)sender {
    [self jumpAddressBookSelectPhoneWithRelationType:@"other"];
}

- (void)jumpAddressBookSelectPhoneWithRelationType:(NSString *)relationType {
    NSString * removePhone = [relationType isEqualToString:@"lineal"] ? self.contactEntity.other_mobile : self.contactEntity.lineal_mobile;
    if (([YKTools yk_permissionsWithAddressBook] == YKPermissionStatusNotDenied)) {
        [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeContactBook contentText:@""];
        return;
    }
    if (([YKTools yk_permissionsWithAddressBook] == YKPermissionStatusAuthorized)) {
        YKAddressBookVC * vc = [[YKAddressBookVC alloc] initWithRemovePhone:removePhone];
        WEAK_SELF
        vc.selectBlock = ^(YKAddressEntity *entity) {
            if ([entity.phone containsString:@":"]) {
                [weakSelf showContantAlertWithPhones:entity.phone name:entity.name relationType:relationType];
            } else {
                [weakSelf upLoadContactsDataAndUIWithPhone:entity.phone name:entity.name relationType:relationType];
            }
        };
        
        if (vc.titleArr.count == 0 && vc.cellData.count == 0) {
            [[YKContactUploadManager shareContact] yk_checkContactPermissionGrantStatusWithFailToTip:NO];
            [[iToast makeText:@"没有联系人"] show];
        } else {
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

#pragma mark -- 一个联系人有多个手机号弹框选择
- (void)showContantAlertWithPhones:(NSString *)phones name:(NSString *)name relationType:(NSString *)relationType
{
    [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeContactsPhone contentText:phones];
    [YKAccessAlertView sharedAlertManager].selectContantsPhoneBlock = ^(NSString *phone) {
        [self upLoadContactsDataAndUIWithPhone:phone name:name relationType:relationType];
    };
}

#pragma mark -- 选择手机号后 更新本页数据
- (void)upLoadContactsDataAndUIWithPhone:(NSString *)phone name:(NSString *)name relationType:(NSString *)relationType
{
    if ([relationType isEqualToString:@"lineal"]) {
        self.contactEntity.lineal_name   = name;
        self.contactEntity.lineal_mobile = phone;
        [self.directPhoneBtn setTitle:[NSString stringWithFormat:@"%@ - %@",name,phone] forState:UIControlStateNormal];
    }
    if ([relationType isEqualToString:@"other"]) {
        self.contactEntity.other_name    = name;
        self.contactEntity.other_mobile  = phone;
        [self.commonPhoneBtn setTitle:[NSString stringWithFormat:@"%@ - %@",name,phone] forState:UIControlStateNormal];
    }
}

#pragma mark -- 更新UI
- (void)updateUI {
    //进度条
    [self.progressImgV sd_setImageWithURL:[NSURL URLWithString:self.contactEntity.progress_bar_img] placeholderImage:[UIImage imageNamed:@"contacts_progress"]];
    //直系联系人
    if (self.contactEntity.lineal_relation && ![self.contactEntity.lineal_relation isEqualToString:@""]) {
        YKContactEntity *entity = (YKContactEntity *)[YKRealNameDataManager getEntityFromArray:self.contactEntity.lineal_list key:@"type" value:self.contactEntity.lineal_relation];
        [self.directPersonBtn setTitle:entity.name forState:UIControlStateNormal];
    }
    if (self.contactEntity.lineal_mobile && ![self.contactEntity.lineal_mobile isEqualToString:@""]) {
        NSString * text = [NSString stringWithFormat:@"%@ - %@",self.contactEntity.lineal_name,self.contactEntity.lineal_mobile];
        [self.directPhoneBtn setTitle:text forState:UIControlStateNormal];
    }
    
    //其他联系人
    if (self.contactEntity.other_relation && ![self.contactEntity.other_relation isEqualToString:@""]) {
        YKContactEntity *entity = (YKContactEntity *)[YKRealNameDataManager getEntityFromArray:self.contactEntity.other_list key:@"type" value:self.contactEntity.other_relation];
        [self.commonPersonBtn setTitle:entity.name forState:UIControlStateNormal];
    }
    
    if (self.contactEntity.other_mobile && ![self.contactEntity.other_mobile isEqualToString:@""]) {
        NSString * text = [NSString stringWithFormat:@"%@ - %@",self.contactEntity.other_name,self.contactEntity.other_mobile];
        [self.commonPhoneBtn setTitle:text forState:UIControlStateNormal];
    }
}

- (void)featchContactData {
    
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kCreditCardGetContacts showLoading:NO param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg)
     {
         STRONG_SELF
         if (success && code == 0) {
             strongSelf.contactEntity = [YKPersonlContactEntity yy_modelWithJSON:json[@"item"]];
             [strongSelf updateUI];
         }
     } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
         [[iToast makeText:errMsg] show];
     }];
}

#pragma mark -- 下一步、确定
- (IBAction)stepBtnClick:(UIButton *)sender {
    if (![self.contactEntity.lineal_relation yk_isValidString]) {
        [[iToast makeText:@"请选择直系联系人"] show];
        return;
    }
    if (![self.contactEntity.lineal_mobile yk_isValidString]) {
        [[iToast makeText:@"请选择直系亲属的联系方式"] show];
        return;
    }
    if (![self.contactEntity.other_relation yk_isValidString]) {
        [[iToast makeText:@"请选择常用联系人"] show];
        return;
    }
    if (![self.contactEntity.other_mobile yk_isValidString]) {
        [[iToast makeText:@"请选择常用联系人的联系方式"] show];
        return;
    }
    if ([self.contactEntity.lineal_mobile isEqualToString:self.contactEntity.other_mobile]) {
        return [[iToast makeText:@"直系亲属和其他联系人电话号不能一致，请重新选择"] show];
    }
    NSDictionary * param = @{@"type":[NSString stringWithFormat:@"%@",self.contactEntity.lineal_relation],
                             @"mobile":[NSString stringWithFormat:@"%@",self.contactEntity.lineal_mobile],
                             @"name":[NSString stringWithFormat:@"%@",self.contactEntity.lineal_name],
                             @"relation_spare":[NSString stringWithFormat:@"%@",self.contactEntity.other_relation],
                             @"mobile_spare":[NSString stringWithFormat:@"%@",self.contactEntity.other_mobile],
                             @"name_spare":[NSString stringWithFormat:@"%@",self.contactEntity.other_name],
                             };
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kCreditCardSaveContacts showLoading:NO param:param succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg)
     {
         if (code == 0) {
             iToast *toast = [iToast makeText:msg];
             [toast setDuration:0.5];
             [toast setToastDidDisappear_blk:^{
                 STRONG_SELF
                 if (strongSelf.shouldNext) {
                     NSString *url = [YK_NSUSER_DEFAULT objectForKey:@"cellular_auth"];
                     if([url yk_isValidString]){
                         YKBrowseWebController *web = [YKBrowseWebController new];
                         web.url = url;
                         web.popToDesignatedPage = PopToDesignatedPageMine;
                         [strongSelf.navigationController pushViewController:web animated:YES];
                     }else{
                         [[iToast makeText:@"链接不可用"] show];
                     }
                 } else {
                     [strongSelf.navigationController popViewControllerAnimated:YES];
                 }
             }];
             [toast show];
             
         } else {
             [[iToast makeText:msg] show];
         }
         
     } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
         [[iToast makeText:errMsg] show];
     }];
}

- (void)backBtnClick {
    if (self.shouldNext) {
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[NSClassFromString(@"YKConfirmOrderViewController") class]]) {
                [self.navigationController popToViewController:vc animated:YES];
                return;
            }
        }
        [[YKTabBarController yk_shareTabController] yk_setSelectedIndex:YKTabSelectedIndexMine viewController:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
