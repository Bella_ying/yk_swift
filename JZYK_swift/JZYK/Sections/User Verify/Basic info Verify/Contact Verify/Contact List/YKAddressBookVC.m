//
//  YKAddressBookVC.m
//  KDIOSApp
//
//  Created by haoran on 16/5/5.
//  Copyright © 2016年 KDIOSApp. All rights reserved.
//

#import "YKAddressBookVC.h"
#import "YKContactFetcher.h"
#import "YKContactUploadManager.h"

@implementation YKAddressEntity
@end

@interface YKAddressBookVC ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, copy) NSString *removePhone;// 排除已经选择的手机号

//**************************联系人数据相关**********************/
@property (nonatomic, retain) NSDictionary *dataDic;//原始数据 字典格式
@property (nonatomic, retain) NSMutableArray *titleArrCopy;
@property (nonatomic, retain) NSMutableArray *cellDataCopy;

@end

@implementation YKAddressBookVC

- (instancetype)initWithRemovePhone:(NSString *)removePhone{
    self = [super init];
    if (self) {
        self.removePhone = removePhone;
        [self manageData];
    }
    return self;
}

#pragma mark 对通讯录进行初始化
- (void)manageData{
    
    if ([[YKContactFetcher shareFetcher] fetchContactData].count > 0 && [[YKContactFetcher shareFetcher] sortedContactData].count > 0) {
        _dataDic = [[YKContactFetcher shareFetcher] fetchContactData];
        _titleArr = [[YKContactFetcher shareFetcher] sortedContactData];
        _cellData = [NSMutableArray array];
        
        __weak typeof(self)weakself = self;
        [_titleArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *indexString = obj;
            NSInteger index = idx;
            [weakself.cellData addObject:[@[] mutableCopy]];
            NSArray *tempArr = weakself.dataDic[indexString];
            [tempArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString *mobile = obj[@"mobile"];
                if (mobile) {
                    if (!(weakself.removePhone && [mobile isEqualToString:weakself.removePhone])) {
                        [weakself.cellData[index] addObject:obj];
                    }
                }
                
            }];
        }];
    }
    _titleArrCopy = [_titleArr mutableCopy];
    _cellDataCopy = [_cellData mutableCopy];
    
    [[YKContactUploadManager shareContact] yk_uploadContactItem];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"联系人";
    
    [self setupUI];
}
- (void)setupUI
{
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 44)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = @"搜索";
    [self.view addSubview:self.searchBar];
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,44.f, WIDTH_OF_SCREEN, HEIGHT_OF_SCREEN - NAV_HEIGHT - 44.f)];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:self.tableView];
}

#pragma mark - table datasource
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return _titleArrCopy;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _titleArrCopy.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.cellDataCopy[section] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_OF_SCREEN, 30)];
    view.backgroundColor = Color.backgroundColor;
    UILabel *sectionHeardLab = [UILabel yk_labelWithFontSize:13 textColor:Color.color_66_C3 superView:view masonrySet:^(UILabel *label, MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(30);
    }];
    sectionHeardLab.text = _titleArrCopy[section];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *str = @"cell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];

        UILabel *nameLabel = [UILabel yk_labelWithFontSize:13 textColor:Color.color_66_C3 superView:cell.contentView masonrySet:^(UILabel *label, MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.top.mas_equalTo(0);
            make.height.mas_equalTo(45.f);
        }];
        nameLabel.tag = 1000;
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(15, 44.5, WIDTH_OF_SCREEN-15, 0.5)];
        line.backgroundColor = Color.color_E6_C6;
        [cell.contentView addSubview:line];
    }
    
    UILabel *label = (UILabel *)[cell.contentView viewWithTag: 1000];
    if (label&&[label isKindOfClass:[UILabel class]]) {
        label.text = self.cellDataCopy[indexPath.section][indexPath.row][@"name"];
    }
    return cell;
}

#pragma mark - tableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectBlock) {
        NSDictionary *tempDic = self.cellDataCopy[indexPath.section][indexPath.row];
        
        NSString *name = tempDic[@"name"];
        NSString *phone =tempDic[@"mobile"];
        phone = [self phoneNumClear:phone];
        
        YKAddressEntity *entity = [[YKAddressEntity alloc]init];
        entity.name = name;
        entity.phone = phone;
        self.selectBlock(entity);
    }
    [self.navigationController popViewControllerAnimated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - searchBar delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self doSearchWith:searchBar.text];
    self.searchBar.showsCancelButton = YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self doSearchWith:searchBar.text];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}
- (void)doSearchWith:(NSString *)searchText
{
    __weak typeof(self)weakself = self;
    self.titleArrCopy = [self.titleArr mutableCopy];
    self.cellDataCopy = [self.cellData mutableCopy];
    if ([searchText isEqualToString:@""]) {
        self.titleArrCopy = [self.titleArr mutableCopy];
        self.cellDataCopy = [self.cellData mutableCopy];
    }else
    {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableArray *indexArray = [NSMutableArray array];
        
        [self.titleArrCopy enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[c] %@",searchText];
            NSString *title = weakself.titleArrCopy[idx];
            NSArray *tempArr = [weakself.cellDataCopy[idx] filteredArrayUsingPredicate:predicate];
            if (tempArr&&tempArr.count>0) {
                [array addObject:tempArr];
                if (![indexArray containsObject:title]) {
                    [indexArray addObject:title];
                }
            }
        }];
        self.titleArrCopy = indexArray;
        self.cellDataCopy = array;
    }
    [self.tableView reloadData];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.text = @"";
    self.titleArrCopy = [self.titleArr mutableCopy];
    self.cellDataCopy = [self.cellData mutableCopy];
    [self.tableView reloadData];
    [searchBar resignFirstResponder];
}

//过滤手机号
- (NSString *)phoneNumClear:(NSString *)str
{
    str = [str stringByReplacingOccurrencesOfString:@"+86" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"-"   withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" "   withString:@""];   
    str = [str stringByReplacingOccurrencesOfString:@"\r"  withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"\n"  withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"·"   withString:@""];
    str = [[str componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""];
    return str;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
