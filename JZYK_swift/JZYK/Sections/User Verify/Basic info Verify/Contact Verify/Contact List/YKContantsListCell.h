//
//  YKContantsListCell.h
//  KDFDApp
//
//  Created by hongyu on 2017/10/12.
//  Copyright © 2017年 JSQB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKContantsListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *telephoneLabel;

@end
