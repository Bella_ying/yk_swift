//
//  YKAddressBookVC.h
//  KDIOSApp
//
//  Created by haoran on 16/5/5.
//  Copyright © 2016年 KDIOSApp. All rights reserved.
//

#import "YKBaseViewController.h"

@interface YKAddressEntity : NSObject
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString *name;
@end

@interface YKAddressBookVC : YKBaseViewController

- (instancetype)initWithRemovePhone:(NSString *)removePhone;// 排除已经选择的手机号

@property (nonatomic, copy) void (^selectBlock)(YKAddressEntity *entity);

@property (nonatomic, retain) NSArray *titleArr;//索引 arr
@property (nonatomic, retain) NSMutableArray *cellData;//展示cell arr

@end
