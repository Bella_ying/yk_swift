//
//  YKPersonlContactEntity.m
//  KDFDApp
//
//  Created by haoran on 16/9/22.
//  Copyright © 2016年 cailiang. All rights reserved.
//

#import "YKPersonlContactEntity.h"

@implementation YKUploadLiveTimeEntity

@end

@implementation YKContactEntity

@end
@implementation YKPersonlContactEntity

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass
{
    return @{@"lineal_list":[YKContactEntity class],
             @"other_list":[YKContactEntity class],
             @"live_time_type_all":[YKUploadLiveTimeEntity class]
             };
}

@end

