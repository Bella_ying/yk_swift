//
//  YKPersonlContactEntity.h
//  KDFDApp
//
//  Created by haoran on 16/9/22.
//  Copyright © 2016年 cailiang. All rights reserved.
//
//居住时长
@interface YKUploadLiveTimeEntity : NSObject

@property (nonatomic, copy) NSString *live_time_type;
@property (nonatomic, copy) NSString *name;

@end

@interface YKContactEntity : NSObject

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *name;

@end


@interface YKPersonlContactEntity : NSObject

@property (nonatomic, copy) NSString *lineal_relation; //1,2..,对应YKContactEntity.type
@property (nonatomic, copy) NSString *lineal_name;
@property (nonatomic, copy) NSString *lineal_mobile;
@property (nonatomic, copy) NSString *other_relation;
@property (nonatomic, copy) NSString *other_name;
@property (nonatomic, copy) NSString *other_mobile;
@property (nonatomic, strong) NSArray <YKContactEntity *>*lineal_list;
@property (nonatomic, strong) NSArray <YKContactEntity *>*other_list;
@property (nonatomic, strong) NSArray <YKUploadLiveTimeEntity *> *live_time_type_all;
//现居地址
@property (nonatomic,copy) NSString *address_distinct;
//详细地址
@property (nonatomic,copy) NSString *address;
//居住时长
@property (nonatomic, copy) NSString *live_period;
//特殊联系人
@property (nonatomic, copy) NSString *special;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *progress_bar_img;//进度条

@end
