//
//  YKVerifyGuideModel.h
//  JZYK
//
//  Created by 吴春艳 on 2018/6/18.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKVerifyGuideModel : NSObject

@property (nonatomic, copy) NSString *background_image; //认证向导图片
@property (nonatomic, copy) NSString *title; //"认证向导"
@property (nonatomic, assign) NSInteger target_tag; //skipCode，1身份认证，3联系人，5运营商
@property (nonatomic, copy) NSString *target_url;  //下发运营商认证h5地址

@end
