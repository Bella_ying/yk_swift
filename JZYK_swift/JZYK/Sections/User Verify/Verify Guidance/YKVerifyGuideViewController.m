//
//  YKVerifyGuideViewController.m
//  JZYK
//
//  Created by 吴春艳 on 2018/6/8.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKVerifyGuideViewController.h"
#import "YKVerifyGuideModel.h"
#import "YKRealNameViewController.h"
#import "YKContactsViewController.h"
#import "YKAccessAlertView.h"
#import "YKLocationManager.h"

@interface YKVerifyGuideViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *verifGuideImageV;
@property (nonatomic, strong) YKVerifyGuideModel *pageModel;

@end

@implementation YKVerifyGuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"认证向导";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = Color.whiteColor;
    [self fetchData];
}

- (void)updateUI {
    [YK_NSUSER_DEFAULT setObject:self.pageModel.target_url forKey:@"cellular_auth"];
    [self.verifGuideImageV sd_setImageWithURL:[NSURL URLWithString:self.pageModel.background_image] placeholderImage:[UIImage imageNamed:@"verfiyGuide"]];
    
}

- (IBAction)startVerifyBtnClick:(UIButton *)sender {
    
    if (!self.pageModel) {
        return;
    }
    
    if ([YKTools yk_permissionsWithLocation] == YKPermissionStatusNotDenied) {
        [[YKAccessAlertView sharedAlertManager] showAlertType:YKShowAlertTypeLocation contentText:@""];
        return;
    }
    if ([YKTools yk_permissionsWithLocation] == YKPermissionStatusAuthorized) {
        [[YKLocationManager location] yk_enableLocationServiceWithCompletion:nil];
    }
    
    YKBaseViewController *vc = nil;
    switch (self.pageModel.target_tag) {
        case 1:
        {
            vc = [YKRealNameViewController new];
        }
            break;
        case 3:
        {
            vc = [YKContactsViewController new];
        }
            break;
        default:
        {
            vc = [YKBrowseWebController new];
            ((YKBrowseWebController *)vc).url = self.pageModel.target_url;
            ((YKBrowseWebController *)vc).popToDesignatedPage = PopToDesignatedPageMine;
            for (UIViewController *containVC in self.navigationController.viewControllers) {
                if ([containVC isKindOfClass:[NSClassFromString(@"YKConfirmOrderViewController") class]]) {
                    ((YKBrowseWebController *)vc).popToDesignatedPage = PopToDesignatedPageMallComfirmOrder;
                }
            }
        }
        break;

    }
    vc.shouldNext = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)fetchData {
    WEAK_SELF
    [[HTTPManager session] getRequestForKey:kCreditCardGuide showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull json, NSInteger code, BOOL success, NSString *msg) {
        if (code == 0 && success) {
            STRONG_SELF
            strongSelf.pageModel = [YKVerifyGuideModel yy_modelWithJSON:json];
            [strongSelf updateUI];
        } else if(code != 0){
            [[iToast makeText:msg] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
