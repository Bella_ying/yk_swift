//
//  YKAddressListCell.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAddressListCell.h"

@interface YKAddressListCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *choiceAddressBtn;
@property (weak, nonatomic) IBOutlet UIButton *addressEditBtn;
@property (weak, nonatomic) IBOutlet UIButton *addressDeletBtn;

@property (nonatomic, assign) BOOL isDefaultChoose;

@end

@implementation YKAddressListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.choiceAddressBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    self.addressEditBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    self.addressDeletBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    NSString *str1 = @"默认地址";
    CGFloat sizeW1 =  [str1 yk_sizeWithFontSize:13].width;
    [self.choiceAddressBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(sizeW1 + 30);
    }];
    NSString *str2 = @"编辑";
    CGFloat sizeW2 =  [str2 yk_sizeWithFontSize:13].width;
    [self.addressEditBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(sizeW2 + 30);
    }];
    [self.addressDeletBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(sizeW2 + 30);
    }];
    
    [self.choiceAddressBtn setImage:[UIImage imageNamed:@"address_choice_yes"] forState:(UIControlStateSelected)];
    [self.choiceAddressBtn setImage:[UIImage imageNamed:@"address_choice_no"] forState:(UIControlStateNormal)];
    [self.choiceAddressBtn addTarget:self action:@selector(chooseDefaultAddress) forControlEvents:(UIControlEventTouchUpInside)];
    [self.addressEditBtn addTarget:self action:@selector(addressEdit) forControlEvents:UIControlEventTouchUpInside];
    [self.addressDeletBtn addTarget:self action:@selector(addressDelet) forControlEvents:UIControlEventTouchUpInside];
    
    self.addressLabel.numberOfLines = 2;
}

- (void)setAddressModel:(YKAddressListModel *)addressModel
{
    _addressModel = addressModel;
    self.nameLabel.text = addressModel.name;
    self.phoneLabel.text = addressModel.mobile;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@ %@",addressModel.province,addressModel.city,addressModel.county,[addressModel.town yk_isEmpty]?@"":addressModel.town,addressModel.address];
    self.choiceAddressBtn.selected = addressModel.status == 2 ? YES:NO;
    
}

- (void)chooseDefaultAddress{
    [self routerEventWithName:kEventAddressSetDefault userInfo:self.addressModel];
}
- (void)addressEdit{
    [self routerEventWithName:kEventAddressEdit userInfo:self.addressModel];
}
- (void)addressDelet{
    [self routerEventWithName:kEventAddressDelet userInfo:self.addressModel];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
