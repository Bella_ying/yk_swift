//
//  YKAddressListCell.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKAddressListModel.h"

static NSString *kEventAddressEdit = @"kEventAddressEdit";
static NSString *kEventAddressDelet = @"kEventAddressDelet";
static NSString *kEventAddressSetDefault = @"kEventAddressSetDefault";

@interface YKAddressListCell : UITableViewCell

@property (nonatomic, strong) YKAddressListModel *addressModel;

@end
