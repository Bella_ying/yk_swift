//
//  YKAddressListModel.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAddressListModel.h"

@implementation YKAddressListModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"addressID" : @"id",
            };
}

@end
