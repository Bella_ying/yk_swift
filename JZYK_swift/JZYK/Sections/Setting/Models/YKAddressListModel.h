//
//  YKAddressListModel.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/15.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseModel.h"

@interface YKAddressListModel : YKBaseModel

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic,copy) NSString *email;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) NSInteger status; //状态 默认2 普通1 删除0
@property (nonatomic, assign) NSInteger addressID; //对应服务端的id
@property (nonatomic, copy) NSString *zip;

@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *province_id;

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *city_id;

@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *county_id;

@property (nonatomic, copy) NSString *town;
@property (nonatomic, copy) NSString *town_id;




@end
