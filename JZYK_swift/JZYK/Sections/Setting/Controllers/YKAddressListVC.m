//
//  YKAddressListVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKAddressListVC.h"
#import "YKAddressListCell.h"
#import "YKNewAddressVC.h"

@interface YKAddressListVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *tableFootView;
@property (nonatomic, strong) UIButton *addAddressBtn;

@property (nonatomic, strong) NSMutableArray *listArr;
@end

@implementation YKAddressListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"配送地址";
    self.listArr = @[].mutableCopy;
    
    [self prepareUI];
    [self prepareTableView];
    [self fetchAddressListData];
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(id)userInfo
{
    if ([eventName isEqualToString:kEventAddressEdit]) {
        //编辑
        YKAddressListModel *model = userInfo;
        YKNewAddressVC *editVC = [[YKNewAddressVC alloc] init];
        editVC.addressType = YKAddressTypeEdit;
        editVC.editAddressModel = model;
        WEAK_SELF
        editVC.refreshBlock = ^{
            [weakSelf fetchAddressListData];
        };
        [self.navigationController pushViewController:editVC animated:YES];
    }else if ([eventName isEqualToString:kEventAddressDelet]){
        //删除
        YKAddressListModel *model = userInfo;
        [[QLAlert alert] showWithMessage:@"是否确认删除地址" btnTitleArray:@[@"确认",@"取消"] btnClicked:^(NSInteger index) {
            if (index == 0) { //确认
                [self operationAddressRequestWithID:model.addressID status:0];
            }
        }];
    }else if([eventName isEqualToString:kEventAddressSetDefault]){
        //设置默认地址
        YKAddressListModel *model = userInfo;
        [self operationAddressRequestWithID:model.addressID status:2];
    }
}

- (void)fetchAddressListData
{
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kAddressUserAddressList showLoading:YES param:nil succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        if (code == 0) {
            DLog(@"====%@",jsonDict);
            [weakSelf.listArr removeAllObjects]; //移除之前的
            NSArray *dataArr = jsonDict[@"data"];
            for (int i = 0; i < dataArr.count; i++) {
                YKAddressListModel *model = [YKAddressListModel yy_modelWithJSON:dataArr[i]];
                [weakSelf.listArr addObject:model];
            }
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        
    }];

}

- (void)operationAddressRequestWithID:(NSInteger)ID status:(NSInteger)status{
    if (status == 0 && self.listArr.count == 1) {
        [[iToast makeText:@"至少留一个收货地址哦"] show];
        return;
    }
    //id:地址id; status:状态 2设为默认地址 0删除该地址
    NSDictionary *paramDict = @{@"id":@(ID),
                                @"status":@(status)
                                };
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kAddressSetUserAddressStatus showLoading:NO param:paramDict succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        if (code == 0) {
            DLog(@"====%@",jsonDict);
            DLog(@"%@",message);
            [weakSelf fetchAddressListData];
        }else{
            [[iToast makeText:message] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

// 页面渲染
- (void)prepareUI
{
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = GRAY_BACKGROUND_COLOR;
    
    [self.tableFootView addSubview:self.addAddressBtn];
    [self.addAddressBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.tableFootView);
        make.left.right.equalTo(self.tableFootView);
        make.height.mas_equalTo(45);
    }];
    
    [self.addAddressBtn addTarget:self action:@selector(clickAddAddress) forControlEvents:(UIControlEventTouchUpInside)];

}

- (void)clickAddAddress{
    DLog(@"点击添加地址");
    if (self.listArr.count >= 10) {
        [[QLAlert alert] showWithMessage:@"不可添加超过10个收货地址" singleBtnTitle:@"好的"];
        return;
    }
    YKNewAddressVC *addVC = [[YKNewAddressVC alloc] init];
    WEAK_SELF
    addVC.refreshBlock = ^{
        [weakSelf fetchAddressListData];
    };
    [self.navigationController pushViewController:addVC animated:YES];
}

// 列表渲染
- (void)prepareTableView
{
    ADJUST_SCROLL_VIEW_INSET(self, self.tableView);
    WEAK_SELF
    [self.tableView hd_tableViewMaker:^(HDTableViewMaker *tableMaker) {
        tableMaker.hd_tableViewHeaderView(^(){
            return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10)];
        });
        tableMaker.hd_tableViewFooterView(^(){
            return self.tableFootView;
        });
        [tableMaker.hd_sectionCount(1) hd_sectionMaker:^(HDSectionMaker *sectionMaker) {
            
            [sectionMaker.hd_dataArr(^() {
//                return @[@"",@"",@"",@""];
                return self.listArr;
            }) hd_cellMaker:^(HDCellMaker *cellMaker) {
                cellMaker.hd_cellClassXib(HDCellClass(YKAddressListCell))
                .hd_rowHeight(106)
                .hd_adapter(^(YKAddressListCell *cell, id data, NSIndexPath *indexPath) {
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
                    YKAddressListModel *model = self.listArr[indexPath.row];
                    [cell setAddressModel:model];
                });
                
                cellMaker.hd_event(^(UITableView *tableView, NSIndexPath *indexPath, id data) {
                    DLog(@"%ld",indexPath.row);
                    //这里区别对待点击事件
                    if (self.isFromMallConfirmOrder) {
                        if (indexPath.row < self.listArr.count) {
                            YKAddressListModel *model = self.listArr[indexPath.row];
                            if (self.addressListBlock) {
                                self.addressListBlock(model);
                            }
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }
                });
            }];
        }];
    }];
    
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        
    }
    return _tableView;
}

- (UIView *)tableFootView
{
    if (!_tableFootView) {
        _tableFootView = [[UIView alloc] init];
        _tableFootView.frame = CGRectMake(0, 0, WIDTH_OF_SCREEN, 105 * ASPECT_RATIO_WIDTH);
    }
    return _tableFootView;
}
- (UIButton *)addAddressBtn
{
    if (!_addAddressBtn) {
        _addAddressBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        UIImage *image = [UIImage imageNamed:@"add"];
        [_addAddressBtn setImage:image forState:(UIControlStateNormal)];
        [_addAddressBtn setTitle:@"新增地址" forState:(UIControlStateNormal)];
        _addAddressBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_addAddressBtn setTitleColor:MAIN_THEME_COLOR forState:(UIControlStateNormal)];
        _addAddressBtn.backgroundColor = WHITE_COLOR;
        _addAddressBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    }
    return _addAddressBtn;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
