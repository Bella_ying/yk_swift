//
//  YKNewAddressVC.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"
#import "YKAddressListModel.h"
typedef void (^BlockRefreshAction)(void);
typedef NS_ENUM(NSUInteger, YKAddressType) {
    YKAddressTypeNewAdd,
    YKAddressTypeEdit,
};

@interface YKNewAddressVC : YKBaseViewController
@property (nonatomic, assign) YKAddressType addressType;
@property (nonatomic, strong) YKAddressListModel *editAddressModel;
@property (nonatomic, copy) BlockRefreshAction refreshBlock;

@end
