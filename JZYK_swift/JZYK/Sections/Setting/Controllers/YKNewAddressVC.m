//
//  YKNewAddressVC.m
//  JZYK
//
//  Created by wangguanjun on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKNewAddressVC.h"
#import "YKPlaceholderTextView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "YKAddressListModel.h"
#import "YKLocationPicker.h"
#import "YKMallAddress.h"

@interface YKNewAddressVC () <UITextFieldDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentBackView;
@property (weak, nonatomic) IBOutlet UITextField *receivingNameTextF; //收货人
@property (weak, nonatomic) IBOutlet UITextField *phoneNumTextF;
//@property (weak, nonatomic) IBOutlet UITextField *locationTextF; //所在地区
@property (weak, nonatomic) IBOutlet UILabel *choiceLocationLabel;

@property (weak, nonatomic) IBOutlet YKPlaceholderTextView *detailTextView; //详细地址
@property (weak, nonatomic) IBOutlet UISwitch *switchControl;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (nonatomic, strong) YKAddressListModel *addressModel;
@property (nonatomic, assign) NSInteger addressID; //修改收货地址的时候需要


@end

@implementation YKNewAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"新增收货地址";
    
    self.detailTextView.placeholder = @"街道、小区、门牌号等";
    self.detailTextView.placeholderColor = LINE_COLOR;
    [self.detailTextView yk_setOriginX:38 holderWidth:150];
    
    self.receivingNameTextF.delegate = self;
    self.phoneNumTextF.delegate = self;
    self.phoneNumTextF.keyboardType = UIKeyboardTypeNumberPad;
    self.detailTextView.delegate = self;
    self.choiceLocationLabel.userInteractionEnabled = YES;
    UIGestureRecognizer *tapG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickChoiceLocation)];
    [self.choiceLocationLabel addGestureRecognizer:tapG];
    
    self.switchControl.on = NO; //默认为不设为默认
    [self.switchControl setOnTintColor:MAIN_THEME_COLOR];

    self.addressModel = [[YKAddressListModel alloc] init];
}

- (void)clickChoiceLocation{
    [self.contentBackView endEditing:YES];
    [YKLocationPicker shared].hiddenNavView = NO;
    [[YKLocationPicker shared] yk_showInView:self.view];
    [[YKLocationPicker shared] setPickerDidSelect_blk_t:^(NSInteger provinceId, NSInteger cityId, NSInteger districtId, NSInteger townId, NSString *fullAddress) {
        
        DLog(@"wgj_id1:%ld id2:%ld id3:%ld id4:%ld fullAdd:%@",provinceId,cityId,districtId,townId,fullAddress);
        NSArray *nameArr = [fullAddress componentsSeparatedByString:@" "];
        self.choiceLocationLabel.text = fullAddress;
        self.addressModel.province = nameArr[0];
        self.addressModel.city = nameArr[1];
        self.addressModel.county = nameArr[2];
        if (nameArr.count > 3) {
            self.addressModel.town = nameArr[3];
            self.addressModel.town_id = [NSString stringWithFormat:@"%ld",townId];
        }else{
            self.addressModel.town = @"";
            self.addressModel.town_id = 0;
        }
        self.addressModel.province_id = [NSString stringWithFormat:@"%ld",provinceId];
        self.addressModel.city_id = [NSString stringWithFormat:@"%ld",cityId];
        self.addressModel.county_id = [NSString stringWithFormat:@"%ld",districtId];
        
    }];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.addressType == YKAddressTypeEdit) {
        self.navigationItem.title = @"修改收货地址";
        if (self.editAddressModel) {
            self.receivingNameTextF.text = self.editAddressModel.name;
            self.phoneNumTextF.text = self.editAddressModel.mobile;
            self.choiceLocationLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",self.editAddressModel.province,self.editAddressModel.city,self.editAddressModel.county,[NSString stringWithFormat:@"%@",[self.editAddressModel.town yk_isValidString]?self.editAddressModel.town:@""]];
            self.detailTextView.text = self.editAddressModel.address;
            self.switchControl.on = self.editAddressModel.status == 2 ? YES : NO;
            self.addressID = self.editAddressModel.addressID;
        }
    }

}

- (void)setLocationTextFieldAddressView{
    
//    self.locationTextF.tintColor = [UIColor clearColor];
//    self.locationTextF.inputView = self.addressView;
//    WEAK_SELF
//    [self.addressView setCancelBtnClickBlock:^{
//        [weakSelf.locationTextF endEditing:YES];
//    }];
//    [self.addressView setConfirmBtnClickBlock:^(NSArray *selectTitles) {
//        [weakSelf.locationTextF endEditing:YES];
//        __block NSString *provinceName = @"";
//        __block NSString *cityName = @"";
//        __block NSString *countyName = @"";
//        __block NSInteger provinceId = 0;
//        __block NSInteger cityId = 0;
//        __block NSInteger countyId = 0;
//        [selectTitles enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            if (idx == 0) {
//                //取出来的是选中省的对象
//                YKMallProvince *province =(YKMallProvince*)[selectTitles objectAtIndex:0];
//                provinceName = province.name;
//                provinceId = province.Id;
//            }else if (idx == 1){
//                //取出来的是选中省的对象
//                YKMallCity *city =(YKMallCity*)[selectTitles objectAtIndex:1];
//                cityName = city.name;
//                cityId = city.cityId;
//            }else if (idx == 2){
//                //取出来的是选中区的对象
//                YKMallCounty *county =(YKMallCounty*)[selectTitles objectAtIndex:2];
//                countyName = county.name;
//                countyId = county.countyId;
//            }
//        }];
//        
//        NSString *addressStr = [NSString stringWithFormat:@"%@ %@ %@ ",provinceName,cityName,countyName];
//        DLog(@"add:%@",addressStr);
//        weakSelf.locationTextF.text = addressStr;
//        
//        weakSelf.addressModel.province = provinceName;
//        weakSelf.addressModel.city = cityName;
//        weakSelf.addressModel.county = countyName;
//        weakSelf.addressModel.province_id = provinceId;
//        weakSelf.addressModel.city_id = cityId;
//        weakSelf.addressModel.county_id = countyId;
//        
//        //        weakSelf.detailModel.addressStr = addressStr;
//        //        if (weakSelf.customViewBlock) {
//        //            weakSelf.customViewBlock();
//    }];
}

- (IBAction)saveAction:(id)sender {
    if (self.receivingNameTextF.text.length <= 0) {
//        iToast *toast = [iToast makeText:@"请输入名字"];
//        [toast setDuration:0.2];
//        [toast show];
        [[iToast makeText:@"请输入收货人"] show];
        return;
    }else if (self.phoneNumTextF.text.length < 11){
        [[iToast makeText:@"请输入正确手机号"] show];
        return;
    }else if (self.choiceLocationLabel.text.length <= 0 || [self.choiceLocationLabel.text isEqualToString:@"请选择"]){
        [[iToast makeText:@"请选择所在地区"] show];
        return;
    }else if (self.detailTextView.text.length <= 0){
        [[iToast makeText:@"请输入详细地址"] show];
        return;
    }
    if (self.addressType == YKAddressTypeEdit) {
        //修改收货地址
        [self updateAddress];
    }else{
        [self addAddress];
    }
    
}

- (void)addAddress
{
    //状态 默认2 普通1
    NSInteger statusNum = self.switchControl.isOn ? 2 : 1;
    NSDictionary *paramDic = @{@"name":self.receivingNameTextF.text?:@"",
                               @"mobile":self.phoneNumTextF.text?:@"",
                               @"province":self.addressModel.province?:@"",
                               @"province_id":self.addressModel.province_id?:@"",
                               @"city":self.addressModel.city?:@"",
                               @"city_id":self.addressModel.city_id?:@"",
                               @"county":self.addressModel.county?:@"",
                               @"county_id":self.addressModel.county_id?:@"",
                               @"town":self.addressModel.town?:@"",
                               @"town_id":self.addressModel.town_id?:@"",
                               @"address":self.detailTextView.text?:@"",
                               @"status":@(statusNum)
                               };
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kAddressAddUserAddress showLoading:YES param:paramDic succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        if (code == 0) {
            DLog(@"====%@",jsonDict);
            if (strongSelf.refreshBlock) {
                strongSelf.refreshBlock();
            }
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [[iToast makeText:message] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

- (void)updateAddress{
    //self.editAddressModel.province,self.editAddressModel.city,self.editAddressModel.county
    //状态 默认2 普通1
    NSInteger statusNum = self.switchControl.isOn ? 2 : 1;
    NSString *townNameStr = @"";
    NSString *town_id = @"";
    if ([self.addressModel.town yk_isValidString]) {
        townNameStr = [self.addressModel.town yk_isValidString]?self.addressModel.town:@"";
        town_id = [self.addressModel.town_id yk_isValidString]?self.addressModel.town_id:@"";
    }else{
        if ([self.addressModel.province yk_isValidString]) {
            townNameStr = @"";
            town_id = @"";
        }else{
            townNameStr = [self.editAddressModel.town yk_isValidString]?self.editAddressModel.town:@"";
            town_id = [self.editAddressModel.town_id yk_isValidString]?self.editAddressModel.town_id:@"";
        }
    }
    NSDictionary *paramDic = @{@"id":@(self.addressID),
                               @"name":self.receivingNameTextF.text?:@"",
                               @"mobile":self.phoneNumTextF.text?:@"",
                               @"province":self.addressModel.province?:self.editAddressModel.province,
                               @"province_id":self.addressModel.province_id?:self.editAddressModel.province_id,
                               @"city":self.addressModel.city?:self.editAddressModel.city,
                               @"city_id":self.addressModel.city_id?:self.editAddressModel.city_id,
                               @"county":self.addressModel.county?:self.editAddressModel.county,
                               @"county_id":self.addressModel.county_id?:self.editAddressModel.county_id,
                               @"town":townNameStr,
                               @"town_id":town_id,
                               @"address":self.detailTextView.text,
                               @"status":@(statusNum)
                               };
    WEAK_SELF
    [[HTTPManager session] postRequestForKey:kAddressUpdateUserAddress showLoading:YES param:paramDic succeed:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSInteger code, BOOL success, NSString *message) {
        STRONG_SELF
        if (code == 0) {
            DLog(@"====%@",jsonDict);
            if (strongSelf.refreshBlock) {
                strongSelf.refreshBlock();
            }
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [[iToast makeText:message] show];
        }
    } failure:^(NSString * _Nonnull errMsg, BOOL isConnect) {
        [[iToast makeText:errMsg] show];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
    NSMutableString *resultStr = [NSMutableString stringWithString:textField.text];
    [resultStr replaceCharactersInRange:range withString:string];
    if (textField == self.phoneNumTextF) {
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        if (resultStr.length <= 11) {
            return YES;
        }else{
            return NO;
        }
    }

    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSMutableString *resultStr = [NSMutableString stringWithString:textView.text];
    [resultStr replaceCharactersInRange:range withString:text];
    if (textView == self.detailTextView) {

        if (resultStr.length <= 25) {
            return YES;
        }else{
            return NO;
        }
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
