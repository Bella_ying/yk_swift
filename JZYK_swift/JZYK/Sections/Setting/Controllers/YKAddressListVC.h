//
//  YKAddressListVC.h
//  JZYK
//
//  Created by wangguanjun on 2018/6/14.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKBaseViewController.h"
@class YKAddressListModel;
typedef void(^ykAddressListBlock)(YKAddressListModel *model);

@interface YKAddressListVC : YKBaseViewController
@property (nonatomic, assign) BOOL isFromMallConfirmOrder; //判断是否是从商城提交订单跳转进来
@property (nonatomic,copy) ykAddressListBlock addressListBlock;

@end
