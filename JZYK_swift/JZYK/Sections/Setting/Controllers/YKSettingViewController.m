//
//  YKSettingViewController.m
//  JZYK
//
//  Created by hongyu on 2018/6/13.
//  Copyright © 2018年 Jeremy Wang. All rights reserved.
//

#import "YKSettingViewController.h"
#import "YKAddressListVC.h"
#import "QBGestureViewController.h"
#import "QBGestureTool.h"
#import "YKTouchIDRecognitionManager.h"
#import "YKTouchIDTool.h"
@interface YKSettingViewController () <QBGestureViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subHeight;
@property (weak, nonatomic) IBOutlet UIScrollView       *subView;
@property (weak, nonatomic) IBOutlet UISwitch           *gestureSwitch;
@property (weak, nonatomic) IBOutlet UILabel            *versionLabel;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *modifyTransactionCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *identityButton;
@property (weak, nonatomic) IBOutlet UISwitch *identitySwitch;
@property (weak, nonatomic) IBOutlet UIView *faceIDView;

@end

@implementation YKSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareUI];
}

- (void)biometicVerify
{
    DeviceSecurityType type = [YKDeviceSecurity isDeviceSupportType];
    switch (type) {
        case DeviceSecurityTypeFaceID:
            [self.identityButton setTitle:@"面容ID" forState:UIControlStateNormal];
            break;
        case DeviceSecurityTypeTouchID:
            [self.identityButton setTitle:@"指纹解锁" forState:UIControlStateNormal];
            break;
        case DeviceSecurityTypeNone:
            [self.faceIDView removeFromSuperview];
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self biometicVerify];
    [self.faceIDView removeFromSuperview];
    
    
    if ([[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:kUserLoginCacheKey]) {
        YKUserModel *user = [[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:kUserLoginCacheKey];
        if (user.real_pay_pwd_status) {
            [self.modifyTransactionCodeBtn setTitle:@"修改交易密码" forState:UIControlStateNormal];
        }else {
            [self.modifyTransactionCodeBtn setTitle:@"设置交易密码" forState:UIControlStateNormal];
        }
    }
    
    self.gestureSwitch.on = [QBGestureTool isGestureEnableByUser];
    self.identitySwitch.on = [YKTouchIDTool yk_isTouchIDEnableByUser];
}

- (void)prepareUI
{
    ADJUST_SCROLL_VIEW_INSET(self, self.subView);
    self.subHeight.constant   = HEIGHT_OF_SCREEN - NAV_HEIGHT + 1;
    self.navigationItem.title = @"设置";
    //版本号label 添加手势
    self.versionLabel.text    = [NSString stringWithFormat:@"V%@",[DeviceInfo appVersion]];
    self.versionLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *versionLabelTap  = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(versionLabelTapAction:)];
    [self.versionLabel addGestureRecognizer:versionLabelTap];
    //图片有文字
    [self.logoutButton setImage:[UIImage imageNamed:@"setting_exit_icon"] forState:UIControlStateNormal];
}

- (IBAction)logout:(UIButton *)sender {
    if ([[YKUserManager sharedUser] yk_isLogin]) {
        //埋点（设置——退出登录）
        [YKClickManager yk_reportWithKey:@"setting" array:@[@"退出登录"]];
        [[QLAlert alert] showWithTitle:@"" message:@"您确定要退出登录吗？" btnTitleArray:@[@"确定",@"取消"] btnClicked:^(NSInteger index) {
            if (index == 0) {
                [[YKUserManager sharedUser] yk_loginOut];
            }
        }];
    }else{
        [[iToast makeText:@"您还没有登录，无法进行退出登录操作"] show];
    }
}

//修改登录密码
- (IBAction)reviseLoginPassWordAction:(id)sender
{
    ModifyPwdController *modifyLoginVC = [ModifyPwdController new];
    [self.navigationController pushViewController:modifyLoginVC animated:YES];
}

//修改交易密码
- (IBAction)reviseBargainPassWordAction:(id)sender
{
    //埋点（设置——修改交易密码）
    [YKClickManager yk_reportWithKey:@"setting" array:@[@"修改交易密码"]];
    TransactionCodeController *transacVc = [TransactionCodeController new];
    if ([[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:kUserLoginCacheKey]) {
        YKUserModel *user = [[YKCacheManager sharedCacheManager] yk_getCacheObjectForKey:kUserLoginCacheKey];
        if (!user.real_pay_pwd_status) {
            if (!user.real_verify_status) {
                [[QLAlert alert] showWithTitle:@"" message: @"亲，请先填写个人信息哦～" btnTitleArray:@[@"确定"] btnClicked:^(NSInteger index) {

                }];
                return;
            }
            //1是设置密码， 0是修改密码;
            transacVc.typeNum = 1;
        }else {
            transacVc.typeNum = 0;
        }
    }
    [self.navigationController pushViewController:transacVc animated:YES];
}

- (IBAction)gestureAction:(UISwitch *)sender
{
    sender.on = !sender.on;
    if (sender.on) {
        if (![QBGestureTool isGestureEnableByUser] && [YKUserManager sharedUser].yk_isLogin) {
            QBGestureViewController *gestureVC = [[QBGestureViewController alloc]init];
            gestureVC.delegate = self;
            [gestureVC showInViewController:self type:QBGestureTypeSetting];
            [QBGestureTool saveGestureEnableByUser:NO];
        }
    }else{
        NSString *msg = [NSString stringWithFormat:@"如果关闭手势解锁，%@也会一同关闭",IS_iPhoneX ? @"面容ID" : @"指纹解锁"];
//        [[QLAlert alert] showWithMessage:msg btnTitleArray:@[@"确定",@"取消"] btnClicked:^(NSInteger index) {
//            if (index == 0) {
//
//            } else {
//
//            }
//        }];
        if ([QBGestureTool isGestureEnableByUser]) {
            [YKTouchIDTool yk_saveTouchIDByUser:NO];
            QBGestureViewController *gestureVC = [[QBGestureViewController alloc]init];
            gestureVC.delegate = self;
            [gestureVC showInViewController:self type:QBGestureTypeVerifying];
            [QBGestureTool saveGestureEnableByUser:YES];
        }
    }
}

//人脸识别或指纹
- (IBAction)identitySwitchAction:(UISwitch *)sender {
    UISwitch *touchIDSwitch = (UISwitch *)sender;
    if (touchIDSwitch.on ) {
        [[YKTouchIDRecognitionManager shareYKTouchIDRecognitionManager] setTouchIDBlock:^(NSDictionary *callBackDic) {
            DLog(@"%@",callBackDic);
            if ([callBackDic[@"code"] integerValue] == 4 ) {
                //设备支持指纹解锁
                [[QLAlert alert] showWithMessage:IS_iPhoneX ? @"开启面容ID，解锁优先使用面容ID" : @"开启指纹解锁，解锁优先使用指纹解锁" btnTitleArray:@[@"确定",@"取消"] btnClicked:^(NSInteger index) {
                    if (index == 0) {
                       [YKTouchIDTool yk_saveTouchIDByUser:YES];
                    } else {
                        touchIDSwitch.on = NO;
                        [YKTouchIDTool yk_saveTouchIDByUser:NO];
                    }
                }];
            }else{ //设备没有开启faceID
                touchIDSwitch.on = NO;
                [YKTouchIDTool yk_saveTouchIDByUser:NO];
                [[QLAlert alert] showWithMessage:callBackDic[@"message"] singleBtnTitle:@"确定"];
            }
        }];
        //指纹
        [[YKTouchIDRecognitionManager shareYKTouchIDRecognitionManager] yk_checkTouchIDPrint];
    }else{
        [YKTouchIDTool yk_saveTouchIDByUser:NO];
    }
}

- (IBAction)addressAction:(id)sender
{
    YKAddressListVC *addressVC = [[YKAddressListVC alloc] init];
    [self.navigationController pushViewController:addressVC animated:YES];
}

- (IBAction)aboutUsAction:(id)sender
{
    [self pushToAboutUsVc];
}

#pragma mark- 关于我们标签跳转
- (void)versionLabelTapAction:(UITapGestureRecognizer *)tap
{
    [self pushToAboutUsVc];
}

#pragma mark- 跳转关于我们H5
- (void)pushToAboutUsVc
{
    NSString *urlStr = [[ConfigManager config] configForKey:kAbout_url];
    YKBrowseWebController *aboutUs = [[YKBrowseWebController alloc] init];
    aboutUs.url = urlStr;
    [self.navigationController pushViewController:aboutUs animated:YES];
}


#pragma mark - <QBGestureViewDelegate>
- (void)gestureView:(QBGestureViewController *)vc didSetted:(NSString *)psw {
    DLog(@"====%@",psw);
    if (psw) {
        [self.gestureSwitch setOn:YES];
        [YKTouchIDTool yk_saveTouchIDByUser:YES];
    }else{
        [self.gestureSwitch setOn:NO];
        [YKTouchIDTool yk_saveTouchIDByUser:NO];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    [QBGestureTool saveGesturePsw:psw];
    [QBGestureTool saveGestureEnableByUser:YES];
}
- (void)gestureViewVerifiedSuccess:(QBGestureViewController *)vc
{
    [self.gestureSwitch setOn:NO];
    [QBGestureTool deleteGesturePsw];
    [QBGestureTool saveGestureEnableByUser:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
