//
//  TransactionCodeController.swift
//  JZYK
//
//  Created by Jeremy on 2018/6/15.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

enum TransactionCodeType: Int {
    case modify
    case set
}

@objcMembers class TransactionCodeController: YKBaseViewController {
    @IBOutlet weak var codeView: UIView!
    lazy var transactionCodeView: TransactionCodeView = {
        let transactionCodeView = TransactionCodeView(frame: CGRect(x: 0, y: 0, width: WIDTH_OF_SCREEN-78, height: 50));
        return transactionCodeView;
    }()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var tipsLabel: UILabel!
    @IBOutlet weak var forgetButton: UIButton!
    
    @objc public var transactionSetSuccess: ((String)->Void)?
    @objc public var isShowAlert: Bool = true;
    
    private var oldText: String = "";
    
    var type: TransactionCodeType {
        get {
            if typeNum == 0 {
                return .modify
            }
            return .set;
        }
        
    }
    var typeNum: Int = 1;  //1是设置， 0是修改
    
    var num = 5;
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        switch type {
        case .modify:
            titleLabel.text = "修改交易密码";
            descLabel.text = "请输入6位交易密码";
            forgetButton.isHidden = false;
        case .set:
            titleLabel.text = "设置交易密码";
            descLabel.text = "请设置6位交易密码";
            forgetButton.isHidden = true;
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        transactionCodeView.inputEndCallBack = {[weak self] (text) in
            if (self?.oldText.isEmpty)! {
                self?.clearInputArea(text)
            } else {
                self?.setOrModify(text);
            }
        }
        codeView.addSubview(transactionCodeView)
    }
    
    func clearInputArea(_ text: String){
        transactionCodeView.clearAllInputContent()
        titleLabel.text = text.isEmpty ? "设置交易密码" : "确认交易密码";
        oldText = text;
    }
    
    func setOrModify(_ text: String) {
        switch type {
        case .modify:
            let resetVc = ResetTransactionCodeController();
            resetVc.pageType = .reset
            resetVc.oldPwdText = text;
            self.navigationController?.pushViewController(resetVc, animated: true);
        case .set:
            self.modifyRequest(pwd: text)
        }
    }
    
    @IBAction func forgetLoginPwd(_ sender: UIButton) {
        self.findTransactionCode { [weak self] (controller) in
             self?.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func modifyRequest(pwd: String) {
        if self.oldText != pwd {
            QLAlert().show(withMessage: "两次密码输入不匹配", btnTitleArray: ["知道了"], btnClicked: { (_) in
                self.clearInputArea("")
            });
            
            return;
        }
        
        let param: Dictionary<String, Any> = ["password":pwd]
        HTTPManager.session.postRequest(forKey: kUserSetPaypassword, param: param, succeed: { (json, code, unwrap, msg) in
            if code == 0 {
                if self.isShowAlert {
                    QLAlert().show(withMessage: msg, btnTitleArray: ["确定"], btnClicked: { (_) in
                        var isContain = false
                        for vc in (self.navigationController?.viewControllers)! {
                            if(vc.isKind(of: YKLoanApplyViewController.self)) {
                                isContain = true
                                self.navigationController?.popToViewController(vc, animated: true)
                            }
                        }
                        if (isContain == false) {
                            self.navigationController?.popToRootViewController(animated: true);
                        }
                    })
                }
                self.transactionSetSuccess?(pwd);
                //更新交易密码状态
                YKUserManager.sharedUser().yk_updatePayPassWordStatus(true);
            } else {
                iToast.makeText(msg).show()
            }
        }) { (errMsg, isConnect) in
            
        }
    }
}
