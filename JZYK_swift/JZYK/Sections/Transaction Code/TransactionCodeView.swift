//
//  TransactionCodeView.swift
//  JZYK
//
//  Created by Jeremy on 2018/6/15.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

@objcMembers class TransactionCodeView: UIView {
    
    let passwordCount: Int = 6 //输入密码是几位
    let space: CGFloat = 16.0
    private var codeViewFrame : CGRect?
    //输入完成的回调
    @objc public var inputEndCallBack: ((String)->Void)?
    
    @objc public var textFiledString : String {
        get{
            return self.textField.text!
        }
    }
    
    override var frame : CGRect {
        get{
            return codeViewFrame!
        }
        set{
            codeViewFrame = newValue
            super.frame = codeViewFrame!
            self.setupUI(codeViewFrame!)
        }
    }
    
    lazy var textField: YKTextfield = {
        let tempTextField: YKTextfield = YKTextfield()
        tempTextField.backgroundColor = UIColor.white
        tempTextField.layer.masksToBounds = true
        tempTextField.layer.borderColor = Color.color_AD_C5.cgColor
        tempTextField.layer.borderWidth = 0.5
        //        tempTextField.layer.cornerRadius = space * 0.5  //整体的圆角设置
        tempTextField.isSecureTextEntry = true
        tempTextField.delegate = self
        tempTextField.tintColor = UIColor.clear
        tempTextField.textColor = UIColor.clear
        tempTextField.font = UIFont.systemFont(ofSize: 24)
        tempTextField.keyboardType = .numberPad
        tempTextField.becomeFirstResponder()
        tempTextField.addTarget(self, action: #selector(textFiledEdingChanged), for:.editingChanged )
        return tempTextField
    }()
    
    @objc private func textFiledEdingChanged() {
        let length : NSInteger = (self.textField.text?.count)!
        var i: Int = 0
        while i < passwordCount {
            let dotLabel : UILabel? = (self.viewWithTag(i + 1) as! UILabel)
            if(dotLabel != nil){
                self.bringSubview(toFront: dotLabel!)
                dotLabel!.isHidden = (length <= i)
            }
            i += 1
        }
        self.textField.sendActions(for: UIControlEvents.valueChanged)
        if self.textField.text?.count == passwordCount {
            guard let text = self.textField.text else { return }
            self.inputEndCallBack?(text);
        }
    }
    
    @objc public func clearAllInputContent() {
        self.textField.text = "";
        for idx in 0..<6 {
            let dotLabel : UILabel? = (self.viewWithTag(idx + 1) as! UILabel)
            if(dotLabel != nil){
                dotLabel?.isHidden = true;
            }
        }
    }
    
    private func setupUI(_ frame: CGRect) {
        
        self.addSubview(self.textField)
        self.textField.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        let perWidth: CGFloat = (frame.size.width - (CGFloat)(passwordCount) + 1) / CGFloat(passwordCount)
        
        var idx: Int = 0
        let lineWidth: CGFloat = 0.5;
        while idx < passwordCount { //6
            if (idx < passwordCount - 1){
                let verticalLineView: UIView = UIView()
                verticalLineView.backgroundColor = Color.color_AD_C5
                let v_x = frame.size.width/CGFloat(passwordCount) * CGFloat(idx+1) + lineWidth;
                verticalLineView.frame = CGRect(x: v_x,
                                         y: 0,
                                         width: lineWidth,
                                         height: frame.height)
                self.addSubview(verticalLineView)
            }
            
            var dotLabel: UILabel? = self.viewWithTag(idx+1) as? UILabel
            if (dotLabel == nil){
                dotLabel = UILabel()
                dotLabel!.tag = idx + 1
                self.addSubview(dotLabel!)
            }
            dotLabel?.frame = CGRect(x: (perWidth + 1) * CGFloat (idx) + (perWidth - space) * 0.5,
                                     y: (frame.size.height - space) * 0.5,
                                     width: space,
                                     height: space)
            dotLabel?.layer.masksToBounds = true
            dotLabel?.layer.cornerRadius = space * 0.5  //圆球半径
            dotLabel?.backgroundColor = UIColor.black
            dotLabel?.isHidden = true
            idx += 1
        }
    }
    

    
}

extension TransactionCodeView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location >= passwordCount){
            return false
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
}
