//
//  ResetTransactionCodeController.swift
//  JZYK
//
//  Created by Jeremy on 2018/6/19.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

enum ChangeType {
    case reset
    case find
}

class ResetTransactionCodeController: YKBaseViewController {
    @IBOutlet weak var newPwd: UIView!
    @IBOutlet weak var oldPwd: UIView!
    private lazy var newPwdInput = InputView.input;
    private lazy var oldPwdInput = InputView.input;
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nextButton: Button!
    public var pageType: ChangeType?
    public var oldPwdText: String?
    public var smsCodeText: String = ""
    public var phoneNumText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let type = pageType else { return  }
        switch type {
        case .find:
            titleLabel.text = "找回交易密码"
        case .reset:
            titleLabel.text = "重置交易密码"
        }
        self.nextButton.switchButtonStatus(false);
        setupInputZone()
        
        nextButton.onClick = {[weak self] (button) in
            self?.remoteResetPwd();
        }
    }
}

extension ResetTransactionCodeController {
    func setupInputZone() {
        newPwdInput.inputType = InputType.code
        newPwdInput.textField.placeholder = "请输入新密码";
        newPwdInput.codeButton.removeFromSuperview()
        newPwdInput.secureButton.removeFromSuperview();
        newPwd.addSubview(newPwdInput)
        
        oldPwdInput.inputType = InputType.code
        oldPwdInput.textField.placeholder = "请确认新密码";
        oldPwdInput.codeButton.removeFromSuperview()
        oldPwdInput.secureButton.removeFromSuperview();
        oldPwd.addSubview(oldPwdInput)
        changeControllerButton(nextButton, firstInput: newPwdInput, secondInput: oldPwdInput)
    }
    
    func remoteResetPwd() {
        guard let type = pageType else { return  }
        switch type {
        case .find:
            self.findTransactionRequest()
        case .reset:
            self.restTransactionPwdRequest()
        }
    }
    
    func findTransactionRequest() {
        if newPwdInput.textField.text != oldPwdInput.textField.text {
            iToast.makeText("您两次输入的密码不统一，请检查输入").show()
            return
        }
        
        guard let newPwd = newPwdInput.textField.text else {
            return
        }
        //kUserChangePaypassword 修改交易密码
        let param = ["code": smsCodeText,
                     "password": newPwd,
                     "phone": phoneNumText];
        HTTPManager.session.postRequest(forKey: kUserResetPayPassword, param: param, succeed: { (json, code, unwrap, msg) in
            if code == 0 && unwrap {
                QLAlert().show(withMessage: msg, btnTitleArray: ["确定"], btnClicked: { (_) in
                    var isContain = false
                    for vc in (self.navigationController?.viewControllers)! {
                        if(vc.isKind(of: YKLoanApplyViewController.self)) {
                            isContain = true
                            self.navigationController?.popToViewController(vc, animated: true)
                        }
                        if(vc.isKind(of: YKConfirmOrderViewController.self)) {
                            isContain = true
                            self.navigationController?.popToViewController(vc, animated: true)
                        }
                    }
                    if (isContain == false) {
                        self.navigationController?.popToRootViewController(animated: true);
                    }
                })
            } else {
                iToast.makeText(msg).show()
            }
        }) { (errMsg, isConnect) in
            
        }
    }
    
    func restTransactionPwdRequest() {
        if newPwdInput.textField.text != oldPwdInput.textField.text {
            iToast.makeText("您两次输入的密码不统一，请检查输入").show()
            return
        }
        
        guard let newPwd = newPwdInput.textField.text else {
            return
        }
        guard let oldPwd = oldPwdText else { return }
        //kUserChangePaypassword 修改交易密码
        let param = ["new_pwd": newPwd,
                     "old_pwd": oldPwd];
        HTTPManager.session.postRequest(forKey: kUserChangePaypassword, param: param, succeed: { (json, code, unwrap, msg) in
            if code == 0 && unwrap {
                QLAlert().show(withMessage: msg, btnTitleArray: ["确定"], btnClicked: { (_) in
                    YKTabBarController.yk_shareTab().yk_pop(toViewController: "YKSettingViewController")
                })
            } else {
                iToast.makeText(msg).show()
            }
        }) { (errMsg, isConnect) in
            
        }
    }
}
