//
//  TransactionCodeFindController.swift
//  JZYK
//
//  Created by Jeremy on 2018/6/15.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class TransactionCodeFindController: YKBaseViewController {
    
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var IDView: UIView!
    @IBOutlet weak var codeView: UIView!
    
    @IBOutlet weak var nextButton: Button!
    
    @IBOutlet weak var containerView: UIView!
    private lazy var phoneInputView = InputView.input
    private lazy var nameInputView = InputView.input
    private lazy var idInputView = InputView.input
    private lazy var smsCodeInputView = InputView.input
    
//    var phoneNum: String?
    @objc public var phoneText: String?
    public var smsCodeText: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideNavigationBarLine()
        self.view.backgroundColor = UIColor.white
        self.setupInputZone()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        self.containerView.addGestureRecognizer(tap)
        self.nextButton.onClick = {[weak self] (button) in
            self?.sendRequestForNextPage()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        if let user = YKCacheManager.shared().yk_getCacheObject(forKey: kUserLoginCacheKey) as? YKUserModel {
            phoneInputView.textField.text = user.username.phoneSecurityNum;
        } else {
            phoneInputView.textField.text = self.phoneText?.phoneSecurityNum
        }
//        CodeButton.timerCountDown(forButton: smsCodeInputView.codeButton, title: "秒") { }
        self.nextButton.switchButtonStatus(false);
    }
    
    @objc func tapAction() {
        self.containerView.endEditing(true)
    }
    
}

extension TransactionCodeFindController {
    func setupInputZone() {
        
        self.phoneInputView.removeAllButtons()
        phoneInputView.textField.placeholder = ""
        phoneInputView.isUserInteractionEnabled = false
        phoneView.addSubview(phoneInputView)
        
        
        nameInputView.inputType = InputType.name
        nameInputView.removeAllButtons()
        nameView.addSubview(nameInputView)
        
        
        idInputView.inputType = InputType.IDNum
        idInputView.removeAllButtons()
        IDView.addSubview(idInputView)
        
        
        smsCodeInputView.inputType = InputType.code
        smsCodeInputView.secureButton.removeFromSuperview()
        smsCodeInputView.codeButton.codeType = .resetTransaction
        smsCodeInputView.codeButton.codeAction = { [weak self] button in
            return self?.phoneText
        }
        codeView.addSubview(smsCodeInputView)
        
        smsCodeInputView.textFieldDidChangeCallBack = {[weak self] (tf) in
            guard let count = tf.text?.count else { return }
            if count >= 6 {
                self?.nextButton.switchButtonStatus(true)
            } else {
                self?.nextButton.switchButtonStatus(false)
            }
        }
    }
    
   
    
    func sendRequestForNextPage() {
        guard let phone = self.phoneText else { return }
        guard let smsCode = smsCodeInputView.textField.text else {
            iToast.makeText("请输入正确的验证码").show()
            return
        }
        guard let IDNum = idInputView.textField.text else {
            iToast.makeText("请输入正确的身份证号码").show()
            return
        }
        guard let name = nameInputView.textField.text else {
            iToast.makeText("姓名不能为空").show()
            return
        }
       let param = ["phone": phone,
                 "id_card": IDNum,
                 "realname": name,
                 "code": smsCode,
                 "type" : "find_pay_pwd"]

        HTTPManager.session.postRequest(forKey: kUserVerifyResetPassword, param: param, succeed: { (json, code, unwrap, msg) in
            if code == 0 && unwrap {
                let resetVc = ResetTransactionCodeController();
                resetVc.pageType = .find
                resetVc.smsCodeText = smsCode;
                resetVc.phoneNumText = phone;
                self.navigationController?.pushViewController(resetVc, animated: true);
            } else {
                QLAlert().show(withMessage: msg, singleBtnTitle: "确定")
            }
        }, failure: { (errMsg, isConnect) in
            
        })
    }
}
